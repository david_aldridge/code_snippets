digital_asset = DigitalAsset.find(309)
transfer = digital_asset.contact_digital_asset_transfers.last
digital_asset.approved_covers_changed.each do |approved_cover|
  transfer.transfer_includes.
            create( :book_id                   => approved_cover[:book ].id,
                    :transfer_includeable_type => approved_cover[:obj].class.name,
                    :transfer_includeable_id   => approved_cover[:obj].id,
                    :fingerprint               => approved_cover[:image].fingerprint)
  end
end
