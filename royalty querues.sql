    with
      cte_data_consolidator as (
        select
          rbb.royalty_batch_id                        batch_id,
          rbb.book_id                                         ,
          rbb.id                         royalty_batch_book_id,
          cast(s.masterchannel_id as integer) masterchannel_id,
          s.channel_id                                        ,
          s.invoice_date                                      ,
          s.id                                         sale_id,
          b.client_id                                         ,
          bm_rs.id                   book_royalty_specifier_id,
          b_rs.id      book_masterchannel_royalty_specifier_id,
          coalesce(bc_rs.royalty_rate_base_pct       ,bm_rs.royalty_rate_base_pct       ,b_rs.royalty_rate_base_pct           )    royalty_rate_base_pct,
          coalesce(bc_rs.royalty_rate_upper_limit_pct,bm_rs.royalty_rate_upper_limit_pct,b_rs.royalty_rate_upper_limit_pct,100) royalty_rate_upper_limit,
          coalesce(bc_rs.royalty_rate_lower_limit_pct,bm_rs.royalty_rate_lower_limit_pct,b_rs.royalty_rate_lower_limit_pct,  0) royalty_rate_lower_limit,
          case
            when bc_rs.price_basis != 'Inherit' then bc_rs.price_basis
            when bm_rs.price_basis != 'Inherit' then bm_rs.price_basis
            else Coalesce(b_rs.price_basis,'Missing')
          end price_basis,
          -- Appropriate values for sales without any escalators
          Cast('None' as varchar) quantity_modifier_type                ,
          Cast('None' as varchar) date_modifier_type                    ,
          Cast(Null   as date   ) date_escalator_escalation_date        ,
          Cast(Null   as decimal) date_escalator_royalty_percentage     ,
          Cast(Null   as decimal) date_escalator_royalty_rate_factor    ,
          Cast(Null   as decimal) discount_escalator_escalation_discount,
          Cast(Null   as decimal) discount_escalator_royalty_percentage ,
          Cast(Null   as decimal) discount_escalator_royalty_rate_factor,
          Cast(Null   as decimal) quantity_escalator_lower_limit        ,
          Cast(0      as decimal) quantity_escalator_royalty_percentage ,
          cast(/*p.price_amount*/ 0 as decimal) price_amount                ,
     /*     p.price_type   ,*/
          s.sale_value   ,
          s.sale_quantity,
          s.use_for_royalties
        from
          royalty_batch_books        rbb   join
          royalty_algorithm_versions rav   on (rav.id                   = rbb.royalty_algorithm_version_id   ) join
          royalty_algorithms         ra    on (ra.id                    = rav.royalty_algorithm_id           ) join
          sales                      s     on (s.book_id                = rbb.book_id                        ) join
          book_masterchannels        bm    on (bm.book_id               = s.book_id and
                                               bm.masterchannel_id      = cast(s.masterchannel_id as integer)) join
          book_channels              bc    on (bc.channel_id            = s.channel_id and
                                               bc.book_masterchannel_id = bm.id                              ) join
          books                      b     on (bm.book_id               = b.id                               ) left outer join
    /*      prices                     p     on (p.book_id                = b.id and
                                               p.defaultprice           = true                               ) left outer join */
          royalty_specifiers         b_rs  on (b_rs.royaltyable_id      = b.id and
                                               b_rs.royaltyable_type    = 'Book'                             ) left outer join
          royalty_specifiers         bm_rs on (bm_rs.royaltyable_id     = bm.id and
                                               bm_rs.royaltyable_type   = 'BookMasterchannel'                ) left outer join
          royalty_specifiers         bc_rs on (bc_rs.royaltyable_id     = bc.id and
                                               bc_rs.royaltyable_type   = 'BookChannel'                      )
        where
          b.isbn = '9781781381625' and
          rbb.royalty_batch_id = 29 and
          ra.name              = 'Simple'   and
          rav.version_number   = 3          and
          s.invoice_date       <= date '2015-07-31'),
      royalty_calc as (
        select
          client_id                             ,
          batch_id                              ,
          royalty_batch_book_id                 ,
          sale_id                               ,
          book_id                               ,
          masterchannel_id                      ,
          channel_id                            ,
          invoice_date                          ,
          sale_value                            ,
          sale_quantity                         ,
          use_for_royalties                     ,
          price_amount                          ,
          royalty_rate_base_pct                 ,
          royalty_rate_lower_limit              ,
          royalty_rate_upper_limit              ,
          quantity_modifier_type                ,
          Cast('None' as varchar) quantity_escalator_lower_limit       ,
          Cast('None' as varchar) quantity_escalator_royalty_percentage,
          Cast('None' as varchar) sale_fraction                        ,
          price_basis                           ,
          discount_escalator_escalation_discount,
          discount_escalator_royalty_percentage ,
          discount_escalator_royalty_rate_factor,
          date_modifier_type                    ,
          date_escalator_escalation_date        ,
          date_escalator_royalty_percentage     ,
          date_escalator_royalty_rate_factor    ,
          case use_for_royalties
            when true then
            case price_basis
              when 'Discount'
              then sale_value
              else sale_quantity*price_amount -- list price
            end
            else 0
          end royaltyable_amount,
          0.01
          *
          case use_for_royalties
            when true then
            case price_basis
              when 'Discount'
              then sale_value
              else sale_quantity*price_amount -- list price
            end
            else 0
          end
          *
          least(
            coalesce(royalty_rate_upper_limit,100),
            Greatest(
              coalesce(royalty_rate_lower_limit,0),
              royalty_rate_base_pct))  royalty_payable_amount
        from cte_data_consolidator)
    /*insert into
      sale_royalties (
        client_id                                     ,
        calculation_version                           ,
        royalty_batch_id                              ,
        sale_id                                       ,
        book_id                                       ,
        masterchannel_id                              ,
        channel_id                                    ,
        invoice_date                                  ,
        sale_value                                    ,
        sale_quantity                                 ,
        price_amount                                  ,
        royalty_rate_base_pct                         ,
        royalty_rate_lower_limit                      ,
        royalty_rate_upper_limit                      ,
        quantity_modifier_type                        ,
        quantity_escalator_lower_limit                ,
        quantity_escalator_royalty_percentage         ,
        sale_fraction                                 ,
        price_basis                                   ,
        discount_escalator_escalation_discount        ,
        discount_escalator_royalty_percentage         ,
        discount_escalator_royalty_rate_factor        ,
        date_modifier_type                            ,
        date_escalator_escalation_date                ,
        date_escalator_royalty_percentage             ,
        date_escalator_royalty_rate_factor            ,
        royaltyable_amount                            ,
        royalty_payable_amount                        ,
        change_in_payable_amount                      ,
        changed_book_id                               ,
        changed_masterchannel_id                      ,
        changed_channel_id                            ,
        changed_invoice_date                          ,
        changed_sale_value                            ,
        changed_sale_quantity                         ,
        changed_price_amount                          ,
        changed_royalty_rate_base_pct                 ,
        changed_royalty_rate_lower_limit              ,
        changed_royalty_rate_upper_limit              ,
        changed_quantity_modifier_type                ,
        changed_quantity_escalator_lower_limit        ,
        changed_quantity_escalator_royalty_percentage ,
        changed_sale_fraction                         ,
        changed_price_basis                           ,
        changed_discount_escalator_escalation_discount,
        changed_discount_escalator_royalty_percentage ,
        changed_discount_escalator_royalty_rate_factor,
        changed_date_modifier_type                    ,
        changed_date_escalator_escalation_date        ,
        changed_date_escalator_royalty_percentage     ,
        changed_date_escalator_royalty_rate_factor    ,
        changed_royaltyable_amount                    ,
        changed_royalty_payable_amount                ,
        sale_use_for_royalties                        ,
        royalty_batch_book_id)*/
    select
      rc.client_id                             ,
      Coalesce(sr.calculation_version,0)+1 calculation_version,
      rc.batch_id                              ,
      rc.sale_id                               ,
      rc.book_id                               ,
      rc.masterchannel_id                      ,
      rc.channel_id                            ,
      rc.invoice_date                          ,
      rc.sale_value                            ,
      rc.sale_quantity                         ,
      rc.price_amount                          ,
      rc.royalty_rate_base_pct                 ,
      rc.royalty_rate_lower_limit              ,
      rc.royalty_rate_upper_limit              ,
      rc.quantity_modifier_type                ,
      rc.quantity_escalator_lower_limit        ,
      rc.quantity_escalator_royalty_percentage ,
      rc.sale_fraction                         ,
      rc.price_basis                           ,
      rc.discount_escalator_escalation_discount,
      rc.discount_escalator_royalty_percentage ,
      rc.discount_escalator_royalty_rate_factor,
      rc.date_modifier_type                    ,
      rc.date_escalator_escalation_date        ,
      rc.date_escalator_royalty_percentage     ,
      rc.date_escalator_royalty_rate_factor    ,
      rc.royaltyable_amount                    ,
      round(cast(rc.royalty_payable_amount as numeric),4) royalty_payable_amount,
      round(cast(rc.royalty_payable_amount as numeric),4)-coalesce(sr.royalty_payable_amount,0) change_in_payable_amount,
      Case when sr.sale_id is null or rc.book_id                               =sr.book_id                                or Coalesce(rc.book_id                               ,sr.book_id                               ) is null then false else true end changed_book_id                               ,
      Case when sr.sale_id is null or rc.masterchannel_id                      =sr.masterchannel_id                       or Coalesce(rc.masterchannel_id                      ,sr.masterchannel_id                      ) is null then false else true end changed_masterchannel_id                      ,
      Case when sr.sale_id is null or rc.channel_id                            =sr.channel_id                             or Coalesce(rc.channel_id                            ,sr.channel_id                            ) is null then false else true end changed_channel_id                            ,
      Case when sr.sale_id is null or rc.invoice_date                          =sr.invoice_date                           or Coalesce(rc.invoice_date                          ,sr.invoice_date                          ) is null then false else true end changed_invoice_date                          ,
      Case when sr.sale_id is null or rc.sale_value                            =sr.sale_value                             or Coalesce(rc.sale_value                            ,sr.sale_value                            ) is null then false else true end changed_sale_value                            ,
      Case when sr.sale_id is null or rc.sale_quantity                         =sr.sale_quantity                          or Coalesce(rc.sale_quantity                         ,sr.sale_quantity                         ) is null then false else true end changed_sale_quantity                         ,
      Case when sr.sale_id is null or rc.price_amount                          =sr.price_amount                           or Coalesce(rc.price_amount                          ,sr.price_amount                          ) is null then false else true end changed_price_amount                          ,
      Case when sr.sale_id is null or rc.royalty_rate_base_pct                 =sr.royalty_rate_base_pct                  or Coalesce(rc.royalty_rate_base_pct                 ,sr.royalty_rate_base_pct                 ) is null then false else true end changed_royalty_rate_base_pct                 ,
      Case when sr.sale_id is null or rc.royalty_rate_lower_limit              =sr.royalty_rate_lower_limit               or Coalesce(rc.royalty_rate_lower_limit              ,sr.royalty_rate_lower_limit              ) is null then false else true end changed_royalty_rate_lower_limit              ,
      Case when sr.sale_id is null or rc.royalty_rate_upper_limit              =sr.royalty_rate_upper_limit               or Coalesce(rc.royalty_rate_upper_limit              ,sr.royalty_rate_upper_limit              ) is null then false else true end changed_royalty_rate_upper_limit              ,
      Case when sr.sale_id is null or rc.quantity_modifier_type                =sr.quantity_modifier_type                 or Coalesce(rc.quantity_modifier_type                ,sr.quantity_modifier_type                ) is null then false else true end changed_quantity_modifier_type                ,
      Case when sr.sale_id is null or rc.quantity_escalator_lower_limit        =sr.quantity_escalator_lower_limit         or Coalesce(rc.quantity_escalator_lower_limit        ,sr.quantity_escalator_lower_limit        ) is null then false else true end changed_quantity_escalator_lower_limit        ,
      Case when sr.sale_id is null or rc.quantity_escalator_royalty_percentage =sr.quantity_escalator_royalty_percentage  or Coalesce(rc.quantity_escalator_royalty_percentage ,sr.quantity_escalator_royalty_percentage ) is null then false else true end changed_quantity_escalator_royalty_percentage ,
      Case when sr.sale_id is null or cast('1' as varchar)                     =sr.sale_fraction                          then false else true end changed_sale_fraction                         ,
      Case when sr.sale_id is null or rc.price_basis                           =sr.price_basis                            or Coalesce(rc.price_basis                           ,sr.price_basis                           ) is null then false else true end changed_price_basis                           ,
      Case when sr.sale_id is null or rc.discount_escalator_escalation_discount=sr.discount_escalator_escalation_discount or Coalesce(rc.discount_escalator_escalation_discount,sr.discount_escalator_escalation_discount) is null then false else true end changed_discount_escalator_escalation_discount,
      Case when sr.sale_id is null or rc.discount_escalator_royalty_percentage =sr.discount_escalator_royalty_percentage  or Coalesce(rc.discount_escalator_royalty_percentage ,sr.discount_escalator_royalty_percentage ) is null then false else true end changed_discount_escalator_royalty_percentage ,
      Case when sr.sale_id is null or rc.discount_escalator_royalty_rate_factor=sr.discount_escalator_royalty_rate_factor or Coalesce(rc.discount_escalator_royalty_rate_factor,sr.discount_escalator_royalty_rate_factor) is null then false else true end changed_discount_escalator_royalty_rate_factor,
      Case when sr.sale_id is null or rc.date_modifier_type                    =sr.date_modifier_type                     or Coalesce(rc.date_modifier_type                    ,sr.date_modifier_type                    ) is null then false else true end changed_date_modifier_type                    ,
      Case when sr.sale_id is null or rc.date_escalator_escalation_date        =sr.date_escalator_escalation_date         or Coalesce(rc.date_escalator_escalation_date        ,sr.date_escalator_escalation_date        ) is null then false else true end changed_date_escalator_escalation_date        ,
      Case when sr.sale_id is null or rc.date_escalator_royalty_percentage     =sr.date_escalator_royalty_percentage      or Coalesce(rc.date_escalator_royalty_percentage     ,sr.date_escalator_royalty_percentage     ) is null then false else true end changed_date_escalator_royalty_percentage     ,
      Case when sr.sale_id is null or rc.date_escalator_royalty_rate_factor    =sr.date_escalator_royalty_rate_factor     or Coalesce(rc.date_escalator_royalty_rate_factor    ,sr.date_escalator_royalty_rate_factor    ) is null then false else true end changed_date_escalator_royalty_rate_factor    ,
      Case when sr.sale_id is null or rc.royaltyable_amount                    =sr.royaltyable_amount                     or Coalesce(rc.royaltyable_amount                    ,sr.royaltyable_amount                    ) is null then false else true end changed_royaltyable_amount                    ,
      Case when Coalesce(round(cast(rc.royalty_payable_amount as numeric),4),0)-coalesce(sr.royalty_payable_amount,0) =0 then false else true end                                                                                                           changed_royalty_payable_amount                ,
      rc.use_for_royalties,
      rc.royalty_batch_book_id
    from
      royalty_calc   rc  left outer join
      sale_royalties    sr on (sr.sale_id             = rc.sale_id and
                               sr.calculation_version = (select max(sr2.calculation_version)
                                                         from   sale_royalties sr2
                                                         where  sr2.sale_id = rc.sale_id))
    --where   Coalesce(round(cast(rc.royalty_payable_amount as numeric),4),0) != coalesce(sr.royalty_payable_amount,0)
    order by
            rc.book_id         ,
            rc.channel_id      ,
            rc.invoice_date;
