x  =     {
      jpg_rgb_0050w:     ["50x"  , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0075w:     ["75x"  , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0250w:     ["250x" , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0150h:     ["x150" , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0250h:     ["x250" , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0400h:     ["x400" , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_0650h:     ["x650" , :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_1500h:     ["x1500", :jpg , convert_options: ["-colorspace RGB", "-strip", "-quality 90"] ],
      jpg_rgb_original:  [""     , :jpg , convert_options: ["-colorspace RGB", "-strip"]            ],
      jpg_cmyk_original: [""     , :jpg , convert_options: "-colorspace CMYK"                  ],
      tiff_original:     [""     , :tiff],
      png_original:      [""     , :png ]
    }


Supportingresource.find(259875).image.reprocess!


filename = File.join(Rails.root, 'tmp',('a'..'z').to_a.shuffle[0,8].join + ".jpg")



sr = Supportingresource.find(259875)



Supportingresource.joins(:client).where.not(client_id: 361).where("image_file_size >= ?", 1_000_000).where("supportingresources.id < 219424").order(id: :desc).each do |sr|
  original_exists = sr.image.exists?(:original)
  if original_exists
    low_res_exists = sr.image.exists?(:jpg_rgb_0050w)
    if low_res_exists
      begin
        filename = File.join(Rails.root, 'tmp',('a'..'z').to_a.shuffle[0,30].join + ".jpg")
        sr.image.copy_to_local_file(:jpg_rgb_0050w, filename)
        img = Magick::Image::read(filename)[0]
        colorspace = img.colorspace
        reprocess = !(colorspace.to_s == "sRGBColorspace")
      ensure
        File.delete filename
      end
    else
      reprocess = true
    end
  else
    reprocess = false
  end
  puts "id: #{sr.id}"
  puts "original_exists: #{original_exists}"
  puts "low_res_exists: #{low_res_exists}"
  puts "colorspace: #{colorspace}" if low_res_exists
  puts "reprocess: #{reprocess}"
  if reprocess
    sr.image.styles.map(&:first).each do |style|
      sr.image.reprocess! style
    end
  end
end

x = Client.is_quiller.supportingresources.where(resourceable_type: "Work").select{|sr| sr.image.height}.map do |sr|
  [
    sr.image.height * sr.image.width,
    sr.resourceable_id,
    sr.image.height,
    sr.image.width
  ]
end.sort_by(&:first)

pp x


x = Client.is_quiller.
  supportingresources.
  where(resourceable_type: "Work").
  select do |sr|
    sr.image.height && [sr.image.height, sr.image.width].max < 1000
  end.
  map do |sr|
    [
      [sr.image.height, sr.image.width].max,
      "https://app.bibliocloud.com/works/#{sr.resourceable_id}",
      sr.resourceable.first_pub_date
    ]
  end.select{|x| x.last}.sort_by(&:first)

pp x







unless tempfile.nil?
    geometry = Paperclip::Geometry.from_file(tempfile)
    self.dimensions = [geometry.width.to_i, geometry.height.to_i]
  end


image = Supportingresource.find(259875).image
filename = File.join(Rails.root, 'tmp',('a'..'z').to_a.shuffle[0,30].join)
image.copy_to_local_file(:original, filename)
img = Magick::Image::read(filename)[0]
colorspace = img.colorspace
Paperclip::Geometry.from_file(filename)



before_save :extract_image_metadata

def extract_image_metadata
  tempfile = upload.queued_for_write[:original]
  unless tempfile.nil? || height.present?
    geometry               = Paperclip::Geometry.from_file(tempfile)
    self.image_height      = geometry.height.to_i
    self.image_width       = geometry.width.to_i
    self.image_colourspace = Magick::Image::read(tempfile)[0].colorspace.to_s
  end
end

:image_height, :integer
:image_width, :integer
:image_colourspace, :string
:image_format, :string


