class Test
  Message = Struct.new(:currency_code, :price_type)

  def initialize(message)
    raise ArgumentError unless message.is_a? Message
    @message = message
  end

  def to_s
    "#{message.currency_code} #{message.price_type}"
  end

  private

  attr_reader :message

end

Test.new(
  Test::Message.new("GBP", "05")
  ).to_s

  Message = Struct.new(:currency_code, :price_type) do
    def <=>(other)
      currency_code <=> other.currency_code
    end

    def currency_code_backwards
      currency_code.reverse
    end
  end

Struct.new("Message", :currency_code, :price_type)
