class ToThema
  def initialize(bic_codes)
    @bic_codes = bic_codes
  end

  def call
    {
      thema_main_code:      thema_main_code,
      thema_subject_codes:  thema_subject_codes,
      thema_geog_codes:     thema_geog_codes,
      thema_lang_codes:     thema_lang_codes,
      thema_time_codes:     thema_time_codes,
      thema_edu_codes:      thema_edu_codes,
      thema_age_codes:      thema_age_codes,
      thema_interest_codes: thema_interest_codes,
      thema_style_codes:    thema_style_codes
    }
  end

  private

  CONVERSIONS = YAML.load_file(Rails.root.join("config", "bic_to_thema.yml")).freeze

  private_constant :CONVERSIONS

  attr_reader :bic_codes

  delegate  :main_subject,
            :additional_subjects,
            :geographical,
            :language,
            :time_period,
            :educational_purpose,
            :interest_age,
            :special_interest,
            to: :bic_codes

  delegate  :convert,
            to: :class

  private_class_method def self.convert(codes)
    Array.wrap(codes).flat_map {|code| CONVERSIONS[code] }
  end

  def main_subject_map
    convert(main_subject)
  end

  def additional_subjects_map
    convert(additional_subjects)
  end

  def geographical_map
    convert(geographical)
  end

  def language_map
    convert(language)
  end

  def time_period_map
    convert(time_period)
  end

  def educational_purpose_map
    convert(educational_purpose)
  end

  def interest_age_map
    convert(interest_age)
  end

  def special_interest_map
    convert(special_interest)
  end

  def thema_main_code
    main_subject_map.first
  end

  def non_main_codes
    @non_main_codes ||= (
      main_subject_map(1..-1) +
      additional_subjects_map +
      geographical_map +
      language_map +
      time_period_map +
      educational_purpose_map +
      interest_age_map +
      special_interest_map
    ).uniq
  end

  def thema_subject_codes
    non_main_codes.select{|code| code.first.in?((A..Z)) }
  end

  def thema_geog_codes
    non_main_codes.select{|code| code.starts_with?("1") }
  end

  def thema_lang_codes
    non_main_codes.select{|code| code.starts_with?("2") }
  end

  def thema_time_codes
    non_main_codes.select{|code| code.starts_with?("3") }
  end

  def thema_edu_codes
    non_main_codes.select{|code| code.starts_with?("4") }
  end

  def thema_age_codes
    non_main_codes.select{|code| code.starts_with?("5A") }
  end

  def thema_interest_codes
    non_main_codes.select{|code| code.starts_with?("5") && !code.starts_with?("5A") }
  end

  def thema_style_codes
    non_main_codes.select{|code| code.starts_with?("6") }
  end
end