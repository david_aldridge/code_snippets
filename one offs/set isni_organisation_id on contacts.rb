Contact.where.not(isni: nil).where(corporate_name: nil).each do |contact|
  contact.update_attributes(
    isni_organisation_id: ISNI::Organisation.find_by(isni: contact.isni.gsub(/[^0-9X]/,""))&.id
  )
end



Contact.where.not(isni: nil).where.not(isni_organisation_id: nil).each do |contact|
  [
    contact.isni,
    contact.corporate_name,
    contact.isni_organisation.name,
    contact.country.to_s,
    contact.isni_organisation.country_code
  ]
end
