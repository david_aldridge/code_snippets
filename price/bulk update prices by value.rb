prices = {
  0.99 => OpenStruct.new(eur: 1.99, aud: 2.99, usd: 1.99, cad: 1.99),
  1.99 => OpenStruct.new(eur: 2.99, aud: 4.99, usd: 2.99, cad: 2.99),
  3.99 => OpenStruct.new(eur: 4.99, aud: 6.99, usd: 5.99, cad: 5.99),
  4.99 => OpenStruct.new(eur: 5.99, aud: 9.99, usd: 7.99, cad: 7.99),
  7.99 => OpenStruct.new(eur: 8.99, aud: 16.99, usd: 9.99, cad: 9.99),
  12.99 => OpenStruct.new(eur: 14.99, aud: 22.99, usd: 16.99, cad: 16.99),
  14.99 => OpenStruct.new(eur: 16.99, aud: 19.99, usd: 17.99, cad: 17.99),
  16.99 => OpenStruct.new(eur: 18.99, aud: 23.99, usd: 20.99, cad: 20.99),
  38.5 => OpenStruct.new(eur: 56.5, aud: 71.95, usd: 75.95, cad: 75.95),
  41.5 => OpenStruct.new(eur: 60.5, aud: 75.95, usd: 80.95, cad: 80.95),
  42.5 => OpenStruct.new(eur: 56.5, aud: 120.95, usd: 79.95, cad: 110.95),
  43.5 => OpenStruct.new(eur: 63.5, aud: 80.95, usd: 85.95, cad: 85.95),
  45.5 => OpenStruct.new(eur: 60.5, aud: 124.95, usd: 84.95, cad: 115.95),
  47.5 => OpenStruct.new(eur: 63.5, aud: 129.95, usd: 89.95, cad: 120.95),
}

client = Client.is_boldwood

ActiveRecord::Base.transaction do
  client.books.has_prices.each do |book|
    new_prices = prices[book.current_gbp_inctax_consumer_price.to_f]
    next unless new_prices

    book.current_eur_exctax_consumer_price = new_prices.eur
    book.current_aud_inctax_consumer_price = new_prices.aud
    book.current_usd_exctax_consumer_price = new_prices.usd
    book.current_cad_exctax_consumer_price = new_prices.cad
  end
end
