One of the most challenging parts of providing quality metadata to the supply chain is the correct formating of marketing texts.

We support nearly forty different types, such as main descriptions, short descriptions, tables of contents, lists of illustrations, indexes, reviews, and biographical notes. Any one of them can be supplied through ONIX in a number of different formats, which pretty much come down to the following:

* Plain text
* HTML
* XML

I can see you're bored already, so let's just talk briefly about plain text and HTML.

## Plain text

Just words, spaces, punctuation ... nothing fancy. Supported by everyone, displayable everywhere, and until a couple of years ago whatever you sent to Nielsen got disseminated to all of their metadata subscribers as plain text anyway. Completely non-controversial.

Plain text is great for brief, unstructured texts, like short descriptions or promotional headlines, but for anything more structured or lengthy you need to go to ...

## HTML

HTML lets you add structure and meaning to your text in a way that plain text can't. It lets you emphasise the name of a reviewer, author, or the title of a book, link to to other pages, add tables of data, insert images, reference CSS styles, and provide structured, multilevel tables of contents.

Unfortunately it also lets you do a lot of things that metadata recipients don't like. Some HTML that a browser will accept and diplay is in fact technically malformed (which accounts for some display glitches that Consonance has suffered from, where HTML marketing texts have not been technically correct), and there is a strict type of HTML called XHTML that is preferred for use in ONIX.

Furthermore the ONIX documentation states that "... some ONIX recipients may be reluctant to use XHTML text that contains links, images, tables, or that uses attributes such as style". In fact the ONIX best practice guide lists a small set of tags that are recommnded:

* Paragraph and line breaks
  * `<p>`
  * `<br />`
* Emphasis
  * `<strong>`
  * `<em>`
  * `<b>`
  * `<i>`
  * `<cite>` (only for references to titles of works: books, poems, movies, musicals, exhibitions, computer programs, etc)
* Bulleted and numbered lists
  * `<ul>`
  * `<ol>`
  * `<li>`
* Sub/superscripts
  * `<sub>`
  * `<sup>`
* Definitions
  * `<dl>`
  * `<dt>`
  * `<dd>`
* Glosses
  * `<ruby>`
  * `<rb>`
  * `<rp>`
  * `<rt>`

The first two sets are widely used in descriptive texts, the third in tables of contents and other lists, and the rest pretty rarely.

It's worth mentioning the issue surrounding the use of the <b> and <i> versus <strong> and <emphasis>. Basically, there used to be just <b> and <i>, then <strong> and <em> were introduced, then <b> and <i> were deprecated in favour of <strong> and <em>, and now <b> and <i> have been restored but for text that is to have a different style but is not meant to be more significant than other text. For this reason <b> and <i> are not regarded as accessible either for presentation on the internet or for ebooks, because they are not announced by screenreaders. Use of <b> and <i> are WCAG Level A failures. https://support.siteimprove.com/hc/en-gb/articles/115002726312-Accessibility-Bold-vs-Strong-and-Italic-vs-Emphasis

<cite>



So in fact writing a marketing text that will be acceptable to all recipients means the following:

* Writing plain text
* Writing HTML that:
    1 Is strictly correct XHTML, not just HTML.
    2 Does not have elements or attributes that recipients might object to.

This is not too difficult to do for small, simple marketing texts, but gets quite challenging on larger texts.