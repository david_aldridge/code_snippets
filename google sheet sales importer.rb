class GoogleSheetSalesImport


  def initialize(key,client)
    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key(key)
    @product_sheet = @sheet.worksheet_by_title("Products")
    @product_list  = @product_sheet.list
    @client        = client
  end

  def raw_data
    @raw_data
  end

  def product_list
    @product_list
  end

  def push_products
    @client.books.order(:isbn, :id).each_with_index do |book, i|
      if ISBN.valid? book.isbn
        @product_list[i]={"ISBN" => book.isbn,
                          "Title"=> book.title,
                          "Product form" => book.product_form,
                          "Pub date"     => book.pub_date,
                          "ID"           => book.id}
      end
    end
    @product_sheet.save
  end

end


__END__

i = GoogleSheetSalesImport.new("1Jo3WDs-LG2OPSM-0KzA7GSNHOTrdG4gxWXM1HowKsNA",  Client.find_by(:webname => "vertebratepublishing")).push_products




    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key("1_yqENJN2c4cpj70uDg_oT8sWf2gFMHg2Tw6NgEAsQG4")
    @product_sheet = @sheet.worksheet_by_title("Products")
    @product_list  = @product_sheet.list
    @client        = Client.find(373)

@client.books.order(:isbn, :id).each_with_index do |book, i|
  @product_list[i]

@product_list[0]={"ISBN" => "9781898", "Title"=> "Whatever"}