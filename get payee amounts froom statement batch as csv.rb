csv_string = CSV.generate(quote_char: '"', force_quotes: true) do |csv|
  csv << [
    "statement_id",
    "payee_id","payee_name",
    "payee_supplier_reference",
    "statement_from_date",
    "statement_to_date",
    "statement_status",
    "statement_payment_amount"
  ]
  StatementBatch.find(261).
    statements.
    includes(
        statement_batch_contact: :contact,
        statement_contracts: :statement_alternative_payees
    ).each do |s|
      csv <<
        [
          s.id,
          s.contact.id,
          s.contact.to_s,
          s.contact.supplier_ref,
          s.date_from&.to_formatted_s(:iso8601),
          s.date_to&.to_formatted_s(:iso8601),
          s.status,
          s.payee_amt.to_f
        ]
    end
end ; 0
