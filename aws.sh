# http://docs.aws.amazon.com/cli/latest/userguide/using-s3-commands.html
#move a folder

aws s3 mv s3://bibliocloudimages-iop/:client_id/ s3://bibliocloudimages-iop/ --recursive --acl public-read


aws s3 cp s3://generalproductslog/ --include "2018-05-30-13-4*"


aws s3 cp s3://generalproductslog/bibliocloudimages/ . --exclude "*" --include "2018-05-30-13-4*"



aws s3 cp s3://bibliocloudimages/1/1/ s3://consonance-dev/1/1 --recursive --acl public-read

aws s3 cp . s3://consonance-nbni-onix-backup --recursive --storage-class REDUCED_REDUNDANCY --include "*.zip" --region eu-west




Copy from one bucket to another
==================================

aws s3 cp s3://bibliocloudimages/1/ s3://bibliocloudimagesdev3/1/ --recursive --acl public-read

aws s3 cp s3://consonance-tf/2/product_assets/ . --recursive


1/250696//_original.jpg