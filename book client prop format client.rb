 Book.
   joins(:proprietary_edition_description).
   where("books.client_id != proprietary_edition_descriptions.client_id").
   joins(:client).
   merge(Client.active).
   each do |book|
   book.update(
     proprietary_edition_description:  book.client.proprietary_edition_descriptions.find_by(name: book.proprietary_edition_description.name)
end