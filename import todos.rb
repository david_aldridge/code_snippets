todos = [
["Acquisitions: Agmts from Contributors", "Acquisitions: Agmts to Contributors"],
["Acquisitions: Agmts to Contributors", "Acquisitions: Board Approved"],
["Acquisitions: Board Approved", "Acquisitions: Board Packets Sent"],
["Acquisitions: Board Packets Sent", "Acquisitions: Peer Reviews In"],
["Acquisitions: Book Transmitted ", "Acquisitions: Contract from Author"],
["Acquisitions: Contract from Author", "Acquisitions: Contract to Author"],
["Acquisitions: Contract to Author", "Acquisitions: Board Approved"],
["Acquisitions: MS received", ""],
["Acquisitions: MS to Copy Editing", "Acquisitions: MS received, Acquisitions: Contract from Author, Acquisitions: Board Approved"],
["Acquisitions: Out for Peer Review", "Acquisitions: MS received"],
["Acquisitions: Peer Reviews In", "Acquisitions: Out for Peer Review"],
["Editorial: 2nd Pass Checked", "Editorial: 2nd Pass In"],
["Editorial: 2nd Pass In from Production", "Production: Out for 2nd Pass"],
["Editorial: 2nd Pass plus index to Production", "Editorial: 2nd Pass Checked, Editorial: Index In"],
["Editorial: Collating Finished", "Editorial: Collating Started"],
["Editorial: Collating Started", "Editorial: First Pass Back from Proofreader, Editorial: First Pass Back from Author"],
["Editorial: Edited MS From Author", "Editorial: Edited MS to Author"],
["Editorial: Edited MS to Author", "Editorial: MS from Copyeditor"],
["Editorial: First Pass Back from Author", "Editorial: First Pass To Author"],
["Editorial: First Pass Back from Proofreader", "Editorial: First Pass To Proofreader"],
["Editorial: First Pass In from Production", "Production: First Pass In"],
["Editorial: First Pass To Author", "Production: First Pass In"],
["Editorial: First Pass To Proofreader", "Production: First Pass In"],
["Editorial: First Pass to Indexer", "Production: First Pass In"],
["Editorial: Index In", "Editorial: First Pass to Indexer"],
["Editorial: Index checked", "Editorial: Index In"],
["Editorial: MS from Copyeditor", "Editorial: MS to Copyeditor"],
["Editorial: MS to Copyeditor", "Acquisitions: MS to Copy Editing"],
["Editorial: MS to Design", "Editorial: Edited MS From Author"],
["Editorial: Pre-Proof Check", ""],
["Editorial: Proofreader Name", ""],
["Marketing: AQ In", "Marketing: AQ sent"],
["Marketing: AQ sent", "Acquisitions: Book Transmitted "],
["Marketing: All Blurbs In", "Marketing: Out for Blurbs"],
["Marketing: Author Bio Approved", "Marketing: Author Bio Sent"],
["Marketing: Author Bio Sent", "Marketing: AQ In"],
["Marketing: Catalog Copy Approved by Author", "Marketing: Catalog Copy Sent to Author"],
["Marketing: Catalog Copy Ready", "Marketing: Catalog Copy Routing Started"],
["Marketing: Catalog Copy Routing Started", "Marketing: Catalog Copy Approved by Author"],
["Marketing: Catalog Copy Sent to Author", "Marketing: Catalog Copy Written"],
["Marketing: Catalog Copy Written", "Marketing: All Blurbs In, Marketing: Author Bio Approved"],
["Marketing: Catalog Ready", "Marketing: Catalog Copy Ready"],
["Marketing: Catalog Routing Started", "Marketing: Catalog Ready"],
["Marketing: Catalog to Printer", "Marketing: Catalog Approved"],
["Marketing: Cover Copy Approved by Author", "Marketing: Cover Copy Sent to Author"],
["Marketing: Cover Copy Ready", "Marketing: Catalog Copy Routing Started"],
["Marketing: Cover Copy Routing Started", "Marketing: Cover Copy Approved by Author"],
["Marketing: Cover Copy Sent to Author", "Marketing: Cover Copy Written"],
["Marketing: Cover Copy Written", "Marketing: All Blurbs In, Marketing: Author Bio Approved"],
["Marketing: Cover Copy to Production", "Marketing: Cover Copy Ready"],
["Marketing: Final Catalog Approved", "Marketing: Catalog Routing Started"],
["Marketing: First Pass Bound Galley In", "Marketing: First Pass Bound Galley to Printer"],
["Marketing: First Pass Bound Galley Ready", "Production: Create Bound Galley files for Marketing"],
["Marketing: First Pass Bound Galley to Printer", "Production: Create Bound Galley files, Production: First Pass In"],
["Marketing: Out for Blurbs", "Production: First Proofs In"],
["Production: 2nd Pass In", "Production: Out for 2nd Pass"],
["Production: 2nd Pass plus Index Back from Editorial", "Production: 2nd Pass to Editorial, Editorial: Index In"],
["Production: 2nd Pass to Editorial", "Production: 2nd Pass In"],
["Production: Book Released", "Production: Text Proof Approved, Production: Cover Proof Approved"],
["Production: Compositor Name", ""],
["Production: Cover Files In", "Production: Cover Files to Designer"],
["Production: Cover Files to Designer", "Marketing: Cover Copy Ready"],
["Production: Cover Proof Approved", "Production: Cover Proof In"],
["Production: Cover Proof In", "Production: To Printer"],
["Production: Cover Routing Started", "Production: Cover Files In"],
["Production: Create Bound Galley files for Marketing", "Production: First Pass In"],
["Production: Files sent to ebook vendor", "Production: Text Proof Approved"],
["Production: Final Cover Approved", "Production: Cover Routing Started"],
["Production: Final Pass Approved", "Production: Final Pass In"],
["Production: Final Pass In", "Production: Out for Final Pass"],
["Production: First Pass Back from Editorial", "Production: First Pass to Editorial, Editorial: Collating Finished"],
["Production: First Pass In", "Production: Out for First Pass"],
["Production: First Pass to Editorial", "Production: First Pass In"],
["Production: Out for 2nd Pass", "Production: First Pass Back from Editorial"],
["Production: Out for Final Pass", "Production: 2nd Pass In"],
["Production: Out for First Pass", "Editorial: MS to Design"],
["Production: Text Proof/Check Copy Approved", "Production: Text Proof In"],
["Production: Text Proof/Check Copy In", "Production: To Printer"],
["Production: To Printer", "Production: Final Proofs Approved, Production: Final Cover Approved"],
["Production: Typeface", ""],
["Production: ebooks In", "Production: Files sent to ebook vendor"]
]


client = Client.is_notre_dame

work = client.works.find(92131)

todos.each do |todo_name, blocking_todo_names|
  work.todos.regular.create(name: todo_name)
end

todos.each do |todo_name, blocking_todo_names|
  next if blocking_todo_names.blank?
  puts todo_name
  work.todos.find_by(name: todo_name.squish).precursors =
    work.todos.where(name: blocking_todo_names.split(","))
end
