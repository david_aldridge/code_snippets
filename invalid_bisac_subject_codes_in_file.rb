file = "/Users/david/Documents/Bibliocloud/Clients/McGill Queens/From McGill/ebooks.xml"

doc = Nokogiri::XML(open(file)) ; 0
doc.remove_namespaces! ; 0

Struct.new("Subject", :scheme, :code)

additional_codes_in_file =
  doc.xpath("/ONIXMessage/Product/Subject").map do |subject|
    Struct::Subject.new(
      subject.at_xpath("SubjectSchemeIdentifier")&.text,
      subject.at_xpath("SubjectCode")&.text
    )
  end

main_codes_in_file =
  doc.xpath("/ONIXMessage/Product/BASICMainSubject").map do |subject|
    Struct::Subject.new(
      "10",
      subject.text
    )
  end

all_codes_in_file = (main_codes_in_file + additional_codes_in_file).uniq

valid_bisac_codes = Work::MAIN_BISAC_CODES.map(&:last).map(&:to_s)

invalid_bisac_codes_in_file =
  all_codes_in_file.select{|code| code.scheme == "10"}.map(&:code) - valid_bisac_codes
