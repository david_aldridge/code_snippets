Marketingtext.joins(:client).merge(Client.active).
  group(:main_type, :text_type, :legacy_code).
  order(:main_type, :text_type, :legacy_code).
  count.
  group_by{|codes, count| codes.first}

Marketingtext.joins(:client).merge(Client.active).
  group(:main_type, :legacy_code).
  order(:main_type, :legacy_code).
  count.
  group_by{|codes, count| codes.first}

Marketingtext.joins(:client).merge(Client.active).
  where(main_type: nil, legacy_code: nil).
  group(:pull_quote).
  count

Marketingtext.joins(:client).merge(Client.active).
  group(:legacy_code, :main_type, :text_type).
  order(:legacy_code, :main_type, :text_type).
  count.
  group_by{|codes, count| codes.first}

reload!
Marketingtext.
  joins(:client).
  merge(Client.active).
  where.not(legacy_code: Marketingtext::TYPE_DEFINITIONS.values.map(&:onix2_code).compact).
  group(:legacy_code).
  order("count(*)").
  count

reload!
Marketingtext.
  joins(:client).
  merge(Client.active).
  where.not(text_type: Marketingtext::TYPE_DEFINITIONS.values.map(&:onix3_code).compact).
  group(:text_type, :legacy_code).
  order("count(*)").
  count


OnixCode.where(list_number: 153).pluck(:value) - Marketingtext::SUBTYPE_DEFINITIONS.values.map(&:onix3_code)



