client = Client.is_academic_studies

copyright_holders = client.contacts.where(id: 92849)

client.
  works.
  where(copyright_year: nil).
  each do |work|
    work.copyright_owners = copyright_holders
    first_published = work.books.minimum(:pub_date)
    next unless first_published
    work.update_attributes(copyright_year: first_published.year)
  end



client.
  works.
  group(:copyright_year).
  order(:copyright_year).
  count

pp Client.is_pharma.works.where(copyright_year: nil).pluck(:title).sort

imprint = Client.is_liverpool.imprints.find_by(code: "VOLT")
copyright_holders = Client.is_liverpool.contacts.where(corporate_name: "Voltaire Foundation")

imprint.
  works.
  each do |work|
    work.copyright_owners = copyright_holders
    first_published = work.books.minimum(:pub_date)
    next unless first_published
    work.update_attributes(copyright_year: first_published.year)
  end

