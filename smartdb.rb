@order = Order.find(324)
if @order.high_risk? && @order.limit > 2000
  @order.lines.open.reduce_discount_by(10)
end

Order.
  high_risk.
  limit_exceeds(2000).
  find_by(id:324).
  order_lines.
  open.
  reduce_discount_by(10)
