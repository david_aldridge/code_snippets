Client.is_sas.marketingtexts do |text|
  text.update_attributes(
    pull_quote: text.pull_quote&.gsub("<i>", "<em>")&.gsub("</i>", "</em>")&.gsub("<b>", "<strong>")&.gsub("</b>", "</strong>"),
    marketing_text: text.marketing_text&.gsub("<i>", "<em>")&.gsub("</i>", "</em>")&.gsub("<b>", "<strong>")&.gsub("</b>", "</strong>"),
    alternate_pull_quote: text.alternate_pull_quote&.gsub("<i>", "<em>")&.gsub("</i>", "</em>")&.gsub("<b>", "<strong>")&.gsub("</b>", "</strong>"),
    alternate_marketing_text: text.alternate_marketing_text&.gsub("<i>", "<em>")&.gsub("</i>", "</em>")&.gsub("<b>", "<strong>")&.gsub("</b>", "</strong>")
  )
end
