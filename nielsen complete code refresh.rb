client = Client.is_liverpool

if Date.today == Date.today.end_of_week
  # Nielsen, Bowker, BDS, Turpin, Cambridge-Core
  [14].each do |cdatt_id|
    DigitalAssetPush.new( client,
                          client.contact_digital_asset_transfer_templates.find(14),
                          pub_dates_to: Date.today + 9.months,
                          incremental: false).run

====================================

client = Client.is_riba

nielsen_for_publishing_transfer_template =
  client.contact_digital_asset_transfer_templates.find(24)

nielsen_for_distribution_transfer_template =
  client.contact_digital_asset_transfer_templates.find(46)

templates =
  [
    nielsen_for_publishing_transfer_template,
    nielsen_for_distribution_transfer_template
  ]

templates.each do |template|
  DigitalAssetPush.new(client, template, books: client.books.onix_export_allowed,
  incremental: false).run
end

====================================


client = Client.is_zed

nielsen_transfer_template =
  client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first

DigitalAssetPush.new(client, nielsen_transfer_template, books: client.books.onix_export_allowed,
incremental: false).run


====================================

    PED_ID_FOR_VOLUMES    = 532
    PED_ID_FOR_MONOGRAPHS = 695

    PUB_STATUS_UNKNOWN = "00"

client = Client.is_bds

nielsen_transfer_template =
  client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first

standard_books = (
  client.
  books.
  onix_export_allowed.
  joins(:proprietary_edition_description).
  references(:proprietary_edition_description).
  where(proprietary_edition_descriptions: {id: [PED_ID_FOR_VOLUMES, PED_ID_FOR_MONOGRAPHS]}).
  where.not(publishing_status: [nil, PUB_STATUS_UNKNOWN]).
  reject(&:isbn_invalid?)
  )

DigitalAssetPush.new(client, nielsen_transfer_template  , books: standard_books, incremental: false).run
