client = Client.is_british_library

CreateStandardContacts.new(client: client, identifiers: ["nielsen"]).call




product_forms = client.books.onix_export_allowed.pluck(:product_form).compact.uniq - ["00"]
publishing_statuses = client.books.onix_export_allowed.pluck(:publishing_status).compact.uniq.sort


eb = client.export_batches.create(
  contact_digital_asset_transfer_template_id: client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first.id,
  name: "Nielsen test",
  incremental: false
  )

eb.books +=
  client.imprints.flat_map do |imprint|
      books = imprint.books.
        onix_export_allowed

      selected = books.order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "imprint #{imprint}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
product_forms.flat_map do |product_form|
      books = client.books.
        where(product_form: product_form).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today))


      selected = books.
        where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "form #{product_form}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
publishing_statuses.flat_map do |pub_status|
      books = client.books.
        where(publishing_status: pub_status).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 1.months))

      selected = books.where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "status #{pub_status} (#{OnixCode.lookup(:publishing_statuses, pub_status)}): #{selected.size} of #{books.size}"
      selected
    end ; 0


eb.get_content_items


ExportBatchItem.where(id: [11038153, 11038147, 11038127]).destroy_all




client.works.select do |w|
  !w.marketingtexts.where(legacy_code: "02").any? && w.marketingtexts.where(legacy_code: "03").any?
end.size


client.works.includes(:marketingtexts).map do |w|
  [
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)


client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)

["04", false, true, true]=>166,

client.books.onix_export_allowed.includes(:marketingtexts).select do |w|
    w.marketingtexts.none?{|x| x.legacy_code == "01"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "02"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
end.map do |w|
  [
    w.marketingtexts.detect{|x| x.legacy_code == "02"}.pull_quote.size,
    w.marketingtexts.detect{|x| x.legacy_code == "03"}.pull_quote.size
  ]
end.sort
group_by(&:itself).transform_values(&:size)







client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.detect{|x| x.legacy_code == "01"}&.pull_quote&.size&.>(350),
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.detect{|x| x.legacy_code == "02"}&.pull_quote&.size&.<=(350),
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)






client = Client.is_trigger
client.books.onix_export_allowed.map do |b|
 [
   b.imprint.to_s,
   b.isbn_registrant_hyphenated
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}
