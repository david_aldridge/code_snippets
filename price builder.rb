PriceBuilder.new(book.prices).
  discard(currency_code: ["AUD"]).
  using(currency_code: ["GBP"], including_tax: false).
  change(including_tax: true).
  save.
  using(currency_code: ["EUR"], including_tax: true).
  copy.
  remove_tax.
  change(country_code: "FR").
  add_tax.
  change(qualifier: "06").
  change(price: "* 6").
  using_all.
  prices


PriceBuilder.new(book.prices).apply_actions(JSON.parse(row.price_builder_actions)).prices

    #def call
    #  method_chain.reduce(self) do |obj, method|
    #    obj.send(method)
    #  end
    #end


[
  {
    action: "discard",
    options: {
      currency_code: ["GBP"]
    }
  },
  {

  }
]

pb = PriceBuilder.new
directions.each do |direction|
  raise UnknownAction unless pb.respond_to?(direction["action"])
  pb.send(direction["action"], *direction["options"])
end
pb.prices

PriceBuilder.new(book.prices).
  discard(currency_code: ["GBP","USD"]).
  using(currency_code: ["EUR"], including_tax: true).
  change(including_tax: false).
  change(country_code: "FR").
  change(including_tax: true).
  using(currency_code: ["EUR"], including_tax: true).
  change(including_tax: false).
  change(country_code: "FR").
  change(including_tax: true)

PriceBuilder.new(book.prices).
  exclude_tax.
  prices


PriceBuilder.new(book.prices).
  discard(currency_code: ["GBP","USD"]).
  using(currency_codes: ["EUR"], including_tax: true).
    exclude_tax.
    country_codes(["FR"]).
    include_tax.
    set_tax_components.
  using(currency_code: ["EUR"], including_tax: true).
    change(including_tax: false).
    change(country_code: "FR").
    change(including_tax: true)
