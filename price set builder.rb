class Thing
  def initialize(price_stack)
    raise "Must be an array of Price" unless self.class.array_of_prices?(price_stack)
    self.price_stack    = price_stack
    self.working_prices = []
  end

  def self.array_of_prices?(prices)
    return false unless prices.is_a? Array
    return false if prices.detect {|price| !price.is_a? Price}
    true
  end

  def using
    self.working_prices = price_stack.select{|price| yield(price)}
    self
  end

  def without
    self.working_prices -= price_stack.select{|price| yield(price)}
    self
  end

  def with
    self.working_prices = (self.working_prices + price_stack.select{|price| yield(price)}).uniq
    self
  end


  attr_reader :price_stack, :working_prices

  private

  attr_writer :price_stack, :working_prices
end


thing = Thing.new(Book.first.prices.to_a)

thing.with{|price| price.usd?}
