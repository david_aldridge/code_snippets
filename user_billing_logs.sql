select log_date,
       count(*) filter (where billable),
       count(*) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = 'innova'
group by log_date
order by log_date desc;

select log_date,
       count(*) filter (where billable),
       count(*) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = 'scribd'
group by log_date
order by log_date desc;




select date_trunc('month',log_date),
       count(distinct user_id) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = 'scribd'
group by date_trunc('month',log_date)
order by date_trunc('month',log_date);


select c.identifier,
       date_trunc('month',log_date),
       count(*) filter (where billable),
       count(distinct user_id) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
group by date_trunc('month',log_date)
order by 2 desc, 4 desc, 1;

select log_date,
       count(*) filter (where billable),
       count(*) filter (where activity),
       repeat('=', (100* count(*) filter (where activity) / count(count(*) filter (where activity)) over ())::int)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
group by log_date
order by log_date desc;

select log_date,
       count(*) filter (where billable),
       count(*) filter (where activity),
       repeat('=', (count(*) filter (where activity))::int)
         as "....|....10...|....20"
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
group by log_date
order by log_date desc;


select u.email,
       min(log_date) filter (where billable),
       max(log_date) filter (where billable),
       min(log_date) filter (where activity),
       max(log_date) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = 'liverpool'
group by u.email
order by u.email;

select u.email,
       min(log_date) filter (where billable),
       max(log_date) filter (where billable),
       min(log_date) filter (where activity),
       max(log_date) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = 'de-montfort'
group by u.email
order by u.email;

select u.email,
       min(log_date) filter (where billable),
       max(log_date) filter (where billable),
       min(log_date) filter (where activity),
       max(log_date) filter (where activity)
from user_billing_logs ubl join
     users u on u.id = ubl.user_id join
     clients c on u.client_id = c.id
where c.identifier = '404'
group by u.email
order by u.email;
