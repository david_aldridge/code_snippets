client = Client.is_leuven

ActiveRecord::Base.transaction do
  client.proprietary_edition_descriptions.each do |old_ped|
    new_ped = old_ped.dup
    new_ped.save
    old_ped.update_attributes(name: "#{old_ped.name} - Print")
    old_ped.update_attributes(code: "#{old_ped.code} - P") if old_ped.code
    new_ped.update_attributes(name: "#{new_ped.name} - EBook")
    new_ped.update_attributes(code: "#{new_ped.code} - E") if new_ped.code
    new_ped.books_count = 0
    new_ped.save
    old_ped.books.is_ebook.each do |ebook|
      ebook.proprietary_edition_description = new_ped
      ebook.save
    end
  end
  # raise ActiveRecord::Rollback
end


