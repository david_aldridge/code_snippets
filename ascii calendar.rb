start_date = Date.today - 10.days
end_date = start_date + 12.days
days  = (start_date.beginning_of_month.beginning_of_week..start_date.end_of_month.end_of_week)
weeks = days.group_by(&:cweek)

cal = [
  "Mo Tu We Th Fr Sa Su",
  "--------------------",
  weeks.map do |week_number, days|
    days.map do |d|
      if d < start_date || d > end_date
        d.day.to_s.rjust 2
      elsif d == start_date
        "|-"
      elsif d == end_date
        "-|"
      else
        ">>"
      end
    end.join(" ")
  end
]


cal.each {|c| puts c} ; 0
