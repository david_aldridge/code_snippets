

  def load_contacts
    #@client.contacts.destroy_all
    Contact.skip_callback(:validation , :before , :set_names     )
    Contact.skip_callback(:validation , :before , :set_life_dates)

    @list.each do |row|
      next if row["Unknown"] == 'y'
      contact = @client.contacts.find(row['id'])
      contact.assign_attributes(
                            :corporate_name              => row['corporate_name'             ].strip.presence,
                            :corporate_acronym           => row['corporate_acronym'          ].strip.presence,
                            :is_a_single_name_person     => row['is_a_single_name_person'    ].strip.presence,
                            :titles_before_names         => row['titles_before_names'        ].strip.presence,
                            :names_before_key            => row['names_before_key'           ].strip.presence,
                            :keyname_prefix              => row['keyname_prefix'             ].strip.presence,
                            :keynames                    => row['keynames'                   ].strip.presence,
                            :name_after_keyname          => row['name_after_keyname'         ].strip.presence,
                            :suffix_after_keyname        => row['suffix_after_keyname'       ].strip.presence,
                            :qualification_after_keyname => row['qualification_after_keyname'].strip.presence,
                            :titles_after_names          => row['titles_after_names'         ].strip.presence)

      part1       = [ contact.names_before_key    ]

      part2       = [ contact.keyname_prefix       ,
                      contact.keynames             ,
                      contact.name_after_keyname   ,
                      contact.suffix_after_keyname ].compact.join(' ')

      pn          = [ part1, part2 ].compact.join(' ')
      pni         = [ part2, part1 ].compact.join(', ')

      if contact.corporate_name.blank?
        if contact.is_a_single_name_person
          person_name          = contact.keynames
          person_name_inverted = contact.keynames
        else
          person_name          = pn
          person_name_inverted = pni
        end
      else
        person_name          = nil
        person_name_inverted = nil
      end

      contact.assign_attributes(:person_name          => person_name         ,
                                :person_name_inverted => person_name_inverted)

      puts "#{row['id']}: #{contact.person_name || contact.corporate_name}"
      if contact.valid?
        contact.save!
      else
        puts contact.errors.full_messages.join(": ")
      end
    end

    Contact.set_callback(:validation , :before , :set_names     )
    Contact.set_callback(:validation , :before , :set_life_dates)

  end

