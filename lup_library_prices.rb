Rails.logger.level=1

Client.is_liverpool.books.is_ebook.where(:epub_type_code => '002').each do |ebook|
  unless gbp_consumer_price = ebook.current_gbp_inctax_consumer_price
    gbp_consumer_price = ebook.work.books.where(:product_form => 'BB').last.try(:current_gbp_inctax_consumer_price)
    ebook.current_gbp_inctax_consumer_price = gbp_consumer_price
  end

  if gbp_consumer_price
    ebook.current_gbp_inctax_library_price = (2.25 * gbp_consumer_price).round(2)
  end

  unless usd_consumer_price = ebook.current_usd_exctax_consumer_price
    usd_consumer_price = ebook.work.books.where(:product_form => 'BB').last.try(:current_usd_exctax_consumer_price)
    ebook.current_usd_exctax_consumer_price = usd_consumer_price
  end

  if usd_consumer_price
    ebook.current_usd_exctax_library_price = (2.25 * usd_consumer_price).round(2)
  end
end


pp( Client.is_liverpool.
       books.
       is_ebook.
       where(:epub_type_code => '002').
       where("pub_date < ?", Date.today + 2.months).
       all.
       select{|b| b.current_usd_exctax_library_price.nil? || b.current_gbp_inctax_library_price.nil?}.
       map{|b| [b.pub_date, b.title, b.work.books.where("product_form like 'B%'").pluck(:product_form)]}, nil, 200)


Book.find(74451)






Client.is_liverpool.books.hardbacks
