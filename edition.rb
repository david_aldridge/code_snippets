class Edition << ActiveRecord::Base

  attr_accessible :client_id         ,
                  :work_id           ,
                  :number            ,
                  :edition_type_codes

end
