DelayedJob.all.each do |d|
  d.last_error = nil
  d.run_at = Time.now
  d.failed_at = nil
  d.locked_at = nil
  d.locked_by = nil
  d.attempts = 0
  d.failed_at = nil # needed in Rails 5 / delayed_job (4.1.2)
  d.save!
end

DelayedJob.where("handler like '--- !ruby/object:BulkUploadPro%'").each do |d|
  d.last_error = nil
  d.run_at = Time.now
  d.failed_at = nil
  d.locked_at = nil
  d.locked_by = nil
  d.attempts = 0
  d.failed_at = nil # needed in Rails 5 / delayed_job (4.1.2)
  d.save!
end


DelayedJob.pluck(:handler).map{|x| x.first(30) }

DelayedJob.where("handler like '--- !ruby/object:BulkUploadPro%'").pluck(:last_error).map{|x| x.first(30) }