---
title: "To-do FAQs"
slug: "to-do_faqs"
excerpt: "To-do Frequently Asked Questions"
layout: docs_layout
tags: Article
date: 2018-11-01

---

# How do we get started?

Start with a work that you will be publishing fairly soon. You probably have a pretty good idea from experience of what needs to be done, and who will need to do it. Add all of the tasks you can think of.

Do not worry too much at the moment about when you will be doing them, unless it's pretty clear without too much thought (there may be some tasks that you will need to do very close to or on the publication day for example, such as send out some social media messages, or check that the product is showing as “available” on retailer sites).

Work together to review the list. Very often a person will recall a task because they saw another one that someone else added.

For now, don't worry too much about tasks you have already done, but add them if it distracts you that they are not present.

Do not skimp on the number of to-dos. If it is worth being reminded of, it is worth adding, and it is easier to delete ones that you know you will not need later on than it is to remember to add them.

When you have a good sized list, think about when tasks need to be done. Relative dates are a powerful feature that will make your list re-usable for other works, so always try to specify a relative date, based on publication date for production tasks.

Think about exactly which basis date is the most appropriate for each to-do. Maybe you have multiple publication dates, for different formats and markets. Does that mean that some to-dos need to be repeated on different dates, one for each format and /or market? Create new to-dos for these, and if they are specific to formats then mention that in the name: “Social media messaging for: "Now available as an ebook"”, or “Hardback confirmed in stock confirmed at US distributor”.

As you add dates to the to-dos, consider whether it might be convenient to have multiple to-dos all on the same day or not. Perhaps you have a set of to-dos to remind you to add or check metadata in Consonance – instead of adding a single to-do, consider adding one for each category of information so you can check off each step. Perhaps you will not have time to check marketing texts,

Maybe you need placeholder to-dos for events, just to remind you that they are happening – publication dates, for example.

Start using the calendar export as early as possible, and get used to looking on your personal or corporate calendar for to-dos. If you use a separate task management or planning system, such as Omnifocus, then it might also be able to read the iCal format and you can see your to-dos in there.

Assign a person to the to-dos. Each person has their own calendar link to see to-dos assigned to them, and if nobody is assigned to a to-do, it might not appear on anyone's calendar.


# When should we use fixed dates instead of relative?

If you need to do something by a particular date, regardless of any changes in contract or publication dates, and you do not need to re-use that to-do on another work, then a fixed date may be the best choice.

Examples include:

1. If you change critical metadata, such as a publication date or a cover design, on a product you have already announced, add a fixed date to-do to remind you to check that retailer websites have updated.
2. If you are due to give an update to a sales agency by a particular date.
3. When a work has been published, so the publication date cannot change any more (although if it is a task )
