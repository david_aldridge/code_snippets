values = [
  ["9781783520770", "*BUS045000,BUS090030,BUS004000", "electronic cash system; alternative currency; banking; finance; Blockchain technology; cryptocurrency; cryptography; peer-to-peer payment network; bitcoin mining; open source software; digital asset"],
  ["9781783524518", "*BIO005000,PER010030,PER015000", "sketch comedy; Cambridge University; Footlights Dramatic Club; comedian, double act; comedy duo; British culture; British Broadcasting Corporation; BBC; British Television Programs; slapstick"],
  ["9781783524914", "*EDU034000,EDU040000,EDU009000", "teachers; classroom interactions; education reform; Program for International Student Assessment; literacy; International Standard Classification of Education; education systems; school accountability "],
  ["9781783525195", "*CKB115000,CKB077000,CKB030000", "recipes; wine recommendations; soups; seasonal cooking; cookbooks; professional chef; home cook; cooking essays; food ingredients  "],
  ["9781783525799", "*BIO004000,BIO026000,MUS035000", "Glam Rock; Slade; Rock and Roll; depression; guitarists; Rock musicians; backing vocalist; English musicians; Wolverhampton; musical groups"],
  ["9781783526383", "*BIO026000,SEL021000,SEL031000", "middle age; handicraft; Cornwall; self discovery; stress management; self-knowledge; overcoming limitations; realization of potential; surfboard construction"],
  ["9781783526420", "*FIC014020,FIC041000,FIC032000", "chivalry; King Edward III; England; knights; Plantagenet; medieval life; Middle Ages; British history; military; 14th century"],
  ["9781783526451", "*HUM007000,HUM008000,HUM005000", "self-esteem; feminism; self-respect; witty; empowerment; rhymes; bravery; women's rights; equality "],
  ["9781783526604", "*BIO022000,BIO015000,HIS058000", "Women's Engineering Society; trailblazers; women's suffrage; labor rights; pioneers; British women; female engineers; Parliament; women's liberation; feminism; Electrical Association for Women"],
  ["9781783527083", "*BIO007000,BIO022000,LIT004120", "bohemianism; 20th century; 1984; political engagement; sexual freedom; English novelist; social injustice; democratic socialism; Big Brother; Thought Police; mind control"],
  ["9781783527229", "*PSY031000,SOC052000,PSY036000", "positive psychology; sociology; media consumption; mental health; contemporary journalism; social media burnout; news coverage; negative messages; newscasts; well-being; positive thinking"],
  ["9781783527489", "*FIC014000,FIC008000,FIC045000", "Edinburgh; factory workers; labor strike; Clydebank; Royal Infirmary; Scotland; throw-away society; family secrets; working class; Scottish; heart-wrenching"],
  ["9781783527878", "*GAM013000,BIO026000,HUM000000", "Nintendo; Sony; Sega; Atari; video gamer; online console gaming; humor; pop culture; Sonic the Hedgehog; Fortnite; Electronic Arts"],
  ["9781783527960", "*PSY004000,PSY013000,PSY008000", "laughter; developmental psychology; cognitive development; infancy; emotional development; neuroscience; child development; cognitive psychology; Baby Laughter Project; science; ultrasounds"],
  ["9781783527977", "*FIC014020,FIC027160,FIC011000", "knights; medieval; English knighthood; Scottish; squire; 14th century; historical; Edward III; chivalry; England; Scotland "],
  ["9781783527991", "*SOC005000,SOC002010,CKB041000", "Irish folklore; recipes; Irish rituals; superstitions; Ceide Fields; Great Famine; tenant farmers; social history; hand-to-mouth farming; potatoes; Irish Seed Savers Association"],
  ["9781783528011", "*PHI046000,BIO009000,BIO022000", "women philosophers; Hypatia; Ban Zhao; Angela Davis; American Black Power Movement; feminist philosophy; Simone de Beauvoir; Hannah Arendt; Mary Wollstonecraft; women's rights; Edith Stein"],
  ["9781783528059", "*SOC065000,PSY016000,HIS000000", "sex history; sexual practices; brothels; gender; sexual shame; stereotypes; sex workers; Whores of Yore; courtship rituals; sexual nature; sexual fetishism "],
  ["9781783528110", "*BIO026000,HIS001020,BIO038000", "Ugandan Civil War; Lord's Resistance Army; religious mysticism; hardships; trauma; brutality; survival; redemption; rebel forces; Joseph Kony; guerrilla group"],
  ["9781783528424", "*REL034020,CKB042000,CRA034000", "winter; Winter Solstice; family recipes; evergreens; seasonal decorations; advent wreaths; carols; celebration; stockings; entertainments; games"],
  ["9781783528431", "*CRA000000,SEL021000,SEL027000", "activism; global issues; empowerment; activist; engage; injustice; debates; collaboration; craft kits; conversation; gentleness"],
  ["9781783528585", "*TEC031010,POL068000,TRA001000", "air pollution; renewable energy; innovations; automotive industry; solar panels; wind turbines; environmental impact; solar power; wind power; global warming; sustainable transport"],
  ["9781783528721", "*BIO022000,HIS026020,BIO026000", "women's rights; Kurdish; Peshmerga; Iranian Revolution; Kurdistan; resilience; courage; first-hand account; equality; freedom fighter"],
  ["9781783528776", "*BIO026000,BIO022000,PSY016000", "pornography; female orgasm; sexual intimacy; self-love; empowerment; BDSM; tantric sex; female sexual pleasure; sex life; sexual awakening; female sexuality"],
  ["9781783528981", "*PSY036000,NAT004000,SEL024000", "birdwatching; anecdotes; nature experiences; Royal Society for the Protection of Birds; wildlife; bird habitats; mental health; anxiety reduction; stress management; calming; obsessive-compulsive disorder"],
  ["9781783529032", "*BUS019000,SEL009000,BUS107000", "fearlessness; curiousity; business problems; creativity; problem solving; practical guide; imagination; creators; dreamers; creative process; essays"],
  ["9781789650594", "*BIO026000,HIS008000,BIO002000", "Great Proletarian Cultural Revolution; communism; 20th Century China; social reform; Sichuan Province; Communist Party; political history; hardship; comradeship; injustice; Communists"],
  ["9781783528004", "*FIC011000,FIC043000", "coming-of-age; overcoming obstacles; courage; Greater Manchester; LGBT; England; Madonna Louise Ciccone; homosexuality; bullying; pop culture"],
  ["9781783528196", "*HUM020000,HUM001000", "cartoons; famous people; celebrities; Bruce Springsteen; Elton John; Kraftwerk; Paul Weller; Lady Gaga; Absolute Radio; musings; illustrations "],
  ["9781783528325", "*GAM013000,GAM012000", "urban legends; video gaming; video game developers; anecdotes; video game console; gaming world; botched game launches; Nintendo; video game myths; PlayStation; Tomb Raider"],
  ["9781783528967", "*FIC027020,FIC044000", "heartbreak; break-up; grief; healing; love story; bittersweet; heart-wrenching; modern romance; heartache; failed relationship, love and loss"],
  ["9781783527410", "*GAM007000", "word puzzle; concentration; nonlinear narrative; murderer; puzzlers; murder weapon; complexity; literary adventure; crimes"],
  ["9781783527632", "*GAM013000", "first-person shooter; platform games; fighting games; Nintendo; arcade; role-playing games; Sega; Japanese gaming culture; Namco; role playing games; Konami"]
]

values_2 = values.map do |isbn, bisac_string, keyword_string|
  bisacs = bisac_string.split(",").map(&:squish).map(&:presence).compact

  [
    isbn,
    bisacs.detect{|c| c.starts_with?("*")}.delete("*").squish,
    bisacs.reject{|c| c.starts_with?("*")},
    keyword_string
  ]
end

values_2.select{|isbn, main_bisac_code, other_codes, keywords| main_bisac_code.blank? || main_bisac_code.size != 9 || other_codes.any?{|c| c.size != 9 }}

codes = values_2.flat_map {|isbn, main_bisac_code, other_codes, keywords| [main_bisac_code] + other_codes}.uniq

valid_bisac_codes = Work::MAIN_BISAC_CODES.map(&:last).map(&:to_s)
invalid_codes = codes - valid_bisac_codes


client = Client.is_unbound

ActiveRecord::Base.transaction do
  values_2.each do |isbn, main_bisac_code, other_codes, keywords|
    work = client.works.joins(:books).find_by(books: {isbn: isbn})
    raise "Error finding #{isbn}" unless work

    attribs = ArrayOfHashesToHashOfArrays.new(
      array_of_hashes: other_codes.map{|code| ONIX::BISACCodeToAttribute.new(subject_code: code).call}
    ).call

    work.assign_attributes(
      {
        main_bisac_code:      main_bisac_code,
        bisac_general_codes:  [],
        bisac_edu_codes:      [],
        bisac_geog_codes:     [],
        bisac_juvenile_codes: [],
        bisac_lang_codes:     [],
        bisac_merch_codes:    [],
        bisac_history_codes:  [],
        keywords:             keywords
      }.merge(attribs)
    )
    puts work.changes
    work.save!
  end
  # raise ActiveRecord::Rollback
end
