module CPG
  class Row
    attr_reader :row, :work

    def initialize(row)
      @row = row
    end

    def call
      return "Could not find #{isbn}" unless book

      work.onix_audiences.destroy_all if audience_codes.any?
      audience_codes.each do |code|
        aud = work.onix_audiences.new(audience_code_value: code)
        byebug unless aud.save
      end
      byebug unless work.update_attributes(work_attributes)
    end

    def book
      @_book ||= client.
      books.
      where(isbn: isbn}
      ).distinct.first
    end

    delegate :client, to: :class

    def self.client
      @_client ||= Client.is_mcgill_queens
    end

    PUB_STATUSES = {
      "active"                     => "04"
      "cancelled"                  => "01"
      "forthcoming"                => "02"
      "inactive"                   => "08"
      "not yet published"          => "02"
      "out of print"               => "07"
      "out of stock indefinitely"  => "06"
      "publication postponed"      => "03"
      "temporarily out of stock"   => nil
      "withdrawn from sale"        => "11"
    }

    PRODUCT_STATUS = {
      "available"                  => ""
      "manufactured on demand"     => ""
      "not available"              => ""
      "not yet available"          => ""
      "replaced by new product"    => ""
      "temporarily unavailable"    => ""
    }

    [
      "Sheet",
      "ISBN",
      "USD",
      "CAD",
      "GBP",
      "PUB DATE",
      "AVAILABILITY",
      "PUB STATUS",
      "PROD AVAILABILITY"
    ].each do |column_name|
      define_method column_name.parameterize.to_sym do
        row.fetch(column_name).presence
      end
    end
  end
end

__END__
reload!
filename = "/Users/david/Documents/Bibliocloud/Clients/McGill Queens/From McGill/metadata/Fall 2020/data correction.xlsx"


spreadsheet = Roo::Spreadsheet.open(filename, extension: :xlsx)
rows        = spreadsheet.sheet(0).parse(headers: true, clean: true)[1..1189] ; 0
objs = rows.map{|row| CPG::Row.new(row) } ; 0


ActiveRecord::Base.transaction do
  objs.each(&:call)
  # raise ActiveRecord::Rollback
end
