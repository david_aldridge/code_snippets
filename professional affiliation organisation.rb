select
  date_trunc('month', created_at),
  count(*),
  count(*) filter (where organisation_id is not null) from professional_affiliations
group by
  date_trunc('month', created_at) order by 1;

select
  case when c.id is null then 'y ' else ' n' end contact,
  coalesce(corporate_name, organisation_name),
  count(*)
from
          professional_affiliations pa
left join contacts c on c.id = pa.organisation_id
group by coalesce(corporate_name, organisation_name), case when c.id is null then 'y ' else ' n' end
order by 2,1;