reload!
s = ScaleElementXyPrice.first
s.price(64,25)
s.price(64,25)[:value].to_f

    def interpolate(z, z_low, z_high, p_low, p_high, z_interpolation)
      case z_interpolation
      when 0 then nil
      when 1 then p_low + (((z - z_low) / (z_high - z_low)) * (p_high - p_low))
      when 2 then p_low
      when 3 then p_high
      when 4 then [p_low,p_high].min
      when 5 then [p_low,p_high].max
      end
    end

interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,0).to_f
interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,1).to_f
interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,2).to_f
interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,3).to_f
interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,4).to_f
interpolate(7.to_d,5.to_d,10.to_d,10.to_d,20.to_d,5).to_f

    def extrapolate(z, z_end, z_penul, p_end, p_penul, z_extrapolation)
      case z_extrapolation
      when 0
      then nil
      when 1
      then interpolate(z, z_end, z_penul, p_end, p_penul, 1)
      when 2
      then p_end
      end
    end

extrapolate(3.to_d,9.to_d,10.to_d,90.to_d,100.to_d,0).to_f
extrapolate(3.to_d,9.to_d,10.to_d,90.to_d,100.to_d,1).to_f
extrapolate(3.to_d,9.to_d,10.to_d,90.to_d,100.to_d,2).to_f

reload!
s = ScaleElementXyPrice.first
s.y_prices_for_x(40)

reload!
s = ScaleElementXyPrice.first
s.price(:units => 50, :pages => 64)
