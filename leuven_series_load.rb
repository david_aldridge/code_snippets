connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1kvFzBRBwUjBNbYXER5enPlEaD7kej4CuDKk6LRHEOw0")
metadata_sheet = sheet.worksheets.first
metadata       = metadata_sheet.list
client         = Client.is_leuven

class LoadSeries
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    work.workcontacts = work_contacts
    work.seriesnames  = series
  end

  private

  def book
    @book ||= client.books.find_by(isbn: isbn)
  end

  def isbn
    row["ISBN"].squish.presence
  end

  def work
    @work ||= book.work
  end

  def title
    row["Title"].squish.presence
  end

  def series_name_text
    row["Series Name"].squish.presence
  end

  def series
    return [] unless series_name_text
    [client.seriesnames.find_by(title_without_prefix: series_name_text)]
  end

  def contributor_names
    [
      row["Contributor 1"].squish.presence,
      row["Contributor 2"].squish.presence,
      row["Contributor 3"].squish.presence,
      row["Contributor 4"].squish.presence,
      row["Contributor 5"].squish.presence,
      row["Contributor 6"].squish.presence
    ]
  end

  def contributors
    contributor_names.map do |person_name|
      contact(person_name)
    end
  end

  def work_contacts
    contributors.each_with_index.map do |contact, idx|
      Workcontact.new(
        client:            client,
        contact:           contact,
        work_contact_role: contributor_roles[idx],
        sequence_number:   idx + 1
      )
    end.select{ |wc| wc.contact }
  end

  def contact(person_name)
    return unless person_name
    contact = FindContactByPersonName.new(client: client, person_name: person_name).call.value
    return contact if contact
    person_name_inverted = Contacts::InvertPersonName.call(person_name)
    contact = Contacts::CreateAuthorService.new(
      client:               client,
      person_name:          person_name,
      person_name_inverted: person_name_inverted
    ).call
  end

  def contributor_roles
    [
      row["Contributor 1 Role"][0..2].presence || "A01",
      row["Contributor 2 Role"][0..2].presence || "A01",
      row["Contributor 3 Role"][0..2].presence || "A01",
      row["Contributor 4 Role"][0..2].presence || "A01",
      row["Contributor 5 Role"][0..2].presence || "A01",
      row["Contributor 6 Role"][0..2].presence || "A01"
    ]
  end

  attr_reader :row, :client
end

ActiveRecord::Base.transaction do
  works_sheet.each do |row|
    LoadWorks.new(row, client).call
  end
end
