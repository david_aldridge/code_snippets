
select
  as_of_date,
  af.attachment_file_name,
  count(*) ,
  max(sa.created_at)
from
  stock_availabilities sa join
  any_files            af on af.id = sa.sourceable_id join
  contacts             c on c.id = sa.stock_reportable_id
where
  as_of_date >= current_date - interval '4 months' and
  af.attachment_file_name != 'OPPTMRP.CSV'
group by
  as_of_date ,
  af.attachment_file_name
order by
  1,4;

  AnyFile.unprocessed.
          order(:id).
          select do |af|
            af.processing_class == "FileImport::NBNI::PriceAndAvailabilityONIX" &&
            af.attachment_file_name.starts_with?("nbni_all190409") #&&
            # !af.attachment_file_name.in?(
            #   %w[
            #   nbni_all1902270600_1.xml
            #   nbni_all1902270600_5.xml
            #   nbni_all1902270600_6.xml
            #   nbni_all1902270600_9.xml
            #   ]
            # )
          end.
          each do |any_file|
    any_file.attachment_object.call
    any_file.processed!
    any_file.processing_errors += any_file.attachment_object.processing_errors
    any_file.save
    SendSystemNotification.new(message: "Ingested NBNI stock report on #{ENV['DOMAIN_NAME']}").call
  end ; 0

