# Create a new follower database.

heroku pg:info DATABASE -a bibliocloud-app
heroku addons:create heroku-postgresql:standard-0 --follow postgresql-clean-67782 -a bibliocloud-app
heroku pg:wait -a bibliocloud-app

# Get the colour of the new database

heroku pg:info -a bibliocloud-app

# Make new database unfollow DATABASE
heroku pg:unfollow HEROKU_POSTGRESQL_BLUE_URL -a bibliocloud-app

# Drop data from new database
Davids-MBP-2:consonance davidaldridge$ heroku pg:psql HEROKU_POSTGRESQL_BLUE_URL -a bibliocloud-app
--> Connecting to postgresql-defined-11138
psql (9.6.13, server 9.6.16)
SSL connection (protocol: TLSv1.2, cipher: ECDHE-RSA-AES256-GCM-SHA384, bits: 256, compression: off)
Type "help" for help.

bibliocloud-app::HEROKU_POSTGRESQL_BLUE=>

This query gives you the large tables
SELECT nspname || '.' || relname AS "relation",
    pg_size_pretty(pg_total_relation_size(C.oid)) AS "total_size"
  FROM pg_class C
  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
    AND C.relkind <> 'i'
    AND nspname !~ '^pg_toast'
  ORDER BY pg_total_relation_size(C.oid) DESC
  LIMIT 20;

truncate table versions;
truncate table digital_asset_content_items;
truncate table export_batch_items;
truncate table stock_availabilities;
truncate table request_logs;
truncate table digital_asset_contents;
truncate table shopify_logs;

# Make backup of new database
heroku pg:backup:capture HEROKU_POSTGRESQL_BLUE_URL -a bibliocloud-app

# List the backups
heroku pg:backups -a bibliocloud-app

# Download
heroku pg:backups:url b2684 --app sushi