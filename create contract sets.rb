client = Client.is_british_library
client.contracts.count
client.contractsets.count

ActiveRecord::Base.transaction do
  client.contracts.each do |contract|
    new_contractset =
      Contractset.create!(
        client_id:        contract.client_id,
        contractset_name: "#{contract} #{contract.id}"
      )
    new_contractset.contracts << contract
  end
end

client.contracts.count
client.contractsets.count
