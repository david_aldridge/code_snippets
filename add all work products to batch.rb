ActiveRecord::Base.transaction do
  Collection.where("name like 'Release%'").each do |collection|
    collection.books = collection.books.flat_map{|b| b.work.books}.uniq
  end
  #raise ActiveRecord::Rollback
end
