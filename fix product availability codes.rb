x =
  {
    "OP" => "51",
    "IP" => "20",
    "TU" => "30",
    "NP" => "10",
    "MD" => "23",
    "TP" => "31",
    "RF" => "43",
    "RU" => "32"
  }

x.each do |old_value,new_value|
  StockAvailability.where(product_availability: old_value).update_all(product_availability: new_value)
end


x.each do |old_value,new_value|
  SupplierProduct.where(product_availability: old_value).update_all(product_availability: new_value)
end
