ActiveRecord::Base.transaction do
  [
    ["vidisha.biswas@zedbooks.net","james.illman@bloomsbury.com"],
    ["moira.ashcroft@zedbooks.net","james.mcknight@bloomsbury.com"],
    ["melanie.scagliarini@zedbooks.net","Melanie.Scagliarini@bloomsbury.com"],
    ["jhaines@zedbooks.net","Tia.Ali@bloomsbury.com"],
    ["hi@vincentvanuffelen.com","Fiona.green@bloomsbury.com"]
  ].each do |email_from, email_to|
  puts email_from
    Client.is_zed.users.find_by(email: email_from).update(email: email_to)
  end
end
