client = Client.is_purdue

client.books.flat_map(&:versions).select{|v| v.event == 'destroy'}
client.books.flat_map(&:versions).size


PaperTrail::Version.where(event: :destroy, item_type: "Book", client_id: client.id).map do |v|
  [
    v.created_at,
    User.find_by(id: v.whodunnit)&.email,
    v.reify.isbn,
    v.reify.title
  ]
end




26 May 2017 08:12:42 UTC, nil, "placeholder", "Calculated Risk: The Supersonic Life and Times of Gus Grissom"
26 May 2017 08:12:42 UTC, nil, "placeholder", "Bibliocloud sample product"
26 May 2017 08:12:43 UTC, nil, "placeholder", "Calculated Risk: The Supersonic Life and Times of Gus Grissom"
25 Oct 2017 14:13:59 UTC, "swegener@purdue.edu", "placeholder", "Of Levinas and Shakespeare"
22 Dec 2017 13:28:31 UTC, "david+purdue@bibliocloud.com", "placeholder", "Methods of IT Project Management"
24 Jan 2018 21:21:08 UTC, "rlcorbin@purdue.edu", "placeholder", "Calculated Risk"
24 Jan 2018 21:21:09 UTC, "rlcorbin@purdue.edu", "placeholder", "Calculated Risk"
24 Jan 2018 21:21:09 UTC, "rlcorbin@purdue.edu", "9781557538291", "Calculated Risk"
31 Jan 2018 14:53:02 UTC, "swegener@purdue.edu", "placeholder", "Rhetorical, Cognitive, and Epistemological Perspectives on Science and Culture"
02 Feb 2018 15:04:51 UTC, "swegener@purdue.edu", "placeholder", "Piercing the Horizon"
27 Feb 2018 15:00:41 UTC, "rlcorbin@purdue.edu", "placeholder", "Philip Roth Studies"
27 Feb 2018 15:03:56 UTC, "rlcorbin@purdue.edu", "9781557538406", "Philip Roth Studies Volume 14"
27 Feb 2018 15:08:27 UTC, "rlcorbin@purdue.edu", "placeholder", "Philip Roth Studies"
16 Mar 2018 17:14:50 UTC, "swegener@purdue.edu", "placeholder", "Veterinary Medical School Admission Requirements (VMSAR)"
16 Mar 2018 17:15:03 UTC, "swegener@purdue.edu", "placeholder", "Veterinary Medical School Admission Requirements (VMSAR)"



Contact.find(83589).versions.sort_by(&:id).map do |v|
  [
    v.created_at,
    User.find_by(id: v.whodunnit)&.email,
    v.reify&.biographical_note
  ]
end



x = Contact.find(83589).versions.sort_by(&:id).map do |v|
  [
    v.object_changes
  ]
end


pp x


