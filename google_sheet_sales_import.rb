class GoogleSheetSalesImport < ActiveRecord::Base

  attr_accessible :client    ,
                  :user      ,
                  :key

  belongs_to :client, :inverse_of => :google_sheet_sales_imports
  belongs_to :user

  validates :client, :presence => true
  validates :user  , :presence => true

  after_initialize do
    if new_record?
      self.result ||= "Not yet run"
    end
  end

  after_create do
    push_products
  end

  def self.nice_name
    "Google Sheet sales import"
  end

  def push_products
    lock
    sheet         = GoogleDriveConn.new.session.spreadsheet_by_key(self.key)
    product_sheet = sheet.worksheet_by_title("Products")
    product_list  = product_sheet.list
    client.books.order(:isbn, :id).each_with_index do |book, i|
      if ISBN.valid? book.isbn
        product_list[i]={"ISBN"         => book.isbn,
                         "Title"        => book.title,
                         "Product form" => book.product_form,
                         "Pub date"     => book.pub_date,
                         "ID"           => book.id}
      end
    end
    product_sheet.save
    self.result = "Imported"
  rescue
    puts "Rescuing ..."
    self.result = "Failed"
  ensure
    puts "Ensuring ..."
    unlock
    self.save
  end
  handle_asynchronously :push_products

  def sales_data
    sheet.worksheet_by_title("Sales").list
  end

  def import_sales
    lock
    sheet         = GoogleDriveConn.new.session.spreadsheet_by_key(self.key)
    sales_sheet = sheet.worksheet_by_title("Sales")
    sales_list  = sales_sheet.list
    book_cache  = {}
    channel_cache = {}
    sales_list.each do |new_sale|
      book    = (book_cache[new_sale["ISBN"]] ||
                 book_cache[new_sale["ISBN"]] = client.books.find_by(:isbn => new_sale["ISBN"])
      channel = (channel_cache[new_sale["Channel"]] ||
                 channel_cache[new_sale["Channel"]] = client.books.find_by(:isbn => new_sale["Channel"])
      book.channels = book

  rescue
    puts "Rescuing ..."
    self.result = "Failed"
  ensure
    puts "Ensuring ..."
    unlock
    self.save
  end
  handle_asynchronously :import_sales

  def lock
    client.company.update_column(:google_sales_sheet_lock, true)
  end

  def unlock
    client.company.update_column(:google_sales_sheet_lock, false)
  end
end

__END__

c = Client.find_by(:webname => "vertebratepublishing")
co = c.company
co.google_sales_sheet_key = "1Jo3WDs-LG2OPSM-0KzA7GSNHOTrdG4gxWXM1HowKsNA"
co.save!

reload!
c = Client.find_by(:webname => "vertebratepublishing")
i = c.google_sheet_sales_imports.new
i.user = c.users.first
i.save



