ActiveRecord::Base.connection.transaction do
  Note.where("lower(note_text) like '%reprint%'").where(:notable_type => "Book").each do |note|
    book = note.notable
    book.reprint_notes = book.reprint_notes.blank? ? note.note_text : book.reprint_notes + "/n" + note.note_text
    book.save!
    note.destroy
  end
  #ActiveRecord::Rollback
end
