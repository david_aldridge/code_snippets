
def number_reader(input)
  return input if input.is_a? Numeric
  string_to_decimal(input) if input.is_a? String
end

def string_to_decimal(string)
  return fraction_to_decimal(string) if string.include? "/"
  string.to_d
end

def fraction_to_decimal(string)
  return mixed_fraction_to_decimal(string) if string.include? " "
  numerator, denominator = string.split("/")
  numerator.to_d / denominator.to_d
end

def mixed_fraction_to_decimal(string)
  number, fraction = string.split(" ")
  decimal = fraction_to_decimal(fraction)
  number.to_d + decimal
end


fractioner("3.4")
fractioner("3")
fractioner(3)
fractioner(3.1)
fractioner(3.7.to_d)
fractioner("1/8")
fractioner("2 1/8")
