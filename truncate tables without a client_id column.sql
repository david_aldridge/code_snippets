do $$
<<first_block>>
declare
  rec record;
begin
  for rec in
    select t.table_name
    from   information_schema.tables t
    where  exists (
      select  null
      from    information_schema.columns c
      where   c.table_catalog = t.table_catalog and
              c.table_schema  = t.table_schema and
              c.table_name    = t.table_name and
              c.column_name   = 'client_id'
      ) and
      t.table_name not like 'DEP%' and
      t.table_type = 'BASE TABLE'
  loop
    execute 'truncate table '||rec.table_name||' cascade' ;
  end loop;
end
first_block $$;
