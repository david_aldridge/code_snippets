explain analyze
select * from (
select w.id,
       (select coalesce(sum(a.advance_value),0)
          from advances a
         where a.contract_id = w.contract_id)  contract_advance_value,
       (select coalesce(sum(r.advance_value),0)
          from rightsadvances r
          join foreignrights  f on f.id = r.foreignright_id
         where f.work_id = w.id) rights_contract_advance_value,
       (select coalesce(sum(p.total_shipment_cost),0)
          from prints p
         where p.work_id = w.id) total_shipment_cost,
        (select coalesce(sum(m.base_currency_amount),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type is null
           join books     b on b.id       = e.book_id
          where b.work_id = w.id and
                e.estimate_type = 'Sales') total_estimated_book_sales_for_pandl,
        (select coalesce(sum(r.receipt_value),0)
           from receipts      r
           join foreignrights f on r.foreignright_id = f.id
          where f.work_id = w.id) total_actual_rights_sales,
        (select coalesce(sum(m.base_currency_amount),0)
           from money m
           join print_bundle_components pbc on pbc.id = m.moneyable_id and
                                               m.moneyable_type = 'PrintBundleComponent' 
           join printer_estimates       pe  on pe.id = m.moneysourceable_id and
                                               m.moneysourceable_type = 'PrinterEstimate' 
           join prints                  p   on pe.print_id = p.id 
           join charge_finance_codes    cfc on cfc.chargeable_type = 'PrintBundleComponent' and
                                               cfc.chargeable_id   = pbc.id 
           join finance_codes           fc  on cfc.finance_code_id = fc.id
          where pe.accepted = 't' and
                fc.code_type is null and
                p.work_id = w.id) estimated_print_bundle_cost,
        (select coalesce(sum(m.base_currency_amount),0)
           from money m
           join print_bundle_components pbc on pbc.id = m.moneyable_id and
                                               m.moneyable_type = 'PrintBundleComponent' 
           join printer_estimates       pe  on pe.id = m.moneysourceable_id and
                                               m.moneysourceable_type = 'PrinterEstimate' 
           join prints                  p   on pe.print_id = p.id 
           join charge_finance_codes    cfc on cfc.chargeable_type = 'PrintBundleComponent' and
                                               cfc.chargeable_id   = pbc.id 
           join finance_codes           fc  on cfc.finance_code_id = fc.id
          where pe.accepted = 't' and
                fc.code_type is null and
                p.work_id = w.id) estimated_print_bundle_cost
from   works w) t
where id = 976;




select  b.id book_id,
        b.work_id,
        (select coalesce(sum(m.base_currency_amount),0)
          from money      m
          join line_items l  on m.moneyable_type       = 'LineItem' and
                                m.moneyable_id         = l.id       and
                                m.moneysourceable_type = 'SpendBudget'
          join orders     o  on o.id             = l.order_id
         where b.id             = o.book_id and
               o.order_type = 'Sales' and
               o.status     = 'Approved' ) confirmed_budget_book_sales_amt,
        (select coalesce(sum(m.base_currency_amount),0)
          from money      m
          join line_items l  on m.moneyable_type       = 'LineItem' and
                                m.moneyable_id         = l.id       and
                                m.moneysourceable_type = 'SpendBudget'
          join orders     o  on o.id             = l.order_id
         where b.id             = o.book_id and
               o.order_type = 'Rights' and
               o.status     = 'Approved' ) confirmed_budget_rights_sales_amt,
        (select coalesce(sum(m.base_currency_amount),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type is null
          where b.id       = e.book_id and
                e.estimate_type = 'Sales') estimated_book_sales_amt,
        (select coalesce(sum(m.quantity),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type is null
          where b.id       = e.book_id and
                e.estimate_type = 'Sales') estimated_book_sales_qty,
        (select coalesce(sum(bp.quantity),0)
           from bookprints bp
          where bp.book_id = b.id) total_planned_print_qty,
        (select coalesce(sum(m.base_currency_amount),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type is null
          where b.id       = e.book_id and
                e.estimate_type = 'Rights') estimated_rights_sales_amt,
        (select coalesce(sum(m.base_currency_amount),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type = 'SpendBudget'
          where b.id       = e.book_id and
                e.estimate_type = 'Sales') budget_book_sales_amt,
        (select coalesce(sum(m.base_currency_amount),0)
           from money     m
           join estimates e on m.moneyable_type = 'Estimate' and
                               m.moneyable_id   = e.id       and
                               m.moneysourceable_type = 'SpendBudget'
          where b.id       = e.book_id and
                e.estimate_type = 'Rights') budget_rights_sales_amt,
        (select coalesce(sum(s.sale_value),0)
           from sales s
          where s.book_id      = b.id  and
                s.invoice_date between b.pub_date
                                   and b.pub_date + interval '1' year and
                b.pub_date is not null) actual_book_sales_amt
from   books b