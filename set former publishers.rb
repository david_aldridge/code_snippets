imprint = Client.is_liverpool.imprints.find_by(code: "VOLT")
former_publisher = Client.is_liverpool.contacts.where(corporate_name: "Voltaire Foundation")

imprint.
  works.
  each do |work|
    work.former_publishers = copyright_holders
  end
