isbns.each_with_object(Hash.new(0)){|i,h| h[i] += 1}.reject{|k,v| v == 1}

pp isbns.sort.map{|i| Lisbn.new(i)}.select(&:valid?).map{|l| l.isbn_with_dash.split("-")[0..2].join("-")}.map{|p| [p, 10**(12 - p.length+2)]}.each_with_object(Hash.new(0)){|i,h| h[i] += 1}.reject{|k,v| v == 1}.map{|k,v| [k[0], k[1], v, k[1] - v]}
