Client.is_pharma.prices.where(:currency_code => ['INR', 'AUD']).destroy_all

Client.is_pharma.update_attributes(:standard_price_types => ["gbp_inctax", "gbp_prepub_inctax", "usd_exctax", "usd_prepub_exctax"])

Client.is_pharma.update_attributes(:standard_price_types => ["gbp_inctax","gbp_exctax", "gbp_prepub_inctax", "usd_exctax", "usd_prepub_exctax"])


Client.is_pharma.update_attributes(:standard_price_qualifiers => ["consumer", "member"])


Client.is_pharma.
       books.
       where("product_form like 'B%'").
       select(&:current_gbp_inctax_consumer_price).
       map(&:current_gbp_inctax_consumer_price).map(&:to_f)

bnf_books = Client.is_pharma.books.joins(:seriesnames).where(:seriesnames => {:code => ['BNF', 'BNFC']})

bnf_books.reject{|b| b.product_form.starts_with? 'D'}.
          each{|b| b.current_gbp_inctax_member_price = (b.current_gbp_inctax_consumer_price*0.5).round(2)}

Client.is_pharma.books.
                    reject{|b| b.id.in? bnf_books.map(&:id)}.
                    reject{|b| b.product_form && b.product_form.starts_with?('D')}.
                    reject{|b| b.full_title ==  "BNF 58 and BNFC 2009 package"}.
                    reject{|b| b.current_gbp_inctax_consumer_price.nil?}.
                    each{|b| b.current_gbp_inctax_member_price = (b.current_gbp_inctax_consumer_price*0.75).round(2)}

Client.is_pharma.books.
                    reject{|b| b.id.in? bnf_books.map(&:id)}.
                    reject{|b| b.product_form && b.product_form.starts_with?('D')}.
                    reject{|b| b.full_title ==  "BNF 58 and BNFC 2009 package"}.
                    reject{|b| b.current_gbp_inctax_consumer_price.nil?}.
                    each{|b| b.current_gbp_prepub_inctax_member_price = (b.current_gbp_inctax_consumer_price*0.70).round(2)}


map(&:full_title).uniq.sort





There is a standard discount for members of 25% on published print books, and 30% on pre-pub print books. There are (of course) exceptions to this rule on an individual basis. BNF and BNFC are 50% off, and MEP has a fixed member price which changes each year.













Price.where(:book => bnf_books).group(:currency_code,:price_type).count

Client.is_pharma.prices.group(:currency_code,:price_type).count


Client.is_pharma.books.joins(:prices).where(:prices => {:currency_code => 'GBP',:price_type => '01'}).group(:product_form).count

Client.is_pharma.books.
                 where("books.product_form like 'B_'").
                 select{|book| book.current_gbp_exctax_consumer_price.presence}.
                 each{|book| book.current_gbp_inctax_consumer_price = book.current_gbp_exctax_consumer_price}


Client.is_pharma.books.
                 where.not("books.product_form like 'B_'").
                 select{|book| book.current_gbp_exctax_consumer_price.presence}.
                 each{|book| book.current_gbp_inctax_consumer_price = (book.current_gbp_exctax_consumer_price*1.2).round(2)}


Client.is_pharma.prices.where(:currency_code => 'GBP',:price_type => '01').destroy_all




Client.is_pharma.books.joins(:prices).where(:prices => {:currency_code => 'USD',:price_type => '02'}).group(:product_form).count

Client.is_pharma.prices.group(:currency_code, :price_type).count

Client.is_pharma.prices.where(:currency_code => 'USD',:price_type => '02').group(:tax_rate_code).count

Client.is_pharma.books.
                 select{|book| book.current_usd_inctax_consumer_price.presence}.
                 each{|book| book.current_usd_exctax_consumer_price = book.current_usd_inctax_consumer_price}

Client.is_pharma.prices.where(:currency_code => 'USD',:price_type => '02').destroy_all


Client.is_pharma.prices.where(:currency_code => 'GBP',:price_type => '01').destroy_all


pp Client.is_pharma.books.joins(:prices).where(:prices => {:currency_code => 'GBP',:price_type => '01'}).map{|b| [b.product_form, b.current_gbp_exctax_consumer_price.to_f, b.current_gbp_inctax_consumer_price.to_f]}


pp Client.is_pharma.books.joins(:prices).where(:prices => {:currency_code => 'GBP',:price_type => '01'}).map{|b| [b.product_form, b.current_gbp_exctax_consumer_price.to_f, b.current_gbp_inctax_consumer_price.to_f]}





Remove all but JFC as contributors on BNF and NNFC
==================================================

bnf_works = Client.is_pharma.works.joins(:seriesnames).where(:seriesnames => {:code => ['BNF', 'BNFC']})


jfc = Client.is_pharma.contacts.find_by(:corporate_name => 'Joint Formulary Committee')
jfc.update_attributes(:corporate_acronym => 'JFC')

ActiveRecord::Base.transaction do
  bnf_works.each do |work|
    work.workcontacts.destroy_all
    work.workcontacts.create(:contact_id => jfc.id,client_id: work.client_id, work_contact_role: 'B01', sequence_number: 1)
    work.books.each{|book| book.workcontacts = work.workcontacts}
  end
end




Set eBook prices to 90% of hardback prices
==========================================

Client.find(404).
Client.is_pharma.
       books.joins(:work).
       is_ebook.
       each do |ebook|

  next unless print_book = ebook.work.first_uncancelled_hard_or_paperback_by_pub_date_hard_preferred

  if print_gbp_price = print_book.try(:current_gbp_inctax_consumer_price)
    ebook.current_gbp_inctax_consumer_price  = (print_gbp_price * 0.9).round(2)
    ebook.current_gbp_inctax_library_price   = print_gbp_price
    ebook.current_gbp_inctax_corporate_price = (print_gbp_price * 0.9).round(2)
  end

  if print_usd_price = print_book.try(:current_usd_exctax_consumer_price)
    ebook.current_usd_exctax_consumer_price  = (print_usd_price * 0.9).round(2)
    ebook.current_usd_exctax_library_price   = print_usd_price
    ebook.current_usd_exctax_corporate_price = (print_usd_price * 0.9).round(2)
  end

end
