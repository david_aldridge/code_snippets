dimensions = {
  "9780091800345" => {product_height_mm: 240, product_width_mm: 160, unit_weight_gr: 560},
  "9780906371169" => {product_height_mm: 282, product_width_mm: 227, unit_weight_gr: 1420},
  "9780906371220" => {product_height_mm: 237, product_width_mm: 157, unit_weight_gr: 1550},
  "9780906371541" => {product_height_mm: 200, product_width_mm: 123, unit_weight_gr: 240},
  "9780954813154" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 270},
  "9780954813161" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 280},
  "9780954813192" => {product_height_mm: 210, product_width_mm: 100, unit_weight_gr: 60},
  "9781898573210" => {product_height_mm: 229, product_width_mm: 150, unit_weight_gr: 500},
  "9781898573227" => {product_height_mm: 230, product_width_mm: 150, unit_weight_gr: 420},
  "9781898573234" => {product_height_mm: 215, product_width_mm: 140, unit_weight_gr: 730},
  "9781898573241" => {product_height_mm: 245, product_width_mm: 170, unit_weight_gr: 920},
  "9781898573265" => {product_height_mm: 235, product_width_mm: 150, unit_weight_gr: 1260},
  "9781898573340" => {product_height_mm: 230, product_width_mm: 165, unit_weight_gr: 540},
  "9781898573432" => {product_height_mm: 228, product_width_mm: 151, unit_weight_gr: 460},
  "9781898573470" => {product_height_mm: 203, product_width_mm: 132, unit_weight_gr: 430},
  "9781898573487" => {product_height_mm: 235, product_width_mm: 185, unit_weight_gr: 620},
  "9781898573500" => {product_height_mm: 226, product_width_mm: 150, unit_weight_gr: 580},
  "9781898573524" => {product_height_mm: 210, product_width_mm: 150, unit_weight_gr: 560},
  "9781898573555" => {product_height_mm: 230, product_width_mm: 150, unit_weight_gr: 600},
  "9781898573562" => {product_height_mm: 195, product_width_mm: 115, unit_weight_gr: 380},
  "9781898573579" => {product_height_mm: 248, product_width_mm: 315, unit_weight_gr: 1540},
  "9781898573586" => {product_height_mm: 230, product_width_mm: 150, unit_weight_gr: 560},
  "9781898573647" => {product_height_mm: 213, product_width_mm: 161, unit_weight_gr: 520},
  "9781898573654" => {product_height_mm: 234, product_width_mm: 155, unit_weight_gr: 870},
  "9781898573661" => {product_height_mm: 226, product_width_mm: 150, unit_weight_gr: 350},
  "9781898573685" => {product_height_mm: 220, product_width_mm: 120, unit_weight_gr: 480},
  "9781898573692" => {product_height_mm: 260, product_width_mm: 225, unit_weight_gr: 1240},
  "9781898573708" => {product_height_mm: 310, product_width_mm: 240, unit_weight_gr: 1800},
  "9781898573715" => {product_height_mm: 235, product_width_mm: 155, unit_weight_gr: 550},
  "9781898573760" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 340},
  "9781898573784" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 840},
  "9781898573852" => {product_height_mm: 233, product_width_mm: 155, unit_weight_gr: 1210},
  "9781898573869" => {product_height_mm: 220, product_width_mm: 120, unit_weight_gr: 400},
  "9781898573876" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 700},
  "9781898573890" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 400},
  "9781898573913" => {product_height_mm: 216, product_width_mm: 137, unit_weight_gr: 360},
  "9781898573920" => {product_height_mm: 198, product_width_mm: 130, unit_weight_gr: 120},
  "9781898573944" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 395},
  "9781898573968" => {product_height_mm: 190, product_width_mm: 260, unit_weight_gr: 390},
  "9781898573982" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 490},
  "9781900623216" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 860},
  "9781906148003" => {product_height_mm: 170, product_width_mm: 170, unit_weight_gr: 240},
  "9781906148010" => {product_height_mm: 220, product_width_mm: 160, unit_weight_gr: 510},
  "9781906148027" => {product_height_mm: 210, product_width_mm: 100, unit_weight_gr: 60},
  "9781906148041" => {product_height_mm: 105, product_width_mm: 146, unit_weight_gr: 140},
  "9781906148058" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 320},
  "9781906148065" => {product_height_mm: 210, product_width_mm: 147, unit_weight_gr: 640},
  "9781906148072" => {product_height_mm: 170, product_width_mm: 170, unit_weight_gr: 280},
  "9781906148089" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 230},
  "9781906148096" => {product_height_mm: 190, product_width_mm: 260, unit_weight_gr: 700},
  "9781906148102" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 275},
  "9781906148126" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 210},
  "9781906148133" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 230},
  "9781906148140" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 240},
  "9781906148157" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148164" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148188" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 280},
  "9781906148195" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 420},
  "9781906148201" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 450},
  "9781906148218" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 240},
  "9781906148225" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148232" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 280},
  "9781906148249" => {product_height_mm: 190, product_width_mm: 190, unit_weight_gr: 520},
  "9781906148256" => {product_height_mm: 0, product_width_mm: 0, unit_weight_gr: 600},
  "9781906148263" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 275},
  "9781906148270" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 820},
  "9781906148287" => {product_height_mm: 243, product_width_mm: 168, unit_weight_gr: 550},
  "9781906148294" => {product_height_mm: 310, product_width_mm: 258, unit_weight_gr: 1650},
  "9781906148300" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 470},
  "9781906148317" => {product_height_mm: 0, product_width_mm: 0, unit_weight_gr: 220},
  "9781906148324" => {product_height_mm: 174, product_width_mm: 120, unit_weight_gr: 210},
  "9781906148331" => {product_height_mm: 241, product_width_mm: 164, unit_weight_gr: 625},
  "9781906148348" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 270},
  "9781906148416" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148423" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 600},
  "9781906148447" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 440},
  "9781906148461" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 450},
  "9781906148478" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 340},
  "9781906148492" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148515" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 690},
  "9781906148522" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 220},
  "9781906148539" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 580},
  "9781906148553" => {product_height_mm: 220, product_width_mm: 160, unit_weight_gr: 620},
  "9781906148560" => {product_height_mm: 189, product_width_mm: 246, unit_weight_gr: 520},
  "9781906148584" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 320},
  "9781906148607" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 680},
  "9781906148621" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 200},
  "9781906148638" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 240},
  "9781906148645" => {product_height_mm: 285, product_width_mm: 260, unit_weight_gr: 1500},
  "9781906148683" => {product_height_mm: 120, product_width_mm: 175, unit_weight_gr: 200},
  "9781906148690" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 390},
  "9781906148720" => {product_height_mm: 310, product_width_mm: 240, unit_weight_gr: 2300},
  "9781906148782" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 165},
  "9781906148812" => {product_height_mm: 225, product_width_mm: 140, unit_weight_gr: 190},
  "9781906148829" => {product_height_mm: 225, product_width_mm: 140, unit_weight_gr: 190},
  "9781906148898" => {product_height_mm: 310, product_width_mm: 240, unit_weight_gr: 1900},
  "9781906148904" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 270},
  "9781906148911" => {product_height_mm: 175, product_width_mm: 118, unit_weight_gr: 220},
  "9781906148928" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 220},
  "9781906148942" => {product_height_mm: 198, product_width_mm: 130, unit_weight_gr: 300},
  "9781906148980" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 620},
  "9781909461017" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 300},
  "9781909461031" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 620},
  "9781909461062" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 280},
  "9781909461109" => {product_height_mm: 260, product_width_mm: 260, unit_weight_gr: 1040},
  "9781909461130" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 215},
  "9781909461147" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 370},
  "9781909461161" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 350},
  "9781909461185" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 360},
  "9781909461208" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 330},
  "9781909461222" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 260},
  "9781909461246" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 210},
  "9781909461260" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 370},
  "9781909461284" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 370},
  "9781909461307" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 430},
  "9781909461321" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 330},
  "9781909461345" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 320},
  "9781909461369" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 300},
  "9781909461383" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 460},
  "9781909461406" => {product_height_mm: 216, product_width_mm: 156, unit_weight_gr: 280},
  "9781909461420" => {product_height_mm: 216, product_width_mm: 158, unit_weight_gr: 350},
  "9781909461444" => {product_height_mm: 216, product_width_mm: 157, unit_weight_gr: 670},
  "9781909461475" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 400},
  "9781909461499" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 360},
  "9781909461512" => {product_height_mm: 254, product_width_mm: 203, unit_weight_gr: 730},
  "9781909461529" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 100},
  "9781909461536" => {product_height_mm: 210, product_width_mm: 130, unit_weight_gr: 270},
  "9781909461543" => {product_height_mm: 246, product_width_mm: 190, unit_weight_gr: 1230},
  "9781909461550" => {product_height_mm: 240, product_width_mm: 170, unit_weight_gr: 740},
  "9781910240007" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 730},
  "9781910240021" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 230},
  "9781910240052" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 210},
  "9781910240076" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 500},
  "9781910240083" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 630},
  "9781910240144" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 190},
  "9781910240182" => {product_height_mm: 240, product_width_mm: 330, unit_weight_gr: 1400},
  "9781910240199" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 680},
  "9781910240335" => {product_height_mm: 148, product_width_mm: 210, unit_weight_gr: 310},
  "9781910240342" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 310},
  "9781910240366" => {product_height_mm: 148, product_width_mm: 210, unit_weight_gr: 490},
  "9781910240373" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 540},
  "9781910240397" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 776},
  "9781910240410" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 920},
  "9781910240434" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 650},
  "9781910240458" => {product_height_mm: 225, product_width_mm: 140, unit_weight_gr: 220},
  "9781910240465" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 210},
  "9781910240519" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 530},
  "9781910240526" => {product_height_mm: 230, product_width_mm: 230, unit_weight_gr: 1860},
  "9781910240533" => {product_height_mm: 246, product_width_mm: 189, unit_weight_gr: 650},
  "9781910240540" => {product_height_mm: 189, product_width_mm: 246, unit_weight_gr: 710},
  "9781910240557" => {product_height_mm: 225, product_width_mm: 140, unit_weight_gr: 220},
  "9781910240564" => {product_height_mm: 225, product_width_mm: 140, unit_weight_gr: 220},
  "9781910240588" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 0},
  "9781910240632" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 200},
  "9781910240656" => {product_height_mm: 250, product_width_mm: 300, unit_weight_gr: 1440},
  "9781910240663" => {product_height_mm: 240, product_width_mm: 165, unit_weight_gr: 770},
  "9781910240694" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 740},
  "9781910240717" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 0},
  "9781910240724" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 170},
  "9781910240731" => {product_height_mm: 210, product_width_mm: 148, unit_weight_gr: 0},
  "9781910240748" => {product_height_mm: 246, product_width_mm: 189, unit_weight_gr: 1330},
  "9781910240755" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 0},
  "9781910240779" => {product_height_mm: 246, product_width_mm: 189, unit_weight_gr: 850},
  "9781910240816" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 460},
  "9781910240830" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 600},
  "9781910240861" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 230},
  "9781910240922" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 390},
  "9781910240946" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 390},
  "9781910240960" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 170},
  "9781910240977" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 220},
  "9781910240984" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 220},
  "9781910240991" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 210},
  "9781911342281" => {product_height_mm: 175, product_width_mm: 120, unit_weight_gr: 190},
  "9781911342311" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 470},
  "9781911342328" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 350},
  "9781911342366" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 210},
  "9781911342380" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 190},
  "9781911342403" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 0},
  "9781911342489" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 330},
  "9781911342502" => {product_height_mm: 219, product_width_mm: 276, unit_weight_gr: 940},
  "9781911342519" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 900},
  "9781911342526" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 570},
  "9781911342533" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 580},
  "9781911342540" => {product_height_mm: 233, product_width_mm: 157, unit_weight_gr: 460},
  "9781911342557" => {product_height_mm: 231, product_width_mm: 156, unit_weight_gr: 640},
  "9781911342564" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 175},
  "9781911342595" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 210},
  "9781911342618" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 120},
  "9781911342632" => {product_height_mm: 198, product_width_mm: 129, unit_weight_gr: 195},
  "9781911342724" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 560},
  "9781911342731" => {product_height_mm: 0, product_width_mm: 0, unit_weight_gr: 560},
  "9781911342748" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 0},
  "9781911342793" => {product_height_mm: 234, product_width_mm: 156, unit_weight_gr: 650},
  "9781911342816" => {product_height_mm: 240, product_width_mm: 330, unit_weight_gr: 0},
  "9781911342878" => {product_height_mm: 285, product_width_mm: 215, unit_weight_gr: 0},
  "9781911342908" => {product_height_mm: 267, product_width_mm: 267, unit_weight_gr: 0}
}

client = Client.is_vertebrate

cantfind = dimensions.keys - client.books.pluck(:isbn)
ActiveRecord::Base.transaction do
  dimensions.each do |isbn, attributes|
    book = client.books.find_by(isbn: isbn)
    raise "Cannot find #{isbn}" unless book
    book.update_attributes(attributes.reject{|_,value| value.zero?})
  end
end





