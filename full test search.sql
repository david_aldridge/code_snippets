class Work
  pg_search_scope :title_search,
    against:           {
      title:              "A",
      subtitle:           "A",
      alternative_titles: "C"
    },
                  using:     { trigram: { threshold: 0.3 } },
                  ranked_by: ":trigram"
end

CREATE INDEX book_full_title_trgm_idx ON books USING GIN (full_title gin_trgm_ops);
drop INDEX book_full_title_trgm_idx;

CREATE INDEX book_full_title_trgm_idx ON books USING gin(to_tsvector('english', full_title));



Work.search_title_only("Adept").map(&:to_s)

pp Book.where("similarity(full_title, ?) >= 0.3", "success").order("similarity(full_title, 'Adept') desc").map(&:to_s)
Book.where("to_tsvector('english', full_title) @@ to_tsvector('english', 'adept'::text)").map(&:to_s)


Work.title_search("Profitable").map{|w| [w.title, w.subtitle.presence].compact.join(": ")}
