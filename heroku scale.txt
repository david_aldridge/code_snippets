heroku ps:scale web=1:Standard-2X -a bibliocloud-monochrome
heroku config:set MIN_THREADS=3 MAX_THREADS=3 -a bibliocloud-monochrome

heroku ps:scale web=1:Performance -a bibliocloud-monochrome
heroku config:set MIN_THREADS=8 MAX_THREADS=8 -a bibliocloud-monochrome
