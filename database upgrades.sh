# Get information on databases
heroku pg:info -a bibliocloud-app
# Create a follower
heroku addons:create heroku-postgresql:standard-0 --follow DATABASE_URL -a bibliocloud-app
time heroku pg:wait -a bibliocloud-app

# Turn system off
heroku maintenance:on -a bibliocloud-app
heroku scale -a bibliocloud-app
heroku scale web=0 worker=0 -a bibliocloud-app

# Upgrade the follower
heroku pg:upgrade HEROKU_POSTGRESQL_OLIVE_URL --version 9.6  -a bibliocloud-app
heroku pg:wait -a bibliocloud-app
heroku pg:info -a bibliocloud-app
#Promote the follower
heroku pg:promote HEROKU_POSTGRESQL_OLIVE_URL -a bibliocloud-app

# Turn the system on
heroku maintenance:off -a bibliocloud-app
heroku scale web=1 worker=2 -a bibliocloud-app
heroku maintenance:off -a bibliocloud-app

# Destroy the old database
heroku addons:destroy HEROKU_POSTGRESQL_IVORY_URL  -a bibliocloud-app







heroku pg:info -a bibliocloud-demo

heroku addons:create heroku-postgresql:hobby-basic --version 9.6 -a bibliocloud-demo

heroku maintenance:on -a bibliocloud-demo
heroku scale -a bibliocloud-demo
heroku scale web=0 worker=0 -a bibliocloud-demo

heroku pg:copy DATABASE_URL HEROKU_POSTGRESQL_GRAY_URL -a bibliocloud-demo
heroku pg:wait -a bibliocloud-demo

heroku pg:promote HEROKU_POSTGRESQL_OLIVE_URL -a bibliocloud-app

heroku addons:destroy HEROKU_POSTGRESQL_GRAY_URL  -a bibliocloud-demo
