class Board
  def initialize(*vals)
    @cells = vals.each_with_index.map do |val, idx|
      Cell.new(self, idx, val)
    end
  end

  def cell_row_indices
    @cell_row_numbers ||= @cells.each_with_object({}) do |cell, hash|
      hash[cell.position] = cell.position / 9
    end
  end

  def cell_column_indices
    @cell_col_numbers ||= @cells.each_with_object({}) do |cell, hash|
      hash[cell.position] = cell.position % 9
    end
  end

  def cell_square_indices
    @cell_col_numbers ||= @cells.each_with_object({}) do |cell, hash|
      hash[cell.position] = begin
        square_column = cell_column_indices[cell.position] % 3
        square_row    = cell_row_indices[cell.position] % 3
        square_number = square_column + (square_row * 3)
        square_number
      end
    end
  end
end

class Cell
  def initialize(board, position, value = nil)
    @board      = board
    @position   = position
    @value      = value
    @test_value = nil
  end

  attr_reader :position, :value, :test_value

  def known?
    !value.nil?
  end
end


board = Board.new(
  5,  3,nil,nil,  7,nil,nil,nil,nil,
  6,nil,nil,  1,  9,  5,nil,nil,nil,
nil,  9,  8,nil,nil,nil,nil,  6,nil,

  8,nil,nil,nil,  6,nil,nil,nil,  3,
  4,nil,nil,  8,nil,  3,nil,nil,  1,
  7,nil,nil,nil,  2,nil,nil,nil,  6,

nil,  6,nil,nil,nil,nil,  2,  8,nil,
nil,nil,nil,  4,  1,  9,nil,nil,  5,
nil,nil,nil,nil,  8,nil,nil,  7,  9
)

board.cell_row_indices
board.cell_column_indices
board.cell_square_indices
