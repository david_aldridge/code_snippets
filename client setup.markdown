# New Client Setup
* Review ONIX
  * Contributors
    * Corporate contributors not correctly coded?
    * Single persons not correctly coded
    * Name variants present
  * Titles
    * Variants
    * Subtitle variants
* Create Client
* Create User
* Import ONIX
* Import cover images (app: bulk importer)
* Publishers and imprint
  * Connect imprints to publishers (app)
  * Set publication countries and cities (app)
  * Set codes (app)
  * Set ISNIs (app)
  * Upload logos (app)
  * Setup AIs (app)
  * Correct duplicate or incorrect imprint/publisher names

  ```
    client.works.where(imprint_id: ?).each do |w|
      w.update_attributes(imprint_id: ??)
    end
  ```
* Works and Products
  * Review product forms
    * Obsolete/bad/vague product forms?
      ```
      client = Client.is_british_library
      require "terminal-table"
      deprecated_forms = OnixCode.product_forms.where("notes ilike '%deprecated%'").pluck(:value)
      vague_forms   = OnixCode.product_forms.where("notes like '%A'").pluck(:value)
      unknown_forms = [nil, "00"]
      products      = client.books.onix_export_allowed.where(product_form: deprecated_forms + vague_forms + unknown_forms)
      puts Terminal::Table.new(
        headings: [
          "ID",
          "Title",
          "Pub Date",
          "ISBN",
          "Form",
          "Notes"
        ],
        rows:     products.map do |b|
          [
            b.id,
            b.title,
            b.pub_date,
            b.isbn,
            b.product_form,
            b.onix_product_form&.notes
          ]
        end
      )

      if products.any?
        c = client.collections.create(name: "Ingestion: Product form issues")
        c.books = products
      end

      ```
    * Unknowns (ePub type?)
      ```
      client = Client.is_british_library
      require "terminal-table"
      products      = client.books.onix_export_allowed.is_ebook.order(:epub_type_code)
      puts Terminal::Table.new(
        headings: [
          "ID",
          "Title",
          "Pub Date",
          "ISBN",
          "EPub type",
          "Notes"
        ],
        rows:     products.map do |b|
          [
            b.id,
            b.title,
            b.pub_date,
            b.isbn,
            b.epub_type_code,
            b.onix_product_form&.notes
          ]
        end
      )


      ```
    * Unusual forms (tax implications? recipient implications?)

      ```
      products = client.books.onix_export_allowed.where.not(product_form: %w(BB BC DG))
      products.group(:product_form).count.transform_keys do |x|
        OnixCode.product_forms.find_by(value: x)&.to_s
      end


      if products.any?
        c = client.collections.create(name: "Ingestion: Unusual product forms")
        c.books = products
      end
      ```
    * Mixed media
      * Check that they really are mixed media

      ```
      require "terminal-table"
      puts Terminal::Table.new(
        headings: ["URL", "Product", "Pub Date", "ISBN"],
        rows:     client.books.onix_export_allowed.where(product_form: "WW").pluck(:id,:title, :pub_date, :isbn)
      )

      ```
      * components are mixed
      * Add product components
      * Check GBP prices for tax
  * Set inferred edition type codes

    ```
    client.books.onix_export_allowed.where("edition_statement ilike '%rev%' or edition > 1").each do |b|
      b.update_attributes(edition_type_codes: ((b.edition_type_codes || []) + ["REV"]).sort.uniq)
    end
    ```
  * Prices
    * What price types have been loaded?

      ```
      x = client.
        prices.
        group(:currency_code, :price_type, :onix_price_type_qualifier_id).
        order("2,3,4").
        count

      require "terminal-table"

      puts Terminal::Table.new(
        headings: [
          "Curr",
          "Type",
          "Qual",
          "Count"
        ],
        rows:     x.map do |b,c|
          [
            b[0],
            b[1],
            b[2],
            c
          ]
        end
      )

      client.prices.where.not(currency_code: "GBP").where.not(price_type: "01").update_all(price_type: "01")


      x = Client.is_british_library.prices.gbp.exctax.map do |price|
        OpenStruct.new(
          value: price.price_amount,
          product_form: price.book.is_ebook?,
          isbn: price.book.isbn,
          title: price.book.title
        )
      end

      puts Terminal::Table.new(
        headings: [
          "Price",
          "eBook?",
          "ISBN",
          "Title"
        ],
        rows:  x.map do |v|
          [v.value,
          v.product_form,
          v.isbn,
          v.title]
        end
      )







      ```
    * Delete currencies not required

      ```
      client.prices.inr.destroy_all
      ```
    * Check on missing prices -- might need loading

      ```
      require "terminal-table"
      products      = client.books.onix_export_allowed.includes(:prices).where(prices: {id: nil})
      puts Terminal::Table.new(
        headings: [
          "ID",
          "Title",
          "Pub Date",
          "ISBN",
          "Pub status",
          "Form"
        ],
        rows:     products.map do |b|
          [
            b.id,
            b.title.first(20),
            b.pub_date,
            b.isbn,
            b.inferred_publishing_status_string,
            b.product_form
          ]
        end
      )

      if products.any?
        c = client.collections.create(name: "Ingestion: Products without a price")
        c.books = products
      end

      ```


    * Modify VAT types (does client want to specify exc- or inc- for GBP?)

      ```
      client.prices.gbp.consumer.exctax.update_all(price_type: "02")
      ```
    * Remove price tax components (except mixed media)

      ```
      client.prices.update_all(
        tax_rate_code: nil,
        tax_rate_percent: nil,
        taxable_amount: nil,
        tax_value: nil,
        tax_rate_code_2: nil,
        tax_rate_percent_2: nil,
        taxable_amount_2: nil,
        tax_value_2: nil
      )
      ```
    * Set client default price types

      ```
      client.update_columns(
        standard_price_types: ["gbp_inctax", "usd_exctax", "eur_exctax"]
      )
      ```
    * Set price territoriality

      ```
      client.client_currencies.create(
        currency_code:   "GBP",
        territory_codes: ["ROW"]
        )
      client.client_currencies.create(
        currency_code: "USD",
        territory_codes: ["US & TERRITORIES"],
        country_codes: ["CA", "MX"]
      )

      client.client_currencies.create(
        currency_code:   "CAD",
        country_codes: ["CA"]
        )
      client.client_currencies.create(
        currency_code: "USD",
        territory_codes: ["ROW"]
      )
      client.client_currencies.create(
        currency_code: "GBP",
        country_codes: ["GI"],
        territory_codes: ["BRITISH ISLES"]
      )

      ```
* Pub status
      ```
      client = Client.is_british_library
      require "terminal-table"
      products      = client.books.onix_export_allowed.select{|b| b.inferred_publishing_status.nil?}
      puts Terminal::Table.new(
        headings: [
          "ID",
          "Title",
          "Pub Date",
          "ISBN",
          "Form"
        ],
        rows:     products.map do |b|
          [
            b.id,
            b.title,
            b.pub_date,
            b.isbn,
            b.product_form
          ]
        end
      )
      if products.any?
        c = client.collections.create(name: "Ingestion: Active products without an ISBN")
        c.books = products
      end
      ```

* No ISBN
      ```
      client = Client.is_british_library
      require "terminal-table"
      products      = client.books.onix_export_allowed.select(&:isbn_invalid?)
      puts Terminal::Table.new(
        headings: [
          "ID",
          "Title",
          "Pub Date",
          "Form"
        ],
        rows:     products.map do |b|
          [
            b.id,
            b.title,
            b.pub_date,
            b.product_form
          ]
        end
      )
      if products.any?
        c = client.collections.create(name: "Ingestion: Products without an ISBN")
        c.books = products
      end
      ```

* Suppliers
  * Create Contacts (including for publisher)
  * Create SupplierConfig
    * Product forms?
    * ePub types?
    * Territoriality?
* Data recipients
  * Create contact
  * Create FTP template
  * Create CDATT
    * Product forms?
    * Identifiers?
    * Prices?
    * Suppliers?
  * Create ONIX rake task
  * Create ONIX build class
* Review Contacts
  * Corporate contributors not correctly coded.
  * Single-names not correctly coded.
  * Duplicate names

    ```
    dupe_keynames = client.contacts.joins(:books).merge(Book.onix_export_allowed).group("lower(unaccent(keynames))").having("count(distinct contacts.id) > 1").count
    suspect_contacts = client.contacts.joins(:books).merge(Book.onix_export_allowed).where("lower(unaccent(keynames)) in (?)", dupe_keynames.keys).order("lower(keynames)")
    require "terminal-table"
    puts Terminal::Table.new(
      headings: ["ID", "Name", "Works", "Pub. texts", "Author texts"],
      rows:     suspect_contacts.includes(:works, :marketingtexts_published, :marketingtexts_authored).to_a.uniq.map do |x|
        [
          x.id,
          x.to_s,
          x.works.size,
          x.marketingtexts_published.size,
          x.marketingtexts_authored.size
        ]
      end
    )

    dupe_keynames = client.contacts.group("lower(unaccent(keynames))").having("count(*) > 1").count
    suspect_contacts = client.contacts.where("lower(unaccent(keynames)) in (?)", dupe_keynames.keys).order("lower(keynames)")

    ```
  * Dodgy names

    ```
    names = client.contacts.joins(:books).merge(Book.onix_export_allowed).where(
      " unaccent(names_before_key) ilike 'dr%'
       or unaccent(names_before_key) ilike 'doct%'
       or unaccent(names_before_key) ilike 'prof%'
       or unaccent(names_before_key) ilike 'mr%'
       or unaccent(names_before_key) ilike '%university%'
       or unaccent(names_before_key) ilike '%college%'
       or unaccent(names_before_key) ilike '%ltd%'
       or unaccent(names_before_key) ilike '%limited%'
       or unaccent(names_before_key) ilike '%gmbh%'
       or names_before_key ilike '% and %'
       or unaccent(names_before_key) ~ '[^A-za-z\\- \\.]'
       or unaccent(keynames) ilike '%university%'
       or unaccent(keynames) ilike '%college%'
       or unaccent(keynames) ilike '%ltd%'
       or unaccent(keynames) ilike '%limited%'
       or unaccent(keynames) ilike '%gmbh%'
       or keynames ilike '% and %'
       or unaccent(keynames) ~ '[^A-za-z\\- \\.]'
      "
    ).uniq.map do |c|
      [
        c.titles_before_names,
        c.names_before_key&.truncate(50),
        c.keyname_prefix,
        c.keynames&.truncate(50),
        c.name_after_keyname,
        c.suffix_after_keyname,
        c.qualification_after_keyname,
        c.titles_after_names
      ]
    end
    require "terminal-table"
    puts Terminal::Table.new(
      headings: ["Title", "Name b4 🔑", "🔑prefix", "🔑 name", "name after 🔑", "suffix", "qual", "title"],
      rows:     names
    )

    ```

* Products with no contributors.

    ```
    c = client.collections.where(name: "No contributor").first_or_create
    c.books = client.books.onix_export_allowed.includes(:bookcontacts).reject(&:has_contributors?)
    if c.books.any?
      puts "http://app.bibliocloud.com/collections/#{c.id}"
    else
      c.destroy
    end
    ```
  * Setup reviewers

    ```
    client.contacts.joins(:marketingtexts_authored).uniq.each do |c|
      c.contact_types = (c.contact_types + ContactType.where(name: "Reviewer")).uniq
    end

    client.contacts.joins(:marketingtexts_published).uniq.each do |c|
      c.contact_types = (c.contact_types + ContactType.review_publishers).uniq
    end

    ```
* Review common formats
    ```
    x = client.books.onix_export_allowed.not_ebook.order(:product_height_mm, :product_width_mm, :product_thickness_mm).group_by do |x|
      [
        (x.product_height_mm.to_i/5).round*5,
        (x.product_width_mm.to_i/5).round*5,
         x.product_thickness_mm.to_i
      ]
    end.transform_values(&:size)

    puts Terminal::Table.new(
      headings: [
        "Height",
        "Width",
        "Thickness",
        "Count"
      ],
      rows:     x.map do |b, c|
        [
          b[0],
          b[1],
          b[2],
          c
        ]
      end
    )

    x = client.books.onix_export_allowed.not_ebook.order(:product_height_mm, :product_width_mm, :product_form).group_by do |x|
      OpenStruct.new(
        product_form: x.product_form,
        height: x.product_height_mm.to_i,
        width: x.product_width_mm.to_i
      )
    end.transform_values(&:size)

    puts Terminal::Table.new(
      headings: [
        "Height",
        "Width",
        "Form",
        "Count"
      ],
      rows:     x.map do |b, c|
        [
          b.height,
          b.width,
          b.product_form,
          c
        ]
      end
    )

    ```
    Create proprietary formats

    ```

    threshold = 10

    formats = client.books.onix_export_allowed.not_ebook.order(:product_height_mm, :product_width_mm, :product_form).group_by do |x|
      OpenStruct.new(
        product_form: x.product_form,
        height: x.product_height_mm.to_i,
        width: x.product_width_mm.to_i
      )
    end.transform_values(&:size)

    formats.select do |format, count|
      format.height.nonzero? && format.width.nonzero? && count >= threshold
    end.each do |format, _count|
      format = client.proprietary_formats.where(
        product_height_mm: format.product_height_mm,
        product_width_mm:  format.product_width_mm,
        product_form:      format.product_form
      ).find_or_initialize

      format.update_attributes()
    end


    ```
    # Bad dimensions
    ```
    require "terminal-table"
    products      = client.books.onix_export_allowed.not_ebook.select do |x|
      x.product_height_mm && x.product_height_mm > 400 ||
      x.product_width_mm && x.product_width_mm > 300
    end
    puts Terminal::Table.new(
      headings: [
        "ID",
        "Title",
        "Pub Date",
        "ISBN",
        "Pub status",
        "Height",
        "Width"
      ],
      rows:     products.map do |b|
        [
          b.id,
          b.title.first(20),
          b.pub_date,
          b.isbn,
          b.inferred_publishing_status_string,
          b.product_height_mm,
          b.product_width_mm
        ]
      end
    )
    ```

    # Not portrait
    ```
    require "terminal-table"
    products      = client.books.onix_export_allowed.not_ebook.select do |x|
      x.product_height_mm && x.product_width_mm && x.product_height_mm <= x.product_width_mm
    end
    puts Terminal::Table.new(
      headings: [
        "ID",
        "Title",
        "Pub Date",
        "ISBN",
        "Pub status",
        "Height",
        "Width"
      ],
      rows:     products.map do |b|
        [
          b.id,
          b.title.first(20),
          b.pub_date,
          b.isbn,
          b.inferred_publishing_status_string,
          b.product_height_mm,
          b.product_width_mm
        ]
      end
    )
    ```
    # Fractional dimensions
    ```
    require "terminal-table"
    products      = client.books.onix_export_allowed.not_ebook.select do |x|
      x.product_height_mm && x.product_height_mm.frac.nonzero? ||
      x.product_width_mm && x.product_width_mm.frac.nonzero? ||
      x.product_thickness_mm && x.product_thickness_mm.frac.nonzero? ||
      x.unit_weight_gr && x.unit_weight_gr.frac.nonzero?
    end
    puts Terminal::Table.new(
      headings: [
        "ID",
        "Title",
        "Pub Date",
        "ISBN",
        "Pub status",
        "Height",
        "Width",
        "Thick",
        "Weight"
      ],
      rows:     products.map do |b|
        [
          b.id,
          b.title.first(20),
          b.pub_date,
          b.isbn,
          b.inferred_publishing_status_string,
          b.product_height_mm,
          b.product_width_mm,
          b.product_thickness_mm,
          b.unit_weight_gr
        ]
      end
    )

    client.books.where.not(product_height_mm: nil).select do |b|
      b.product_height_mm.frac.nonzero?
    end.each do |b|
      b.update_attributes(product_height_mm: b.product_height_mm.ceil)
    end

    client.books.where.not(product_width_mm: nil).select do |b|
      b.product_width_mm.frac.nonzero?
    end.each do |b|
      b.update_attributes(product_width_mm: b.product_width_mm.ceil)
    end

    client.books.where.not(product_thickness_mm: nil).select do |b|
      b.product_thickness_mm.frac.nonzero?
    end.each do |b|
      b.update_attributes(product_thickness_mm: b.product_thickness_mm.ceil)
    end

    client.books.where.not(unit_weight_gr: nil).select do |b|
      b.unit_weight_gr.frac.nonzero?
    end.each do |b|
      b.update_attributes(unit_weight_gr: b.unit_weight_gr.ceil)
    end


    # Page thickness
    ```
    require "terminal-table"
    products      = client.books.onix_export_allowed.not_ebook.select do |x|
      x.pages_arabic && x.product_thickness_mm
    end
    puts Terminal::Table.new(
      headings: [
        "ID",
        "Title",
        "Pub Date",
        "ISBN",
        "Form",
        "Pages",
        "Thick",
        "per 100 pages"
      ],
      rows:     products.map do |b|
        [
          b.id,
          b.title.first(20),
          b.pub_date,
          b.isbn,
          b.pages_arabic,
          b.product_form,
          b.product_thickness_mm,
          (b.product_thickness_mm / b.pages_arabic * 100.0).round(2)
        ]
      end.sort_by(&:last)
    )
    ```

    # No cover image
    ```
    require "terminal-table"
    products      = client.books.onix_export_allowed.reject(&:approved_cover)
    puts Terminal::Table.new(
      headings: [
        "ID",
        "Title",
        "Pub Date",
        "ISBN",
        "Pub status"
      ],
      rows:     products.map do |b|
        [
          b.id,
          b.title.first(20),
          b.pub_date,
          b.isbn,
          b.inferred_publishing_status_string
        ]
      end
    )
    ```

    # Multiple cover images
    ```
    require "terminal-table"
    include ActionView::Helpers::TextHelper
    works      =
      Work.where(id:
      client.works.
        joins(:books, :supportingresources).
        merge(Book.onix_export_allowed).
        group("works.id").
        having("count(distinct supportingresources.id) > 1").
        count("distinct supportingresources.id").
        keys
        )
    puts Terminal::Table.new(
      headings: [
        "Work Title",
        "Edition",
        "Latest Pub Date"
      ],
      rows:     works.map do |w|
        [
          word_wrap(w.title, line_width: 40),
          w.edition,
          w.books.maximum(:pub_date)
        ]
      end,
      style: {all_separators: true}
    )
    ```
* Children audience but no CBMC

   client.works.select{|w| w.audiences.pluck(:audience_code_value).include? "02"}.reject(&:cbmc_interest_level)

    ```
    require "terminal-table"
    include ActionView::Helpers::TextHelper
    works      =
      client.works.select{|w| w.audiences.pluck(:audience_code_value).include? "02"}.reject(&:cbmc_interest_level)
    puts Terminal::Table.new(
      headings: [
        "Work Title",
        "Edition",
        "Latest Pub Date"
      ],
      rows:     works.map do |w|
        [
          word_wrap(w.title, line_width: 40),
          w.edition,
          w.books.maximum(:pub_date)
        ]
      end,
      style: {all_separators: true}
    )
    ```






  * Create ProprietaryFormatDescriptions?
