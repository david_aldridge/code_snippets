def Region
  def self.new(code)
    return World if World.
    return SubnationalRegion.new(code) if SubnationalRegion.includes(code)
    return CustomRegion
    return Country.new(code) if
  end

end

def World
  def self.countries
    Country.codes
  end
end

def SubcountrySet
  def country
    Country.new(code.first(2))
  end

  codes = {
  "US-AL" => "Alaska",
  "US-CO" => "Colorado",
  "US-OH" => "Ohio"
  }

  def country_codes
    codes.keys.group_by{ |k,v| k.first(2) }


    }
  end
end

def SubnationalRegionSet
  def countries
    codes.map(&:country).sort.uniq
  end
end

def SupranationalRegion
  def countries

  end
end

def SupranationalRegionSet
  def countries
    codes.flat_map(&:country).sort.uniq
  end
end

def CustomRegion
end

Territoriality = Struct.new(world, supranational_regions, countries, subnational_countries) do

end

class Territory
  def initialize(country_set_inc: [], subcountry_set_inc: [], country_set_exc: [], subcountry_set_exc: [])
    @country_set_inc    = country_set_inc
    @subcountry_set_inc = subcountry_set_inc
    @country_set_exc    = country_set_exc
    @subcountry_set_exc = subcountry_set_exc
  end

  private

  attr_reader :countries,
              :regions,
              :countries_excluded,
              :regions_excluded
end
