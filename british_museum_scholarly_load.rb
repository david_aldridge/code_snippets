connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1nNi-gEkS1cZ0-R3dBm5gUFI7Y78cu-XGfkJlC1IaU_o")
series_sheet   = sheet.worksheet_by_title("Series").list
works_sheet    = sheet.worksheet_by_title("Works").list
products_sheet = sheet.worksheet_by_title("Products").list
client         = Client.is_british_museum


class LoadProducts
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    book
    work.update_attributes(title: title)
    contract.update_attributes(contract_name: title)
  end

  private

  def isbn
    row["ISBN-13"].squish
  end

  def book
    @book ||= existing_book || new_book
  end

  def existing_book
    @existing_book ||= client.books.find_by(isbn: isbn)
  end

  def new_book
    CreateProductService.new(
      work:              work,
      attributes: {
        isbn:              isbn,
        title:             title,
        product_form:      product_form,
        publishing_status: status
      }
    ).call
  end

  def product_form
    row["Format"].squish[0..1].presence || "00"
  end

  def status
    row["Status"].squish[0..1].presence || "00"
  end

  def pub_date_text
    row["Pub date"].squish.presence
  end

  def pub_date
    return unless pub_date_text
    Date.parse(pub_date_text)
  end

  def work
    @work ||= existing_work || new_work
  end

  def existing_work
    return unless existing_book
    @existing_work ||= book.work
  end

  def new_work
    @new_work ||= CreateWorkService.new(contract: contract, title: title).call
  end

  def contract
    @contract ||= existing_contract || new_contract
  end

  def existing_contract
    return unless existing_work
    @existing_contract ||= existing_work.contract
  end

  def new_contract
    @new_contract ||= CreateContractService.new(client: client, title: title).call
  end

  def title
    row["Select work"].squish.presence
  end

  attr_reader :row, :client
end

ActiveRecord::Base.transaction do
  products_sheet.each do |row|
    LoadProducts.new(row, client).call
  end
  #raise ActiveRecord::Rollback
end














class LoadWorks
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    work.workcontacts = work_contacts
    work.seriesnames  = series
  end

  private

  def book
    @book ||= client.books.find_by(isbn: isbn)
  end

  def isbn
    row["ISBN"].squish.presence
  end

  def work
    @work ||= book.work
  end

  def title
    row["Title"].squish.presence
  end

  def series_name_text
    row["Series Name"].squish.presence
  end

  def series
    return [] unless series_name_text
    [client.seriesnames.find_by(title_without_prefix: series_name_text)]
  end

  def contributor_names
    [
      row["Contributor 1"].squish.presence,
      row["Contributor 2"].squish.presence,
      row["Contributor 3"].squish.presence,
      row["Contributor 4"].squish.presence,
      row["Contributor 5"].squish.presence,
      row["Contributor 6"].squish.presence
    ]
  end

  def contributors
    contributor_names.map do |person_name|
      contact(person_name)
    end
  end

  def work_contacts
    contributors.each_with_index.map do |contact, idx|
      Workcontact.new(
        client:            client,
        contact:           contact,
        work_contact_role: contributor_roles[idx],
        sequence_number:   idx + 1
      )
    end.select{ |wc| wc.contact }
  end

  def contact(person_name)
    return unless person_name
    contact = FindContactByPersonName.new(client: client, person_name: person_name).call.value
    return contact if contact
    person_name_inverted = Contacts::InvertPersonName.call(person_name)
    contact = Contacts::CreateAuthorService.new(
      client:               client,
      person_name:          person_name,
      person_name_inverted: person_name_inverted
    ).call
  end

  def contributor_roles
    [
      row["Contributor 1 Role"][0..2].presence || "A01",
      row["Contributor 2 Role"][0..2].presence || "A01",
      row["Contributor 3 Role"][0..2].presence || "A01",
      row["Contributor 4 Role"][0..2].presence || "A01",
      row["Contributor 5 Role"][0..2].presence || "A01",
      row["Contributor 6 Role"][0..2].presence || "A01"
    ]
  end

  attr_reader :row, :client
end

ActiveRecord::Base.transaction do
  works_sheet.each do |row|
    LoadWorks.new(row, client).call
  end
end
