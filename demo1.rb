class Post < ActiveRecord::Base
  def self.created_before(date)
    where(Post.arel_table[:creation_date].lt(date))
  end

  def self.archive!
    update_all(archived: 1)
  end
end

Post.created_before(Date.today.beginning_of_year).archive!