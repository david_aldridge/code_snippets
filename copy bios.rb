client = Client.is_libri

=========================================================================================================
Set Contact from Work

works_with_1_contact  = client.works.joins(:workcontacts).group("works.id").having("count(*) = 1")
works_with_1_contact_ids = works_with_1_contact.pluck(:id)
contacts_without_bios = client.contacts.where(:biographical_note => nil)
contacts_without_bios.size
contacts_with_work_bios = contacts_without_bios.includes(works: :biographical_notes).each_with_object({}) do |c,h|
  h[c] = c.works.where(id: works_with_1_contact_ids).flat_map(&:biographical_notes).map(&:pull_quote).uniq
end
contacts_with_work_bios.values.map(&:size).group_by(&:itself).transform_values(&:size)

contacts_with_work_bios.select{|contact, bios| bios.size.in?((1..2))}.each do |contact, bios|
  contact.update(biographical_note: bios.sort_by(&:size).last)
end


Work.find(78917).workcontacts_by_sequence_number.map(&:contact).select{|c| c.biographical_note}.map do |c|
  "<b><i>#{c.person_name}</b></i><p>#{c.biographical_note}</p>"
end.join("<br>")

=========================================================================================================
Set Work from Contact

works_without_bio_with_single_contributor_bio = Client.is_libri.works.select{|w| w.marketingtexts.bios.none? }.select{|w| w.contacts.select(&:biographical_note?).size == 1 }

ActiveRecord::Base.transaction do
  works_without_bio_with_single_contributor_bio.each do |work|
    work_bio = work.marketingtexts.bios.create(
      pull_quote: work.contacts.select(&:biographical_note?).first.biographical_note
    )

    work_bio.books = work.books
  end
  # raise ActiveRecord::Rollback
end

