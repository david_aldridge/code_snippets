select
  sc.id,
  coalesce(p.person_name, p.corporate_name) as name,
  c.contract_name
from
  clients cl
  join statement_contracts sc on sc.client_id = cl.id
  join contracts c on c.id = sc.contract_id
  join statements s on s.id = sc.statement_id
  join statement_batch_contacts sbc on sbc.id = s.statement_batch_contact_id
  join contacts p on p.id = sbc.contact_id
where
  cl.id = 356
order by 2,3;