Client.is_boldwood.works.each do |work|
  isbn = work.books.epubs.first&.isbn
  next unless Lisbn.new(isbn).valid?
  work.update(identifying_isbn13: isbn)
end
