
def fully_contain1?(first, second)
  first & second == second
end

def fully_contain2?(first, second)
  return true if second.empty?
  return false if first.empty?

  second_length = second.length
  return false if second_length > first.length

  second_position = 0
  second_item = second[second_position]

  first.each do |item|
    if item == second_item
      second_position += 1

      if second_position >= second_length
        return true
      else
        second_item = second[second_position]
      end
    end
  end

  false
end

require 'set'
ary1 = [3, 6, 18, 34, 50]
ary2 = [3, 18, 34]
sset1 = SortedSet.new(ary1)
sset2 = SortedSet.new(ary2)
set1 = Set.new(ary1)
set2 = Set.new(ary2)

sset1 & sset2 == sset2
(sset2 - sset1).any?
ary1 & ary2 == ary2
(ary2 - ary1).any?
set1 & set2 == set2
(set2 - set1).any?

fully_contain1?(ary1,ary2)
fully_contain2?(ary1,ary2)
fully_contain1?(set1,set2)
fully_contain2?(set1,set2)
fully_contain1?(sset1,sset2)
fully_contain2?(sset1,sset2)








n = 100000
Benchmark.bm(7) do |x|
  x.report("ss0")  { for i in 1..n; sset1 & sset2 == sset2; end }
  x.report("ss1")  { for i in 1..n; (sset2 - sset1).any?; end }
  x.report("ss2")  { for i in 1..n; sset2 <= sset1; end }
  x.report("ss3")  { for i in 1..n; sset2.size < sset1.size && sset2.all? { |o| sset1.include?(o) }; end }

  x.report("a0")   { for i in 1..n; ary1 & ary2 == ary2; end }
  x.report("a1")   { for i in 1..n; (ary2 - ary1).any?; end }
  x.report("s0")   { for i in 1..n; set1 & set2 == set2; end }
  x.report("s1")   { for i in 1..n; (set2 - set1).any?; end }
  x.report("s2")   { for i in 1..n; set2 <= set1; end }
  x.report("s3")   { for i in 1..n; set2.size < set1.size && set2.all? { |o| set1.include?(o) }; end }
end
