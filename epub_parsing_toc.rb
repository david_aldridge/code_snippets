pf = Client.is_pharma.production_files.epubs.last


temp_filename = File.join(Rails.root, 'tmp', pf.attachment.original_filename)
pf.attachment.copy_to_local_file(:original, temp_filename)

e = Epub.new(epub_file: temp_filename)
e.version
pp e.toc


x = pf.work.marketingtexts.table_of_contents.create(
  pull_quote: e.toc,
  main_type:   "05",
  text_type:   "04",
  legacy_code: "04"
)


class Epub
  def initialize(epub_file:)
    @epub_file = epub_file
  end

  def toc?
    xhtml_table_of_contents.present?
  end

  def toc
    xhtml_table_of_contents.to_xhtml(:indent => 2, :encoding => 'UTF-8')
  end

  def files
    unzipped_file.entries.map(&:name)
  end

  def version
    package_element&.attribute("version")&.value&.to_f
  end

  def unique_identifier
    package_element&.attribute("unique-identifier")&.value
  end

  def identifiers
    package_element&.xpath("metadata/identifier")&.map(&:text)
  end

  def titles
    package_element&.xpath("metadata/title")&.map(&:text)
  end

  def languages
    package_element&.xpath("metadata/language")&.map(&:text)
  end

  private

  attr_reader :epub_file

  def unzipped_file
    @unzipped_file ||= Zip::File.open(epub_file)
  end

  def container_file_present?
    unzipped_file.include?("META-INF/container.xml")
  end

  def container_entry
    container_file_present? && unzipped_file.get_entry("META-INF/container.xml")
  end

  def container_doc
    container_entry && Nokogiri::HTML(container_entry.get_input_stream.read)
  end

  def content_file_full_path
    container_doc && container_doc.at_xpath("//rootfile").attribute("full-path").value
  end

  def content_file_entry
    content_file_full_path && unzipped_file.get_entry(content_file_full_path)
  end

  def content_doc
    content_file_entry && Nokogiri::HTML(content_file_entry.get_input_stream.read)
  end

  def package_element
    content_doc &&
    content_doc.at_xpath("html/body/package")
  end

  def nav_file_name
    package_element &&
    package_element&.at_xpath("manifest/item[@properties='nav']")&.attribute("href")&.text
  end

  def navigation_document
    unzipped_file&.get_entry(nav_file_name)
  end

  def navigation_doc
    return unless navigation_document
    @navigation_doc ||= Nokogiri::HTML(navigation_document.get_input_stream.read)
  end

  def xhtml_table_of_contents
    return unless navigation_doc
    @xhtml_table_of_contents ||= (
      navigation_doc.xpath('//@class').remove
      navigation_doc.xpath('//@id').remove

      navigation_doc.xpath("//li").each do |li|
        a = li.at_xpath("a")
        next unless a
        li.prepend_child a.text
        a.remove
      end

      navigation_doc.xpath("//ol").each do |ol|
        ul = navigation_doc.create_element "ul"
        ul.children = ol.children
        ol.replace ul
      end

      navigation_doc.at_xpath("//ul")
    )
  end
end

e = Epub.new(epub_file: "/Users/david/Documents/Bibliocloud/Clients/Kogan Page/ePub/978-1-911390-49-7 ExemplarProduct_EPUB.epub")

pp e.toc

#  File.open("/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/test1_toc.html", 'w') do |file|
#    file.write(toc_html.at_xpath("//ul").to_html)
#  end

 File.open("/Users/david/Documents/Bibliocloud/Clients/Kogan Page/exemplar_product_toc.html", 'w') do |file|
   file.write(toc_html.at_xpath("//ul").to_html)
 end


  container_file = unzipped_file.glob("META-INF/container.xml").first


container_as_html = Nokogiri::HTML(container_file.get_input_stream.read) ; ""
rootfile = container_as_html.at_xpath("//rootfile").attribute("full-path").value
rootfile_as_html = Nokogiri::HTML(unzipped_file.glob(rootfile).first.get_input_stream.read) ; ""

2.4.2 :080 > pp rootfile_as_html.to_html
"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\" \"http://www.w3.org/TR/REC-html40/loose.dtd\">\n" +
"<?xml version='1.0' encoding='UTF-8'?><html><body>\n" +
"<package xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:container=\"urn:oasis:names:tc:opendocument:xmlns:container\" xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:db=\"http://docbook.org/ns/docbook\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:epub=\"http://www.idpf.org/2007/ops\" xmlns:html=\"http://www.w3.org/1999/xhtml\" xmlns:m=\"http://www.w3.org/1998/Math/MathML\" xmlns:ncx=\"http://www.daisy.org/z3986/2005/ncx/\" xmlns:pub=\"http://publishingxml.org/ns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.idpf.org/2007/opf\" version=\"3.0\" xml:lang=\"en-US\" prefix=\"ibooks: http://vocabulary.itunes.apple.com/rdf/ibooks/vocabulary-extensions-1.0/\" unique-identifier=\"uuid\">\n" +
"\t<metadata>\n" +
"\t\t<title id=\"title1\">Exemplar Product</title>\n" +
"\t\t<identifier id=\"uuid\">e853c509-d7b3-4a6f-89f9-c7e3b9ec827e</identifier><language>en-US</language><meta property=\"dcterms:modified\">2018-01-11T18:42:46Z<meta name=\"cover\" content=\"cover_Ebook_cover_RGB_jpg\">\n" +
"<meta property=\"ibooks:specified-fonts\">true<rights>All rights reserved.</rights></metadata>\n" +
"\n" +
"\t<manifest>\n" +
"\t<item id=\"nav_xhtml\" href=\"nav.xhtml\" media-type=\"application/xhtml+xml\" properties=\"nav\"></item>\n" +
"\t<item id=\"nav_ncx\" href=\"nav.ncx\" media-type=\"application/x-dtbncx+xml\"></item>\n" +
