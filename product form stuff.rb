
class ProductFormat
  def initialize(form, form_details = [], form_description = nil, packaging = [], content_type = [])
    self.form             = form
    self.form_details     = form_details
    self.form_description = form_description
    self.packaging        = packaging
    self.content_type     = content_type
  end

  attr_accessor :form, :form_details, :form_description, :packaging, :content_type

  def class
    @class ||= ProductFormatClass.call(self)
  end

  def measurement_types
    @measurement_types ||= ProductFormatMeasurements.call(self)
  end

  def physical?
    @physical ||= ProductIsPhysical.call(self)
  end
end

class ProductForm
  def self.call(product_form_string)
    new(product_form_string).call
  end

  def initialize(product_form_string)
    @product_form_string = product_form_string
  end

  def class
    @class ||= ProductFormatClass.call(product_form_string)
  end

  def measurement_types
    @measurement_types ||= ProductFormatMeasurements.call(self)
  end
  private

  attr_reader :product_form_string
end

module ProductFormatClass
  CLASSES = {
    "A" => :audio,
    "B" => :book,
    "C" => :cartographic,
    "D" => :digital,
    "E" => :electronic,
    "P" => :print,
    "W" => :mixed,
    "V" => :video,
    "X" => :trade
  }

  def self.call(product_format)
    return :unknown if product_format.nil?
    return :unknown if product_format.form.nil?
    CLASSES[product_format.form.first] || :unknown
  end
end

class ProductFormatMeasurements
  def self.call(product_format)
    new(product_format).call
  end

  def initialize(product_format)
    @product_format = product_format
  end

  def call
    [
      (:height      if boxy?),
      (:width       if boxy?),
      (:thickness   if boxy?),
      (:page_trim_height if book?),
      (:page_trim_width  if book?),
      (:unit_weight if weight?),
      (:duration    if duration?),
      (:filesize    if filesize?)
    ].compact
  end

  private

  attr_reader :product_format

  def boxy?
    product_format.class.in? %i(book mixed audio print video trade)
  end

  def book?
    product_format.class.in? %i(book mixed)
  end

  def weight?
    product_format.physical?
  end

  def thickness?
    product_format.class.in? %i(book mixed audio print video trade)
  end

  def duration?
    product_format.class.in? %i(audio video)
  end

  def filesize?
    product_format.class.in? %i(audio electronic digital)
  end
end

module ProductIsPhysical
  PHYSICAL_CLASSES = %i(
    audio
    book
    cartographic
    digital
    license
    microform
    multiitem_retail
    print
    mixed
    video
    trade
    general
  ).freeze

  DIGITAL_PRODUCT_FORM_EXCEPTIONS = %w(AJ LB)

  def self.call(product_format)
    class = product_format.class
    form  = product_format.form
    class.in?(PHYSICAL_CLASSES) && !form.in?(DIGITAL_PRODUCT_FORM_EXCEPTIONS)
  end
end

ProductFormat.new('BB').class
ProductFormat.new('BB').measurement_types

ProductFormat.new('DG').class
ProductFormat.new('DG').measurement_types

ProductFormat.new('VX').class
ProductFormat.new('VX').measurement_types

ProductFormat.new('XM').class
ProductFormat.new('XM').measurement_types




ProductFormat.new("XM").measurement_types



class EditionTypeCode
  include Comparable
  def initialize(string)
    if valid?(string)
      @code = string
    else
      raise ArgumentError, "edition type code is not valid"
    end
    freeze
  end

  delegate :to_s, :<=>, to: :code

  protected

  attr_reader :code

  def valid?(string)
    string =~ /\A[A-Z]{3}\z/
  end
end

x = EditionTypeCode.new('ABC')
y = EditionTypeCode.new('ABC')
