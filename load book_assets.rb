Rails.logger.level=2

client = Client.is_taylor_francis

template_ids = [
  client.contact_digital_asset_transfer_templates.first.id,
  client.contact_digital_asset_transfer_templates.last.id
]

book_ids = client.books.where("id > ?", Book::Asset.maximum(:book_id)).order(:id).limit(200_000).pluck(:id) ; 0

book_ids.in_groups_of(3000, false).each do |book_id_group|
  DelayedJob.import(
      book_id_group.product(template_ids).map do |book_id, asset_template_id|
        DelayedJob.new(
        priority: 0,
        attempts: 0,
        handler: "--- !ruby/object:UpdateBookAssetJob\nbook_id: #{book_id}\nasset_template_id: #{asset_template_id}\n",
        last_error: nil,
        run_at: Time.now,
        locked_at: nil,
        failed_at: nil,
        locked_by: nil,
        created_at: Time.now,
        updated_at: Time.now,
        queue: "default",
        job_object_id: nil,
        client_id: nil,
        user_id: nil
      )
    end
  )
end ; 0

insert into delayed_jobs(
  priority,
  attempts,
  handler,
  last_error,
  run_at,
  locked_at,
  failed_at,
  locked_by,
  created_at,
  updated_at,
  queue,
  job_object_id,
  client_id,
  user_id
)
select
  0 as priority,
  0 as attempts,
  '--- !ruby/object:UpdateBookAssetJob'
    || E'\n'
    || 'book_id: '
    || book_id
    || E'\n'
    || 'asset_template_id: '
    || asset_template_id
    || E'\n'
    as handler,
  null           as last_error,
  localtimestamp as run_at,
  null           as locked_at,
  null           as failed_at,
  null           as locked_by,
  localtimestamp   as created_at,
  localtimestamp   as updated_at,
  'default'      as queue,
  null           as job_object_id,
  null           as client_id,
  null           as user_id
from
  book_assets
limit 10;



ActiveRecord::Base.transaction do
  book_ids.product(template_ids).each do |book_id, asset_template_id|
    Delayed::Job.enqueue UpdateBookAssetJob.new(book_id: book_id, asset_template_id: asset_template_id)
  end ; 0
end ; 0


DelayedJob.count
DelayedJob.where.not(last_error: nil).count

Book::Asset.select{|a| a.asset_errors.any? }.map(&:asset_errors)




aws s3 cp s3://consonance-tf/2/product_assets/ . --recursive


client = Client.is_taylor_francis
Book::Asset.find_or_initialize_by(
  book_id:           client.books.first.id,
  asset_template_id: client.contact_digital_asset_transfer_templates.last.id
).process.save

client = Client.is_taylor_francis
Book::Asset.find_or_initialize_by(
  book_id:           client.books.first.id,
  asset_template_id: client.contact_digital_asset_transfer_templates.first.id
).process.save
