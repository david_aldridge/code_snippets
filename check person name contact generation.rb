Contact.joins(:client).merge(Client.active).people.
  select{|c| c.person_name != c.person_name_constructed }.
  map{|c| [c.id, c.keynames, c.person_name, c.person_name_constructed, c.person_name_inverted] }

Client.is_bds.contacts.people.
  select do |c|
    c.person_name != c.person_name_constructed ||
    c.person_name_inverted != c.person_name_inverted_constructed
  end.
  map do |c|
    [
      c.id,
      c.person_name,
      c.person_name_inverted,
      c.names_before_key,
      c.keynames,
      c.send(:person_name_part_1),
      c.send(:person_name_part_2),
      c.person_name_constructed,
      c.person_name_inverted_constructed
    ]
  end

Contact.all.
  select{|c| c.person_name_in != c.person_name_constructed }.
  map{|c| [c.id, c.person_name, c.person_name_constructed] }

