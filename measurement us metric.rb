client = Client.is_iop
client.metric_units_only!

client.proprietary_format_description.each do |pfd|
  pfd.product_height_in    = pfd.product_height_in_value    if pfd.product_height_in.nil?    && pfd.product_height_in_value
  pfd.product_width_in     = pfd.product_width_in_value     if pfd.product_width_in.nil?     && pfd.product_width_in_value
  pfd.product_thickness_in = pfd.product_thickness_in_value if pfd.product_thickness_in.nil? && pfd.product_thickness_in_value
  pfd.unit_weight_oz       = pfd.unit_weight_oz_value       if pfd.unit_weight_oz.nil?       && pfd.unit_weight_oz_value

  pfd.page_trim_height_in  = pfd.page_trim_height_in_value  if pfd.page_trim_height_in.nil?  && pfd.page_trim_height_in_value
  pfd.page_trim_width_in   = pfd.page_trim_width_in_value   if pfd.page_trim_width_in.nil?   && pfd.page_trim_width_in_value

  pfd.product_height_mm    = pfd.product_height_mm_value    if pfd.product_height_mm.nil?    && pfd.product_height_mm_value
  pfd.product_width_mm     = pfd.product_width_mm_value     if pfd.product_width_mm.nil?     && pfd.product_width_mm_value
  pfd.product_thickness_mm = pfd.product_thickness_mm_value if pfd.product_thickness_mm.nil? && pfd.product_thickness_mm_value
  pfd.unit_weight_gr       = pfd.unit_weight_gr_value       if pfd.unit_weight_gr.nil?       && pfd.unit_weight_gr_value

  pfd.page_trim_height_mm  = pfd.page_trim_height_mm_value  if pfd.page_trim_height_mm.nil?  && pfd.page_trim_height_mm_value
  pfd.page_trim_width_mm   = pfd.page_trim_width_mm_value   if pfd.page_trim_width_mm.nil?   && pfd.page_trim_width_mm_value

  unless pdf.metric_units?
    pfd.product_height_mm    = nil
    pfd.product_width_mm     = nil
    pfd.product_thickness_mm = nil
    pfd.unit_weight_gr       = nil
    pfd.page_trim_height_mm  = nil
    pfd.page_trim_width_mm   = nil
  end

  unless pdf.us_units?
    pfd.product_height_in    = nil
    pfd.product_width_in     = nil
    pfd.product_thickness_in = nil
    pfd.unit_weight_oz       = nil
    pfd.page_trim_height_in  = nil
    pfd.page_trim_width_in   = nil
  end

  pfd.save if pfd.changed?
end
