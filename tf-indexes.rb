SELECT $4 AS one
FROM "works"
WHERE LOWER("works"."registrants_internal_reference") = LOWER($1)
        AND "works"."client_id" = $2 LIMIT $3


        create index concurrently index_works_on_registrants_internal_reference on works(registrants_internal_reference);

drop index concurrently index_works_on_registrants_internal_reference;
create index concurrently index_works_on_registrants_internal_reference on works(lower(registrants_internal_reference));
create index concurrently index_works_on_registrants_internal_reference_1 on works(registrants_internal_reference);

create index concurrently index_books_on_pub_date_status on books(pub_date, publishing_status);

create materialized view client_work_product_time_map
as
select
  client_id,
  extract(year from coalesce(pub_date, planned_pub_date)) as year,
  extract(month from coalesce(pub_date, planned_pub_date)) as month,
  count(*) as products,
  count(distinct work_id) as works
from
  books
where
  publishing_status not in ('00', '01', '03')
group by
  client_id,
  extract(year from coalesce(pub_date, planned_pub_date)),
  extract(month from coalesce(pub_date, planned_pub_date));

create index concurrently index_client_work_product_time_map_on_client_id on client_work_product_time_map(client_id);

create index concurrently index_delayed_jobs_hirefire on delayed_jobs(queue, run_at, locked_at);

((run_at <= $3
        AND (locked_at IS NULL
        OR locked_at < $4)
        OR locked_by = $5)
        AND failed_at IS NULL)
        AND "delayed_jobs"."queue" IN ($6, $7)

SELECT "subjectcodes".* FROM "subjectcodes" WHERE "subjectcodes"."client_id" = 2 AND EXISTS (SELECT "works".* FROM "works" INNER JOIN "work_subjectcodes" ON "work_subjectcodes"."id" = "works"."subjectcode_id" WHERE (work_subjectcodes.subjectcode_id = subjectcodes.id)) ORDER BY "subjectcodes"."value" ASC;


(225.1ms)  SELECT "works"."id" FROM "works" INNER JOIN "books" ON "books"."work_id" = "works"."id" WHERE "books"."publishing_status" != $1 AND coalesce("books"."pub_date", "books"."planned_pub_date") >= '2019-01-01' AND "books"."publishing_status" != $2 AND coalesce("books"."pub_date", "books"."planned_pub_date") <= '2019-01-31' ORDER BY (select min(books.pub_date)
2020-01-16T09:44:10.151421+00:00 app[web.1]: from   books
2020-01-16T09:44:10.151423+00:00 app[web.1]: where  books.work_id = works.id
2020-01-16T09:44:10.151424+00:00 app[web.1]: order by 1
2020-01-16T09:44:10.151426+00:00 app[web.1]: limit 1) DESC NULLS LAST, works.title  [["publishing_status", "01"], ["publishing_status", "01"]]
2020-01-16T09:44:10.156330+00:00 app[web.1]: Rendered components/_search_field.html.erb (0.2ms)
2020-01-16T09:44:10.510415+00:00 app[web.1]:  (353.0ms)  SELECT COUNT(*) FROM (SELECT DISTINCT works.*, (select min(books.pub_date)
2020-01-16T09:44:10.510422+00:00 app[web.1]: from   books
2020-01-16T09:44:10.510423+00:00 app[web.1]: where  books.work_id = works.id
2020-01-16T09:44:10.510424+00:00 app[web.1]: order by 1
2020-01-16T09:44:10.510438+00:00 app[web.1]: limit 1) FROM "works" INNER JOIN "books" ON "books"."work_id" = "works"."id" WHERE "books"."publishing_status" != $1 AND coalesce("books"."pub_date", "books"."planned_pub_date") >= '2019-01-01' AND "books"."publishing_status" != $2 AND coalesce("books"."pub_date", "books"."planned_pub_date") <= '2019-01-31') subquery_for_count  [["publishing_status", "01"], ["publishing_status", "01"]]
2020-01-16T09:44:10.831100+00:00 app[web.1]: Work Load (317.9ms)  SELECT  DISTINCT works.*, (select min(books.pub_date)
2020-01-16T09:44:10.831105+00:00 app[web.1]: from   books
2020-01-16T09:44:10.831106+00:00 app[web.1]: where  books.work_id = works.id
2020-01-16T09:44:10.831107+00:00 app[web.1]: order by 1
2020-01-16T09:44:10.831109+00:00 app[web.1]: limit 1) FROM "works" INNER JOIN "books" ON "books"."work_id" = "works"."id" WHERE "books"."publishing_status" != $1 AND coalesce("books"."pub_date", "books"."planned_pub_date") >= '2019-01-01' AND "books"."publishing_status" != $2 AND coalesce("books"."pub_date", "books"."planned_pub_date") <= '2019-01-31' ORDER BY (select min(books.pub_date)
2020-01-16T09:44:10.831110+00:00 app[web.1]: from   books
2020-01-16T09:44:10.831111+00:00 app[web.1]: where  books.work_id = works.id
2020-01-16T09:44:10.831113+00:00 app[web.1]: order by 1
2020-01-16T09:44:10.831114+00:00 app[web.1]: limit 1) DESC NULLS LAST, works.title LIMIT $3 OFFSET $4  [["publishing_status", "01"], ["publishing_status", "01"], ["LIMIT", 50], ["OFFSET", 0]]



SELECT  books.* FROM "books" INNER JOIN (SELECT "books"."id" AS pg_search_id, (ts_rank((setweight(to_tsvector('simple', unaccent(coalesce("books"."isbn"::text, ''))), 'A') || setweight(to_tsvector('simple', unaccent(coalesce("books"."full_title"::text, ''))), 'B') || setweight(to_tsvector('simple', unaccent(coalesce("books"."subtitle"::text, ''))), 'C')), (to_tsquery('simple', ''' ' || unaccent('phosphorus') || ' ''' || ':*')), 0)) + 0.5 * (similarity(unaccent('phosphorus'), (unaccent(coalesce("books"."full_title"::text, '') || ' ' || coalesce("books"."subtitle"::text, ''))))) AS rank FROM "books" WHERE ((setweight(to_tsvector('simple', unaccent(coalesce("books"."isbn"::text, ''))), 'A') || setweight(to_tsvector('simple', unaccent(coalesce("books"."full_title"::text, ''))), 'B') || setweight(to_tsvector('simple',
 unaccent(coalesce("books"."subtitle"::text, ''))), 'C')) @@ (to_tsquery('simple', ''' ' || unaccent('phosphorus') || ' ''' || ':*'))) OR (similarity(unaccent('phosphorus'), (unaccent(coalesce("books"."full_title"::text, '') || ' ' || coalesce("books"."subtitle"::text, '')))) >= 0.05)) AS pg_search_6e317bcd6839e887739541 ON "books"."id" = pg_search_6e317bcd6839e887739541.pg_search_id ORDER BY pg_search_6e317bcd6839e887739541.rank DESC, books.pub_date desc LIMIT $1 OFFSET $2




create materialized view client_book_epub_type_codes
as
select
  client_id,
  array(select distinct unnest(array_agg(distinct epub_type_code) filter (where product_form = 'DG' and epub_type_code is not null)) order by 1) as codes
from
  books
group by
  client_id;

  MiniSql::Connection.get(ActiveRecord::Base.connection.raw_connection).
        query_single(
          <<~SQL,
            select   codes
            from     client_book_epub_type_codes
            where    client_id = ?
          SQL
          2
        )


create materialized view client_book_epub_type_codes
as
select distinct
  client_id,
  epub_type_code
from
  books
where
  product_form = 'DG' and epub_type_code is not null;

create unique index index_client_book_epub_type_codes on client_book_epub_type_codes (client_id, epub_type_code);

MiniSql::Connection.get(ActiveRecord::Base.connection.raw_connection).query_single("select epub_type_code from client_book_epub_type_codes where client_id = ? order by 1", user.client_id)
          <<~SQL,
            select   codes
            from     client_book_epub_type_codes
            where    client_id = ?
          SQL
          2
)

create materialized view client_in_use_work_values
as
        SELECT works.client_id,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.main_bisac_code) FILTER (WHERE works.main_bisac_code IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.main_bisac_code) FILTER (WHERE works.main_bisac_code IS NOT NULL)))) AS main_bisac_codes,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT substr(works.main_bisac_code::text, 1, 3)) FILTER (WHERE works.main_bisac_code IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT substr(works.main_bisac_code::text, 1, 3)) FILTER (WHERE works.main_bisac_code IS NOT NULL)))) AS main_bisac_code_prefixes,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.main_bic_code) FILTER (WHERE works.main_bic_code IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.main_bic_code) FILTER (WHERE works.main_bic_code IS NOT NULL)))) AS main_bic_codes,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.thema_main_code) FILTER (WHERE works.thema_main_code IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.thema_main_code) FILTER (WHERE works.thema_main_code IS NOT NULL)))) AS main_thema_codes,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.dewey_decimal_code) FILTER (WHERE works.dewey_decimal_code IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.dewey_decimal_code) FILTER (WHERE works.dewey_decimal_code IS NOT NULL)))) AS dewey_decimal_codes,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.dewey_decimal_edition) FILTER (WHERE works.dewey_decimal_edition IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.dewey_decimal_edition) FILTER (WHERE works.dewey_decimal_edition IS NOT NULL)))) AS dewey_decimal_editions,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.lc_subject_heading) FILTER (WHERE works.lc_subject_heading IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.lc_subject_heading) FILTER (WHERE works.lc_subject_heading IS NOT NULL)))) AS lc_subject_headings,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.lc_subject_heading_region) FILTER (WHERE works.lc_subject_heading_region IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.lc_subject_heading_region) FILTER (WHERE works.lc_subject_heading_region IS NOT NULL)))) AS lc_subject_heading_regions,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.lc_fiction_genre_heading) FILTER (WHERE works.lc_fiction_genre_heading IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.lc_fiction_genre_heading) FILTER (WHERE works.lc_fiction_genre_heading IS NOT NULL)))) AS lc_fiction_genre_headings,
        ARRAY( SELECT DISTINCT unnest(array_agg(DISTINCT works.lc_childrens_subject_heading) FILTER (WHERE works.lc_childrens_subject_heading IS NOT NULL)) AS unnest
              ORDER BY (unnest(array_agg(DISTINCT works.lc_childrens_subject_heading) FILTER (WHERE works.lc_childrens_subject_heading IS NOT NULL)))) AS lc_childrens_subject_headings,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_subject_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_subject_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_subject_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_subject_codes) > 0), ','::text)))) AS bic_subject_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_geog_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_geog_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_geog_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_geog_codes) > 0), ','::text)))) AS bic_geog_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_language_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_language_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_language_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_language_codes) > 0), ','::text)))) AS bic_language_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_time_period_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_time_period_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_time_period_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_time_period_codes) > 0), ','::text)))) AS bic_time_period_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_edu_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_edu_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_edu_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_edu_codes) > 0), ','::text)))) AS bic_edu_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_age_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_age_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_age_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_age_codes) > 0), ','::text)))) AS bic_age_codes,
        ARRAY( SELECT DISTINCT unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_special_interest_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_special_interest_codes) > 0), ','::text)) AS unnest
              ORDER BY (unnest(string_to_array(string_agg(DISTINCT array_to_string(works.bic_special_interest_codes, ','::text), ','::text) FILTER (WHERE array_ndims(works.bic_special_interest_codes) > 0), ','::text)))) AS bic_special_interest_codes
       FROM works
      GROUP BY works.client_id;


create index index_on_books_publishers_reference on books(client_id, publishers_reference);
create index index_on_books_publishing_status on books(client_id, publishing_status);


select   distinct publishing_status
from     books
where    publishing_status is not null and
        client_id = 2
order by 1;

create index index_on_books_publishing_status on books(client_id, publishing_status);

SELECT  1 AS one FROM "works" WHERE "works"."client_id" = 2 AND "works"."year_of_annual" IS NOT NULL LIMIT 1

create index index_on_works_year_of_annual on works(client_id, year_of_annual);



SELECT  books.*,       books.pub_date
FROM "books" ORDER BY       books.pub_date
DESC NULLS LAST, books.title LIMIT 50 OFFSET 0;

create index index_on_books_pub_date_title on books(pub_date desc nulls last, title);

create materialized view client_check_class_results
as
select
  client_id,
  check_class_id,
  count(*) filter (where outcome = 0) as passed_count,
  count(*) filter (where outcome = 1) as failed_count,
  count(*) filter (where outcome = 2) as blocked_count,
  count(*) filter (where outcome = 3) as suppressed_count,
  count(*) filter (where outcome = 4) as not_applicable_count,
  count(*) count
from
  check_results
group by
client_id,
check_class_id;

create unique index index_on_client_check_class_results_unq on client_check_class_results (client_id, check_class_id);

create index concurrently index_on_check_results_for_summary on check_results (client_id, check_class_id, outcome);

UPDATE "delayed_jobs" SET locked_at = '2020-01-24 14:26:54.977019',
locked_by = 'host:6d38aba3-972f-4541-833c-e378003dbe46 pid:4'
WHERE id IN (
      SELECT  "delayed_jobs"."id"
      FROM "delayed_jobs"
      WHERE ((run_at <= '2020-01-24 14:26:54.976097' AND (locked_at IS NULL OR locked_at < '2020-01-24 10:26:54.976121') OR
      locked_by = 'host:6d38aba3-972f-4541-833c-e378003dbe46 pid:4') AND failed_at IS NULL)
      AND "delayed_jobs"."queue" IN ('default', 'paperclip')
      ORDER BY priority ASC, run_at ASC LIMIT 1 FOR UPDATE)
      RETURNING *