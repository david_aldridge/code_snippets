# tkersey/isbn

def gem_1(isbn)
  isbn = isbn[0..-2]
  isbn.split(//).zip([1,3]*6).inject(0) {|s,n| s += n[0].to_i * n[1]}
end

# zapnap/isbn_validation

def gem_2(isbn)
  isbn_values = isbn.upcase.gsub(/\ |-/, '').split('')
  sum = 0
  isbn_values.each_with_index do |value, index|
    multiplier = (index % 2 == 0) ? 1 : 3
    sum += multiplier * value.to_i
  end
  sum
end

# techwhizbang/faker-isbn

def gem_3(isbn_13)
  isbn_array = Array.new
  isbn_13[0..-2].split(//).each { |digit| isbn_array << digit.to_i }
  dot_product = [isbn_array, [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3]].transpose.inject(0) { |result, pair| result += pair[0]*pair[1]; result }
end

# hakanensari/bookland

WEIGHTS = ([1, 3] * 6).freeze

def gem_4(raw)
  digits = raw.split('')[0...-1]
  sum = digits.map(&:to_i).zip(WEIGHTS).reduce(0) { |a, (i, j)| a + i * j }
end


# EmmanuelOga/risbn

def gem_5(isbn)
  digits = isbn[0..-2].split("")
  sum = 0
  digits.each_with_index do |value, index|
    multiplier = (index % 2 == 0) ? 1 : 3
    sum += multiplier * value.to_i
  end
  sum
end

# tylerjohnst/bookworm

def gem_6(identifier)
  identifier, sum = identifier.rjust(13,"978")[/(.+)\w/,1], 0

  12.times do |index|
    digit = identifier[index].to_i
    sum += index.even? ? digit : digit * 3
  end
  sum
end


# asaaki/sjekksum

def gem_7(isbn)
  digits = isbn.to_s.scan(/\d/).map{ |b| b.chr.to_i }

  sum    = digits.map.with_index do |digit, idx|
    idx.odd? ? (digit * 3) : digit
  end.reduce(&:+)
end

def method0(isbn)
  base = (isbn.length == 13 ? '' : '978') + isbn[0..-2]

  products = base.each_char.each_with_index.map do |chr, i|
    chr.to_i * (i % 2 == 0 ? 1 : 3)
  end

  remainder = products.inject(0) {|m, v| m + v}
end

def method1(isbn)
  nums = isbn.chars.map(&:to_i)
  sum = 3 * (nums[1] + nums[3] + nums[5] + nums[7] + nums[9] + nums[11]) +
        nums[0] + nums[2] + nums[4] + nums[6] + nums[8] + nums[10] + nums[12]
end

def method2(isbn)
  nums = isbn.chars
  sum = 3 * (nums[1].to_i + nums[3].to_i + nums[5].to_i + nums[7].to_i + nums[9].to_i + nums[11].to_i) +
        nums[0].to_i + nums[2].to_i + nums[4].to_i + nums[6].to_i + nums[8].to_i + nums[10].to_i + nums[12].to_i
end

def method7(isbn)
(
  isbn[1].to_i +
  isbn[3].to_i +
  isbn[5].to_i +
  isbn[7].to_i +
  isbn[9].to_i +
  isbn[11].to_i
) * 3 +
isbn[0].to_i +
isbn[2].to_i +
isbn[4].to_i +
isbn[6].to_i +
isbn[8].to_i +
isbn[10].to_i
end

def method3(isbn)
  nums = isbn.chars
  sum = nums.map!.with_index{|x,i| i.odd? ? x.to_i*3 : x.to_i }.reduce(&:+)
end

def method4(isbn)
  nums = isbn.chars
  sum = nums.map.with_index{|x,i| i.odd? ? x.to_i*3 : x.to_i }.reduce(&:+)
end

def method5(isbn)
  sum = isbn.each_char.with_index.inject(0){|sum, (x,i)| sum + (i.odd? ? x.to_i*3 : x.to_i )}
end

def method6(isbn)
  sum=0
  (0..isbn.size-1).each_with_index{|n,i| sum += isbn[n].to_i * (i % 2 == 0 ? 1 : 3) }
end

def method8(isbn)
  digits = isbn.unpack("CCCCCCCCCCCC")
  sum = (
          digits[1] +
          digits[3] +
          digits[5] +
          digits[7] +
          digits[9] +
          digits[11]
        ) * 3 +
        digits[0] +
        digits[2] +
        digits[4] +
        digits[6] +
        digits[8] +
        digits[10] -
        1_152
end


def method9(isbn)
(
  isbn[1].ord +
  isbn[3].ord +
  isbn[5].ord +
  isbn[7].ord +
  isbn[9].ord +
  isbn[11].ord
) * 3 +
isbn[0].ord +
isbn[2].ord +
isbn[4].ord +
isbn[6].ord +
isbn[8].ord +
isbn[10].ord -
1_152
end


def method10(isbn)
(
  isbn.getbyte(1) +
  isbn.getbyte(3) +
  isbn.getbyte(5) +
  isbn.getbyte(7) +
  isbn.getbyte(9) +
  isbn.getbyte(11)
) * 3 +
isbn.getbyte(0) +
isbn.getbyte(2) +
isbn.getbyte(4) +
isbn.getbyte(6) +
isbn.getbyte(8) +
isbn.getbyte(10) -
1_152
end

def method11(isbn)
  odds = isbn.getbyte(1) +
        isbn.getbyte(3) +
        isbn.getbyte(5) +
        isbn.getbyte(7) +
        isbn.getbyte(9) +
        isbn.getbyte(11)
  sum = odds + odds + odds +
        isbn.getbyte(0) +
        isbn.getbyte(2) +
        isbn.getbyte(4) +
        isbn.getbyte(6) +
        isbn.getbyte(8) +
        isbn.getbyte(10) -
        1_152
end

isbn = "9780306406150"

method0(isbn)
method1(isbn)
method2(isbn)
method3(isbn)
method4(isbn)
method5(isbn)
method7(isbn)
gem_1(isbn)
gem_2(isbn)
gem_3(isbn)
gem_4(isbn)
gem_5(isbn)
gem_6(isbn)
gem_7(isbn)
Checksum.call(isbn)



require 'benchmark'

n = 100000
Benchmark.bm(7) do |x|
  x.report("method 0")   { for i in 1..n; method0(isbn); end }
  x.report("method 1")   { for i in 1..n; method1(isbn); end }
  x.report("method 2")   { for i in 1..n; method2(isbn); end }
  x.report("method 3")   { for i in 1..n; method3(isbn); end }
  x.report("method 4")   { for i in 1..n; method4(isbn); end }
  x.report("method 5")   { for i in 1..n; method5(isbn); end }
  x.report("method 6")   { for i in 1..n; method6(isbn); end }
  x.report("method 7")   { for i in 1..n; method7(isbn); end }
  x.report("method 8")   { for i in 1..n; method8(isbn); end }
  x.report("method 9")   { for i in 1..n; method9(isbn); end }
  x.report("method 10")   { for i in 1..n; method10(isbn); end }
  x.report("method 11")   { for i in 1..n; method11(isbn); end }
  x.report("gem 1")   { for i in 1..n; gem_1(isbn); end }
  x.report("gem 2")   { for i in 1..n; gem_2(isbn); end }
  x.report("gem 3")   { for i in 1..n; gem_3(isbn); end }
  x.report("gem 4")   { for i in 1..n; gem_4(isbn); end }
  x.report("gem 5")   { for i in 1..n; gem_5(isbn); end }
  x.report("gem 6")   { for i in 1..n; gem_6(isbn); end }
  x.report("gem 7")   { for i in 1..n; gem_7(isbn); end }
  x.report("Rust"){ for i in 1..n; Checksum.call(isbn); end }
end ; ""
