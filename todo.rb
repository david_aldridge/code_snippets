Reminder::Override.destroy_all
Reminder::Template.destroy_all

print_it = Reminder::Template.create(
  client_id:       Client.is_snowbooks.id,
  notifiable_type: "Book",
  message:         "Print it!",
  months_prior_to: 1,
  days_prior_to:   7,
  date_object:     "pub_date",
  start_date:      Date.today
  )


tweet_it = Reminder::Template.create(
  client_id:       Client.is_snowbooks.id,
  notifiable_type: "Book",
  message:         "Tweet about it!",
  months_prior_to: 0,
  days_prior_to:   0,
  date_object:     "pub_date",
  start_date:      Date.today
)

create or replace view all_reminders as
with template_reminders as (
  select
    t.client_id,
    t.id reminder_template_id,
    t.notifiable_type,
    b.id notifiable_id,
    t.message,
    t.months_prior_to,
    t.days_prior_to,
    case t.date_object
      when 'pub_date' then b.pub_date
    end date_value,
    make_interval(0, months_prior_to, 0, days_prior_to,0,0,0),
    start_date
  from
    reminder_templates t join
    books              b on (b.client_id = t.client_id)
  where
    case t.date_object
      when 'pub_date' then b.pub_date
    end is not null
)
select
  tr.client_id,
  tr.notifiable_type,
  tr.notifiable_id,
  coalesce(message_override, message) message,
  coalesce(
    date_override,
    date_value - make_interval(
      0,
      coalesce(months_prior_to_override, months_prior_to),
      0,
      coalesce(days_prior_to_override, days_prior_to),
      0,
      0,
      0
    )
  ) date,
  coalesce(status_override, 0) status
from
  template_reminders tr left join
  reminder_overrides ro on (
    tr.reminder_template_id = ro.reminder_template_id and
    tr.notifiable_type      = ro.notifiable_type and
    tr.notifiable_id        = ro.notifiable_id
    )
/* where
  ro.id is null or
  date_override is not null or
  date_value - make_interval(
      0,
      coalesce(months_prior_to_override, months_prior_to),
      0,
      coalesce(days_prior_to, days_prior_to),
      0,
      0,
      0) >= start_date*/
union all
select  client_id,
        notifiable_type,
        notifiable_id,
        message,
        date,
        status
from    reminders
order by notifiable_id
;


print_override = Reminder::Override.create(
  client_id:                Client.is_snowbooks.id,
  reminder_template_id:     print_it.id,
  notifiable_type:          "Book",
  notifiable_id:            3384,
  months_prior_to_override: 2,
  days_prior_to_override:   7
  )

print_override.update(
  message_override:         "Print it now!",
  date_override:      Date.today
  )



select * from all_reminders where notifiable_id = 3384;


 client_id | notifiable_type | notifiable_id |     message     |        date         | status
-----------+-----------------+---------------+-----------------+---------------------+--------
         1 | Book            |          3384 | Tweet about it! | 2004-05-01 00:00:00 |      0
         1 | Book            |          3384 | Print it!       | 2004-03-25 00:00:00 |      0

          client_id | notifiable_type | notifiable_id |     message     |        date         | status
-----------+-----------------+---------------+-----------------+---------------------+--------
         1 | Book            |          3384 | Tweet about it! | 2004-05-01 00:00:00 |      0
         1 | Book            |          3384 | Print it!       | 2004-02-23 00:00:00 |      0

