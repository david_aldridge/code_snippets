with
  cte_book_estimates as (
    select
      to_char(date_trunc('month',pub_date),'YYYY MM') pub_month,
      b.id book_id,
      sum(m.money_amount) sale_amt
    from  estimates               e
    join  money                  m   on (m.moneyable_id   = e.id and
                                         m.moneyable_type = 'Estimate')
    join  books                  b   on (b.id = e.book_id )
    group by b.id),
  cte_summaries as (
    select
      pub_month,
      book_id,
      sale_amt,
      sum(sale_amt) over (partition by pub_month) month_sale_amt,
      sum(sale_amt) over (partition by pub_month
                              order by sale_amt desc
                                  rows unbounded preceding) cume_month_sale_amt
    from  cte_book_estimates)
select
  pub_month,
  book_id,
  sale_amt::integer,
  round(cume_month_sale_amt / month_sale_amt * 100,2),
  case when round(cume_month_sale_amt / month_sale_amt * 100,2) 
from
  cte_summaries
order by
  pub_month,
  sale_amt desc;
