options = {
    attachments: [
        {
            "color":       "good",
            "author_name": "Bibliocloud",
            "author_link": "https://app.bibliocloud.com",
            "author_icon": "https://app.bibliocloud.com/favicon-32x32.png",
            "title": "ONIX Batch Executed",
            "title_link": "https://app.bibliocloud.com/export_batches/5764/edit",
            "fields": [
                {
                    "title": "From",
                    "value": "Snowbooks",
                    "short": true
                },
                {
                    "title": "Products checked",
                    "value": "458",
                    "short": true
                },
                {
                    "title": "To",
                    "value": "Amazon",
                    "short": true
                },
                {
                    "title": "Products sent",
                    "value": "2",
                    "short": true
                }
            ]
        }
    ]
}


SendSystemNotification.new(message: nil,options: options).call

{:attachments=>[{:color=>"good", :author_name=>"Bibliocloud", :author_link=>"https://app.bibliocloud.com", :author_icon=>"https://app.bibliocloud.com/favicon-32x32.png", :title=>"ONIX Batch Executed", :title_link=>"https://app.bibliocloud.com/export_batches/5764/edit", :fields=>[{:title=>"From", :value=>"Snowbooks", :short=>true}, {:title=>"Products checked", :value=>"458", :short=>true}, {:title=>"To", :value=>"Amazon", :short=>true}, {:title=>"Products sent", :value=>"2", :short=>true}]}]}

{:color=>"good"
:author_name=>"Bibliocloud"
:author_link=>"https://app.bibliocloud.com"
:author_icon=>"https://app.bibliocloud.com/favicon-32x32.png"
:title=>"Export Batch successfully executed"
:fields=>[{:title=>"From"
:value=>"Zed Books"
:short=>true}
{:title=>"To"
:value=>"Nielsen BookData"
:short=>true}
{:title=>"Products checked"
:value=>185
:short=>true}
{:title=>"Products sent", :value=>0, :short=>true}]}
