ActiveRecord::Base.transaction do
  Work.no_touching do
    Client.is_cpg.works.each do |work|
      work.languages.destroy_all
      work.languages_of_text.english.first_or_create
    end
  end
end

client = Client.is_cpg

ActiveRecord::Base.transaction do
  Work.no_touching do
    %w[
      9780819223319
      9780898694499
      9780898695120
      9780898692181
      9780898691443
      9780898699258
      9780898692204
      9781640653399
      9780819229861
      9781889108995
      9781640653030
      9780898699463
      9780898696271
      9780898696295
      9780898696318
      9781640653283
      9780898699500
    ].each do |isbn|
      work = client.works.joins(:books).find_by(books: {isbn: isbn})
      if work.nil?
        puts "No work found for #{isbn}"
        next
      end
      work.languages.destroy_all
      work.languages_of_text.create!(
        language_code: "spa",
        country_code:  "ES"
      )
    end
  end
end


ActiveRecord::Base.transaction do
  Work.no_touching do
    %w[
      9780898691054
    ].each do |isbn|
      work = client.works.joins(:books).find_by(books: {isbn: isbn})
      if work.nil?
        puts "No work found for #{isbn}"
        next
      end
      work.languages.destroy_all
      work.languages_of_text.create!(
        language_code: "fre",
        country_code:  "FR"
      )
    end
  end
end



ActiveRecord::Base.transaction do
  Work.no_touching do
    %w[
      9780898693904
    ].each do |isbn|
      work = client.works.joins(:books).find_by(books: {isbn: isbn})
      if work.nil?
        puts "No work found for #{isbn}"
        next
      end
      work.languages.destroy_all
      work.languages_of_text.create!(
        language_code: "chi",
        country_code:  "CN"
      )
    end
  end
end
