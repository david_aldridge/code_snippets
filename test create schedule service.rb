work = Work.find(42498)
template = Schedule.find(76)


work.schedules.destroy_all

new = CreateScheduleService.new(
        name:              'test'          ,
        schedulable:       work   ,
        associate_with:    work.books,
        schedule_to_clone: template
      ).call