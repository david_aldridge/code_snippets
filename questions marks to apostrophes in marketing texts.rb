client = Client.find_by(:webname => 'zedbooks')

client.works.each_with_object(Hash.new(0)) do |w, h|
  long_size  = w.marketingtexts.long_descriptions.first.try(:pull_quote).try(:size)
  short_size = w.marketingtexts.short_descriptions.first.try(:pull_quote).try(:size)
  long_type  = case  when long_size.nil? then "None" when long_size <= 350 then "Short" else "Long" end
  short_type  = case  when short_size.nil? then "None" when short_size <= 350 then "Short" else "Long" end
  h[[short_type, long_type]] += 1
end


Marketingtext.find(157651).pull_quote


client.marketingtexts.where("pull_quote like '%?s%'").pluck(:pull_quote).each do |t|
  puts t[t.index("?s")-5..t.index("?s")+5]
end


client.marketingtexts.where("pull_quote similar to '%\?[a-zA-Z]+\?%'").pluck(:pull_quote).each do |t|
  puts t[t.index("?")-10..t.index("?")+10]
end ; 0


client.marketingtexts.pluck(:pull_quote).select{|t| t =~ /[^\w]\?[\w]+\?[^\w]/}

select regexp_replace(pull_quote    , '\?s', '’s', 'g'),
       regexp_replace(marketing_text, '\?s', '’s', 'g')
from marketingtexts
where client_id = 354 and ((pull_quote     like '%?s%' and pull_quote     not like '% ?s%') or
                           (marketing_text like '%?s%' and marketing_text not like '% ?s%'))


update marketingtexts
set    pull_quote     = regexp_replace(pull_quote    , '\?s', '’s', 'g'),
       marketing_text = regexp_replace(marketing_text, '\?s', '’s', 'g')
where client_id = 354 and ((pull_quote     like '%?s%' and pull_quote     not like '% ?s%') or
                           (marketing_text like '%?s%' and marketing_text not like '% ?s%'))
