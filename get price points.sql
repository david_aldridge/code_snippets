select
  product_form,
  price_amount,
  count(*)
from
  prices p
  join books b on b.id = p.book_id
where
  currency_code = 'CAD' and
  price_type = '01' and
  onix_price_type_qualifier_id = '05' and
  b.client_id = 518
group by
  price_amount,
  product_form
order by
  1,2;