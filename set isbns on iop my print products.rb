client   = Client.is_iop
format   = client.proprietary_format_descriptions.is_my_book
my_books = format.books
my_books_without_isbn = my_books.reject(&:isbn_valid?)
unused_isbns = client.isbnlists.available.sort_by(&:number).map(&:number)
my_book_isbns = unused_isbns[20..(20+my_books_without_isbn.size-1)]
client.books.where(isbn: my_book_isbns)

ActiveRecord::Base.transaction do
  my_books_without_isbn.each_with_index do |my_book, index|
    my_book.isbn = my_book_isbns[index]
    my_book.save
  end
  # raise ActiveRecord::Rollback
end


client.isbnlists.where(number: unused_isbns).group(:used).count


client   = Client.is_iop
format   = client.proprietary_format_descriptions.is_my_book
my_books = format.books
my_books.group(:sales_restriction_internal_use_only).count
