s3 = AWS::S3.new(
  :access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
  :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'])

bucket = s3.buckets[ENV['AWS_BUCKET']]

bucket.objects.with_prefix('4').select{|x| x.key.match? /^4\/\d/ }.each do |obj|
  new_key = obj.key.sub("4", "3")
  obj.copy_to(new_key, :bucket => bucket, :acl => :public_read)
end
