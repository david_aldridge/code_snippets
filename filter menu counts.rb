books = Client.is_pharma.books ; 0

h = {product_form: (books.group(:product_form).order(:product_form).count),
edition_name: books.group(:proprietary_edition_description_id).order(:proprietary_edition_description_id).count,
main_bic_code: books.joins(:work).group("works.main_bic_code").order("works.main_bic_code").count}

pp h