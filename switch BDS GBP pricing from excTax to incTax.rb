pp Client.is_bds.books.map{|b| [b.product_form, b.current_gbp_exctax_consumer_price.to_f, b.current_gbp_inctax_consumer_price.to_f]}.uniq.sort

Rails.logger.level=1

Client.is_bds.books.each do |book|
  next if book.current_gbp_exctax_consumer_price.nil?
  tax = (book.product_form == 'DG' ? 1.2 : 1.0)
  book.current_gbp_inctax_consumer_price = tax * book.current_gbp_exctax_consumer_price
end

Client.is_bds.books.each do |book|
  next if book.current_gbp_exctax_consumer_price.nil?
  book.current_gbp_exctax_consumer_price = nil
end

Client.is_bds.update_attributes(standard_price_types: ["gbp_inctax", "eur_exctax", "usd_exctax", "cad_exctax"])