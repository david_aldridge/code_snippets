select
  onix_price_type_qualifier_id as qualifier,
  price_type,
  price_amount as current_inc,
  round(price_amount / 1.2, 2) as calculated_exc,
  round(round(price_amount / 1.2, 2) * 1.2, 2) as recalculated_inc,
  price_amount  =
  round(round(price_amount / 1.2, 2) * 1.2, 2) as no_change,
  count(*)
from prices p join books b on b.id = p.book_id
join clients c on c.id = b.client_id
where c.identifier = 'xxx' and
b.product_form = 'DG'
and price_type = '02' and currency_code = 'GBP'
group by onix_price_type_qualifier_id, price_type,
price_amount, round(price_amount / 1.2, 2),  round(price_amount / 1.2, 2)  =
  round(round(price_amount / 1.2, 2) * 1.2, 2)
order by 1,2

select
  price_amount, max(p.updated_at),count(*)
from prices p join books b on b.id = p.book_id
join clients c on c.id = b.client_id
where c.identifier = 'xxx' and
b.product_form = 'DG'
and price_type = '02' and currency_code = 'GBP'
group by price_amount order by 1;



update prices p
set    price_amount = round(price_amount / 1.2, 2)
where  p.book_id in (
  select b.id
  from   books b join
         clients c on c.id = b.client_id
  where c.identifier = 'xxx' and
        b.product_form = 'DG'
        ) and
  price_type = '02' and
  currency_code = 'GBP'



 current_inc | calculated_exc | count
-------------+----------------+-------
        30.0 |          25.00 |  1850
        32.0 |          26.67 |     1
        42.0 |          35.00 |     1
        65.0 |          54.17 |     2
        82.0 |          68.33 |     3
        95.0 |          79.17 |    50
        97.0 |          80.83 |     1
       105.0 |          87.50 |     1
       114.0 |          95.00 |     3
       120.0 |         100.00 |     3
       125.0 |         104.17 |    30
       130.0 |         108.33 |     2
       145.0 |         120.83 |    14
       146.0 |         121.67 |     5
       150.0 |         125.00 |     6
       156.0 |         130.00 |    10
       162.0 |         135.00 |     1
       168.0 |         140.00 |    19
       173.0 |         144.17 |     2
       180.0 |         150.00 |    30
       188.0 |         156.67 |     2
       192.0 |         160.00 |    16
       204.0 |         170.00 |    22
       216.0 |         180.00 |    30
       220.0 |         183.33 |     5
       228.0 |         190.00 |    26
       235.0 |         195.83 |     1
       240.0 |         200.00 |     9
       251.0 |         209.17 |     2
       252.0 |         210.00 |    18
       264.0 |         220.00 |     4
       266.0 |         221.67 |     4
       276.0 |         230.00 |     8
       282.0 |         235.00 |     3
       288.0 |         240.00 |     2
       329.0 |         274.17 |     1
       344.0 |         286.67 |     1
       360.0 |         300.00 |     3
       376.0 |         313.33 |     1
       391.0 |         325.83 |     1
       406.0 |         338.33 |     4
       421.0 |         350.83 |     1
       436.0 |         363.33 |     3
       466.0 |         388.33 |     2
       481.0 |         400.83 |     1
       496.0 |         413.33 |     2
       511.0 |         425.83 |     2
       541.0 |         450.83 |     1
       571.0 |         475.83 |     1
       586.0 |         488.33 |     1
       601.0 |         500.83 |     1
       631.0 |         525.83 |     1
       677.0 |         564.17 |     3
       737.0 |         614.17 |     2
       756.0 |         630.00 |     1
       770.0 |         641.67 |     1
       785.0 |         654.17 |     1
       799.0 |         665.83 |     1
       856.0 |         713.33 |     1
       884.0 |         736.67 |     1
      1027.0 |         855.83 |     1
      1156.0 |         963.33 |     1
      1170.0 |         975.00 |     1
      1184.0 |         986.67 |     1
      1212.0 |        1010.00 |     1
      1298.0 |        1081.67 |     1
      1313.0 |        1094.17 |     1
      1393.0 |        1160.83 |     1
      1420.0 |        1183.33 |     1
      1685.0 |        1404.17 |     1
      1724.0 |        1436.67 |     1
      1937.0 |        1614.17 |     1
      1963.0 |        1635.83 |     1
      2070.0 |        1725.00 |     1
      2255.0 |        1879.17 |     1
      2401.0 |        2000.83 |     1
      2467.0 |        2055.83 |     1
      2680.0 |        2233.33 |     1
      2693.0 |        2244.17 |     1
      3184.0 |        2653.33 |     1
      8650.0 |        7208.33 |     1
