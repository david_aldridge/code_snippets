
Client.is_iop.audiences.destroy_all

Client.is_iop.works.each do |work|
  work.onix_audiences.create(audience_code_value: "06")
end


Client.is_leuven.works.joins(:books).where(books: {isbn: isbns_05}).uniq.each do |work|
  work.onix_audiences.create(audience_code_value: "05")
end

Client.is_leuven.works.joins(:books).where(books: {isbn: isbns_01}).uniq.each do |work|
  work.onix_audiences.create(audience_code_value: "01")
end
