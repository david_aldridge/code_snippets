select
  case
    when t.status in ('STATUS_ACTIVE', 'STATUS_UNDEFINED','STATUS_SUSPENDED') then
      case
        when "end" < current_date then
          current_date - "end"
        when "start" > current_date then
          current_date - "start"
        else
          ("end" - "start" + 1) / (current_date - "start" + 1)
      end
  end urgency,
  "start",
  "end",
  ("end" - "start" + 1) ,
  (current_date - "start" + 1),
  progress
from
tasks t
where  current_date between "start" and "end" and
t.status in ('STATUS_ACTIVE', 'STATUS_UNDEFINED','STATUS_SUSPENDED')
order by 1 desc;
