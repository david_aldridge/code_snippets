module Ordinalize
  ORDINALS = %w(
  zeroth
  first
  second
  third
  fourth
  fifth
  sixth
  seventh
  eighth
  ninth
  tenth
  eleventh
  twelfth
  thirteenth
  fourteenth
  fifteenth
  sixteenth
  seventeenth
  eighteenth
  nineteenth
  twentieth
  twenty-first
  twenty-second
  twenty-third
  twenty-fourth
  twenty-fifth
  twenty-sixth
  twenty-seventh
  twenty-eighth
  twenty-ninth
  thirtieth
  thirty-first
  ).freeze

  def self.call(input)
    return integer_to_ordinal(input) if input.is_a? Fixnum
    return ordinal_to_integer(input) if input.is_a? String
  end

  def self.ordinal_to_integer(ordinal)
    ORDINALS.index(ordinal)
  end

  def self.integer_to_ordinal(integer)
    ORDINALS[integer]
  end
end



Ordinalize.call(3)
