class Interpolator
  # y = ax + b
  def initialize(a:, b:)
    @a = a.to_f
    @b = b.to_f
  end

  def call(x)
    a * x + b
  end

  def self.constructor(x0:, y0:, x1:, y1:)
    a = (y1.to_f - y0.to_f) / (x1.to_f - x0.to_f)
    b = y0 - a * x0
    new(a: a, b: b)
  end

  attr_reader :a, :b
end

__END__

i1 = Interpolator.new(a: 1, b: 2)

x0 = 80
x1 = 352
y0 = 5.512
y1 = 24.308
i2 = Interpolator.constructor(x0: x0, y0: y0, x1: x1, y1: y1)

i2.call(208)



@a=0.06910294117647059, @b=-0.01623529411764757


module SpineWidth
  MM_PER_PAGE   = 0.06910294117647059
  NO_PAGE_MM = -0.01623529411764757
  def self.call(pages)
    (NO_PAGE_MM + pages * MM_PER_PAGE).round(3)
  end
end


SpineWidth.call(0)
