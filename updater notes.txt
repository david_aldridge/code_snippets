A few issues ...

1. The "Biographical note present" check now passes if there is only one (or no) contributor. Check failures will be re-calculated and updated by a bulk operation later in the day, so there may still be some false failures in there for a while. Viewing the work checks page does recalculate the checks immediately though, so they will only appear to be incorrect on the main Checks page, and only for a short while. I'll update via this thread when they are corrected.
2. Physical, non-book products (e.g. CD-ROMs) could not have measurements assigned to them. Corrected.
3. The top-right icon link for the Production Files card on the metadata page now points to the bulk uploader page, instead of opening the modal window for adding a new marketing image.
4. The bulk uploader now recognises .mp3 and .wav files, and tags them as "Audio - public".
5. In ONIX 3.0 the Collection Sequence composites were not being added. Corrected.