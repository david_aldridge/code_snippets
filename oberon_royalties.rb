oberon_books = Client.find_by(:webname => "Oberon Books")


ActiveRecord::Base.transaction do

  oberon_books.masterchannels.destroy_all
  oberon_books.channels.destroy_all
  oberon_books.royalty_templates.destroy_all

  hps = oberon_books.masterchannels.create!(:masterchannel_name => "Home Print Sales"   , :sales_type => "Product", :inc_product_forms => ["BB", "BC"])
  eps = oberon_books.masterchannels.create!(:masterchannel_name => "Export Print Sales" , :sales_type => "Product", :inc_product_forms => ["BB", "BC"])
  wds = oberon_books.masterchannels.create!(:masterchannel_name => "World Digital Sales", :sales_type => "Product", :inc_product_forms => ["DG"])
  rts = oberon_books.masterchannels.create!(:masterchannel_name => "Rights"             , :sales_type => "Right"  )

  oberon_books.masterchannels.find_by(:masterchannel_name => "Rights"             ).update_attributes(:sales_type => "Right")
  oberon_books.masterchannels.find_by(:masterchannel_name => "World Digital Sales").update_attributes(:inc_product_forms => ["DG"])
  oberon_books.masterchannels.find_by(:masterchannel_name => "Home Print Sales"   ).update_attributes(:inc_product_forms => ["BB", "BC"])
  oberon_books.masterchannels.find_by(:masterchannel_name => "Export Print Sales" ).update_attributes(:inc_product_forms => ["BB", "BC"])

  hps.channels.create!(:channel_name => "Marston (Gardners)"      )
  hps.channels.create!(:channel_name => "Marston (Bertrams)"      )
  hps.channels.create!(:channel_name => "Marston (Website)"       )
  hps.channels.create!(:channel_name => "Marston (Amazon)"        )
  hps.channels.create!(:channel_name => "Marston (Home)"          )
  hps.channels.create!(:channel_name => "Direct"                  )
  hps.channels.create!(:channel_name => "Gratis"                  )
  hps.channels.create!(:channel_name => "Programme texts"         )
  eps.channels.create!(:channel_name => "Theatre Comms Group"     )
  eps.channels.create!(:channel_name => "Currency"                )
  eps.channels.create!(:channel_name => "Marston (Website Export)")
  eps.channels.create!(:channel_name => "Marston (Amazon Export)" )
  eps.channels.create!(:channel_name => "Marston (Export)"        )
  eps.channels.create!(:channel_name => "Peter Hyde"              )
  wds.channels.create!(:channel_name => "Vearsa"                  )
  wds.channels.create!(:channel_name => "Vearsa (Apple)"          )
  wds.channels.create!(:channel_name => "Vearsa (Kobo)"           )
  wds.channels.create!(:channel_name => "Vearsa (Overdrive)"      )
  wds.channels.create!(:channel_name => "Vearsa (EBL)"            )
  wds.channels.create!(:channel_name => "Amazon (UK)"             )
  wds.channels.create!(:channel_name => "Amazon (US)"             )
  wds.channels.create!(:channel_name => "Amazon (EU)"             )
  wds.channels.create!(:channel_name => "Amazon (ROW)"            )
  rts.channels.create!(:channel_name => "Photocopying"            )
  rts.channels.create!(:channel_name => "Amateur Performance"     )
  rts.channels.create!(:channel_name => "Quotation"               )
  rts.channels.create!(:channel_name => "Sheets"                  )
  rts.channels.create!(:channel_name => "Large print"             )
  rts.channels.create!(:channel_name => "Digest"                  )
  rts.channels.create!(:channel_name => "Sublicense"              )
  rts.channels.create!(:channel_name => "Foreign Language"        )
  rts.channels.create!(:channel_name => "First Serial"            )
  rts.channels.create!(:channel_name => "Second Serial"           )
  rts.channels.create!(:channel_name => "Audio"                   )
  rts.channels.create!(:channel_name => "Digital subscription"    )
  rts.channels.create!(:channel_name => "Braille"                 )
  rts.channels.create!(:channel_name => "Non-commercial audio"    )
  rts.channels.create!(:channel_name => "Non-UK republication"    )
  rts.channels.create!(:channel_name => "Anthology"               )

  oberon_books.reload


  play_texts_template                = oberon_books.royalty_templates.first_or_create(:name => "Play texts")
  play_texts_template.masterchannels = oberon_books.masterchannels
  play_texts_template.channels       = oberon_books.channels

ActiveRecord::Base.transaction do

  #spec  = play_texts_template.
  #          royalty_template_masterchannels.
  #          joins(:masterchannel).
  #          where(:masterchannels => {:masterchannel_name => "Home Print Sales"}).
  #          first.
  #          create_royalty_specifier(:price_basis            => RoyaltySpecifier::ESCALATOR_ADD,
  #                                   :royalty_rate_base_pct  => 10)


    play_texts_template.royalty_template_masterchannels.
      joins(:masterchannel).
      where(:masterchannels => {:masterchannel_name => "Home Print Sales" }).
      first.
      create_royalty_specifier!(:price_basis            => RoyaltySpecifier::ESCALATOR_ADD,
                                 :royalty_rate_base_pct  => 10,
                                 :royalty_discount_escalators_attributes => [{:escalation_discount => 50,
                                                                              :royalty_percentage  => -3.5,
                                                                              :client_id           => oberon_books.id}])


  play_texts_template.royalty_template_masterchannels.joins(:masterchannel).where(:masterchannels => {:masterchannel_name => "World Digital Sales" }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  25)
  play_texts_template.royalty_template_masterchannels.joins(:masterchannel).where(:masterchannels => {:masterchannel_name => "Export Print Sales" }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  7.5)

  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Gratis"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Programme texts"     }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 10)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Photocopying"        }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 75)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Quotation"           }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 70)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Sheets"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 70)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Large print"         }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Digest"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Braille"             }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Non-commercial audio"}).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Non-UK republication"}).first.create_royalty_specifier!(:royalty_rate_base_pct  => 80)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Anthology"           }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
end



reload!

oberon_books = Client.find_by(:webname => "Oberon Books")

book = oberon_books.books.first
book = oberon_books.books.is_ebook.first
Work.find(21630).books.each do |book|
book.masterchannels = book.possible_masterchannels
book.book_masterchannels.each do |bmc|
#mc = book.masterchannels.first
  bmc.royalty_specifier = bmc.masterchannel.royalty_template_masterchannels.find_by(:royalty_template_id => play_texts_template.id).royalty_specifier.deep_clone include: RoyaltySpecifier::DEEP_CLONE_ATTR
  bmc.channels = bmc.masterchannel.channels
  bmc.book_channels.each do |bc|
    bc.royalty_specifier = bc.channel.royalty_template_channels.find_by(:royalty_template_id => play_texts_template.id).royalty_specifier.try(:deep_clone, include: RoyaltySpecifier::DEEP_CLONE_ATTR)
  end
end
end

book.book_masterchannels.first.royalty_specifier.royalty_discount_escalators



book.save


.where("? = any (inc_product_forms) or ? != any (inc_product_forms)", "BC", "BC")
