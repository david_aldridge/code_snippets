
def method1_t
  local_errors = [
    ("a" if true),
    ("b" if true),
    ("c" if true),
    ("d" if true),
  ].compact
end


def method2_t
  local_errors = [].tap do |local_error|
    local_error << "a" if true
    local_error << "b" if true
    local_error << "c" if true
    local_error << "d" if true
  end
end

def method1_f
  local_errors = [
    ("a" if false),
    ("b" if false),
    ("c" if false),
    ("d" if false),
  ].compact
end


def method2_f
  local_errors = [].tap do |local_error|
    local_error << "a" if false
    local_error << "b" if false
    local_error << "c" if false
    local_error << "d" if false
  end
end

def method2_e
  local_errors = [].tap do |local_error|
    if true
      local_error << "a"
      break
    end
    local_error << "b" if false
    local_error << "c" if false
    local_error << "d" if false
  end
end


require 'benchmark'

n = 1000000
Benchmark.bm(7) do |x|
  x.report("method 0")   { n.times {} }
  x.report("method 1t")   { n.times {method1_t}}
  x.report("method 2t")   { n.times {method2_t}}
  x.report("method 1f")   { n.times {method1_f}}
  x.report("method 2f")   { n.times {method2_f}}
  x.report("method 2e")   { n.times {method2_e}}
end ; ""

