Rails.logger.level=0

ActiveRecord::Base.transaction do
  Client.is_restless_books.works.map do |w|
    next if w.keywords_array.any?
    new_keywords = w.subjects.where(subject_scheme_identifier: "20").map(&:subject_heading_text).join(";").split(",").map(&:squish).join(";")
    next if new_keywords.blank?
    w.update(keywords: new_keywords)
  end
  # raise ActiveRecord::Rollback
end