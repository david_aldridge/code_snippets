DigitalAssetContentItem.
  where(DigitalAssetContentItem.arel_table["created_at"].gt Date.today - 1.days).
  group(:client).
  count.
  transform_keys(&:to_s)
