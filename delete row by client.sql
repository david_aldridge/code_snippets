SELECT table_name, column_name
FROM information_schema.columns
WHERE table_schema = 'public'
  AND table_name   = 'books'


;


SELECT distinct 'delete from '||table_name ||' where client_id = 352;'
FROM information_schema.columns
WHERE table_schema = 'public'
  AND column_name = 'client_id'
  and table_name not in ('clients', 'users') order by 1;

SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'public'
  AND table_name not in (SELECT distinct table_name
FROM information_schema.columns
WHERE table_schema = 'public'
  AND column_name = 'client_id') ;

SELECT distinct 'truncate table '||table_name ||';'
FROM information_schema.columns
WHERE table_schema = 'public'
  AND column_name = 'client_id'
  and table_name not in ('clients', 'users') order by 1;



delete from barcodeimgs where client_id = 356;


 delete from addresses where client_id = 356;
 delete from advances where client_id = 356;
 delete from advisories where client_id = 356;
 delete from ai_archives where client_id = 356;
 delete from ai_templates where client_id = 356;
 delete from altcontacts where client_id = 356;
 delete from amazontitles where client_id = 356;
 delete from backup_contract_work_id where client_id = 356;
 delete from bicgeogsubjects where client_id = 356;
 delete from bicsubjects where client_id = 356;
 delete from bisacsubjects where client_id = 356;
 delete from book_briefs where client_id = 356;
 delete from book_channel_forecasts where client_id = 356;
 delete from book_channels where client_id = 356;
 delete from book_collection_items where client_id = 356;
 delete from book_forecast_origination_costs where client_id = 356;
 delete from book_validation_reports where client_id = 356;
 delete from book_validation_templates where client_id = 356;
 delete from book_validation_tests where client_id = 356;
 delete from bookaudiences where client_id = 356;
 delete from bookbicgeogsubjects where client_id = 356;
 delete from bookbisacsubjects where client_id = 356;
 delete from bookcontacts where client_id = 356;
 delete from bookmarks where client_id = 356;
 delete from books where client_id = 356;
 delete from booksubjects where client_id = 356;
 delete from briefing_attachments where client_id = 356;
 delete from briefing_text_setups where client_id = 356;
 delete from briefing_texts where client_id = 356;
 delete from briefs where client_id = 356;
 delete from chargeables where client_id = 356;
 delete from collection_items where client_id = 356;
 delete from collection_tasks where client_id = 356;
 delete from collections where client_id = 356;
 delete from comments where client_id = 356;
 delete from contact_digital_asset_transfer_templates where client_id = 356;
 delete from contact_digital_asset_transfers where client_id = 356;
 delete from contact_marketing_items where client_id = 356;
 delete from contact_prints where client_id = 356;
 delete from contact_seriesnames where client_id = 356;
 delete from contactfeatures where client_id = 356;
 delete from contacts where client_id = 356;
 delete from contained_items where client_id = 356;
 delete from content_items where client_id = 356;
 delete from contract_attachments where client_id = 356;
 delete from contract_channels where client_id = 356;
 delete from contractcontributor_sale_royalties where client_id = 356;
 delete from contractcontributors where client_id = 356;
 delete from contracts where client_id = 356;
 delete from contractsets where client_id = 356;
 delete from copy_trackers where client_id = 356;
 delete from cost_estimates where client_id = 356;
 delete from cost_groups where client_id = 356;
 delete from cover_transmissions where client_id = 356;
 delete from currencies where client_id = 356;
 delete from delayed_jobs where client_id = 356;
 delete from delete_me_contact_roles where client_id = 356;
 delete from deliveries where client_id = 356;
 delete from designs where client_id = 356;
 delete from digital_asset_content_items where client_id = 356;
 delete from digital_asset_contents where client_id = 356;
 delete from digital_assets where client_id = 356;
 delete from edition_import_batches where client_id = 356;
 delete from editorial_originations where client_id = 356;
 delete from emails where client_id = 356;
 delete from estimate_periods where client_id = 356;
 delete from estimate_templates where client_id = 356;
 delete from estimated_rights_deal_revenues where client_id = 356;
 delete from estimated_rights_deals where client_id = 356;
 delete from estimates where client_id = 356;
 delete from events where client_id = 356;
 delete from excels where client_id = 356;
 delete from expectedsales where client_id = 356;
 delete from extents where client_id = 356;
 delete from finance_code_allocations where client_id = 356;
 delete from foreignright_classifications where client_id = 356;
 delete from foreignrights where client_id = 356;
 delete from ftp_templates where client_id = 356;
 delete from help_contents where client_id = 356;
 delete from helps where client_id = 356;
 delete from identifiable_onix_identifier_types where client_id = 356;
 delete from import_errors where client_id = 356;
 delete from import_tables where client_id = 356;
 delete from importadvancecsvs where client_id = 356;
 delete from importeditioncsvs where client_id = 356;
 delete from importeditions where client_id = 356;
 delete from importgeneralcsvs where client_id = 356;
 delete from importisbncsvs where client_id = 356;
 delete from importisbns where client_id = 356;
 delete from importisbnxlsxes where client_id = 356;
 delete from importmainordercsvs where client_id = 356;
 delete from importmainorders where client_id = 356;
 delete from importpaymentcsvs where client_id = 356;
 delete from importpostcsvs where client_id = 356;
 delete from importposts where client_id = 356;
 delete from importpurchasecsvs where client_id = 356;
 delete from importsalecsvs where client_id = 356;
 delete from importsales where client_id = 356;
 delete from importukexportcontactparsers where client_id = 356;
 delete from importukexportcontacts where client_id = 356;
 delete from importwatkinscontacts where client_id = 356;
 delete from keywords where client_id = 356;
 delete from languages where client_id = 356;
 delete from licensed_rights where client_id = 356;
 delete from line_items where client_id = 356;
 delete from local_production_files where client_id = 356;
 delete from locks where client_id = 356;
 delete from logins where client_id = 356;
 delete from mailing_lists where client_id = 356;
 delete from marketing_items where client_id = 356;
 delete from marketingtexts where client_id = 356;
 delete from masterrules where client_id = 356;
 delete from measurements where client_id = 356;
 delete from money where client_id = 356;
 delete from monthly_sale_profiles where client_id = 356;
 delete from ms_delivery_dates where client_id = 356;
 delete from notes where client_id = 356;
 delete from onixarchives where client_id = 356;
 delete from orders where client_id = 356;
 delete from payments where client_id = 356;
 delete from periodactives where client_id = 356;
 delete from periods where client_id = 356;
 delete from permissions where client_id = 356;
 delete from phones where client_id = 356;
 delete from posts where client_id = 356;
 delete from price_value_defaults where client_id = 356;
 delete from prices where client_id = 356;
 delete from print_binding_details where client_id = 356;
 delete from print_bundle_components where client_id = 356;
 delete from print_costings where client_id = 356;
 delete from print_cover_details where client_id = 356;
 delete from print_embellishments where client_id = 356;
 delete from print_finishes where client_id = 356;
 delete from print_item_cost_centres where client_id = 356;
 delete from print_item_print_item_cost_centres where client_id = 356;
 delete from print_items where client_id = 356;
 delete from print_on_demand_specs where client_id = 356;
 delete from print_originations where client_id = 356;
 delete from print_paper_details where client_id = 356;
 delete from print_part_items where client_id = 356;
 delete from print_parts where client_id = 356;
 delete from print_print_costings where client_id = 356;
 delete from printer_estimates where client_id = 356;
 delete from prints where client_id = 356;
 delete from prizes where client_id = 356;
 delete from product_titles where client_id = 356;
 delete from productcodes where client_id = 356;
 delete from production_files where client_id = 356;
 delete from profitarchives where client_id = 356;
 delete from progress_bars where client_id = 356;
 delete from project_attachments where client_id = 356;
 delete from projects where client_id = 356;
 delete from proposal_texts where client_id = 356;
 delete from purchase_line_items where client_id = 356;
 delete from purchases where client_id = 356;
 delete from quick_deals where client_id = 356;
 delete from quickprices where client_id = 356;
 delete from quickprints where client_id = 356;
 delete from quickrights where client_id = 356;
 delete from quickstarts where client_id = 356;
 delete from receipts where client_id = 356;
 delete from relatedproducts where client_id = 356;
 delete from relationships where client_id = 356;
 delete from reports where client_id = 356;
 delete from reprints where client_id = 356;
 delete from returnvols where client_id = 356;
 delete from rightlists where client_id = 356;
 delete from rightrules where client_id = 356;
 delete from rights where client_id = 356;
 delete from rightsadvances where client_id = 356;
 delete from rightsguide_archives where client_id = 356;
 delete from rightslist_bundles where client_id = 356;
 delete from role_adopters where client_id = 356;
 delete from role_involvements where client_id = 356;
 delete from role_sets where client_id = 356;
 delete from royalty_batches where client_id = 356;
 delete from royalty_date_escalators where client_id = 356;
 delete from royalty_discount_escalators where client_id = 356;
 delete from royalty_liabilities where client_id = 356;
 delete from royalty_quantity_escalator_channel_rates where client_id = 356;
 delete from royalty_quantity_escalators where client_id = 356;
 delete from royalty_specifiers where client_id = 356;
 delete from royarchives where client_id = 356;
 delete from roystatements where client_id = 356;
 delete from rules where client_id = 356;
 delete from sale_royalties where client_id = 356;
 delete from saleimportrecords where client_id = 356;
 delete from sales where client_id = 356;
 delete from sales_profiles where client_id = 356;
 delete from sales_restrictions where client_id = 356;
 delete from scales where client_id = 356;
 delete from schedules where client_id = 356;
 delete from scratchpads where client_id = 356;
 delete from seriesname_channels where client_id = 356;
 delete from seriesname_default_prices where client_id = 356;
 delete from shipments where client_id = 356;
 delete from shipping_rates where client_id = 356;
 delete from shop_collections where client_id = 356;
 delete from shop_product_images where client_id = 356;
 delete from shop_product_variants where client_id = 356;
 delete from shop_products where client_id = 356;
 delete from sign_offs where client_id = 356;
 delete from simple_print_prices where client_id = 356;
 delete from spend_authorisations where client_id = 356;
 delete from spend_budgets where client_id = 356;
 delete from spreads where client_id = 356;
 delete from statement_batch_contacts where client_id = 356;
 delete from statement_batches where client_id = 356;
 delete from statement_bmc_channel_rates where client_id = 356;
 delete from statement_bmc_date_escalators where client_id = 356;
 delete from statement_bmc_discount_escalators where client_id = 356;
 delete from statement_bmc_qty_escalators where client_id = 356;
 delete from statement_bmc_specifiers where client_id = 356;
 delete from statement_book_date_escalators where client_id = 356;
 delete from statement_book_discount_escalators where client_id = 356;
 delete from statement_book_qty_escalators where client_id = 356;
 delete from statement_book_specifiers where client_id = 356;
 delete from statement_books where client_id = 356;
 delete from statement_channels where client_id = 356;
 delete from statement_contract_channels where client_id = 356;
 delete from statement_contracts where client_id = 356;
 delete from statement_masterchannels where client_id = 356;
 delete from statement_monthly_royalties where client_id = 356;
 delete from statement_monthly_sales where client_id = 356;
 delete from statement_return_reserve_rates where client_id = 356;
 delete from statement_templates where client_id = 356;
 delete from statement_works where client_id = 356;
 delete from statements where client_id = 356;
 delete from subject_corporate_bodies where client_id = 356;
 delete from subject_people where client_id = 356;
 delete from subject_places where client_id = 356;
 delete from subjectcodes where client_id = 356;
 delete from subjects where client_id = 356;
 delete from supplies where client_id = 356;
 delete from supply_lists where client_id = 356;
 delete from supplydetails where client_id = 356;
 delete from supportingresources where client_id = 356;
 delete from system_codes where client_id = 356;
 delete from system_feature_users where client_id = 356;
 delete from system_messages where client_id = 356;
 delete from task_roles where client_id = 356;
 delete from tasks where client_id = 356;
 delete from territories where client_id = 356;
 delete from transmissions where client_id = 356;
 delete from tweets where client_id = 356;
 delete from upload_file_types where client_id = 356;
 delete from upload_files where client_id = 356;
 delete from uploads where client_id = 356;
 delete from validation_templates where client_id = 356;
 delete from web_links where client_id = 356;
 delete from webs where client_id = 356;
 delete from work_users where client_id = 356;
 delete from workcontacts where client_id = 356;
 delete from works where client_id = 356;
 delete from xmlimports where client_id = 356;
 delete from zip_archives where client_id = 356;