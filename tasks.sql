select    s.name,
          --'https://'|| ? ||'/schedules/' || s.id::varchar as schedule_url,
          w.title,
          t.index_number,
          t.id,
          t.name,
          t.duration,
          t.status,
          t.start,
          t.end,
          t.benchmark
from      tasks      t
left join task_users tu on t.id = tu.task_id
left join users      u  on u.id = tu.user_id
     join schedules  s  on s.id = t.schedule_id
left join works      w  on w.id = s.schedulable_id and s.schedulable_type = 'Work'
where     s.template is not true
--t.client_id in (?)
order by  s.name,
          t.index_number;
