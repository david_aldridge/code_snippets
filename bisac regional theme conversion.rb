h = {"1.0.0.0.0.0.0" =>	"Europe",
"1.1.0.0.0.0.0" =>	"British Isles",
"1.1.1.0.0.0.0" =>	"Ireland",
"1.1.1.0.0.1.0" =>	"Dublin",
"1.1.2.0.0.0.0" =>	"United Kingdom, Great Britain",
"1.1.2.1.0.0.0" =>	"Channel Islands",
"1.1.2.2.0.0.0" =>	"England",
"1.1.2.2.0.1.0" =>	"London",
"1.1.2.2.0.2.0" =>	"Manchester",
"1.1.2.3.0.0.0" =>	"Isle of Man",
"1.1.2.4.0.0.0" =>	"Northern Ireland",
"1.1.2.5.0.0.0" =>	"Scotland",
"1.1.2.5.0.1.0" =>	"Edinburgh",
"1.1.2.5.0.2.0" =>	"Glasgow",
"1.1.2.6.0.0.0" =>	"Wales",
"1.2.0.0.0.0.0" =>	"Western Continental Europe",
"1.2.1.0.0.0.0" =>	"Belgium",
"1.2.2.0.0.0.0" =>	"France",
"1.2.2.0.0.1.0" =>	"Paris",
"1.2.2.1.0.0.0" =>	"Corsica",
"1.2.3.0.0.0.0" =>	"Luxembourg",
"1.2.4.0.0.0.0" =>	"Monaco",
"1.2.5.0.0.0.0" =>	"Netherlands",
"1.2.5.0.0.1.0" =>	"Amsterdam",
"1.3.0.0.0.0.0" =>	"Central Europe",
"1.3.1.0.0.0.0" =>	"Austria",
"1.3.1.0.0.1.0" =>	"Vienna",
"1.3.2.0.0.0.0" =>	"Germany",
"1.3.2.0.0.1.0" =>	"Berlin",
"1.3.3.0.0.0.0" =>	"Liechtenstein",
"1.3.4.0.0.0.0" =>	"Switzerland",
"1.3.4.0.0.1.0" =>	"Geneva",
"1.4.0.0.0.0.0" =>	"Northern Europe, Scandinavia",
"1.4.1.0.0.0.0" =>	"Denmark",
"1.4.1.0.0.1.0" =>	"Copenhagen",
"1.4.1.1.0.0.0" =>	"Faroe Islands",
"1.4.1.2.0.0.0" =>	"Greenland",
"1.4.2.0.0.0.0" =>	"Finland",
"1.4.3.0.0.0.0" =>	"Iceland",
"1.4.4.0.0.0.0" =>	"Norway",
"1.4.5.0.0.0.0" =>	"Sweden",
"1.4.5.0.0.1.0" =>	"Stockholm",
"1.5.0.0.0.0.0" =>	"Southern Europe",
"1.5.1.0.0.0.0" =>	"Andorra",
"1.5.2.0.0.0.0" =>	"Gibraltar",
"1.5.3.0.0.0.0" =>	"Italy",
"1.5.3.0.0.1.0" =>	"Rome",
"1.5.3.0.0.2.0" =>	"Venice",
"1.5.3.1.0.0.0" =>	"Sicily",
"1.5.3.2.0.0.0" =>	"Tuscany",
"1.5.3.2.0.1.0" =>	"Florence",
"1.5.4.0.0.0.0" =>	"Malta",
"1.5.5.0.0.0.0" =>	"Portugal",
"1.5.5.0.0.1.0" =>	"Lisbon",
"1.5.5.1.0.0.0" =>	"Azores",
"1.5.5.2.0.0.0" =>	"Madeira",
"1.5.6.0.0.0.0" =>	"San Marino",
"1.5.7.0.0.0.0" =>	"Spain",
"1.5.7.0.0.1.0" =>	"Barcelona",
"1.5.7.0.0.2.0" =>	"Madrid",
"1.5.7.1.0.0.0" =>	"Canary Islands",
"1.5.8.0.0.0.0" =>	"Vatican",
"1.6.0.0.0.0.0" =>	"Eastern Europe",
"1.6.1.0.0.0.0" =>	"Cyprus",
"1.6.2.0.0.0.0" =>	"Czech Republic",
"1.6.2.0.0.1.0" =>	"Prague",
"1.6.3.0.0.0.0" =>	"Hungary",
"1.6.3.0.0.1.0" =>	"Budapest",
"1.6.4.0.0.0.0" =>	"Poland",
"1.6.4.0.0.1.0" =>	"Warsaw",
"1.6.5.0.0.0.0" =>	"Slovakia",
"1.6.6.0.0.0.0" =>	"Turkey",
"1.6.6.0.0.1.0" =>	"Istanbul",
"1.6.7.0.0.0.0" =>	"Former Soviet Union, USSR (Europe)",
"1.6.8.0.0.0.0" =>	"Armenia",
"1.6.9.0.0.0.0" =>	"Azerbaijan",
"1.6.10.0.0.0.0" =>	"Belarus",
"1.6.11.0.0.0.0" =>	"Estonia",
"1.6.12.0.0.0.0" =>	"Georgia (Republic)",
"1.6.13.0.0.0.0" =>	"Latvia",
"1.6.14.0.0.0.0" =>	"Lithuania",
"1.6.15.0.0.0.0" =>	"Moldova (Moldavia)",
"1.6.16.0.0.0.0" =>	"Russia",
"1.6.16.0.0.1.0" =>	"Moscow",
"1.6.16.0.0.2.0" =>	"Saint Petersburg",
"1.6.16.0.1.0.0" =>	"Chechnya",
"1.6.17.0.0.0.0" =>	"Ukraine",
"1.7.0.0.0.0.0" =>	"Southeast Europe",
"1.7.1.0.0.0.0" =>	"Albania",
"1.7.2.0.0.0.0" =>	"Bulgaria",
"1.7.3.0.0.0.0" =>	"Greece",
"1.7.3.0.0.1.0" =>	"Athens",
"1.7.3.1.0.0.0" =>	"Greek Islands",
"1.7.3.1.1.0.0" =>	"Crete",
"1.7.4.0.0.0.0" =>	"Bosnia-Herzegovina",
"1.7.5.0.0.0.0" =>	"Croatia",
"1.7.6.0.0.0.0" =>	"Kosovo",
"1.7.7.0.0.0.0" =>	"Macedonia",
"1.7.8.0.0.0.0" =>	"Montenegro",
"1.7.9.0.0.0.0" =>	"Serbia",
"1.7.10.0.0.0.0" =>	"Slovenia",
"1.7.11.0.0.0.0" =>	"Romania",
"1.7.12.0.0.0.0" =>	"Yugoslavia & former Yugoslavia",
"1.8.0.0.0.0.0" =>	"Alps"}




h = Hash[*codelist("publisher_role").flatten]



h.each_with_object(Hash.new){|(k, v), h2| h2[k]= h[k.split(".")[0]+".0.0.0.0.0.0"] + ": " + v}

.to_a
