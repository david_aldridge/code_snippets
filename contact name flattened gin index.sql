explain select id from contacts
where client_id = 473 and
coalesce(
        case when corporate_acronym is not null then
          unaccent(regexp_replace(corporate_name, '[^[:alnum:]]', '', 'g')) || '~' || unaccent(regexp_replace(corporate_acronym, '[^[:alnum:]]', '', 'g'))
        else
          unaccent(regexp_replace(corporate_name, '[^[:alnum:]]', '', 'g'))
        end,
        unaccent(regexp_replace(person_name, '[^[:alnum:]]', '', 'g'))
      )
      ilike '%' || unaccent(regexp_replace('niel', '[^[:alnum:]]', '', 'g')) || '%'
;

 Bitmap Heap Scan on contacts  (cost=485.77..3626.08 rows=168 width=4)
   Recheck Cond: (client_id = 473)
   Filter: (COALESCE(CASE WHEN (corporate_acronym IS NOT NULL) THEN ((unaccent(regexp_replace((corporate_name)::text, '[^[:alnum:]]'::text, ''::text, 'g'::text)) || '~'::text) || unaccent(regexp_replace((corporate_acronym)::text, '[^[:alnum:]]'::text, ''::text, 'g'::text))) ELSE unaccent(regexp_replace((corporate_name)::text, '[^[:alnum:]]'::text, ''::text, 'g'::text)) END, unaccent(regexp_replace((person_name)::text, '[^[:alnum:]]'::text, ''::text, 'g'::text))) ~~* (('%'::text || unaccent('niel'::text)) || '%'::text))
   ->  Bitmap Index Scan on index_contacts_on_client_id_and_legacy_identifier  (cost=0.00..485.73 rows=20975 width=0)
         Index Cond: (client_id = 473)
(5 rows)

contact_name_flattened



create index contacts_client_flattened_name on contacts USING gin(
client_id,
contact_name_flattened(corporate_acronym, corporate_name, person_name)
);

CREATE OR REPLACE FUNCTION contact_name_flattened(
  corporate_acronym contacts.corporate_acronym%TYPE,
  corporate_name contacts.corporate_name%TYPE,
  person_name contacts.person_name%TYPE
  )
  RETURNS text
AS
$BODY$
    select coalesce(
        case when corporate_acronym is not null then
          unaccent(regexp_replace(corporate_name, '[^[:alnum:]]', '', 'g')) || '~' || unaccent(regexp_replace(corporate_acronym, '[^[:alnum:]]', '', 'g'))
        else
          unaccent(regexp_replace(corporate_name, '[^[:alnum:]]', '', 'g'))
        end,
        unaccent(regexp_replace(person_name, '[^[:alnum:]]', '', 'g'))
      );
$BODY$
LANGUAGE sql
IMMUTABLE;


explain select id from contacts
where client_id = 473 and
contact_name_flattened(corporate_acronym, corporate_name, person_name)
      ilike '%' || unaccent(regexp_replace('niel', '[^[:alnum:]]', '', 'g')) || '%'
;


---------------------------------------------------------------------------------------------------------------------------------------------
 Bitmap Heap Scan on contacts  (cost=200.62..8033.60 rows=167 width=4)
   Recheck Cond: (client_id = 473)
   Filter: (contact_name_flattened(corporate_acronym, corporate_name, person_name) ~~* (('%'::text || unaccent('niel'::text)) || '%'::text))
   ->  Bitmap Index Scan on contacts_client_flattened_name  (cost=0.00..200.58 rows=20877 width=0)
         Index Cond: (client_id = 473)
(5 rows)

bibliocloud_app=#


