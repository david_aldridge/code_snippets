Delete obsolete records
=======================

## Total ExportBatchItems
ExportBatchItem.count
## Deletable ExportBatchItems
ExportBatchItem.joins(:export_batch).where(export_batches: {incremental: true}).where("export_batches.created_at < ?", Date.today - 3.months).count
## Deletable ExportBatchItems
ExportBatchItem.joins(:export_batch).where(export_batches: {incremental: true}).where("export_batches.created_at < ?", Date.today - 3.months).delete_all

vacuum verbose analyze export_batch_items


ExportBatch.count
ExportBatch.where(incremental: true).where("created_at < ?", Date.today - 3.months).count
ExportBatch.where(incremental: true).where("created_at < ?", Date.today - 3.months).delete_all

DigitalAssetContentItem.count
DigitalAssetContentItem.orphans.count
DigitalAssetContentItem.orphans.destroy_all
ShopifyLog.where("created_at < ?", Date.today - 7.days).delete_all





SELECT
table_name
    , pg_size_pretty(total_bytes) AS total
    , pg_size_pretty(toast_bytes) AS toast
    , pg_size_pretty(table_bytes) AS table
    , pg_size_pretty(index_bytes) AS index
  FROM (
  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
              , c.reltuples AS row_estimate
              , pg_total_relation_size(c.oid) AS total_bytes
              , pg_indexes_size(c.oid) AS index_bytes
              , pg_total_relation_size(reltoastrelid) AS toast_bytes
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r'
  ) a
) a
order by toast_bytes + table_bytes desc nulls last;
