Client.is_pharma.
  books.is_print.
  where.not(product_form_detail: nil).
  select do |b|
    b.product_form_detail.select{|pfd| pfd.starts_with? "E" }.any?
  end.each do |b|
    b.update_attributes(product_form_detail: nil)
  end
