client = Client.is_liverpool

books = client.publishernames.find_by(publisher_name: "Historic England").books ; 0

books.group(:allow_onix_exports).count
books.update_all(allow_onix_exports: false)
books.group(:product_form).count

irb Liverpool::SendONIX.new

custom_run Contact::CLOUD_PUBLISH, books:  books.includes(:seriesnames), transmit: false