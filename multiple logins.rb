pp RequestLog.where("created_at between ? and ?", Time.now - 10.days, Time.now).where.not(user_id: nil).where(request: "/users/login").group(:user_id, :ip_address).order(:user_id).count.group_by{ |k, v| k.first }.transform_keys{|k| User.find(k).email}

{"emma@snowbooks.com"=>[[[2, "81.135.84.252"], 2]],
 "charles@amberbooks.co.uk"=>
  [[[5228, "217.138.31.130"], 2], [[5228, "81.168.70.17"], 3]],
 "sallym@tworiverspress.com"=>[[[5371, "80.2.128.105"], 1]],
 "rebecca@inpressbooks.co.uk"=>
  [[[5416, "31.22.6.67"], 7], [[5416, "94.195.49.61"], 1]],
 "sophie@inpressbooks.co.uk"=>
  [[[5418, "31.22.6.67"], 2], [[5418, "82.33.157.139"], 1]],
 "editor@theemmapress.com"=>
  [[[5430, "176.251.29.60"], 1],
   [[5430, "90.194.78.71"], 2],
   [[5430, "94.4.78.15"], 6]],
 "info@alisonjones.com"=>
  [[[5452, "86.130.165.87"], 2], [[5452, "86.180.200.81"], 2]],
 "nick@canelo.co"=>[[[5511, "83.216.89.182"], 1]],
 "francesca@canelo.co"=>[[[5512, "83.216.89.182"], 4]],
 "michael@canelo.co"=>
  [[[5513, "83.216.89.182"], 5], [[5513, "90.221.128.143"], 1]],
 "david+lup@bibliocloud.com"=>[[[5514, "81.135.84.252"], 5]],
 "editorial.assistant@zedbooks.net"=>
  [[[5518, "176.250.148.0"], 1], [[5518, "5.148.137.253"], 7]],
 "kim.walker@zedbooks.net"=>
  [[[5520, "5.148.137.253"], 9], [[5520, "92.10.46.189"], 1]],
 "emma+lup@bibliocloud.com"=>[[[5524, "81.135.84.252"], 1]],
 "jennifer.howard@liverpool.ac.uk"=>[[[5525, "138.253.53.163"], 3]],
 "ken.barlow@zedbooks.net"=>[[[5532, "5.148.137.253"], 2]],
 "renata.kasprzak@zedbooks.net"=>[[[5533, "5.148.137.253"], 2]],
 "ruben@zedbooks.net"=>[[[5534, "5.148.137.253"], 8]],
 "vidisha.biswas@zedbooks.net"=>[[[5535, "5.148.137.253"], 6]],
 "eleanor@poetrybusiness.co.uk"=>[[[5573, "81.174.201.111"], 1]],
 "office@poetrybusiness.co.uk"=>
  [[[5575, "37.252.28.110"], 1], [[5575, "81.174.201.111"], 1]],
 "anneberth@inpressbooks.co.uk"=>[[[5593, "2.120.200.141"], 4]],
 "janet.mcdermott@liv.ac.uk"=>[[[5597, "138.253.53.145"], 14]],
 "awelsby@liv.ac.uk"=>
  [[[5599, "109.154.17.246"], 3], [[5599, "138.253.53.174"], 1]],
 "hshgalla@student.liverpool.ac.uk"=>[[[5601, "138.253.53.179"], 8]],
 "chloe.johnson@liverpool.ac.uk"=>
  [[[5602, "138.253.53.155"], 6], [[5602, "138.253.53.170"], 2]],
 "kika.sroka-miller@zedbooks.net"=>[[[5607, "5.148.137.253"], 1]],
 "rob.burleigh@bdspublishing.com"=>[[[5611, "82.20.18.150"], 6]],
 "john.coefield@v-publishing.co.uk"=>
  [[[5620, "81.136.255.131"], 10], [[5620, "84.51.134.94"], 2]],
 "emma+lundh@bibliocloud.com"=>[[[5642, "146.90.42.190"], 1]],
 "lmyers@lundhumphries.com"=>[[[5643, "62.252.28.66"], 5]],
 "sthorowgood@lundhumphries.com"=>[[[5645, "62.252.28.66"], 1]],
 "lclark@lundhumphries.com"=>
  [[[5646, "62.252.28.66"], 5], [[5646, "86.2.21.138"], 1]],
 "lewin@lundhumphries.com"=>[[[5647, "62.252.28.66"], 6]],
 "tfurness@lundhumphries.com"=>[[[5648, "62.252.28.66"], 2]],
 "vbenjamin@lundhumphries.com"=>[[[5649, "62.252.28.66"], 5]],
 "simon@canelo.co"=>[[[5650, "83.216.89.182"], 3]],
 "david+lundh@bibliocloud.com"=>[[[5656, "81.135.84.252"], 1]],
 "vrose@lundhumphries.com"=>[[[5664, "62.252.28.66"], 8]],
 "m.lo@gold.ac.uk"=>[[[5665, "158.223.48.246"], 7]],
 "karen.phair@liverpool.ac.uk"=>[[[5666, "138.253.53.123"], 1]],
 "ehooker@lundhumphries.com"=>[[[5667, "62.252.28.66"], 3]],
 "david+riba@bibliocloud.com"=>
  [[[5668, "31.185.194.114"], 1], [[5668, "81.135.84.252"], 1]],
 "lesley.henderson@ribabookshops.com"=>[[[5671, "212.44.32.134"], 7]],
 "philip.handley@ribapublishing.com"=>[[[5672, "212.44.32.134"], 3]],
 "richard.blackburn@ribapublishing.com"=>[[[5674, "85.115.52.201"], 9]],
 "ginny.mills@ribapublishing.com"=>
  [[[5676, "146.198.144.76"], 1], [[5676, "212.44.32.134"], 4]],
 "steven.plimmer@ribaenterprises.com"=>[[[5680, "212.44.32.134"], 1]],
 "laurence.radford@zedbooks.net"=>
  [[[5712, "5.148.137.253"], 6], [[5712, "82.28.76.23"], 1]],
 "emily@bibliocloud.com"=>
  [[[5716, "213.205.251.106"], 1],
   [[5716, "2.30.135.226"], 3],
   [[5716, "79.67.177.111"], 1],
   [[5716, "81.135.84.252"], 2]],
 "jason@unbound.co.uk"=>[[[5721, "185.53.226.154"], 1]],
 "amy@unbound.co.uk"=>[[[5724, "185.53.226.154"], 1]],
 "deandra@unbound.co.uk"=>[[[5725, "185.53.226.154"], 1]],
 "agata.mrva-montoya@sydney.edu.au"=>[[[5727, "129.78.56.142"], 1]],
 "denise.odea@sydney.edu.au"=>
  [[[5728, "129.78.56.135"], 1], [[5728, "129.78.56.143"], 3]],
 "alison.major@ucl.ac.uk"=>
  [[[5737, "128.86.177.160"], 2],
   [[5737, "193.60.240.99"], 2],
   [[5737, "213.205.251.14"], 1]],
 "c.penfold@ucl.ac.uk"=>[[[5739, "128.41.35.160"], 11]],
 "j.biggins@ucl.ac.uk"=>[[[5740, "128.41.13.14"], 7]],
 "e.sullivan@ucl.ac.uk"=>[[[5741, "128.41.13.175"], 3]],
 "andy@bibliocloud.com"=>[[[5747, "81.135.84.252"], 2]],
 "patrick.brereton@liverpool.ac.uk"=>[[[5776, "91.231.45.10"], 3]],
 "emily+amber@bibliocloud.com"=>[[[5778, "81.135.84.252"], 1]],
 "emily.morrell@sas.ac.uk"=>
  [[[5781, "128.86.173.146"], 1],
   [[5781, "128.86.173.196"], 1],
   [[5781, "82.14.23.122"], 3]],
 "emily+lund@bibliocloud.com"=>[[[5789, "81.135.84.252"], 3]],
 "lorna@v-publishing.co.uk"=>[[[5791, "81.136.255.131"], 2]],
 "nick.judd@rpharms.com"=>[[[5801, "80.82.136.68"], 8]],
 "deanna.marbeck@gmail.com"=>
  [[[5802, "80.82.136.68"], 1], [[5802, "81.110.147.127"], 4]],
 "david+pharma@bibliocloud.com"=>[[[5803, "81.135.84.252"], 5]],
 "elaine.pennington@rpharms.com"=>[[[5809, "80.82.136.68"], 5]],
 "lauren.de-ath@sas.ac.uk"=>[[[5815, "194.80.206.57"], 2]],
 "anna@unbound.com"=>[[[5825, "185.53.226.154"], 2]],
 "naziur.rahman@rpharms.com"=>[[[5826, "80.82.136.68"], 1]],
 "george.mensah@rpharms.com"=>[[[5827, "80.82.136.68"], 1]],
 "pfroehli@purdue.edu"=>[[[5832, "98.223.75.58"], 1]],
 "kpurple@purdue.edu"=>[[[5834, "128.210.106.225"], 1]],
 "bshaffer@purdue.edu"=>[[[5835, "128.210.106.225"], 1]],
 "jessica.davies@sas.ac.uk"=>[[[5847, "194.80.206.24"], 3]],
 "jamie@valleypressuk.com"=>[[[5850, "185.4.159.9"], 2]],
 "kwaku@unbound.co.uk"=>[[[5853, "185.53.226.154"], 2]],
 "phil@unbound.co.uk"=>[[[5854, "185.53.226.154"], 1]],
 "imogen@unbound.co.uk"=>[[[5855, "185.53.226.154"], 1]],
 "alexe@unbound.co.uk"=>[[[5856, "185.53.226.154"], 3]],
 "sophie@v-publishing.co.uk"=>[[[5858, "81.136.255.131"], 9]],
 "sofia@lundhumphries.com"=>[[[5859, "62.252.28.66"], 6]],
 "amanda.renwick@bdspublishing.com"=>
  [[[5860, "82.20.18.150"], 14], [[5860, "86.184.75.61"], 1]],
 "jurgens.vanniekerk@rpharms.com"=>[[[5861, "80.82.136.68"], 1]],
 "jon@v-publishing.co.uk"=>[[[5862, "81.136.255.131"], 1]],
 "alexander.white@ribapublishing.com"=>[[[5863, "78.146.237.8"], 1]],
 "daniel.culver@ribapublishing.com"=>[[[5864, "85.115.52.201"], 2]],
 "charmian.beedie@ribaenterprises.com"=>
  [[[5865, "212.44.32.134"], 3], [[5865, "92.234.36.71"], 1]],
 "rik.ubhi@zedbooks.net"=>
  [[[5884, "46.233.116.20"], 1], [[5884, "5.148.137.253"], 5]],
 "sara@bibliocloud.com"=>[[[5888, "81.135.84.252"], 2]],
 "david+leuven@bibliocloud.com"=>[[[5894, "81.135.84.252"], 4]],
 "e.burridge@liverpool.ac.uk"=>[[[5899, "138.253.53.204"], 8]],
 "l.whittick@liverpool.ac.uk"=>[[[5900, "138.253.53.136"], 2]],
 "annemie.vandezande@kuleuven.be"=>[[[5901, "134.58.253.56"], 7]],
 "veerle.delaet@kuleuven.be"=>[[[5902, "134.58.253.56"], 8]],
 "mary.simpson@rpharms.com"=>[[[5903, "80.82.136.68"], 7]],
 "amy.jordan@zedbooks.net"=>[[[5904, "5.148.137.253"], 12]],
 "gurdeep.singh@rpharms.com"=>[[[5907, "80.82.136.68"], 1]],
 "emily+inpress@bibliocloud.com"=>[[[5912, "2.30.135.226"], 1]],
 "emily+britishmuseum@bibliocloud.com"=>[[[5921, "81.135.84.252"], 1]],
 "mupton@britishmuseum.org"=>[[[5922, "85.115.54.202"], 3]],
 "sforster@britishmuseum.org"=>[[[5926, "85.115.54.202"], 4]],
 "efelton@liv.ac.uk"=>[[[5928, "138.253.53.211"], 6]],
 "beatrice.vaneeghem@kuleuven.be"=>[[[5929, "134.58.253.56"], 12]],
 "patricia.dicostanzo@kuleuven.be"=>[[[5930, "134.58.253.56"], 6]],
 "swegener@purdue.edu"=>
  [[[5934, "128.210.106.225"], 9], [[5934, "128.210.106.49"], 2]],
 "charlotte@unbound.co.uk"=>[[[5935, "185.53.226.154"], 1]],
 "laure@unbound.co.uk"=>[[[5937, "185.53.226.154"], 1]],
 "kimberlea.smith@sydney.edu.au"=>
  [[[5938, "129.78.56.133"], 3], [[5938, "129.78.56.139"], 1]],
 "ben@arcpublications.co.uk"=>[[[5943, "86.130.223.216"], 3]],
 "rosiejohns@serenbooks.com"=>[[[5944, "94.30.3.145"], 1]],
 "yen-yen@theemmapress.com"=>[[[5953, "90.194.78.71"], 2]]}


x = RequestLog.where(user_id: 5904).
   where(request: "/users/login").
   pluck(:created_at, :ip_address).
   sort

pp x
