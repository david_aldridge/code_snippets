class ProductCharge

  CHARGE_TIERS = [
    [ (1..49) , 0.04],
    [ (50..199) , 0.03],
    [ (200..999) , 0.02],
    [ (1000..4999) , 0.01],
    [ (5000..9999999) , 0.009]
  ]

  def initialize(product_count)
    @product_count = product_count
  end

  def total
    breakout.sum{|tier| tier[:charge]}
  end

  def breakout
    @breakout ||=
      CHARGE_TIERS.
      select{|range,_price| (0..product_count).overlaps?(range)}.
      map do |range,price|
        intersection = (range.min..[range.max,product_count].min).count
        {rate: price, qty: intersection, charge: price * intersection * 30}
      end
  end

  private

  attr_reader :product_count

end

__END__

ProductCharge.new(123).breakout
ProductCharge.new(1234).total
pp [1,10,50,100,250,500,1000,2500,5000,10000,100000].to_a.map{|n| [n,ProductCharge.new(n).total.round(2),(ProductCharge.new(n).total/n).round(2)] }



pp Client.select{|c| c.users.count > 3}.map{|client|[client.identifier,client.books.active.count, ProductCharge.new(client.books.active.count).total.round(2),  client.users.count*50  ]}.sort.select{|x| x[2] > x[3]}
