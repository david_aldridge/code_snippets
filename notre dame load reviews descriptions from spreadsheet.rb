spreadsheet = Roo::Spreadsheet.open("https://bibliocloudimages.s3-eu-west-1.amazonaws.com/temp/nd_season.xlsx")
spreadsheet = Roo::Spreadsheet.open("/Users/david/Documents/Bibliocloud/Clients/Notre Dame/From Notre Dame/Season Data.xlsx")

worksheet = spreadsheet.sheet(0)

client = Client.is_notre_dame
ActiveRecord::Base.transaction do
  worksheet.parse(headers: true)[1..-1].each do |row|
    isbn = row.fetch("ISBN13_1")&.gsub(/\D/, "")&.squish.presence
    next unless isbn

    raw_description = row.fetch("Description").presence
    raw_reviews     = row.fetch("Reviews").presence
    raw_blurbs      = row.fetch("Blurbs").presence

    next unless raw_description || raw_reviews || raw_blurbs

    work = client.works.joins(:books).find_by(books: {isbn: isbn})
    next unless work

    if work.reviews.none? && (raw_blurbs || raw_reviews)
      reviews = []
      reviews += raw_blurbs.split(/\n+/) if raw_blurbs
      reviews += raw_reviews.split(/\n+/) if raw_reviews

      reviews.each do |review|
        work.marketingtexts.review_quote.create(pull_quote: review)
      end
      work.books.each do |book|
        book.marketingtexts += work.marketingtexts.review_quote
      end
    end

    if work.descriptions.none? && raw_description
      description = raw_description.split("PULL QUOTE").first.rstrip
      if description.match?("\n")
        description = description.split("\n").map(&:strip).map(&:presence).compact.map{|t| "<p>#{t}</p>"}.join
      end
      work.marketingtexts.descriptions.create(pull_quote: description)
      work.books.each do |book|
        book.marketingtexts += work.marketingtexts.descriptions
      end
    end
  end ; 0

  # raise ActiveRecord::Rollback
end
