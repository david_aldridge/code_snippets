CustomReport.where(:modelname => 'Book').pluck(:selected_columns).flatten.compact.uniq.map(&:to_i).sort - BookConstants::REPORT_COLUMNS.keys



deletable_columns = [33, 37, 90, 169, 179, 241, 245].map(&:to_s)
CustomReport.where(:modelname => 'Book').where.not(:selected_columns => nil).select{|r| (r.selected_columns & deletable_columns).any?}.each do |r|
  r.selected_columns = r.selected_columns - deletable_columns
  r.save
end



