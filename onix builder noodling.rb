require "builder"

class ONIXProductBuilder
  def initialize(product:, options: {})
    @product = product
    @options = options
    @xml     = ::Builder::XmlMarkup.new(indent: 2)
  end

  def build(args)
    args.reduce(self) do |obj, method|
      obj.send(method)
    end
  end

  def record_reference
    RecordReferenceBuilder.
      new(product: @product, xml: @xml, options: {}).
      record_reference
    self
  #    notification_type.
  #    deletion_text.
  #    record_source_type.
  #    record_source_identifiers.
  #    record_source_name
  end

  def product_description
    ProductDescriptionBuilder.
      new(product: @product, xml: @xml, options: {}).
      product_form
    self
  #    product_parts.
  #    collection.
  #    product_title_detail.
  #    authorship.
  #    conference.
  #    edition.
  #    language.
  #    extents_and_other_content.
  #    subject.
  #    audience
  end

  def to_s
    @xml.target!
  end

  class RecordReferenceBuilder
    def initialize(product:, xml: , options: {})
      @product = product
      @options = options
      @xml     = xml || ::Builder::XmlMarkup.new(indent: 2)
    end

    def record_reference
      @xml.RecordReference "abc"
      self
    end
  end

  class ProductDescriptionBuilder
    def initialize(product:, xml: , options: {})
      @product = product
      @options = options
      @xml     = xml || ::Builder::XmlMarkup.new(indent: 2)
    end

    def product_form
      @xml.ProductForm @product.product_form
      self
    end
  end
end

ONIXProductBuilder.new(product: Book.first, options: {}).
  record_reference.
  product_description
  to_s

ONIXProductBuilder.new(product: Book.first, options: {}).
  build([:record_reference, :product_description, :to_s])


module ONIX
  module Director
    class Product
      def initialize(product: product)
        @product = product
      end

      def builder
        ONIXProductBuilder.new(product: @product, options: {})
      end

      def build
        builder.
          record_reference.
          product_description
      end

      delegate :to_s, to: :build
    end
  end
end
ONIXDirector.new(product: Book.first, options: {}).
  record_reference.
  product_description
  to_s
