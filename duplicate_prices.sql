select p1.book_id,
       p1.id,
       p1.currency_code,
       p1.price_amount
from   prices p1
where  exists (
         select 1
         from   prices p2
         where  p2.book_id = p1.book_id
           and  p2.id      > p1.id
           and  p2.price_type    = p1.price_type
           and  p2.price_amount  = p1.price_amount
           and  p2.tax_rate_code = p1.tax_rate_code
           and  p2.currency_code = p1.currency_code)
order by 1,2;


delete
from   prices p1
where  exists (
         select 1
         from   prices p2
         where  p2.book_id = p1.book_id
           and  p2.id      > p1.id
           and  p2.price_type    = p1.price_type
           and  p2.price_amount  = p1.price_amount
           and  p2.tax_rate_code = p1.tax_rate_code
           and  p2.currency_code = p1.currency_code);

select b.id,
       b.pub_date,
       b.default_price_amount,
       b.default_price_currency,
       b.default_price_id,
       (select count(*)
        from   prices p
        where  p.book_id = b.id
        and    p.currency_code = b.default_price_currency
        and    p.price_amount  = regexp_replace(b.default_price_amount,'[^0-9\.]','')::numeric)
from books b
order by pub_date asc;

update books b
   set default_price_id = (
         select p.id
           from prices p
          where p.book_id = b.id
            and p.currency_code = b.default_price_currency
            and p.price_amount  = regexp_replace(b.default_price_amount,'[^0-9\.]','')::numeric
          order by p.updated_at desc
          limit 1)
