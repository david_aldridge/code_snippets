ary = [{"book_id"=>"62236",
  "masterchannel_name"=>"Specials",
  "channel_name"=>"Author Sale",
  "royalty_rate_base_pct"=>"0.0",
  "royalty_rate_lower_limit"=>nil,
  "royalty_rate_upper_limit"=>nil,
  "price_basis"=>"List",
  "quantity_modifier_type"=>"None",
  "date_modifier_type"=>"None",
  "price_escalator_specifier"=>nil,
  "quantity_escalator_specifier"=>nil,
  "date_escalator_specifier"=>nil,
  "quantity_escalator_aggregator"=>nil},
 {"book_id"=>"62236",
  "masterchannel_name"=>"Trade",
  "channel_name"=>"Trade Sale",
  "royalty_rate_base_pct"=>"20.0",
  "royalty_rate_lower_limit"=>nil,
  "royalty_rate_upper_limit"=>nil,
  "price_basis"=>"Discount",
  "quantity_modifier_type"=>"None",
  "date_modifier_type"=>"None",
  "price_escalator_specifier"=>nil,
  "quantity_escalator_specifier"=>nil,
  "date_escalator_specifier"=>nil,
  "quantity_escalator_aggregator"=>nil},
  {"book_id"=>"62238",
  "masterchannel_name"=>"Trade",
  "channel_name"=>"Trade Sale",
  "royalty_rate_base_pct"=>"20.0",
  "royalty_rate_lower_limit"=>nil,
  "royalty_rate_upper_limit"=>nil,
  "price_basis"=>"Discount",
  "quantity_modifier_type"=>"None",
  "date_modifier_type"=>"None",
  "price_escalator_specifier"=>nil,
  "quantity_escalator_specifier"=>nil,
  "date_escalator_specifier"=>nil,
  "quantity_escalator_aggregator"=>nil}]

class MyHash < Hash
  include Hashie::Extensions::DeepMerge
end


keys = ary.map{|row| MyHash[{row["book_id"] => {row["masterchannel_name"] => {row["channel_name"] => row }}}]}

hsh = MyHash[{}]

keys.each do |x|
  hsh.deep_merge! x
end




  def rates_query_result
    ActiveRecord::Base.connection.execute(rates_query)
  end
