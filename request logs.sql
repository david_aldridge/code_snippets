select date_trunc('day', request_logs.created_at) "day",count(distinct user_id) users,count(*)
from   request_logs  join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
where  request not in ('/notices','/users/login') and
      clients.identifier = 'innova'
group by date_trunc('day', request_logs.created_at)
order by 1 desc;

select date_trunc('week', request_logs.created_at) "week",count(distinct user_id) users,count(*)
from   request_logs  join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
where  request not in ('/notices','/users/login') and
      clients.identifier = 'innova'
group by date_trunc('week', request_logs.created_at)
order by 1 desc;

select request_logs.created_at, users.email, request
from   request_logs  join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
where  request not in ('/notices','/users/login') and
      clients.identifier = 'innova'
order by 1 desc;



== requests by email and time for a client ==
select users.email, request_logs.created_at, substr(request,1,30)
from request_logs join users on users.id = request_logs.user_id
join clients c on c.id = users.client_id
where users.email not like '%@consonance.app'
order by 2 desc;

select users.email, request_logs.created_at, substr(request,1,30)
from request_logs join users on users.id = request_logs.user_id
join clients c on c.id = users.client_id
where users.email = 'tik@snowbooks.com'
order by 2 desc;



select request_logs.created_at, request
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.id = '5651'
order by 1 desc;

select request_logs.created_at, ip_address
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.email = 'rob.burleigh@bdspublishing.com'
order by 1 desc;

Common IP addresses per client
==============================

select ip_address, count(*)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.email like '%trigger%'
group by ip_address
order by 2;

Recent requests by IP Address
=============================

select request, created_at from request_logs where ip_address = '62.30.200.254' and
created_at >= current_date - interval '10 days'
order by created_at desc limit (200)



select request_logs.created_at, request
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.email = 'rob.davies@bl.uk'
order by 1 desc;


select ip_address,count(*), min(request_logs.created_at), max(request_logs.created_at)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.email = 'rob.burleigh@bdspublishing.com'
group by ip_address
order by 1 desc;

select
  date_trunc('day', request_logs.created_at),
  ip_address,
  count(*)
from request_logs left join users on users.id = request_logs.user_id
join clients c on c.id = users.client_id
where request not in ('/notices','/users/login') and
c.identifier= 'amber'
group by date_trunc('day', request_logs.created_at),
  ip_address
order by 1 desc;

select
  date_trunc('hour', request_logs.created_at),
  users.email,
  ip_address,
  count(*)
from request_logs left join users on users.id = request_logs.user_id
join clients c on c.id = users.client_id
where request not in ('/notices','/users/login') and
c.identifier= 'amber'
group by date_trunc('hour', request_logs.created_at),
  users.email,
  ip_address
order by 1 desc, 2;






where users.email not like '%@consonance.app'





select users.email, request_logs.created_at, substr(request,1,30)
from request_logs left join users on users.id = request_logs.user_id
where users.email like '%pharma%'
order by 2 desc;

select c.identifier, users.email, request_logs.created_at, substr(request,1,30)
from request_logs join users on users.id = request_logs.user_id
join clients c on c.id = users.client_id

order by 3 desc
limit 500;


select users.email, request_logs.created_at, substr(request,1,30)
from request_logs left join users on users.id = request_logs.user_id
where users.email like '%@britishmuseum.org%'
order by 2 desc;


select request_logs.created_at, ip_address
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login') and
users.email = 'rob.burleigh@bdspublishing.com'
order by 1 desc;


select email,count(*)
from   request_logs  join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
where  request not in ('/notices','/users/login') and
      clients.identifier = 'facet'
group by email
order by 1 desc;



select email,count(*)
from   versions  join
       users   on users.id::varchar   = versions.whodunnit join
       clients on clients.id = users.client_id
where  clients.identifier = 'facet' and
       users.email not like '%bibliocloud.com'
group by email
order by 1 desc;





select date_trunc('day', request_logs.created_at) "day",  users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email like '%@canelo%'
group by date_trunc('day', request_logs.created_at), users.email
order by 1 desc,2;




select users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where email like '%miles%' and request_logs.created_at >= current_date - interval '2 month'
group by users.email
order by 1,2;

email, created at, request

select users.email, request_logs.created_at, substr(request,1,30)
from request_logs left join users on users.id = request_logs.user_id
where email like '%miles%' and request_logs.created_at >= current_date - interval '2 month'
order by 1,2 desc;


select clients.identifier,
       date_trunc('day', request_logs.created_at) "day",
       count(*),
       count(distinct users.id)
from   request_logs
join   users on users.id = request_logs.user_id
join   clients on clients.id = users.client_id
where  request not in ('/notices','/users/login')
and request_logs.created_at >= current_date - 60
group by clients.identifier, date_trunc('day', request_logs.created_at)
order by 1,2;


select clients.identifier,
       count(*),
       count(distinct users.id)
from   request_logs
join   users on users.id = request_logs.user_id
join   clients on clients.id = users.client_id
where  request not in ('/notices','/users/login')
and request_logs.created_at >= current_date - 60
and users.email not like '%bibliocloud.com'
group by clients.identifier
order by 1,2;



email, day, count

select users.email, date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login')
group by users.email, date_trunc('day', request_logs.created_at)
order by 1,2;

day, email, count

select date_trunc('day', request_logs.created_at) "day",  users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
group by date_trunc('day', request_logs.created_at), users.email
order by 1,2;

month, email, count

select date_trunc('month', request_logs.created_at) "month",  users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
group by date_trunc('month', request_logs.created_at), users.email
order by 1,2;

month, email, count

select date_trunc('week', request_logs.created_at) "month",  count(distinct users.email), count(*)
from request_logs left join users on users.id = request_logs.user_id
where  users.email like '%canel%'
group by date_trunc('week', request_logs.created_at)
order by 1,2;

day, email, count

select date_trunc('day', request_logs.created_at) "month",  count(distinct users.email), count(*)
from request_logs left join users on users.id = request_logs.user_id
where  users.email like '%miles%'
group by date_trunc('day', request_logs.created_at)
order by 1,2;

email, month, ip_address
select users.email, date_trunc('month', request_logs.created_at) "month", ip_address,  count(*)
from request_logs left join users on users.id = request_logs.user_id
group by date_trunc('month', request_logs.created_at), users.email, ip_address
order by 1,2,3;

select users.email, date_trunc('month', request_logs.created_at) "month", ip_address,  count(*)
from request_logs join users on users.id = request_logs.user_id
where users.email like '%zed%'
group by date_trunc('month', request_logs.created_at), users.email, ip_address
order by 1,2,3;

select users.email, date_trunc('month', request_logs.created_at) "month",  count(*)
from request_logs join users on users.id = request_logs.user_id
where users.email like '%zed%'
group by date_trunc('month', request_logs.created_at), users.email
order by 1,2;

day, count

select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login','/')
group by date_trunc('day', request_logs.created_at)
order by 1;

select date_trunc('day', request_logs.created_at) "day",
       count(*),
       count(distinct ip_address),
       count(distinct email)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login','/') and
       users.client_id = 385
group by date_trunc('day', request_logs.created_at)
order by 1;


select date_trunc('day', request_logs.created_at) "day",
       count(*),
       count(distinct ip_address),
       count(distinct email)
from request_logs left join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login','/') and
       users.client_id = 388 -- unbound
group by date_trunc('day', request_logs.created_at)
order by 1;





select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where request like '%direct%'
group by date_trunc('day', request_logs.created_at)
order by 1;

select  ip_address, left(request,30)
from request_logs join users on users.id = request_logs.user_id
where request not in ('/notices','/users/login','/')
order by 1;

select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'emily.badger@watkinspublishing.com'
group by date_trunc('day', request_logs.created_at)
order by 1;

select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email like '%oberon%'
group by date_trunc('day', request_logs.created_at)
order by 1;


select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'timmoore@garnetpublishing.co.uk'
group by date_trunc('day', request_logs.created_at)
order by 1;

select date_trunc('hour', request_logs.created_at) "hour", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'timmoore@garnetpublishing.co.uk'
group by date_trunc('hour', request_logs.created_at)
order by 1;


select request_logs.created_at, request_logs.created_at - lag(request_logs.created_at) over (partition by users.email order by request_logs.created_at),
        substr(request,1,50)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'timmoore@garnetpublishing.co.uk'
and date_trunc('day', request_logs.created_at) = date '2016-05-24'
order by 1;


select request_logs.created_at, request_logs.created_at - lag(request_logs.created_at) over (partition by users.email order by request_logs.created_at),
        substr(request,1,50)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'timmoore@garnetpublishing.co.uk'
and date_trunc('day', request_logs.created_at) = date '2016-03-07'
order by 1;


email count

select users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
group by users.email
order by 1;


select users.email, count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
group by users.email
order by 1;





select date_trunc('hour', request_logs.created_at) "hour", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'amanda@mileskelly.net'
group by date_trunc('hour', request_logs.created_at)
order by 1;


select date_trunc('month', request_logs.created_at) "day", to_char(request_logs.created_at, 'day'), count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'amanda@mileskelly.net'
group by date_trunc('month', request_logs.created_at),to_char(request_logs.created_at, 'day')
order by 1;


select request_logs.created_at, to_char(request_logs.created_at, 'day'), ip_address
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'amanda@mileskelly.net'
and request_logs.created_at >= date '2017-01-01'
order by 1;









select date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where users.email = 'emily.badger@watkinspublishing.com' and request = '/users/login'
group by date_trunc('day', request_logs.created_at)
order by 1;

select coalesce(users.email,users.id::varchar) ,date_trunc('day', request_logs.created_at) "day", count(*), count(distinct ip_address)
from request_logs left join users on users.id = request_logs.user_id
where request_logs.created_at >= date '2015-12-08'
group by coalesce(users.email,users.id::varchar),date_trunc('day', request_logs.created_at)
order by 1;


select coalesce(users.email,users.id::varchar) ,date_trunc('hour', request_logs.created_at) "hour", ip_address
from request_logs left join users on users.id = request_logs.user_id
 where request_logs.created_at >= date '2015-12-08'
order by 1,2;

select date_trunc('hour', request_logs.created_at) "hour",coalesce(users.email,users.id::varchar) , ip_address
from request_logs left join users on users.id = request_logs.user_id
 where request_logs.created_at >= date '2015-12-08'
order by 1,2;

select date_trunc('hour', request_logs.created_at) "hour", coalesce(users.email,users.id::varchar) , ip_address, request
from request_logs left join users on users.id = request_logs.user_id
 where request_logs.created_at >= date '2015-12-08'
order by 1,2;


select coalesce(users.email,users.id::varchar) ,date_trunc('hour', request_logs.created_at) "hour", ip_address
from request_logs left join users on users.id = request_logs.user_id
 where request_logs.created_at >= date '2015-12-08' and
      ip_address = '5.148.128.6'
order by 1,2;



select distinct users.email
from request_logs join users on users.id = request_logs.user_id
where request_logs.created_at >=  date '2015-07-01' and
users.email not like '%+%'
order by 1;


Direct search?
==============

select date_trunc('month', request_logs.created_at) "month",  count(*)
from request_logs join users on users.id = request_logs.user_id
where users.email like '%zed%' and
      request like '/books/direct_search.json?q=%'
group by date_trunc('month', request_logs.created_at)
order by 1;

select date_trunc('month', request_logs.created_at) "month",  count(*)
from request_logs join users on users.id = request_logs.user_id
where users.email like '%zed%' and
      request like '/books/index%'
group by date_trunc('month', request_logs.created_at)
order by 1;




select date_trunc('month', request_logs.created_at) "month",  count(*)
from request_logs join users on users.id = request_logs.user_id
where request like '/books/direct_search.json?q=%'
group by date_trunc('month', request_logs.created_at)
order by 1;

select date_trunc('month', request_logs.created_at) "month",  count(*)
from request_logs join users on users.id = request_logs.user_id
where request like '/books/index%'
group by date_trunc('month', request_logs.created_at)
order by 1;



========== Client stuff ============


select clients.id,
       clients.webname,
       count(*),
       count(distinct ip_address),
       max(request_logs.created_at)
from   request_logs join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
group by clients.id
order by 1;


select clients.id,
       clients.webname,
       users.id,
       users.email,
       count(*),
       max(request_logs.created_at)
from   request_logs right join
       users on users.id = request_logs.user_id join
       clients on clients.id = users.client_id
where  users.email not like '%@bibliocloud.com' and
       users.created_at <= date '2016-01-01'
group by clients.id, users.id
having coalesce(max(request_logs.created_at), date '2015-10-01') <= date '2015-10-01'
order by 1,3;





User.where.not(RequestLog.where("users.id = request_logs.user_id").exists).
     where("users.created_at <= ?", Date.civil(2016,1,1)).
