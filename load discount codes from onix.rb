

client          = Client.is_mcgill_queens
books           = client.books
supplier_config = client.supplier_configs.find(380)
doc             = Nokogiri::XML(open("https://bibliocloudimages.s3-eu-west-1.amazonaws.com/clients/McGill_Queens_ref.xml")) ; 0

supplier_config.update(
  discount_code_type:      "02",
  discount_code_type_name: nil,
  discount_codes:          %w[UMA UMS UMT]
)

Work.no_touching do
  ActiveRecord::Base.transaction do
    doc.xpath('/ONIXMessage/Product').each do |product_node|
      isbn = product_node.at_xpath('ProductIdentifier[ProductIDType=03]/IDValue')&.text
      raise "No ISBN for #{product_node.at_xpath('RecordReference')&.text}" unless isbn

      discount_code = product_node.at_xpath('SupplyDetail/Price/DiscountCoded/DiscountCode')&.text
      next unless discount_code

      book = books.find_by(isbn: isbn)
      next unless book

      book_supplier_config = supplier_config.book_supplier_configs.where(
        book_id: book.id
      ).first_or_initialize

      book_supplier_config.discount_code = discount_code
      book_supplier_config.save
    end
  end
end ; 0

client.book_supplier_configs.group(:discount_code).count
