Client.
  is_mcgill_queens.
  contacts.
  is_nielsen.
  export_batches.
  last.
  books.
  map{|b| [b.imprint&.to_s, b.isbn_registrant_hyphenated]}.
  group_by(&:first).
  transform_values{|v| v.map(&:last).uniq.sort}



McGill-Queen's University Press
978-0-2280
978-0-7735

Acumen Publishing
978-0-7735

John Deutsch Institute for the Study of Economic Policy
978-0-7735

McCord Museum
978-1-895615

IPI Press
978-0-9829155
978-0-9961938

CIGI Press
978-1-928096

Editions Acadie
978-0-7735

Defence Management Series
978-1-55339

Queen's School, Policy-Metropolis
978-1-55339

McGill University Libraries
978-0-7735
978-2-89448

Les éditions du Septentrion
978-0-7735
978-2-89448

Institute of Intergovernmental Relations
978-1-55339

Queen's School of Policy Studies
978-1-55339

Institute for Research on Public Policy
978-0-7735
978-0-88645
