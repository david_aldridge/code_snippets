
isbns = %w(
9781781380475
9781781380635
9781781380819
9781786948083
9781786948090
9781846314513
9781846316760
9781846317002
9781846317101
9781846317279
9781846317316
9781846317330
9781846317385
9781846317392
9781846317408
9781846317934
9781846317958
9781846317965
9781846317972
9781846317989
9781846317996
9781846318009
9781846318016
9781846318023
9781846318030
9781846318047
9781846318061
9781846319969
9781846319976
9781846319983
9781942954330
9781942954415
)

urls =
  Client.is_liverpool.
    books.
    where(isbn: isbns).
    select { |b| b.allow_onix_exports }.
    map { |book| [book.isbn, book.production_files.last.try(:attachment).try(:url)] }.
    reject { |x| x.last.nil? }.
    map {|isbn, url| "curl #{url} --output #{isbn}.pdf" }

pp urls

curl https://s3-eu-west-1.amazonaws.com/bibliocloudimages/production_files/137/9781846314513_original.pdf --output 9781846314513.pdf
curl https://s3-eu-west-1.amazonaws.com/bibliocloudimages/production_files/1127/9781786948083_original.pdf --output 9781786948083.pdf
curl https://s3-eu-west-1.amazonaws.com/bibliocloudimages/production_files/816/9781786948090_original.pdf --output 9781786948090.pdf
curl https://s3-eu-west-1.amazonaws.com/bibliocloudimages/production_files/815/9781942954330_original.pdf --output 9781942954330.pdf
