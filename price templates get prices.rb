Client.first.works.map do |w|
  w.books.sort_by{|b| b.product_form || "x"}.map do |b|
    [b.product_form, b.prices.map{|p| [p.price_type, p.currency_code, p.price_amount.to_f]}]
  end
end


 Client.first.prices.map{|p| [p.price_type, p.onix_price_type_qualifier_id, p.currency_code, p.price_amount.to_f]}.group_by{|a| a}