problem_book_ids = []
problem_titles = Book.where("full_title like '%'||E'\n'||'%'")
problem_books_id = problem_titles.pluck(:id)
problem_titles.each{|b| b.update(full_title: b.full_title.squish.gsub(/\p{Co}/, ""))}

problem_titles_wop = Book.where("title like '%'||E'\n'||'%'")
problem_books_ids = problem_titles_wop.pluck(:id)
problem_titles_wop.each{|b| b.update(title: b.title.squish.gsub(/\p{Co}/, ""))}


problem_subtitles = Book.where("subtitle like '%'||E'\n'||'%'")
problem_book_ids += problem_subtitles.pluck(:id)
problem_subtitles.each{|b| b.update(subtitle: b.subtitle.squish.gsub(/\p{Co}/, ""))}

Work.where(edition_number: 0).count
Book.where("subtitle like '%'||E'\n'||'%'").each{|b| b.update(subtitle: b.subtitle.squish.gsub(/\p{Co}/, ""))}

book_ids = Book::Asset.has_asset_errors.select{|a| "PriceAmount".in? a.asset_errors.first}.pluck(:book_id)
Book::Asset.has_asset_errors.select{|a| "EditionNumber".in? a.asset_errors.first}.map(&:process)