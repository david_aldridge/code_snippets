ProductionFile.joins(:books).
  merge(Book.is_print).
  where("attachment_file_name like '%CVR%'").
  pluck(:attachment_file_name, :file_type).
  map{|name,type| [name.gsub(/^\d*/,""),type] }.
  group_by(&:itself).
  transform_values(&:size).
  sort_by(&:last)



ProductionFile.joins(:books).merge(Book.is_print).where("attachment_file_name like '%CVR%'").update_all(file_type: "PDF cover - final")

ProductionFile.joins(:books).merge(Book.is_print).
  where.not("attachment_file_name ilike '%CVR%'").
  where("attachment_file_name ilike '%print%'").
  where("attachment_file_name ilike '%PDF'").pluck(:file_type,:attachment_file_name).sort

ProductionFile.joins(:books).merge(Book.is_print).
  where.not("attachment_file_name ilike '%CVR%'").
  where("attachment_file_name ilike '%print%'").
  where("attachment_file_name ilike '%PDF'").update_all(file_type: "PDF interior layout - final")




ProductionFile.pluck(:file_type).uniq
