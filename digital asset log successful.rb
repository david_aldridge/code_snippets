pp Client.is_liverpool.
          digital_assets.
          order(:id => :desc).
          limit(9).
          flat_map{|x| x.contact_digital_asset_transfers.last}.
          compact.
          map{|x| x.transfer_loggable.logs.last.response[0..2]}