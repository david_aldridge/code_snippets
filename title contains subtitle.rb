pp Client.is_purdue.books.where.not(subtitle: nil).select{|w| w.title.upcase.squish.ends_with?(w.subtitle.upcase.squish) }.map{|b| [b.isbn, b.pub_date, b.title] }


[["9781612495491",
  Sat, 15 Sep 2018,
  "Quest for Redemption: Central European Jewish Thought in Joseph Roth's Works"],
 ["9781612495507",
  Sat, 15 Sep 2018,
  "Quest for Redemption: Central European Jewish Thought in Joseph Roth's Works"],
 ["9781557538239",
  Wed, 15 Aug 2018,
  "Intellectual Philanthropy: The Seduction of the Masses"],
 ["9781612493176",
  Mon, 30 Sep 2013,
  "Life Constructed:Reflections on Breaking Barriers and Building Opportunities"],
 ["9781612493183",
  Mon, 30 Sep 2013,
  "Life Constructed:Reflections on Breaking Barriers and Building Opportunities"],
 ["9781612495453",
  Wed, 15 Aug 2018,
  "Intellectual Philanthropy: The Seduction of the Masses"],
 ["9781612495460",
  Wed, 15 Aug 2018,
  "Intellectual Philanthropy: The Seduction of the Masses"],
 ["9781557538307",
  Sat, 15 Sep 2018,
  "Quest for Redemption: Central European Jewish Thought in Joseph Roth's Works"]]

