Subject.
  joins(:work).
  where(
    works: {
      client_id: Client.is_lund.id
      }
    ).
  order(:subject_scheme_identifier).
  group(:subject_scheme_identifier).
  count
