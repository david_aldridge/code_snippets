google_drive_key = '1wOEvcXDJlz38UnUSaxIEMXxu4oKruWh9auucEtdPAvE'
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
connection = Google::DriveConn.new
sheet      = connection.session.spreadsheet_by_key(google_drive_key)
worksheet  = sheet.worksheet_by_title("Sheet 1")
list       = worksheet.list

title_corrections          = Hash[*list.reject{|row| row['Corrected Title'].strip.blank?}.map{|row| [row['ISBN'].delete("-").strip, row['Corrected Title'] ]}.flatten]
edition_number_corrections = Hash[*list.reject{|row| row['Corrected edition number'].strip.blank?}.map{|row| [row['ISBN'].delete("-").strip, row['Corrected edition number'] ]}.flatten]
series_corrections         = Hash[*list.reject{|row| row['Corrected Series'].strip.blank?}.map{|row| [row['ISBN'].delete("-").strip, {:name => row['Corrected Series'].presence, :number => row['Corrected Series Number'].presence, :year => row['Series Year of Annual'].presence}]}.flatten]
yoa_corrections            = Hash[*list.reject{|row| row['Work Year of Annual'].strip.blank?}.map{|row| [row['ISBN'].delete("-").strip, :year => row['Work Year of Annual'].presence]}.flatten]


file_name = '/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/NielsenRecordSupply.xml'

doc = Nokogiri::XML(open(file_name), nil) do |config|
  config.default_xml.noblanks
end ; 0

doc.xpath('/ONIXMessage/Product/Title').count

doc.xpath('/ONIXMessage/Product/Title/TitleText').count

doc.xpath('/ONIXMessage/Product/Title/TitlePrefix').each{|x| x.remove}
doc.xpath('/ONIXMessage/Product/Title/TitleWithoutPrefix').each{|x| x.remove}



doc.xpath('/ONIXMessage/Product/Contributor').each do |contributor|
  next unless contributor.at_xpath('PersonName').try(:text) == contributor.at_xpath('PersonNameInverted').try(:text)
  puts contributor.at_xpath('PersonName').text
  new_node         = Nokogiri::XML::Node.new "CorporateName", doc
  new_node.content = contributor.at_xpath('PersonName').text
  contributor.at_xpath('PersonName').add_next_sibling new_node
  contributor.at_xpath('PersonName').try(:remove)
  contributor.at_xpath('PersonNameInverted').try(:remove)
  contributor.at_xpath('KeyNames').try(:remove)
end ; 0


substitute_names = {"Royal Pharmaceutical Society of Great Britain" => "Royal Pharmaceutical Society",
                    "B M A" => "British Medical Association"}


doc.xpath('/ONIXMessage/Product/Contributor/CorporateName').each do |contributor|
  next unless contributor.text.in? substitute_names.keys
  puts "#{contributor.text} changing to #{substitute_names[contributor.text]}"
  contributor.content = substitute_names[contributor.text]
end ; 0

substitute_titles = {"Hospital Preregistration Pharmacist Training" => "Hospital Pre-Registration Pharmacist Training"}

doc.xpath('/ONIXMessage/Product/Title/TitleText').each do |title|
  next unless title.text.in? substitute_titles.keys
  puts "#{title.text} changing to #{substitute_titles[title.text]}"
  title.content = substitute_titles[title.text]
end ; 0

substitute_series = {"9780853699682" => {name: "Hospital Pre-Registration Pharmacist Training", year_of_annual: '2012-2013', number_within_series: '3'}}

doc.xpath('/ONIXMessage/Product').each do |product|
  isbn_node   = product.at_xpath('ProductIdentifier[ProductIDType=15]/IDValue')
  puts isbn_node.text
  series_data = substitute_series[isbn_node.text]
  next unless series_data
  puts series_data
  product.xpath('Series').each{|series| series.remove}

  series_node         = Nokogiri::XML::Node.new "Series", doc
  product.at_xpath("Title").add_previous_sibling series_node

  series_title_node = Nokogiri::XML::Node.new "Title", doc
  series_node.add_child series_title_node

  series_title_type_node = Nokogiri::XML::Node.new "TitleType", doc
  series_title_type_node.content = '01'
  series_title_node.add_child series_title_type_node

  series_title_text_node   = Nokogiri::XML::Node.new "TitleText", doc
  series_title_text_node.content = series_data[:name]
  series_title_type_node.add_next_sibling series_title_text_node

  if series_data[:number_within_series]
    number_within_series_node   = Nokogiri::XML::Node.new "NumberWithinSeries", doc
    number_within_series_node.content = series_data[:number_within_series]
    series_title_node.add_next_sibling number_within_series_node
  end

  if series_data[:year_of_annual]
    year_of_annual_node   = Nokogiri::XML::Node.new "YearOfAnnual", doc
    year_of_annual_node.content = series_data[:year_of_annual]
    (number_within_series_node || series_title_node).add_next_sibling year_of_annual_node
  end
end ; 0

title_corrections.keys.each do |isbn|
  puts isbn
  product_node   = doc.at_xpath("ONIXMessage/Product[RecordReference=#{isbn}]")
  puts old_title = product_node.at_xpath('Title/TitleText').text
  puts new_title = title_corrections[isbn]
  product_node.at_xpath('Title/TitleText').content = title_corrections[isbn]
end

edition_number_corrections.keys.each do |isbn|
  puts isbn
  product_node   = doc.at_xpath("ONIXMessage/Product[RecordReference=#{isbn}]")
  product_node.at_xpath('EditionNumber').try(:remove)
  edition_number_node = Nokogiri::XML::Node.new "EditionNumber", doc
  edition_number_node.content = edition_number_corrections[isbn]
  product_node.add_child edition_number_node
end

series_fragment = Nokogiri::XML::fragment("<Series>
<Title>
<TitleType>01</TitleType>
<TitleText>Some title</TitleText>
</Title>
</Series>
")


doc.xpath("ONIXMessage/Product/Series").each(&:remove)

series_corrections.keys.each do |isbn|
  puts isbn
  product_node       = doc.at_xpath("ONIXMessage/Product[RecordReference=#{isbn}]")
  series_node        = Nokogiri::XML::Node.new "Series", doc
  title_node         = Nokogiri::XML::Node.new "Title", doc
  title_type         = Nokogiri::XML::Node.new "TitleType", doc
  title_type.content = '01'
  title_text         = Nokogiri::XML::Node.new "TitleText", doc
  title_text.content = series_corrections[isbn][:name]

  series_node.add_child       title_node
  title_node.add_child        title_type
  title_type.add_next_sibling title_text

  if yoa = series_corrections[isbn][:year]
    yoa_node         = Nokogiri::XML::Node.new "YearOfAnnual", doc
    yoa_node.content = yoa
    title_node.add_next_sibling yoa_node
  end

  if number = series_corrections[isbn][:number]
    number_node         = Nokogiri::XML::Node.new "NumberWithinSeries", doc
    number_node.content = number
    title_node.add_next_sibling number_node
  end
  product_node.add_child series_node

end

yoa_corrections.keys.each do |isbn|
  puts isbn
  product_node       = doc.at_xpath("ONIXMessage/Product[RecordReference=#{isbn}]")
  yoa_node           = Nokogiri::XML::Node.new "YearOfAnnual", doc
  yoa_node.content   = yoa_corrections[isbn][:year]

  product_node.add_child  yoa_node
end






File.write('/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/NielsenRecordSupply out.xml', doc.to_xml(:indent_text => " ", :indent => 2))
