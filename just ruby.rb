class Book

  def initialize(options = {})
    raise "No price supplied" unless options[:price]
    @title = options[:title]
    @price = options[:price]
  end

  def title=(str)
    my_str = str
    my_str = my_str.strip.downcase
    @title = my_str
  end

  def title
    @title
  end

  def has_title?
    !@title.nil?
  end

  def to_s
    "#{@title} #{@price}"
  end

  def price=(num)
    @price = num
  end

  def price
    @price
  end

end



b = Book.new
b.has_title?
b.title="War & peas"
b.has_title?
b.title
b.price=3.99
b.to_s
