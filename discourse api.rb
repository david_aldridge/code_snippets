
client.invite_user(email: "david@snowbooks.com", group_ids: "trust_level_0,trust_level_1")

module DiscourseApi
  module API
    module Users
      def list_users(type)
        response = get("admin/users/list/#{type}.json?show_emails=true")
        response[:body]
      end
    end
  end
end

module Discourse
  def self.client
    @client ||= begin
      DiscourseApi::Client.new("https://community.consonance.app").tap do |client|
        client.api_key = "cb8a7c64292969c77be19582767ffb47ce3593f7b426febc9f8dd5f0c9e3420b"
        client.api_username = "DavidA"
      end
    end
  end

  def self.latest_topics
    client.latest_topics.map{|u| OpenStruct.new(u)}
  end

  def self.active_users
    client.list_users("active").map{|u| OpenStruct.new(u)}
  end
end



discourse_emails = Discourse.active_users.map(&:email)


Client.order(id: :desc).limit(20).map{|x| [x.id, x.identifier, x.status]}

User.joins(:client).
  order(:client_id).
  where(RequestLog.where("request_logs.user_id = users.id").where("created_at >= ?", Date.today - 2.months).exists).
  merge(Client.active).
  not_support.
  not_dormant.
  each{|u| puts u.email} ; nil



discourse_emails - consonance_emails
consonance_emails - discourse_emails









class User

  def discourse
    @discourse ||= Discourse.new(self)
  end

  def invite_to_discourse
    discourse.invite!
  end
end

class User
  class Discourse
    def initialize(user)
      @user = user
    end

    def invite!
      client.invite_user(email: email, group_ids: "trust_level_0,trust_level_1")
    end

    private

    def discourse
      @discourse ||= begin
        DiscourseApi::Client.new("http://community.consonance.app").tap do |client|
          client.api_key = "6f7f2b235104944a05e4aabacb19fa6c2440ffc09efe21de910bb924d3429cb3"
          client.api_username = "DavidA"
        end
      end
    end

    delegate :email, to: :user

    attr_reader :user
  end
end
