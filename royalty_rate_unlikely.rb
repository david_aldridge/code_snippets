select
  c.id client_id,
  b.product_form,
  count(*),
  min(rs.royalty_rate_base_pct),
  max(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct),
  stddev(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct) - stddev(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct) + stddev(rs.royalty_rate_base_pct)
from
  clients c join
  books b on c.id = b.client_id join royalty_specifiers rs
    on royaltyable_id = b.id and royaltyable_type = 'Book'
    where c.status = 1
group by
c.id,
b.product_form
order by 1,2;



select
  c.id client_id,
  b.product_form,
  count(*),
  min(rs.royalty_rate_base_pct),
  max(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct),
  stddev(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct) - stddev(rs.royalty_rate_base_pct),
  avg(rs.royalty_rate_base_pct) + stddev(rs.royalty_rate_base_pct)
from
  clients c join
  books b on c.id = b.client_id join royalty_specifiers rs
    on royaltyable_id = b.id and royaltyable_type = 'Book'
    where c.status = 1
group by
c.id,
b.product_form
having min(rs.royalty_rate_base_pct) < avg(rs.royalty_rate_base_pct) - stddev(rs.royalty_rate_base_pct)
or
 max(rs.royalty_rate_base_pct) > avg(rs.royalty_rate_base_pct) + stddev(rs.royalty_rate_base_pct)
order by 1,2;
