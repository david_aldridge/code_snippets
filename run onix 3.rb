product       = Book.find(154543)
template      = product.client.contacts.is_ingram.contact_digital_asset_transfer_templates.first
configuration = ONIXWrite.default_configuration
onix = ONIXWrite::V3::Product.new(
  product:       product,
  version:       SemanticVersion.new("3.0.5"),
  configuration: configuration,
  template:      template
)

onix.call
