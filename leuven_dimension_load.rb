connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1kvFzBRBwUjBNbYXER5enPlEaD7kej4CuDKk6LRHEOw0")
metadata_sheet = sheet.worksheets.first
metadata       = metadata_sheet.list
client         = Client.is_leuven

class LoadLeuvenRow
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    return unless book
    return unless series_name_text
    return if series_name.in? work.seriesnames
    series_name.works << work
    return unless number_in_series
    work.work_seriesnames.first.update_attributes(number_within_series: number_in_series)
  end

  private

  def isbn
    row["isbn"].squish
  end

  def book
    @book ||= client.books.find_by(isbn: isbn)
  end

  def work
    @work ||= book.work
  end

  def series_name_text
    row["series"].squish.presence
  end

  def series_name
    @seriesname ||=
      client.
      seriesnames.
      where(title_without_prefix: series_name_text).
      first_or_create
  end

  def number_in_series
    row["series vol"].squish.presence
  end

  attr_reader :row, :client
end

ActiveRecord::Base.transaction do
  metadata.each do |row|
    LoadLeuvenRow.new(row, client).call
  end
end
