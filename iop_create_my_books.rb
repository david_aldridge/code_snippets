Book.where("created_at >= ?", Date.today - 1.days).destroy_all

Book.joins(:proprietary_format_description).where(proprietary_format_descriptions: {identifier: "my-book"}).map(&:ancillary_content)

ProprietaryFormatDescription.pluck(:identifier).compact
ProprietaryEditionDescription.pluck(:identifier).compact

ActiveRecord::Base.transaction do
  Work.includes(:client).each do |work|
    print_book = work.books.where(product_form: %w(BB BC)).order(:pub_date).take
    next unless print_book

    my_book                                   = DuplicateBookService.new(old_book: print_book).call
    primary_product                           = InstituteOfPhysics::PrimaryProduct.call(work)
    if primary_product
      my_book.pub_date                          = primary_product.pub_date
      my_book.identifying_doi                   = primary_product.identifying_doi
      my_book.publishing_status                 = primary_product.publishing_status
    end
    my_book.publishers_reference              = nil
    my_book.page_trim_height_mm               = nil
    my_book.page_trim_width_mm                = nil
    my_book.product_width_mm                  = nil
    my_book.product_height_mm                 = nil
    my_book.product_thickness_mm              = nil
    my_book.unit_weight_gr                    = nil
    my_book.save
    my_book.supportingresources               = print_book.supportingresources
    my_book.current_gbp_exctax_consumer_price = 25
    my_book.current_usd_exctax_consumer_price = 30
    my_book.current_eur_exctax_consumer_price = nil
    my_book.current_cad_exctax_consumer_price = nil
    my_book.current_aud_exctax_consumer_price = nil
    my_book.proprietary_edition_description   = work.client.proprietary_edition_descriptions.is_my_book
    my_book.proprietary_format_description    = work.client.proprietary_format_descriptions.is_my_book
    my_book.save
  end
end ; 0




ActiveRecord::Base.transaction do
  Book.where(isbn: ["9781681747057", "9781681745169", "9781681746975", "9781681745763", "9781681747132", "9781681746654", "9781681747019", "9781681743882", "9781681745602", "9781681742809", "9781681745725", "9781681740188", "9781681746524", "9780750317870", "9781681746890"]).
  map(&:work).each do |work|
    print_book = work.books.where(product_form: %w(BB BC)).order(:pub_date).take
    next unless print_book

    my_book                                   = DuplicateBookService.new(old_book: print_book).call
    primary_product                           = InstituteOfPhysics::PrimaryProduct.call(work)
    if primary_product
      my_book.pub_date                          = primary_product.pub_date
      my_book.identifying_doi                   = primary_product.identifying_doi
      my_book.publishing_status                 = primary_product.publishing_status
    end
    my_book.publishers_reference              = nil
    my_book.page_trim_height_mm               = nil
    my_book.page_trim_width_mm                = nil
    my_book.product_width_mm                  = nil
    my_book.product_height_mm                 = nil
    my_book.product_thickness_mm              = nil
    my_book.unit_weight_gr                    = nil
    my_book.save
    my_book.supportingresources               = print_book.supportingresources
    my_book.current_gbp_exctax_consumer_price = 25
    my_book.current_usd_exctax_consumer_price = 30
    my_book.current_eur_exctax_consumer_price = nil
    my_book.current_cad_exctax_consumer_price = nil
    my_book.current_aud_exctax_consumer_price = nil
    my_book.proprietary_edition_description   = work.client.proprietary_edition_descriptions.is_my_book
    my_book.proprietary_format_description    = work.client.proprietary_format_descriptions.is_my_book
    my_book.save
  end
end ; 0
