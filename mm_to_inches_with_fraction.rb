def mm_to_inches_fraction(mm)
  inches = mm / 2.54
  def greatest_common_divisor(a, b)
     while a%b != 0
         a,b = b.round,(a%b).round
       end
     return b
  end
  sixteenths  = (16*(inches - inches.floor)).round
  unle
  gcd         = greatest_common_divisor(sixteenths,16)
  numerator   = sixteenths / gcd
  denominator = 16  / gcd
  if numerator == denominator
    (inches.floor + 1).to_s
  else
    "#{inches.floor} #{numerator}/#{denominator}"
  end
end

mm_to_inches_fraction(123)

mm_to_inches_fraction(130)
mm_to_inches_fraction(254)
mm_to_inches_fraction(245)
mm_to_inches_fraction(123)

> (0.25*16)
 => 4.0
2.2.3 :002 > (0.25*16).round
 => 4
2.2.3 :003 > (0.25*16).round/16
 => 0
2.2.3 :004 > (0.25*16).round/16.0
 => 0.25
2.2.3 :005 > (0.25*16).round
 => 4
2.2.3 :006 >   def greatest_common_divisor(a, b)
2.2.3 :007?>        while a%b != 0
2.2.3 :008?>            a,b = b.round,(a%b).round
2.2.3 :009?>          end
2.2.3 :010?>        return b
2.2.3 :011?>     end
 => :greatest_common_divisor
2.2.3 :012 > greatest_common_divisor(4/16)
ArgumentError: wrong number of arguments (1 for 2)
	from (irb):6:in `greatest_common_divisor'
	from (irb):12
	from /Users/david/.rvm/rubies/ruby-2.2.3/bin/irb:15:in `<main>'
2.2.3 :013 > greatest_common_divisor(4,16)
 => 4
2.2.3 :014 > greatest_common_divisor(4,16)
