heroku pg:backups:capture -a bibliocloud-iop
heroku pg:backups:capture -a bibliocloud-app

curl -o ~/Downloads/bibliocloud-iop.dump `heroku pg:backups:url b852 -q -a bibliocloud-iop`
curl -o ~/Downloads/bibliocloud-app.dump `heroku pg:backups:url -q -a bibliocloud-app`

/Applications/Postgres.app/Contents/Versions/9.6/bin/psql -d postgres <<EOF
drop database if exists bibliocloud_iop2;
create database bibliocloud_iop2;
EOF

/Applications/Postgres.app/Contents/Versions/9.6/bin/psql -d postgres <<EOF
drop database if exists bibliocloud_app;
create database bibliocloud_app;
EOF

/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_restore --format=custom --no-acl --no-owner -d bibliocloud_iop2 ~/Downloads/bibliocloud-iop.dump
/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_restore --format=custom --no-acl --no-owner -d consonance_app ~/Downloads/consonance.dump


/Applications/Postgres.app/Contents/Versions/10/bin/pg_restore  --format=custom --no-acl --no-owner -p5433 -d consonance_app2 ~/Downloads/bibliocloud-app.dump



curl -o ~/Downloads/bibliocloud-app.dump `heroku pg:backups:url -q -a bibliocloud-app`

psql -d postgres <<EOF
drop database if exists bibliocloud_app;
create database bibliocloud_app;
EOF

pg_restore --format=custom --no-acl --no-owner -d bibliocloud_app ~/Downloads/bibliocloud-app.dump




/Applications/Postgres.app/Contents/Versions/9.6/bin/psql -d postgres <<EOF
drop database if exists bibliocloud_bkp;
create database bibliocloud_bkp;
EOF

/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_restore --format=custom --no-acl --no-owner -d consonance_pre_onix3 ~/Downloads/bibliocloud-app.dump






/Applications/Postgres.app/Contents/Versions/11/bin/pg_restore --format=custom --no-acl --no-owner -d consonance_pg11 ~/Downloads/bibliocloud-iop.dump
