client = Client.is_leuven

shop = Shop.find(client.shops.first)
book = client.shops.first.books.is_ebook.first
price = book.current_eur_inctax_consumer_price_instance.price_amount.to_f
product = Shopify::ProductPresenter.new(product: book, shop: shop).call[:variants]
price
product

2.4.4 :015 > price
 => 52.0
2.4.4 :016 > product
 => [{:barcode=>"9789461660312", :title=>"Churches", :price=>"52.0", :requires_shipping=>false, :taxable=>true}]

2.4.4 :013 > price
 => 52.0
2.4.4 :014 > product
 => [{:barcode=>"9789461660312", :title=>"Churches", :price=>42.98, :requires_shipping=>false, :taxable=>true}]


client = Client.is_lund
shop = Shop.find(client.shops.first)
book = client.shops.first.books.first
price = book.current_gbp_inctax_consumer_price_instance.price_amount.to_f
product = Shopify::ProductPresenter.new(product: book, shop: shop).call[:variants]
price
product
2.4.4 :041 > price
 => 45.0
2.4.4 :042 > product
 => [{:barcode=>"9781848220140", :title=>"Brian O'Doherty/Patrick Ireland", :price=>"45.0", :requires_shipping=>true, :taxable=>false}]

2.4.4 :020 > price
 => 45.0
2.4.4 :021 > product
 => [{:barcode=>"9781848220140", :title=>"Brian O'Doherty/Patrick Ireland", :price=>"45.0", :requires_shipping=>true, :taxable=>false}]

client = Client.is_lund
shop = Shop.find(client.shops.last)
book = client.shops.first.books.first
price = book.current_usd_exctax_consumer_price_instance.price_amount.to_f
product = Shopify::ProductPresenter.new(product: book, shop: shop).call[:variants]
price
product
2.4.4 :052 > price
 => 89.99
2.4.4 :053 > product
 => [{:barcode=>"9781848220140", :title=>"Brian O'Doherty/Patrick Ireland", :price=>0.8999e2, :requires_shipping=>true, :taxable=>true}]

2.4.4 :027 > price
 => 89.99
2.4.4 :028 > product
 => [{:barcode=>"9781848220140", :title=>"Brian O'Doherty/Patrick Ireland", :price=>89.99, :requires_shipping=>true, :taxable=>true}]

client = Client.is_liverpool
shop = Shop.find(client.shops.last)
book = client.shops.first.books.first
price = book.current_gbp_inctax_consumer_price_instance.price_amount.to_f
product = Shopify::ProductPresenter.new(product: book, shop: shop).call[:variants]
price
product

2.4.4 :074 > price
 => 19.99
2.4.4 :075 > product
 => [{:barcode=>"9781846319570", :title=>"3am: Wonder, Paranoia and the Restless Night", :price=>"19.99", :requires_shipping=>true, :taxable=>false}]

2.4.4 :034 > price
 => 19.99
2.4.4 :035 > product
 => [{:barcode=>"9781846319570", :title=>"3am: Wonder, Paranoia and the Restless Night", :price=>"19.99", :requires_shipping=>true, :taxable=>false}]






client = Client.is_liverpool
shop = Shop.find(client.shops.last)
book = client.shops.first.books.first
price = book.current_gbp_inctax_consumer_price_instance.price_amount.to_f
product = Shopify::ProductPresenter.new(product: book, shop: shop).call[:variants]



connection = Shopify::Connection.new(shop: shop)




remote_product = ShopifyAPI::Product.where(handle: book.id).first







