
"List resources that don't have a file"
=======================================

Client.is_facet.supportingresources.reject do |supportingresource|
  supportingresource.image.exists?
end.map(&:id)


"Check that all styles have a file"
===================================

Client.is_facet.supportingresources.limit(10).flat_map do |supportingresource|
  missing = supportingresource.image.styles.keys.reject do |style|
    supportingresource.image.exists?(style)
  end
  .map{|x| [x.id, x.style]}
end

"Reprocess particular ISBNs"
===========================

isbns = %w[
  9781850749875,
  9781848021969,
  9781848023437,
  9781848023611,
  9781850743255,
  9781848024052,
  9781848023147,
  9781873592892,
  9781848024243,
  9781848023215,
  9781848020306,
  9781848024236,
  9781848021686,
  9781873592823,
  9781848021679,
  9781848021853,
  9781848023444,
  9781848022379,
  9781848022720,
  9781848021655,
  9781848020269,
  9781848023390,
  9781848022256,
  9781848020429,
  9781848021594,
  9781848024939,
  9781848024205,
  9781848021518,
  9781848021488,
  9781848021471,
  9781848021464,
  9781848021457,
  9781848023642,
  9781848021440,
  9781848024168,
  9781848024137,
  9781848021433,
  9781848023628,
  9781848024298,
  9781848024106,
  9781848023505,
  9781859463970,
  9781848024823,
  9781848023185,
  9781848020290,
  9781848023482,
  9781848022041,
  9781905624485,
  9781848023093,
  9781848023727,
  9781848023451,
  9781848023819,
  9781848023253,
  9781848021174
]

works = Client.is_liverpool.works.joins(:books).where(books: {isbn: isbns}).uniq

works.flat_map(&:supportingresources).each do |supportingresource|
  supportingresource.image.styles.keys.map do |style|
    next if supportingresource.image.exists?(style)
    supportingresource.image.reprocess!(style)
  end
end


"Reprocess missing styles"
==========================

Client.is_facet.supportingresources.map do |supportingresource|
  supportingresource.image.styles.keys.map do |style|
    next if supportingresource.image.exists?(style)
    supportingresource.image.reprocess!(style)
  end
end

Rails.logger.level=0

Supportingresource.where("created_at > ?", Date.today - 20.days).each do |supportingresource|
  supportingresource.image.styles.keys.map do |style|
    next if supportingresource.image.exists?(style)
    puts "Processing #{supportingresource.id} #{style}"
    supportingresource.image.reprocess!(style)
  end
end.count


Rails.logger.level=0

supportingresource = Supportingresource.find(1467)

supportingresource.image.styles.keys.map do |style|
  supportingresource.image.exists?(style)
end

supportingresource.image.styles.keys




Client.is_pharma.supportingresources.reject{|sr| sr.image.exists? :jpg_rgb_1500h}.map(&:id)



Supportingresource.where(:id => [1527]).each do |sr|
  Supportingresource::IMAGE_STYLES.keys.each do |style|
    next if sr.image.exists?(style)
    sr.image.reprocess!(style)
  end
end


Supportingresource.order(id: :desc).limit(85).each do |sr|
  Supportingresource::IMAGE_STYLES.keys.each do |style|
    next if sr.image.exists?(style)
    sr.image.reprocess!(style)
  end
end


Supportingresource.where(:id => [249117]).each do |sr|
  Supportingresource::IMAGE_STYLES.keys.each do |style|
    sr.image.reprocess!(style)
  end
end



Supportingresource.find(249117).image.reprocess! :jpg_rgb_0650h



{[:jpg_rgb_0050w, true]=>324, [:jpg_rgb_0050w, false]=>4, [:jpg_rgb_0075w, true]=>324, [:jpg_rgb_0075w, false]=>4, [:jpg_rgb_0250w, true]=>324, [:jpg_rgb_0250w, false]=>4, [:jpg_rgb_0150h, true]=>324, [:jpg_rgb_0150h, false]=>4, [:jpg_rgb_0250h, true]=>324, [:jpg_rgb_0250h, false]=>4, [:jpg_rgb_0400h, true]=>324, [:jpg_rgb_0400h, false]=>4, [:jpg_rgb_0650h, true]=>324, [:jpg_rgb_0650h, false]=>4, [:jpg_rgb_1500h, true]=>324, [:jpg_rgb_1500h, false]=>4, [:jpg_rgb_original, false]=>5, [:jpg_rgb_original, true]=>323, [:jpg_cmyk_original, false]=>5, [:jpg_cmyk_original, true]=>323, [:tiff_original, false]=>5, [:tiff_original, true]=>323, [:png_original, false]=>5, [:png_original, true]=>323, [:original, true]=>328}