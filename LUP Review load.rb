#LUP Review load
reload!
@client = Client.find_by(:client_name => "lup")

@client.marketingtexts.where(:legacy_code => ["07", "08", "10"]).destroy_all

doc = Nokogiri::XML(open("http://s3-eu-west-1.amazonaws.com/bibliocloudimages/lup_books_onix.xml")) ; 0
#doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/LUP/LUP_Onix.xml")) ; 0
doc.remove_namespaces! ; 0

ActiveRecord::Base.transaction do
  doc.xpath("/ONIXMessage/Product").each do |node|  #CUSTOM
    @onix_isbn = node.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue|./productidentifier[b221=15]/b244").try(:text).try(:strip).presence
    @book      = @client.books.find_by(:isbn => @onix_isbn)
    if @book
      node.xpath("OtherText[TextTypeCode=07]|OtherText[TextTypeCode=08]|OtherText[TextTypeCode=10]").each do |text|
        text_author           = text.at_xpath("TextAuthor"         ).try(:text).try(:strip).presence
        text_source_corporate = text.at_xpath("TextSourceCorporate").try(:text).try(:strip).presence
        text_source_title     = text.at_xpath("TextSourceTitle"    ).try(:text).try(:strip).presence
        legacy_code           = text.at_xpath("TextTypeCode"       ).try(:text).try(:strip).presence
        text                  = text.at_xpath("Text"               ).try(:text).try(:strip).presence

        mt_attribs = {:legacy_code    => legacy_code,
                      :marketing_text => text       ,
                      :pull_quote     => text       ,
                      :client_id      => @client.id}

        if text_author
          contact               = Contact.where(:client_id        => @client.id ,
                                                :person_name      => text_author,
                                                :contributor_type => "Person").first_or_create!
          contact.contact_types << ContactType.find_by(:name => "Reviewer") unless contact.is_of_type?("Reviewer")
          mt_attribs.merge!(:contact => contact)
        end

        if text_source_corporate
          publisher               = Contact.where(:client_id        => @client.id,
                                                  :corporate_name   => text_source_corporate   ,
                                                  :contributor_type => "Company").first_or_create!
          publisher.contact_types << ContactType.find_by(:name => "Review publisher") unless publisher.is_of_type?("Review publisher")
          mt_attribs.merge!(:publisher_id => publisher.id)

          if mt_attribs[:contact]
            Relationship.where(:contact_id    => mt_attribs[:contact].id,
                               :relation_id   => publisher.id                           ,
                               :relation_type => "Writes reviews for"                   ).first_or_create!
          end
        end

        mt_attribs.merge!(:publication_name  => text_source_title) if text_source_title


        puts "marketing text = #{mt_attribs}"

        mt = @book.work.marketingtexts.create!(mt_attribs)

        @book.marketingtexts << mt
      end
    end
  end
  puts @client.marketingtexts.count
  puts @client.marketingtexts.where.not(:publisher_id   => nil).count
  puts @client.marketingtexts.where.not(:publication_name => nil).count
  puts @client.marketingtexts.where.not(:contact_id     => nil).count


end

