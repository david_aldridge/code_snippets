reload!
client   = Client.is_iop
#file     = open('/Users/david/Documents/Bibliocloud/Clients/IoP/ep_titles_onix.xml')
file     = open('https://s3-eu-west-1.amazonaws.com/bibliocloudimages-iop/onix/ep_titles_onix.xml')
document = ONIX::DocumentReader.new(xml: file)



document.product_nodes.map(&:issn)



ActiveRecord::Base.transaction do
  Book.all.each do |book|
    next unless book.read_attribute(:identifying_doi)
    book.identifying_doi = book.read_attribute(:identifying_doi).sub(/^.*10\.1088/,'10.1088')
    book.save if book.changed?
  end
end


Rails.logger.level=1

ActiveRecord::Base.transaction{ ONIX::ImportProducts.new(     document: document, client: client).call } ; 0
ActiveRecord::Base.transaction{ ONIX::ImportContributors.new( document: document, client: client).call } ; 0
ActiveRecord::Base.transaction{ ONIX::ImportContributions.new(document: document, client: client).call } ; 0
ActiveRecord::Base.transaction{ ONIX::ImportSubjects.new(     document: document, client: client).call } ; 0
ActiveRecord::Base.transaction{ ONIX::ImportTextContents.new( document: document, client: client).call } ; 0





Client.first.update_attributes(standard_price_types: ["gbp_exctax", "eur_exctax", "usd_exctax", "cad_exctax", "aud_exctax"])


Book.where.not(internal_reference: nil).each{|b| b.update_attributes(:publishers_reference => b.internal_reference.upcase) }

Client.first.imprints.first.works = Client.first.works

Imprint.first.update_attributes(
  publication_country: "GB",
  publication_city:    "Bristol",
  code:                "IOPP")

Work.all.each{|w| w.languages.create(country_code: 'GB', language_code: 'eng', language_role: '01')}

Client.first.seriesnames.create(
  title_without_prefix: 'IOP Expanding Physics',
  issn: '2053-2563',
  imprint: Imprint.first,
  code: 'EP',
  short_description: 'IOP Expanding Physics publishes high-quality texts from leading voices across the research landscape on key areas in physics and related subject areas')

Physics World Discovery
Client.first.seriesnames.first.works = Client.first.works

Subjectcode.destroy_all

Client.first.subjectcodes.create(code: '10' , value: 'General & Introductory Physics'          )
Client.first.subjectcodes.create(code: '20' , value: 'Classical Physics'                       )
Client.first.subjectcodes.create(code: '30' , value: 'Thermodynamics'                          )
Client.first.subjectcodes.create(code: '40' , value: 'Dynamics & Fluid Dynamics'               )
Client.first.subjectcodes.create(code: '50' , value: 'Mechanics & Statics'                     )
Client.first.subjectcodes.create(code: '60' , value: 'Electricity, Magnetism & Electrodynamics')
Client.first.subjectcodes.create(code: '70' , value: 'Applied Physics'                         )
Client.first.subjectcodes.create(code: '80' , value: 'Engineering & Industrial Physics'        )
Client.first.subjectcodes.create(code: '90' , value: 'Astronomy & Astrophysics'                )
Client.first.subjectcodes.create(code: '100', value: 'Gravitational Physics & Cosmology'       )
Client.first.subjectcodes.create(code: '110', value: 'Geophysics & Planetary Science'          )
Client.first.subjectcodes.create(code: '120', value: 'Environmental Physics & Clean Technology')
Client.first.subjectcodes.create(code: '130', value: 'Atomic & Molecular Physics'              )
Client.first.subjectcodes.create(code: '140', value: 'Chemical Physics'                        )
Client.first.subjectcodes.create(code: '150', value: 'Condensed Matter Physics'                )
Client.first.subjectcodes.create(code: '160', value: 'Materials Science'                       )
Client.first.subjectcodes.create(code: '170', value: 'Electronic Materials & Devices'          )
Client.first.subjectcodes.create(code: '180', value: 'Mathematical & Computational Physics'    )
Client.first.subjectcodes.create(code: '190', value: 'Lasers, Optics & Photonics'              )
Client.first.subjectcodes.create(code: '200', value: 'Imaging & Image Processing'              )
Client.first.subjectcodes.create(code: '210', value: 'Biophysics'                              )
Client.first.subjectcodes.create(code: '220', value: 'Medical Physics & Biomedical Engineering')
Client.first.subjectcodes.create(code: '230', value: 'Electrical & Electronic Engineering'     )
Client.first.subjectcodes.create(code: '240', value: 'Metrology, Instrumentation & Sensors'    )
Client.first.subjectcodes.create(code: '250', value: 'Nanoscience & Nanotechnology'            )
Client.first.subjectcodes.create(code: '260', value: 'Nuclear Physics'                         )
Client.first.subjectcodes.create(code: '270', value: 'Particle & High Energy Physics'          )
Client.first.subjectcodes.create(code: '280', value: 'Plasma Physics'                          )
Client.first.subjectcodes.create(code: '290', value: 'Quantum Physics'                         )
Client.first.subjectcodes.create(code: '300', value: 'Quantum Information & Computing'         )
Client.first.subjectcodes.create(code: '310', value: 'Nonlinear and Statistical Physics'       )
Client.first.subjectcodes.create(code: '900', value: 'Other'                                   )



Client.is_iop.collections.create(:name => 'Release 1')
Client.is_iop.collections.create(:name => 'Release 2')
Client.is_iop.collections.create(:name => 'Release 3')
Client.is_iop.collections.create(:name => 'Release 4')
Client.is_iop.collections.create(:name => 'Release 5')
Client.is_iop.collections.create(:name => 'Release 1 (AAS)')



client.proprietary_format_descriptions.destroy_all

hb = client.proprietary_format_descriptions.create(
  name: 'Hardback',
  product_form: 'BB',
  product_form_details: '[B402]',
  product_height_mm: 254.0,
  product_width_mm: 178.0)

mobi = client.proprietary_format_descriptions.create(
  name: 'Mobi',
  product_form: 'DG',
  epub_type_code: '022',
  product_content_type: ['10'])

epub = client.proprietary_format_descriptions.create(
  name: 'ePub',
  product_form: 'DG',
  epub_type_code: '029',
  product_content_type: ['10'])

pdf = client.proprietary_format_descriptions.create(
  name: 'Print PDF',
  product_form: 'DG',
  epub_type_code: '002',
  product_content_type: ['10'])



hb.books = client.books.is_hardback
mobi.books = client.books.mobis
epub.books = client.books.epubs
pdf.books = client.books.pdfs



client.proprietary_edition_descriptions.each{|e| e.books = []}
client.proprietary_edition_descriptions.destroy_all

hb_e = client.proprietary_edition_descriptions.create(
  code: 'HB',
  name: 'Hardback')

mobi_e = client.proprietary_edition_descriptions.create(
  code: 'MOBI',
  name: 'Mobi')

epub_e = client.proprietary_edition_descriptions.create(
  code: 'EPUB',
  name: 'ePub')

pdf_e = client.proprietary_edition_descriptions.create(
  code: 'PDF',
  name: 'PDF')

ch_mobi_e = client.proprietary_edition_descriptions.create(
  code: 'MOBI_Ch',
  name: 'Mobi Chapter')

ch_epub_e = client.proprietary_edition_descriptions.create(
  code: 'EPUB_Ch',
  name: 'ePub Chapter')

ch_pdf_e = client.proprietary_edition_descriptions.create(
  code: 'PDF_Ch',
  name: 'PDF Chapter')



#hb   = client.proprietary_format_descriptions.find_by(name: 'Hardback')
#mobi = client.proprietary_format_descriptions.find_by(name: 'Mobi')
#epub = client.proprietary_format_descriptions.find_by(name: 'ePub')
#pdf  = client.proprietary_format_descriptions.find_by(name: 'Print PDF')

hb_e.proprietary_format_descriptions      = [hb]
mobi_e.proprietary_format_descriptions    = [mobi]
epub_e.proprietary_format_descriptions    = [epub]
pdf_e.proprietary_format_descriptions     = [pdf]
ch_mobi_e.proprietary_format_descriptions = [mobi]
ch_epub_e.proprietary_format_descriptions = [epub]
ch_pdf_e.proprietary_format_descriptions  = [pdf]



hb_e.books   = client.books.is_hardback
mobi_e.books = client.books.mobis
epub_e.books = client.books.epubs
pdf_e.books  = client.books.pdfs





Client.first.contacts








CONCISE_PHYSICS



client = Client.is_morgan_claypool

client.update_attributes(identifier: 'morgan-claypool')

client.update_attributes(standard_price_types: ["gbp_exctax", "eur_exctax", "usd_exctax", "cad_exctax", "aud_exctax"])


client.books.where.not(internal_reference: nil).each{|b| b.update_attributes(:publishers_reference => b.internal_reference.upcase) }

client.imprints.first.works = client.works





#Imprint.first.update_attributes(
#  publication_country: "GB",
#  publication_city:    "Bristol",
#  code:                "IOPP")

client.books.each{|w| w.languages.create(country_code: 'GB', language_code: 'eng', language_role: '01')}



client.seriesnames.destroy_all


Client.is_morgan_claypool.seriesnames.create(
  title_without_prefix: 'IOP Concise Physics',
  print_issn:           '2054-7307',
  online_issn:          '2053-2571',
  imprint: Client.is_morgan_claypool.imprints.first,
  code: 'CP',
  short_description: 'IOP Concise Physics developed with Morgan & Claypool Publishers (M&C), focuses on shorter texts in rapidly advancing areas or topics where an introductory text is more appropriate.')

client.subjectcodes.destroy_all

client.subjectcodes.create(code: '10' , value: 'General & Introductory Physics'          )
client.subjectcodes.create(code: '20' , value: 'Classical Physics'                       )
client.subjectcodes.create(code: '30' , value: 'Thermodynamics'                          )
client.subjectcodes.create(code: '40' , value: 'Dynamics & Fluid Dynamics'               )
client.subjectcodes.create(code: '50' , value: 'Mechanics & Statics'                     )
client.subjectcodes.create(code: '60' , value: 'Electricity, Magnetism & Electrodynamics')
client.subjectcodes.create(code: '70' , value: 'Applied Physics'                         )
client.subjectcodes.create(code: '80' , value: 'Engineering & Industrial Physics'        )
client.subjectcodes.create(code: '90' , value: 'Astronomy & Astrophysics'                )
client.subjectcodes.create(code: '100', value: 'Gravitational Physics & Cosmology'       )
client.subjectcodes.create(code: '110', value: 'Geophysics & Planetary Science'          )
client.subjectcodes.create(code: '120', value: 'Environmental Physics & Clean Technology')
client.subjectcodes.create(code: '130', value: 'Atomic & Molecular Physics'              )
client.subjectcodes.create(code: '140', value: 'Chemical Physics'                        )
client.subjectcodes.create(code: '150', value: 'Condensed Matter Physics'                )
client.subjectcodes.create(code: '160', value: 'Materials Science'                       )
client.subjectcodes.create(code: '170', value: 'Electronic Materials & Devices'          )
client.subjectcodes.create(code: '180', value: 'Mathematical & Computational Physics'    )
client.subjectcodes.create(code: '190', value: 'Lasers, Optics & Photonics'              )
client.subjectcodes.create(code: '200', value: 'Imaging & Image Processing'              )
client.subjectcodes.create(code: '210', value: 'Biophysics'                              )
client.subjectcodes.create(code: '220', value: 'Medical Physics & Biomedical Engineering')
client.subjectcodes.create(code: '230', value: 'Electrical & Electronic Engineering'     )
client.subjectcodes.create(code: '240', value: 'Metrology, Instrumentation & Sensors'    )
client.subjectcodes.create(code: '250', value: 'Nanoscience & Nanotechnology'            )
client.subjectcodes.create(code: '260', value: 'Nuclear Physics'                         )
client.subjectcodes.create(code: '270', value: 'Particle & High Energy Physics'          )
client.subjectcodes.create(code: '280', value: 'Plasma Physics'                          )
client.subjectcodes.create(code: '290', value: 'Quantum Physics'                         )
client.subjectcodes.create(code: '300', value: 'Quantum Information & Computing'         )
client.subjectcodes.create(code: '310', value: 'Nonlinear and Statistical Physics'       )
client.subjectcodes.create(code: '900', value: 'Other'                                   )



client.collections.create(:name => 'Release 1')
client.collections.create(:name => 'Release 2')
client.collections.create(:name => 'Release 3')
client.collections.create(:name => 'Release 4')
client.collections.create(:name => 'Release 5')






client.proprietary_format_descriptions.destroy_all

pb = client.proprietary_format_descriptions.create(
  name: 'Paperback',
  product_form: 'BC',
  product_height_mm: 254.0,
  product_width_mm: 178.0)

epub = client.proprietary_format_descriptions.create(
  name: 'ePub',
  product_form: 'DG',
  epub_type_code: '029',
  product_content_type: ['10'])

pdf = client.proprietary_format_descriptions.create(
  name: 'PDF',
  product_form: 'DG',
  epub_type_code: '002',
  product_content_type: ['10'])



pb.books = client.books.is_paperback
epub.books = client.books.epubs
pdf.books = client.books.pdfs



client.proprietary_edition_descriptions.each{|e| e.books = []}
client.proprietary_edition_descriptions.destroy_all

pb_e = client.proprietary_edition_descriptions.create(
  code: 'PB',
  name: 'Paperback')

epub_e = client.proprietary_edition_descriptions.create(
  code: 'EPUB',
  name: 'ePub')

pdf_e = client.proprietary_edition_descriptions.create(
  code: 'PDF',
  name: 'PDF')

ch_epub_e = client.proprietary_edition_descriptions.create(
  code: 'EPUB_Ch',
  name: 'ePub Chapter')

ch_pdf_e = client.proprietary_edition_descriptions.create(
  code: 'PDF_Ch',
  name: 'PDF Chapter')



#hb   = client.proprietary_format_descriptions.find_by(name: 'Hardback')
#mobi = client.proprietary_format_descriptions.find_by(name: 'Mobi')
#epub = client.proprietary_format_descriptions.find_by(name: 'ePub')
#pdf  = client.proprietary_format_descriptions.find_by(name: 'Print PDF')

pb_e.proprietary_format_descriptions      = [pb]
epub_e.proprietary_format_descriptions    = [epub]
pdf_e.proprietary_format_descriptions     = [pdf]
ch_epub_e.proprietary_format_descriptions = [epub]
ch_pdf_e.proprietary_format_descriptions  = [pdf]



pb_e.books   = client.books.is_paperback
epub_e.books = client.books.epubs
pdf_e.books  = client.books.pdfs


pp Client.is_iop.books.where.not(:publishers_reference => nil).pluck(:publishers_reference, :epub_type_code).map{|x| [x[1],(x[0]||'')[-1] ]}.group_by(&:to_a).map{|k,v| [k,v.size]}



Work.all.pluck(:registrants_internal_reference)





Work.all.each{|w| w.update_attributes(:registrants_internal_reference => w.registrants_internal_reference) }

Book.all.each{|b| b.update_attributes(:publishers_reference => b.publishers_reference) }

Client.is_iop.works.map(&:seriesnames)

EP = Seriesname.find_by(code: 'EP')


Client.is_iop.works.each{|w| w.seriesnames << EP unless w.seriesnames.include? EP}

CP = Seriesname.find_by(code: 'CP')

Client.is_morgan_claypool.works.each{|w| w.seriesnames << CP unless w.seriesnames.include? CP}




Book.pluck(:publishers_reference).sort_by{|x| x || '' }
Work.pluck(:registrants_internal_reference)



pp Book.pluck(:publishers_reference,:client_id, :product_form, :epub_type_code).sort_by{|x| x[0] || '' }


ePub = E
mobi = m
print = p






pp Client.is_iop.works.map{|w| [w.id, w.books.map{|b| b.ebook_format_name || 'Print'}.sort.join(",")]}.select{|x| x[1] != "EPUB,MobiPocket,Print" }.sort_by{|x| x[1]}

pp Client.is_morgan_claypool.works.map{|w| [w.id, w.books.map{|b| b.ebook_format_name || 'Print'}.sort.join(",")]}.select{|x| x[1] != "EPUB,PDF,Print" }.sort_by{|x| x[1]}







Work.where.not(:main_bic_code => nil).each{|w| w.update_attributes(main_bic_code: w.main_bic_code.gsub( /\[|\]|\\|\"/ , '' ) ) }





Briefing notes
==============

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1_W_mHnEleMOr4qs0OsTyyKADxuLHNz-pVd7SOtcucFk',
                            worksheet_name:    'Sheet 1',
                            import_class:      nil,
                            rollback_on_error: true)

topics = import.send(:list).first.keys.select{|key| key =~ /^Topic/}
import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  brief       = work.briefs.where(:title => 'Imported').first_or_create!
  brief.books = work.books
  topics.each do |topic|
    contents = row[topic].squish.presence
    next unless contents
    note_title = topic.gsub(/^[^"]*"|"[^"]*/,'')
    brief.briefing_texts.where(:briefing_text_type => note_title).first_or_create!(:briefing_text => contents)
  end
end


user = Client.first.users.new(:email => '1', :first_name=> 'Charlotte', :last_name => 'Ashbrooke' ) ; user.save(:validate => false)
user = Client.first.users.new(:email => '2', :first_name=> 'Jeanine'  , :last_name => 'Burke'     ) ; user.save(:validate => false)
user = Client.first.users.new(:email => '3', :first_name=> 'Jess'     , :last_name => 'Fricchione') ; user.save(:validate => false)
user = Client.first.users.new(:email => '4', :first_name=> 'Jessica'  , :last_name => 'Fricchione') ; user.save(:validate => false)
user = Client.first.users.new(:email => '5', :first_name=> 'Joel'     , :last_name => 'Claypool'  ) ; user.save(:validate => false)
user = Client.first.users.new(:email => '6', :first_name=> 'Nicki'    , :last_name => 'Dennis'    ) ; user.save(:validate => false)
user = Client.first.users.new(:email => '7', :first_name=> 'Wayne'    , :last_name => 'Yuhasz'    ) ; user.save(:validate => false)




Task roles
==========


user_map = Hash[*User.all.map{|u| [u.full_name,u]}.flatten].freeze
managing_editor_role = TaskRole.find_by(:name =>"Managing editor")
commissioning_editor_role = TaskRole.find_by(:name =>"Senior Commissioning Manager")

ActiveRecord::Base.transaction do
  import.send(:list).each do |row|
    work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
    unless work
      puts "No work for #{row['Book Ref']}"
      next
    end
    managing_editor_name      = row['Managing Editor'].squish.presence
    managing_editor           = user_map[managing_editor_name]
    if managing_editor
      managing_editor.task_roles << managing_editor_role unless managing_editor.task_roles.include? managing_editor_role
      work.task_roles            << managing_editor_role unless work.task_roles.include?            managing_editor_role
      work.role_involvements.find_by(:task_role => managing_editor_role).role_adopters =
                                [managing_editor.role_adopters.find_by(:task_role => managing_editor_role)]
    end

    commissioning_editor_name = row['Commissioning Editor'].squish.presence
    commissioning_editor      = user_map[commissioning_editor_name]
    if commissioning_editor
      commissioning_editor.task_roles << commissioning_editor_role unless commissioning_editor.task_roles.include? commissioning_editor_role
      work.task_roles            << commissioning_editor_role unless work.task_roles.include?            commissioning_editor_role
      work.role_involvements.find_by(:task_role => commissioning_editor_role).role_adopters =
                                [commissioning_editor.role_adopters.find_by(:task_role => commissioning_editor_role)]
    end

  end
  #raise ActiveRecord::Rollback
end


Subject codes
=============

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1_W_mHnEleMOr4qs0OsTyyKADxuLHNz-pVd7SOtcucFk',
                            worksheet_name:    'Sheet 1',
                            import_class:      nil,
                            rollback_on_error: true)

import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  work.subjectcodes = work.client.subjectcodes.where(:code => [ row["Subject 1"],row["Subject 2"],row["Subject 3"] ].map(&:squish).map(&:presence) )
  work.subjectcode  = work.work_subjectcodes.joins(:subjectcode).find_by(:subjectcodes => {:code => row["Subject 1"]})
  work.save if work.changed?
end

Spine widths
=============

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1_W_mHnEleMOr4qs0OsTyyKADxuLHNz-pVd7SOtcucFk',
                            worksheet_name:    'Sheet 1',
                            import_class:      nil,
                            rollback_on_error: true)

import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  work.update_attributes(:audience_description => row["Readership"].squish.presence)
  print_book = work.books.is_hardback.first || work.books.is_paperback.first
  unless print_book
    puts "No print book for #{row['Book Ref']}"
    next
  end
  print_book.update_attributes(:product_thickness_mm => row["Spine Width"].to_d) if row["Spine Width"].squish.presence

end

import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  contract = work.contract
  date_due     = Date.parse(row['MS Submission Date']       ) if row['MS Submission Date'].presence
  slipped_date = Date.parse(row['MS received in Production']) if row['MS received in Production'].presence
  if date_due || slipped_date
    delivery_dates = {}
    delivery_dates.merge!(date_due: date_due ||slipped_date   )
    delivery_dates.merge!(date_due: slipped_date) if slipped_date
    contract.ms_delivery_dates.where(:deliverable_type => 'Content').first_or_create!(delivery_dates)
  end

end


import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  contract = work.contract
  date_due     = Date.parse(row['MS Submission Date']       ) if row['MS Submission Date'].presence
  slipped_date = Date.parse(row['MS received in Production']) if row['MS received in Production'].presence
  if date_due || slipped_date
    delivery_dates = {}
    delivery_dates.merge!(date_due: date_due ||slipped_date   )
    delivery_dates.merge!(date_due: slipped_date) if slipped_date
    contract.ms_delivery_dates.where(:deliverable_type => 'Content').first_or_create!(delivery_dates)
  end

end






import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  keywords = row["Keywords"].squish.presence
  next unless keywords
  keywords = keywords.gsub(',',';').split(';').map(&:squish).map(&:presence).compact.join(";")
  work.keywords = keywords
  work.save if work.changed?
end



Releases
=========

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1_W_mHnEleMOr4qs0OsTyyKADxuLHNz-pVd7SOtcucFk',
                            worksheet_name:    'Releases',
                            import_class:      nil,
                            rollback_on_error: true)

import.send(:list).each do |row|
  book = Book.find_by(:isbn => row["ebook ISBN"]) || Book.find_by(:isbn => row["Print ISBN"])
  unless book
    puts "No book for #{row["ebook ISBN"]}"
    next
  end
  release = "Release #{row['Release']}"
  batch = Collection.where(:name => release, :client_id => book.client_id).first_or_create!
  batch.books = (batch.books + book.work.books).uniq
end




Work.pluck(:main_bisac_code, :bisac_general_codes)


Work.where.not(:main_bisac_code => nil).each{ |w| w.update_attributes(:main_bisac_code => w.main_bisac_code.gsub(/"|\[|\]/,'')) }


Work.where.not(:main_bic_code => nil).each{ |w| w.update_attributes(:main_bic_code => w.main_bic_code.gsub(/"|\[|\]/,'')) }


Work.where(:main_bic_code => nil).each do |w|
  next if w.bic_subject_codes.empty?
  w.update_attributes(:main_bic_code => w.bic_subject_codes.first, :bic_subject_codes => w.bic_subject_codes[1..-1])
end

Work.all.each do |w|
  next if w.bisac_general_codes.empty?
  w.update_attributes(:main_bisac_code => w.bisac_general_codes.first, :bisac_general_codes => w.bisac_general_codes[1..-1])
end




Work.pluck(:thema_main_code, :thema_subject_codes)

Work.where.not(:thema_main_code => nil).each{ |w| w.update_attributes(:thema_main_code => w.thema_main_code.gsub(/"|\[|\]/,'')) }


Work.where(:thema_main_code => nil).each do |w|
  next if w.thema_subject_codes.empty?
  new_code = w.thema_subject_codes.sort_by{|x| [x.size,x]}.first
  w.update_attributes(:thema_main_code => new_code, :thema_subject_codes => w.thema_subject_codes - [new_code] )
end




.sort_by{|x| [x.size,x]}




["MKR", "MKSH"].sort_by{|x| [x.size,x]}