require 'benchmark'

def method0(str)
  true
end

def method1(str)
  Integer(str || '')
  true
rescue ArgumentError
  false
end

def method2(str)
  return false unless str.ascii_only?
  str.each_byte.none? { |byte| byte > 57 || byte < 48 }
end

n = 1000000
Benchmark.bm(12) do |x|
  x.report("method 0")   { n.times {method0("6464257347568568")} }
  x.report("method 1 good")   { n.times {method1("6464257347568568")} }
  x.report("method 2 good")   { n.times {method2("6464257347568568")} }
  x.report("method 1 bad")   { n.times {method1("64642573475685x8")} }
  x.report("method 2 bad")   { n.times {method2("64642573475685x8")} }
  x.report("method 1 bad")   { n.times {method1("x646425734756858")} }
  x.report("method 2 bad")   { n.times {method2("x646425734756858")} }
end ; ""
