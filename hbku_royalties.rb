client = Client.is_hbku

ActiveRecord::Base.transaction do

  #client.masterchannels.destroy_all

  #hps = client.masterchannels.create!(:masterchannel_name => "Home Print Sales"   , :sales_type => "Product", :inc_product_forms => ["BB", "BC"])
  #rts = client.masterchannels.create!(:masterchannel_name => "Rights"             , :sales_type => "Right"  )

  client.masterchannels.find_by(:masterchannel_name => "Rights"             ).update_attributes(:sales_type => "Right")
  client.masterchannels.find_by(:masterchannel_name => "World Digital Sales").update_attributes(:inc_product_forms => ["DG"])
  client.masterchannels.find_by(:masterchannel_name => "Home Print Sales"   ).update_attributes(:inc_product_forms => ["BB", "BC"])
  client.masterchannels.find_by(:masterchannel_name => "Export Print Sales" ).update_attributes(:inc_product_forms => ["BB", "BC"])

  hps.channels.create!(:channel_name => "Marston (Gardners)"      )
  hps.channels.create!(:channel_name => "Marston (Bertrams)"      )
  hps.channels.create!(:channel_name => "Marston (Website)"       )
  hps.channels.create!(:channel_name => "Marston (Amazon)"        )
  hps.channels.create!(:channel_name => "Marston (Home)"          )
  client.reload


  play_texts_template                = client.royalty_templates.first_or_create(:name => "10% -> 12% @ 3000qty")
  #play_texts_template.masterchannels = client.masterchannels
  #play_texts_template.channels       = client.channels

ActiveRecord::Base.transaction do

  #spec  = play_texts_template.
  #          royalty_template_masterchannels.
  #          joins(:masterchannel).
  #          where(:masterchannels => {:masterchannel_name => "Home Print Sales"}).
  #          first.
  #          create_royalty_specifier(:price_basis            => RoyaltySpecifier::ESCALATOR_ADD,
  #                                   :royalty_rate_base_pct  => 10)


    play_texts_template.royalty_template_masterchannels.
      joins(:masterchannel).
      where(:masterchannels => {:masterchannel_name => "Home Print Sales" }).
      first.
      create_royalty_specifier!(:price_basis            => RoyaltySpecifier::ESCALATOR_ADD,
                                 :royalty_rate_base_pct  => 10,
                                 :royalty_discount_escalators_attributes => [{:escalation_discount => 50,
                                                                              :royalty_percentage  => -3.5,
                                                                              :client_id           => client.id}])


  play_texts_template.royalty_template_masterchannels.joins(:masterchannel).where(:masterchannels => {:masterchannel_name => "World Digital Sales" }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  25)
  play_texts_template.royalty_template_masterchannels.joins(:masterchannel).where(:masterchannels => {:masterchannel_name => "Export Print Sales" }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  7.5)

  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Gratis"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Programme texts"     }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 10)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Photocopying"        }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 75)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Quotation"           }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 70)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Sheets"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 70)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Large print"         }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Digest"              }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Braille"             }).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Non-commercial audio"}).first.create_royalty_specifier!(:royalty_rate_base_pct  =>  0)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Non-UK republication"}).first.create_royalty_specifier!(:royalty_rate_base_pct  => 80)
  play_texts_template.royalty_template_channels.joins(:channel).where(:channels => {:channel_name => "Anthology"           }).first.create_royalty_specifier!(:royalty_rate_base_pct  => 50)
end



reload!

client = Client.find_by(:webname => "Oberon Books")

book = client.books.first
book = client.books.is_ebook.first
Work.find(21630).books.each do |book|
book.masterchannels = book.possible_masterchannels
book.book_masterchannels.each do |bmc|
#mc = book.masterchannels.first
  bmc.royalty_specifier = bmc.masterchannel.royalty_template_masterchannels.find_by(:royalty_template_id => play_texts_template.id).royalty_specifier.deep_clone include: RoyaltySpecifier::DEEP_CLONE_ATTR
  bmc.channels = bmc.masterchannel.channels
  bmc.book_channels.each do |bc|
    bc.royalty_specifier = bc.channel.royalty_template_channels.find_by(:royalty_template_id => play_texts_template.id).royalty_specifier.try(:deep_clone, include: RoyaltySpecifier::DEEP_CLONE_ATTR)
  end
end
end

book.book_masterchannels.first.royalty_specifier.royalty_discount_escalators



book.save


.where("? = any (inc_product_forms) or ? != any (inc_product_forms)", "BC", "BC")
