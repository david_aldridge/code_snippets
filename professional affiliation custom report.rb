520 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 1 Professional Affiliation 1",
  "The first professional affiliation for the first contributor",
  proc { |b| b.workcontacts_by_sequence_number[0]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
521 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 1 Professional Affiliation 2",
  "The second professional affiliation for the first contributor",
  proc { |b| b.workcontacts_by_sequence_number[0]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
522 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 2 Professional Affiliation 1",
  "The first professional affiliation for the second contributor",
  proc { |b| b.workcontacts_by_sequence_number[1]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
523 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 2 Professional Affiliation 2",
  "The second professional affiliation for the second contributor",
  proc { |b| b.workcontacts_by_sequence_number[1]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
524 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 3 Professional Affiliation 1",
  "The first professional affiliation for the third contributor",
  proc { |b| b.workcontacts_by_sequence_number[2]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
525 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 3 Professional Affiliation 2",
  "The second professional affiliation for the third contributor",
  proc { |b| b.workcontacts_by_sequence_number[2]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
526 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 4 Professional Affiliation 1",
  "The first professional affiliation for the fourth contributor",
  proc { |b| b.workcontacts_by_sequence_number[3]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
527 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 4 Professional Affiliation 2",
  "The second professional affiliation for the fourth contributor",
  proc { |b| b.workcontacts_by_sequence_number[3]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
528 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 5 Professional Affiliation 1",
  "The first professional affiliation for the fifth contributor",
  proc { |b| b.workcontacts_by_sequence_number[4]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
529 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 5 Professional Affiliation 2",
  "The second professional affiliation for the fifth contributor",
  proc { |b| b.workcontacts_by_sequence_number[4]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
530 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 6 Professional Affiliation 1",
  "The first professional affiliation for the sixth contributor",
  proc { |b| b.workcontacts_by_sequence_number[5]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
531 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 6 Professional Affiliation 2",
  "The second professional affiliation for the sixth contributor",
  proc { |b| b.workcontacts_by_sequence_number[5]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
532 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 7 Professional Affiliation 1",
  "The first professional affiliation for the seventh contributor",
  proc { |b| b.workcontacts_by_sequence_number[6]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
533 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 7 Professional Affiliation 2",
  "The second professional affiliation for the seventh contributor",
  proc { |b| b.workcontacts_by_sequence_number[6]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
534 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 8 Professional Affiliation 1",
  "The first professional affiliation for the eighth contributor",
  proc { |b| b.workcontacts_by_sequence_number[7]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
535 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 8 Professional Affiliation 2",
  "The second professional affiliation for the eighth contributor",
  proc { |b| b.workcontacts_by_sequence_number[7]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
536 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 9 Professional Affiliation 1",
  "The first professional affiliation for the ninth contributor",
  proc { |b| b.workcontacts_by_sequence_number[8]&.professional_affiliations_by_created_at&.send(:[], 0)&.to_s }
),
537 =>
CustomReport::Element.new(
  "Contributors",
  "Contrib 9 Professional Affiliation 2",
  "The second professional affiliation for the ninth contributor",
  proc { |b| b.workcontacts_by_sequence_number[8]&.professional_affiliations_by_created_at&.send(:[], 1)&.to_s }
),
