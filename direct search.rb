
  def self.direct_search_select(params)
    where("books.title % ? or
           books.isbn = regexp_replace(?, '[^0-9X]', '', 'g')", "%#{params}%", params).order("upper(title), edition, product_form, pub_date")
  end



  def direct_search
    respond_to do |format|
      format.json { render json: current_user.client.books.direct_search_select(params[:q]).pluck("json_build_object('id', books.id, 'value', books.title||' : '||case when books.edition is null then '' else '(Ed. '||books.edition::varchar||') : ' end || Coalesce((Select pfd.name from proprietary_format_descriptions pfd where pfd.id = books.proprietary_format_description_id), case books.product_form when 'DG' then books.product_form || ' (' || books.epub_type_code|| ')' else Coalesce(books.product_form,'') end ) ||' : '|| Coalesce(books.isbn, 'No ISBN') || ' : ' || Coalesce(to_char(books.pub_date, 'YYYY-MM-DD'), 'No pub date'))") }
    end
  end



  def direct_search_results
    unless params[:book][:direct_search].blank?
      @book = Book.find(params[:book][:direct_search])
      redirect_to @book
    else
      redirect_to :back
    end
  end



select json_build_object('id', obj||id, 'value', val)
from (
  select books.id id,
         'Book' obj,
         books.title ||
         ' : ' ||
         case
           when books.edition is null
           then ''
           else '(Ed. ' || books.edition::varchar || ') : '
          end ||
         Coalesce((Select pfd.name
                     from proprietary_format_descriptions pfd
                    where pfd.id = books.proprietary_format_description_id),
                   case books.product_form
                     when 'DG' then books.product_form || ' (' || books.epub_type_code || ')'
                     else Coalesce(books.product_form,'')
                   end ) ||
          ' : ' ||
          Coalesce(books.isbn, 'No ISBN') ||
          ' : ' ||
          Coalesce(to_char(books.pub_date, 'YYYY-MM-DD'), 'No pub date') val
  from   books
  where  client_id = 1 and
         ( books.title % 'niel' or
           books.isbn = regexp_replace('niel', '[^0-9X]', '', 'g') )
  union all
  select contacts.id id,
         'Contact' obj,
         coalesce(contacts.person_name, contacts.corporate_name)
  from   contacts
  where  client_id = 1 and
         ( contacts.person_name ilike '%niel%' or
           contacts.corporate_name ilike '%niel%' )) t
  order by val
