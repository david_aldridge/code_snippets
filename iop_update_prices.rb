client = Client.is_iop
client.books.includes(:prices).each do |book|
  book.current_gbp_exctax_consumer_price = (book.is_ebook? ?  95 :  99)
  book.current_eur_exctax_consumer_price = (book.is_ebook? ? nil : 115)
  book.current_usd_exctax_consumer_price = (book.is_ebook? ? nil : 159)
  book.current_cad_exctax_consumer_price = (book.is_ebook? ? nil : 178)
  book.current_aud_exctax_consumer_price = (book.is_ebook? ? nil : 182)
  book.default_price_status = (book.pub_date.nil? ? "00" : "02")
end
