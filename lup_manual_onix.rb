
client   = Client.is_liverpool
Rails.logger.level=1

[14, 17, 18, 20].each do |cdatt_id|
  DigitalAssetPush.new( client,
                        client.contact_digital_asset_transfer_templates.find(cdatt_id),
                        :pub_dates_to => Date.today + 12.months,
                        :transmit => false).run
end

[50].each do |cdatt_id|
  DigitalAssetPush.new( client,
                        client.contact_digital_asset_transfer_templates.find(cdatt_id),
                        :incremental    => false,
                        :pub_dates_to   => Date.today + 6.months,
                        :transmit => false).run
end

    # iGroup, PLS, CNPIEC, AcademicPub
[21, 36, 35, 40].each do |cdatt_id|
  DigitalAssetPush.new( client,
                        client.contact_digital_asset_transfer_templates.find(cdatt_id),
                        :pub_dates_to   => Date.today + 12.months,
                        :transmit => false).run
end


 pp Client.is_liverpool.stock_availabilities.where.not(:product_availability => nil).order(:availability_code).group(:availability_code,:product_availability).count

StockAvailability.where.not(:product_availability => nil).order(:availability_code).group(:availability_code,:product_availability, :client_id).count

[
 ["IP", "20"],
 ["MD", "23"],
 ["NP", "10"],
 ["OP", "51"],
 ["TP", "31"]
].each do |p|
  StockAvailability.where(:product_availability => p[0]).update_all(:product_availability => p[1])
end


