
ProprietaryEditionDescription.pluck(:id).each do |id|
  ProprietaryEditionDescription.reset_counters(id, :books)
end

Client.pluck(:id).each do |id|
  Client.reset_counters(id, :books, :works)
end


Book.joins(:client).where.not(clients: {identifier: nil}).pluck(:id).each do |id|
  Book.reset_counters(id, :prices, :book_briefs, :bookprints, :book_schedules, :royalty_batch_books)
end


Client.update_all(
  "books_count = (select count(*) from books x where x.client_id = clients.id),
   works_count = (select count(*) from works x where x.client_id = clients.id)")

Work.update_all(
  "books_count = (select count(*) from books x where x.work_id = works.id)")

Book.update_all(
  "book_briefs_count         = (select count(*) from book_briefs         x where x.book_id = books.id),
   bookprints_count          = (select count(*) from bookprints          x where x.book_id = books.id),
   prices_count              = (select count(*) from prices              x where x.book_id = books.id),
   royalty_batch_books_count = (select count(*) from royalty_batch_books x where x.book_id = books.id)
  ")


ProprietaryFormatDescription.update_all(
  "books_count = (
    select count(*)
    from books x
    where x.proprietary_format_description_id = proprietary_format_descriptions.id)"
)




Work.update_all(
  "books_count = (select count(*) from books x where x.work_id = works.id)")

Imprint.update_all(
  "books_count = (select count(*) from books b join works w on w.id = b.work_id where w.imprint_id = imprints.id)")
