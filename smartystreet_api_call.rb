class SmartyStreetApiCall < ActiveRecord::Base
  attr_accessible :client_id,
                  :country_code,
                  :request_hash,
                  :api_response

  validates :client, :presence => true
  validates :country_code, :inclusion => {:in => Constant::WORLD_COUNTRIES_LIST}
  validates :request_hash, :presence => true, :uniqueness => {:scope => [:client_id, :country_code]}
end
