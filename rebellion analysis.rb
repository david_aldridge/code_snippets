# Rebellion spreadsheet load
client      = Client.is_rebellion
filename    = "/Users/david/Documents/Bibliocloud/Clients/Rebellion/From Rebellion/eBook_Metadata.xlsx"
spreadsheet = Roo::Spreadsheet.open(filename, extension: :xlsx)
rows        = spreadsheet.sheet(0).parse(headers: true, clean: true)[1..471]

epub_isbns = rows.map{|r| r["ISBN13Epub"].to_s}.reject(&:blank?)
mobi_isbns = rows.map{|r| r["ISBN13Mobi"].to_s}.reject(&:blank?)
epub_isbns & mobi_isbns

epub_isbns.count
mobi_isbns.count


epub_isbns = rows.map{|r| r["ISBN13Epub"].to_s}.reject(&:blank?).group_by(&:itself).select{|isbn, isbns| isbns.many? }
mobi_isbns = rows.map{|r| r["ISBN13Mobi"].to_s}.reject(&:blank?).group_by(&:itself).select{|isbn, isbns| isbns.many? }

ebook_isbns = (epub_isbns + mobi_isbns).uniq
ebook_isbns.count
ebook_isbns.reject{|x| Lisbn.new(x).valid? }
client.books.pluck(:isbn) & ebook_isbns
ebook_isbns - client.books.pluck(:isbn)

uk_print_isbns = rows.map{|r| r["ISBN13PrintUK"].to_s}.reject(&:blank?)
us_print_isbns = rows.map{|r| r["ISBN13PrintUS"].to_s}.reject(&:blank?) - uk_print_isbns
print_isbns = uk_print_isbns + us_print_isbns
print_isbns.count
print_isbns.reject{|x| Lisbn.new(x).valid? }.uniq
print_isbns.select{|x| Lisbn.new(x).valid? }.group_by(&:itself).select{|isbn, isbns| isbns.many? }

(print_isbns - client.books.pluck(:isbn)).count

print_isbns - client.books.pluck(:isbn)

print_isbns.map{|x| ISBNReader.new(isbn: x).isbn_with_spaces.split(" ").first(3).join("-")}.group_by(&:itself).transform_values(&:size)
ebook_isbns.map{|x| ISBNReader.new(isbn: x).isbn_with_spaces.split(" ").first(3).join("-")}.group_by(&:itself).transform_values(&:size)

rows.select{|r| [r["ISBN13Epub"].to_s, r["ISBN13Mobi"].to_s, r["ISBN13PrintUK"].to_s, r["ISBN13PrintUS"].to_s].any?{|x| x.starts_with? "978185798"}}.map{|r| r["Title"]}


rows.select{|r| r["ISBN13PrintUK"].blank? && r["ISBN13PrintUS"].blank?}.map{|r| r["Title"]}
rows.reject{|r| r["ISBN13PrintUK"].blank? && r["ISBN13PrintUS"].blank?}.map{|r| r["PrintBookForm"]}

system_isbns = client.books.pluck(:isbn)

rows.select{|r| (r["ISBN13PrintUK"].present? ||r["ISBN13PrintUS"].present?) && (!r["ISBN13PrintUK"].present? || !r["ISBN13PrintUK"].in?(system_isbns)) || (!r["ISBN13PrintUS"].present? && !r["ISBN13PrintUS"].in?(system_isbns))}.map{|r| r["Title"]}




sheet_contributors = rows.flat_map{|r| [r["Creator1SortName"], r["Creator2SortName"], r["Creator3SortName"], r["Creator4SortName"]]}.compact.uniq.sort
system_contributors = client.contacts.map(&:person_name_inverted)
new_contributors = sheet_contributors - system_contributors
new_contributors.map{|c| [c, client.contacts.where("lower(keynames) = lower(?)", c.split(",").first.squish.downcase).pluck(:person_name_inverted)]}.select{|x| x.last.any?}
new_contributors.select{|c| client.contacts.where("lower(keynames) = lower(?)", c.split(",").first.squish.downcase).pluck(:person_name_inverted)]}.select{|x| x.last.any?}
