ActiveRecord::Base.transaction do
  Work.no_touching do
    Client.is_liverpool.
      marketingtexts.where(main_type: "31").
      where.not(pull_quote: '').
      each do |text|
        text.update(
          pull_quote:     '',
          marketing_text: text.pull_quote
          )
      end
  end
end


ActiveRecord::Base.transaction do
  Work.no_touching do
    Client.is_liverpool.
      marketingtexts.where(main_type: "17").
      where.not(pull_quote: '').
      each do |text|
        text.update(
          pull_quote:     '',
          marketing_text: text.pull_quote
          )
      end
  end
end


