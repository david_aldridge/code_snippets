books = Client.is_bds.books ; 0

Benchmark.bm(13) do |x|
  x.report("array 1")      { books.select{|b| b.bookcontacts.count > 1 } }
  x.report("array 1")      { books.select{|b| b.bookcontacts.limit(2).count > 1 } }
  x.report("array 1")      { books.select{|b| b.zero_or_one_contributor? } }
end ; ""

more_than_one_contributor?