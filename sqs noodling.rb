https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/SQS/Client.html#receive_message-instance_method


sqs       = Aws::SQS::Client.new
queue_url = "https://sqs.eu-west-1.amazonaws.com/708746154661/consonance-test-sqs.fifo"

message =  sqs.receive_message(queue_url: queue_url)


resp = sqs.send_message({
  queue_url: queue_url,
  message_body: "Test message body",
  message_group_id: "String",
  message_deduplication_id: "987654"})

first_message = message.messages.first

 sqs.delete_message({
  queue_url: queue_url,
  message_body: "Test message body",
  message_group_id: "String",
  message_deduplication_id: "987654"})

resp = sqs.delete_message_batch({
  queue_url: queue_url,
  entries: [
    {
      id: first_message.message_id,
      receipt_handle: first_message.receipt_handle
    }
  ]
})