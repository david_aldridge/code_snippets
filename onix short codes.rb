f = File.open "/Users/david/rails/consonance/lib/onix/v3.0/dtd/ONIX_BookProduct_3.0_reference.dtd"

pairs = f.read.scan(/refname \((.*)\).*\n.*shortname \((.*)\)/)
subs = pairs.flat_map do |ref, short|
  this =
    [
      ["<#{ref}>", "<#{short}>"],
      ["</#{ref}>", "</#{short}>"]
    ]
  this << ["<#{ref}/>", "<#{short}>"] if ref.starts_with? "No"
  this
end.to_h

subs = pairs.flat_map do |ref, short|
  this =
    [
      ["<#{ref}>", "<#{short}>"],
      ["</#{ref}>", "</#{short}>"]
    ]
  this << ["<#{ref}/>", "<#{short}>"] if ref.starts_with? "No"
  this
end.to_h


onix = <<~ONIX
<?xml version="1.0" encoding="utf-8"?>
<ONIXMessage xmlns="http://ns.editeur.org/onix/3.0/reference" release="3.0">
  <Header>
    <Sender>
      <SenderName>Kogan Page</SenderName>
      <ContactName>Martin Klopstock</ContactName>
      <EmailAddress>mklopstock@koganpage.com</EmailAddress>
    </Sender>
    <SentDateTime>20190607</SentDateTime>
    <DefaultLanguageOfText>eng</DefaultLanguageOfText>
  </Header>
  <Product>
    <RecordReference>KoganPage12643</RecordReference>
    <NotificationType>02</NotificationType>
    <RecordSourceType>01</RecordSourceType>
    <RecordSourceName>Kogan Page</RecordSourceName>
    <ProductIdentifier>
      <ProductIDType>01</ProductIDType>
      <IDTypeName>Virtusales Biblio ID</IDTypeName>
      <IDValue>12643</IDValue>
    </ProductIdentifier>
    <ProductIdentifier>
      <ProductIDType>02</ProductIDType>
      <IDValue>1789664543</IDValue>
    </ProductIdentifier>
    <NoContributor/>
    <ProductIdentifier>
ONIX



doc = Nokogiri::XML.parse(onix)

from, to = ["NotificationType", "a002"]
pairs.each do |from, to|
  doc.xpath("//xmlns:#{from}").each{|element| element.name=to}
end

pp doc.to_xml



onix.gsub /<(.*?)>/ do |match|
  puts match
  subs[match] || match
end



subs  = {"<Addressee>"=>"<addressee>", "AddresseeIdentifier"=>"addresseeidentifier"}

onix = "<Addressee> blah<Addressee>"

onix.gsub /<(.*?)>/ do |match|
  puts match
  subs[match] || match
end




Nokogiri::XML::DTD.parse(DTD)