require 'benchmark'

n = 100000
Benchmark.bm(7) do |x|
  x.report("method 0")   { n.times {} }
  x.report("method 1")   { n.times {nil}}
end ; ""



Benchmark.bm(7) do |x|
  x.report("lite1")   { Book.limit(5000).includes(:marketingtexts_lite1).select{|b| b.marketingtexts_lite1.blank?}.size}
  x.report("lite2")   { Book.limit(5000).includes(:marketingtexts_lite1).select{|b| b.marketingtexts_lite1.blank?}.size}
  x.report("sprlit1") { Book.limit(5000).includes(:marketingtexts_lite2).select{|b| b.marketingtexts_lite2.blank?}.size}
  x.report("sprlit2") { Book.limit(5000).includes(:marketingtexts_lite2).select{|b| b.marketingtexts_lite2.blank?}.size}
  x.report("norm")    { Book.limit(5000).includes(:marketingtexts).select{|b| b.marketingtexts.blank?}.size}
  x.report("norm")    { Book.limit(5000).includes(:marketingtexts).select{|b| b.marketingtexts.blank?}.size}
end


