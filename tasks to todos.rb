ActiveRecord::Base.transaction do
  Work.no_touching do
    tasks = Client.is_iop.works.find(818).tasks.reject(&:has_children?)

    tasks.each do |task|
        work = task.work
        next unless work
        earliest_pub_date = work.books.minimum(:pub_date)
        Todo.create(
          client_id:                      task.client_id,
          object_type:                    "Work",
          object_id:                      task.work.id,
          name:                           task.name,
          done_at:                        (task.updated_at if task.done?),
          discarded_at:                   nil,
          fixed_due_date:                 (task.end unless earliest_pub_date),
          relative_to:                    (0 if earliest_pub_date),
          months_before:                  (0 if earliest_pub_date),
          days_before:                    ((earliest_pub_date - task.end) if earliest_pub_date),
          assignee_id:                    task.users.first&.id,
          due_date_when_marked_as_done: (task.end if task.completed? && earliest_pub_date.present?)
        )
      end
  end
end
