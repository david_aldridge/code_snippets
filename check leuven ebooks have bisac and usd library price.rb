Client.is_leuven.
  imprints.where(identifier: ["leuven-university-press", "universitaire-pers-leuven"]).
  flat_map(&:books).
  select(&:is_ebook?).
  select(&:active_or_forthcoming?).
  reject(&:main_bisac_code).
  map(&:isbn)

9789461660060
9789461661135
9789461661159
9789461661166
9789461661012
9789461661029
9789461661036
9789461660473
9789461660480
9789461660053
9789461660435
9789461660442
9789461660756
9789461660985
9789461661098
9789461661104
9789461661142

Client.is_leuven.
  imprints.where(identifier: ["leuven-university-press", "universitaire-pers-leuven"]).
  flat_map(&:books).
  select(&:is_ebook?).
  select(&:active_or_forthcoming?).
  reject{|b| b.prices.usd.library}.
  map(&:isbn)

[]

Client.is_leuven.
  imprints.where(identifier: ["leuven-university-press", "universitaire-pers-leuven"]).
  flat_map(&:books).
  select(&:is_ebook?).
  select(&:active_or_forthcoming?).
  reject{|b| b.language.presence ||b.language_of_text.try(:language_code)}.
  map(&:isbn)
