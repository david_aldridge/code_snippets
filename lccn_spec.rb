require "rails_helper"
# rubocop:disable Style/SpaceInsideBrackets, Style/SpaceBeforeComma
describe LCCN do
  normalised_pairs =
    {
      "n78-890351"         => "n78890351" ,
      "n78-89035"          => "n78089035" ,
      "n 78890351 "        => "n78890351" ,
      " 85000002 "         => "85000002"  ,
      "85-2 "              => "85000002"  ,
      "2001-000002"        => "2001000002",
      "75-425165//r75"     => "75425165"  ,
      " 79139101 /AC/r932" => "79139101"
    }

  invalid_set =
    [
      "n78--890351" ,
      "n78-8903X1"  ,
      "878-8903X11" ,
      "cn78-89035"  ,
      "n 78890351 " ,
      "n 788903511 ",
      "n 7889035"   ,
      "nn 7880358"  ,
      "98533-2 "    ,
      "001-0x0002"
    ]

  normalised_pairs.keys.each do |valid_lccn|
    it "recognises \"#{valid_lccn}\" as valid" do
      expect(LCCN.new(valid_lccn).valid?).to eq true
    end
  end

  normalised_pairs.each do |lccn, normalised_lccn|
    it "normalises \"#{lccn}\"" do
      expect(LCCN.new(lccn).normalised).to eq normalised_lccn
    end
  end

  invalid_set.each do |invalid_lccn|
    it "recognises \"#{invalid_lccn}\" as invalid" do
      expect(LCCN.new(invalid_lccn).valid?).to eq false
    end
  end
end
