select
  c.id,
  contribution_types,
  'A' = ANY(contribution_types) content_contributor,
  'B' = ANY(contribution_types) adaptation_contributor,
  'C' = ANY(contribution_types) compilation_contributor,
  'D' = ANY(contribution_types) direction_contributor,
  'E' = ANY(contribution_types) performance_contributor,
  'F' = ANY(contribution_types) recording_contributor,
  'Z' = ANY(contribution_types) other_contributor,
  'A01' = ANY(contribution_roles) author_of_text,
  'A08' = ANY(contribution_roles) photographer,
  'A12' = ANY(contribution_roles) illustrator,
  -- ARRAY['B01'::Text, 'B09'::Text, 'B10'::Text, 'B11'::Text, 'B12'::Text, 'B13'::Text, 'B14'::Text, 'B15'::Text, 'B16'::Text, 'B19'::Text, 'B20'::Text, 'B21'::Text, 'B24'::Text, 'B26'::Text, 'B29'::Text, 'B31'::Text] && contribution_roles editor,
  (select count(*) from workcontacts wc where wc.contact_id = c.id) contributions_count,
  exists (select null from print_on_demand_specs x where x.printer_contact_id = c.id) on_demand_printer
from
  contacts c left join
  (
    select
      contact_id,
      array_agg(distinct work_contact_role) contribution_roles,
      array_agg(distinct left(work_contact_role,1)) contribution_types
    from
      workcontacts
    group by
      contact_id
  ) contributions on contributions.contact_id = c.id
;
