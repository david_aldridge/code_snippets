x = Client.is_bds.
  contacts.
  is_nbni.
  contact_digital_asset_transfer_templates.
  first.
  export_batches.
  new(client: Client.is_bds, name: "Fix", incremental: false)

x.client = Client.is_bds

x.books = x.client.works.find(61587).books
