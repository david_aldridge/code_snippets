require "/Users/david/Documents/Bibliocloud/Clients/Rebellion/temp/bisacs.rb"

client = Client.is_mcgill_queens

Client.is_mcgill_queens.works.where(main_bisac_code: nil).count
=> 8
Client.is_mcgill_queens.works.where.not(main_bisac_code: nil).count
=> 3515

irb(main):004:0> Client.is_mcgill_queens.works.where(main_bisac_code: nil).count
=> 3281 (8605 -> 5870)
irb(main):005:0> Client.is_mcgill_queens.works.where.not(main_bisac_code: nil).count
=> 606


ActiveRecord::Base.transaction do
  Work.no_touching do
    Temp.values.each do |isbn, main_bisac_code|
      work = client.works.joins(:books).where(main_bisac_code: nil).find_by(books: {isbn: isbn})

      work.update(main_bisac_code: main_bisac_code) if work
    end
  end
end ; 0
