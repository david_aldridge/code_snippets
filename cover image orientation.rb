BookDimensions = Struct.new(client, isbn, d1, d2, d3, weight) {
  def book
    client.books.find_by(isbn: isbn)
  end

  def aspect
    return unless book
    file_name = "#{isbn}.jpg"
    file = book.supportingresources&.image&.copy_to_local_file(:jpg_rgb_0050w, file_name)
    return nil unless file
    img = Magick::Image::read(file_name).first
    img.columns <=> img.rows
  end
}
dimensions = [
[97812345678,1,2,3,4]
].map do |numbers|

Client.is_quiller.supportingresources.first(10).map do |r|
  r.image.copy_to_local_file(:jpg_rgb_0050w, "x.jpg")
  img = Magick::Image::read("x.jpg").first
  [r.work_id, img.columns, img.rows]
end
