brew install jemalloc
xcode-select --install
curl -L https://get.rvm.io | bash -s stable --autolibs=enabled
rvm cleanup all
rvm reload
rvm reinstall 2.2.5 -C --with-jemalloc



ruby -r rbconfig -e "puts RbConfig::CONFIG['LIBS']"


heroku buildpacks:set https://github.com/mojodna/heroku-buildpack-jemalloc -i 1 -a bibliocloud-demo
heroku buildpacks:add --index 1 https://github.com/mojodna/heroku-buildpack-jemalloc.git#v3.6.0 -a bibliocloud-demo

heroku buildpacks:set https://github.com/ello/heroku-buildpack-imagemagick -i 2 -a bibliocloud-demo
heroku buildpacks:set heroku/nodejs -i 3 -a bibliocloud-demo
heroku buildpacks:set heroku/ruby -i 4 -a bibliocloud-demo

heroku config:set LD_PRELOAD=/app/vendor/jemalloc/lib/libjemalloc.so.1 -a bibliocloud-demo



git commit --allow-empty -m "empty commit"
git push demo master
