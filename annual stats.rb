last_year = ((Date.today.beginning_of_year-12.months)..(Date.today.beginning_of_year - 1.days))
previous_year = ((Date.today.beginning_of_year-24.months)..(Date.today.beginning_of_year - 1.days - 12.months))

DigitalAsset.where(created_at: last_year).count
DigitalAsset.where(created_at: previous_year).count
DigitalAssetContentItem.where(created_at: last_year).count
DigitalAssetContentItem.where(created_at: previous_year).count
