doc       = Nokogiri::XML(File.open("/Users/david/Downloads/nbni_all1805170600_7.xml")) ; 0

known_keys = FileImport::NBNI::PriceAndAvailabilityONIX.publisher_id_client_map.keys

missing_maps = doc.xpath("/xmlns:ONIXMessage/xmlns:Product/xmlns:Publisher").map do |x|
  [
    x.at_xpath("xmlns:PublisherName").text,
    x.at_xpath("xmlns:NameCodeValue").text
  ]
end.uniq.reject{|x| x.last.in?(known_keys) }

correlated = missing_maps.map do |m|
  client_record = Client.find_by("lower(client_name) = ?", m.first.downcase)
  id = client_record&.id
  identifier = client_record&.identifier
  m << id
  m << identifier
end


pp (missing_maps.map{|x| [x.first.downcase, "NBNI",x[1],""]} + Client.all.map{|x| [x.client_name.downcase, "BC", x.id, x.identifier]}).sort


obsolete = [267, 52, 269, 294]







found = correlated.select{|x| x[2].present?}

pp found
