doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/Crimson/import/crimson_onix.xml")).document ; 0
doc.remove_namespaces! ; 0

"https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/crimson.xml"


doc = Nokogiri::XML(open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/crimson.xml")).document ; 0
doc.remove_namespaces! ; 0


doc.xpath("/ONIXMessage/Product").map do |product_node|
  reader = ONIX::XML::Product21.new(node: product_node)
  isbn = reader.product_identifiers.select(&:isbn13).first&.value
  next unless isbn
  book = Client.is_crimson.books.find_by(isbn: isbn)
  #book = Book.find_by(isbn: isbn)
  unless book
    puts "No book found for ISBN #{isbn}"
    next
  end
  prices = reader.supply_details.flat_map(&:prices).map do |price_composite|
    ONIX::PriceCompositeToAttributes.new(price_composite: price_composite).call
  end

  # prices.map{|p| [isbn, p[:currency_code], p[:price_amount], p[:price_type]]}

  prices.map do |p|
    unless book.prices.exists?(currency_code: p[:currency_code])
      puts book.id, book.product_form, p[:currency_code],p[:price_amount], "-#{p[:price_type]}", "---"
      book.current_gbp_inctax_consumer_price = p[:price_amount]
    end
  end
end ; 0
