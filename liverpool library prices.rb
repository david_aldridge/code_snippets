Client.is_liverpool.
       books.
       is_ebook.
       each do |ebook|

  if ebook.current_gbp_inctax_library_price.nil? && ebook.current_gbp_inctax_consumer_price
    ebook.current_gbp_inctax_library_price = (ebook.current_gbp_inctax_consumer_price * 2.25).round(2)
  end

  if ebook.current_usd_exctax_library_price.nil? && ebook.current_usd_exctax_consumer_price
    ebook.current_usd_exctax_library_price = (ebook.current_usd_exctax_consumer_price * 2.25).round(2)
  end

end
