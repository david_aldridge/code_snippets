pp Client.is_pharma.works.group(:bic_geog_codes, :bisac_geog_codes).count

geog_mapping =
{
  ["1DBK"]   => ["1.1.2.0.0.0.0"],
  ["1DBKE", "1DBKW"]=>  ["1.1.2.2.0.0.0", "1.1.2.6.0.0.0"],
  ["1DBR"]   => ["1.1.1.0.0.0.0"],
  ["1D"]     => ["1.0.0.0.0.0.0"],
  ["1DB"]    => ["1.1.0.0.0.0.0"],
  ["1DBKE"]  => ["1.1.2.2.0.0.0"],
  ["1KBB"]   => ["4.0.1.0.0.0.0"]
  }


Client.is_pharma.works.reject{|w| w.bic_geog_codes.empty?}.each do |work|
  work.bisac_geog_codes = geog_mapping[work.bic_geog_codes]
  work.save
end
