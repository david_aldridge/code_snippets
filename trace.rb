def start_trace
  stack = []
  trace = TracePoint.new(:call) { |tp| stack += [tp.path, tp.lineno, tp.event, tp.method_id] }

  trace.enable
  yield
  trace.disable
  stack
end

trace = start_trace { Book.first.onix}

trace.reject{|t| t.contains? "/.rvm/gems/"}
