class Resequencer
  def initialize(klass:, id_col: "id", sequence_attribute_name: "sequence", id_seq_pairs:)
    @id_col                  = id_col
    @klass                   = klass
    @sequence_attribute_name = sequence_attribute_name
    @id_seq_pairs            = id_seq_pairs
  end

  def call
    klass.
      update_all(sql_set).
      where(id: ids).
      where.not(sql_where)
  end

  private

  attr_reader :id_col, :klass, :sequence_attribute_name, :id_seq_pairs

  def sql_case
    @_case ||=unmemoized_sql_case
  end

  def unmemoized_sql_case
    out = "case id"
    id_seq_pairs.each {|id, s| out << " when #{id} then #{s}"}
    out << " end"
    out
  end

  def sql_set
    "#{sequence_attribute_name} = #{sql_case}"
  end

  def sql_where
    "id = #{sql_case}"
  end

  def ids
    id_seq_pairs.map(&:first)
  end
end

__end__

irb Resequencer.new(klass: Book, id_seq_pairs: [[1,1], [2,4], [3,6]])
