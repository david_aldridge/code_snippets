require 'benchmark'

def method1
  ONIXValue::Country.codes.each { |code| ONIXValue::Country.new("GB") }
end

n = 10000
Benchmark.bm(7) do |x|
  x.report("method 1")   { n.times {method1}}
end ; ""



GC.start
GC.disable

pp ObjectSpace.count_objects

100_000.times {ONIXValue::Country.codes.each { |code| ONIXValue::Country.new(code) }}

pp ObjectSpace.count_objects







GC.start
GC.disable
pp ObjectSpace.count_objects
Book.select(:epub_type_code) ; 0

pp ObjectSpace.count_objects


{:TOTAL=>1110286,    {:TOTAL=>76397252,      {:TOTAL=>25997124,   {:TOTAL=>968044,
 :FREE=>392364,       :FREE=>387,             :FREE=>247,          :FREE=>170664,
 :T_OBJECT=>59486,    :T_OBJECT=>25266837,    :T_OBJECT=>66837,    :T_OBJECT=>67088,
 :T_CLASS=>12979,     :T_CLASS=>12981,        :T_CLASS=>12981,     :T_CLASS=>12981,
 :T_MODULE=>2078,     :T_MODULE=>2078,        :T_MODULE=>2078,     :T_MODULE=>2078,
 :T_FLOAT=>11,        :T_FLOAT=>11,           :T_FLOAT=>11,        :T_FLOAT=>11,
 :T_STRING=>280850,   :T_STRING=>25516267,    :T_STRING=>25516270  :T_STRING=>316267,
 :T_REGEXP=>5740,     :T_REGEXP=>5742,        :T_REGEXP=>5742,     :T_REGEXP=>5742,
 :T_ARRAY=>132187,    :T_ARRAY=>141744,       :T_ARRAY=>141746,    :T_ARRAY=>141747,
 :T_HASH=>15394,      :T_HASH=>21811,         :T_HASH=>21813,      :T_HASH=>21813,
 :T_STRUCT=>6090,     :T_STRUCT=>6602,        :T_STRUCT=>6602,     :T_STRUCT=>6602,
 :T_BIGNUM=>10,       :T_BIGNUM=>10,          :T_BIGNUM=>10,       :T_BIGNUM=>10,
 :T_FILE=>18,         :T_FILE=>18,            :T_FILE=>18,         :T_FILE=>18,
 :T_DATA=>21753,      :T_DATA=>24126,         :T_DATA=>24126,      :T_DATA=>24126,
 :T_MATCH=>65,        :T_MATCH=>1160,         :T_MATCH=>1160,      :T_MATCH=>1162,
 :T_COMPLEX=>1,       :T_COMPLEX=>1,          :T_COMPLEX=>1,       :T_COMPLEX=>1,
 :T_RATIONAL=>907,    :T_RATIONAL=>14011,     :T_RATIONAL=>14011,  :T_RATIONAL=>14011,
 :T_SYMBOL=>4928,     :T_SYMBOL=>4928,        :T_SYMBOL=>4928,     :T_SYMBOL=>4928,
 :T_IMEMO=>171779,    :T_IMEMO=>25374855,     :T_IMEMO=>174860,    :T_IMEMO=>175112,
 :T_NODE=>116,        :T_NODE=>153,           :T_NODE=>153,        :T_NODE=>153,
 :T_ICLASS=>3530}     :T_ICLASS=>3530}        :T_ICLASS=>3530}     :T_ICLASS=>3530}


{:TOTAL=>1110299,   {:TOTAL=>1545202,   {:TOTAL=>1545203,
 :FREE=>392430,      :FREE=>695,         :FREE=>688,
 :T_OBJECT=>59484,   :T_OBJECT=>68210,   :T_OBJECT=>68209,
 :T_CLASS=>12979,    :T_CLASS=>13117,    :T_CLASS=>13117,
 :T_MODULE=>2078,    :T_MODULE=>2124,    :T_MODULE=>2124,
 :T_FLOAT=>11,       :T_FLOAT=>11,       :T_FLOAT=>11,
 :T_STRING=>280807,  :T_STRING=>737160,  :T_STRING=>737162,
 :T_REGEXP=>5740,    :T_REGEXP=>5796,    :T_REGEXP=>5796,
 :T_ARRAY=>132181,   :T_ARRAY=>276232,   :T_ARRAY=>276234,
 :T_HASH=>15394,     :T_HASH=>43640,     :T_HASH=>43641,
 :T_STRUCT=>6090,    :T_STRUCT=>6122,    :T_STRUCT=>6122,
 :T_BIGNUM=>10,      :T_BIGNUM=>10,      :T_BIGNUM=>10,
 :T_FILE=>18,        :T_FILE=>21,        :T_FILE=>21,
 :T_DATA=>21753,     :T_DATA=>77848,     :T_DATA=>77848,
 :T_MATCH=>61,       :T_MATCH=>3426,     :T_MATCH=>3426,
 :T_COMPLEX=>1,      :T_COMPLEX=>1,      :T_COMPLEX=>1,
 :T_RATIONAL=>907,   :T_RATIONAL=>907,   :T_RATIONAL=>907,
 :T_SYMBOL=>4928,    :T_SYMBOL=>17976,   :T_SYMBOL=>17976,
 :T_IMEMO=>171781,   :T_IMEMO=>250245,   :T_IMEMO=>250249,
 :T_NODE=>116,       :T_NODE=>37939,     :T_NODE=>37939,
 :T_ICLASS=>3530}    :T_ICLASS=>3722}    :T_ICLASS=>3722}




{:TOTAL=>1545203,
 :FREE=>688,
 :T_OBJECT=>68209,
 :T_CLASS=>13117,
 :T_MODULE=>2124,
 :T_FLOAT=>11,
 :T_STRING=>737162,
 :T_REGEXP=>5796,
 :T_ARRAY=>276234,
 :T_HASH=>43641,
 :T_STRUCT=>6122,
 :T_BIGNUM=>10,
 :T_FILE=>21,
 :T_DATA=>77848,
 :T_MATCH=>3426,
 :T_COMPLEX=>1,
 :T_RATIONAL=>907,
 :T_SYMBOL=>17976,
 :T_IMEMO=>250249,
 :T_NODE=>37939,
 :T_ICLASS=>3722}
