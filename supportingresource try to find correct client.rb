Publishername.is_morgan_claypool.works.flat_map(&:supportingresources).map do |sr|
  if sr.image.exists?
    "correct"
  else
    sr.update_columns(client_id: 4)
    if sr.image.exists?
      "correct after switch"
    else
      "can't find"
    end
  end
end.uniq


Supportingresource.all.map do |sr|
  if sr.image.exists?
    "correct"
  else
    sr.update_attributes(client_id: (sr.client_id == 4 ? 3 : 4) )
    if sr.image.exists?
      "correct after switch"
    else
      "can't find"
    end
  end
end.group_by(&:itself).transform_values(&:size)



 Supportingresource.joins(books: {work: {imprint: :publishername}}).where(publishernames: {identifier: Client::MORGAN_CLAYPOOL}).uniq.update_all(client_id: 3)
