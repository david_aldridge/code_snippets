select id,
 work_id work_id_on_contracts,
  (select id from works                where contract_id = c.id) work_referencing_contract               ,
  (select count(*) from advances             where contract_id = c.id) +
  (select count(*) from contract_attachments where contract_id = c.id) +
  (select count(*) from contractcontributors where contract_id = c.id) +
  (select count(*) from licensed_rights      where contract_id = c.id) +
  (select count(*) from masterrules          where contract_id = c.id) +
  (select count(*) from ms_delivery_dates    where contract_id = c.id) +
  (select count(*) from payments             where contract_id = c.id) +
  (select count(*) from rightrules           where contract_id = c.id) +
  (select count(*) from schedules              where schedulable_type              = 'Contract' and schedulable_id              = c.id)+
  (select count(*) from digital_asset_contents where digital_asset_includable_type = 'Contract' and digital_asset_includable_id = c.id)+
  (select count(*) from notes                  where notable_type                  = 'Contract' and notable_id                  = c.id) others,
  updated_at
from
contracts c
where id not in (select contract_id from works)
order by 2,1;



  (select count(*) from schedules              where schedulable_type              = 'Contract' and schedulable_id              = c.id)
  (select count(*) from digital_asset_contents where digital_asset_includable_type = 'Contract' and digital_asset_includable_id = c.id)
  (select count(*) from notes                  where notable_type                  = 'Contract' and notable_type_id             = c.id)     





select id,
 work_id work_id_on_contracts,
  (select count(*) from advances             where contract_id = c.id) +
  (select count(*) from contract_attachments where contract_id = c.id) +
  (select count(*) from contractcontributors where contract_id = c.id) +
  (select count(*) from licensed_rights      where contract_id = c.id) +
  (select count(*) from masterrules          where contract_id = c.id) +
  (select count(*) from ms_delivery_dates    where contract_id = c.id) +
  (select count(*) from payments             where contract_id = c.id) +
  (select count(*) from rightrules           where contract_id = c.id) +
  (select count(*) from schedules              where schedulable_type              = 'Contract' and schedulable_id              = c.id)+
  (select count(*) from digital_asset_contents where digital_asset_includable_type = 'Contract' and digital_asset_includable_id = c.id)+
  (select count(*) from notes                  where notable_type                  = 'Contract' and notable_id                  = c.id) others,
  updated_at
from
contracts c
where work_id in (select work_id from contracts group by work_id having count(*) > 1) and 
not exists (select null from works                where contract_id = c.id)
order by 2,1;



delete from
  contracts
where
  not exists (select null
                from works
               where works.contract_id = contracts.id)
/













select
  c.id,
  w.id,
  b.id,
  co.id,
  case when c.contract_name = w.title and c.contract_name =  b.title then true else false end matches,
  c.contract_name,
  w.title,
  b.title,
  co.person_name_inverted
from contracts c
left join works w on w.contract_id = c.id
left join books b on b.work_id = w.id
left join workcontacts wc on wc.work_id = w.id
left join contacts co on co.id = wc.contact_id
where lower(c.contract_name) like '%dark%' or 
      lower(w.title) like '%dark%'
order by 1,2,3,4;


