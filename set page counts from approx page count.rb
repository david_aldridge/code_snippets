Client.is_restless_books.books.is_print.
  where.not(pages_arabic: nil).
  where(production_page_count: nil).
  update_all("production_page_count = pages_arabic")


Client.is_restless_books.books.ebook.
  where.not(epub_type_code: "002").
  where.not(pages_arabic: nil).
  where(number_of_pages_in_print_counterpart: nil).
  update_all("number_of_pages_in_print_counterpart = pages_arabic")





Client.is_ucl.books.is_print.
  where.not(pages_arabic: nil).
  where(production_page_count: nil).
  update_all("production_page_count = pages_arabic")


Client.is_ucl.books.ebook.
  where.not(epub_type_code: "002").
  where.not(pages_arabic: nil).
  where(number_of_pages_in_print_counterpart: nil).
  update_all("number_of_pages_in_print_counterpart = pages_arabic")

  Client.is_ucl.books.ebook.pdfs.
  where.not(pages_arabic: nil).
  where(absolute_page_count: nil).
  update_all("absolute_page_count = pages_arabic, number_of_pages_in_print_counterpart = null")
