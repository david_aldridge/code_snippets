client = Client.is_purdue
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1IFyQ34GyDvzvKer1l8pDvRjuEMEpdPDG41mf0rvgkQA',
                            worksheet_name:    'Sheet1',
                            import_class:      nil,
                            rollback_on_error: true)


row = import.send(:list).first

  book = client.books.find_by(isbn: row["ISBN"])

  def order_number
    row["Order Number"]
  end

  def invoice_date
    row["Order Date"]
  end

  def isbn
    row["ISBN"]
  end

  def quantity
    row["Order Quantity"]
  end
  row["Ship Quantity"]
  row["Line Price"]
  row["Ship Amount"]
  row["Tax Amount"]
  def discount
    row["Discount Percent"].to_d
  end
  row["Line Status"]
  row["Order Type"]
  row["PO Number"]
  row["Bill Name"]
  row["Customer Number"]
  row["Ship Name"]
  row["Ship Date"]
  row["Shipping Carrier"]
  row["Shipping Method Description"]
  row["Ship Tracking"]
  row["Source Code"]
end


import.send(:list).each do |row|
  work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
  unless work
    puts "No work for #{row['Book Ref']}"
    next
  end
  brief       = work.briefs.where(:title => 'Imported').first_or_create!
  brief.books = work.books
  topics.each do |topic|
    contents = row[topic].squish.presence
    next unless contents
    note_title = topic.gsub(/^[^"]*"|"[^"]*/,'')
    brief.briefing_texts.where(:briefing_text_type => note_title).first_or_create!(:briefing_text => contents)
  end
end
