require 'objspace'
require 'ostruct'


hash2 = {a: 1, b: 2}
Y2 = Struct.new(:a, :b)
struct2 = Y2.new(1,2)
open_struct2 = OpenStruct.new(a: 1, b: 2)
array2 = [1,2]
set2 = Set.new array2
ObjectSpace.memsize_of(hash2)
ObjectSpace.memsize_of(struct2)
ObjectSpace.memsize_of(open_struct2)
ObjectSpace.memsize_of(array2)
ObjectSpace.memsize_of(set2)


hash3 = {a: 1, b: 2, c: 3}
Y3 = Struct.new(:a, :b, :c)
struct3 = Y3.new(1,2,3)
open_struct3 = OpenStruct.new(a: 1, b: 2, c: 3)

ObjectSpace.memsize_of(hash3)
ObjectSpace.memsize_of(struct3)
ObjectSpace.memsize_of(open_struct3)


hash7 = {a: 1, b: 2, c: 3, d: 4, e: 5, f: 6, g: 7}
Y7 = Struct.new(:a, :b, :c, :d, :e, :f, :g)
struct7 = Y7.new(1,2,3,4,5,6,7)
open_struct7 = OpenStruct.new(a: 1, b: 2, c: 3, d: 4, e: 5, f: 6, g: 7)
array7 = [1,2,3,4,5,6,7]
set7 = Set.new array7
sortedset7 = SortedSet.new array7
ObjectSpace.memsize_of(hash7)
ObjectSpace.memsize_of(struct7)
ObjectSpace.memsize_of(open_struct7)
ObjectSpace.memsize_of(array7)
ObjectSpace.memsize_of(set7)
ObjectSpace.memsize_of(sortedset7)

array = (1..1000).to_a
set = Set.new array
sset = SortedSet.new array
ObjectSpace.memsize_of(array)
ObjectSpace.memsize_of(set)
ObjectSpace.memsize_of(sset)



Y7 = Struct.new(:a, :b, :c, :d, :e, :f, :g) {
  def to_s
    "hello"
  end
}
struct7 = Y7.new(1,2,3,4,5,6,7)
