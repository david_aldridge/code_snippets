isbn = "9780306406150"

def split_regexp(isbn)
  isbn.split(//)
end

def split_string(isbn)
  isbn.split('')
end

def scan_digit(isbn)
  isbn.scan(/\d/)
end

def chars(isbn)
  isbn.chars
end

def unpack(isbn)
  isbn.unpack('aaaaaaaaaaaaa')
end


split_regexp(isbn)
split_string(isbn)
scan_digit(isbn)
chars(isbn)
unpack(isbn)

require 'benchmark'

n = 300000
Benchmark.bm(12) do |x|
  x.report("split regexp")   { for i in 1..n; split_regexp(isbn); end }
  x.report("split_string")   { for i in 1..n; split_string(isbn); end }
  x.report("scan_digit")   { for i in 1..n; scan_digit(isbn); end }
  x.report("chars")   { for i in 1..n; scan_digit(isbn); end }
  x.report("unpack")   { for i in 1..n; unpack(isbn); end }
end ; ""
