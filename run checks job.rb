Client.is_rebel_girls.works.pluck(:id).each do |work_id|
  Delayed::Job.enqueue(
    UpdateWorkChecksJob.new(work_id: work_id),
    queue: DELAYED_JOB_QUEUES.default
  )
end ; 0


"--- !ruby/object:UpdateWorkChecksJob\nwork_id: 5852\n"

insert into delayed_jobs(
  priority,
  attempts,
  handler,
  last_error,
  run_at,
  locked_at,
  failed_at,
  locked_by,
  created_at,
  updated_at,
  queue,
  --job_object_id,
  client_id,
  user_id
)
select
  9 as priority,
  0 as attempts,
  '--- !ruby/object:UpdateWorkChecksJob'
    || E'\n'
    || 'work_id: '
    || w.id
    || E'\n'
    as handler,
  null           as last_error,
  localtimestamp as run_at,
  null           as locked_at,
  null           as failed_at,
  null           as locked_by,
  localtimestamp   as created_at,
  localtimestamp   as updated_at,
  'default'      as queue,
  --null           as job_object_id,
  null           as client_id,
  null           as user_id
from
  works w join clients c on c.id = w.client_id
  where c.identifier = 'mcgill-queens';

and  exists (
  select null from check_results where object_id = w.id and object_type = 'Work'
)
limit 100000;
