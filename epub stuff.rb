client = Client.is_pharma

ProductionFile.epubs.each do |epub_file_record|
  puts "Reading #{epub_file_record.work.title} ..."
  attachment = epub_file_record.attachment
  next unless attachment.exists?
  epub_file = Tempfile.new ['', ".epub"]
  epub_file.binmode
  epub_file.write open(attachment.url).read
  epub_file.rewind

  unzipped_file = Zip::File.open("/Users/david/Documents/Bibliocloud/Clients/Kogan Page/ExemplarProduct_EPUB.epub")
  container_entry = unzipped_file.get_entry("META-INF/container.xml")


  container_doc = Nokogiri::HTML(container_entry.get_input_stream.read) ; ""
content_file_full_path = container_doc.at_xpath("//rootfile").attribute("full-path").value
content_file_entry = unzipped_file.get_entry(content_file_full_path)
content_doc = Nokogiri::HTML(content_file_entry.get_input_stream.read) ; ""
package = content_doc.at_xpath("html/body/package")

  version = package.attribute("version").value.to_f
  package.attribute("unique-identifier").value
  package.xpath("metadata/identifier").map(&:text)
  package.xpath("metadata/title").map(&:text)
  package.xpath("metadata/language").map(&:text)
  nav_file_name = package.at_xpath("manifest/item[@properties='nav']").attribute("href").text

  toc_file_entry = unzipped_file.get_entry(nav_file_name)

  toc_doc = Nokogiri::HTML(toc_file_entry.get_input_stream.read) ; ""

  unzipped_file.close
  epub_file.close

  toc_doc.xpath('//@*').remove

  toc_doc.xpath("//li").each do |li|
    a = li.at_xpath("a")
    next unless a
    li.prepend_child a.text
    a.remove
  end

  toc_doc.xpath("//ol").each do |ol|
    ul = toc_doc.create_element "ul"
    ul.children = ol.children
    ol.replace ul
  end

  toc = toc_doc.at_xpath("//ul")
  if toc
    puts "SUCCESS"
    puts toc.to_html
  else
    puts "FAIL"
    puts toc_html.to_html
  end
end
