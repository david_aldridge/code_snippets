/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_restore --format=custom --list ~/Downloads/bibliocloud-app.dump | grep -v 'TABLE DATA public versions' > bibliocloud.list


/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_dump $(heroku config:get DATABASE_URL -a bibliocloud-app) \
 --data-only \
 --format=plain \
 --no-owner \
 --verbose \
 -t="public.batch_message_levels" \
 -t=batch_message_templates \
 -t=check_classes \
 -t=client_applications \
 -t=digital_asset_subtypes \
 -t=digital_asset_types \
 -t=flipper_features \
 -t=flipper_gates \
 -t=levels \
 -t=onix_code_lists \
 -t=onix_codes \
 -t=schema_migrations \
 > seed.dump
