class Thema
  def self.new(code)
    return NilObject.instance if code.nil?
    if valid? code
      super code
    else
      Invalid.instance
    end
  end

  def self.valid?(code)
    CONFIG.include? code
  end

  def initialize(code)
    @code = code
  end

  def parent
    Thema.new(parent_code)
  end

  delegate  :eql?,
            to: :code

  delegate  :description,
            :notes,
            :issue_number,
            :parent_code,
            :type,
            to: :lookup

  private

  attr_reader :code

  def lookup
    CONFIG[code]
  end

  Lookup = Struct.new(:code, :description, :notes, :parent_code, :issue_number) do
    DISCRIMINATOR = {
      "1" => :geography,
      "2" => :language,
      "3" => :time,
      "4" => :education,
      "5" => :special_interest,
      "6" => :style
    }
    def type
      return :age if code.starts_with? "5A"
      DISCRIMINATOR[code.first] || :subject
    end

    delegate :size, to: :code

    def selectable?
      size > 1
    end
  end

  CONFIG = Hash[*YAML.load_file(Rails.root.join("config", "thema.yml")).map do |code, config|
    [
      code,
      Lookup.new(
        code,
        config["CodeDescription"],
        config["CodeNotes"],
        config["CodeParent"],
        config["IssueNumber"]
        )
    ]
  end.flatten]

  class Invalid
    include Singleton
    include HasBulkReplies

    reply_with false, to: :valid?

    reply_with nil, to: %i(code description notes issue_number parent type)
  end

  class NilObject
    include Singleton
    include HasBulkReplies

    reply_with false, to: :valid?

    reply_with nil, to: %i(code description notes issue_number parent type)

    reply_with true, to: %i(nil?, blank?)
  end
end
