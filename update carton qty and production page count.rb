hash = {
  "9781786760845" => OpenStruct.new(pages: 476, carton_qty: 16),
  "9781786760968" => OpenStruct.new(pages: 328, carton_qty: 22),
  "9781786760920" => OpenStruct.new(pages: 332, carton_qty: 22),
  "9781786760609" => OpenStruct.new(pages: 444, carton_qty: 16),
  "9781786760401" => OpenStruct.new(pages: 566, carton_qty: 12),
  "9781786761040" => OpenStruct.new(pages: 308, carton_qty: 22),
  "9781786760524" => OpenStruct.new(pages: 606, carton_qty: 12),
  "9781786761767" => OpenStruct.new(pages: 614, carton_qty: 12),
  "9781786762764" => OpenStruct.new(pages: 420, carton_qty: 18),
  "9781786760807" => OpenStruct.new(pages: 236, carton_qty: 30),
  "9781786761880" => OpenStruct.new(pages: 354, carton_qty: 20),
  "9781786760647" => OpenStruct.new(pages: 504, carton_qty: 16),
  "9781786760005" => OpenStruct.new(pages: 426, carton_qty: 18),
  "9781786761248" => OpenStruct.new(pages: 262, carton_qty: 26),
  "9781786760289" => OpenStruct.new(pages: 440, carton_qty: 16),
  "9781786760241" => OpenStruct.new(pages: 300, carton_qty: 24),
  "9781786760449" => OpenStruct.new(pages: 362, carton_qty: 20),
  "9781786760760" => OpenStruct.new(pages: 432, carton_qty: 18),
  "9781786760487" => OpenStruct.new(pages: 434, carton_qty: 18),
  "9781786760081" => OpenStruct.new(pages: 350, carton_qty: 20),
  "9781786760562" => OpenStruct.new(pages: 254, carton_qty: 26),
  "9781786761965" => OpenStruct.new(pages: 294, carton_qty: 24),
  "9781786762009" => OpenStruct.new(pages: 488, carton_qty: 16),
  "9781786760166" => OpenStruct.new(pages: 688, carton_qty: 10),
  "9781786762160" => OpenStruct.new(pages: 368, carton_qty: 20),
  "9781786761644" => OpenStruct.new(pages: 478, carton_qty: 16),
  "9781786761521" => OpenStruct.new(pages: 412, carton_qty: 18),
  "9781786760883" => OpenStruct.new(pages: 292, carton_qty: 24),
  "9781786761408" => OpenStruct.new(pages: 400, carton_qty: 18),
  "9781786761323" => OpenStruct.new(pages: 572, carton_qty: 12),
  "9781786760128" => OpenStruct.new(pages: 472, carton_qty: 16),
  "9781786760203" => OpenStruct.new(pages: 352, carton_qty: 20),
  "9781786760685" => OpenStruct.new(pages: 344, carton_qty: 20),
  "9781786760043" => OpenStruct.new(pages: 324, carton_qty: 22),
  "9781786760722" => OpenStruct.new(pages: 352, carton_qty: 20),
  "9781786761446" => OpenStruct.new(pages: 362, carton_qty: 20),
  "9781786761484" => OpenStruct.new(pages: 470, carton_qty: 16),
  "9781786762160" => OpenStruct.new(pages: 368, carton_qty: 20),
  "9781786761200" => OpenStruct.new(pages: 548, carton_qty: 14),
  "9781786761842" => OpenStruct.new(pages: 570, carton_qty: 12),
  "9781786762047" => OpenStruct.new(pages: 516, carton_qty: 14),
  "9781786761125" => OpenStruct.new(pages: 342, carton_qty: 20),
  "9781786761002" => OpenStruct.new(pages: 322, carton_qty: 22),
  "9781786761286" => OpenStruct.new(pages: 330, carton_qty: 22),
  "9781786761163" => OpenStruct.new(pages: 288, carton_qty: 24),
  "9781786761927" => OpenStruct.new(pages: 464, carton_qty: 16),
  "9781786761088" => OpenStruct.new(pages: 464, carton_qty: 16),
  "9781786761361" => OpenStruct.new(pages: 434, carton_qty: 18),
  "9781786760326" => OpenStruct.new(pages: 618, carton_qty: 12),
  "9781786761606" => OpenStruct.new(pages: 560, carton_qty: 14),
  "9781786761569" => OpenStruct.new(pages: 380, carton_qty: 18),
  "9781786761682" => OpenStruct.new(pages: 590, carton_qty: 12)
}

client = Client.is_bds

ActiveRecord::Base.transaction do
  Work.no_touching do
    hash.each do |isbn, attribs|
      book = client.books.find_by(isbn: isbn)
      if book
        book.update(
          production_page_count: attribs.pages,
          carton_qty:            attribs.carton_qty
        )
      else
        puts "Cannot find #{isbn}"
      end
    end
  end
  # raise ActiveRecord::Rollback
end
