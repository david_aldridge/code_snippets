require 'nokogiri'

class ONIX
  def initialize(file)
    @file = file
  end

  def products
    doc.xpath("/ONIXMessage/Product")
  end

  private

  attr_reader :file

  def doc
    @_doc ||= Nokogiri::XML(open(file))
  end

  VERSIONS = {
    "http://www.editeur.org/onix/2.1/reference/onix-international.dtd" => {
      name: "2.1",
      klass: ONIX::V2
    }
  }
  def version
    VERSION.fetch(doc.internal_subset.system_id)
  end
end

file = "/Users/david/Documents/Bibliocloud/Clients/CPG/CPI_ONIX_books_052019_new2.xml"

onix = ONIX.new(file)



doc = Nokogiri::XML(open(file))
doc.xpath("/Product").count

doc.xpath("//Product/MediaFile[MediaFileTypeCode='04']").each do |media_file|
  puts "curl #{media_file.at_xpath("MediaFileLink").content} --output #{media_file.at_xpath("../ProductIdentifier[ProductIDType='15']/IDValue").content}.jpg"
end
