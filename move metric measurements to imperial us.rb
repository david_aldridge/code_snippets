client = Client.is_rethink

ActiveRecord::Base.transaction do
  client.books.each do |book|
    book.product_height_in    = book.product_height_in_value unless book.product_height_in
    book.product_width_in     = book.product_width_in_value unless book.product_width_in
    book.product_thickness_in = book.product_thickness_in_value unless book.product_thickness_in
    book.unit_weight_oz       = book.unit_weight_oz_value unless book.unit_weight_oz
    book.page_trim_height_in  = book.product_height_in
    book.page_trim_width_in   = book.product_width_in
    if book.hardback?
      book.product_height_in += 3/8.0 if book.product_height_in
      book.product_width_in  += 1/4.0 if book.product_width_in
    end
    book.save
  end
end

ActiveRecord::Base.transaction do
  client.proprietary_format_descriptions.each do |book|
    book.product_height_in    = book.product_height_in_value unless book.product_height_in
    book.product_width_in     = book.product_width_in_value unless book.product_width_in
    book.product_thickness_in = book.product_thickness_in_value unless book.product_thickness_in
    book.unit_weight_oz       = book.unit_weight_oz_value unless book.unit_weight_oz
    book.page_trim_height_in  = book.product_height_in
    book.page_trim_width_in   = book.product_width_in
    puts book.errors.full_messages unless book.valid?
    book.save
  end
end

client.books.order(:page_trim_height_in, :page_trim_width_in).
  where.not(page_trim_height_in: nil, page_trim_width_in: nil).
  group(:page_trim_height_in, :page_trim_width_in).count.transform_keys{|k| k.map(&:to_s)}

{["0.31", "5.0"]=>1,
 ["3.35", "5.5"]=>1,
 ["5.0", "8.0"]=>1,
 ["7.0625", "5.0"]=>3,
 ["8.0", "5.0"]=>67,
 ["8.0", "8.0"]=>8,
 ["8.0625", "5.0"]=>1,
 ["8.0625", "8.0625"]=>1,
 ["8.3125", "5.875"]=>2,
 ["8.4375", "5.5625"]=>1,
 ["8.5", "5.5"]=>22,
 ["8.5625", "5.5625"]=>152,
 ["8.5625", "8.5625"]=>2,
 ["9.0", "6.0"]=>7,
 ["9.0625", "6.0"]=>22,
 ["11.75", "8.3125"]=>3}

client.books.where(page_trim_height_in: [8.3125, 7.0625, 8.0625, 8.5625, 9.0625] ).each do |book|
  book.page_trim_height_in = book.page_trim_height_in - 0.0625
  book.product_height_in   = book.product_height_in - 0.0625
  book.save
end

client.books.where(page_trim_width_in: [8.0625, 5.5625, 8.3125, 8.5625] ).each do |book|
  book.page_trim_width_in = book.page_trim_width_in - 0.0625
  book.product_width_in   = book.product_width_in - 0.0625
  book.save
end

client.proprietary_format_descriptions.where(page_trim_height_in: [8.4375,    7.1875, 7.8125,   9.6875, 8.6875, 8.3125, 7.0625, 8.0625, 8.5625, 9.0625] ).each do |book|
  book.page_trim_height_in = book.page_trim_height_in - 0.0625
  book.product_height_in   = book.product_height_in - 0.0625
  book.save
end

client.proprietary_format_descriptions.where(page_trim_width_in: [6.3125, 6.1875, 5.4375, 7.5625,   4.1875,4.3125,8.0625, 5.5625, 8.3125, 8.5625] ).each do |book|
  book.page_trim_width_in = book.page_trim_width_in - 0.0625
  book.product_width_in   = book.product_width_in - 0.0625
  book.save
end

client.books.update_all(
page_trim_height_mm: nil,
page_trim_width_mm: nil,
product_height_mm: nil,
product_width_mm: nil,
product_thickness_mm: nil,
spherical_diameter_mm: nil,
cylindrical_diameter_mm: nil,
sheet_height_mm: nil,
sheet_width_mm: nil,
rolled_sheet_package_side_mm: nil,
unit_weight_gr: nil
)





