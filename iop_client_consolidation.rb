# Step 1: Ruby

[
  [34,31],
  [35,32],
  [30,13],
  [28,11],
  [27,14]
].each do |from_id, to_id|
  from_pfd     = ProprietaryFormatDescription.find(from_id)
  to_pfd       = ProprietaryFormatDescription.find(to_id)
  raise "Cannot find pfd" unless from_pfd && to_pfd
  to_pfd.books += from_pfd.books
  from_pfd.update_attributes(name: from_pfd.name + " (obsolete)")
end

ProprietaryFormatDescription.find(29).update_attributes(name: "Print PDF (PB)")
ProprietaryFormatDescription.find(12).update_attributes(name: "Print PDF (HB)")

ProprietaryEditionDescription.where(client_id: 4).each do |from|
  to = ProprietaryEditionDescription.find_by(client_id: 3, name: from.name)
  raise "Cannot find target" unless to
  from.books.update_all(proprietary_edition_description_id: to.id)
  from.update_attributes(name: from.name + " (obsolete)")
end

Publishername.find(4).update_columns(identifier: Client::MORGAN_CLAYPOOL)
Publishername.find(3).update_columns(identifier: Client::IOP)

ProprietaryEditionDescription.all.each do |ped|
  next unless ped.name.ends_with?(" (obsolete)")
  new_ped = ProprietaryEditionDescription.find_by(name: ped.name.sub(" (obsolete)", ""))
  raise "Cannot find non-obsolete form" unless new_ped
  new_ped.books += ped.books
  new_ped.save
end

ProprietaryFormatDescription.pluck(:id).each do |id|
 ProprietaryFormatDescription.reset_counters(id, :books)
end

ProprietaryEditionDescription.pluck(:id).each do |id|
 ProprietaryEditionDescription.reset_counters(id, :books)
end

ProprietaryFormatDescription.all.each{|x| x.destroy if x.name.ends_with?(" (obsolete)")}
ProprietaryEditionDescription.all.each{|x| x.destroy if x.name.ends_with?(" (obsolete)")}

# Step 2: PostgreSQL

delete from companies                                where client_id = 4;
delete from contacts                                 where client_id = 4 and identifier is not null;
delete from contact_digital_asset_transfer_includes  where client_id = 4;
delete from contact_digital_asset_transfer_templates where client_id = 4;
delete from contact_digital_asset_transfers          where client_id = 4;
delete from digital_asset_content_items              where client_id = 4;
delete from digital_asset_contents                   where client_id = 4;
delete from digital_assets                           where client_id = 4;
delete from export_batch_items                       where client_id = 4;
delete from export_batches                           where client_id = 4;
delete from ftp_templates                            where client_id = 4;
delete from supplier_configs                         where client_id = 4;
delete from groups;
delete from client_groups;
update users set group_id = null;

update addresses                                set client_id = 3 where client_id = 4;
update advisories                               set client_id = 3 where client_id = 4;
update age_range_codes                          set client_id = 3 where client_id = 4;
update ai_archives                              set client_id = 3 where client_id = 4;
update ai_templates                             set client_id = 3 where client_id = 4;
update amazon_ecs_requests                      set client_id = 3 where client_id = 4;
update ancillary_contents_bkp                   set client_id = 3 where client_id = 4;
update any_files                                set client_id = 3 where client_id = 4;
update api_keys                                 set client_id = 3 where client_id = 4;
update audiences                                set client_id = 3 where client_id = 4;
update book_briefs                              set client_id = 3 where client_id = 4;
update book_channels                            set client_id = 3 where client_id = 4;
update book_masterchannels                      set client_id = 3 where client_id = 4;
update book_validation_templates                set client_id = 3 where client_id = 4;
update book_validation_tests                    set client_id = 3 where client_id = 4;
update bookaudiences                            set client_id = 3 where client_id = 4;
update bookcontacts                             set client_id = 3 where client_id = 4;
update bookmarks                                set client_id = 3 where client_id = 4;
update books                                    set client_id = 3 where client_id = 4;
update brief_setups                             set client_id = 3 where client_id = 4;
update briefing_attachments                     set client_id = 3 where client_id = 4;
update briefing_text_setups                     set client_id = 3 where client_id = 4;
update briefing_texts                           set client_id = 3 where client_id = 4;
update briefs                                   set client_id = 3 where client_id = 4;
update bulk_uploaders                           set client_id = 3 where client_id = 4;
update calendars                                set client_id = 3 where client_id = 4;
update catalogue_archives                       set client_id = 3 where client_id = 4;
update catalogue_templates                      set client_id = 3 where client_id = 4;
update channels                                 set client_id = 3 where client_id = 4;
update charge_finance_codes                     set client_id = 3 where client_id = 4;
update clauses                                  set client_id = 3 where client_id = 4;
update clausetypes                              set client_id = 3 where client_id = 4;
update client_currencies                        set client_id = 3 where client_id = 4;
update client_groups                            set client_id = 3 where client_id = 4;
update collection_items                         set client_id = 3 where client_id = 4;
update collections                              set client_id = 3 where client_id = 4;
update contact_seriesnames                      set client_id = 3 where client_id = 4;
update contact_types                            set client_id = 3 where client_id = 4;
update contactfeatures                          set client_id = 3 where client_id = 4;
update contacts                                 set client_id = 3 where client_id = 4;
update contract_attachments                     set client_id = 3 where client_id = 4;
update contracts                                set client_id = 3 where client_id = 4;
update contracttemplates                        set client_id = 3 where client_id = 4;
update copy_trackers                            set client_id = 3 where client_id = 4;
update copyright_ownerships                     set client_id = 3 where client_id = 4;
update cost_groups                              set client_id = 3 where client_id = 4;
update currencies                               set client_id = 3 where client_id = 4;
update custom_reports                           set client_id = 3 where client_id = 4;
update deals                                    set client_id = 3 where client_id = 4;
update defaults                                 set client_id = 3 where client_id = 4;
update draft_contacts                           set client_id = 3 where client_id = 4;
update editorial_originations                   set client_id = 3 where client_id = 4;
update emails                                   set client_id = 3 where client_id = 4;
update estimates                                set client_id = 3 where client_id = 4;
update excels                                   set client_id = 3 where client_id = 4;
update finance_codes                            set client_id = 3 where client_id = 4;
update foreignrights                            set client_id = 3 where client_id = 4;
update gratis_copies                            set client_id = 3 where client_id = 4;
update imports                                  set client_id = 3 where client_id = 4;
update imprints                                 set client_id = 3 where client_id = 4;
update isbnlists                                set client_id = 3 where client_id = 4;
update issues                                   set client_id = 3 where client_id = 4;
update languages                                set client_id = 3 where client_id = 4;
update licensed_rights                          set client_id = 3 where client_id = 4;
update marketingtexts                           set client_id = 3 where client_id = 4;
update masterchannels                           set client_id = 3 where client_id = 4;
update messages                                 set client_id = 3 where client_id = 4;
update money                                    set client_id = 3 where client_id = 4;
update ms_delivery_dates                        set client_id = 3 where client_id = 4;
update notes                                    set client_id = 3 where client_id = 4;
update notifications                            set client_id = 3 where client_id = 4;
update payments                                 set client_id = 3 where client_id = 4;
update permissions                              set client_id = 3 where client_id = 4;
update phones                                   set client_id = 3 where client_id = 4;
update pipelines                                set client_id = 3 where client_id = 4;
update prices                                   set client_id = 3 where client_id = 4;
update print_bundle_components                  set client_id = 3 where client_id = 4;
update print_on_demand_specs                    set client_id = 3 where client_id = 4;
update print_originations                       set client_id = 3 where client_id = 4;
update print_paper_details                      set client_id = 3 where client_id = 4;
update printer_estimates                        set client_id = 3 where client_id = 4;
update prints                                   set client_id = 3 where client_id = 4;
update product_parts                            set client_id = 3 where client_id = 4;
update productcodes                             set client_id = 3 where client_id = 4;
update production_files                         set client_id = 3 where client_id = 4;
update professional_affiliations                set client_id = 3 where client_id = 4;
update proposal_texts                           set client_id = 3 where client_id = 4;
update proprietary_edition_descriptions         set client_id = 3 where client_id = 4;
update proprietary_format_descriptions          set client_id = 3 where client_id = 4;
update publishernames                           set client_id = 3 where client_id = 4;
update publishing_roles                         set client_id = 3 where client_id = 4;
update relatedproducts                          set client_id = 3 where client_id = 4;
update request_logs                             set client_id = 3 where client_id = 4;
update rightrules                               set client_id = 3 where client_id = 4;
update rightsadvances                           set client_id = 3 where client_id = 4;
update role_adopters                            set client_id = 3 where client_id = 4;
update role_involvements                        set client_id = 3 where client_id = 4;
update role_sets                                set client_id = 3 where client_id = 4;
update royalty_specifiers                       set client_id = 3 where client_id = 4;
update sales_rights                             set client_id = 3 where client_id = 4;
update schedules                                set client_id = 3 where client_id = 4;
update seriesnames                              set client_id = 3 where client_id = 4;
update sign_offs                                set client_id = 3 where client_id = 4;
update stages                                   set client_id = 3 where client_id = 4;
update stock_availabilities                     set client_id = 3 where client_id = 4;
update subjectcodes                             set client_id = 3 where client_id = 4;
update subjects                                 set client_id = 3 where client_id = 4;
update supplier_configs                         set client_id = 3 where client_id = 4;
update supplier_products                        set client_id = 3 where client_id = 4;
update support_tickets                          set client_id = 3 where client_id = 4;
update supportingresources                      set client_id = 3 where client_id = 4;
update task_roles                               set client_id = 3 where client_id = 4;
update tasks                                    set client_id = 3 where client_id = 4;
update tweets                                   set client_id = 3 where client_id = 4;
update users                                    set client_id = 3 where client_id = 4;
update validation_templates                     set client_id = 3 where client_id = 4;
update versions                                 set client_id = 3 where client_id = 4;
update web_links                                set client_id = 3 where client_id = 4;
update work_derivations                         set client_id = 3 where client_id = 4;
update work_similars                            set client_id = 3 where client_id = 4;
update workcontacts                             set client_id = 3 where client_id = 4;
update works                                    set client_id = 3 where client_id = 4;
update zip_archives                             set client_id = 3 where client_id = 4;
