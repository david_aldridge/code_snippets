# collection @works, :root => "works"
# http://localhost:3000/products.json?api_version=5&q%5Bshops_id_in%5D=7
#object false


collection @books
@books.each do |book|
#child @books => :products do |book|
  #@product_md5 = Digest::MD5.new
  #partial "product", :view_path => 'app/views/apis/products',:object => book
attributes :id                            ,
           :work_id                       ,
           :full_title                    ,
           :subtitle                      ,
           :pub_date                      ,
           :product_form                  ,
           :edition_type_codes            ,
           :proprietary_format_description
attribute  :epub_type_code => :epub_type, :if => lambda { |b| b.is_ebook? }
attributes :format_isbn_with_dashes  => :isbn
attributes :edition                  => :edition_number
attributes :product_dimensions_all   => :size
attributes :page_trim_dimensions_all => :page_trim

node(:online_authorship_description) {|book| book.online_authorship_description || book.work.online_authorship_description }

child :book_seriesnames => :series, :object_root => false do
  glue(:seriesname) do
    attributes :title_without_prefix => :title
    attributes :id, :subtitle
  end
  attributes :number_within_series, :year_of_annual
end
#node(:product_dimensions_mm) { |book| book.product_dimensions('mm') }
#node(:product_dimensions_in) { |book| book.product_dimensions('in') }
#node(:page_trim_mm         ) { |book| book.page_trim_dimensions('mm') }
#node(:page_trim_in         ) { |book| book.page_trim_dimensions('in') }

child :prices do
  attributes :id,
             :currency_code,
             :price_amount,
             :includes_tax
  attributes :onix_price_type_qualifier_id => :price_type_qualifier
end

child :workcontacts => :contributors do
  attributes :id,
             :sequence_number
  attribute  :work_contact_role => :contributor_role

  #node(:role) {|work_contributor| work_contributor.translated_work_contact_role }
  glue(:contact) do
    attributes :id, :biographical_note
    node(:name) {|contact| contact.to_s }
  end
end

child :supportingresources, :object_root => false do
  attribute :id
  attribute :resource_content_type
  #node(:type) { |supportingresource| supportingresource.translated_resource_content_type }
  child :style_urls, :root => :style_urls, :object_root => false  do
    node (:style)  {|style_url| style_url[:style]}
    node (:url  )  {|style_url| style_url[:url]}
  end
end

child :marketingtexts do
  attribute :id
  attribute :legacy_code => "code"
  node(:external_text          ) { |marketingtext| marketingtext.pull_quote               }
  node(:internal_text          ) { |marketingtext| marketingtext.marketing_text           }
  node(:alternate_external_text) { |marketingtext| marketingtext.alternate_pull_quote     }
  node(:alternate_internal_text) { |marketingtext| marketingtext.alternate_marketing_text }
end


child :all_related_products do
  attributes :relation_code
  node(:product_id) { |all_related_product| all_related_product.product.id             }
  node(:isbn) { |all_related_product| all_related_product.product.isbn             }

end

  #@product_md5
end

code(:current_page) { |m| @books.current_page }
code(:per_page)     { |m| @books.length }
code(:total_pages)  { |m| @books.total_pages }
node(:url)          { |m| request.url }
