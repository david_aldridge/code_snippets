x = ScaleElementXPrice.new("name"=>"List",
  "x_units"=>"0",
  "x_interpolation"=>"0",
  "x_extrapolation"=>"1",
  "x_values_str"=>"1 2 3",
  "price_units"=>"0",
  "price_values_str"=>"2 3 4")

reload!
s = ScaleElementXPrice.first
s.price(:units => 50)
s.price(1, 1, 50, 256, 60000)[:value].to_f
