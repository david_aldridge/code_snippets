require "terminal-table"

puts Terminal::Table.new(
headings: ["Product form", "Detail", "Title"],
rows: Client.is_what_on_earth.books.pluck(:product_form, :product_form_detail, :title).sort_by(&:first)
)
