
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page_size=150&page=1"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e"\
 "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page_size=150&page=1&q[id_in][]=68863"

curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e"\
 "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&version=1&page_size=500&page=1&digest_only=1"

68863


curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "http://localhost:3000/api/products.json?q[shops_id_eq]=7&version=1&digest_only=1&page_size=150&page=4"

IOP version 2
===============

curl -g -H "Authorization: Token token=d89f56e2359a470e9cea26fbe8a608a3" "https://iop.consonance.app/api/v2/products.json?page_size=10"




For a particular subject, if you know the subject ID ...

curl -g -H "Authorization: Token token=d89f56e2359a470e9cea26fbe8a608a3" "https://iop.consonance.app/api/v2/products.json?q[subjectcodes_id_eq]=70&page_size=10"

It's broadly the same syntax that you see when you click on "products" from the subject categories page:

https://iop.consonance.app/books/index?q%5Bsubjectcodes_id_eq%5D=70


For a particular series, if you know the series ID ...

curl -g -H "Authorization: Token token=d89f56e2359a470e9cea26fbe8a608a3" "https://iop.consonance.app/api/v2/products.json?q[seriesnames_id_eq]=3&page_size=10"

https://iop.consonance.app/books/index?q%5Bseriesnames_id_eq%5D=3

For only active products ...

curl -g -H "Authorization: Token token=d89f56e2359a470e9cea26fbe8a608a3" "https://iop.consonance.app/api/v2/products.json?q[publishing_status_eq]=04&page_size=10"

For only active products in series

curl -g -H "Authorization: Token token=d89f56e2359a470e9cea26fbe8a608a3" "https://iop.consonance.app/api/v2/products.json?q[publishing_status_eq]=04&q[seriesnames_id_eq]=3&page_size=10"




Local version 2
===============

curl -g -H "Authorization: Token token=371bc2bbe85749c3abf8643f1c5904b8" "http://localhost:3000/api/v2/products.json?version=1&page_size=150&page=4"



curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "http://localhost:3000/api/v2/products.json?q[shops_id_eq]=7&version=1&page_size=150&page=4"


curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "http://test.bibliocloud.com/api/v2/products.json?q[shops_id_eq]=7&version=1&page_size=150&page=4"



curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "http://test.bibliocloud.com/api/v2/products.json?page_size=150&page=0"






curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[work_id_eq]=47418"


curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[id_eq]=68978"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[id_eq]=68978&digest_only=1"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[shops_id_eq]=7&q[id_eq]=68978&digest_only=1"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[id_in][]=68900&q[id_in][]=68978&q[id_in][]=69158&q[id_in][]=69446&q[id_in][]=69538&q[id_in][]=69553&q[id_in][]=69610&q[id_in][]=69647&digest_only=1"

Assata
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://v4.bibliocloud.com/api/products.json?q[id_eq]=[69732]"



curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?q[id_eq]=68978&q[id_eq]=68978"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?q[id_eq]=68978&digest_only=1"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?q[shops_id_eq]=7&q[id_eq]=68978&digest_only=1"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?q[id_in][]=68900&q[id_in][]=68978&q[id_in][]=69158&q[id_in][]=69446&q[id_in][]=69538&q[id_in][]=69553&q[id_in][]=69610&q[id_in][]=69647&digest_only=1"


d7f5ef6f9c404422ba29db08288f4662




curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?q[shops_id_eq]=7&q[id_eq]=68978"
curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "127.0.0.1:3000/api/products.json?&q[id_eq]=68978"



431254f6ce817d8ba3d143622f9ec9a1


https://v4.bibliocloud.com/api/products.json?q[shops_id_eq]=7&version=1&page=3

time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "localhost:3000/api/products.json?q[shops_id_eq]=7&page=10"


time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=10"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=11"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=12"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=13"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=14"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=15"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=16"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=17"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=18"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=19"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=20"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=21"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=22"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=23"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=24"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=25"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=26"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=27"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=28"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=29"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=30"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=31"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=32"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=33"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=34"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=35"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=36"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=37"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=38"
time curl -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&page=39"








time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=1"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=2"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=3"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=4"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=5"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=6"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=7"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=8"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=9"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=10"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=11"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=12"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=13"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=14"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=15"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=16"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=17"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=18"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=19"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=20"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=21"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=22"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=23"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=24"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=25"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=26"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=27"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=28"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=29"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=30"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=31"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=32"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=33"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=34"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=35"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=36"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=37"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=38"
time curl -v -g -H "Authorization: Token token=ddace0bc495546be8f467e488ef7672e" "https://web.consonance.app/api/products.json?q[shops_id_eq]=7&digest_only=1&page=39"
