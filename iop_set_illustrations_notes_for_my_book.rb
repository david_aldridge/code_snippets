Book.all.map(&:illustrations_note_to_use).compact.each_with_object(Hash.new(0)){|x,h| h[x] += 1}

Work.update_all(illustrations_note: "With figures in colour and in black and white")

Book.update_all(use_work_ancillary_content_spec: true, illustrations_note: nil)

Book.joins(:proprietary_format_description).
  where(proprietary_format_descriptions: {identifier: "my-book"}).
  update_all(use_work_ancillary_content_spec: false, illustrations_note:  "With figures in black and white")
