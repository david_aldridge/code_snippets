s3 = Aws::S3::Client.new(region: ENV['AWS_REGION'])

keys = s3.list_objects( bucket: ENV['AWS_BUCKET'], prefix: "JSON/9").contents.map(&:key)

keys.map do |key|
  JSON.parse(
    s3.get_object(
      bucket: ENV['AWS_BUCKET'],
      key:    key
    ).body.read
  )
end


keys.flat_map do |key|
  JSON.parse(
    s3.get_object(
      bucket: ENV['AWS_BUCKET'],
      key:    key
    ).body.read
  )["contributor"].map{|c| c.values_at("givenName", "familyName")}
end.uniq


keys.flat_map do |key|
  text = s3.get_object(
    bucket: ENV['AWS_BUCKET'],
    key:    key
  ).body.read
  JSON.parse("{#{text.match(/(?<capture_name>"contributor"\:.*?\n    \])/m)[:capture_name]}}").values.slice("givenName","familyName")
end



JSON.parse("{#{x.match(/(?<capture_name>"contributor"\:.*?\n    \])/m)[:capture_name]}}")

x.match(/(?<capture_name>"contributor"\:.*\n)/)


















bucket = s3.buckets[ENV['AWS_BUCKET']]

bucket.objects.with_prefix('4')

bucket.objects.with_prefix('4').select{|x| x.key.match? /^4\/\d/ }.each do |obj|
  new_key = obj.key.sub("4", "3")
  obj.copy_to(new_key, :bucket => bucket, :acl => :public_read)
end


bucket.objects.with_prefix('3/129')


bucket.objects.select{|x| x.key == "3/129/252/252_jpg_cmyk_original.jpg"}

bucket.objects.with_prefix('4').each do |obj|
  obj.copy_to(obj.key, :bucket => to_bucket, :acl => :public_read)
end

bucket.objects.with_prefix('4').first.key











s3 = Aws::S3::Resource.new
bucket.objects.each do |object|
  object.acl=:public_read
end

obj = bucket.objects.first




bucket.objects.with_prefix('978').delete_all

bucket.versions(:prefix => '978').count


collect(&:key)

client_ids = [268,
289,
290,
291,
292,
293,
293,
293,
295,
295,
296,
297,
297,
298,
299,
301,
302,
302,
303,
303,
303,
303,
303,
304,
305,
306,
307,
308,
308,
309,
310,
311,
312,
313,
313,
314,
315,
317,
318,
319,
320,
321,
321,
321,
322,
323,
324,
325,
325,
325,
325,
325,
326,
327,
328,
330,
337,
337,
337].uniq




client_ids.each do |c|
 puts bucket.objects.with_prefix("#{c}/").delete_all
end

bucket = s3.buckets["bibliocloudimagesdev"]

bucket.objects["quarto.xml"].write(open("/Users/davidaldridge/Documents/Bibliocloud/Clients/quarto/onix-AURUM-20141212.xml"))

bucket.objects["pg.dump"].write(open("/Users/davidaldridge/Downloads/bibliocloud.dmp"))

==========

@files = Dir.glob("/Users/davidaldridge/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/*")

=====================================================

s3 = AWS::S3.new(
  :access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
  :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'])

from_bucket = s3.buckets["bibliocloudimagesdev"]
to_bucket   = s3.buckets["bibliocloud-demo"]


to_bucket.objects.delete_all


bucket.objects.with_prefix('353/')


from_bucket.objects.with_prefix('353/').each do |obj|
  obj.copy_to(obj.key, :bucket => to_bucket, :acl => :public_read)
end


from_bucket = s3.buckets["publihubsecure"]
from_bucket.objects.each do |obj|
  obj.copy_to(obj.key, :bucket => to_bucket, :acl => :public_read)
end

bucket.objects.with_prefix('4').first.key

====================
s3 = AWS::S3.new(
  :access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
  :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'])

bucket = s3.buckets[ENV['AWS_BUCKET']]

s3_keys = bucket.objects.with_prefix('barcode').collect(&:key)

paperclip_paths = Barcodeimg.all.map{|b| b.image.path}

paperclip_paths = Barcodeimg.where(:client_id).map{|b| b.image.path}

paperclip_paths.count
s3_keys.count



paperclip_paths - s3_keys


=======================
s3 = AWS::S3.new(
  :access_key_id     =>"AKIAIGCG4JH5G22RHHXA",
:secret_access_key => "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")

bucket = s3.buckets["bibliocloudimages"]
obj = bucket.objects['Stock_Rec_corrected.csv'].write(open("/Users/davidaldridge/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/Stock_Rec_corrected.csv"), :acl => :public_read)
obj = bucket.objects['zed_books_dealers.csv'].write(open("/Users/davidaldridge/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/Dealers.csv"), :acl => :public_read)




obj.acl= :public_read
