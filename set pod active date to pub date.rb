Client.is_ucl.
  print_on_demand_specs.
  joins(:book).
  where(print_on_demand_specs: {active: true}).
  where(active_from_date: nil).
  where("books.pub_date > ?",Date.today).each do |pod|
  pod.update_attributes(active_from_date: pod.book.pub_date - 3.days)
end
