client = CreateClient.new(
  "Wiley",
  identifier: "wiley"
).call

client = Client.is_wiley

client.users.create(
  email: "david+wiley@consonance.app",
  password: "iiaamahs1o3",
  password_confirmation: "iiaamahs1o3"
)




class WileyProduct < ActiveRecord::Base

  CLIENT           = Client.is_wiley.freeze
  BOOK_CONTRIBUTOR = ContactType.where(name: "Book contributor")

  def isbn_value
    Lisbn.new(isbn)
  end

  def isbn_valid?
    isbn_value.valid?
  end

  def self.imprints
    publisher = CLIENT.publishernames.first
    all.pluck(:imprint).uniq.map do |imprint|
      publisher.imprints.new(value: imprint, client_id: publisher.client_id)
    end
  end

  def self.contacts
    all.pluck(:author).uniq.map do |author|
      CLIENT.contacts.new(
          keynames: author,
          contact_types: BOOK_CONTRIBUTOR
        )
    end
  end

  def self.publisher
    client.publishernames.new(publisher_name: "Wiley")
  end

  def self.create_publisher
    publisher
  end

  def self.create_imprints
    ActiveRecord::Base.transaction do
      imprints.each(&:save)
    end
  end

  def self.create_contacts
    ActiveRecord::Base.transaction do
      contacts.each(&:save)
    end
  end


end


PaperTrail.enabled = false

WileyProduct.publisher
WileyProduct.imprints
WileyProduct.contacts
WileyProduct.create_imprints
WileyProduct.create_contacts


client.publishernames.create(publisher_name: "Wiley")


WileyProduct.imprints
