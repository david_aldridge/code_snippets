client = Client.is_what_on_earth

require "terminal-table"

puts Terminal::Table.new(
headings: %w(Title Subtitle),
rows: Client.is_what_on_earth.works.order(:title, :subtitle).pluck(:title, :subtitle).map{|x,y| "#{x} (#{y})"}
)

client.contracts.first.destroy
client.works.first.destroy
client.books.where(product_form: "WW").update_all(product_form: "BB")
client.prices.update_all(tax_rate_code: nil, tax_rate_percent: nil, taxable_amount: nil, tax_value: nil, tax_rate_code_2: nil, tax_rate_percent_2: nil, taxable_amount_2: nil, tax_value_2: nil)
client.books.where("title like '%Sticker%'").update_all(product_form: "BC")

client.prices.group(:currency_code, :price_type).count

client.client_currencies.create(currency_code: "GBP", territory_codes: ["ROW"])
client.client_currencies.create(currency_code: "USD", territory_codes: ["US & TERRITORIES"], country_codes: ["CA", "MX"])

client.books.group(:product_form, :epub_type_code).count


ips     = client.contacts.where(corporate_name: "Ingram", identifier: Contact::INGRAM).first_or_create
nielsen = client.contacts.where(corporate_name: "Nielsen Bookdata", identifier: Contact::NIELSEN).first_or_create
bds     = client.contacts.where(corporate_name: "Bibliographic Data Services", identifier: Contact::BIBLIOGRAPHIC_DATA_SERVICES).first_or_create
publisher = client.contacts.where(corporate_name: "What On Earth Books", identifier: Client::WHAT_ON_EARTH).first_or_create

ips.contact_types     = ContactType.distributors + ContactType.data_aggregators
nielsen.contact_types = ContactType.data_aggregators
bds.contact_types     = ContactType.data_aggregators
publisher.contact_types = ContactType.publishers


client.supplier_configs.create(
  :supply_to_territories=>["WORLD"],
  :returns_code_type=>"01",
  :supplier_role=>"01",
  :active=>true,
  :epub_type_codes=>[],
  :price_currency_codes=>[],
  :price_type_codes=>[],
  :price_type_qualifier_codes=>[],
  :product_forms=>[],
  :supplier_id=>ips.id,
  :publishername_ids=>[],
  :imprint_ids=>[]
)

client.supplier_configs.create(
  :supply_to_countries=>["CA"],
  :supply_to_territories=>["US & TERRITORIES"],
  :returns_code_type=>"01",
  :supplier_role=>"02",
  :active=>true,
  :epub_type_codes=>[],
  :price_currency_codes=>[],
  :price_type_codes=>[],
  :price_type_qualifier_codes=>[],
  :product_forms=>[],
  :supplier_id=>ips.id,
  :publishername_ids=>[],
  :imprint_ids=>[]
).first_or_create



INGRAM
======

client.contact_digital_asset_transfer_templates.create(
 :contact_id=>ips.id,
 :digital_asset_subtype_id=>2,
 :supplier_ids=>[],
 :product_forms=>["BB", "BC"],
 :pub_dates_from=>nil,
 :compress=>false,
 :epub_type_codes=>[],
 :price_currency_codes=>["CAD", "USD"],
 :price_type_codes=>[],
 :price_type_qualifier_codes=>[],
 :publishername_ids=>[],
 :covers_directory=>nil,
 :ebook_files_directory=>nil,
 :send_ebook_files=>false,
 :send_cover_files=>true,
 :cover_style=>"jpg_rgb_1500h",
 :include_media_file_link=>true,
 :use_isbn_as_reference=>false,
 :suppress_market_representations=>false,
 :require_isbn=>true,
 :exclude_seriesname_ids=>[],
 :send_eur_prices_excvat=>false,
 :exctax_currency_codes=>[],
 :append_number_to_series=>false,
 :suppress_price_countries=>false,
 :create_sets_for_work_derivations=>false,
 :send_internal_work_identifier_only=>true,
 :apply_coresource_product_form_description=>true,
 :group_id=>nil,
 :prioritise_related_products_for_format=>false,
 :use_print_isbn_as_reference=>false,
 :add_person_name=>true,
 :send_publishers_subject_categories=>false,
 :cover_file_name_format=>1,
 :send_publisher_category_as_hierarchy=>false,
 :product_relation_codes=>[],
 :onix_file_name_pattern=>"#{client.identifier.tr("-","_")}_[%Y%m%d].xml",
 :directory=>nil,
 :send_publishers_work_url=>false,
 :use_alternative_marketing_text=>true,
 :add_barcode_indicator=>false,
 :allow_pub_date_on_any_status=>false,
 :suppress_auto_supplier_expected_ship_date=>false,
 :imprint_ids=>[],
 :directors=>[]
 )

 NIELSEN
 =========

client.contact_digital_asset_transfer_templates.create(
 :contact_id=>nielsen.id,
 :digital_asset_subtype_id=>2,
 :supplier_ids=>[],
 :product_forms=>[],
 :pub_dates_from=>nil,
 :compress=>false,
 :epub_type_codes=>[],
 :price_currency_codes=>[],
 :price_type_codes=>[],
 :price_type_qualifier_codes=>[],
 :publishername_ids=>[],
 :covers_directory=>"/images",
 :ebook_files_directory=>nil,
 :send_ebook_files=>false,
 :send_cover_files=>true,
 :cover_style=>"jpg_rgb_0650h",
 :include_media_file_link=>true,
 :use_isbn_as_reference=>true,
 :suppress_market_representations=>false,
 :require_isbn=>true,
 :exclude_seriesname_ids=>[],
 :send_eur_prices_excvat=>false,
 :exctax_currency_codes=>[],
 :append_number_to_series=>false,
 :suppress_price_countries=>false,
 :create_sets_for_work_derivations=>true,
 :send_internal_work_identifier_only=>false,
 :apply_coresource_product_form_description=>false,
 :group_id=>nil,
 :prioritise_related_products_for_format=>false,
 :use_print_isbn_as_reference=>false,
 :add_person_name=>true,
 :send_publishers_subject_categories=>true,
 :cover_file_name_format=>0,
 :send_publisher_category_as_hierarchy=>false,
 :product_relation_codes=>[],
 :onix_file_name_pattern=>"#{client.identifier.tr("-","_")}_[%Y%m%d].xml",
 :directory=>"/data",
 :send_publishers_work_url=>true,
 :use_alternative_marketing_text=>false,
 :add_barcode_indicator=>false,
 :allow_pub_date_on_any_status=>false,
 :suppress_auto_supplier_expected_ship_date=>false,
 :imprint_ids=>[],
 :directors=>["apply_tax_components_to_gbp"]
)


BDS
===
bds_ftp = client.ftp_templates.create(
  client_id: client.id,
  contact_id: bds.id,
  name: "BDS ONIX",
  description: nil,
  login_user: "bibliocloud",
  login_password: "3R8Q0D8c",
  login_acct: nil,
  connect_host: "ftp.bdsweb.co.uk",
  connect_port: nil,
  use_passive: nil,
  image_directory: nil,
  ftps_mode: 0
)

client.contact_digital_asset_transfer_templates.create(
 :contact_id=>bds.id,
 :digital_asset_subtype_id=>2,
 :transferable_id=>bds_ftp.id,
 :transferable_type=>"FtpTemplate",
 :supplier_ids=>[],
 :product_forms=>[],
 :pub_dates_from=>nil,
 :compress=>false,
 :epub_type_codes=>[],
 :price_currency_codes=>[],
 :price_type_codes=>[],
 :price_type_qualifier_codes=>[],
 :publishername_ids=>[],
 :covers_directory=>"/",
 :ebook_files_directory=>nil,
 :send_ebook_files=>false,
 :send_cover_files=>false,
 :cover_style=>"jpg_rgb_1500h",
 :include_media_file_link=>true,
 :use_isbn_as_reference=>true,
 :suppress_market_representations=>false,
 :require_isbn=>true,
 :exclude_seriesname_ids=>[],
 :send_eur_prices_excvat=>false,
 :exctax_currency_codes=>[],
 :append_number_to_series=>false,
 :suppress_price_countries=>false,
 :create_sets_for_work_derivations=>true,
 :send_internal_work_identifier_only=>false,
 :apply_coresource_product_form_description=>false,
 :group_id=>nil,
 :prioritise_related_products_for_format=>false,
 :use_print_isbn_as_reference=>false,
 :add_person_name=>true,
 :send_publishers_subject_categories=>true,
 :cover_file_name_format=>0,
 :send_publisher_category_as_hierarchy=>false,
 :product_relation_codes=>[],
 :onix_file_name_pattern=>"#{client.identifier.tr("-","_")}_[%Y%m%d].xml",
 :directory=>"/",
 :send_publishers_work_url=>false,
 :use_alternative_marketing_text=>false,
 :add_barcode_indicator=>false,
 :allow_pub_date_on_any_status=>false,
 :suppress_auto_supplier_expected_ship_date=>false,
 :imprint_ids=>[],
 :directors=>["apply_tax_components_to_gbp"]
)
