def print_usage(description)
  mb = GetProcessMem.new.mb
  puts "#{ description } - MEMORY USAGE(MB): #{ mb.round }"
end

def print_usage_before_and_after
  print_usage("Before")
  yield
  print_usage("After")
end

print_usage_before_and_after do
  Contact.can_be_contributor(Client.is_zed.id).select(%i(id names_before_key keynames corporate_name corporate_acronym)).lazy.map(&:dropdown).sort_by {|dropdown| I18n.transliterate(dropdown.first.upcase.sub(/^[[:punct:] ]*/,""))}
end


print_usage_before_and_after do
  x = Contact.can_be_contributor(Client.is_zed.id).select(%i(id names_before_key keynames corporate_name corporate_acronym)).to_a
  x.map!(&:dropdown)
  x.sort_by! {|dropdown| I18n.transliterate(dropdown.first.upcase.sub(/^[[:punct:] ]*/,""))}
  x
end


print_usage_before_and_after do
  x = Contact.can_be_contributor(Client.is_zed.id).select(%i(id names_before_key keynames corporate_name corporate_acronym)).to_a
end
148-161


print_usage_before_and_after do
  x = Contact.can_be_contributor(Client.is_zed.id).select(%i(id names_before_key keynames corporate_name corporate_acronym)).to_a
  x.map!(&:dropdown)
  x
end
149-163

print_usage_before_and_after do
  x = Contact.can_be_contributor(Client.is_zed.id).select(%i(id names_before_key keynames corporate_name corporate_acronym)).to_a
end
148-161

print_usage_before_and_after do
    MiniSql::Connection.new(ActiveRecord::Base.connection.raw_connection).
      query(
        <<~SQL,
          SELECT
            "contacts"."id",
            "contacts"."names_before_key",
            "contacts"."keynames",
            "contacts"."corporate_name",
            "contacts"."corporate_acronym"
          FROM "contacts"
          INNER JOIN "contact_contact_types" ON "contact_contact_types"."contact_id" = "contacts"."id"
          INNER JOIN "contact_types" ON "contact_types"."id" = "contact_contact_types"."contact_type_id"
          WHERE "contacts"."client_id" = ? AND "contact_types"."category" = ?
          order by coalesce(corporate_name, keynames || coalesce(names_before_key,''))
        SQL
        Client.is_zed.id,
        "Contributor"
      )
end



a = "a"
b = "b"
c = "c"




