Client.is_liverpool.publishernames.last.books.joins(:seriesnames).uniq.each do |book|
  book.seriesnames.each do |seriesname |
    BookSeriesname.where(book_id: book.id, seriesname_id: seriesname.id).first_or_create!
  end
end
