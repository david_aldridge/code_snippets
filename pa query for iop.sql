
with
  cte_works as (
    select works.*
    from   works join clients on clients.id = works.client_id
    where  clients.identifier = 'iop'),
 cte_contribs as (
  select  work_id,
          string_agg(person_name, ', ') contributors
  from    (
            select   wc.work_id,
                     c.person_name
            from     cte_works  w
            join     workcontacts wc on (wc.work_id = w.id)
            join     contacts     c on (c.id = wc.contact_id)
            order by wc.work_id,
                     wc.sequence_number) t
  group by work_id),
cte_products as (
  select  w.id work_id,
          b.isbn,
          case b.product_form
            when 'BB' then 'Hardback'
            when 'BC' then 'Paperback'
            when 'DG' then
              case b.epub_type_code
              when '002' then 'PDF'
              when '022' then 'Mobi'
              when '029' then 'ePub'
              end
            end format,
          row_number () over (partition by w.id order by b.product_form, b.epub_type_code) rn
  from    cte_works w
  join    books b on b.work_id = w.id
  where   b.pub_date <= current_date + interval '3 months' and
          b.isbn like '9%')
select  w.title ||
          case when w.edition is null
          then ''
          else ' (Edition '|| w.edition::varchar || ')'
          end as "Title",
        c.contributors as "Author",
        'Ebook' as "bookTypeDescription",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 1) "isbn1",
        (select format from cte_products p where p.work_id = w.id and rn = 1) "isbn1Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 2) "ibsn2",
        (select format from cte_products p where p.work_id = w.id and rn = 2) "isbn2Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 3) "isbn3",
        (select format from cte_products p where p.work_id = w.id and rn = 3) "isbn3Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 4) "isbn4",
        (select format from cte_products p where p.work_id = w.id and rn = 4) "isbn4Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 5) "isbn5",
        (select format from cte_products p where p.work_id = w.id and rn = 5) "isbn5Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 6) "isbn6",
        (select format from cte_products p where p.work_id = w.id and rn = 6) "isbn6Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 7) "isbn7",
        (select format from cte_products p where p.work_id = w.id and rn = 7) "isbn7Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 8) "isbn8",
        (select format from cte_products p where p.work_id = w.id and rn = 8) "isbn8Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 9) "isbn9",
        (select format from cte_products p where p.work_id = w.id and rn = 9) "isbn9Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 10) "isbn10",
        (select format from cte_products p where p.work_id = w.id and rn = 10) "isbn10Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 11) "isbn11",
        (select format from cte_products p where p.work_id = w.id and rn = 11) "isbn11Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 12) "ibsn12",
        (select format from cte_products p where p.work_id = w.id and rn = 12) "isbn12Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 13) "isbn13",
        (select format from cte_products p where p.work_id = w.id and rn = 13) "isbn13Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 14) "isbn14",
        (select format from cte_products p where p.work_id = w.id and rn = 14) "isbn14Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 15) "isbn15",
        (select format from cte_products p where p.work_id = w.id and rn = 15) "isbn15Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 16) "isbn16",
        (select format from cte_products p where p.work_id = w.id and rn = 16) "isbn16Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 17) "isbn17",
        (select format from cte_products p where p.work_id = w.id and rn = 17) "isbn17Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 18) "isbn18",
        (select format from cte_products p where p.work_id = w.id and rn = 18) "isbn18Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 19) "isbn19",
        (select format from cte_products p where p.work_id = w.id and rn = 19) "isbn19Description",
        (select isbn   from cte_products p where p.work_id = w.id and rn = 20) "isbn20",
        (select format from cte_products p where p.work_id = w.id and rn = 20) "isbn20Description"
from    works w
join    cte_contribs c on w.id = c.work_id
where   (select isbn from cte_products p where p.work_id = w.id and rn = 1) is not null
;
