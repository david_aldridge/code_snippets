client = Client.is_boldwood
format = client.proprietary_format_descriptions.find(2999)
format.books.each do |book|
  book.product_height_mm    = book.product_height_mm    || format.product_height_mm
  book.product_width_mm     = book.product_width_mm     || format.product_width_mm
  book.product_thickness_mm = book.product_thickness_mm || format.product_thickness_mm
  book.save
end
