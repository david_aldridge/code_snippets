

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install.html

$ brew update
$ brew install awsebcli
$ eb --version
EB CLI 3.14.6 (Python 2.7.1)

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-configuration.html

$ eb init

Select a default region
1) us-east-1 : US East (N. Virginia)
2) us-west-1 : US West (N. California)
3) us-west-2 : US West (Oregon)
4) eu-west-1 : EU (Ireland)
5) eu-central-1 : EU (Frankfurt)
6) ap-south-1 : Asia Pacific (Mumbai)
7) ap-southeast-1 : Asia Pacific (Singapore)
8) ap-southeast-2 : Asia Pacific (Sydney)
9) ap-northeast-1 : Asia Pacific (Tokyo)
10) ap-northeast-2 : Asia Pacific (Seoul)
11) sa-east-1 : South America (Sao Paulo)
12) cn-north-1 : China (Beijing)
13) cn-northwest-1 : China (Ningxia)
14) us-east-2 : US East (Ohio)
15) ca-central-1 : Canada (Central)
16) eu-west-2 : EU (London)
17) eu-west-3 : EU (Paris)
(default is 3): 4

Enter Application Name
(default is "consonance"):
Application consonance has been created.

It appears you are using Ruby. Is this correct?
(Y/n): y

Select a platform version.
1) Ruby 2.6 (Passenger Standalone)
2) Ruby 2.6 (Puma)
3) Ruby 2.5 (Passenger Standalone)
4) Ruby 2.5 (Puma)
5) Ruby 2.4 (Passenger Standalone)
6) Ruby 2.4 (Puma)
7) Ruby 2.3 (Passenger Standalone)
8) Ruby 2.3 (Puma)
9) Ruby 2.2 (Passenger Standalone)
10) Ruby 2.2 (Puma)
11) Ruby 2.1 (Passenger Standalone)
12) Ruby 2.1 (Puma)
13) Ruby 2.0 (Passenger Standalone)
14) Ruby 2.0 (Puma)
15) Ruby 1.9.3
(default is 1): 6
Note: Elastic Beanstalk now supports AWS CodeCommit; a fully-managed source control service. To learn more, see Docs: https://aws.amazon.com/codecommit/
Do you wish to continue with CodeCommit? (y/N) (default is n): y

Select a repository
1) bibliocloud
2) [ Create new Repository ]
(default is 2): 2

Enter Repository Name
(default is "origin"): consonance
Successfully created repository: consonance

Enter Branch Name
***** Must have at least one commit to create a new branch with CodeCommit *****
(default is "Isbnlist_robustifier"): master
Enter passphrase for key '/Users/david/.ssh/id_rsa':
Successfully created branch: master
Do you want to set up SSH for your instances?
(Y/n): y

Select a keypair.
1) DavesEC2Key
2) steamserver
3) [ Create new KeyPair ]
(default is 2): 3

Type a keypair name.
(Default is aws-eb):
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/david/.ssh/aws-eb.
Your public key has been saved in /Users/david/.ssh/aws-eb.pub.
The key fingerprint is:
SHA256:it4lhy7Cnia9c3UGNMZ3s/WN4EMYlDqN0oeKxUUHS2I aws-eb
The key's randomart image is:
+---[RSA 2048]----+
|     .E.+o++     |
|     .=ooo= +    |
|     + =.* * o o |
|      = * + o o .|
|     o +So   .   |
|    ..ooo        |
| o  ..+oo        |
|. *ooo +         |
| ++=..o          |
+----[SHA256]-----+
Enter passphrase:
WARNING: Uploaded SSH public key for "aws-eb" into EC2 for region eu-west-1.


comment: At this point, it's in no way obvious that the application is linked with CodeCommit!

$ eb create consonance-demo
Starting environment deployment via CodeCommit
--- Waiting for Application Versions to be pre-processed ---
Finished processing application version app-474fc6-190508_190659
Setting up default branch
Environment details for: consonance-demo
  Application name: consonance
  Region: eu-west-1
  Deployed Version: app-474fc6-190508_190659
  Environment ID: e-2bxkf3e4s7
  Platform: arn:aws:elasticbeanstalk:eu-west-1::platform/Puma with Ruby 2.4 running on 64bit Amazon Linux/2.9.3
  Tier: WebServer-Standard-1.0
  CNAME: UNKNOWN
  Updated: 2019-05-08 18:07:56.639000+00:00
Printing Status:
2019-05-08 18:07:55    INFO    createEnvironment is starting.
2019-05-08 18:07:56    INFO    Using elasticbeanstalk-eu-west-1-708746154661 as Amazon S3 storage bucket for environment data.
2019-05-08 18:08:27    INFO    Created security group named: sg-0c495d65e38735124
2019-05-08 18:08:43    INFO    Created load balancer named: awseb-e-2-AWSEBLoa-1XMFGK6C7OJ2L
2019-05-08 18:08:43    INFO    Created security group named: awseb-e-2bxkf3e4s7-stack-AWSEBSecurityGroup-X8VMOTH4G6N7
2019-05-08 18:08:43    INFO    Created Auto Scaling launch configuration named: awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingLaunchConfiguration-17UBQNF5R71HS
2019-05-08 18:10:32    INFO    Created Auto Scaling group named: awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingGroup-VG2XRO0QH3UO
2019-05-08 18:10:32    INFO    Waiting for EC2 instances to launch. This may take a few minutes.
2019-05-08 18:10:32    INFO    Created Auto Scaling group policy named: arn:aws:autoscaling:eu-west-1:708746154661:scalingPolicy:bc706f89-643c-4a9d-b0f7-4c0e7c5a1805:autoScalingGroupName/awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingGroup-VG2XRO0QH3UO:policyName/awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingScaleDownPolicy-E24GECZBT0LI
2019-05-08 18:10:32    INFO    Created Auto Scaling group policy named: arn:aws:autoscaling:eu-west-1:708746154661:scalingPolicy:e5d5b4bc-0128-4491-b4de-4903aaaf6e36:autoScalingGroupName/awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingGroup-VG2XRO0QH3UO:policyName/awseb-e-2bxkf3e4s7-stack-AWSEBAutoScalingScaleUpPolicy-80WCK2LI4C4C
2019-05-08 18:10:32    INFO    Created CloudWatch alarm named: awseb-e-2bxkf3e4s7-stack-AWSEBCloudwatchAlarmLow-F5RXC0D9GEY3
2019-05-08 18:10:32    INFO    Created CloudWatch alarm named: awseb-e-2bxkf3e4s7-stack-AWSEBCloudwatchAlarmHigh-3CGS8GGNMUGB
2019-05-08 18:10:57    ERROR   [Instance: i-08ea8139b2b93b42e] Command failed on instance. Return code: 1 Output: (TRUNCATED)...:in `find_spec_for_exe': can't find gem bundler (>= 0.a) with executable bundle (Gem::GemNotFoundException)
	from /opt/rubies/ruby-2.4.6/lib/ruby/site_ruby/2.4.0/rubygems.rb:308:in `activate_bin_path'
	from /opt/rubies/ruby-2.4.6/bin/bundle:23:in `<main>'.
Hook /opt/elasticbeanstalk/hooks/appdeploy/pre/10_bundle_install.sh failed. For more detail, check /var/log/eb-activity.log using console or EB CLI.
2019-05-08 18:10:58    INFO    Command execution completed on all instances. Summary: [Successful: 0, Failed: 1].
2019-05-08 18:12:00    ERROR   Create environment operation is complete, but with errors. For more information, see troubleshooting documentation.

Alert: An update to the EB CLI is available. Run "pip install --upgrade awsebcli" to get the latest version.


Comment: used https://stackoverflow.com/questions/55360450/elastic-beanstalk-cant-find-gem-bundler-0-a-with-executable-bundle-gem
Added gem_install_bundler.config


$ eb deploy consonance-demo
Enter passphrase for key '/Users/david/.ssh/id_rsa':
ERROR: InvalidParameterValueError - "Error making request to CodeCommit: Could not retrieve 406625857aa26b3a4a184e699dbca8d7e9800fe2 (Service: AWSCodeCommit; Status Code: 400; Error Code: CommitIdDoesNotExistException; Request ID: 76ee1413-fd21-4dd1-86cf-1eef845a748f)"
Davids-MBP-2:consonance davidaldridge$ git remote
codecommit-origin
demo
iop
iop-test
origin
production
test
Davids-MBP-2:consonance davidaldridge$ git push codecommit-origin
Counting objects: 5, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 1.48 KiB | 0 bytes/s, done.
Total 5 (delta 2), reused 0 (delta 0)
To https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/consonance
 * [new branch]          beanstalk -> beanstalk


Davids-MBP-2:consonance davidaldridge$ eb deploy consonance-demo
Enter passphrase for key '/Users/david/.ssh/id_rsa':
Starting environment deployment via CodeCommit
--- Waiting for Application Versions to be pre-processed ---
Finished processing application version app-406625-190508_192905
2019-05-08 18:30:08    INFO    Environment update is starting.
2019-05-08 18:30:55    INFO    Deploying new version to instance(s).
2019-05-08 18:31:22    ERROR   [Instance: i-08ea8139b2b93b42e] Command failed on instance. Return code: 11 Output: (TRUNCATED)...it://github.com/rails/activerecord-session_store.git'
"/opt/rubies/ruby-2.4.6/lib/ruby/gems/2.4.0/cache/bundler/git/activerecord-session_store-cf0cfe22b7614cc4fcd416e139ff2bd62ebd6f1d"
--bare --no-hardlinks --quiet` in directory /var/app/ondeck has failed.
Hook /opt/elasticbeanstalk/hooks/appdeploy/pre/10_bundle_install.sh failed. For more detail, check /var/log/eb-activity.log using console or EB CLI.
2019-05-08 18:31:22    INFO    Command execution completed on all instances. Summary: [Successful: 0, Failed: 1].
2019-05-08 18:31:22    ERROR   Unsuccessful command execution on instance id(s) 'i-08ea8139b2b93b42e'. Aborting the operation.
2019-05-08 18:31:23    ERROR   Failed to deploy application.

ERROR: ServiceError - Failed to deploy application.


Comment: Looked like git is not installed by default.
Added 00_packages.config based on https://stackoverflow.com/questions/13642171/elastic-beanstalk-ruby-rails-need-to-install-git-so-bundle-install-works-but-i

$ git push codecommit-origin
Counting objects: 4, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 1.02 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/consonance
   406625857..ecc0ba21e  beanstalk -> beanstalk


$ eb deploy consonance-demo


... and then it couldn't install rmagisk because the imagemagick version was too low

"Can't install RMagick 3.0.0. You must have ImageMagick 6.8.9 or later."
