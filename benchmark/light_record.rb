def print_usage(description)
  mb = GetProcessMem.new.mb
  puts "#{ description } - MEMORY USAGE(MB): #{ mb.round }"
end

def print_usage_before_and_after
  print_usage("Before")
  yield
  print_usage("After")
end

print_usage_before_and_after do
  Book.limit(6000).to_a
end

print_usage_before_and_after do
  Book.limit(6000).light_records.to_a
end
