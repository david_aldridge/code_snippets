Client.is_canelo.books.where(proprietary_format_description_id: 1064).pluck(:product_form_detail, :product_height_mm, :product_width_mm)


Client.is_canelo.
  books.where(proprietary_format_description_id: 1064).
  each do |book|
    book.update(
      product_form_detail: ["B105"],
      product_height_mm:   198,
      product_width_mm:    129
    )
  end


Client.is_canelo.books.where(proprietary_format_description_id: 2573).pluck(:product_form_detail, :product_height_mm, :product_width_mm)
