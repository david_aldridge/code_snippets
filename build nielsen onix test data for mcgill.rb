client = Client.is_mcgill_queens

CreateStandardContacts.new(client: client, identifiers: ["nielsen"]).call




product_forms = client.books.
  pluck(:product_form).compact.uniq - ["00","WW","WX"]
publishing_statuses = client.books.
  pluck(:publishing_status).compact.uniq.sort


eb = client.export_batches.create(
  contact_digital_asset_transfer_template_id: client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first.id,
  name: "Nielsen test",
  incremental: false
  )
  => 89943

eb.books +=
  client.imprints.flat_map do |imprint|
      books = imprint.books

      selected = books.order(pub_date: :desc).first(5)
      puts "imprint #{imprint}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
product_forms.flat_map do |product_form|
      books = client.books.
        where(product_form: product_form).pub_date_before(Date.today)


      selected = books.
        where.not(id: eb.book_ids).
        order(pub_date: :desc).
        first(5)
      puts "form #{product_form}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
publishing_statuses.flat_map do |pub_status|
      books = client.books.
        where(publishing_status: pub_status).pub_date_before(Date.today + 1.months)

      selected = books.where.not(id: eb.book_ids).
        order(pub_date: :desc).
        first(5)
      puts "status #{pub_status} (#{ONIXCode.lookup(:publishing_statuses, pub_status)}): #{selected.size} of #{books.size}"
      selected
    end ; 0


eb.get_content_items


ExportBatchItem.where(id: [11038153, 11038147, 11038127]).destroy_all


books  = eb.books.where.not(publishing_status: "01").for_check_runner
checks = CheckClass.all.checks

client.check_results.delete_all

CheckRunner.call(checks, client.books.for_check_runner).persist



eb.books.distinct.pluck(:work_id).each do |work_id|
  Delayed::Job.enqueue(
    UpdateWorkChecksJob.new(work_id: work_id),
    queue: DELAYED_JOB_QUEUES.default
  )
end ; 0


client.works.select do |w|
  !w.marketingtexts.where(legacy_code: "02").any? && w.marketingtexts.where(legacy_code: "03").any?
end.size


client.works.includes(:marketingtexts).map do |w|
  [
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)


client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)

["04", false, true, true]=>166,

client.books.onix_export_allowed.includes(:marketingtexts).select do |w|
    w.marketingtexts.none?{|x| x.legacy_code == "01"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "02"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
end.map do |w|
  [
    w.marketingtexts.detect{|x| x.legacy_code == "02"}.pull_quote.size,
    w.marketingtexts.detect{|x| x.legacy_code == "03"}.pull_quote.size
  ]
end.sort
group_by(&:itself).transform_values(&:size)







client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.detect{|x| x.legacy_code == "01"}&.pull_quote&.size&.>(350),
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.detect{|x| x.legacy_code == "02"}&.pull_quote&.size&.<=(350),
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)






client = Client.is_rethink
client.books.onix_export_allowed.map do |b|
 [
   b.imprint.to_s,
   b.isbn_registrant_hyphenated
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}
