
User.where("email like '%iop.org'").reject(&:dormant?).map do |u|
  new_email = u.email.split("@").first + "@ioppublishing.org"
  u.update(email: new_email)
end
