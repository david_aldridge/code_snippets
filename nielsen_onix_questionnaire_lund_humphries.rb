client = Client.by_key("lund")
ISBNs


pp client.books.
       select{|b| b.isbn_valid?}.
       map{|b| Lisbn.new(b.isbn).parts.try(:[],0,3).try(:join,Constant::HYPHEN)}.
       each_with_object(Hash.new(0)){|i,h| h[i] += 1}.sort.
       map{|k,v| puts "#{k.ljust(13)} #{v}"}

       978-0-7546    22      978-0-7546    2017-03-08
       978-0-85331   474     978-0-85331   2014-08-28
       978-0-85967   18      978-0-85967   1994-07-28
       978-1-84822   197     978-1-84822   2019-03-28
       978-1-85928   21      978-1-85928   2003-07-28
       978-1-86317   23      978-1-86317   2008-08-28


pp client.books.
      select{|b| b.isbn_valid?}.
      map{|b| [Lisbn.new(b.isbn).parts.try(:[],0,3).try(:join,Constant::HYPHEN), b.pub_date]}.
      each_with_object(Hash.new){|i,h| h.merge!(i[0] => i[1]){|k, v1, v2| [v1, v2].compact.max}}.sort.
      map{|k,v| puts "#{k.ljust(13)} #{v}"}

      978-0-7045
      978-0-7187    1991-02-28
      978-0-7546    2017-03-08
      978-0-7981    1999-02-28
      978-0-85307   1967-10-01
      978-0-85331   2014-08-28
      978-0-85351   1991-08-15
      978-0-85967   1994-07-28
      978-0-86824   1999-11-15
      978-0-88179   2001-07-28
      978-0-89468
      978-0-900157  2009-07-28
      978-0-904563
      978-0-904831  2002-01-28
      978-0-905227  1995-02-28
      978-0-906030  1990-01-28
      978-0-934658  2001-09-28
      978-0-948489  1999-02-28
      978-0-9515811 1998-06-30
      978-0-9519387
      978-0-9538525 2006-09-28
      978-1-4094    2012-11-01
      978-1-84014   2003-07-28
      978-1-84822   2019-03-28
      978-1-85514   2010-02-28
      978-1-85745   1993-04-28
      978-1-85928   2003-07-28
      978-1-86317   2008-08-28
      978-1-874084  1993-10-28
      978-1-901352  2011-05-28
      978-1-903470  2000-12-28
      978-2-7118    2007-11-28
      978-90-238    1989-01-06
      978-90-6611
      978-90-72828  1997-04-01

client.imprints.pluck(:name)
client.works.count
client.books.count

client.prices.
       group(:currency_code).
       order(:currency_code).
       count


isbn_prefixes = %w(978-0-7546 978-0-85331 978-0-85967 978-1-84822 978-1-85928 978-1-86317)


PUBLISHING_STATUSES  =  OnixCodeList.find_by(:list_number => 68 ).to_h
client.books.
       select{|b| Lisbn.new(b.isbn).parts.try(:[],0,3).try(:join,Constant::HYPHEN).in? isbn_prefixes}.
       map{|b| b.inferred_publishing_status[:status]}.
       each_with_object(Hash.new(0)){|b,h| h[b] += 1}.
       map{|p, c| puts "#{p}: #{PUBLISHING_STATUSES[p]} #{c}"}

pub_statuses = %w(       06
       04
       11
       07
       05
       02
       09
       08
       01
       03
       10)

       Rails.logger.level = 1

client.export_batches.create(:client_id => client.id,
                             :name => 'Nielsen text',
                             :incremental => false,
                                  
ExportBatch.find(39).items.destroy_all
pub_statuses = client.books.select(:publishing_status).uniq.pluck(:publishing_status)
books = []
pub_statuses.each do |publishing_status|
  books += client.books.where("pub_date between date '1990-01-01'  and ?", Date.today + 9.months).
                        order(:pub_date => :desc).
                        select{|b| b.inferred_publishing_status == publishing_status && b.isbn_valid}[0..50]
end

isbn_prefixes = [
'978086232%',
'9780901787%',
'9780905762%',
'978178032%',
'978178360%',
'978184277%',
'978184813%',
'978185649%']

isbn_prefixes.each do |isbn_prefix|
  books += client.books.where("pub_date between date '1990-01-01'  and ?", Date.today + 9.months).order(:pub_date => :desc).where("isbn like ?", isbn_prefix).limit(50).select{|b| b.isbn_valid?}
end


books.each_with_object(Hash.new(0)){|b, h| h[b.publishing_status] += 1}
books.uniq! ; 0
ExportBatch.find(39).books += books; 0
Rails.logger.level = 1
ExportBatch.find(39).get_content_items ; 0



client.prices.group(:book_id, :currency_code, :price_type, :onix_price_type_qualifier_id).having("count(*) > 1").count


client.prices.where(:currency_code => 'GBP', :price_type => '01').joins(:book).where(:books => {:product_form => ['BB','BC']})

Book.where(:id =>
client.prices.where(:currency_code => 'GBP', :price_type => '01').pluck(:book_id) -
client.prices.where(:currency_code => 'GBP', :price_type => '02').pluck(:book_id) ).pluck(:id, :pub_date, :product_form)





       client.books.
              group(:publishing_status).
              order(:publishing_status).
              count.
              map{|p, c| puts "#{p}: #{PUBLISHING_STATUSES[p]} #{c}"}


client.books.
      group(:product_form).
      order(:product_form).
      count

client.books.
      group(:epub_type_code).
      order(:epub_type_code).
      count
