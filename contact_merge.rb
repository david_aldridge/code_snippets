class ContactMerge
  def self.call(from_contact:, to_contact:)
    new(from_contact: from_contact, to_contact: to_contact).call
  end

  def initialize(from_contact:, to_contact:)
    self.from_contact = from_contact
    self.to_contact   = to_contact
  end

  def call
    to_contact.workcontacts = from_contact.workcontacts
  end

  private

  attr_accessor :from_contact, :to_contact

end
