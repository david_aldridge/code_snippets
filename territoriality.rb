class Territoriality
  def initialize(territoriality)
    self.territoriality = territoriality
  end

  COUNTRY_CODES = %w(GB IE FR DE PO US CA AU NZ)

  REGIONS = {
    "WORLD" => COUNTRY_CODES,
    "EUROPE" => %w(GB IE FR DE PO),
    "US" => %w(OH ID NY NE MS).map{|c| "US-#{c}"}
  }

  private

  attr_accessor :territoriality

  def tokenized

end




matcher = Regexp.union(["WORLD", "US-OH", "US", "+", "-"])

"IE GB" =~ matcher
matcher =~ "IE GB"
"IE GB WORLD".scan(matcher)

matches = "WORLD -US+US-OH".scan(matcher)
matches.map{|match| REGIONS[match] || match}
