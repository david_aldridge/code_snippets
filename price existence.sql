select
  id book_id,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'GBP') then true else false end has_gbp,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'EUR') then true else false end has_eur,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'USD') then true else false end has_usd,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'CAD') then true else false end has_cad,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'NZD') then true else false end has_nzd,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'INR') then true else false end has_inr,
  case when exists(select 1 from prices p where p.book_id = b.id and p.currency_code = 'ZAR') then true else false end has_zar
from books b
