class DeweyDecimalClassification
  def initialize(ddc)
    @ddc = ddc
  end

  def valid?
    ddc&.match VALID
  end

  private

  attr_reader :ddc

  VALID = /\A\d{3}(\.\d+)?\z/
end
