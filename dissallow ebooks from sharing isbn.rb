p = Book.joins(:client).merge(Client.active).
  isbn_assigned.
  where("isbn like '9%'").
  group(:client_id, :isbn).
  having("count(*) > 1").
  count.keys.map(&:first).uniq


Client.where.not(id: p).where(allow_ebooks_to_share_isbns: true).update(allow_ebooks_to_share_isbns: false)
