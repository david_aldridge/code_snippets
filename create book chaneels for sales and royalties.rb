ActiveRecord::Base.transaction do
  Client.is_bds.sales.
    includes(:book, :channel).
    map{|s| [s.book, s.channel]}.
    uniq.
    each do |book, channel|
    next if channel.in? book.channels

    masterchannel = channel.masterchannel
    book.masterchannels << masterchannel unless masterchannel.in? book.masterchannels
    book_masterchannel = book.book_masterchannels.find_by(masterchannel_id: masterchannel.id)
    book_masterchannel.channels << channel
  end
end




    def setup_book_channels
      ActiveRecord::Base.transaction do
        books_and_channels.each do |book, channel|
          next if channel.in? book.channels

          masterchannel = channel.masterchannel
          book.masterchannels << masterchannel unless masterchannel.in? book.masterchannels
          book_masterchannel = book.book_masterchannels.find_by(masterchannel_id: masterchannel.id)
          book_masterchannel.channels << channel
        end
      end
    end
