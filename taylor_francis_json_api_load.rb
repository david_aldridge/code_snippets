  json_text = URI.open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/456yhgf456ygfe56yhg.xml") { |io| io.read }

  feed = JsonFeed.new(JSON.parse(json_text))

  client = Client.is_taylor_francis

  client.works.destroy_all
  Rails.logger.level=0

client.standard_price_types = ["gbp_exctax", "usd_exctax", "aud_exctax", "nzd_exctax", "inr_exctax"]

client.standard_price_qualifiers = ["consumer", "corporate"]


url = "https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/456yhgf456ygfe56yhg.xml"

TaylorFrancis::Importer::Loader.new(url).call


