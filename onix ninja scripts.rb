EBook
=============================================


file = "/Users/david/rails/onix_ninja_files/koganpage_201811280902_orig.xml"

Rails.logger.level = 1

CurrencyExchangeRate.update_rates

KoganPage.new(file).augment

ReadOnixPrice.new(file).read_prices ; 0

ReadOnixPrice.new(file.gsub("_orig", "_new")).read_prices ; 0


Print
=============================================

Add discount codes at the end of the existing list in:
  "/Users/david/rails/onix_ninja_files/isbn_discount_code_override.csv"
Professional/Academic is AKOGA02
General is AKOGA01
Specialist mono & Yearbooks are AKOGA02

Rails.logger.level = 1
file = "/Users/david/rails/onix_ninja_files/KoganPage101220181440_orig.xml"
KoganPagePrint.new(file).augment

EBook Test
=============================================

file = "/Users/david/rails/onix_ninja_files/test_orig.xml"

Rails.logger.level = 1

irb KoganPage.new(file)



No Library price
================


doc = Nokogiri::XML(open(file), nil) do |config|
  config.default_xml.noblanks
end ; 0
doc.xpath("//xmlns:Product").map{|n| ProductNode.new(n)}.select do |node|
  node.library_price_subject_node.nil?
end.map(&:isbn)


"9780749474959"
"9780749474959"
"9780749474959"
"9780749471323"
"9780749471323"
"9780749471323"
"9780749468071"
"9780749468071"
"9780749468071"
"9780749469733"
"9780749469733"
"9780749469733"
"9780749471675"
"9780749471675"
"9780749471675"
"9780749452469"


Read ISBNS
============


doc2 = Nokogiri::XML(open(file2), nil) do |config|
  config.default_xml.noblanks
end

onix_isbns2 = doc2.xpath("xmlns:ONIXMessage/xmlns:Product/xmlns:ProductIdentifier[xmlns:ProductIDType=15]/xmlns:IDValue").map(&:text)
onix_isbns.size
onix_isbns.uniq.size

spreadsheet_isbns =  %w(
9780749472603
9780749473501
9780749474324
9780749477684
9780749478032
9780749478414
9780749478452
9780749478735
9780749478841
9780749478902
9780749479770
9780749479817
9780749479930
9780749480011
9780749480059
9780749480134
9780749480158
9780749480233
9780749480356
9780749480479
9780749480608
9780749480622
9780749480684
9780749480707
9780749480745
9780749480769
9780749480882
9780749480943
9780749480967
9780749480981
9780749481001
9780749481025
9780749481049
9780749481063
9780749481124
9780749481148
9780749481162
9780749481209
9780749481223
9780749481247
9780749481285
9780749481322
9780749481384
9780749481407
9780749481445
9780749481483
9780749481544
9780749481568
9780749481599
9780749481643
9780749481698
9780749481704
9780749481735
9780749481797
9780749481827
9780749481858
9780749481872
9780749481896
9780749481933
9780749481957
9780749481971
9780749482008
9780749482015
9780749482046
9780749482084
9780749482107
9780749482121
9780749482145
9780749482169
9780749482183
9780749482206
9780749482220
9780749482244
9780749482282
9780749482305
9780749482329
9780749482343
9780749482367
9780749482381
9780749482428
9780749482466
9780749482558
9780749482572
9780749482657
9780749482732
9780749482831
9780749483012
9780749483050
9780749483074
9780749483135
9780749483623
9780749483159
9780749483807
9780749474942
)


onix_isbns - spreadsheet_isbns
spreadsheet_isbns - onix_isbns



No ProductSupply
============

doc = Nokogiri::XML(open(file), nil) do |config|
  config.default_xml.noblanks
end

no_supply = doc.xpath("xmlns:ONIXMessage/xmlns:Product").select do |product_node|
  product_node.xpath("xmlns:ProductSupply").nil?
end

no_supply_isbns = no_supply.map do |product_node|
  product_node.at_xpath("xmlns:ProductIdentifier[xmlns:ProductIDType=15]/xmlns:IDValue").text
end

Not 3 ProductSupplies
=====================

doc = Nokogiri::XML(open(file), nil) do |config|
  config.default_xml.noblanks
end

no_supply = doc.xpath("xmlns:ONIXMessage/xmlns:Product").select do |product_node|
  product_node.xpath("xmlns:ProductSupply").size != 3
end

no_supply = no_supply.map do |product_node|
  product_node.at_xpath("xmlns:ProductIdentifier[xmlns:ProductIDType=15]/xmlns:IDValue").text
end


No related product
==================

doc = Nokogiri::XML(open(file), nil) do |config|
  config.default_xml.noblanks
end; 0

doc.xpath("//xmlns:Product").map{|x| ProductNode.new(x)}.reject do |node, i|
  node.related_product("13".freeze, "BC".freeze) ||
  node.related_product("13".freeze, "BB".freeze) ||
  node.related_product("13".freeze, "BA".freeze) ||
  node.related_product("13".freeze, nil        )
end.map(&:isbn)


doc.xpath("//xmlns:Product").map{|x| ProductNode.new(x)}.select do |node, i|
  node.product_supplies.empty?
end.map(&:isbn)


product_supplies



No price
==================

file = "/Users/david/rails/onix_ninja_files/KoganPage021120181533.xml"

doc = Nokogiri::XML(open(file), nil) do |config|
  config.default_xml.noblanks
end; 0

all_isbns = doc.xpath("//xmlns:Product").map{|x| ProductNode.new(x)}.map(&:isbn)
usd_isbns = doc.xpath("//xmlns:Product[xmlns:ProductSupply/xmlns:SupplyDetail/xmlns:Price/xmlns:CurrencyCode='USD']").map{|x| ProductNode.new(x)}.map(&:isbn)
cad_isbns = doc.xpath("//xmlns:Product[xmlns:ProductSupply/xmlns:SupplyDetail/xmlns:Price/xmlns:CurrencyCode='CAD']").map{|x| ProductNode.new(x)}.map(&:isbn)

all_isbns - usd_isbns
all_isbns - cad_isbns
2.2.7 :023 > all_isbns - usd_isbns
 => ["9780749484668", "9780749498139", "9780749481537", "9780749481667"]
2.2.7 :024 > all_isbns - cad_isbns
 => ["9780749484668", "9780749498139", "9780749481537", "9780749481667"]
2.2.7 :025 >


product_supplies
