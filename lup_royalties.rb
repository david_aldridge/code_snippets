class LupRoyalties

  def initialize(key,client)
    @connection = GoogleDriveConn.new
    @sheet      = @connection.session.spreadsheet_by_key(key)
    @worksheet  = @sheet.worksheet_by_title("Royalty data")
    @raw_data   = @worksheet.list
    @client     = client
  end

  def raw_data
    @raw_data
  end

  def set_work_ids
    @raw_data.each do |row|
      unless row["HB ISBN"].blank?
        if hb = Book.find_by(:isbn => row["HB ISBN"])
          row["HB Book ID"] = hb.id
          row["HB Work ID"] = hb.work_id
        end
      end
      unless row["PB ISBN"].blank?
        if pb = Book.find_by(:isbn => row["PB ISBN"])
          row["PB Book ID"] = pb.id
          row["PB Work ID"] = pb.work_id
        end
      end
    end
    @worksheet.save
  end
end


__END__

reload!
Rails.logger.level = 1
p = LupRoyalties.new("1c8UYwhxs2Fp_IYCjo8gnWyIXID59S3K-_Q7X_eRcJJg", Client.by_key("lup"))
p.isbn_hash
p.raw_data
p.set_work_ids
