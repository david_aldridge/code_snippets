client = Client.is_leuven

client.books.includes(:work).each do |b|
  b.full_title = b.work.title
  b.save if b.changed?
end
