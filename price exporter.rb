

class PriceExporter

  def initialize(client:, user: nil)
    @client = client
    @user   = user
  end

  def data_rows
    products.map do |product|
      [
        product.id,
        product.title,
        product.product_form,
        product.pub_date_y_m_d,
        product.edition,
        product.isbn,
        product.publishers_reference,
        price_methods.map{|price_method| product.send(price_method)}
      ].flatten(1)
    end
  end

  def header_row
    ["ID", "Title", "Product form", "Pub date", "Edition", "ISBN", "Publisher's Ref"] + prices.map{|_k,v| v.map{|qual| qual[:method]}}.flatten
  end

  def send_to_s3
    s3     = AWS::S3.new
    key    = File.basename(file_name)
    upload = s3.buckets[ENV['AWS_BUCKET']].objects["temp/#{key}"].write(:file => file_name, :acl => :public_read,:expires=> (Time.now + 3600).httpdate)
  end


  private
    attr_accessor :client

    def upload_url
      if upload.is_a? AWS::S3::S3Object
        upload.public_url
      else
        upload.object.public_url
      end
    end

    def upload_object
      @upload_object ||= (
        s3     = AWS::S3.new
        key    = File.basename(file_name)
        s3.buckets[ENV['AWS_BUCKET']].objects["temp/#{key}"].write(:file => file_name, :acl => :public_read)
        )
    end

    def prices
      prices = client.standard_prices
    end

    def products
      client.books.order(:id).includes(:work, :prices, :client).select(:id, :work_id, :client_id, :edition, :title, :product_form, :pub_date, :isbn, :publishers_reference)
    end

    def price_methods
      prices.map{|_k,v| v.map{|qual| qual[:method]}}.flatten
    end

    def excel
      @excel ||= Axlsx::Package.new
    end

    def file_name
      @file_name ||= "#{Rails.root}/public/#{(client.identifier || client.id.to_s)}_#{Time.now.strftime('%Y%m%d%H%M%S')}.xlsx"
    end


end

class ExcelBuilder

  def initialize(data_obj:, base_name: 'data', worksheet_name: 'Sheet1')
    self.data_obj       = data_obj
    self.base_name      = base_name
    self.worksheet_name = worksheet_name
  end

  def excel
    @excel = (
      exceller.workbook do |workbook|
        workbook.add_worksheet(:name => worksheet_name) do  |worksheet|
          #worksheet.fit_to_page    = true
          #worksheet.show_gridlines = false
          worksheet.add_row        data_obj.data_rows[0]
          data_obj.data_rows[1..-1].each{ |data_row| worksheet.add_row data_row }
        end
      end
      exceller
      )
  end

  def file_name
    @file = (
      exceller.use_shared_strings = true
      exceller.serialize(full_file_name)
      full_file_name
      )
  end

  private
    attr_accessor :data_obj, :base_name, :worksheet_name

    def exceller
      @exceller ||= Axlsx::Package.new
    end

    def full_file_name
      "#{Rails.root}/public/#{base_name}.xlsx"
    end


end


class S3Upload
  def initialize(file_obj:, timestamp_object_name: true, delete_after_upload: true, prefix: 'temp/', acl: :public_read, expiry_seconds: nil)
    self.file_obj            = file_obj
    self.delete_after_upload = delete_after_upload
    self.location            = location
    self.acl                 = acl
    self.expiry_seconds      = expiry_seconds
    self.timestamp_object_name = timestamp_object_name
  end

  def call
    @call ||= bucket.objects[object_name].write(write_options)
  end

  def url
    call.public_url
  end

  private
    attr_accessor :file_obj, :delete_after_upload, :prefix, :acl, :expiry_seconds, :timestamp_object_name

    def bucket
      @bucket ||= AWS::S3.new.buckets[ENV['AWS_BUCKET']]
    end

    def write_options
      {
        :file => file_name,
        :acl  => acl
      }
    end

    def object_name
      [
        prefix,
        File.basename(file_name,'.*'),
        (Time.now.strftime('%Y%m%d%H%M%S') if timestamp_object_name),
        File.extname(file_name)
      ].compact.join
    end

    delegate :file_name, to: :file_obj

end



S3Upload.new(file_obj: ExcelBuilder.new(data_obj: PriceExporter.new(client: Client.is_liverpool), worksheet_name: 'Sheet2')).url



NotifyJob = Struct.new(:proc, :user, ) do
  def perform
    emails.each { |e| NewsletterMailer.deliver_text_to_email(text, e) }
  end
end

Delayed::Job.enqueue NewsletterJob.new('lorem ipsum...', Customers.pluck(:email))
