class DigitalObjectIdentifier
  REGEX = %r{^(?<directory_indicator>10).(?<registrant>[^/ ]+)/(?<suffix>.+)$}

  def initialize(string, resolving_service = "https://dx.doi.org")
    @match             = string.match REGEX
    @resolving_service = resolving_service
  end

  def url
    "#{resolving_service}/#{URI.encode(to_s)}" if valid?
  end

  def to_s
    "#{directory_indicator}.#{registrant}/#{suffix}" if valid?
  end

  def directory_indicator
    @match[:directory_indicator] if valid?
  end

  def registrant
    @match[:registrant] if valid?
  end

  def suffix
    @match[:suffix] if valid?
  end

  def valid?
    !match.nil?
  end

  private

  attr_reader :match, :resolving_service
end
