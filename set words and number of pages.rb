Client.is_purdue.books.includes(work: :books).where(pages_arabic: nil).each do |b|
  b.pages_arabic = b.work.books.map(&:total_numbered_pages).compact.max ||
   b.work.books.map(&:main_content_page_count).compact.max ||
   b.work.books.map(&:production_page_count).compact.max ||
   b.work.books.map(&:pages_arabic).compact.max
  b.save
end

Client.is_purdue.books.includes(work: :books).where(number_of_words: nil).each do |b|
  b.number_of_words = b.work.books.map(&:number_of_words).compact.max
  b.save
end
