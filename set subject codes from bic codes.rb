code_map = {
"BGS" => "Canelo",
"BGSA" => "Canelo",
"BM" => "Canelo",
"FC" => "Abandoned Bookshop",
"FF" => "Canelo",
"FFC" => "Abandoned Bookshop",
"FFH" => "Canelo Adventure",
"FHD" => "Canelo Action Thriller",
"FJH" => "Canelo Adventure",
"FJM" => "Canelo Action Thriller",
"FJMS" => "Canelo Action Thriller",
"FK" => "Canelo",
"FM" => "Canelo Adventure",
"FQ" => "Canelo Adventure",
"FR" => "Canelo",
"FT" => "Canelo Saga",
"FV" => "Canelo Saga",
"FYB" => "Frisch & Co.",
"KJP" => "Canelo",
"LNRC" => "Canelo",
"WGF" => "Canelo",
"WH" => "Canelo",
"WT" => "Abandoned Bookshop",
"YFM" => "Canelo"
}

code_map.each do |bic, in_house|
  client.works.where(main_bic_code: bic).each do | work|
    work.subjectcodes = [client.subjectcodes.find_by(value: in_house)]
    work.subjectcode  = work.work_subjectcodes.first
    work.save if work.changed?
  end
end
