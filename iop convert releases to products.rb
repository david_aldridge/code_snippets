ActiveRecord::Base.transaction do
  Client.is_morgan_claypool.collections.select{|c| c.name.starts_with?("Release ")}.each do |collection|
    client = collection.client
    new_work = client.works.create(
      title: collection.name,
      imprint: collection.books.first.try(:imprint)
    )
    new_work.create_contract
    new_work.work_originations = collection.works.uniq.map do |original_work|
      WorkDerivation.new(
        derivation_types: [WorkDerivation::COMPILATION_TYPE_TEXT],
        original_work_id: original_work.id,
        client_id: client.id
      )
    end

    ped = client.proprietary_edition_descriptions.where(
      name: "Release"
    ).first_or_create(
      code: "Release"
    )

    pfd = client.proprietary_format_descriptions.where(
      name: "eBook Multiformat (Release)"
    ).first_or_create(
      description: "Use for Releases only",
      product_form: "DG",
      epub_type_code: "098",
      product_content_type: ["07", "10", "19"]
    )

    new_work.books.create(
      title: new_work.title,
      proprietary_edition_description_id: ped.id,
      proprietary_format_description_id: pfd.id
    )
  end
  #raise ActiveRecord::Rollback
end

Work.where("created_at >= ?", Date.today - 1).destroy_all


Client.is_morgan_claypool.works.find_by(title: "Release 1").update_attributes(title: "IOP Concise Physics, Release 1")
Client.is_morgan_claypool.works.find_by(title: "Release 2").update_attributes(title: "IOP Concise Physics, Release 2")
Client.is_morgan_claypool.works.find_by(title: "Release 3").update_attributes(title: "IOP Concise Physics, Release 3")
Client.is_morgan_claypool.works.find_by(title: "Release 4").update_attributes(title: "IOP Concise Physics, Release 4")
