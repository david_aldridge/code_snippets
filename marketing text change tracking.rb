class ChangeTracking

  def initialize(instance:, attribute:)
    self.instance  = instance
    self.attribute = attribute
  end

  def changes
    instance.versions.where("object_changes like '%#{attribute.to_s}:%'").select{|version| version.changeset.has_key? attribute.to_s}.
             map{|version| change_struct(version)}
  end

  def change_struct(version)
    result = Struct.new(:version,:from, :to, :diff)
    from,to = version.changeset[attribute.to_s]
    result.new(
      version,
      from,
      to,
      Diffy::Diff.new(from,to)
      )
  end

  def sequential_char_diff
    char_diff.each_with_object([]) do |diff, big_diff|
      if big_diff.empty? || big_diff.last[0] != diff[0]
        big_diff << [diff[0], diff[1] ]
      else
        big_diff.last[1] += diff[1]
      end
    end
    big_diff
  end

  private
    attr_accessor :instance, :attribute

end






__END__


instance = Marketingtext.find(45107)
attribute = :marketing_text


ct = ChangeTracking.new(instance: instance, attribute: attribute)
ct.changes.first

instance.versions.where("object_changes like '\n%#{attribute.to_s}:%'").first.changeset.has_key? "marketing_text"





seq1 = "The flip-side of Pan Peter, Alias blah blah Hook is a time-traveling love story about male and female, love and war, and the delicate art of growing up."

seq2=  "The flip-side of Peter Pan, Alias Hook is a time-traveling love or something story about male and female, love and war, and the delicate art of growing up. North American rights just sold for a 4 digit sum to St Martin's Press."



require 'diff/lcs'
require 'diff/lcs/string'



pp Diff::LCS.sdiff(seq1, seq2).first



pp Diff::LCS.sdiff(seq1, seq2).map{|c| [c.action, c.old_element || c.new_element ]}


char_diff = Diff::LCS.sdiff(seq1, seq2).map{|c| [c.action, c.old_element || c.new_element ]}

big_diff = []

sequence_diff = char_diff.each_with_object([]) do |diff, big_diff|
  if big_diff.empty? || big_diff.last[0] != diff[0]
    big_diff << [diff[0], diff[1] ]
  else
    big_diff.last[1] += diff[1]
  end
  puts big_diff
end


PP.pp(sequence_diff, )
