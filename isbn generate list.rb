ISBNReader.new(
    isbn: "9781947447001"
  ).isbn13_check_digit



Isbnlist.generate_list_from_prefix(
  ISBNReader.new(
    isbn: "9781947447"
  ).
  isbn_registrant_hyphenated.delete("-")
).join(",")

client = Client.is_notre_dame

isbns = Isbnlist.
  generate_list_from_prefix("9780268").
  select{|x| x >= "9780268200008"}.
  map do |isbn|
  Isbnlist.new(
    client: client,
    number: isbn,
    used:   "not used",
    imprint: "University of Notre Dame Press"
  )
end ; 0
Isbnlist.import isbns

978-0-268-20000-8
