pp Price.group(:currency_code, :price_type, :onix_price_type_qualifier_id).count.sort

[["AUD", "01", "05"], 739],
[["AUD", "02", "05"], 6581],
[["AUD", "12", "05"], 45],
[["AUD", "22", "05"], 3],
[["CAD", "01", "05"], 3623],
[["CAD", "02", "05"], 585],
[["CNY", "41", nil], 2],
[["EUR", "01", "05"], 3675],
[["EUR", "02", "05"], 3152],
[["EUR", "02", "06"], 203],
[["GBP", "01", "05"], 8393],
[["GBP", "01", "06"], 133],
[["GBP", "02", "01"], 656],
[["GBP", "02", "05"], 36918],
[["GBP", "02", "06"], 4888],
[["GBP", "02", "12"], 156],
[["GBP", "03", "05"], 60],
[["GBP", "22", "01"], 516],
[["GBP", "42", "00"], 2],
[["GBP", "42", "05"], 2],
[["GBP", "42", "06"], 1],
[["GIP", "01", "05"], 1],
[["INR", "01", "05"], 703],
[["INR", "02", "05"], 87],
[["JPY", "02", "05"], 305],
[["NZD", "01", "05"], 692],
[["NZD", "02", "05"], 5428],
[["QAR", "01", "05"], 227],
[["USD", "01", "01"], 146],
[["USD", "01", "05"], 22813],
[["USD", "01", "06"], 5069],
[["USD", "01", "12"], 156],
[["USD", "02", "05"], 3325],
[["USD", "21", "01"], 145],
[["USD", "41", "00"], 4],
[["USD", "41", "05"], 3],
[["USD", "41", "06"], 206],
[["ZAR", "02", "05"], 1024]]

x = Price.aud.
  joins(:client).
  group(:client_id, :price_type, :onix_price_type_qualifier_id).
  count.
  map do |x,c|
    client = Client.find(x.first)
    if client.request_logs.exists?
      [
        client.client_name,
        #client.identifier,
        x.first,
        #client.request_logs.last.created_at,
        x[1],
        x[2],
        c
      ]
    else
      [
        Client.find(x.first).client_name,
        #client.identifier,
        x.first,
        #nil,
        x[1],
        x[2],
        c
      ]
    end
  end.
  sort

pp x

347

Client.find(402).prices.delete_all
Client.find(347).prices.delete_all
Client.find(351).prices.delete_all
Client.find(341).prices.delete_all
Client.find(349).prices.delete_all





client_with_funky_gbp_price =
  Price.usd.
    inctax.
    joins(:client).
    group(:client).
    count.
    map{|c,x| [c.client_name,x]}

pp client_with_funky_gbp_price



pp Client.all.map(&:standard_price_types).uniq.sort
pp Client.all.flat_map(&:standard_price_types).uniq.sort


x = ContactDigitalAssetTransferTemplate.
  select{|t| t.exctax_currency_codes.any?}.
  map{|x| [x.id, x.client.client_name, x.contact.to_s, x.exctax_currency_codes, x.client.standard_price_types]}







snowbooks_amazon_digital_template = ContactDigitalAssetTransferTemplate.find(75)
snowbooks_amazon_digital_template.exctax_currency_codes = []
snowbooks_amazon_digital_template.directors = %w(use_gbp_inc_to_add_gbp_exc)
snowbooks_amazon_digital_template.save

ExportBatch.find(11041).get_content_items



snowbooks_nielsen_template = Client.is_snowbooks.contacts.is_nielsen.contact_digital_asset_transfer_templates.first
snowbooks_nielsen_template.exctax_currency_codes = []
snowbooks_nielsen_template.directors = %w(apply_tax_components_to_gbp)
snowbooks_nielsen_template.save

ExportBatch.find(11044).get_content_items


ContactDigitalAssetTransferTemplate.
"amazon", "amazon-p2k"
