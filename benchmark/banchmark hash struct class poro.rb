require 'memory_profiler'

MemoryProfiler.report do
  x = {
    isbn:  "978-1-911390-21-3",
    title: "Shakespeare Vs. Cthulhu"
  }
end.pretty_print

allocated memory by class
-----------------------------------
       192  Hash
        80  String


MemoryProfiler.report do
  OpenStruct.new(
    isbn:  "978-1-911390-21-3",
    title: "Shakespeare Vs. Cthulhu"
  )
end.pretty_print

allocated memory by class
-----------------------------------
       384  Hash
        80  String
        40  OpenStruct

MemoryProfiler.report do
  OpenStruct.new.tap do |y|
    y.isbn = "978-1-911390-21-3"
    y.title = "Shakespeare Vs. Cthulhu"
  end
end.pretty_print

allocated memory by class
-----------------------------------
       640  Proc
       560  MatchData
       504  Class
       320  String
       192  Hash
       160  Array
        40  OpenStruct


x = Struct.new(:isbn, :title)
MemoryProfiler.report do
  x.new("978-1-911390-21-3", "Shakespeare Vs. Cthulhu")
end.pretty_print


allocated memory by class
-----------------------------------
        80  String
        40  <<Unknown>>


class Y
  attr_accessor :isbn, :title
end

MemoryProfiler.report do
  Y.new.tap do |y|
    y.isbn = "978-1-911390-21-3"
    y.title = "Shakespeare Vs. Cthulhu"
  end
end.pretty_print

allocated memory by class
-----------------------------------
        80  String
        40  Y

class X
  def initialize(isbn:, title:)
    @isbn = isbn
    @title = title
  end

  attr_reader :isbn, :title
end

MemoryProfiler.report do
  X.new(isbn: "978-1-911390-21-3", title: "Shakespeare Vs. Cthulhu")
end.pretty_print

allocated memory by class
-----------------------------------
       384  Hash
        80  String
        40  X



class Z
  def initialize(isbn, title)
    @isbn = isbn
    @title = title
  end

  attr_reader :isbn, :title
end

MemoryProfiler.report do
  Z.new("978-1-911390-21-3", "Shakespeare Vs. Cthulhu")
end.pretty_print

allocated memory by class
-----------------------------------
        80  String
        40  Z



