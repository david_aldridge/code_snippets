client = Client.is_quiller

2: Missing Contained Items

client.books.where(product_form: "WW").pluck(:isbn)

Dave to fix.



3:  Missing Edition Number

client.books.includes(:work).map do |book|
  [
    book.edition,
    book.edition_statement,
    book.edition_type_codes
  ]
end.
reject{|x| x.compact.empty?}.
group_by(&:itself).
transform_values(&:size)

pp _

{[nil, "New edition", nil]=>43,
 [2, nil, ["REV"]]=>3,
 [2, "2nd Revised edition", ["REV"]]=>59,
 [nil, "Reprint edition", nil]=>1,
 [14, "14th Revised edition", nil]=>2,
 [11, "11th Revised edition", nil]=>1,
 [2, "2nd Revised edition", nil]=>14,
 [nil, "New ed of 11 Revised ed", nil]=>1,
 [nil, "Revised edition", nil]=>5,
 [2, "2nd edition", nil]=>2,
 [3, "3rd Edition", nil]=>1,
 [3, "3rd ed.", nil]=>1,
 [5, "5th Revised edition", ["REV"]]=>2,
 [3, "3rd Revised edition", ["REV"]]=>6,
 [2, "2nd ed.", ["REV"]]=>2,
 [2, "New edition", nil]=>3,
 [1, nil, nil]=>21,
 [nil, "Limited edition", nil]=>2,
 [1, "Revised edition", ["REV"]]=>1,
 [2, nil, nil]=>5,
 [nil, "Deluxe ed.", ["NUM", "SPE"]]=>1,
 [2, "2nd Edition", nil]=>3,
 [4, "4th Revised edition", ["REV"]]=>7,
 [15, nil, nil]=>1,
 [2, "Revised (Pocket) Edition", ["REV"]]=>2,
 [11, "11th Revised edition", ["REV"]]=>1,
 [7, "7th Revised edition", ["REV"]]=>4,
 [nil, "New Revised Edition", nil]=>2,
 [16, "16th Revised edition", ["REV"]]=>1,
 [nil, "Completely revised and updated", ["REV"]]=>1,
 [3, nil, nil]=>2,
 [nil, "British ed.", nil]=>1,
 [4, "4th Revised edition", nil]=>1,
 [2, "2nd ed.", nil]=>5,
 [12, "12th Revised edition", nil]=>2,
 [nil, "Rev ed.", nil]=>1,
 [13, "13th Revised edition", nil]=>2,
 [3, "3rd Revised edition", nil]=>1,
 [2, "2nd Enlarged edition", ["REV"]]=>2,
 [2, "Second Edition", nil]=>3,
 [8, "8th Revised edition", nil]=>1,
 [nil, "Revised and updated ed", nil]=>1,
 [6, "6th New edition", nil]=>1,
 [9, "9th Revised edition", nil]=>1,
 [2, "2nd Revised, Updated ed.", nil]=>1,
 [9, "9th New edition", nil]=>1,
 [3, "3rd New edition", ["REV"]]=>1,
 [nil, "Updated edition", nil]=>1,
 [10, "10th Revised edition", ["REV"]]=>1,
 [nil, "Coaching ed", nil]=>1,
 [nil, nil, ["REV"]]=>1,
 [nil, "Facsimile of 1980 ed", nil]=>1,
 [nil, "Revised edition", ["REV"]]=>7,
 [4, "4th ed.", ["REV"]]=>1,
 [nil, "Volume 38 ed.", nil]=>1,
 [nil, "First", nil]=>1,
 [6, "6th Revised edition", ["REV"]]=>3,
 [nil, "New ed of 2 Revised ed", ["REV"]]=>2,
 [8, "8th Revised edition", ["REV"]]=>2,
 [nil, "Revised ed.", ["REV"]]=>1,
 [nil, "New ed of 3 Revised ed", ["REV"]]=>1,
 [12, "12th Revised edition", ["REV"]]=>1,
 [5, "5th ed.", ["REV"]]=>1,
 [2, "2nd Revised ed.", ["REV"]]=>1,
 [18, "18th Revised edition", ["REV"]]=>1,
 [9, "9th Revised edition", ["REV"]]=>1,
 [17, "17th ed.", ["REV"]]=>1,
 [2, "2nd edition", ["REV"]]=>1}

4: Capitalisation in Title and Subtitle Fields

client.books.where.not(subtitle: nil).select do |book|
  subtitle        = book.subtitle
  initial_letters = subtitle.split(" ").map(&:first)
  lowercase       = initial_letters.count{|l| l.in? ("a".."z")}
  total           = initial_letters.count
  lowercase.to_f/total > 0.65
end.map(&:subtitle)

client.books.select do |book|
  title           = book.title
  initial_letters = title.split(" ").map(&:first)
  lowercase       = initial_letters.count{|l| l.in? ("a".."z")}
  total           = initial_letters.count
  lowercase.to_f/total > 0.65
end.map(&:title)

5: Apostrophes Replaced by Blank Spaces


client.marketingtexts.where("pull_quote like '% s %'").pluck(:pull_quote)
client.marketingtexts.where("pull_quote like '% s %'").map(&:books).map(&:first).map(&:isbn)

pp _

6: Unusual Characters in Review Text

client.marketingtexts.where("pull_quote like '%Â%'").pluck(:pull_quote)

client.marketingtexts.where("pull_quote like '%Â%'").map(&:books).map(&:first).map(&:isbn)
client.marketingtexts.where("pull_quote like '%Â%'").pluck(:pull_quote)[2]




