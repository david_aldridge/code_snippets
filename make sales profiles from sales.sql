cte_sales as (
  select
    book_id,
    sale_month,
    sum(sale_quantity) sale_quantity,
    sum(sale_value   ) sale_value,
    round(1000* sum(sale_quantity)/sum(sum(sale_quantity)) over (partition by book_id)) quantity_factor,
    round(1000* sum(sale_value   )/sum(sum(sale_value   )) over (partition by book_id)) value_factor
  from
    (select b.id book_id,
            b.client_id,
            b.format_detail_id,
              EXTRACT(year  FROM age(date_trunc('month',s.invoice_date),date_trunc('month',b.pub_date)))*12
            + EXTRACT(month FROM age(date_trunc('month',s.invoice_date),date_trunc('month',b.pub_date))) +1 sale_month,
            sale_quantity,
            sale_value
     from   sales   s
     join   books   b on b.id = s.book_id
     where  b.pub_date     is not null and
            s.sale_value   > 0         and
            s.invoice_date < date_trunc('month',b.pub_date) + interval '61' month) sales
            where book_id = 3475
  group by
    book_id,
    sale_month


    3475 |          0 |            40 |        155 |              26 |           26
    3475 |          2 |           730 |       2787 |             472 |          475
    3475 |          3 |           125 |        479 |              81 |           82
    3475 |          5 |            82 |        317 |              53 |           54
    3475 |          7 |            94 |        372 |              61 |           63
    3475 |          8 |            32 |        131 |              21 |           22
    3475 |          9 |            46 |        177 |              30 |           30
    3475 |         10 |            42 |        164 |              27 |           28
    3475 |         12 |            88 |     330.54 |              57 |           56
    3475 |         13 |            18 |      69.13 |              12 |           12
    3475 |         16 |            10 |         36 |               6 |            6
    3475 |         21 |             4 |         14 |               3 |            2
    3475 |         22 |             2 |          7 |               1 |            1
    3475 |         23 |             4 |         12 |               3 |            2
    3475 |         24 |             6 |         21 |               4 |            4
    3475 |         25 |             6 |         22 |               4 |            4
    3475 |         26 |            12 |         44 |               8 |            7
    3475 |         27 |            20 |      73.58 |              13 |           13
    3475 |         28 |             3 |        9.6 |               2 |            2
    3475 |         29 |             6 |         20 |               4 |            3
    3475 |         30 |             3 |       10.8 |               2 |            2
    3475 |         31 |             3 |       10.8 |               2 |            2
    3475 |         32 |             4 |       14.4 |               3 |            2
    3475 |         33 |             3 |       10.8 |               2 |            2
    3475 |         34 |             4 |       14.4 |               3 |            2
    3475 |         35 |             7 |       25.2 |               5 |            4
    3475 |         36 |             3 |       10.8 |               2 |            2
    3475 |         37 |             7 |       25.2 |               5 |            4
    3475 |         38 |            11 |       39.6 |               7 |            7
    3475 |         39 |            10 |         36 |               6 |            6
    3475 |         40 |             4 |       14.4 |               3 |            2
    3475 |         41 |             3 |       10.8 |               2 |            2
    3475 |         42 |            14 |       50.4 |               9 |            9
    3475 |         43 |             7 |       25.2 |               5 |            4
    3475 |         44 |             5 |         18 |               3 |            3
    3475 |         45 |             8 |       28.8 |               5 |            5
    3475 |         46 |             5 |         18 |               3 |            3
    3475 |         47 |             8 |       28.8 |               5 |            5
    3475 |         48 |            23 |       82.8 |              15 |           14
    3475 |         49 |            27 |       97.2 |              17 |           17
    3475 |         50 |            16 |       57.6 |              10 |           10
    
select * from crosstab(
$$with
  cte_book_months as (
    select
      id book_id,
      generate_series(0,36) sale_month
    from
      books
    where
      client_id = 1 and
      pub_date between date '2010-01-01' and date '2012-01-01'
  ),
cte_sales as (
  select
    s.book_id,
      EXTRACT(year  FROM age(date_trunc('month',s.invoice_date),min(s.invoice_date) over (partition by s.book_id)))*12
    + EXTRACT(month FROM age(date_trunc('month',s.invoice_date),min(s.invoice_date) over (partition by s.book_id))) sale_month,
    sale_quantity,
    sale_value
  from
    sales s
  where sale_value > 0 and
        s.book_id in (select distinct book_id from cte_book_months)),
  cte_sum_sales as (
    select
      book_id,
      sale_month,
      sum(sale_quantity) sale_quantity
    from
      cte_sales
    group by
      book_id,
      sale_month),
  cte_all_sales as (
    select
      b.book_id,
      b.sale_month,
      coalesce(s.sale_quantity,0) sale_quantity,
      sum(s.sale_quantity) over (partition by b.book_id) total_sales
    from
      cte_book_months b left join cte_sum_sales s on s.book_id = b.book_id and s.sale_month = b.sale_month),
cte_profiles as (
  select
    b.id,
    b.title,
    f.code format,
    substr(w.main_bic_code,1,1) bic,
    extract(day from b.pub_date),
    s.sale_month,
    sum(sale_quantity),
    round(sum(sale_quantity)::numeric / total_sales::numeric,3)*100 profile
  from
    cte_all_sales s join books b on b.id = s.book_id left join formats f on b.format_detail_id = f.id join works w on b.work_id = w.id
  where
    (f.code = 'BC' and total_sales > 500) or
    coalesce(f.code,'X') != 'BC'
  group by
    s.total_sales,
    b.id,
    f.code,
    s.sale_month,
    substr(w.main_bic_code,1,1))
select
  'Format "'||format||'", Genre "'||bic||'"'::varchar description,
  sale_month,
  round(avg(profile)) profile
from
  cte_profiles
group by 
  format,
  bic,
  sale_month
order by description,sale_month$$) as (
 description     text,
 month_01_factor numeric,
 month_02_factor numeric,
 month_03_factor numeric,
 month_04_factor numeric,
 month_05_factor numeric,
 month_06_factor numeric,
 month_07_factor numeric,
 month_08_factor numeric,
 month_09_factor numeric,
 month_10_factor numeric,
 month_11_factor numeric,
 month_12_factor numeric,
 month_13_factor numeric,
 month_14_factor numeric,
 month_15_factor numeric,
 month_16_factor numeric,
 month_17_factor numeric,
 month_18_factor numeric,
 month_19_factor numeric,
 month_20_factor numeric,
 month_21_factor numeric,
 month_22_factor numeric,
 month_23_factor numeric,
 month_24_factor numeric,
 month_25_factor numeric,
 month_26_factor numeric,
 month_27_factor numeric,
 month_28_factor numeric,
 month_29_factor numeric,
 month_30_factor numeric,
 month_31_factor numeric,
 month_32_factor numeric,
 month_33_factor numeric,
 month_34_factor numeric,
 month_35_factor numeric,
 month_36_factor numeric)
