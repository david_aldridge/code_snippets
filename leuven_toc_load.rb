connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1-Oecf-T1yBN-EuT2Vea6w_i9zPfqpst2tN4rOiA6aaU")
metadata_sheet = sheet.worksheets.first
metadata       = metadata_sheet.list
client         = Client.is_leuven

metadata.each do |row|
  LoadLeuvenRow.new(row, client).toc_call
end

class LoadLeuvenRow
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def toc_call
    return unless table_of_content_text
    return unless work
    return if tocs.any?
    text = tocs.create(
      pull_quote:    table_of_content_text
    )
    text.books = text.work.books
  end

  def contrib_bio_call
    return unless contributor_bio_texts.any?
    return unless contributors.any?
    return unless contributor_bio_texts.size == contributors.size
    contributors.each_with_index do |contact, idx|
      next unless contact
      next if contact.biographical_note
      bio = contributor_bio_texts[idx]
      next unless bio
      contact.update_attributes(biographical_note: bio)
    end
  end

  def editor_bio_call
    return unless editor_bio_texts.any?
    return unless editors.any?
    return unless editor_bio_texts.size == editors.size
    editors.each_with_index do |contact, idx|
      next unless contact
      next if contact.biographical_note
      bio = editor_bio_texts[idx]
      next unless bio
      contact.update_attributes(biographical_note: bio)
    end
  end

  def review_call
    return unless review_texts
    return unless work
    review_texts.each do |text|
      new_review = reviews.create(
        pull_quote:    text
      )
      new_review.books = new_review.work.books
    end
  end

  private

  def isbn
    row["SKU"].squish
  end

  def _bios
    work.marketingtexts.where(
      text_type: "12",
      legacy_code: "13",
      main_type:   Marketingtext::TYPE_FOR_BIO
    )
  end

  def reviews
    work.marketingtexts.where(
      text_type: "06",
      legacy_code: "08",
      main_type:   Marketingtext::TYPE_FOR_REVIEW_QUOTE
    )
  end

  def work
    @work ||= client.works.joins(:books).find_by(books: {isbn: isbn})
  end

  def table_of_content_text
    row["TABLE OF CONTENT"].squish
  end

  def contributor_name_texts
    row["AUTHORS"].split(/---{3,}/).map(&:squish).map(&:presence)
  end

  def editor_name_texts
    row["EDITORS"].split(/---{3,}/).map(&:squish).map(&:presence)
  end

  def contributor_bio_texts
    @contributor_bio_texts ||=
      row["AUTHORS BIO"].
        split(/---{3,}/).
        map(&:squish).
        map{|t| t.gsub(/^<p><\/p>$/,"") }.
        map(&:presence)
  end

  def editor_bio_texts
    @editor_bio_texts ||=
      row["EDITORS BIO"].
        split(/---{3,}/).
        map(&:squish).
        map{|t| t.gsub(/^<p><\/p>$/,"") }.
        map(&:presence)
  end

  def contributors
    contributor_name_texts.map do |person_name_inverted|
      client.contacts.find_by(person_name_inverted: person_name_inverted)
    end
  end

  def editors
    editor_name_texts.map do |person_name_inverted|
      client.contacts.find_by(person_name_inverted: person_name_inverted)
    end
  end

  def review_texts
      row["REVIEWS"].
        split(/---{3,}/).
        map(&:squish).
        map{|t| t.gsub(/^<p><\/p>$/,"") }.
        map(&:presence).
        compact
  end

  attr_reader :row, :client
end

metadata.each do |row|
  LoadLeuvenRow.new(row, client).contrib_bio_call
end

metadata.each do |row|
  LoadLeuvenRow.new(row, client).editor_bio_call
end

metadata.each do |row|
  LoadLeuvenRow.new(row, client).review_call
end
