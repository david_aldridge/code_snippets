User.not_dormant.joins(Client).merge(Client.active).group(:client)



users = User.not_dormant.joins(:client).merge(Client.active).group(:client).count.transform_keys(&:to_s)

books = Book.forthcoming_or_active.joins(:client).merge(Client.active).group(:client).having("count(*) > 0").count.transform_keys(&:to_s)
books = Work.joins(:client).merge(Client.active).group(:client).having("count(*) > 0").count.transform_keys(&:to_s)

books.keys.each_with_object({}) do |client, h|
  next if books[client].nil? || users[client].nil?
  h[client] = [
    users.fetch(client),
    books.fetch(client),
    (books.fetch(client)/users.fetch(client).to_f).to_i,
    (users.fetch(client)/books.fetch(client).to_f)*55.0*12 ]
end.sort_by {|client, (users, books, books_per_user, cost_per_product)|cost_per_product }
pp _ ; 0
