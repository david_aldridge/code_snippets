client = Client.is_bds
pp client.contact_digital_asset_transfer_templates.map{|cdatt| [cdatt.id, cdatt.contact.to_s]}


[23].each do |cdatt_id|
  p = DigitalAssetPush.new( client,
                        client.contact_digital_asset_transfer_templates.find(32),
                        :pub_dates_to => Date.today + 10.years,
                        :incremental => false)


  p.create_batch
  p.get_content_items
  p.build_content_file
end

  p = DigitalAssetPush.new( client,
                        client.contact_digital_asset_transfer_templates.find(32),
                        :incremental => false)



[[18, "Bibliographic Data Services Limited"], daily
 [21, "iGroup"], monthly
 [36, "PLS"],
 [25, "British Library (Legal Deposit)"],
 [15, "Copyright Clearance Center"],
 [17, "Bowker"],
 [22, "JSTOR"],
 [14, "Nielsen BookData"],
 [35, "CNPIEC"],
 [20, "Turpin Distribution"],
 [38, "Cambridge CORE"],
 [40, "AcademicPub"],
 [50, "Prospero’s Books Budapest Ltd"]]



company = CreateCompanyService.new(client: client, corporate_name: "Nielsen Bookdata").call.value
SetContactTypesService.new(contact: company, contact_type_names: ["Data aggregator"]).call

company = CreateCompanyService.new(client: client, corporate_name: "Bibliographic Data Systems", corporate_acronym: 'BDS').call.value
SetContactTypesService.new(contact: company, contact_type_names: ["Data aggregator"]).call

company = CreateCompanyService.new(client: client, corporate_name: "Bowker").call.value
SetContactTypesService.new(contact: company, contact_type_names: ["Data aggregator"]).call

company = CreateCompanyService.new(client: client, corporate_name: "Amazon").call.value
SetContactTypesService.new(contact: company, contact_type_names: ["Data aggregator", "Online retailer"]).call




reload!
CreateFTPTemplateService.new(contact: client.contacts.find_by(corporate_name: "Nielsen Bookdata")).call
CreateFTPTemplateService.new(contact: client.contacts.find_by(corporate_name: "Bibliographic Data Systems")).call
CreateFTPTemplateService.new(contact: client.contacts.find_by(corporate_name: "Bowker")).call
CreateFTPTemplateService.new(contact: client.contacts.find_by(corporate_name: "Amazon")).call

 name: "BDS FTP", login_user: "bibliocloud", login_password: "3R8Q0D8c", login_acct: nil, connect_host: "ftp.bdsweb.co.uk", ftps_mode: 0



da = DigitalAsset.find(916)
@doc = Nokogiri::XML(open(da.file.url), nil) do |config|
  config.default_xml.noblanks
end ; 0

publisher_prefixes = @doc.xpath('//Product').map do |product_node|
  publisher = product_node.at_xpath('Publisher/PublisherName').try(:text)
  isbn = product_node.at_xpath('ProductIdentifier[ProductIDType=15]/IDValue').try(:text)
  isbn_prefix = Lisbn.new(isbn).isbn_with_dash.split("-")[0..2].join("-")
  [publisher, isbn_prefix]
end.sort
pp publisher_prefixes.each_with_object(Hash.new(0)){|i,h| h[i] += 1}


@doc.xpath('//CurrencyCode').map(&:text).each_with_object(Hash.new(0)){|i,h| h[i] += 1}
@doc.xpath('//AvailabilityCode').map(&:text).each_with_object(Hash.new(0)){|i,h| h[i] += 1}
@doc.xpath('//PublishingStatus').map(&:text).each_with_object(Hash.new(0)){|i,h| h[i] += 1}
@doc.xpath('//ProductForm').map(&:text).each_with_object(Hash.new(0)){|i,h| h[i] += 1}
