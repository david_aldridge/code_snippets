pp Client.is_zed.books.is_print.where.not(:product_height_mm => nil).
                             where.not(:page_trim_height_mm => nil).
                             where.not(:product_width_mm => nil).
                             where.not(:page_trim_width_mm => nil).
                             each_with_object(Hash.new(0)){|b,h| h[[b.product_form, (b.product_height_mm - b.page_trim_height_mm).to_f, (b.product_width_mm - b.page_trim_width_mm).to_f]] += 1}.sort


Rails.logger.level=1
PaperTrail.enabled = false

Client.is_zed.books.is_print.each do |book|
  if book.product_form.eql? 'BB'
    book.product_height_mm ||= book.page_trim_height_mm + 6.0  if book.page_trim_height_mm
    book.product_height_in ||= book.page_trim_height_in + 0.24 if book.page_trim_height_in
    book.product_width_mm  ||= book.page_trim_width_mm  + 4.0  if book.page_trim_width_mm
    book.product_width_in  ||= book.page_trim_width_in  + 0.2  if book.page_trim_width_in
  else
    book.product_height_mm ||= book.page_trim_height_mm
    book.product_height_in ||= book.page_trim_height_in
    book.product_width_mm  ||= book.page_trim_width_mm
    book.product_width_in  ||= book.page_trim_width_in
  end
  book.save if book.changed?
end ; 0
