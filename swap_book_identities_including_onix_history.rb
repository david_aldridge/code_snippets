Client.is_zedbooks.book.find_by(:isbn => '9781783605958')

client = Client.is_zed

book_pairs = [[69989, 82837],
              [69990, 82836],
              [69987, 82840],
              [69988, 82838],
              [69986, 82839]]

ActiveRecord::Base.transaction do
  book_pairs.each do |book_pair|
    from_book = client.books.find_by(id: book_pair[0])
    to_book   = client.books.find_by(id: book_pair[1])
    raise 'Cannot get books' unless from_book && to_book
    ExportBatchItem.where(export_batchable_type: 'Book', export_batchable_id: from_book.id).update_all(export_batchable_id: to_book.id)
    to_book.isbn, from_book.isbn = from_book.isbn, 'placeholder'
    from_book.save!
    to_book.save!
  end
  #raise ActiveRecord::Rollback
end


ActiveRecord::Base.transaction do
  book_pairs.each do |book_pair|
    DigitalAssetContentItem.where(:digital_asset_item_includable_id => book_pair[0]).update_all(:digital_asset_item_includable_id => book_pair[1])
  end
  #raise ActiveRecord::Rollback
end



Work.find(47086).supportingresource_transfers.each(&:destroy)
Work.find(47086).production_file_transfers.each(&:destroy)

client.contact_digital_asset_transfer_includes.where(:transfer_includeable_id => )
Work.find(47086).contract.destroy

! stock availabilities
