create table onix_code_lists_bkp as select * from onix_code_lists;
SELECT 163
create table onix_codes_bkp as select * from onix_codes;
SELECT 4292

OnixCodeListUpdate.new.update("https://www.editeur.org/files/ONIX%20for%20books%20-%20code%20lists/ONIX_BookProduct_Codelists_Issue_48.xml")




select
  l1.list_number,
  l1.list_description,
  l1.list_issue_number,
  l2.list_description,
  l2.list_issue_number
from
  onix_code_lists l1 full outer join onix_code_lists_bkp l2 on l1.list_number = l2.list_number
where
  l2.list_number is null
  or l1.list_description != l2.list_description
  ;



select
  l1.list_number,
  l1.value,
  l1.description,
  l2.description,
  l2.notes,
  l2.notes
from
  onix_codes l1 full outer join
  onix_codes_bkp l2 on l1.list_number = l2.list_number and l1.value = l2.value
where
  l2.list_number is null or
  l2.value is null or
  l1.description != l2.description or
  l1.notes != l2.notes
order by
1,2
  ;

select value,
 issue_number,
 description,
 notes
 from onix_codes
 where list_number = 21
 order by 1;


select l.list_number,
value,
 issue_number,
 description,
 notes
 from onix_codes c join onix_code_lists l on l.list_number = c.list_number
 where list_issue_number <= 36
 and issue_number > 36
 and notes not like '%ONIX 3.0 only'
 ;



file_path = Rails.root.join("config", "onix", "codes.yml")

    File.open(file_path, "w") do |file|
      file.write(
        OnixCode.
        order(:list_number, :value).
        group_by(&:list_number).
        transform_values do |v|
          Hash[
            *v.flat_map do |x|
              [
                x.value,
                {
                  description:  x.description,
                  notes:        x.notes,
                  issue_number: x.issue_number
                }
              ]
            end
          ]
        end.to_yaml(line_width: -1)
      )
    end
