Work.no_touching do
  Client.is_liverpool.
  marketingtexts.
  notices.
  where(work_id: 55677).
  includes(work: :books).
  each {|n| n.books = n.work.books}
end
