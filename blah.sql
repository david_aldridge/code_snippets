          with
            cte_book_list as (
              select id book_id
              from   books
              where  work_id = 9996),
            cte_sales as (
              select estimate_id,
                    sum(sale_amt) sale_amt,
                    sum(sale_qty) sale_qty
              from   forecast_estimate_gross_sales
              where  book_id in (select book_id from cte_book_list)
              group by estimate_id
            ),
            cte_returns as (
              select estimate_id,
                    sum(returns_amt) returns_amt,
                    sum(returns_qty) returns_qty
              from   forecast_estimate_net_sales
              where  book_id in (select book_id from cte_book_list)
              group by estimate_id
            ),
            cte_net_sales as (
            select
              Coalesce(s.estimate_id, r.estimate_id) estimate_id,
              Coalesce(sale_amt,      0) sale_amt,
              Coalesce(sale_qty,      0) sale_qty,
              Coalesce(returns_amt,   0) returns_amt,
              Coalesce(returns_qty,   0) returns_qty
              from
                cte_sales   s full join
                cte_returns r on s.estimate_id = r.estimate_id
            ),
            cte_detail as (
              select
                b.id  as book_id,
                ch.id as channel_id,
                ns.estimate_id,
                coalesce(
                    rc_bc.royalty_rate_base_pct,
                    rc_bmc.royalty_rate_base_pct,
                    rc_b.royalty_rate_base_pct,
                    ch.average_royalty_rate
                  ) as royalty_rate,
                  case coalesce(cast(c.royalty_basis as numeric), ch.default_royalty_basis, 1)
                    when 1 then (ns.sale_amt - ns.returns_amt)
                    when 3 then p.price_amount * cu.exchange_rate_to_base * m.quantity
                  end as royaltyable_amount,
                  ns.sale_amt,
                  ns.returns_amt,
                (
                  0.01 *
                  coalesce(
                    rc_bc.royalty_rate_base_pct,
                    rc_bmc.royalty_rate_base_pct,
                    rc_b.royalty_rate_base_pct,
                    ch.average_royalty_rate
                  ) *
                  case coalesce(cast(c.royalty_basis as numeric), ch.default_royalty_basis, 1)
                    when 1 then (ns.sale_amt - ns.returns_amt)
                    when 3 then p.price_amount * cu.exchange_rate_to_base * m.quantity
                  end
                ) as royalty_cost,
                (
                  (0.01 * ch.commissionable_sales_rate) *
                  (0.01 * ch.ave_sales_rep_rate) *
                  (ns.sale_amt - ns.returns_amt)
                ) as sales_commission,
                (
                  (0.01 * (100 - ch.pristine_returns_rate)) *
                  ch.returns_pulp_charge_per_unit *
                  ns.returns_qty
                ) as returns_pulp_charge,
                (
                  (0.01 * ch.ave_distribution_rate) *
                  ns.sale_amt
                ) as distribution_cost
                from
                          works         w
                join      clients       cl  on cl.id          = w.client_id
                join      contracts     c   on c.id           = w.contract_id
                join      books         b   on b.work_id      = w.id
                left join prices        dp  on dp.id          = b.default_price_id
                left join estimates     e   on e.book_id      = b.id
                left join cte_net_sales ns  on ns.estimate_id = e.id
                left join channels      ch  on ch.id          = e.channel_id
                left join book_channels bc  on
                  (
                    bc.channel_id = e.channel_id and
                    bc.book_masterchannel_id in (
                      select id
                      from   book_masterchannels bmc
                      where  bmc.book_id = b.id
                    )
                  )
                left join book_masterchannels bmc on bmc.id = bc.book_masterchannel_id
                left join royalty_specifiers rc_b on
                  (
                    b.id                    = rc_b.royaltyable_id   and
                    rc_b.royaltyable_type   = 'Book'
                  )
                left join royalty_specifiers rc_bmc on
                  (
                    bmc.id                  = rc_bmc.royaltyable_id and
                    rc_bmc.royaltyable_type = 'BookMasterchannel'
                  )
                left join royalty_specifiers rc_bc  on
                  (
                    bc.id                   = rc_bc.royaltyable_id  and
                    rc_bc.royaltyable_type  = 'BookChannel'
                  )
                left join currencies cu on cu.id = e.currency_id
                left join prices     p  on
                  (
                    p.book_id                      = b.id and
                    p.price_type                   in ('01','02') and
                    p.onix_price_type_qualifier_id = '05' and
                    p.currency_code                = coalesce(cu.currencycode, cl.default_currency_code) and
                    not exists (
                      select null
                      from   prices p2
                      where  p2.book_id                      = p.book_id and
                            p2.price_type                   = p.price_type and
                            p2.onix_price_type_qualifier_id = p.onix_price_type_qualifier_id and
                            p2.currency_code                = p.currency_code and
                            p2.id                           < p.id
                    )
                  )
                left join currency_exchange_rates cer1 on
                  (
                    cer1.currency_code_from = p.currency_code and
                    cer1.currency_code_to   = 'EUR' and
                    cer1.as_of_date         = (select max(as_of_date) from currency_exchange_rates)
                  )
                left join currency_exchange_rates cer2 on
                  (
                    cer2.currency_code_to   = cl.default_currency_code and
                    cer2.currency_code_from = 'EUR' and
                    cer2.as_of_date         = cer1.as_of_date
                  )
                left join money  m on
                  (
                    m.moneyable_type       = 'Estimate' and
                    m.moneyable_id         = e.id and
                    m.moneysourceable_type is null
                  )
                where
                  w.id = 9996
              )
              select
                book_id,
                channel_id,
                estimate_id,
                royalty_rate,
                royaltyable_amount,
                sale_amt,
                returns_amt,
                sum(royalty_cost)        as royalty_cost,
                sum(sales_commission)    as sales_commission,
                sum(distribution_cost)   as distribution_cost,
                sum(returns_pulp_charge) as returns_pulp_charge
              from
                cte_detail
              group by
                book_id,
                channel_id,
                estimate_id,
                royalty_rate,
                sale_amt,
                returns_amt,
                royaltyable_amount
              order by
                book_id,
                channel_id,
                estimate_id
