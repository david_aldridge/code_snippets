require "/Users/david/Documents/Bibliocloud/Clients/Rebellion/temp/page_extents.rb"

client = Client.is_rebellion

ActiveRecord::Base.transaction do
  Work.no_touching do
    Rebellion.numbers.each do |isbn, page_count|
      book = client.books.find_by(isbn: isbn)
      next unless book

      book.work.books.each do |book|
        book.pages_arabic = page_count
        if book.is_print?
          book.main_content_page_count = page_count
        else
          book.number_of_pages_in_print_counterpart = page_count
        end
        book.save!
      end
    end
  end
end
