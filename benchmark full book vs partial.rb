

require 'benchmark'
IDS = Book.limit(100).pluck(:id)
def method1
  Book.where(id: IDS).load
end

def method2
  Book.where(id: IDS).select(:id).load
end

def method3
  Book.where(id: IDS).select(:id, :title, :subtitle, :work_id).load
end

n = 10
Benchmark.bm(7) do |x|
  x.report("method 1")   { n.times {method1}}
  x.report("method 2")   { n.times {method2}}
  x.report("method 3")   { n.times {method2}}
end ; ""

require 'memory_profiler'
MemoryProfiler.report do
  method1
end.pretty_print

allocated memory by gem
-----------------------------------
   1237575  activerecord-4.2.10
     20752  activesupport-4.2.10
     11864  arel-6.0.4
       272  other
        40  activemodel-4.2.10

allocated memory by class
-----------------------------------
    778344  Hash
    225000  Array
    208815  String
     17040  Proc
     14400  Book
      8000  ActiveRecord::LazyAttributeHash
      7200  ActiveSupport::Callbacks::Filters::Environment
      4000  ActiveRecord::AttributeSet
      4000  Arel::Nodes::Casted
      1680  MatchData

MemoryProfiler.report do
  method2
end.pretty_print

allocated memory by gem
-----------------------------------
    112640  activerecord-4.2.10
     20752  activesupport-4.2.10
     11784  arel-6.0.4
       352  other
        80  activemodel-4.2.10

allocated memory by class
-----------------------------------
     57232  Hash
     34952  Array
     14400  Book
     11936  String
      8000  ActiveRecord::LazyAttributeHash
      7200  ActiveSupport::Callbacks::Filters::Environment
      4000  ActiveRecord::AttributeSet
      4000  Arel::Nodes::Casted

MemoryProfiler.report do
  method3
end.pretty_print

allocated memory by gem
-----------------------------------
    134667  activerecord-4.2.10
     20752  activesupport-4.2.10
     12216  arel-6.0.4
       352  other
       200  activemodel-4.2.10

allocated memory by class
-----------------------------------
     57328  Hash
     39976  Array
     27235  String
     14400  Book
      8000  ActiveRecord::LazyAttributeHash
      7200  ActiveSupport::Callbacks::Filters::Environment
      4000  ActiveRecord::AttributeSet
      4000  Arel::Nodes::Casted
      3360  MatchData
       448  ActiveRecord::Relation

books  = Book.where(id: 3384)
checks = Check::Product::MarketingTexts::ShortDescriptionWithinCharacterLimit
require 'memory_profiler'

report = MemoryProfiler.report do
  CheckRunner.call(checks,
    Client.is_what_on_earth.books.for_check_runner)
end ; 0
report.pretty_print

timing = Benchmark.measure do
  CheckRunner.call(checks,
    Client.is_what_on_earth.books.for_check_runner).persist
end


require 'memory_profiler'
report = MemoryProfiler.report do
  Price.new
end

report.pretty_print

allocated memory by file
448680  /Users/david/rails/bibliocloud/app/models/price.rb

6780 objects
141784 retained
1328 retained objects




require 'benchmark/ips'

def noop(arg)
end

Benchmark.ips do |x|
  x.report("normal") { noop("foo") }
  x.report("frozen") { noop("foo".freeze)  }
end

Benchmark.ips do |x|
  x.report("normal") { 100.times {1000.times { |x| "x#{x}"}}}
  x.report("normal") { 100.times {1000.times { |x| "x#{x}".freeze}}}
  x.report("normal") { 1000.times {100.times { |x| "x#{x}"}}}
  x.report("normal") { 1000.times {100.times { |x| "x#{x}".freeze}}}
  x.report("normal") { 10000.times {10.times { |x| "x#{x}"}}}
  x.report("normal") { 10000.times {10.times { |x| "x#{x}".freeze}}}
end


require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("normal") { Book.find(3384).to_s }
end






require 'memory_profiler'

ary = ["a".freeze, "b".freeze, "c".freeze, "d".freeze, "e".freeze, "f".freeze, "g".freeze, "h".freeze, "i".freeze, "j".freeze]

MemoryProfiler.report do
  10000.times {ary.each { |x| y = x}}
end.pretty_print

MemoryProfiler.report do
  10000.times {ary.each { |x| y = "#{x}".freeze}}
end.pretty_print

MemoryProfiler.report do
  10000.times {ary.each { |x| y = -"#{x}".freeze}}
end.pretty_print


MemoryProfiler.report do
  10000.times {%w(a b c d e f g h i j).each { |x| y = -"#{x}".freeze}}
end.pretty_print


MemoryProfiler.report do
  10000.times {%w(a b c d e f g h i j).each { |x| y = "x#{x}"}}
end.pretty_print

MemoryProfiler.report do
  10000.times {%w(a b c d e f g h i j).each { |x| y = -"x#{x}".freeze}}
end.pretty_print

MemoryProfiler.report do
  10000.times {%w(a b c d e f g h i j).each { |x| y = "x#{x}"; y.freeze}}
end.pretty_print





Total allocated: 9666991 bytes (102062 objects)
Total retained:  605891 bytes (7154 objects)

allocated memory by gem
-----------------------------------
   5804842  activemodel-4.2.10
   1962834  activerecord-4.2.10
   1156467  activesupport-4.2.10
    295120  web-console-3.1.1
    138821  bibliocloud/app
     90328  pg-0.17.1
     57743  newrelic_rpm-4.0.0.332
     55830  bootsnap-1.2.0
     21485  ruby-2.4.4/lib
     16616  thread_safe-0.3.6
     15576  arel-6.0.4
     11785  kaminari-0.16.3
     10304  protected_attributes-1.1.3
      9360  paperclip-4.3.6
      5624  json-1.8.6
      3880  paper_trail-4.1.0
      3568  spring-1.7.1
      2888  rack-mini-profiler-1.0.0
      1496  acts-as-taggable-on-3.5.0
       904  pg_search-1.0.2
       600  yard-0.8.7.6
       400  rails-observers-0.1.2
       240  other
       120  polyamorous-1.3.0
       120  strip_attributes-1.7.1
        40  railties-4.2.10

Total allocated: 9667595 bytes (102054 objects)
Total retained:  606199 bytes (7156 objects)

allocated memory by gem
-----------------------------------
   5804842  activemodel-4.2.10
   1961645  activerecord-4.2.10
   1156547  activesupport-4.2.10
    295120  web-console-3.1.1
    139805  bibliocloud/app
     90905  pg-0.17.1
     57743  newrelic_rpm-4.0.0.332
     55870  bootsnap-1.2.0
     21485  ruby-2.4.4/lib
     16616  thread_safe-0.3.6
     15576  arel-6.0.4
     11785  kaminari-0.16.3
     10304  protected_attributes-1.1.3
      9360  paperclip-4.3.6
      5624  json-1.8.6
      3880  paper_trail-4.1.0
      3568  spring-1.7.1
      3000  rack-mini-profiler-1.0.0
      1496  acts-as-taggable-on-3.5.0
       904  pg_search-1.0.2
       600  yard-0.8.7.6
       400  rails-observers-0.1.2
       240  other
       120  polyamorous-1.3.0
       120  strip_attributes-1.7.1
        40  railties-4.2.10

@book = Book.first
query = @book.prices.gbp.consumer.select(:price_amount).limit(1).to_sql


query = "SELECT  \"prices\".\"price_amount\" FROM \"prices\" WHERE \"prices\".\"book_id\" = $1 AND \"prices\".\"currency_code\" = $2 AND \"prices\".\"onix_price_type_qualifier_id\" = '05' LIMIT 1"
ActiveRecord::Base.connection.raw_connection.async_exec query

ActiveRecord::Base.connection.raw_connection.async_exec(query, ["3384", "GBP"]).values
ActiveRecord::Base.connection.raw_connection.async_exec(query, ["3384", "GBP", 0]).values
ActiveRecord::Base.connection.raw_connection.async_exec(query, ["3384", "GBP", 0]).getvalue(0,0)


module GetPrice
  def self.call(book_id:, currency_code:, price_type:)
    connection.exec_prepared(
      "get_price".freeze,
      [
        book_id,
        currency_code,
        price_type
      ]
    ).getvalue(0,0)

  rescue PG::InvalidSqlStatementName
    puts "error, preparing ..."
    prepare_statement
    puts "retrying ..."
    retry
  end

  def self.prepare_statement
    connection.prepare(
      "get_price".freeze,
      QUERY
      )
  end

  def self.connection
    @connection ||= ActiveRecord::Base.connection.raw_connection
  end

  QUERY = "SELECT  \"prices\".\"price_amount\" FROM \"prices\" WHERE \"prices\".\"book_id\" = $1 AND \"prices\".\"currency_code\" = $2 AND \"prices\".\"onix_price_type_qualifier_id\" = $3 LIMIT 1".freeze
end


GetPrice.call(book_id: 3685, currency_code: "GBP".freeze, price_type: "05".freeze)



book_ids = Price.gbp.consumer.limit(1000).pluck(:book_id).uniq
require 'memory_profiler'
report = MemoryProfiler.report do
  book_ids.each do |book_id|
    GetPrice.call(book_id: book_id, currency_code: "GBP".freeze, price_type: "05".freeze)
  end
end


report.pretty_print





  IDS.first(1000).select do |id|
    conn.async_exec("select pull_quote from marketingtexts where id = #{id}").getvalue(0,0).size&.> 350
  end
end
