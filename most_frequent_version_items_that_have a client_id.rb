PaperTrail::Version.group(:item_type).order("1 desc").limit(50).count.keys.map{|s| s.try(:constantize)}.select{|x| x.column_names.include? "client_id"}.map(&:class_name)


PaperTrail::Version.select(:id, :item_id, :item_type).each{|v| v.update_columns(:client_id => v.item.try(:client_id)) unless v.try(:item).try(:client_id).nil? rescue nil}
