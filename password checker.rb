passwords = %w(111111
123123
123321
1234
12345
123456
1234567
12345678
123456789
1234567890
123qwe
18atcskd2w
1q2w3e
1q2w3e4r
1q2w3e4r5t
1qaz2wsx
3rjs1la7qe
555555
654321
666666
7777777
987654321
abc123
baseball
dragon
football
google
letmein
login
master
monkey
mynoob
passw0rd
password
princess
qwerty
qwertyuiop
solo
starwars
welcome
zxcvbnm
)

passwords.each do | password |
  puts "Checking ... #{password}"
  puts User.all.select{|u| u.valid_password? password}.map(&:email)
end


User.joins(:client).merge(Client.active).order(id: :desc).limit(100).each_with_object([]) do | user, ary |
  password = passwords.detect{|p| user.valid_password? p}
  next unless password
  ary << "#{user.email} of #{user.client} is using \"#{password}\""
end
