__END__

Rails.logger.level=1
sc = Statement.find(51571)
puts sc.sales_amt.to_s
puts sc.advances_due_amt.to_s
puts sc.advances_paid_amt.to_s
puts sc.unpaid_advance_amt.to_s
puts sc.sales_qty.to_s
puts sc.sales_amt.to_s
puts (sc.sales_amt/sc.sales_qty).to_s
puts sc.all_payees_royalties_due_amt.to_s
puts sc.gross_royalties_due_amt.to_s
puts sc.unearned_advance_amt.to_s
puts sc.total_paid_amt.to_s
puts sc.opening_balance_amt.to_s
puts sc.fees_due_by_name.map{|fee_due| fee_due[1].to_f}
puts sc.net_royalty_and_fees_amt
puts sc.deminimus_met?
puts sc.final_statement_amt.to_s
puts sc.carry_forward_amt.to_s
puts sc.payee_amt.to_s
puts sc.statement_alternative_payees.map{|sap| sap.payment}
    money_ary << ["Total royalty"          , number_to_currency(   @statement.net_royalty_and_fees_amt            , precision: 2, unit: @currency_symbol, negative_format: "(%u%n)")] #unless @statement.net_royalty_amt.eql? @statement.gross_royalties_due_amt
    @statement.statement_alternative_payees.each_with_object(Hash.new(0)){|sap, h| h[sap.alternative_payee.to_s] += sap.payment}.each_with_index do |(sap, payment), i|
      money_ary << [sap, number_to_currency(   payment, precision: 2, unit: @currency_symbol, negative_format: "(%u%n)")]
    end
    if @statement.deminimus_met?
      money_ary << ["Total amount payable"   , number_to_currency(   @statement.payment_less_all_alternative_payees_amt, precision: 2, unit: @currency_symbol, negative_format: "(%u%n)")]
    else
      money_ary << ["Royalties carried fwd"  , number_to_currency(   @statement.payment_after_deminimus_amt, precision: 2, unit: @currency_symbol, negative_format: "(%u%n)")]
    end
