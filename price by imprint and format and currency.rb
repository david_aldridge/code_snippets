Price.includes(book: {work: :imprint}).select{|p| p.book.imprint && p.book.product_form}.map{|p| [p.book.imprint&.to_s, p.book.product_form, p.currency_code, p.price_amount.to_f]}.sort.group_by(&:itself).transform_values(&:size)


"BB", "AUD", 182.00
"BB", "CAD", 178.00
"BB", "EUR", 115.00
"BB", "GBP",  99.00
"BB", "USD", 159.00

"BC", "AUD",  ?
"BC", "CAD", 30.00
"BC", "EUR",  ?
"BC", "GBP", 25.00
"BC", "USD", 30.00

"DG", "AUD",   ?
"DG", "CAD", 150.00
"DG", "EUR",   ?
"DG", "GBP",  95.00
"DG", "USD", 150.00










"Morgan & Claypool Publishers", "BC", "AUD", 41.99   8,
"Morgan & Claypool Publishers", "BC", "AUD", 45.0   1,
"Morgan & Claypool Publishers", "BC", "AUD", 47.99   8,
"Morgan & Claypool Publishers", "BC", "AUD", 48.0   13,
"Morgan & Claypool Publishers", "BC", "AUD", 50.0   2,
"Morgan & Claypool Publishers", "BC", "AUD", 53.99   9,
"Morgan & Claypool Publishers", "BC", "AUD", 55.0   3,
"Morgan & Claypool Publishers", "BC", "AUD", 59.99   5,
"Morgan & Claypool Publishers", "BC", "AUD", 65.99   11,
"Morgan & Claypool Publishers", "BC", "AUD", 71.99   1,
"Morgan & Claypool Publishers", "BC", "AUD", 77.99   1,
"Morgan & Claypool Publishers", "BC", "AUD", 89.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 41.99   27,
"Morgan & Claypool Publishers", "BC", "CAD", 45.0   1,
"Morgan & Claypool Publishers", "BC", "CAD", 45.5   1,
"Morgan & Claypool Publishers", "BC", "CAD", 46.99   2,
"Morgan & Claypool Publishers", "BC", "CAD", 47.99   10,
"Morgan & Claypool Publishers", "BC", "CAD", 50.0   2,
"Morgan & Claypool Publishers", "BC", "CAD", 52.99   4,
"Morgan & Claypool Publishers", "BC", "CAD", 53.99   10,
"Morgan & Claypool Publishers", "BC", "CAD", 55.0   3,
"Morgan & Claypool Publishers", "BC", "CAD", 58.5   1,
"Morgan & Claypool Publishers", "BC", "CAD", 58.99   2,
"Morgan & Claypool Publishers", "BC", "CAD", 59.99   7,
"Morgan & Claypool Publishers", "BC", "CAD", 64.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 65.99   11,
"Morgan & Claypool Publishers", "BC", "CAD", 66.99   3,
"Morgan & Claypool Publishers", "BC", "CAD", 71.99   2,
"Morgan & Claypool Publishers", "BC", "CAD", 77.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 78.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 79.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 89.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 91.99   1,
"Morgan & Claypool Publishers", "BC", "CAD", 93.99   1,
"Morgan & Claypool Publishers", "BC", "EUR", 30.99   7,
"Morgan & Claypool Publishers", "BC", "EUR", 34.99   1,
"Morgan & Claypool Publishers", "BC", "EUR", 35.99   9,
"Morgan & Claypool Publishers", "BC", "EUR", 38.0   13,
"Morgan & Claypool Publishers", "BC", "EUR", 39.99   10,
"Morgan & Claypool Publishers", "BC", "EUR", 41.99   3,
"Morgan & Claypool Publishers", "BC", "EUR", 44.99   6,
"Morgan & Claypool Publishers", "BC", "EUR", 48.99   11,
"Morgan & Claypool Publishers", "BC", "EUR", 52.99   1,
"Morgan & Claypool Publishers", "BC", "EUR", 57.99   1,
"Morgan & Claypool Publishers", "BC", "EUR", 65.99   1,
"Morgan & Claypool Publishers", "BC", "GBP", 21.99   7,
"Morgan & Claypool Publishers", "BC", "GBP", 24.99   8,
"Morgan & Claypool Publishers", "BC", "GBP", 25.0   222,
"Morgan & Claypool Publishers", "BC", "GBP", 27.5   1,
"Morgan & Claypool Publishers", "BC", "GBP", 27.99   8,
"Morgan & Claypool Publishers", "BC", "GBP", 29.99   2,
"Morgan & Claypool Publishers", "BC", "GBP", 30.99   6,
"Morgan & Claypool Publishers", "BC", "GBP", 32.5   3,
"Morgan & Claypool Publishers", "BC", "GBP", 33.99   11,
"Morgan & Claypool Publishers", "BC", "GBP", 35.5   13,
"Morgan & Claypool Publishers", "BC", "GBP", 36.99   1,
"Morgan & Claypool Publishers", "BC", "GBP", 39.99   1,
"Morgan & Claypool Publishers", "BC", "GBP", 41.99   1,
"Morgan & Claypool Publishers", "BC", "GBP", 45.99   1,
"Morgan & Claypool Publishers", "BC", "USD", 30.0   219,
"Morgan & Claypool Publishers", "BC", "USD", 35.0   30,
"Morgan & Claypool Publishers", "BC", "USD", 40.0   15,
"Morgan & Claypool Publishers", "BC", "USD", 45.0   13,
"Morgan & Claypool Publishers", "BC", "USD", 50.0   14,
"Morgan & Claypool Publishers", "BC", "USD", 55.0   15,
"Morgan & Claypool Publishers", "BC", "USD", 60.0   3,
"Morgan & Claypool Publishers", "BC", "USD", 65.0   1,
"Morgan & Claypool Publishers", "BC", "USD", 70.0   2,
"Morgan & Claypool Publishers", "BC", "USD", 75.0   1,
"Morgan & Claypool Publishers", "DG", "GBP", 10.99   10,
"Morgan & Claypool Publishers", "DG", "GBP", 11.99   9,
"Morgan & Claypool Publishers", "DG", "GBP", 12.99   2,
"Morgan & Claypool Publishers", "DG", "GBP", 14.99   10,
"Morgan & Claypool Publishers", "DG", "GBP", 15.99   2,
"Morgan & Claypool Publishers", "DG", "GBP", 16.99   1,
"Morgan & Claypool Publishers", "DG", "GBP", 17.99   16,
"Morgan & Claypool Publishers", "DG", "GBP", 18.99   9,
"Morgan & Claypool Publishers", "DG", "GBP", 19.99   6,
"Morgan & Claypool Publishers", "DG", "GBP", 20.99   10,
"Morgan & Claypool Publishers", "DG", "GBP", 21.99   14,
"Morgan & Claypool Publishers", "DG", "GBP", 22.99   3,
"Morgan & Claypool Publishers", "DG", "GBP", 23.99   7,
"Morgan & Claypool Publishers", "DG", "GBP", 24.99   20,
"Morgan & Claypool Publishers", "DG", "GBP", 25.0   12,
"Morgan & Claypool Publishers", "DG", "GBP", 25.99   4,
"Morgan & Claypool Publishers", "DG", "GBP", 26.99   22,
"Morgan & Claypool Publishers", "DG", "GBP", 27.5   2,
"Morgan & Claypool Publishers", "DG", "GBP", 27.99   2,
"Morgan & Claypool Publishers", "DG", "GBP", 29.99   6,
"Morgan & Claypool Publishers", "DG", "GBP", 30.99   4,
"Morgan & Claypool Publishers", "DG", "GBP", 36.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 15.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 16.0   10,
"Morgan & Claypool Publishers", "DG", "USD", 19.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 20.0   11,
"Morgan & Claypool Publishers", "DG", "USD", 20.99   2,
"Morgan & Claypool Publishers", "DG", "USD", 23.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 24.0   13,
"Morgan & Claypool Publishers", "DG", "USD", 24.99   2,
"Morgan & Claypool Publishers", "DG", "USD", 27.99   3,
"Morgan & Claypool Publishers", "DG", "USD", 28.0   27,
"Morgan & Claypool Publishers", "DG", "USD", 28.99   4,
"Morgan & Claypool Publishers", "DG", "USD", 30.0   12,
"Morgan & Claypool Publishers", "DG", "USD", 31.99   7,
"Morgan & Claypool Publishers", "DG", "USD", 32.0   34,
"Morgan & Claypool Publishers", "DG", "USD", 35.0   2,
"Morgan & Claypool Publishers", "DG", "USD", 35.99   3,
"Morgan & Claypool Publishers", "DG", "USD", 36.0   25,
"Morgan & Claypool Publishers", "DG", "USD", 36.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 39.2   3,
"Morgan & Claypool Publishers", "DG", "USD", 39.99   7,
"Morgan & Claypool Publishers", "DG", "USD", 40.0   22,
"Morgan & Claypool Publishers", "DG", "USD", 40.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 43.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 44.0   25,
"Morgan & Claypool Publishers", "DG", "USD", 45.0   4,
"Morgan & Claypool Publishers", "DG", "USD", 47.99   2,
"Morgan & Claypool Publishers", "DG", "USD", 48.0   8,
"Morgan & Claypool Publishers", "DG", "USD", 50.0   6,
"Morgan & Claypool Publishers", "DG", "USD", 52.0   2,
"Morgan & Claypool Publishers", "DG", "USD", 54.99   2,
"Morgan & Claypool Publishers", "DG", "USD", 55.99   1,
"Morgan & Claypool Publishers", "DG", "USD", 56.0   4,
"Morgan & Claypool Publishers", "DG", "USD", 60.0   1}