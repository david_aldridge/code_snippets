{
  "Template": {
    "TemplateName": "OnixTransmissionSuccess",
    "SubjectPart": "Consonance file transmission notification",
    "HtmlPart": "<h3>Notification of transmission</h3><p>  The following files have been sent by Consonance to {{ recipient }} on behalf of {{ client }}:</p><ol>  <% @items.each do |item| %>    <li><%= item %></li>  <% end %></ol><p>  For issues regarding this transfer, or to no longer receive these emails, please contact <a href="mailto:support@consonance.app" subject= "Consonance files from  {{ client }} to {{ recipient }}" target="_top">support@consonance.app</a></p>"
    "TextPart": "Notification of transmission\n============================\nThe following files have been sent by Consonance to {{ recipient }} on behalf of {{ client }}:\n{{#each items}}\n  {{item}}\n{{/each}}\nFor issues regarding this transfer, or to no longer receive these emails, please contact support@consonance.app"
  }
}