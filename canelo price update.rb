USD
2.99 => 3.99
3.99 => 5.99
4.99 => 6.99
5.99 => 7.99
6.99 => 8.99

2.99 => 3.99
3.99 => 4.99
4.99 => 5.99
5.99 => 6.99
6.99 => 7.99
7.99 => 8.99

Rails.logger.level=1

ActiveRecord::Base.transaction do
  prices = Client.is_canelo.prices.joins(:book).merge(Book.ebook).consumer.current

  pp prices.usd.order(:price_amount).group(:price_amount).count.transform_keys(&:to_s)

  prices.usd.where(price_amount: 6.99).update_all(price_amount: 8.99)
  prices.usd.where(price_amount: 5.99).update_all(price_amount: 7.99)
  prices.usd.where(price_amount: 4.99).update_all(price_amount: 6.99)
  prices.usd.where(price_amount: 3.99).update_all(price_amount: 5.99)
  prices.usd.where(price_amount: 2.99).update_all(price_amount: 3.99)

  pp prices.usd.order(:price_amount).group(:price_amount).count.transform_keys(&:to_s)

  pp prices.cad.order(:price_amount).group(:price_amount).count.transform_keys(&:to_s)

  prices.cad.where(price_amount: 7.99).update_all(price_amount: 8.99)
  prices.cad.where(price_amount: 6.99).update_all(price_amount: 7.99)
  prices.cad.where(price_amount: 5.99).update_all(price_amount: 6.99)
  prices.cad.where(price_amount: 4.99).update_all(price_amount: 5.99)
  prices.cad.where(price_amount: 3.99).update_all(price_amount: 4.99)
  prices.cad.where(price_amount: 2.99).update_all(price_amount: 3.99)

  pp prices.cad.order(:price_amount).group(:price_amount).count.transform_keys(&:to_s)

  # raise ActiveRecord::Rollback
end
