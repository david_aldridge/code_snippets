#In the Model

include ONIXValueObjects


"BB: Hardback"

"YU: Yugoslavia"

"YU: Yugoslavia (Deprecated)"


-For a single value attribute-
  define_onix_value :epub_type_code, ONIXValue::EpubType
  define_onix_value :country_code,   ONIXValue::Country
  define_onix_value :address_line_4, ONIXValue::Country

-For a set of values-
  define_onix_value :epub_type_codes, ONIXValue::EpubTypeSet

##Apply validation

validate :epub_type_codes, valid: true
validate :epub_type_codes, valid: true, allow_nil: true


#View

##In forms

-replace-
  = f.input :epub_type_codes   ,
            :as => :select,
            :collection => ONIXValue::EpubType.dropdown,
            :multiple => true,
            :label => (link_to "EPub type codes" ,
                        onix_code_list_path(10), :target => "_blank").html_safe

-with-
  - onix_value_form_input :epub_type_codes, f
-or-
  - onix_value_form_input :epub_type_code, f

= f.onix_value :epub_type_codes


##Static view

Replace
  - hovery_text(OnixCode.lookup(:countries, country_code).try(:value_tooltip))
-with-
  - hovery_text country.value_tooltip



  - rep.countries.each do |country|
    - hovery_text(OnixCode.lookup(:countries, country).try(:value_tooltip))
-with-
  - rep.countries.each { |code| hovery_text code.value_tooltip }
-or-
  - rep.epub_type_codes.each { |code| hovery_text code.value_tooltip }
