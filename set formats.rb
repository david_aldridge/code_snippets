probs = []
Book.where.not(:proprietary_format_description_id => nil).each do |b|
  keys_to_copy = ["product_form"                        ,
                  "product_form_detail"                 ,
                  "epub_type_code"                      ,
                  "epub_type_version"                   ,
                  "epub_type_description"               ,
                  "epub_format_code"                    ,
                  "epub_format_version"                 ,
                  "epub_format_description"             ,
                  "epub_source_code"                    ,
                  "epub_source_version"                 ,
                  "epub_source_description"             ,
                  "epub_type_note"                      ,
                  "feature_colour_of_cover"             ,
                  "feature_colour_of_page_edge"         ,
                  "feature_text_font"                   ,
                  "feature_special_cover_material"      ,
                  "feature_dvd_region"                  ,
                  "feature_epub_accessibility"          ,
                  "feature_cpsia_choking_hazard_warning",
                  "feature_eu_toy_safety_hazard_warning",
                  "feature_paper_environmental_statement",
                  "product_packaging"                    ,
                  "product_form_description"             ,
                  "number_of_pieces"                     ,
                  "trade_category"                       ,
                  "product_content_type"    ]

  b.assign_attributes(b.proprietary_format_description.attributes.select{|key,_| keys_to_copy.include? key})
  probs << [b.client.to_s,
            b.id         ,
            b.isbn       ,
            b.product_form_was         , b.product_form       ,
            b.product_form_detail_was  , b.product_form_detail,
            b.epub_type_code_was       , b.epub_type_code     ,
            b.product_content_type_was , b.product_content_type
            ] if b.changed?
end
probs.each do |p|
  puts p
end

