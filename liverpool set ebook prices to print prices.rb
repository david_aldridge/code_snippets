  Work.no_touching do
    ActiveRecord::Base.transaction do
      Client.is_liverpool.books.ebook.has_gbp_price.includes(:work).each do |ebook|
        next if ebook.current_gbp_inctax_consumer_price.nil?

        gbp_price =
          ebook.work.paperbacks.joins(:prices).merge(Price.current.gbp.inctax.consumer.rrp).distinct.detect do |book|
            book.current_gbp_inctax_consumer_price && book.current_gbp_inctax_consumer_price > 0
          end&.current_gbp_inctax_consumer_price ||
          ebook.work.hardbacks.joins(:prices).merge(Price.current.gbp.inctax.consumer.rrp).distinct.detect do |book|
            book.current_gbp_inctax_consumer_price && book.current_gbp_inctax_consumer_price > 0
          end&.current_gbp_inctax_consumer_price

        next unless gbp_price

        ebook.current_gbp_inctax_consumer_price = gbp_price
        ebook.current_gbp_inctax_library_price  = (gbp_price * 2.25).round(2)

        next unless ebook.current_usd_exctax_consumer_price

        usd_price =
          ebook.work.paperbacks.joins(:prices).merge(Price.current.usd.exctax.consumer.rrp).distinct.detect do |book|
            book.current_usd_exctax_consumer_price && book.current_usd_exctax_consumer_price > 0
          end&.current_usd_exctax_consumer_price ||
          ebook.work.hardbacks.joins(:prices).merge(Price.current.usd.exctax.consumer.rrp).distinct.detect do |book|
            book.current_usd_exctax_consumer_price && book.current_usd_exctax_consumer_price > 0
          end&.current_usd_exctax_consumer_price

        next unless usd_price

        ebook.current_usd_exctax_consumer_price = usd_price

        next unless ebook.current_usd_exctax_library_price

        ebook.current_usd_exctax_library_price  = (usd_price * 2.25).round(2)
      end ; 0
    end
  end





isbns = %w[
  9781786946775
  9781909821750
  9781781386248
]
