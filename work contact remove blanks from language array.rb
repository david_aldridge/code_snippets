ActiveRecord::Base.transaction do
  Work.no_touching do
    Workcontact.
      select{|wc| wc.languages_from.any?(&:blank?)}.each do |wc|
        wc.update(languages_from: wc.languages_from.reject(&:blank?))
      end
    end
  end
end
