client = Client.is_purdue

[
[75842, 75672]
].each do |from_id, to_id|
  from_contact = client.contacts.find_by(id: from_id)
  to_contact   = client.contacts.find_by(id: to_id)
  from_contact.workcontacts.each do |work_contact|
    work_contact.update_attributes(contact_id: to_contact.id)
  end
  from_contact.update_attributes(keynames: "#{from_contact.keynames} DELETE ME" )
end


75842,75672
76055,75886