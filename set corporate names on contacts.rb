names = [
[50339,	"Lawrence Hill & Company"],
[50347,	"International Labour Office [ILO]"],
[50356,	"Editions Flammarion"],
[50357,	"Les Editions Stock"],
[50358,	"Vikas Publishing House Publishing"],
[50368,	"Journal of African Marxists"],
[50378,	"Africa Groups of Sweden"],
[50396,	"La Librairie Francois Maspero"],
[50403,	"The Resource Center"],
[50414,	"Marram Books"],
[50420,	"Tamil Information Centre"],
[50424,	"Les Editions Jeune Afrique"],
[50426,	"Universidade Eduardo Mondlane"],
[50439,	"Organisation of Angolan Women"],
[50458,	"Committee Against Repression & for Democratic Rights"],
[50480,	"Permanent Peoples Tribune"],
[50489,	"WIN (Women in Nigeria)"],
[50528,	"Kali for Women Women"],
[50536,	"World Commission on Environment and Development"],
[50560,	"Éditions Mercure de France"],
[50562,	"The Daisan Shokan Co Ltd"],
[50566,	"David Philip Publishers"],
[50567,	"Iwanami Shoten Publishers"],
[50577,	"Unistad Uitgaven VZW"],
[50578,	"David Robie Publishing"],
[50593,	"Rutgers University Press (Contract a/c)"],
[50596,	"The Namibia Communications Centre"],
[50601,	"World Council of Churches"],
[50616,	"Editions Jean-Claude Lattes"],
[50626,	"Popular History Trust"],
[50629,	"Service Liaison Non-Governmental UN JUNIC (NGLS)"],
[52404,	"The Group of Green Economists"],
[52432,	"Sage Publications India Pvt Ltd Pvt"],
[52456,	"World University Service"],
[52496,	"International Commission on Peace & Food"],
[52777,	"Arab Women's Solidarity Association"],
[54202,	"UN International Research and Training Institute for the Advancement of Women (UN-INSTRAW)"],
[54203,	"Pesticide Action Network UK"],
]

client = Client.is_zed

ActiveRecord::Base.transaction do
  names.each do |id, corporate_name|
    contact = client.contacts.find_by(id: id)
    raise "Cannot find contact #{id}" unless contact
    contact.update_attributes(
      contributor_type:            "Company",
      corporate_name:              corporate_name,
      person_name_inverted:        nil,
      titles_before_names:         nil,
      names_before_key:            nil,
      keyname_prefix:              nil,
      keynames:                    nil,
      suffix_after_keyname:        nil,
      qualification_after_keyname: nil,
      titles_after_names:          nil
    )
  end
  #raise ActiveRecord::Rollback
end
