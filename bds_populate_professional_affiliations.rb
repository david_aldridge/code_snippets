client = Client.is_bds
client.contacts.people.each do |person|
  next unless organisation_name = person.addresses.take.try(:organisation_name)
  next unless organisation      = client.contacts.where("upper(corporate_name) = ? or upper(corporate_acronym) = ?", organisation_name.upcase, organisation_name.upcase).take
  person.professional_affiliations << organisation
end ; 0
