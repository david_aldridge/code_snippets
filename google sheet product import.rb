class GoogleSheetProductImport

  WORK_COLUMNS=["Publisher"            ,
                "Imprint"              ,
                "Title"                ,
                "Subtitle"             ,
                "Vol No"               ,
                "Series Title"         ,
                "Creator 1 ID"         ,
                "Creator One Type"     ,
                "Creator 2 ID"         ,
                "Creator Two Type"     ,
                "Creator 3 ID"         ,
                "Creator Three Type"   ,
                "Contributor statement",
                "Copyright Date"       ,
                "Short Desc"           ,
                "Long Desc"            ,
                "Contents"             ,
                "Reviews"              ,
                "BISAC one"            ,
                "BISAC two"            ,
                "BISAC three"          ,
                "BIC one"              ,
                "BIC two"              ,
                "BIC three"            ,
                "Language"             ,
                "Pages"                ,
                "Geog Rights"          ,
                "Copyright Holder"     ,
                "Keywords"             ]

  def initialize(webname)
    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key("1lSDtciO_DEmF3kE7VkFgOru_kcN1O3_JauHj7j8n5D0")
    @author_sheet  = @sheet.worksheet_by_title("Authors")
    @authors       = @author_sheet.list
    @product_sheet = @sheet.worksheet_by_title("Products")
    @products      = @product_sheet.list
    @client        = Client.find_by(:webname => webname)
  end

  def authors
    @authors
  end

  def products
    @products
  end

  def import_authors
    @client.contacts.destroy_all
    @authors.each do |author|
      new_contact = @client.contacts.create!(:person_name       => author["Name"],
                                             :biographical_note => author["contrib_statement"])
      author["Bibliocloud ID"] = new_contact.id
    end
    @author_sheet.save
    @products.each do |product|
      [ product["Creator One"].presence,
        product["Creator Two"].presence,
        product["Creator Three"].presence].compact.each do |name|
          Contact.where(:client_id   => @client.id, :person_name => name).first_or_create!
      end
    end
  end

  def import_publishernames
    @client.publishernames = self.publishernames
  end

  def import_imprints
    @client.imprints       = self.imprints
  end

  def import_seriesnames
    @client.seriesnames    = self.seriesnames + self.sets
  end

  def import_contracts
    @client.contracts      = self.contracts
  end

  def import_works
    @client.works          = self.works
  end

  def import_books
    @client.books          = self.books
  end

  def seriesnames
    get_uniq_pair("Series Title","Series Desc").
      select{|x| x[0].presence}.
      map{|s| Seriesname.new(:title_without_prefix => s[0],
                               :short_description    => s[1].length >  350 ? nil : s[1],
                               :long_description     => s[1].length <= 350 ? nil : s[1],
                               :collection_type      => "Series")}
  end

  def publishernames
    get_uniq("Publisher").map{|p| Publishername.new(:publisher_name => p)}
  end

  def imprints
    get_uniq("Imprint").map{|i| Imprint.new(:value => i)}
  end

  def sets
    get_uniq_trip("Title","Subtitle", "Vol No").
      select{|x| x[2].presence}.
      map{|x| [x[0], x[1]]}.
      uniq.
      map{|s| Seriesname.new(:title_without_prefix => s[0],
                             :subtitle             => s[1],
                             :collection_type      => "Sets")}
  end

  def contracts
    get_uniq_pair("Title", "Vol No").map{ |s| Contract.new(:contract_name => [s[0].presence, s[1].presence].compact.join(": ") ) }
  end

  def works
    @products.reject{|a| a["ISBN"] == "9781463203559"}.map do |product|
      Work.new(:client      => @client,
               :contract    => @client.contracts.find_by(:contract_name => [product["Title"].presence, product["Vol No"].presence].compact.join(": ")),
              :title        => [product["Title"].presence, product["Vol No"].presence].compact.join(": "),
              :subtitle     => product["Subtitle"].presence                                           ,
              :publishername => @client.publishernames.find_by(:publisher_name => product["Publisher"]),
              :imprint      => @client.imprints.find_by(:value => product["Imprint"])                 ,
              :workcontacts => [Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator One"].strip).first),
                                                :work_contact_role => product["Creator One Type"].presence || "A01",
                                                :sequence_number   => 1),
                                Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator Two"].strip).first),
                                                :work_contact_role => product["Creator Two Type"].presence || "A01",
                                                :sequence_number   => 2),
                                Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator Three"].strip).first),
                                                :work_contact_role => product["Creator Three Type"].presence || "A01",
                                                :sequence_number   => 3)].reject{|a| a.contact.nil?},
              :work_seriesnames => [WorkSeriesname.new(:seriesname => @client.seriesnames.where(:title_without_prefix => product["Series Title"]).first),
                                    WorkSeriesname.new(:seriesname => @client.seriesnames.where(:title_without_prefix => product["Title"]).first,
                                                       :number_within_series => product["Vol No"])].reject{|x| x.seriesname_id.nil?},
              :marketingtexts   => [Marketingtext.new(:client      => @client,
                                                      :legacy_code => "01",
                                                      :text_type   => "03",
                                                      :main_type   => "02",
                                                      :marketing_text => product["Short Desc"].presence,
                                                      :pull_quote     => product["Short Desc"].presence),
                                    Marketingtext.new(:client      => @client,
                                                      :legacy_code => "02",
                                                      :text_type   => "02",
                                                      :main_type   => "01",
                                                      :marketing_text => product["Long Desc"].presence,
                                                      :pull_quote     => product["Long Desc"].presence),
                                    Marketingtext.new(:client      => @client,
                                                      :legacy_code => "04",
                                                      :text_type   => "04",
                                                      :main_type   => "05",
                                                      :marketing_text => product["Contents"].presence,
                                                      :pull_quote     => product["Contents"].presence)].reject{|x| x.marketing_text.nil?},
              :language_of_text => (product["Language"].presence || "eng").split(",")[0].downcase,
              :main_bisac_code  => product["BISAC one"].presence              )
    end
  end

  def books
    @products.map do |product|
      work = @client.works.find_by(:title => [product["Title"].presence, product["Vol No"].presence].compact.join(": "))
      Book.new(:client          => @client,
               :work            => work       ,
               :title           => [product["Title"].presence, product["Vol No"].presence].compact.join(": "),
               :subtitle        => product["Subtitle"].presence                                           ,
               :publishing_status => product["Avail"].try(:split, ":").try(:[],0),
               :workcontacts    => work.workcontacts,
               :marketingtexts  => work.marketingtexts,
               :language        => (product["Language"].presence || "eng").split(",")[0].downcase,
               :languages       => [Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],0).try(:downcase),
                                                 :language_role => "07"),
                                    Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],1).try(:downcase),
                                                 :language_role => "06"),
                                    Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],2).try(:downcase),
                                                 :language_role => "06")
                                    ].reject{|x| x.language_code.nil?},
               :prices          => [Price.new(:currency_code => "USD",
                                              :price_amount  => product["USD price"],
                                              :price_type    => "01",
                                              :onix_price_type_qualifier_id => "05",
                                              :status_code => "00"),
                                    Price.new(:currency_code => "GBP",
                                              :price_amount  => product["GBP Price"],
                                              :price_type    => "03",
                                              :onix_price_type_qualifier_id => "05",
                                              :status_code => "00")].reject{|x| x.price_amount.nil?},
               :isbn            => product["ISBN"],
               :pub_date        => product["On Sale Date"],
               :extent_page     => Extent.new(:extent_value => product["Pages"]),
               :product_form    => product["Format"].presence,
               :product_form_detail => [product["Format Details"].presence].compact.presence)
    end
  end


  def get_uniq(key)
    @products.map{|product| product[key]}.uniq
  end

  def get_uniq_pair(key1, key2)
    @products.map{|product| [product[key1], product[key2]]}.uniq
  end

  def get_uniq_trip(key1, key2, key3)
    @products.map{|product| [product[key1], product[key2], product[key3]]}.uniq
  end

end


__END__
