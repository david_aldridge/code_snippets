client = Book.find_by(isbn: "9781909747456").client

client.
  marketingtexts.select{|mt| mt.pull_quote.html?}.each do |marketing_text|
  new_text = marketing_text.pull_quote.text.
    gsub("\r", " ").
    gsub("\n", " ").
    squish.
    gsub("> <","><").
    gsub(/(<br>){1,}$/,"")

  marketing_text.update(pull_quote: new_text)
end


PaperTrail::Version.
  where(client_id: client.id).
  where(item_type: "Marketingtext").
  where(event: "update").
  where(whodunnit: nil).
  where("created_at >= ?", Date.today).map(&:reify).each(&:save)