w = Work.find(40571)

BIC = %w{AMGD
JWTY
AMKH
HBJD1
JFHF
WMQL
WMB
WQH
WTHM
ACQ
ACV
ACX}.freeze

BISAC = %w{ARC024010
ARC005060
ART015120
ARC008000
HIS015000
GAR006000
GAR014000
HIS037050
HIS037060
HIS037070
HIS037080}.freeze

Work.find(40571).client.seriesnames.find_by(:title_without_prefix => "Follies of England").works.each do |w|
  w.bisacsubjects = BISAC.map{|x| Bisacsubject.new(:client_id => w.client_id, :code => x)}
  w.bicsubjects   = BIC.map{|x| Bicsubject.new(:client_id => w.client_id,:code => x)}
end
