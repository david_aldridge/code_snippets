[{"Price": {"TaxAmount1": "0.00", "PriceAmount": "9.99", "CurrencyCode": "GBP", "TaxRateCode1": "Z", "PriceTypeCode": "02", "TaxableAmount1": "9.99", "DiscountPercent": "0", "TaxRatePercent1": "0", "BICDiscountGroupCode": "ASNOWA99"}}, {"Price": {"PriceAmount": "11.98", "CurrencyCode": "EUR", "PriceTypeCode": "01", "DiscountPercent": "0"}}, {"Price": {"PriceAmount": "19.95", "CurrencyCode": "USD", "PriceTypeCode": "01", "DiscountPercent": "0"}}]



select cl.client_name,
       b.isbn,
       b.title,
       b.pub_date,
       gbp.price_amount,
       regexp_matches(sp.prices, '"PriceAmount": "(\d\.\d)"')
from   books b
join   clients cl on cl.id = b.client_id
join   client_groups cg on cg.client_id = cl.id
join supplier_products sp on sp.book_id = b.id
left join prices gbp on (gbp.book_id = b.id and gbp.currency_code = 'GBP')
where  cg.group_id = 5
order by cl.client_name
