client = Client.is_rebellion

works_to_update = client.works.where(thema_main_code: nil).where.not(main_bic_code: nil)
client.works.size
works_to_update.count
works_to_update.pluck(:id)

ActiveRecord::Base.transaction do
  Work.no_touching do
    works_to_update.each do |work|
      puts work.id
      work.update_columns(work.bic_codes.to_thema)
    end
  end
end ; 0




client.works.update_all(
  thema_main_code: nil,
  thema_subject_codes: [],
  thema_geog_codes: [],
  thema_lang_codes: [],
  thema_time_codes: [],
  thema_edu_codes: [],
  thema_age_codes: [],
  thema_interest_codes: [],
  thema_style_codes: []
)