Language.update_all(:work_id => nil)
errors = []
Language.joins(:book).each do |language|
  language.assign_attributes(:work_id => language.book.work_id)
  if language.valid?
    language.save
  else
    errors << language.errors.full_messages
  end
end

Language.where(:work_id => nil).count

Language.where(:work_id => nil).delete_all
