c = Client.find_by_webname("zedbooks")
PaperTrail.enabled = false

c.proprietary_format_descriptions.update_all(:books_count => 0) ; 0
c.proprietary_edition_descriptions.update_all(:books_count => 0) ; 0
c.proprietary_edition_descriptions.find_by(:code => "E" ).books = c.books.where(:product_form => "DG") ; 0
c.proprietary_edition_descriptions.find_by(:code => "Hb").books = c.books.where(:product_form => "BB") ; 0
c.proprietary_edition_descriptions.find_by(:code => "Pb").books = c.books.where(:product_form => "BC") ; 0

Rails.logger.level = 1

ActiveRecord::Base.transaction do
  c.books.each_with_index do |book,i|
    puts i+1
    usdexcvat = book.prices.where(:currency_code => "USD", :price_type => "01")
    usdincvat = book.prices.where(:currency_code => "USD", :price_type => "02")
    gbpexcvat = book.prices.where(:currency_code => "GBP", :price_type => "01")
    gbpincvat = book.prices.where(:currency_code => "GBP", :price_type => "02")

    usdincvat.destroy_all if usdexcvat.exists?
    gbpexcvat.destroy_all if gbpincvat.exists?

  end
end ; 0

ActiveRecord::Base.transaction do
  c.books.where("pub_date > ?", Date.new(2045)).each_with_index do |book,i|
    puts i+1
    book.publishing_status = "03" unless book.publishing_status == "01"
    book.pub_date          = nil if ["01", "03"].include? book.publishing_status
    book.save if book.changed?
  end
end ; 0



c = Client.find_by_webname("zedbooks")

ActiveRecord::Base.transaction do
  c.contacts.where.not(:person_name => nil).group(:person_name).having("count(id) > 1").minimum(:id).each do |person_name, min_id|
  #c.contacts.where(:person_name => "Pénélope Larzilliére").group(:person_name).having("count(id) > 1").minimum(:id).each do |person_name, min_id|
    c.contacts.where(:person_name => person_name).where.not(:id => min_id).each do |contact|
      contact.workcontacts.each do |workcontact|
        if move_to_workcontact = workcontact.work.workcontacts.where(:contact_id => min_id).first
          workcontact.bookcontacts.each do |bookcontact|
            bookcontact.workcontact = move_to_workcontact
            bookcontact.save!
          end
          workcontact.reload
          workcontact.destroy!
        else
          workcontact.contact_id = min_id
          workcontact.save!
        end
      end
      contact.reload
      contact.destroy!
    end
  end
  #raise ActiveRecord::Rollback
end

Book.where(:client_id => c.id).update_columns(:publication_city => nil, :publication_country => nil)




@client = Client.find_by(:webname => "zedbooks")

Rails.logger.level = 1

reload!
i = GoogleSheetProductImport.new("11zv-MtukoREr5K0ZoZTIFrKHEkL_MOPu5bJlu6xeeHc","zedbooks")

ActiveRecord::Base.connection.transaction do
  i.import_authors
  #raise ActiveRecord::Rollback
end

#i.import_publishernames
#i.import_imprints

ActiveRecord::Base.connection.transaction do
  i.import_seriesnames
  #raise ActiveRecord::Rollback
end

ActiveRecord::Base.connection.transaction do
  i.import_contracts
  #raise ActiveRecord::Rollback
end ; 0

ActiveRecord::Base.connection.transaction do
  i.import_works
  #raise ActiveRecord::Rollback
end ; 0

ActiveRecord::Base.connection.transaction do
  i.import_books
  #raise ActiveRecord::Rollback
end

Rails.logger.level = 1


code_map = {"03"=>"03",
            "04"=>"05",
            "08"=>"08",
            "02"=>"01",
            "25"=>"13",
            "01"=>"02",
            "13"=>"20"}
Rails.logger.level = 3

c.marketingtexts.where(:legacy_code => code_map.keys).each_with_index do |mt, i|
  puts i+1
  mt.main_type = code_map[mt.legacy_code]
  mt.save! if mt.changed?
end ; 0




PaperTrail.enabled = false

@connection    = GoogleDriveConn.new
@sheet         = @connection.session.spreadsheet_by_key("1QpVZExu8k9w7DdId5u_-Dem197L8C9rNWVZocOlV_QE")
@data_sheet    = @sheet.worksheet_by_title("data")
@data          = @data_sheet.list
@client        = Client.find_by_webname("zedbooks")
all_subjects = []
@data.each do |data|
  #book = @client.books.find_by(:isbn => data["ISBN"])
  subjects = ([data["Main subject"]]+ data["Subjects"].split(";").compact).uniq
  all_subjects += subjects
end

all_subjects = all_subjects.uniq.sort_by{|x| x}.reject{|x| x.blank?}

@client.subjectcodes.destroy_all
all_subjects.each do |s|
  @client.subjectcodes.create(:code => s, :value => s)
end

cannot_find_isbns = []
ActiveRecord::Base.connection.transaction do
  @data.each do |data|
    work              = @client.books.find_by(:isbn => data["ISBN"]).try(:work)
    if work
      main_subject      = data["Main subject"]
      puts "Main subject: #{main_subject} #{main_subject.presence}"
      subjects          = data["Subjects"].split(";").compact.uniq
      all_subjects      = (subjects + [main_subject]).compact.uniq
      work.subjectcodes = @client.subjectcodes.where(:code => all_subjects)
      work.subjectcode_id  = work.work_subjectcodes.joins(:subjectcode).where(:subjectcodes => {:code => main_subject}).first.id if main_subject.presence
      work.save! if work.changed?
    else
      cannot_find_isbns << data["ISBN"]
    end
  end
  raise ActiveRecord::Rollback
end
cannot_find_isbns




@client = Client.find_by_webname("zedbooks")
ActiveRecord:: Base.connection.transaction do
  Workcontact.where(:client_id => @client.id, :work_contact_role => "Z99").each do |wc|
    wc.work.contract.contacts << wc.contact
    wc.destroy!
  end
  #raise ActiveRecord::Rollback
end


@client = Client.find_by_webname("zedbooks")
work_ids    = @client.works.joins(:workcontacts).group("works.id").having("count(*) = 1").pluck(:id)
contacts    = @client.contacts.joins(:works).where(:works => {:id => work_ids}).where(:biographical_note => nil).uniq
Rails.logger.level = 1
ActiveRecord:: Base.connection.transaction do
  contacts.each do |contact|
    contact.update_columns(:biographical_note => Marketingtext.joins(:work => :contacts).where(:contacts => {:id => contact.id}).where(:main_type => "20").first.try(:marketing_text))
  end
  #raise ActiveRecord::Rollback
end



@client = Client.find_by_webname("zedbooks")

PaperTrail.enabled = false

ActiveRecord:: Base.connection.transaction do
  Extent.where(:client_id => @client.id).each do |extent|
    extent.update_columns(:extent_unit_id => extent.extent_type_id, :extent_type_id => extent.extent_unit_id)
  end
end

Extent.includes(:extent_type, :extent_unit).where(:client_id => @client.id).map{|e| e.to_s}.
inject(Hash.new(0)) { |h,p| h[p] += 1; h }.
sort

Rails.logger.level = 1
@client = Client.find_by_webname("zedbooks")

units = ExtentUnit.find_by(:value => "Page count").id
ppc   = ExtentType.find_by(:value => "Production page count").id
npipc = ExtentType.find_by(:value => "Number of pages in print counterpart").id
apc   = ExtentType.find_by(:value => "Absolute page count").id

type_h = {"BB"  => ppc,
          "BC"  => ppc,
          "029" => npipc,
          "031" => npipc,
          "002" => apc}
ActiveRecord:: Base.connection.transaction do
  Extent.where(:client_id => @client.id).eager_load(:book).each do |e|
    type = e.book.product_form == "DG" ? type_h[e.book.epub_type_code] : type_h[e.book.product_form]
    e.update_columns(:extent_type_id => units, :extent_unit_id => type)
  end
  #raise ActiveRecord::Rollback
end ; 0







doc            = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/Zed/ONIX/ZEDUTF800000000215232.xml")) ; 0
doc.remove_namespaces!
doc.xpath("//extent").count

doc.xpath("//extent").map{|e| [e.at_xpath("b218").text, e.at_xpath("b219").text, e.at_xpath("b220").text]}
doc.xpath("//extent").map{|e| [e.at_xpath("b218").text, e.at_xpath("b220").text]}.uniq







@client = Client.find_by_webname("zedbooks")

@client.prices.
        where(:price_type => "02", :currency_code => "GBP").
        joins(:book).
        where(:books => {:product_form => "DG"}).
        pluck(:price_amount).
        select{|p| !!(p.to_s =~ /\.98$/)}.
        inject(Hash.new(0)) { |h,p| h[p] += 1; h }.
        sort

PaperTrail.enabled = false

Rails.logger.level = 1
ActiveRecord::Base.connection.transaction do
  @client.prices.
          where(:price_type => "02", :currency_code => "GBP").
          joins(:book).
          where(:books => {:product_form => "DG"}).
          select{|p| !!(p.price_amount.to_s =~ /\.98$/)}.each do |p|
    p.update_attributes(:price_amount   => p.price_amount + 0.01,
                        :taxable_amount => nil,
                        :tax_value      => nil)
  end
  #ActiveRecord::Rollback
end ; 0


        map{|p| p.price_amount.to_s}.uniq
        inject(Hash.new(0)) { |h,v| h[v] += 1; h }.
        sort


p = 19.99.to_d
ex = (p/1.2).round(2)
p2 = (ex*1.2).round(2)

ex = [p, p-0.01, p+0.01]
ex.map{|ex| }



PaperTrail.enabled = false
Rails.logger.level = 1
Client.find_by_webname("zedbooks").books.where("pub_date < ?", Date.parse("2012-11-01")).includes(:work => :audiences).map{|b| b.work}.uniq.map{|w| w.audiences.pluck(:audience_code_value)}.uniq
ActiveRecord::Base.connection.transaction do
  Client.find_by_webname("zedbooks").books.where("pub_date < ?", Date.parse("2012-11-01")).includes(:work).map{|b| b.work}.uniq.each do |w|
    w.audiences = []
    w.update_attributes(:audiences_attributes => [{client_id:          w.client_id,
                                        audience_code_type: "01",
                                        audience_code_value: "06"},
                                       {client_id:          w.client_id,
                                        audience_code_type: "01",
                                        audience_code_value: "05"}])
  end
end




PaperTrail.enabled = false
Rails.logger.level = 1
h = {}
Client.find_by_webname("zedbooks").books.map{|b| b.measurement_string}.each{|m| h.merge!(m => 1){|k,v1, v2| v1+v2 }}
h


 {nil=>3940,
   "216 x 138mm"=>701,
   "234 x 156mm"=>128,
   "198 x 129mm"=>149,
   "216 x 135mm"=>121,
   "198 x 126mm"=>18,
   "234 x 153mm"=>24,
   "198 x 125mm"=>1,
   "354 x 156mm"=>1,
   "2160 x 138mm"=>3,
   "214 x 138mm"=>1,
   "190 x 129mm"=>1,
   "218 x 138mm"=>2,
   "246 x 138mm"=>1,
   "216 x 139mm"=>1,
   "138 x 129mm"=>1}






Audience codes


@connection = GoogleDriveConn.new
@sheet      = @connection.session.spreadsheet_by_key("11bVgCKLwPsY4PH2t8QsbF0ux-36q4mEM0aMmWDKxbis")
@worksheet  = @sheet.worksheet_by_title("Sheet1")
@raw_data   = @worksheet.list
academic    = [{:audience_code_type => "01", :audience_code_value => "05"},
               {:audience_code_type => "01", :audience_code_value => "06"}]

trade       = [{:audience_code_type => "01", :audience_code_value => "01"}]


@raw_data.each do |raw_data|
  raw_data = @raw_data.first
  work = Work.find(raw_data["id"])
  work.audiences = []
  audiences = []
  audiences += academic if raw_data["Academic"] == "Y"
  audiences += trade    if raw_data["Trade"]    == "Y"
  work.update_attributes!(:audiences_attributes => audiences)
end

Work.find(47293).audiences.pluck(:audience_code_value)
Work.find(47048).audiences.pluck(:audience_code_value)


Work.find(47293).audiences.pluck(:audience_code_value)
Work.find(47188).audiences.pluck(:audience_code_value)
Work.find(47980).audiences.pluck(:audience_code_value)
Work.find(47048).audiences.pluck(:audience_code_value)
Work.find(47038).audiences.pluck(:audience_code_value)
Work.find(48072).audiences.pluck(:audience_code_value)
Work.find(47948).audiences.pluck(:audience_code_value)
Work.find(47119).audiences.pluck(:audience_code_value)
Work.find(47965).audiences.pluck(:audience_code_value)
Work.find(47286).audiences.pluck(:audience_code_value)
Work.find(48083).audiences.pluck(:audience_code_value)
Work.find(47007).audiences.pluck(:audience_code_value)




client = Client.by_key("zedbooks")
client.books.group(:edition_statement).count



client = Client.by_key("zedbooks")
client.works.has_contributor_role("A01").has_contributor_role("A32").count



doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/Zed/ONIX/ZEDUTF800000000215232.xml")) ; 0
doc.remove_namespaces! ; 0
doc.xpath("//salesrights").map{|s| [s.at_xpath("b089").try(:text), s.at_xpath("b090").try(:text), s.at_xpath("b388").try(:text)]}.uniq





client = Client.by_key("zedbooks")
WorkSeriesname.joins(:work).where(works: {client_id: client.id}).where(seriesname_id: nil).






client = Client.by_key("zedbooks")
client.prices.joins(:book).references(:book).where(:books => {:epub_type_code => "002"}).count
client.prices.joins(:book).references(:book).where(:books => {:epub_type_code => "002"}).group(:onix_price_type_qualifier_id).count
Rails.logger.level = 1
PaperTrail.enabled = false
client.prices.joins(:book).references(:book).where(:books => {:epub_type_code => "002"}, :prices => {:onix_price_type_qualifier_id => "05"}).each {|price| price.update_attributes(:onix_price_type_qualifier_id => "10")};0

PF_MAP     = {"002" => "BC", "029" => "BB", "031" => "BB"}.freeze
NEW_QUAL_MAP  = {"002" => "05", "029" => "10", "031" => "10"}.freeze
ActiveRecord::Base.transaction do
  client.books.where(:books => {:product_form => "DG"}).includes(:all_books_on_work).each do |target_book|

  #target_book = client.books.where(:books => {:product_form => "DG"}).where(:isbn => "9781783607877").includes(:all_books_not_ebook).first
    source_book = target_book.all_books_not_ebook.select{|book| book.product_form == PF_MAP[target_book.epub_type_code] && (book.pub_date  || Date.today + 3.years) <= (target_book.pub_date || Date.today + 3.years)}.sort_by{|book| (book.pub_date || Date.today + 3.years)}.reverse.first
    if source_book
      target_book.prices << source_book.prices.map |old_price|
        price = old_price.dup
        price.onix_price_type_qualifier_id = NEW_QUAL_MAP[target_book.epub_type_code]
        price.tax_rate_code                = "S"
        price.taxable_amount               = price.price_amount
        price.tax_value                    = (price.taxable_amount * 0.2).round(2)
        price.price_amount                 = price.taxable_amount + price.tax_value
        price
      end
    end
  end
end




client = Client.by_key("zedbooks")
Rails.logger.level=1
PaperTrail.enabled = false
client.prices.joins(:book).
              references(:book).
              where(:books  => {:product_form                 => 'DG'},
                    :prices => {:onix_price_type_qualifier_id => '10',
                                :tax_rate_code                => 'Z' ,
                                :currency_code                => 'GBP',
                                :price_type                   => '02'}).each do |price|

  price.tax_rate_code    = 'S'
  price.tax_rate_percent = 20.to_d
  price.tax_value        = price.taxable_amount * 0.2.to_d
  price.price_amount     = price.taxable_amount + price.tax_value
  price.save!
end

client.prices.joins(:book).
              references(:book).
              where(:books  => {:product_form                 => 'DG'},
                    :prices => {:onix_price_type_qualifier_id => '05',
                                :tax_rate_code                => 'Z' ,
                                :currency_code                => 'GBP'}).each do |price|
  price.tax_rate_code    = 'S'
  price.tax_rate_percent = 20.to_d
  price.taxable_amount   = (price.price_amount / 1.2.to_d).round(2)
  price.tax_value        = price.price_amount - price.taxable_amount
  price.save!
end ; 0


client.prices.joins(:book).
              references(:book).
              where(:books  => {:product_form                 => 'DG'}).
              group(:currency_code, :onix_price_type_qualifier_id)




client = Client.by_key('zedbooks')

client.prices.
       joins(:book).
       references(:book).
       where(:books => {:product_form => 'BB'}).
       where("books.pub_date >= ?", Date.parse('2015-01-01')).
       where(:price_amount => 65, :currency_code => 'GBP').
       each{|p| p.update_attributes(:price_amount => 70, :taxable_amount => 70)}

 client.prices.
        joins(:book).
        references(:book).
        where(:books => {:product_form => 'DG', :epub_type_code => ['002', '029']}).
        where("books.pub_date >= ?", Date.parse('2015-01-01')).
        where(:onix_price_type_qualifier_id => '10', :currency_code => 'GBP').
        each{|p| p.update_attributes(:price_amount   => 84,
                                     :taxable_amount => 70,
                                     :tax_value      => 14,
                                     :tax_rate_code  => 'S',
                                    :tax_rate_percent => 20,
                                    :price_type       => '02'  )}



 client.prices.
        joins(:book).
        references(:book).
        where(:books => {:product_form => 'DG', :epub_type_code => ['002', '029']}).
        where("books.pub_date >= ?", Date.parse('2015-01-01')).
        where(:onix_price_type_qualifier_id => '10', :currency_code => 'USD').
        each{|p| p.update_attributes(:price_amount     => 95,
                                     :taxable_amount   => 95,
                                     :tax_value        => nil,
                                     :tax_rate_code    => nil,
                                     :tax_rate_percent => nil,
                                     :price_type       => '01')}





client.contacts.select{|c| c.keynames =~ c.names_before_key}




Extent.where(:client_id => client.id).group(:extent_type_id, :extent_unit_id).count
