select  c.id,
        c.client_name,
        book_count    ,
        work_count    ,
        contract_count,
        contact_count
from   clients c
left join (select client_id, count(*) book_count     from books     group by client_id) b on b.client_id = c.id
left join (select client_id, count(*) work_count     from works     group by client_id) w on w.client_id = c.id
left join (select client_id, count(*) contract_count from contracts group by client_id) o on o.client_id = c.id
left join (select client_id, count(*) contact_count  from contacts  group by client_id) d on d.client_id = c.id
order by 3 desc;





de