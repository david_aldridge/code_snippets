client = Client.is_zed
shop = client.shops.find(15)
shop_work_ids = shop.work_ids
shop_book_ids = shop.book_ids
pdfs_required = client.books.pdfs.where(work_id: shop_work_ids)
pdfs_missing = pdfs_required.reject{|b| b.id.in? shop.book_ids}
shop.books += pdfs_missing
