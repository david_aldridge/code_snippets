book = Book.find(95433)
:allow_onix_exports => false,
:isbn => 'placeholder',
:product_form => 'PZ',
:edition_statement => 'Numbered, limited edition print',
:product_form_description => 'Art print',
:edition_type_codes => ['NUM'],
:prices => [],
:main_content_page_count => nil,
:product_height_mm => nil,
:product_width_mm => nil,
:marketingtexts => [],
:supportingresources => []
book.save


new_attribs = {
  :allow_onix_exports => false,
  :isbn => 'placeholder',
  :product_form => 'PZ',
  :edition_statement => 'Numbered, limited edition print',
  :product_form_description => 'Art print',
  :edition_type_codes => ['NUM'],
  :prices => [],
  :main_content_page_count => nil,
  :product_height_mm => nil,
  :product_width_mm => nil,
  :marketingtexts => [],
  :supportingresources => [],
  :illustrations_note => nil
}

Collection.find(1611).books.each do |old_book|
  DuplicateBookService.new(old_book: old_book,
                   new_book_attributes: new_attribs
                   ).call
end
