client = Client.is_vertebrate

client.book_masterchannels.select do |bmc|
  (bmc.channels & bmc.masterchannel.channels).size < bmc.channels.size
end
