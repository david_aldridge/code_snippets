client = Client.is_riba

client.books.select{|b| b.publishername_id.in? distribution_publisher_ids}.size

distribution_publishers = Publishername.where(id: client.contact_digital_asset_transfer_templates.find(46).publishername_ids)

distribution_publisher_ids = distribution_publishers.map(&:id)

distribution_works = distribution_publishers.map{|p| p.works}.flatten

distribution_books = distribution_works.map{|w| w.books}.flatten

client.works.select{|w| w.publishername_id.in? distribution_publisher_ids}.count

distro_book = client.books.select{|b| client.contact_digital_asset_transfer_templates.find(46).supplies_publishername(b.publisher_name)}.count

client.books.where(:allow_onix_exports => true).each{|b| b.update_attributes(:allow_onix_exports => false) }




client.books.where(:allow_onix_exports => true).map{|b| b.publisher_name.try(:publisher_name)}


distro_books = client.books.select{|b| client.contact_digital_asset_transfer_templates.find(46).supplies_publishername(b.publisher_name)}

distro_books_to_send = distro_books.select{|b| b.pub_date && b.pub_date >= Date.civil(2016,8,1) && b.pub_date <= Date.civil(2017,3,31)}.reject{|b| b.isbn.in? ['9783869223704', '9783869223704', '9783869225081'] }

pp distro_books_to_send.map{|b| [b.pub_date, b.title, b.publisher_name.publisher_name]}.sort

distro_books_to_send.each{|b| b.update_attributes(:allow_onix_exports => true) }

pp Client.is_riba.books.where(:allow_onix_exports => true).map{|b| [b.id, b.pub_date, b.title, b.publisher_name.publisher_name]}.sort

 1st of August 2016 up to March 2017 should be set as ‘yes’ and disseminated. There is however one title which I had to set as ‘no’ because the publisher gave us initially the wrong ISBN so please make sure that one is set as no (it’s: Dessau/Worlitz Architectural Guide, 9783869223704). Anything going beyond March 2017