client = Client.is_quiller

client.works.where(thema_main_code: nil).select{|w| w.thema_subject_codes.size == 1}.each do |w|
  w.thema_main_code = w.thema_subject_codes.first
  w.thema_subject_codes = []
  w.save
end

x = Hash[*client.works.
  where(thema_main_code: nil).
  select{|w| w.thema_subject_codes.size > 1}.
  map{|w| w.thema_subject_codes}.
  group_by(&:itself).
  transform_values(&:size).
  sort_by(&:last).
  reverse.
  flatten(1)]

pp x

{["SKL", "WNGH"]=>26,
 ["SK", "WNGH"]=>14,
 ["SVH", "WNGD"]=>12,
 ["MZDH", "WNGH"]=>7,
 ["YBG", "YNNH"]=>5,
 ["SVH", "SVS"]=>5,
 ["YNNH", "YNW"]=>4,
 ["SKL", "SZC"]=>3,
 ["GBCY", "SKL"]=>3,
 ["AJC", "SVH"]=>3,
 ["SVH", "WNC"]=>3,
 ["GBC", "SKL"]=>3,
 ["WNGH", "YNNH"]=>3,
 ["YNNH", "YNVP"]=>3,
 ["SKL", "YNW"]=>3,
 ["SVH", "WNCB"]=>3,
 ["MBN", "SKL"]=>2,
 ["SV", "WBTB"]=>2,
 ["KND", "SVS"]=>2,
 ["WHJ", "WNGH"]=>2,
 ["MBX", "NHD"]=>2,
 ["DNBH", "NHD"]=>2,
 ["SV", "WNF", "WNGD"]=>2,
 ["SVS", "WND"]=>2,
 ["NHT", "SVS"]=>2,
 ["FBA", "FR", "YFB"]=>2,
 ["SCG", "SKL"]=>2,
 ["AFF", "WFA"]=>2,
 ["AFC", "AGA", "AGB"]=>2,
 ["DNL", "SVF"]=>2,
 ["GBC", "MZDH", "WNGH"]=>2,
 ["SKL", "WKDW"]=>2,
 ["SK", "WDK"]=>2,
 ["DNBS", "SK", "WNGH"]=>2,
 ["SK", "SKL", "WNGH"]=>2,
 ["MZ", "SVH", "WNGD"]=>2,
 ["YBG", "YNW"]=>2,
 ["YNVP", "YNW"]=>2,
 ["SKL", "WKD"]=>2,
 ["YFM", "YFP"]=>2,
 ["SVH", "TDP"]=>2,
 ["RNKH", "WNC"]=>1,
 ["SZG", "WTL"]=>1,
 ["AJC", "RNP", "WNCS1"]=>1,
 ["AKT", "JBCC3", "NHT"]=>1,
 ["SVF", "WTL"]=>1,
 ["RNF", "RNKH", "WNC"]=>1,
 ["JPWG", "SV"]=>1,
 ["AVN", "AVP", "DSBH", "DSK"]=>1,
 ["RNF", "RNK", "WN"]=>1,
 ["DC", "XY"]=>1,
 ["AJC", "RNKH", "WN"]=>1,
 ["JNR", "SKL", "WNGH", "YNK"]=>1,
 ["GBC", "WNC"]=>1,
 ["PSVM", "RNF", "RNKH", "WNC"]=>1,
 ["KNS", "VSC", "WGG"]=>1,
 ["RNF", "SV"]=>1,
 ["DNB", "SV"]=>1,
 ["AGN", "SV", "WNCB"]=>1,
 ["JKSN1", "JWXV"]=>1,
 ["DNXP", "WNGD"]=>1,
 ["SV", "XY"]=>1,
 ["WKH", "WND"]=>1,
 ["NHTB", "WF"]=>1,
 ["FBA", "FF"]=>1,
 ["KNJ", "NHTB"]=>1,
 ["JWCK", "NHTM"]=>1,
 ["NHD", "WTH"]=>1,
 ["DNB", "SKG"]=>1,
 ["JBCC", "WBT"]=>1,
 ["KND", "NHTB", "WBXN"]=>1,
 ["KFFM", "KNG", "NHTB"]=>1,
 ["AGA", "AKT", "JBCC3", "NHTB"]=>1,
 ["MZDH", "VFMS", "WNGH"]=>1,
 ["VXHH", "WNGH"]=>1,
 ["SVS", "TBX"]=>1,
 ["SCG", "SKL", "WNGH"]=>1,
 ["SMC", "SVH"]=>1,
 ["FBA", "FYB"]=>1,
 ["AFT", "TV"]=>1,
 ["PSVC", "SVF"]=>1,
 ["AGB", "NHTB"]=>1,
 ["AJC", "SCB"]=>1,
 ["KND", "NHTB"]=>1,
 ["JKSW", "NHTB"]=>1,
 ["GLZ", "NHTB"]=>1,
 ["GBC", "WNGD"]=>1,
 ["LNS", "WNC"]=>1,
 ["SVS", "TDP"]=>1,
 ["KND", "NHTB", "SVS"]=>1,
 ["DNT", "WNGD"]=>1,
 ["WNCB", "WZS"]=>1,
 ["NHT", "SVH"]=>1,
 ["WM", "WNC"]=>1,
 ["WNF", "WNG"]=>1,
 ["SPNG", "TRLD", "TTS"]=>1,
 ["DNBS", "S"]=>1,
 ["GBC", "KND", "SVS"]=>1,
 ["MZDH", "VFDF", "WNGH"]=>1,
 ["VFMS", "WNGH"]=>1,
 ["WQH", "WTRM"]=>1,
 ["SV", "WZ"]=>1,
 ["SVH", "WND"]=>1,
 ["SMC", "SVF"]=>1,
 ["RNK", "WN"]=>1,
 ["SVH", "WN"]=>1,
 ["SKL", "XY"]=>1,
 ["SV", "SZ"]=>1,
 ["DNB", "TB"]=>1,
 ["TBX", "TRC", "WGC"]=>1,
 ["TVH", "WN"]=>1,
 ["NHTB", "SCB", "SVH"]=>1,
 ["WBA", "WBB"]=>1,
 ["KJZ", "KNS"]=>1,
 ["SVH", "WNGD1", "XY"]=>1,
 ["SKL", "WN"]=>1,
 ["SK", "TVH", "WNGH"]=>1,
 ["KNX", "SKL", "VSF"]=>1,
 ["SVH", "XY"]=>1,
 ["AMK", "WKDW"]=>1,
 ["GB", "WNC"]=>1,
 ["GBC", "WNCB"]=>1,
 ["KNA", "TVH"]=>1,
 ["AJC", "WNGH"]=>1,
 ["KND", "NHTK", "SVS"]=>1,
 ["SVS", "WQH"]=>1,
 ["SK", "SKL"]=>1,
 ["DNBA", "TV"]=>1,
 ["DCF", "WBXN"]=>1,
 ["SCBV", "SFT"]=>1,
 ["NHTB", "WBN"]=>1,
 ["AJC", "JWT"]=>1,
 ["ATL", "DNC", "SFD"]=>1,
 ["SVF", "XY"]=>1,
 ["AJC", "WNGD"]=>1,
 ["YNNH", "YZ"]=>1,
 ["SVF", "WFC"]=>1,
 ["AJT", "WNGH"]=>1,
 ["MB", "NHD"]=>1,
 ["MZD", "SV"]=>1,
 ["AGA", "AGN", "WNCB"]=>1,
 ["ATXZ", "WNGD"]=>1,
 ["KJH", "SV"]=>1,
 ["AJ", "PSVJ"]=>1,
 ["SKL", "WDK"]=>1,
 ["MZDH", "VXHH", "WNGH"]=>1,
 ["YFP", "YNV"]=>1,
 ["GBC", "SKL", "WNGH"]=>1,
 ["WHJ", "WNGD"]=>1,
 ["GBC", "MZDH", "VXH", "WNGH"]=>1,
 ["PSV", "VXHT", "WNGD"]=>1,
 ["DNBS", "SKL"]=>1,
 ["MZDH", "SKL"]=>1,
 ["DNB", "WNGH"]=>1,
 ["AJC", "SK"]=>1,
 ["RNF", "SV", "WN"]=>1,
 ["SK", "WJF"]=>1,
 ["SVS", "TV"]=>1,
 ["MBX", "MN"]=>1,
 ["DNB", "SVH"]=>1,
 ["DNBA", "WNC", "WNF", "WNGH"]=>1,
 ["RNF", "WN"]=>1,
 ["TVP", "WM", "WN"]=>1,
 ["WH", "WND", "WNF"]=>1,
 ["RGBL", "WN"]=>1,
 ["PSV", "WNGD"]=>1,
 ["SCGP", "SK", "WNGH"]=>1,
 ["YFP", "YFQ", "YFR"]=>1,
 ["SK", "VXH", "WFB", "WNGH", "WTH"]=>1,
 ["GBC", "SK"]=>1,
 ["GBC", "WNGH"]=>1,
 ["SCG", "SKL", "VFMG"]=>1,
 ["MZDH", "SK", "WNGH"]=>1,
 ["MZDH", "SKL", "WNGH"]=>1,
 ["DNB", "SVF"]=>1,
 ["AFH", "AJC", "AM", "NHF", "WT"]=>1,
 ["AFC", "AGB", "DNBA", "SFD"]=>1,
 ["SV", "WNGD"]=>1,
 ["WCN", "WFS"]=>1,
 ["NHTB", "WCN", "WFS"]=>1,
 ["SVH", "YNW"]=>1,
 ["AFC", "AFF", "AGA", "AGB", "WN"]=>1,
 ["GBC", "SVF"]=>1,
 ["AFC", "AGA", "AGB", "DNB"]=>1,
 ["FBA", "FR"]=>1,
 ["JNRV", "SKL"]=>1,
 ["GBCT", "GBCY", "SKL"]=>1,
 ["SKL", "VFM"]=>1,
 ["VXA", "WNGH"]=>1,
 ["AKT", "KNP", "NHTB"]=>1,
 ["MZDH", "MZH"]=>1,
 ["SCB", "SKL"]=>1,
 ["GBCR", "SVH"]=>1,
 ["DNC", "SVH", "SVS", "SVT"]=>1,
 ["SVH", "SVT"]=>1,
 ["NHTB", "SV"]=>1,
 ["YBG", "YNVP"]=>1,
 ["VSD", "WND"]=>1,
 ["TVHB", "WNGD"]=>1,
 ["NHTB", "SVH"]=>1,
 ["MZDH", "TVHB"]=>1,
 ["SCGP", "SKL"]=>1,
 ["SKL", "VFMG"]=>1,
 ["AFKC", "SVH", "WCC", "WCT"]=>1,
 ["VXHT", "WNGH"]=>1,
 ["SFD", "YNWD"]=>1,
 ["GBC", "SVH", "WNGK"]=>1,
 ["DNBH", "NHB", "NHTB"]=>1,
 ["AFC", "AGA", "AGB", "AGN", "DNB"]=>1,
 ["AKLC", "S", "XY"]=>1,
 ["AFF", "PSVJ", "WNCB"]=>1,
 ["VSG", "WNGH"]=>1,
 ["NHTB", "TVH", "WB"]=>1,
 ["SVF", "WH"]=>1,
 ["TVHB", "WNGH"]=>1,
 ["RNF", "RNKH", "TVR", "WNC"]=>1,
 ["JNU", "SKL"]=>1,
 ["AJC", "TV", "WN"]=>1,
 ["AJC", "AJTF", "SV"]=>1,
 ["AFC", "AGB", "SVF"]=>1,
 ["AJC", "WTM"]=>1,
 ["JBCC1", "JBGB", "WNGC"]=>1,
 ["SK", "WH", "WHX", "WN", "XY"]=>1,
 ["VS", "VX"]=>1}


 x = Hash[*client.works.
   where(thema_main_code: nil).
   select{|w| w.thema_subject_codes.size > 1}.
   map{|w| [w.main_bic_code, w.thema_subject_codes]}.
   group_by(&:itself).
   transform_values(&:size).
   sort_by(&:last).
   reverse.
   flatten(1)]
pp x
