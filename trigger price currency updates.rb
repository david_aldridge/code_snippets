client = Client.is_trigger


books = client.books.includes(:prices) ; 0


target_prices =
  [
    :current_eur_exctax_consumer_price,
    :current_usd_exctax_consumer_price,
    :current_cad_exctax_consumer_price
  ]
price_map =
  {
     5.99 => [ 7.95 ,  7.95 ,  10.95],
     6.99 => [ 7.95 ,  6.95 ,  7.95],
     9.99 => [ 11.95 , 10.95 , 11.95],
    11.99 => [ 15.99 , 12.95 , 21.95],
    14.99 => [ 18.99 , 19.95 , 26.95],
    16.99 => [ 21.99 , 22.95 , 28.95]
  }

price_map.each do |gbp_price, other_prices|
  books.select{|b| b.current_gbp_inctax_consumer_price == gbp_price}.each do |book|
    target_prices.each_with_index do |method, idx|
      book.send("#{method}=".to_sym, other_prices[idx])
    end
  end
end ; 0

