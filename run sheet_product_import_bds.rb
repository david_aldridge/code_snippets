AUTHORS
=========
reload!
client          = Client.is_bds
spreadsheet_key = '1WA1gIaULRtVbH7tWZTRzdjFEMh8N9hMEQgQCT8MwKu8'
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
sheet = Google::SheetAuthorImportBDS.new(spreadsheet_key, "Contacts")

sheet.invalid_organisations
sheet.valid_organisations
sheet.valid_unique_organisations
sheet.new_organisations
sheet.save_new_organisations!

sheet.invalid_people
sheet.valid_people
sheet.valid_unique_people
sheet.new_people
sheet.save_new_people!




PRODUCTS
=========
reload!
Rails.logger.level=1
PaperTrail.enabled=false
spreadsheet_key = '1WA1gIaULRtVbH7tWZTRzdjFEMh8N9hMEQgQCT8MwKu8'
sheet           = Google::SheetProductImportBDS.new(spreadsheet_key, "Products")

sheet.set_author_ids!

sheet.contracts
sheet.invalid_contracts
sheet.valid_contracts
sheet.save_contracts!

sheet.works
sheet.invalid_works
sheet.valid_works
sheet.save_works!

sheet.books
sheet.invalid_books
sheet.valid_books
sheet.save_books!



Client.is_bds.books.
              joins(:proprietary_edition_description).
              where(:proprietary_edition_descriptions => {:name => 'Chapter'}).
              where(Book.arel_table["created_at"].gteq Date.today).each do |book|
  book.current_gbp_inctax_consumer_price = 30
  book.current_eur_exctax_consumer_price = 30.00
  book.current_usd_exctax_consumer_price = 32.50
  book.current_cad_exctax_consumer_price = 42.50
end

c = Client.is_bds.collections.create(name: "December Load")
c.books = c.client.books.where(Book.arel_table["created_at"].gteq Date.today)

gbp_inctax = 30.00
eur_exctax = 30.00
usd_exctax = 32.50
cad_exctax = 42.50

Client.is_bds.contracts.delete_all
Client.is_bds.works.delete_all
Client.is_bds.books.delete_all
Client.is_bds.contacts.delete_all

Client.is_bds.works.includes(:audiences).where(:audiences => {:id => nil}).each{|w| w.audiences.professional_academic.create} ; 0

Client.is_bds.audiences.group(:audience_code_type, :audience_code_value).count



Client.is_bds.contacts.
              includes(:workcontacts, :contact_types).
              where.not(:workcontacts => {:id => nil}).
              where(:contact_types => {:id => nil}).each{|c| c.contact_types = ContactType.all.book_contributors}


Client.is_bds.books.
              joins(:proprietary_edition_description).
              where(:proprietary_edition_descriptions => {:name => 'Chapter'}).each do |book|
  book.current_gbp_inctax_consumer_price = 30
  book.current_eur_exctax_consumer_price = 32.50
  book.current_usd_exctax_consumer_price = 32.50
  book.current_cad_exctax_consumer_price = 42.50
end
