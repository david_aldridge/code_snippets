client = Client.is_british_library

duplicates = Client.is_british_library.contacts.group(:names_before_key, :keynames, :corporate_name).having("count(*) > 1").count

duplicates.keys.each do |names_before_key, keynames, corporate_name|
  base_contact = client.contacts.
    where(
      names_before_key: names_before_key,
      keynames:         keynames,
      corporate_name:   corporate_name
    ).first

  client.contacts.
    where(
      names_before_key: names_before_key,
      keynames:         keynames,
      corporate_name:   corporate_name
    ).where("id > ?", base_contact.id).order(:id).each do |contact|
      contact.workcontacts.each do |workcontact|
        puts "On workcontact #{workcontact.id} replacing contact #{workcontact.contact_id} with #{base_contact.id}"
        workcontact.update(contact_id: base_contact.id)
      end
      contact.destroy
    end
end


MERGES = {
75660 => 75550
}

client = Client.is_purdue

ActiveRecord::Base.transaction do
  MERGES.each do |merge_into_id, duplicate_contact_id|
    base_contact = client.contacts.find_by(id: merge_into_id)
    unless base_contact
      puts "cannot find #{duplicate_contact_id}"
      next
    end

    delete_contact = client.contacts.find_by(id: duplicate_contact_id)
    unless delete_contact
      puts "cannot find #{duplicate_contact_id}"
      next
    end
    delete_contact.workcontacts.each do |workcontact|
      puts "On workcontact #{workcontact.id} replacing contact #{workcontact.contact_id} with #{base_contact.id}"
      workcontact.update(contact_id: base_contact.id)
    end
    delete_contact.destroy
  end
  # raise ActiveRecord::Rollback
end







MERGES = {
75842 => 75672,
75683 => 75736,
75628 => 75631,
75449 => 75479,
76055 => 75886,
75355 => 100795,
75563 => 75430,
75708 => 75686,
75749 => 75818,
75408 => 75410,
75521 => 75509,
75439 => 75666,
75378 => 75379,
75991 => 75840,
75388 => 75535,
96002 => 96450,
75820 => 76063,
75880 => 75387,
75477 => 75705,
75436 => 75438,
71168 => 70956,
75633 => 75636,
75633 => 75635,
75633 => 75638,
75740 => 75634,
75637 => 75697,
75545 => 100668,
75939 => 75487,
75568 => 75570,
75484 => 75480,
75723 => 75822,
75787 => 75789,
75787 => 76052,
76053 => 75817,
75651 => 83574,
75414 => 100757,
96001 => 96451,
75348 => 75350,
76050 => 75798,
75738 => 75661,
83591 => 75877,
75759 => 91467,
75780 => 75778,
75833 => 75890,
75720 => 75725,
75977 => 83583,
75721 => 75724,
75345 => 75764,
75404 => 75446,
75399 => 100768,
76048 => 75793,
70941 => 71164,
71151 => 72477,
75746 => 75662,
75825 => 75899,
75441 => 75442
}

client = Client.is_purdue

ActiveRecord::Base.transaction do
  MERGES.each do |merge_into_id, duplicate_contact_id|
    base_contact = client.contacts.find_by(id: merge_into_id)
    unless base_contact
      puts "cannot find #{duplicate_contact_id}"
      next
    end

    delete_contact = client.contacts.find_by(id: duplicate_contact_id)
    unless delete_contact
      puts "cannot find #{duplicate_contact_id}"
      next
    end
    delete_contact.workcontacts.each do |workcontact|
      puts "On workcontact #{workcontact.id} replacing contact #{workcontact.contact_id} with #{base_contact.id}"
      workcontact.update(contact_id: base_contact.id)
    end
    delete_contact.destroy
  end
  # raise ActiveRecord::Rollback
end



