Client.is_liverpool.
  books.forthcoming_or_active.pdfs.
  includes(:production_files).
  where(production_files: {id: nil}).
  pluck(:isbn)
