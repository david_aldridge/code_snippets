load_timestamp = Client.is_vertebrate.sales.
  where(invoice_date: Date.civil(2019,04,01)).minimum(:created_at)


Client.is_vertebrate.
  book_channels.
  where("created_at between ? and ?", load_timestamp, load_timestamp + 1.hour).
  pluck(:created_at).sort.uniq


Client.is_vertebrate.
  book_masterchannels.
  where("created_at between ? and ?", load_timestamp, load_timestamp + 1.hour).
  pluck(:created_at).sort.uniq




load_timestamp = Client.is_vertebrate.sales.
  where(invoice_date: Date.civil(2019,04,01)).minimum(:created_at)


Client.is_vertebrate.
  book_channels.
  where("created_at between ? and ?", load_timestamp, load_timestamp + 1.hour).
  destroy_all


Client.is_vertebrate.
  book_masterchannels.
  where("created_at between ? and ?", load_timestamp, load_timestamp + 1.hour).destroy_all

Client.is_vertebrate.sales.where(invoice_date: Date.civil(2019,04,01)).destroy_all
