One of the alleged advantages of ONIX 3.0 over ONIX 2.1 is that the former is better at handling confusions between product titles and collection (series or set) titles.

In practice, this means that ONIX 3.0 has much greater flexibility in the way that you name a product that is part of a collection.

In fact the name of a product is commonly made up of many components, for example:

  * A separate title and prefix, to better allow alphabetical presentation of the name "The Hobbit":
    * Title prefix: "The"
    * Title without prefix: "Hobbit"
  * A collection name and product name:
    * Collection: "Penguin Modern Classics"
    * Title prefix: "The"
    * Title without prefix: "Beautiful and Damned"
  * A collection name and a part number, "Advances in Chemical Physics: Volume 140":
    * Collection: "Advances in Chemical Physics"
    * Part number: "Volume 140"
  * A collection name, product title, and part number "The Lord of the Rings, Book 2: The Two Towers"
  * A product title, collection name, and part number "The Two Towers (Book 2 of The Lord of the Rings)"



There are three ways in which a part number can be included in ONIX 3.

Firstly, it can just be part of the product's name. If you publish a work in two volumes, perhaps due to printing limitation


No series, no part

Title

No series, part

Title
Title#Part

Collection, no part

Title (Example 1)
Collection: Title
Title (Collection)

Collection + part

Title – Example 2, 3
Part: Title
Collection: Part –Example 4
Collection: Part: Title – Example 5
Title (Collection, Part) – Example 6
Title (Part of Collection) – Example 6
Title: Collection, Part – Example 6
Title: Part of Collection – Example 6
Title (Collection)
Title: Collection

Title#Part#Collection
Collection#Part#Title
Collection#Title#Part

Part#Title




name = OpenStruct.new(title: "The Two Towers", collection: "Lord of the Rings", part_number: "Book II")

name.title

[
  name.title,
  ("(#{[name.collection, name.part_number].compact.join(', ')})" if name.collection || name.part_number)
].compact.join(" ")

[
  ("#{[name.collection, name.part_number].compact.join(', ')}" if name.collection || name.part_number),
  name.title
].compact.join(": ")

[
  name.title,
  ("(#{[name.part_number, name.collection].compact.join(' of the ')})" if name.collection || name.part_number)
].compact.join(" ")

[
  name.title,
  ([name.part_number, name.collection].compact.join(' of the ') if name.collection || name.part_number)
].compact.join(": ")



Dark Materials
Golden Compass
Book I


Dark Materials Book