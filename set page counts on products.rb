Client.is_pharma.
       books.
       where(:pages_arabic => nil).
       where("substr(product_form,1,1) = 'B' or product_form = 'DG'").
       group(:product_form).
       count

Client.is_pharma.
       books.
       where(:pages_arabic => nil).
       includes(:work).
       each{|b| b.update_attributes(pages_arabic: b.work.books.active.pluck(:pages_arabic).compact.max ) }

Client.is_pharma.
       books.
       where(:pages_arabic => nil).
       includes(:work).
       each{|b| b.update_attributes(pages_arabic: b.work.books.pluck(:pages_arabic).compact.max ) }

Client.is_pharma.
       works.
       map{|w| w.books.group(:pages_arabic).count}.select{|h| h.keys.size > 1}



pp Client.is_pharma.
       books.
       where("substr(product_form,1,1) = 'B' or product_form = 'DG'").
       where('pub_date < ?', Date.civil(2011,6,29)).
       where(:pages_arabic => nil).map{|b| "http://app.bibliocloud.com/works/#{b.work_id}/edit#Extents"}.uniq
