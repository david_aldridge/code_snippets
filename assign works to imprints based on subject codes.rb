client = Client.is_canelo

client.subjectcodes.pluck(:value)
=> ["Canelo Saga", "Canelo Action Thriller", "Canelo Adventure", "Abandoned Bookshop", "Canelo", "Frisch & Co.", "Canelo Escape", "Canelo Original", "Canelo USA"]

client.imprints.pluck(:value)
=> ["Canelo USA", "Canelo Saga", "Canelo Escape", "Canelo Action", "Canelo Adventure", "Frisch & Co.", "Abandoned Bookshop", "Canelo"]

client.imprints.find_by(value: "Canelo Action").works = client.subjectcodes.find_by!(value: "Canelo Action Thriller").works

client.imprints.find_by(value: "Canelo Adventure").works = client.subjectcodes.find_by!(value: "Canelo Adventure").works

client.imprints.find_by(value: "Canelo Escape").works = client.subjectcodes.find_by!(value: "Canelo Escape").works

client.imprints.find_by(value: "Canelo Saga").works = client.subjectcodes.find_by!(value: "Canelo Saga").works

client.imprints.find_by(value: "Canelo USA").works = client.subjectcodes.find_by!(value: "Canelo USA").works
