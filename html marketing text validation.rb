

Client.is_boldwood.marketingtexts.where("pull_quote like '%<%'").map do |text|
  [
    text,
    Nokogiri::HTML("<!DOCTYPE html><html>#{text.pull_quote.to_s}</html>").errors
  ]
end.select do |_text, errors|
  errors.any?
end.map do |text, errors|
  [
    text.work_id,
    text.id,
    errors,
    text.pull_quote.to_s,
    Nokogiri::HTML("<!DOCTYPE html><html>#{text.pull_quote.to_s}</html>").at_xpath("//body").to_xhtml.sub(/\A<body>/, "").sub(/<\/body>\z/, "")
  ]
end


Marketingtext.joins(:client).merge(Client.active).where("pull_quote like '%<%'").pluck(:pull_quote).map do |text|
  Nokogiri::HTML("<!DOCTYPE html><html>#{text}</html>").errors.any?
end.group_by(&:itself).transform_values(&:size)


Marketingtext.
  joins(:client).
  merge(Client.active).
  where("pull_quote like '%<%'").
  pluck(:pull_quote, :client_id).
  map do |text, client_id|
    [
      client_id,
      Nokogiri::HTML("<!DOCTYPE html><html>#{text}</html>").errors.any? ? 1 : 0
    ]
  end.
  sort.
  group_by(&:itself).
  transform_values(&:size)

  string = "\n         How do archaeologists make knowledge? Debates in the latter half of the twentieth century revolved around broad, abstract philosophies and theories such as positivism and hermeneutics which have all but vanished today. By contrast, in recent years there has been a great deal of attention given to more concrete, practice-based study, such as fieldwork. But where one was too abstract, the other has become too descriptive and commonly evades issues of epistemic judgement.\n         \n            Writing the Past attempts to reintroduce a normative dimension to knowledge practices in archaeology, especially in relation to archaeological practice further down the ‘assembly line’ in the production of published texts, where archaeological knowledge becomes most stabilized and is widely disseminated. By exploring the composition of texts in archaeology and the relation between their structural, performative characteristics and key epistemic virtues, this book aims to move debate in both knowledge and writing practices in a new direction.\n         Although this book will be of particular interest to archaeologists, the argument offered has relevance for all academic disciplines concerned with how knowledge production and textual composition intertwine.\n      "
  string = "<P>This comprehensive volume addresses the important question of whether and how the current transformation of targeted killing is transforming the global international order. This book was originally published as a special issue of <I>Contemporary Security Policy</I>.</P>"
  doc = Nokogiri::HTML("<!DOCTYPE html><html>#{string}</html>")
  doc.errors

  doc.at_xpath("//body").to_xhtml.sub(/\A<body>/, "").sub(/<\/body>\z/, "").strip


  string.split("\n").map(&:presence).compact

  "How do archaeologists make knowledge? Debates in the latter half of the twentieth century revolved around broad, abstract philosophies and theories such as positivism and hermeneutics which have all but vanished today. By contrast, in recent years there has been a great deal of attention given to more concrete, practice-based study, such as fieldwork. But where one was too abstract, the other has become too descriptive and commonly evades issues of epistemic judgement.", "Writing the Past attempts to reintroduce a normative dimension to knowledge practices in archaeology, especially in relation to archaeological practice further down the ‘assembly line’ in the production of published texts, where archaeological knowledge becomes most stabilized and is widely disseminated. By exploring the composition of texts in archaeology and the relation between their structural, performative characteristics and key epistemic virtues, this book aims to move debate in both knowledge and writing practices in a new direction.", "Although this book will be of particular interest to archaeologists, the argument offered has relevance for all academic disciplines concerned with how knowledge production and textual composition intertwine."


<Text textformat="05">
<p>How do archaeologists make knowledge? Debates in the latter half of the twentieth century revolved around broad, abstract philosophies and theories such as positivism and hermeneutics which have all but vanished today. By contrast, in recent years there has been a great deal of attention given to more concrete, practice-based study, such as fieldwork. But where one was too abstract, the other has become too descriptive and commonly evades issues of epistemic judgement.</p>
<p>Writing the Past attempts to reintroduce a normative dimension to knowledge practices in archaeology, especially in relation to archaeological practice further down the ‘assembly line’ in the production of published texts, where archaeological knowledge becomes most stabilized and is widely disseminated. By exploring the composition of texts in archaeology and the relation between their structural, performative characteristics and key epistemic virtues, this book aims to move debate in both knowledge and writing practices in a new direction.</p>
<p>Although this book will be of particular interest to archaeologists, the argument offered has relevance for all academic disciplines concerned with how knowledge production and textual composition intertwine.</p>
</Text>

  <Text textformat="05">
  <p>How do archaeologists make knowledge? Debates in the latter half of the twentieth century revolved around broad, abstract philosophies and theories such as positivism and hermeneutics which have all but vanished today. By contrast, in recent years there has been a great deal of attention given to more concrete, practice-based study, such as fieldwork. But where one was too abstract, the other has become too descriptive and commonly evades issues of epistemic judgement.</p>
  <p>Writing the Past attempts to reintroduce a normative dimension to knowledge practices in archaeology, especially in relation to archaeological practice further down the ‘assembly line’ in the production of published texts, where archaeological knowledge becomes most stabilized and is widely disseminated. By exploring the composition of texts in archaeology and the relation between their structural, performative characteristics and key epistemic virtues, this book aims to move debate in both knowledge and writing practices in a new direction.</p>
  <p>Although this book will be of particular interest to archaeologists, the argument offered has relevance for all academic disciplines concerned with how knowledge production and textual composition intertwine.</p>
  </Text>



  string = "<p>blah blah </p><p>blah</p>"

html = Nokogiri::HTML("<!DOCTYPE html><html>#{string}</html>")
html.xpath("//*").map(&:name).uniq
html.xpath("//*").map(&:attributes).uniq

string.scan(/<[^\/].*?>/).uniq



string = "<p>blah blah </p><p>bl<br /><font color=red>Save 20%</font>ah</p>"

recommended = %w[p br strong em b i cite ul ol li sub sup dl dd dt ruby rb rp rt]
html.xpath("//*").map(&:name).uniq - recommended

not_recommended = %w[h1 h2 h3 h4 h5 h6 table caption thead tbody tfoot tr th td colgroup col div span blockquote pre address dfn abbr q small samp code kbd var a img map area bdo hr]

not_allowed = %w[menu dir isindex basefont rfont strike u s center iframe noframes html head body title base meta link style script noscript object param applet form label input select optgroup option textarea fieldset legend button ins del header footer nav section article aside details summary figure figcaption meter progress time wbr mark bdi canvas audio video source embed track datalist keygen output command dialog]


class TestText
  def initialize(text)
    @text = text
  end

  def html_doc
   @_html_doc ||= Nokogiri::HTML::DocumentFragment.parse(text)
  end

  def html_doc_errors
    html_doc.errors
  end

  def xhtml
    html_doc.to_xhtml
  end

  def elements
    html_doc
  end
end




string = "<div>
<p>
  blah
</p>
</div>
"
html = Nokogiri::HTML("<!DOCTYPE html><html>#{string}</html>")
