psql -h bibliocloud-devtest.cpbq0avmzvqp.eu-west-1.rds.amazonaws.com -p 5432 -U QyBMEYxYPEKqsRFj bibliocloud_ex8z4U464po0ckGgLuDzVqAl




pg_restore --format=custom --no-acl --no-owner --use-list=bibliocloud_out.list -h bibliocloud-devtest.cpbq0avmzvqp.eu-west-1.rds.amazonaws.com -p 5432 -U QyBMEYxYPEKqsRFj -d bibliocloud_ex8z4U464po0ckGgLuDzVqAl  ~/Downloads/bibliocloud20160601101234.dump

3mAsupPpGjuqafjDcffBOg1Xam1fH4pBLxVw




select
  i.value imprint_name,
  i.code  imprint_code,
  w.id work_id,
  w.title work_title,
  w.main_bic_code,
  w.main_bisac_code,
  w.number_of_illustrations,
  b.edition,
  b.title book_title,
  b.product_form,
  b.pub_date,
  b.isbn,
  b.publishers_reference,
  b.pages_arabic book_pages,
  case b.product_form
    when 'DG' then b.epub_type_code
    else null
  end epub_type_code,
  c.channel_name,
  mc.masterchannel_name,
  s.invoice_date,
  s.sale_quantity,
  s.sale_value,
  clients.client_name
from
  sales s
  join clients on clients.id = s.client_id
  join books b on b.id = s.book_id
  join channels c on c.id = s.channel_id
  join masterchannels mc on mc.id = c.masterchannel_id
  join works w on w.id = b.work_id
  left join imprints i on i.id = w.imprint_id;
