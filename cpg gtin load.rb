spreadsheet = Roo::Spreadsheet.open("/Users/david/Documents/Bibliocloud/Clients/CPG/From CPG/All products from EPI edited.xls")
spreadsheet = Roo::Spreadsheet.open("https://bibliocloudimages.s3-eu-west-1.amazonaws.com/temp/cpg-products.xls")
worksheet = spreadsheet.sheet(0)

missing_gtins = %w[
  846863002320
846863002740
846863003020
846863003280
846863003310
846863003440
846863003600
846863003730
846863003860
846863003990
846863004010
846863004140
846863009060
846863011650
846863017720
846863017850
846863018840
846863019090
846863019120
846863019830
846863019960
846863020140
846863020270
846863021420
846863021680
846863021840
846863022120
846863022250
]
client = Client.is_cpg

ActiveRecord::Base.transaction do
  work_type = client.work_types.where(name: "Non-book product").first_or_create

  worksheet.parse(headers: true, clean: true)[1..-1].select{|row| row.fetch("GTIN").to_i.to_s.in? missing_gtins }.each do |row|
    gtin =                              row.fetch("GTIN").to_i.to_s
    registrants_internal_reference =    row.fetch("In-house ref.").to_s
    title =                             row.fetch("Title")&.squish.presence
    alternative_title =                 row.fetch("Alternative title for work")&.squish.presence
    subtitle =                          row.fetch("Subtitle")&.squish.presence
    product_form =                      row.fetch("Product form")&.squish.presence
    imprint_name =                      row.fetch("Imprint")&.squish.presence
    page_count =                        row.fetch("Pages").presence
    product_height_in =                 row.fetch("height").presence
    product_width_in  =                 row.fetch("width").presence
    current_usd_exctax_consumer_price = row.fetch("Price").presence
    main_description =                  row.fetch("Description").presence
    keywords =                          (row.fetch("Keywords").presence || "").split(",").map{|k| k&.squish.presence}.compact

    imprint        = client.imprints.find_by!(value: imprint_name) if imprint_name
    epub_type_code = "002" if product_form == "DG"

    work =
      client.works.where(
        imprint:                        imprint,
        registrants_internal_reference: registrants_internal_reference,
        title:                          title,
        #alternative_titles:             alternative_title,
        subtitle:                       subtitle,
        keywords:                       keywords,
        work_type:                      work_type
      ).first_or_create

    book =
      work.books.create(
        title:                             title,
        subtitle:                          subtitle,
        gtin:                              gtin,
        product_form:                      product_form,
        epub_type_code:                    epub_type_code,
        product_height_in:                 product_height_in,
        product_width_in:                  product_width_in,
        current_usd_exctax_consumer_price: current_usd_exctax_consumer_price,
        publishing_status:                 Book::PUB_STATUS_ACTIVE,
        pub_date:                          Date.civil(2000,1,1)
      )

    work.marketingtexts.main_description.create(pull_quote: main_description)
    book.marketingtexts = work.marketingtexts
  end

  # raise ActiveRecord::Rollback
end ; 0
