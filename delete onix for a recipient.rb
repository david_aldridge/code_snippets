client    = Client.is_iop
recipient = client.contacts.is_ingram

recipient.digital_assets.each {|da| da.destroy! }

client.contacts.is_ingram.
  contact_digital_asset_transfer_templates.first.
  export_batches.each {|eb| eb.destroy! }

ContactDigitalAssetTransfer.joins(digital_asset: :recipient).where(contacts: {identifier: "ingram"}).count
