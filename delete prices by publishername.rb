 Client.is_liverpool.publishernames.last.books.is_paperback.flat_map {|b| b.prices.usd.consumer.where(Price.arel_table[:created_at].lt(Date.parse("2018-04-17"))) }

Client.is_liverpool.
  publishernames.last.
  books.is_paperback.each do |b|
    b.prices.
      usd.consumer.
      where(
        Price.arel_table[:created_at].lt(Date.parse("2018-04-17"))
       ).each(&:destroy)
  end




PaperTrail::Version.where(client_id: Client.is_liverpool.id, item_type: "Price", event: "destroy", whodunnit: nil).where("created_at > ?", Time.now - 1.hours).map(&:reify).each(&:save)