client = Client.by_key("zedbooks")

Rails.logger.level = 1
client.books.each_with_object(Hash.new(0)){|b, h| h[b.measurement_string] += 1}
client.books.where.not(:product_form => "DG").each_with_object(Hash.new(0)){|b, h| h[[b.product_form,b.measurement_string,b.proprietary_format_description_id]] += 1}

Rails.logger.level = 1
pfd = {"BB" => client.proprietary_format_descriptions.find_by(:name => "Royal 234x156 Pb").id,
       "BC" => client.proprietary_format_descriptions.find_by(:name => "Royal 234x156 Hb").id}

Client.by_key("lup").books.select{|b| b.measurement_string == "234 x 156mm".freeze}.each do |b|
  b.proprietary_format_description_id = pfd[b.product_form]
  b.save! if b.changed?
end



234mm x 153mm

"BB", "234 x 156mm", nil]=>66,
"BC", "234 x 156mm", nil]=>62,
"BC", nil, nil]=>1213,
"BB", nil, nil]=>954,
"BB", nil, 1206]=>5,
"BC", "216 x 138mm", nil]=>369,
"BB", "198 x 129mm", nil]=>69,
"BC", nil, 256]=>6,
"BB", "216 x 138mm", nil]=>331,
"BC", "216 x 138mm", 256]=>3,
"BB", "216 x 138mm", 256]=>1,
"BC", "198 x 126mm", nil]=>8,
"BB", "198 x 126mm", nil]=>10,
"BB", "216 x 135mm", nil]=>61,
"BC", "198 x 129mm", nil]=>77,
"BC", "216 x 135mm", nil]=>60,
"BC", "234 x 153mm", nil]=>13,
"BB", "234 x 153mm", nil]=>11,
"BB", "2160 x 138mm", nil]=>2,
"BC", "2160 x 138mm", nil]=>1,
"BC", "218 x 138mm", nil]=>1,
"BB", "218 x 138mm", nil]=>1,
"BC", "190 x 129mm", nil]=>1,
"BB", "246 x 138mm", nil]=>1,
"BB", "216 x 139mm", nil]=>1,
"BC", "214 x 138mm", nil]=>1,
"BB", "138 x 129mm", nil]=>1,
"BB", "198 x 125mm", nil]=>1,
"BC", "354 x 156mm", nil]=>1,
"BC", nil, 235]=>1}
