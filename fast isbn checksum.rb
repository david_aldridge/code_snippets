tkersey/isbn

def gem_1(isbn)
  (10 - (isbn.split(//).zip([1,3]*6).inject(0) {|s,n| s += n[0].to_i * n[1]} % 10))
end

zapnap/isbn_validation

def gem_2(isbn)
  isbn_values = isbn.upcase.gsub(/\ |-/, '').split('')
  sum = 0
  isbn_values.each_with_index do |value, index|
    multiplier = (index % 2 == 0) ? 1 : 3
    sum += multiplier * value.to_i
  end
  result = (10 - (sum % 10))
  result = 0 if result == 10
end

techwhizbang/faker-isbn

def gem_3(isbn13)
  isbn_array = Array.new
  isbn_13.split(//).each { |digit| isbn_array << digit.to_i }
  dot_product = [isbn_array, [1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3]].transpose.inject(0) { |result, pair| result += pair[0]*pair[1]; result }
  check_digit = 10 - (dot_product % 10)
  check_digit = 0 if check_digit == 10
  check_digit.to_s
end

hakanensari/bookland

def gem_4(raw)
  WEIGHTS = ([1, 3] * 6).freeze
  digits = raw.split('').digits[0...-1]
  sum = digits.map(&:to_i).zip(WEIGHTS).reduce(0) { |a, (i, j)| a + i * j }
    ((10 - sum % 10) % 10).to_s
  end
end


EmmanuelOga/risbn

def gem_5(isbn)
  isbn[0..-2].split("")
  sum = 0
  digits.each_with_index do |value, index|
    multiplier = (index % 2 == 0) ? 1 : 3
    sum += multiplier * value.to_i
  end
  result = 10 - (sum % 10)
  result = 0 if result == 10
  result == checksum
end

tylerjohnst/bookworm

def gem_6(identifier)
  identifier, sum = identifier.rjust(13,"978")[/(.+)\w/,1], 0

  12.times do |index|
    digit = identifier[index].to_i
    sum += index.even? ? digit : digit * 3
  end

  checksum = (10 - sum % 10)
  checksum == 10 ? '0' : checksum.to_s
end


asaaki/sjekksum

digits = number.to_s.scan(/\d/).map{ |b| b.chr.to_i }

      sum    = digits.map.with_index do |digit, idx|
        idx.odd? ? (digit * 3) : digit
      end.reduce(&:+)

(10 - sum % 10) % 10








isbn = "9780306406150"

def method0(isbn)
  base = (isbn.length == 13 ? '' : '978') + isbn[0..-2]

  products = base.each_char.each_with_index.map do |chr, i|
    chr.to_i * (i % 2 == 0 ? 1 : 3)
  end

  remainder = products.inject(0) {|m, v| m + v} % 10
  (remainder == 0 ? 0 : 10 - remainder).to_s
end

def method1(isbn)
  nums = isbn.chars.map(&:to_i)
  sum = 3 * (nums[1] + nums[3] + nums[5] + nums[7] + nums[9] + nums[11]) +
        nums[0] + nums[2] + nums[4] + nums[6] + nums[8] + nums[10] + nums[12]
  (10 - sum % 10).to_s[-1]
end

def method2(isbn)
  nums = isbn.chars
  sum = 3 * (nums[1].to_i + nums[3].to_i + nums[5].to_i + nums[7].to_i + nums[9].to_i + nums[11].to_i) +
        nums[0].to_i + nums[2].to_i + nums[4].to_i + nums[6].to_i + nums[8].to_i + nums[10].to_i + nums[12].to_i
  (10 - sum % 10).to_s[-1]
end

def method2_5(isbn)
  sum = (
          isbn[1].to_i +
          isbn[3].to_i +
          isbn[5].to_i +
          isbn[7].to_i +
          isbn[9].to_i +
          isbn[11].to_i
        ) * 3 +
        isbn[0].to_i +
        isbn[2].to_i +
        isbn[4].to_i +
        isbn[6].to_i +
        isbn[8].to_i +
        isbn[10].to_i +
        isbn[12].to_i
  (10 - sum % 10).to_s[-1]
end

def method3(isbn)
  nums = isbn.chars
  sum = nums.map!.with_index{|x,i| i.odd? ? x.to_i*3 : x.to_i }.reduce(&:+)
  (10 - sum % 10).to_s[-1]
end

def method4(isbn)
  nums = isbn.chars
  sum = nums.map.with_index{|x,i| i.odd? ? x.to_i*3 : x.to_i }.reduce(&:+)
  (10 - sum % 10).to_s[-1]
end

def method5(isbn)
  sum = isbn.each_char.with_index.inject(0){|sum, (x,i)| sum + (i.odd? ? x.to_i*3 : x.to_i )}
  (10 - sum % 10).to_s[-1]
end

def method6(isbn)
  sum=0
  (0..isbn.size-1).each_with_index{|n,i| sum += isbn[n].to_i * (i % 2 == 0 ? 1 : 3) }
  (10 - sum % 10).to_s[-1]
end


method0(isbn)
method1(isbn)
method2(isbn)
method2_5(isbn)
method3(isbn)
method4(isbn)
method5(isbn)
Checksum.call(isbn)

require 'benchmark'

n = 100000
Benchmark.bm(7) do |x|
  x.report("0")   { for i in 1..n; method0(isbn); end }
  x.report("1")   { for i in 1..n; method1(isbn); end }
  x.report("2")   { for i in 1..n; method2(isbn); end }
  x.report("2.5")   { for i in 1..n; method2_5(isbn); end }
  x.report("3")   { for i in 1..n; method3(isbn); end }
  x.report("4")   { for i in 1..n; method4(isbn); end }
  x.report("5")   { for i in 1..n; method5(isbn); end }
  x.report("6")   { for i in 1..n; method6(isbn); end }
  x.report("Rust"){ for i in 1..n; Checksum.call(isbn); end }
end ; ""


isbn = "0306406150"

  def isbn_10_checksum0(isbn)
    base = isbn.length == 13 ? isbn[3..-2] : isbn[0..-2]

    products = base.each_char.each_with_index.map do |chr, i|
      chr.to_i * (10 - i)
    end

    remainder = products.inject(0) {|m, v| m + v} % 11
    case remainder
      when 0
        0
      when 1
        'X'
      else
        11 - remainder
    end.to_s
  end


  def isbn_10_checksum1(isbn)
    base = isbn.length == 13 ? isbn[3..-2] : isbn[0..-2]

    sum =
      base[0].to_i * 10 +
      base[1].to_i *  9 +
      base[2].to_i *  8 +
      base[3].to_i *  7 +
      base[4].to_i *  6 +
      base[5].to_i *  5 +
      base[6].to_i *  4 +
      base[7].to_i *  3 +
      base[8].to_i *  2

    remainder = sum % 11
    case remainder
      when 0
        "0"
      when 1
        "X"
      else
        (11 - remainder).to_s
    end
  end


require 'benchmark'

n = 100000
Benchmark.bm(7) do |x|
  x.report("old")   { for i in 1..n; isbn_10_checksum0(isbn); end }
  x.report("new")   { for i in 1..n; isbn_10_checksum1(isbn); end }
end ; ""
