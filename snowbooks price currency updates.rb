£0.015 per unit per month



pp Client.is_zed.stock_availabilities.where("in_stock_qty > 0").where(:as_of_date => Date.today - 1.months).map{ |s| [s.in_stock_qty, (s.in_stock_qty * 0.015 * 12).round(2) ] }.sort


pp Book.find(72903).stock_availabilities.order(:as_of_date).map{ |s| [s.as_of_date, s.in_stock_qty, s.in_stock_qty * 0.015 * 12 ]}




Rails.logger.level=1
Client.is_snowbooks.books.epubs.forthcoming_or_active.each{|b| b.update_attributes(:current_usd_exctax_consumer_price => 2.95) } ; 0


Client.is_canelo.client_currencies.each do |cc|
  new_cc = cc.dup
  new_cc.user_id = nil
  new_cc.client_id = 1
  new_cc.save!
end

price_map =
{  4.99 => {current_eur_inctax_consumer_price:   9.99 ,current_usd_exctax_consumer_price:   9.95, current_cad_exctax_consumer_price:    9.95 , current_aud_exctax_consumer_price:    9.95 , current_nzd_exctax_consumer_price:   9.95 , current_inr_exctax_consumer_price:    399 },
   6.99 => {current_eur_inctax_consumer_price:  12.99 ,current_usd_exctax_consumer_price:  12.95, current_cad_exctax_consumer_price:   15.95 , current_aud_exctax_consumer_price:   15.95 , current_nzd_exctax_consumer_price:  15.95 , current_inr_exctax_consumer_price:    449 },
   7.99 => {current_eur_inctax_consumer_price:  14.99 ,current_usd_exctax_consumer_price:  14.95, current_cad_exctax_consumer_price:   19.95 , current_aud_exctax_consumer_price:   19.95 , current_nzd_exctax_consumer_price:  19.95 , current_inr_exctax_consumer_price:    649 },
   8.99 => {current_eur_inctax_consumer_price:  15.99 ,current_usd_exctax_consumer_price:  15.95, current_cad_exctax_consumer_price:   19.95 , current_aud_exctax_consumer_price:   19.95 , current_nzd_exctax_consumer_price:  19.95 , current_inr_exctax_consumer_price:    799 },
   9.99 => {current_eur_inctax_consumer_price:  19.99 ,current_usd_exctax_consumer_price:  19.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:    899 },
  11.99 => {current_eur_inctax_consumer_price:  24.99 ,current_usd_exctax_consumer_price:  24.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:    999 },
  12.99 => {current_eur_inctax_consumer_price:  24.99 ,current_usd_exctax_consumer_price:  24.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:   1299 },
  14.95 => {current_eur_inctax_consumer_price:  24.99 ,current_usd_exctax_consumer_price:  24.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:   1199 },
  14.99 => {current_eur_inctax_consumer_price:  24.99 ,current_usd_exctax_consumer_price:  24.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:   1199 },
  15.00 => {current_eur_inctax_consumer_price:  24.99 ,current_usd_exctax_consumer_price:  24.95, current_cad_exctax_consumer_price:   32.95 , current_aud_exctax_consumer_price:   32.95 , current_nzd_exctax_consumer_price:  32.95 , current_inr_exctax_consumer_price:   1299 },
  19.95 => {current_eur_inctax_consumer_price:  29.99 ,current_usd_exctax_consumer_price:  29.95, current_cad_exctax_consumer_price:   39.95 , current_aud_exctax_consumer_price:   39.95 , current_nzd_exctax_consumer_price:  39.95 , current_inr_exctax_consumer_price:   1699 },
  19.99 => {current_eur_inctax_consumer_price:  29.99 ,current_usd_exctax_consumer_price:  29.95, current_cad_exctax_consumer_price:   39.95 , current_aud_exctax_consumer_price:   39.95 , current_nzd_exctax_consumer_price:  39.95 , current_inr_exctax_consumer_price:   1799 },
  25.00 => {current_eur_inctax_consumer_price:  39.99 ,current_usd_exctax_consumer_price:  39.95, current_cad_exctax_consumer_price:   49.95 , current_aud_exctax_consumer_price:   49.95 , current_nzd_exctax_consumer_price:  49.95 , current_inr_exctax_consumer_price:   1999 },
  30.00 => {current_eur_inctax_consumer_price:  49.99 ,current_usd_exctax_consumer_price:  49.95, current_cad_exctax_consumer_price:   54.95 , current_aud_exctax_consumer_price:   54.95 , current_nzd_exctax_consumer_price:  54.95 , current_inr_exctax_consumer_price:   2499 },
  40.00 => {current_eur_inctax_consumer_price:  69.99 ,current_usd_exctax_consumer_price:  69.95, current_cad_exctax_consumer_price:   99.95 , current_aud_exctax_consumer_price:   99.95 , current_nzd_exctax_consumer_price:  99.95 , current_inr_exctax_consumer_price:   3199 },
 100.00 => {current_eur_inctax_consumer_price: 199.99 ,current_usd_exctax_consumer_price: 199.95, current_cad_exctax_consumer_price:  249.95 , current_aud_exctax_consumer_price:  249.95 , current_nzd_exctax_consumer_price: 249.95 , current_inr_exctax_consumer_price:   8999 }
 }

books = Client.is_snowbooks.books.where(product_form: ['BB','BC']).forthcoming_or_active.includes(:prices) ; 0

price_map.each do |gbp_price, other_prices|
  books.select{|b| b.current_gbp_inctax_consumer_price == gbp_price}.each do |book|
    other_prices.each do |method, price|
      book.send("#{method}=".to_sym, price)
    end
  end
end ; 0

