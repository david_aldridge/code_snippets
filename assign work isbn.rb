client = Client.is_mcgill_queens

client.works.where(identifying_isbn13: nil).count

Rails.logger.level=2

ActiveRecord::Base.transaction do
  client.
    works.
    joins(:books).
    merge(Book.hardback).
    where(identifying_isbn13: nil).
    uniq.each do |work|
      isbn = work.books.hardback.isbn_assigned.order(:pub_date, :id).pluck(:isbn).first
      next unless isbn

      work.update(
        identifying_isbn13: isbn
      )
    end
end ; 0

ActiveRecord::Base.transaction do
  client.
    works.
    joins(:books).
    merge(Book.paperback).
    where(identifying_isbn13: nil).
    uniq.each do |work|
      isbn = work.books.paperback.isbn_assigned.order(:pub_date, :id).pluck(:isbn).first
      next unless isbn

      work.update(
        identifying_isbn13: isbn
      )
    end
end ; 0

ActiveRecord::Base.transaction do
  client.
    works.
    joins(:books).
    merge(Book.paperback).
    where(identifying_isbn13: nil).
    uniq.each do |work|
      isbn = work.books.isbn_assigned.order(:product_form, :pub_date, :id).pluck(:isbn).first
      next unless isbn

      work.update(
        identifying_isbn13: isbn
      )
    end
end ; 0

client.books.joins(:work).where(works: {identifying_isbn13: nil}).group(:product_form).count
