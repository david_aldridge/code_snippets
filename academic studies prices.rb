prices = {
  9.99.to_d => 9.99,
 10.99.to_d => 10.99,
 11.99.to_d => 11.99,
 14.99.to_d => 14.99,
 16.99.to_d => 15.99,
 19.00.to_d => 17.99,
 19.95.to_d => 18.99,
 19.99.to_d => 18.99,
 21.00.to_d => 19.99,
 21.95.to_d => 20.99,
 22.00.to_d => 20.99,
 22.95.to_d => 21.99,
 23.95.to_d => 22.99,
 25.00.to_d => 23.99,
 27.00.to_d => 25.99,
 29.00.to_d => 26.99,
 29.95.to_d => 27.99,
 32.00.to_d => 29.99,
 34.00.to_d => 31.99,
 34.95.to_d => 32.99,
 35.00.to_d => 32.99,
 39.00.to_d => 36.99,
 39.95.to_d => 36.99,
 39.99.to_d => 36.99,
 42.00.to_d => 38.99,
 45.00.to_d => 41.99,
 49.95.to_d => 46.99,
 99.00.to_d => 90.99,
109.00.to_d => 99.99,
119.00.to_d => 109.99,
128.99.to_d => 118.99,
129.00.to_d => 118.99,
139.00.to_d => 127.99,
139.99.to_d => 128.99,
159.00.to_d => 145.99
}

Client.is_academic_studies.books.epubs.map do |b|
  usd_price = b.current_usd_exctax_consumer_price
  gbp_price = prices.fetch(usd_price)
  b.update(current_gbp_inctax_consumer_price: gbp_price)
end