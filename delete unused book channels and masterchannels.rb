pp Client.is_vertebrate.contracts.where(id: [41697, 49803, 55466, 72466, 55571, 53992]).flat_map(&:books).flat_map(&:book_channels).map(&:created_at).map(&:to_date).sort.group_by(&:itself).transform_values(&:size)


ActiveRecord::Base.transaction do
  Client.is_vertebrate.contracts.where(id: [72173]).flat_map(&:books).each do |book|
    book.book_channels.reject do |bc|
      bc.royalty_specifier ||
      book.sales.where(channel: bc.channel).exists?
    end.each(&:destroy)

    book.book_masterchannels.reject do |bmc|
      bmc.royalty_specifier ||
      bmc.book_channels.exists?
    end.each(&:destroy)
  end
  # raise ActiveRecord::Rollback
end
