ContactDigitalAssetTransferTemplate.
  joins(digital_assets: :books).
  where(books: {work_id: 4}).
  group("ARRAY[contact_digital_asset_transfer_templates.id, books.id]").
  count