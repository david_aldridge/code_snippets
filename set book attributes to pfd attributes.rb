Client.is_jacaranda.books.joins(:proprietary_format_description).includes(:proprietary_format_description).each do |book|
  pfd = book.proprietary_format_description
  book.update(
    product_form:   pfd.product_form,
    epub_type_code: pfd.epub_type_code
  )
end