Client.
  where(
    DigitalAsset.
      where("digital_assets.client_id = clients.id").
      where("created_at > ?", Date.today - 1.week).
      exists
  ).
  where.not(
    DigitalAsset.
      where("digital_assets.client_id = clients.id").
      where("digital_assets.created_at > ?", Date.today - 1.year).
      joins(:recipient).
      where(contacts: {identifier: "bowker"}).
      exists
  ).
  where.not(
    DigitalAsset.
      where("digital_assets.client_id = clients.id").
      where("digital_assets.created_at > ?", Date.today - 1.year).
      joins(:recipient).
      where(contacts: {identifier: "ingram"}).
      exists
  ).map(&:to_s)
