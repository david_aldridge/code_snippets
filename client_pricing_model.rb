Liverpool
Leuven
Zed Books PT
SAS
IOP
Amber
Vertebrate
Lund
BDS
Pharma
UCL


clients = %w(liverpool
canelo
leuven
zed
sas
amber
institute_of_physics
vertebrate
lund
bds
pharma
ucl)

data = Client.where(identifier: clients).sort.map do |client|
  [
    client.identifier,
    client.works.size,
    client.works.joins(:books).merge(Book.considered_live_or_forthcoming).uniq.size,
    (client.works.joins(:books).merge(Book.considered_live_or_forthcoming).uniq.size.to_f / client.works.size * 100).round,
    client.books.size,
    client.books.considered_live_or_forthcoming.size,
    (client.books.considered_live_or_forthcoming.size.to_f / client.books.size * 100).round
  ]
end


require "terminal-table"

puts Terminal::Table.new(
headings: %w(Client Works (Live/forthcoming) % Products (Live/forthcoming) %),
rows: data
)


data = Client.where(identifier: clients).sort.map do |client|
  [
    client.identifier,
    client.works.size,
    client.works.joins(:books).merge(Book.considered_live_or_forthcoming).uniq.size,
    (client.works.joins(:books).merge(Book.considered_live_or_forthcoming).uniq.size.to_f / client.works.size * 100).round,
    client.books.size,
    client.books.considered_live_or_forthcoming.size,
    (client.books.considered_live_or_forthcoming.size.to_f / client.books.size * 100).round ,
    client.books.considered_live_or_forthcoming.count("distinct work_id::varchar||product_form"),
    (client.books.considered_live_or_forthcoming.count("distinct work_id::varchar||product_form").to_f / client.works.size * 100).round
  ]
end

puts Terminal::Table.new(
headings: %w(Client Works (Live/forthcoming) % Products (Live/forthcoming) % Forms %),
rows: data
)
