
class Trigrams
  def initialize(string)
    @data = string.downcase.chars.each_cons(3).map(&:join) + [string[0..1] , string[0] , string[-2..-1] , string[-1]]
  end

  def similarity(other)
    raise "not a trigram" unless other.is_a? Trigrams
    (data & other.data).size / (data | other.data).size.to_f
  end

  attr_reader :data
end

t1 = Trigrams.new("abcdfghi")
t2 = Trigrams.new("abcdefghi")

t1.similarity(t2)



class String
  def trigrams
    self.chars.each_cons(3).map(&:join) + [self[0..1] , self[0] , self[-2..-1] , self[-1]]
  end
end


keyword_string = "Celebrity;celebrities;celebrities;celebrity culture;fame;popular culture;reality TV;cult of personality;star power;fandom;stardom"
keyword_array = keyword_string.split(";")

keyword_pairs = keyword_array.combination(2).to_a

keyword_pairs.map do |a,b|
  [Trigrams.new(a).similarity(Trigrams.new(b)),a,b]
end.sort.reverse.uniq.map{|a,b,c| [b,c]}

pp _




