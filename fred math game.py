import random, math
print ("Let`s play a game. I will show an equation and you have to work it out and write your answer to the nearest hundredth.")
shortcut = input ("If you would like to use a shortcut type subtraction (or s), multiplication (or m), division (or d).")
guesses = 0
if shortcut == "s" or shortcut == "subtraction":
  guesses = 10
if shortcut == "m" or shortcut == "multiplication":
  guesses = 20
if shortcut == "d" or shortcut == "division":
  guesses = 30
random1 = 0
random2 = 0
answer = 0
retry = 0
while retry == "yes" or guesses < 10:
  guesses = 1
  random1 = random.randint(1,100)
  random2 = random.randint(1,100)
  guesses = guesses + 1
  print ("ROUND",guesses)
  print (random1,"+",random2)
  answer = int(input("What is the answer?"))
  if random1 + random2 != answer:
    print ("You got to round",guesses)
    print ("The correct answer was",random1+random2)
    retry = input ("Do you want to play again?")
  print ("That`s correct.")
while guesses >= 10 and guesses < 20:
  random1 = random.randint(1,100)
  random2 = random.randint(1,100)
  guesses = guesses + 1
  print ("ROUND",guesses)
  print (random1,"-",random2)
  answer = int(input ("What is the answer?"))
  if random1-random2 != answer:
    print ("You got to round",guesses)
    print ("The correct answer was", random1-random2)
    retry = input ("Do you want to play again?")
  print ("That`s correct")
while guesses >= 20 and guesses < 30:
  random1 = random.randint(1,100)
  random2 = random.randint(1,100)
  guesses = guesses + 1
  print ("ROUND",guesses)
  print (random1,"*",random2)
  answer = int(input ("What is the answer?"))
  if random1 * random2 != answer:
    print ("You got to round", guesses)
    print ("The correct answer was", random1*random2)
    retry = input ("Do you want to play again?")
  print ("That`s correct")
while guesses >= 30:
  random1 = random.randint(50,100)
  random2 = random.randint(1,50)
  guesses = guesses + 1
  print ("ROUND",guesses)
  print (random1,"/",random2)
  answer = (input ("What is the answer?"))
  add = round(random1/random2,2)
  if add == answer:
      print ("That is correct")
  else:
    print ("You got to round", guesses)
    print ("The correct answer was", (round(add,2)))
    retry = input ("Would you like to try again?")