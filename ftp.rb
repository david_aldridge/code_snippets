# -*- encoding : utf-8 -*-
class Ftp
  require 'net/ftp'
  require 'open-uri'

  attr_accessor :login_user    ,
                :login_password,
                :login_acct    ,
                :connect_host  ,
                :connect_port  ,
                :directory     ,
                :use_passive

  attr_reader :session, :templated,:ftp_template, :error, :ended_at
#reload!
#f = Ftp.new
#f.create_session({:login_user     => 'davidaldridge',
#                  :login_password => 'iiaamahs13',
#                  :connect_host   => 'localhost',
#                  :passive        => true       ,
#                  :directory      => "downloads"})
#f.connect
#f.login
#f.chdir


  def create_session(options = {})
    if options[:ftp_template]
      @ftp_template    = options[:ftp_template]
      @login_user      = @ftp_template.login_user
      @login_password  = @ftp_template.login_password
      @login_acct      = @ftp_template.login_acct
      @connect_host    = @ftp_template.connect_host
      @connect_port    = @ftp_template.connect_port
      @directory       = @ftp_template.directory
      @use_passive     = @ftp_template.use_passive
      @templated       = true
    else
      @login_user      = options[:login_user    ]
      @login_password  = options[:login_password]
      @login_acct      = options[:login_acct    ]
      @connect_host    = options[:connect_host  ]
      @connect_port    = options[:connect_port  ]
      @directory       = options[:directory     ]
      @use_passive     = options[:use_passive   ]
      @templated       = false
    end
    @session         = Net::FTP.new
  end

  def transfer(file)
    connect
    login
    set_passive if @use_passive
    change_dir  if @directory
    send(file)
    rescue *[IOError, SystemCallError, Net::FTPError] => e
      log_end(e.message)
      self.error    = e.message
      self.ended_at = Time.now
      self.save
  end

  def connect
    if defined? @connect_host and @connect_port
      @session.connect(@connect_host, @connect_port)
    elsif defined? @connect_host
      @session.connect(@connect_host)
    end
    last_response
  rescue *[IOError, SystemCallError, Net::FTPError] => e
    @error    = e.message
    false
  end


  def last_response
    @session.last_response
  end

  def login
    @session.login(@login_user,@login_password)
    last_response
  rescue *[IOError, SystemCallError, Net::FTPError] => e
    @error    = e.message
    false
  end

  def chdir(directory = nil)
    target_directory = directory || (defined? @directory) ? @directory : nil
    @session.chdir(target_directory) if target_directory
    last_response
  rescue Net::FTPError => e
    @error    = e.message
    false
  end

  def set_passive
    @session.passive
    last_response
  rescue Net::FTPError => e
    @error    = e.message
    false
  end

  def send(file)
    #log_start("Sending file")
    @session.putbinaryfile(file, remotefile = File.basename(file), Net::FTP::DEFAULT_BLOCKSIZE)
    #log_end(last_response)
  end


  def log_start(action)
      @action_number = (@action_number + 1 rescue 1)
      @started_at = Time.now if @action_number == 1
      @current_log = ftp_template_connection_logs.create(:action_number => @action_number,
                                                         :started_at    => @started_at    ,
                                                         :action        => action)
  end

  def log_end(response)
      @current_log.ended_at = Time.now
      @current_log.response = response
      @current_log.save
  end

end