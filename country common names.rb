    COMMON_NAMES = {
      "AE" => "UAE",
      "BO" => "Bolivia",
      "CD" => "Congo",
      "CS" => "Serbia and Montenegro",
      "FK" => "Falkland Islands",
      "FM" => "Micronesia",
      "IR" => "Iran",
      "KP" => "Korea, DPR",
      "LA" => "Laos",
      "MF" => "Saint Martin",
      "MK" => "Macedonia",
      "PM" => "Saint Pierre and Miquelon",
      "SX" => "Sint Maarten",
      "TW" => "Taiwan",
      "TZ" => "Tanzania",
      "UM" => "US Minor Outlying Islands",
      "US" => "USA",
      "VA" => "Vatican City",
      "VE" => "Venezuela"
    }.freeze

    private_constant :COMMON_NAMES

    def common_name
      COMMON_NAMES[code] || description
    end

    def common_name?
      code.in? COMMON_NAMES
    end
