client = Client.find_by_webname("Oberon Books")

c_prog_texts = c.channels.find_by_channel_name("Programme texts")

"UK sales"

 On royalties, I see from the sample Single Play contract that you provided the following royalty rates.

(i) On copies sold in the UK for the home market, 10% of the UK published price, unless the discount granted is greater than 50% of the UK published price, in which case 6.5% of the UK published price;
(ii) On the net price received by the Publisher on copies sold as programme texts, 10%;
(iii) On the net price received by the Publisher on copies sold for export from the UK, 7.5%;
(iv) On the net price received by the Publisher on copies sold electronically, 25%;

... and the following rights rates ...

(i) sale of sheets or quotation rights at 70% of the amount received;
(ii) grant of a license or sublicense of the right to publish outside the United Kingdom as an anthology or singly, at 80% of the amount received;
(iii) anthology, digest, digest book condensation and large print at 50% of the amount received;
(iv) Braille and non-commercial talking books for the handicapped at 0%; and
(v) photocopying and related rights at 75% of the amount received.

If we ca



uk_home     = { client_id: client.id,
                template: false,
                royalty_rate_base_pct: 10,
                royalty_rate_upper_limit_pct: 10,
                royalty_rate_lower_limit_pct: 6.5,
                price_basis: "Escalator (Add)",
                quantity_modifier_type: "None",
                date_modifier_type: "None",
                royalty_discount_escalators_attributes: {escalation_discount: 50 ,
                                                         royalty_percentage:  -3.5}}

programmes = {  client_id: client.id,
                template: false,
                royalty_rate_base_pct: 10,
                royalty_rate_upper_limit_pct: 10,
                royalty_rate_lower_limit_pct: 10,
                price_basis: "Discount",
                quantity_modifier_type: "None",
                date_modifier_type: "None"}

export     = {  client_id: client.id,
                template: false,
                royalty_rate_base_pct: 7.5,
                royalty_rate_upper_limit_pct: 7.5,
                royalty_rate_lower_limit_pct: 7.5,
                price_basis: "Discount",
                quantity_modifier_type: "None",
                date_modifier_type: "None"}

epub       = {  client_id: client.id,
                template: false,
                royalty_rate_base_pct: 25,
                royalty_rate_upper_limit_pct: 25,
                royalty_rate_lower_limit_pct: 25,
                price_basis: "Discount",
                quantity_modifier_type: "None",
                date_modifier_type: "None"}


c.books.each do |b|
  if b.product_form == "DG"
    b.create_royalty_specifier(EPUB)
  end
end

programme_texts_channel = c.channels.find_by_channel_name("Programme texts")


book = c.books.is_ebook.first


channel = c.channels.find_by_channel_name("Programme texts")

book_channel = BookChannel.find(BookChannel.ensure_exists(book.id, programme_texts_channel.id))

book_channel.create_royalty_specifier(epub)

bmc= BokMasterchannel.


