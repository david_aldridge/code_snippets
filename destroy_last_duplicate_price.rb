Price.where(id:
  Client.is_pharma.
    prices.
    group(
      :book_id,
      :currency_code,
      :price_type,
      :onix_price_type_qualifier_id
    ).
    having("count(*) > 1").
    maximum(:id).
    values
  ).
  destroy_all
