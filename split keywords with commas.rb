Work.no_touching do
  Work.joins(:client).merge(Client.active).each do |w|
    next unless w.keyword_array.size == 1
    new_keyword_array = w.keyword_array.flat_map do |kw|
      if kw.split(",").size > 2
        kw.split(",").map(&:strip)
      else
        kw
      end
    end
    if new_keyword_array != w.keyword_array
      w.keywords= new_keyword_array.join(";")
      w.save
    end
  end
end ; 0
