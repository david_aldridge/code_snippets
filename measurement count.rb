Rails.logger.level = 1

client = Client.is_british_library

client.
  books.forthcoming_or_active.
  group(:product_height_mm,
  :product_width_mm,
  :product_form
  ).
  order(:product_height_mm,
  :product_width_mm,
  :product_form
  ).
  count.
  transform_keys{|x| x.map(&:to_s)}

pp _


client = Client.is_notre_dame

client.
  books.forthcoming_or_active.
  group(:product_height_in,
  :product_width_in,
  :product_form
  ).
  order(:product_height_in,
  :product_width_in,
  :product_form
  ).
  count.
  transform_keys{|x| x.map(&:to_s)}

pp _

"8.5", "5.5", "BB" =>4
"8.5", "5.5", "BC" =>2
"9.0", "6.0", "BB" =>55
"9.0", "6.0", "BC" =>25
"9.21", "6.14", "BB" =>1
"9.21", "6.14", "BC" =>3
"9.25", "5.5", "BC" =>1
"10.0", "7.0", "BC" =>3
"", "", "BB" =>362
"", "", "BC" =>787



ActiveRecord::Base.transaction do
  client.
    proprietary_format_descriptions.
    where(product_form: %w(BB BC)).
    limit(5).
    each do |pfd|
    pfd.books = pfd.client.books.where(
      product_form: pfd.product_form,
      product_height_mm: pfd.product_height_mm,
      product_width_mm: pfd.product_width_mm
    )
  end
  raise ActiveRecord::Rollback
end


ActiveRecord::Base.transaction do
  Client.is_leuven.
    proprietary_format_descriptions.
    where(product_form: %w(BB BC)).
    limit(5).
    each do |pfd|
    pfd.client.books.where(
      product_form:      pfd.product_form,
      product_height_mm: pfd.product_height_mm,
      product_width_mm:  pfd.product_width_mm
    ).update_all(proprietary_format_description_id: pfd.id)
  end
  #raise ActiveRecord::Rollback
end



hh = Client.is_leuven.books.where.not(product_form: nil).
each_with_object(Hash.new(0)) do |book , h|
 h[FormatName.new(book).call] += 1
end

class FormatName
  def initialize(book)
    @book = book
  end

  def call
    return unless valid?
    "#{format_name} #{product_height_mm.to_i.to_s} x #{product_width_mm.to_i.to_s}"
  end

  private

  attr_reader :book

  def valid?
    product_height_mm && product_width_mm && format_name
  end

  def format_name
    case product_form
    when "BB" then "Hardback"
    when "BC" then "Paperback"
    end
  end

  delegate :product_height_mm,
           :product_width_mm,
           :product_form,
           to: :book
end
