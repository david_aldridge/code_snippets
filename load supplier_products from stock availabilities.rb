
ary =
Client.
  is_pharma.
  stock_availabilities.
  where(as_of_date: Client.is_pharma.stock_availabilities.maximum(:as_of_date)).
  map do |sa|
          StockAvailabilityToSupplierProduct.
            new(sa).
            call.
            attributes.
            compact
end

ary.each do |supplier_product_attributes|
  SupplierProductUpdateService.new(attributes: supplier_product_attributes).call
end
