url = "https://bibliocloudimages.s3-eu-west-1.amazonaws.com/onix/panmac.xlsx"

file = Roo::Spreadsheet.open(url, extension: :xlsx)

rows = file.sheet(0).parse(headers: true)

client =  Client.is_panmac

rows.each { |row| PanMacRow.new(client, row).call }

class PanMacRow
  def initialize(client, row)
    @row    = row
    @client = client
  end

  attr_reader :row, :client

  def call
    marketingtexts
    book.update(
      pages_arabic:      row["Pages"],
      product_height_mm: row["Length"],
      product_width_mm:  row["Width"]
    )
  end

  def book_attributes
    {}.
    merge(pages_arabic: row["Pages"]).
    merge(measurements)
  end

  def measurements
    if row["Binding"].include? "Hard"
      {
        product_height_mm:   row["Length"] + 6,
        product_width_mm:    row["Width"] + 5,
        page_trim_height_mm: row["Length"],
        page_trim_width_mm:  row["Width"]
      }
    else
      {
        product_height_mm:   row["Length"],
        product_width_mm:    row["Width"],
        page_trim_height_mm: row["Length"],
        page_trim_width_mm:  row["Width"]
      }
    end
  end

  def book
    @_book ||= client.books.find_by(isbn: row["ISBN"])
  end

  def work
    @_work ||= book.work
  end

  def marketingtexts
    work.marketingtexts.table_of_contents.create(pull_quote: row["Table of Contents"])
    work.marketingtexts.main_description.create(pull_quote: row["Book Description"]) unless row["Book Description"].blank?
    work.books.each {|book| book.marketingtexts = work.marketingtexts }
  end

end
