client = Client.is_libri

ActiveRecord::Base.transaction do
  client.marketingtexts.where(legacy_code: "03").uniq.each do |long_description|
    if long_description.work.marketingtexts.where(main_type: "01").any?
      puts "Error: Already exists #{long_description.work_id}"
      next
    end
    if long_description.pull_quote.size > 6000
      puts "Error: Too long #{long_description.work_id}"
      next
    end
    puts "Changing!"
    long_description.main_type   = "02"
    long_description.text_type   = "03"
    long_description.legacy_code = "01"
    long_description.save
  end
  # raise ActiveRecord::Rollback
end
