
reload!
client      = Client.is_mcgill_queens
filename    = "https://bibliocloudimages.s3-eu-west-1.amazonaws.com/mcgill_metadata.xlsx"

spreadsheet = Roo::Spreadsheet.open(filename, extension: :xlsx)
rows        = spreadsheet.sheet(0).parse(headers: true) ; 0
hash        = rows[1..-1].
              map{|row| [row.fetch("isbn-13"), row.fetch("FullAuthorBiography")]}.
              reject{|isbn, bio| bio.blank? || bio == "CA"}.
              to_h ; 0

ActiveRecord::Base.transaction do
  Work.no_touching do
  client.works.joins(:books).where(books: {isbn: hash.keys}).uniq.each do |work|
      bio = hash.slice(*work.books.pluck(:isbn)).values.sort_by(&:size).last
      work.biographical_notes.destroy_all
      note = work.biographical_notes.create(pull_quote: bio)
      note.books = work.books
    end
  end
end ; 0


works_with_1_contact  = client.works.joins(:workcontacts).group("works.id").having("count(*) = 1")
works_with_1_contact_ids = works_with_1_contact.pluck(:id)
contacts_without_bios = client.contacts.where(:biographical_note => nil)
contacts_without_bios.size
contacts_with_work_bios = contacts_without_bios.includes(works: :biographical_notes).each_with_object({}) do |c,h|
  h[c] = c.works.where(id: works_with_1_contact_ids).flat_map(&:biographical_notes).map(&:pull_quote).uniq
end
contacts_with_work_bios.values.map(&:size).group_by(&:itself).transform_values(&:size)

contacts_with_work_bios.select{|contact, bios| bios.size.in?((1..2))}.each do |contact, bios|
  contact.update(biographical_note: bios.sort_by(&:size).last)
end
