(Client.is_liverpool.works - 
 Client.is_liverpool.seriesnames.find_by(title_without_prefix: "Littman Library of Jewish Civilization").works).
 select(&:subtitle).each do |work|
   work.books.update_all(subtitle: work.subtitle)
end
