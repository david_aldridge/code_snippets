create schema report;

drop view report.works;
drop view report.schedules;
drop view report.tasks;

create view
  report.works
as
select
  client_id                    ,
  id                           ,
  title                        ,
  subtitle                     ,
  registrants_internal_reference publishers_reference,
  imprint_id                   ,
  identifying_isbn13           ,
  identifying_doi              ,
  audience_description         ,
  keywords                     ,
  online_authorship_description,
  edition_type_codes           ,
  salesforce_uid
from
  works;

create view
  report.schedules
as
select
  id              ,
  client_id       ,
  name            ,
  schedulable_type,
  schedulable_id  ,
  template        ,
  updated_at
from
  schedules;

create view
  report.tasks
as
select
  id                 ,
  client_id          ,
  schedule_id        ,
  name               ,
  duration           ,
  description        ,
  updated_at         ,
  level              ,
  status             ,
  start                        as start_date,
  "end"                        as end_date  ,
  "startIsMilestone"           as start_is_milestone,
  "endIsMilestone"             as end_is_milestone,
  string_to_array(depends,',') as depends,
  progress       ,
  index_number   ,
  benchmark                    as benchmark_date
from
  tasks;
