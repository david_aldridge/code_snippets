client = Client.is_bds


client.contacts.
  includes(
    :professional_affiliations,
    :workcontacts,
    :addresses
  ).
  where(
    professional_affiliations: {id: nil}
  ).
  where.not(
    workcontacts: {id: nil}
  ).
  where.not(
    addresses: {organisation_name: nil}
  ).pluck(:id)



ActiveRecord::Base.transaction do
  client.contacts.
    includes(
      :professional_affiliations,
      :workcontacts,
      :addresses
    ).
    where(
      professional_affiliations: {id: nil}
    ).
    where.not(
      workcontacts: {id: nil}
    ).
    where.not(
      addresses: {organisation_name: nil}
    ).each do |contact|
    organisation_name = contact.addresses.where.not(addresses: {organisation_name: nil}).first.organisation_name

    organisation = FindContactByCorporateName.new(
      client: client,
      corporate_name: organisation_name,
      check_acronym_also: true,
      create: false
    ).call

    ProfessionalAffiliation.create!(
      contributor_id: contact.id,
      organisation_id: organisation.value && organisation.value.id,
      organisation_name: organisation.value ? nil : organisation_name
    )
  end
end
