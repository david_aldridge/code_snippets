obj = Book.find(82936)

pp obj.class.reflect_on_all_associations.
             select{|assoc| assoc.options[:dependent].eql? :destroy && !!obj.send(assoc.name).presence}.
             map{|assoc| [assoc.options[:dependent], assoc.name]}.
             to_a

[:book_seriesnames, :bookcontacts, :bookmarketingtexts, :booksupportingresources, :prices, :supportingresources]

[ActiveRecord::Reflection::HasManyReflection,
  ActiveRecord::Reflection::ThroughReflection,
  ActiveRecord::Reflection::HasOneReflection,
  ActiveRecord::Reflection::BelongsToReflection]
