"GBP 10.99 USD3.99 EUR4".
  scan(/([a-zA-Z]{3})([^a-zA-Z]+)/).
  map{|currency, price| [currency.delete(" ").upcase, price.to_f] }
