x = Client.
  includes(:request_logs).
  order("max(request_logs.created_at) nulls first").
  group("clients.id").
  maximum("request_logs.created_at").
  map do |client, max_request|
    [Client.find_by(id: client).try(:identifier) || client, max_request]
  end


  pp x
