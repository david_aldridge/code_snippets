bisacs =
{
"COM008000" => nil,
"COM009000" => "COM034000",
"COM020000" => "TEC071000",
"COM020010" => "TEC071000",
"COM020050" => "TEC071010",
"COM020090" => "TEC061000",
"COM021050" => "COM021000",
"COM022000" => "COM087020",
"COM035000" => "BUS059000",
"COM043060" => "COM043000",
"COM046060" => "COM046000",
"COM051020" => "COM051010",
"COM051050" => "COM051010",
"COM051080" => "COM051010",
"COM051090" => "COM051010",
"COM051100" => "COM051010",
"COM051130" => "COM051010",
"COM051140" => "COM051010",
"COM051290" => "COM051010",
"COM051420" => "COM051010",
"COM056000" => "COM005000",
"COM060070" => "COM060000",
"COM060080" => "COM060000",
"COM060090" => "COM051230",
"COM065000" => nil,
"COM069000" => "COM060000",
"COM084000" => "COM005000",
"COM086000" => "COM043020",
"REL037020" => "LAW119000",
}

matches = Work.where(main_bisac_code: bisacs.keys)
counter = matches.group(:main_bisac_code).count

matches.each do |work|
  work.update(main_bisac_code: bisacs.fetch(work.main_bisac_code))
end

matches = Work.where(main_bisac_code: bisacs.keys)
counter = matches.group(:main_bisac_code).count

matches = Work.where("bisac_general_codes && ARRAY[?::Text]", bisacs.keys)
counter = matches.group(:bisac_general_codes).count

counter.map do |codes, count|
  [
    codes,
    count,
    codes & bisacs.keys,
    (codes & bisacs.keys).map{|c| bisacs.fetch(c)}
  ]
end

ActiveRecord::Base.transaction do
  matches.each do |work|
    old_codes = work.bisac_general_codes
    old_codes = (old_codes - [work.main_bisac_code]) if work.main_bisac_code?
    new_codes =
      old_codes.map do |old_code|
        if old_code.in? bisacs.keys
          bisacs[old_code]
        else
          old_code
        end
      end.compact
    puts "#{work.id}: #{work.bisac_general_codes.join(":")}"
    puts "#{work.id}: #{new_codes.compact.uniq.join(":")}"
    work.bisac_general_codes = new_codes.compact.uniq-([work.main_bisac_code] || [])
    work.save

  end
  #raise ActiveRecord::Rollback
end
