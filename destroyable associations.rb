
  def self.restricted_associations
    reflect_on_all_associations.select do |assoc|
      dependent_option = assoc.options[:dependent]
      dependent_option == :restrict_with_error
    end
  end

  delegate :restricted_associations,
           to: :class

  def nondestroyable_associations
    restricted_associations.to_a.select do |assoc|
      return send(assoc.name).any? if send(assoc.name).respond_to?(:any?)
      !send(assoc.name)
    end
  end

  def can_destroy?
    nondestroyable_associations.empty?
  end
