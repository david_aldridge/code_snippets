Book.joins(:work).joins(:client).merge(Client.active).where("books.title != works.title").group(:client).count.transform_keys(&:to_s)


WorkSeriesname.where.not(number_within_series: nil).joins(work: [:client).group("client_name").count.transform_keys(&:to_s)

BookSeriesname.
  joins(book: {work: :work_seriesnames}).
  where("book_seriesnames.seriesname_id = work_seriesnames.seriesname_id").
  where("(work_seriesnames.year_of_annual is not null or work_seriesnames.number_within_series is not null) and
   coalesce(book_seriesnames.year_of_annual, work_seriesnames.year_of_annual or book_seriesnames.number_within_series = work_seriesnames.number_within_series)").
  pluck("book_seriesnames.year_of_annual, work_seriesnames.year_of_annual, book_seriesnames.number_within_series, work_seriesnames.number_within_series")

BookSeriesname.
  joins(book: {work: :work_seriesnames}).
  where("book_seriesnames.seriesname_id = work_seriesnames.seriesname_id").
  where("work_seriesnames.year_of_annual != book_seriesnames.year_of_annual").
  pluck("work_seriesnames.year_of_annual, book_seriesnames.year_of_annual")

BookSeriesname.
  joins(book: {work: :work_seriesnames}).
  where("book_seriesnames.seriesname_id = work_seriesnames.seriesname_id").
  where("work_seriesnames.number_within_series != book_seriesnames.number_within_series").
  pluck("work_seriesnames.number_within_series, book_seriesnames.number_within_series")


  where("(work_seriesnames.year_of_annual is not null or book_seriesnames.number_within_series is not null) and
   coalesce(book_seriesnames.year_of_annual, work_seriesnames.year_of_annual or book_seriesnames.number_within_series = work_seriesnames.number_within_series)").
  pluck("book_seriesnames.year_of_annual, work_seriesnames.year_of_annual, book_seriesnames.number_within_series, work_seriesnames.number_within_series")
