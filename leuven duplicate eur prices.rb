Client.is_leuven.prices.group(:price_type, :currency_code).count

Client.
  is_leuven.
  prices.
  where(
    price_type: "01",
    currency_code: "EUR").
  joins(:book).
  where(
    books: {
      id: Client.is_leuven.prices.where(price_type: "02", currency_code: "EUR").pluck(:book_id)
    }
  ).count



book_ids_with_dup_prices = Client.is_leuven.prices.eur.group(:book_id).having("count(*) > 1").count.keys

Client.
  is_leuven.
  prices.
  eur.
  where(book_id: Client.is_leuven.prices.eur.group(:book_id).having("count(*) > 1").count.keys).
  pluck(:book_id, :price_amount).
  group_by(&:first).
  values.
  map{|x| x.map(&:last)}.
  map{|x| x.map(&:to_f)}.
  reject{|x| x.first == x.last}.
