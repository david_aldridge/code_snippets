irb Liverpool::SendONIX.new

pp all_books.group_by{|b| b.publishername.id}.transform_values(&:size)

pp all_books.group_by{|b| b.seriesname_ids}.transform_values(&:size)

pp coop_books.group_by{|b| b.work.imprint.id}.transform_values(&:size)

pp limited_pdfs.group_by{|b| b.work.imprint.value}.transform_values(&:size)
pp limited_pdfs.group_by{|b| b.seriesnames.pluck(:title_without_prefix)}.transform_values(&:size)

pp bds_books.group_by{|b| b.seriesnames.pluck(:title_without_prefix)}.transform_values(&:size)

      nielsen_books.
        is_print.
        includes(work: :imprint).
        select do |book|
          book.imprint.id.in? COOP_IMPRINT_IDS
        end
