prefixes = [
"97807117",
"97809544767",
"97809545803",
"97809553308",
"9781910336",
"9781911067",
"97898899143"
]



Client.is_crimson.books.select{|b| prefixes.any?{|p| b.isbn.starts_with?(p)}}.map do |b|
  [
    b.pub_date,
    b.isbn,
    b.publishing_status,
    b.allow_onix_exports
  ]
end.sort



Client.is_crimson.books.select{|b| prefixes.any?{|p| b.isbn.starts_with?(p)}}.map do |b|
  [
    b.isbn,
    b.pub_date,
    b.publishing_status,
    b.allow_onix_exports
  ]
end.sort


