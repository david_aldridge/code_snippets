map {|b| [b.isbn, b.title[0..30], b.current_eur_inctax_consumer_price.to_s, b.pub_date, "http://app.bibliocloud.com/works/#{b.work_id}/edit"] }

require "terminal-table"

puts Terminal::Table.new(
headings: %w(ISBN Title EUR_price Pub_date URL),
rows: ebooks_without_agency_price
)
