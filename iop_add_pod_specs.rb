client  = Client.is_iop

client.contacts.find_by(identifier: Contact::LIGHTNING_SOURCE)

CreateStandardContacts.new(client: client, identifiers: [Contact::LIGHTNING_SOURCE]).call
CreateStandardContacts.new(client: client, identifiers: [Contact::LIGHTNING_SOURCE_ASR]).call

printer = client.contacts.is_lightning_source


client.books.considered_live_or_forthcoming.is_print.each do |book|
  book.print_on_demand_specs.
       where(printer: printer).
       first_or_create(
             currency_code:     'USD',
             for_all_suppliers: true,
             active:            book.pub_date <= Date.today)
end


client  = Client.is_iop
printer = client.contacts.is_lightning_source

ActiveRecord::Base.transaction do
  ProprietaryFormatDescription.last.books.is_print.each do |book|
    book.print_on_demand_specs.
        where(printer: printer).
        first_or_create(
              currency_code:     'USD',
              for_all_suppliers: true,
              active:            book.pub_date <= Date.today)
  end
end
