Book.where(:product_form => 'DG').count

Book.where(:product_form => 'DG').
 update_all(page_trim_height_mm: nil,
           page_trim_height_in: nil,
           page_trim_width_mm: nil,
           page_trim_width_in: nil,
           product_height_mm: nil,
           product_height_in: nil,
           product_width_mm: nil,
           product_width_in: nil,
           product_thickness_mm: nil,
           product_thickness_in: nil,
           unit_weight_gr: nil,
           unit_weight_oz: nil,
           spherical_diameter_mm: nil,
           spherical_diameter_in: nil,
           cylindrical_diameter_mm: nil,
           cylindrical_diameter_in: nil,
           sheet_height_mm: nil,
           sheet_height_in: nil,
           sheet_width_mm: nil,
           sheet_width_in: nil,
           rolled_sheet_package_side_mm: nil,
           rolled_sheet_package_side_in: nil,
           main_content_page_count: nil,
           front_matter_page_count: nil,
           back_matter_page_count: nil,
           total_numbered_pages: nil,
           production_page_count: nil,
           content_page_count: nil,
           total_unnumbered_insert_page_count: nil,
           duration: nil,
           duration_of_introductory_matter: nil,
           duration_of_main_content: nil)




Book.where(:product_form => 'DG').
     where('main_content_page_count is not null or
           front_matter_page_count is not null or
           back_matter_page_count is not null or
           total_numbered_pages is not null or
           production_page_count is not null or
           content_page_count is not null or
           total_unnumbered_insert_page_count is not null or
           duration is not null or
           duration_of_introductory_matter is not null or
           duration_of_main_content is not null').count



Book.where(:product_form => 'DG').
     update_all(main_content_page_count: nil,
           front_matter_page_count: nil,
           back_matter_page_count: nil,
           total_numbered_pages: nil,
           production_page_count: nil,
           content_page_count: nil,
           total_unnumbered_insert_page_count: nil,
           duration: nil,
           duration_of_introductory_matter: nil,
           duration_of_main_content: nil)
