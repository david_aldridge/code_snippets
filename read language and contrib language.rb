Workcontact.
  joins(:client).
  select do |work_contact|
    work_contact.languages_to&.any?
  end.
  map do |work_contact|
    [
      work_contact.languages_from,
      work_contact.languages_to,
      work_contact.work.languages.select(&:original_language?).map(&:language_code),
      work_contact.work.languages.select(&:text_language?).map(&:language_code)
    ]
  end.uniq