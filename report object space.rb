if (i%BATCH_SIZE) == BATCH_SIZE-1
  object_space_2 = ObjectSpace.count_objects
  object_space_diff = object_space_2.each_with_object({}){|(k,v),h| h[k] = v-object_space_1[k]}
  puts "ObjectSpace: #{object_space_2}"
  puts "Diff: #{object_space_diff}"
  object_space_1 = object_space_2
end
