books  = Client.is_snowbooks.books.limit(10).for_check_runner ; 0
books  = Client.is_snowbooks.books.limit(10).for_check_runner ; 0
books  = Client.is_snowbooks.books.has_no_check_results.for_check_runner ; 0
checks = CheckClass.all.checks ; 0

    books = Book.
              joins(:client, :work).
              merge(Work.updated_since(Time.current - 25.minutes)).
              merge(Client.active).
              order("books.client_id")


CheckRunner.call(checks, books).persist

============================================
For a client

client = Client.is_mcgill_queens ; 0
checks = CheckClass.all.checks ; 0
books  = client.books.forthcoming ; 0
CheckRunner.call(checks, books).persist
books  = client.books.active.pub_date_between(Date.civil(2019, 1, 1), Date.civil(2019, 12, 31)) ; 0
CheckRunner.call(checks, books).persist
books  = client.books.active.pub_date_between(Date.civil(2018, 1, 1), Date.civil(2018, 12, 31)) ; 0
CheckRunner.call(checks, books).persist
books  = client.books.active.pub_date_between(Date.civil(2015, 1, 1), Date.civil(2017, 12, 31)) ; 0
CheckRunner.call(checks, books).persist

============================================
For an export batch

client = Client.is_libri
export_batch = client.contacts.is_nielsen.export_batches.last

books  = export_batch.books.where.not(publishing_status: "01").for_check_runner
checks = CheckClass.all.checks

client.check_results.delete_all

CheckRunner.call(checks, books).persist







============================================
CheckRunner.call(
  CheckClass.where(
    name: [
 "Check::Work::Audiences::ONIXProfessionalScholarlyAudiencePresent",
      ]
    ).map(&:check),
  Client.is_unbound.books.for_check_runner
).persist

CheckRunner.call(
  CheckClass.where(
    name: [
      "Check::Work::Audiences::ONIXAudiencePresent",
      "Check::Work::Audiences::ONIXChildAudiencePresent",
      "Check::Work::Audiences::ONIXProfessionalScholarlyAudiencePresent",
      "Check::Work::Subjects::BICAdditionalCodesPresent",
      "Check::Work::Subjects::BICGeographyQualifierPresent",
      "Check::Work::Subjects::BICMainCodePresent",
      "Check::Work::Subjects::BICOneCharacterCodesNotPresent",
      "Check::Work::Subjects::BICSubjectNonhierarchical",
      "Check::Work::Subjects::BICTimeQualifierPresent",
      "Check::Work::Subjects::BISACAdditionalCodesPresent",
      "Check::Work::Subjects::BISACMainCodePresent",
      "Check::Work::Subjects::ThemaAdditionalCodesPresent",
      "Check::Work::Subjects::ThemaMainCodePresent",
      ]
    ).map(&:check),
  Client.is_facet.books.for_check_runner
).persist



CheckRunner.call(
  CheckClass.all.checks,
  Client.is_facet.books.for_check_runner
).persist



CheckRunner.call(
  CheckClass.where(
    name: [
      "Check::Product::Formats::FormPresent"]
    ).all,
  Client.is_unbound.books.for_check_runner
).persist


ActiveRecord::Base.transaction do
  Client.is_unbound.check_results.delete_all
  CheckRunner.call(
    CheckClass.all.checks,
    Client.is_unbound.books.for_check_runner
  ).persist
end



[
 "Check::Product::ApprovedCoverPresent",
 "Check::Product::ContributorPresent",
 "Check::Product::Extents::PageCountPresent",
 "Check::Product::Extents::WordCountPresent",
 "Check::Product::Formats::FormNotDeprecated",
 "Check::Product::Formats::FormPresent",
 "Check::Product::Identifiers::IsbnPresent",
 "Check::Product::MarketingTexts::BiographicalNotePresent",
 "Check::Product::MarketingTexts::MainDescriptionPresent",
 "Check::Product::MarketingTexts::OnlyOneShortDescription",
 "Check::Product::MarketingTexts::ShortDescriptionEnoughCharacters",
 "Check::Product::MarketingTexts::ShortDescriptionPresent",
 "Check::Product::MarketingTexts::ShortDescriptionWithinCharacterLimit",
 "Check::Product::Measurements::PageTrimHeightPresent",
 "Check::Product::Measurements::PageTrimWidthPresent",
 "Check::Product::Measurements::ProductHeightCredible",
 "Check::Product::Measurements::ProductHeightExceedsPageTrimHeight",
 "Check::Product::Measurements::ProductHeightPresent",
 "Check::Product::Measurements::ProductThicknessPresent",
 "Check::Product::Measurements::ProductWidthCredible",
 "Check::Product::Measurements::ProductWidthExceedsPageTrimWidth",
 "Check::Product::Measurements::ProductWidthPresent",
 "Check::Product::Measurements::UnitWeightCredible",
 "Check::Product::Measurements::UnitWeightPresent",
 "Check::Product::PublicationDatePresent",
 "Check::Product::PublishingStatusPresent",
 "Check::Product::SalesRightsPresent",
 "Check::Product::SeriesPresent",
 "Check::Work::Audiences::ONIXAudiencePresent",
 "Check::Work::Audiences::ONIXChildAudiencePresent",
 "Check::Work::Audiences::ONIXProfessionalScholarlyAudiencePresent",
 "Check::Work::Subjects::BICAdditionalCodesPresent",
 "Check::Work::Subjects::BICGeographyQualifierPresent",
 "Check::Work::Subjects::BICMainCodePresent",
 "Check::Work::Subjects::BICOneCharacterCodesNotPresent",
 "Check::Work::Subjects::BICSubjectNonhierarchical",
 "Check::Work::Subjects::BICTimeQualifierPresent",
 "Check::Work::Subjects::BISACAdditionalCodesPresent",
 "Check::Work::Subjects::BISACMainCodePresent",
 "Check::Work::Subjects::ThemaAdditionalCodesPresent",
 "Check::Work::Subjects::ThemaMainCodePresent",
]








require 'memory_profiler'


books  = Client.is_what_on_earth.books.for_check_runner
checks = CheckClass.all.checks

report = MemoryProfiler.report do
  CheckRunner.call(checks, books).persist
end

report.pretty_print


ActiveRecord::Base.transaction do
  CheckRunner.call(
    CheckClass.all.checks,
    Client.is_unbound.works.first.books.for_check_runner
  ).persist
end


ActiveRecord::Base.transaction do
  CheckRunner.call(
    CheckClass.all.first,
    Client.is_unbound.works.first.books.for_check_runner
  ).persist
end


Migration example
==================

Client.active.each do |client|
  puts "#{client} #{client.books.count} books"
  start = Time.now
  CheckRunner.call(
    [
      Check::Work::Subjects::ThemaStyleQualifierPresent,
      Check::Work::Subjects::ThemaLanguageQualifierPresent,
      Check::Work::Subjects::ThemaEducationalQualifierPresent,
      Check::Work::Subjects::ThemaInterestQualifierPresent,
      Check::Work::Subjects::ThemaGeographyQualifierPresent,
      Check::Work::Subjects::ThemaOneCharacterCodesNotPresent,
      Check::Work::Subjects::ThemaSubjectNonhierarchical,
      Check::Work::Subjects::ThemaTimeQualifierPresent
    ],
    client.books.includes(:marketingtext_short_descriptions, client: :check_controls, work: :imprint)
  ).persist
  duration = Time.now - start
  puts (client.books.count / duration).round(5)
end ; 0
