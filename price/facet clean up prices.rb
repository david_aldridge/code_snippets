pp Client.is_facet.prices.group(:currency_code, :price_type, :onix_price_type_qualifier_id).order("1,2,3").count

{["GBP", "01", "00"]=>1,
 ["USD", "01", "02"]=>2,
 ["GBP", "03", "02"]=>3,
 ["GBP", "01", "05"]=>11,
 ["GBP", "01", "06"]=>216,
 ["USD", "01", "06"]=>217,
 ["USD", "01", "05"]=>217,
 ["GBP", "02", "05"]=>260,
 ["GBP", "02", "00"]=>374,
 ["GBP", "01", "02"]=>445}

{["GBP", "02", "06"]=>3,
 ["GBP", "01", "06"]=>213,
 ["USD", "01", "06"]=>216,
 ["USD", "01", "05"]=>217,
 ["GBP", "02", "05"]=>262,
 ["GBP", "01", "02"]=>365}

pp Client.is_facet.prices.group(:book_id, :currency_code, :price_type, :onix_price_type_qualifier_id).having("count(*) > 1").count
pp Client.is_facet.prices.group(:book_id, :currency_code, :price_type, :onix_price_type_qualifier_id).having("count(*) > 1").count


Client.is_facet.prices.usd.exctax.export.count

Client.is_facet.prices.gbp.frp_exctax.destroy_all
Client.is_facet.prices.gbp.inctax.library.destroy_all





 ["GBP", "02", "05"]=>262,
 ["GBP", "01", "02"]=>365}

Client.is_facet.prices.gbp.where(onix_price_type_qualifier_id: %w[02 05]).group(:book_id).having("count(*) > 1").count
Client.is_facet.prices.gbp.where(onix_price_type_qualifier_id: "02").each {|b| b.update(onix_price_type_qualifier_id: "05")}

Client.is_facet.prices.gbp.consumer.joins(:book).merge(Book.ebooks).exctax
Client.is_facet.prices.gbp.consumer.update_all(price_type: "02")

["USD", "01", "06"]=>216,
["USD", "01", "05"]=>217,
["GBP", "01", "06"]=>213,
["GBP", "02", "05"]=>627

Client.is_facet.prices.gbp.library.joins(:book).count
Client.is_facet.prices.gbp.library.joins(:book).merge(Book.is_ebook).each do |price|
  price.price_type = "02"
  price.price_amount = (price.price_amount * 1.2).round(2)
  price.save
end


 pp Client.is_facet.prices.group(:book_id, :currency_code, :onix_price_type_qualifier_id).having("count(*) > 1").count


 pp Client.is_facet.prices.gbp.where.not(onix_price_type_qualifier_id: "06").pluck(:book_id, :currency_code, :onix_price_type_qualifier_id, :price_amount).group_by{|x| [x[0], x[1]]}


Client.is_facet.prices.gbp.includes(:book).where.not(onix_price_type_qualifier_id: ["06", "05"]).map do |price|
  price.destroy if price.book.prices.gbp.consumer.exists?
end



Client.is_facet.prices.gbp.includes(:book).where(onix_price_type_qualifier_id: "00").each do |price|
  price.destroy if price.book.prices.gbp.where(onix_price_type_qualifier_id: "02").exists?
end