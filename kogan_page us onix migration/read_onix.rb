class ReadOnix

  HEADER = %w(isbn
              contributors
              contributor_biographies
              product_biographical_note
              main_description
              short_description
              table_of_contents
              usd_price)

  REVIEW_HEADER = %w(isbn
              text)

  def initialize(file_name)
    @file_name = file_name
    @doc = Nokogiri::XML(open(file_name), nil) do |config|
      config.default_xml.noblanks
    end
    @doc.remove_namespaces!
  end


  def write_csv
    CSV.open(@file_name + ".csv", "wb", :headers => HEADER, :write_headers => true, :quote_char => '"') do |csv|
      @doc.xpath("//product").each_with_index do |product_node, i|
        csv << [
          product_node.at_xpath("productidentifier[b221='15']/b244").text,
          product_node.xpath("contributor/b036").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.xpath("contributor/b044").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.xpath("othertext[d102='13']/d104").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.xpath("othertext[d102='01']/d104").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.xpath("othertext[d102='02']/d104").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.xpath("othertext[d102='04']/d104").map{|c| '"'+c.text+'"'}.to_sentence,
          product_node.at_xpath("supplydetail/price[j152='USD']/j151").try(:text)
        ]
      end
    end
  end

  def write_reviews
    CSV.open(@file_name + "_reviews.csv", "wb", :headers => REVIEW_HEADER, :write_headers => true, :quote_char => '"') do |csv|
      @doc.xpath("//product/othertext[d102='08']").each_with_index do |review_node, i|
        csv << [
          review_node.at_xpath("../productidentifier[b221='15']/b244").text,
          review_node.at_xpath("d104").text
        ]
      end
    end
  end

end


__END__


reload!
r = ReadOnix.new("/Users/david/Documents/Bibliocloud/Code/kogan_page us onix migration/KoganPage_10202015_complete_onix21.xml") ; 0
r.write_csv ; 0
r.write_reviews ; 0


@doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Code/kogan_page us onix migration/KoganPage_10202015_complete_onix21.xml"), nil) do |config|
  config.default_xml.noblanks
end
@doc.remove_namespaces!

@doc.xpath("//product").first.xpath("contributor/b306").map{|c| c.text}.to_sentence


#include AugmentsOnix
#ISBN
#Contributor/Contributor name
#Contributor/Biographical Note
#Text Content/Text type 12 (Biography)
#Text Content/Text Type 03/Text (Long Description)
#Text Content/Text Type 02/Text (Short Description)
#Text Content/Text Type 07/Text (Reviews)
#Text Content/Text Type 07/SourceTitle (Reviews)
#Text Content/Text Type 04/Text (TOC)
#ProductSupply/Market/Territory/Countries Included
#SupplyDetail/Price/Pricetype 01/PriceAmount (CurrencyCode USD only)
