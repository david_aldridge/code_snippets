a = Work.where(<<~SQL
  case when identifying_doi is null then 0 else 1 end +
  case when registrants_internal_reference is null then 0 else 1 end +
  case when identifying_isrc is null then 0 else 1 end +
  case when identifying_isbn13 is null then 0 else 1 end >= 2
SQL
).group(:client).count.transform_keys(&:to_s)


b = Contact.where(identifier: "ingram").group(:client).count.transform_keys(&:to_s)


a.keys & b.keys


Client.is_unbound.works.where(<<~SQL
  case when identifying_doi                is null then 0 else 1 end +
  case when registrants_internal_reference is null then 0 else 1 end +
  case when identifying_isrc               is null then 0 else 1 end +
  case when identifying_isbn13             is null then 0 else 1 end >= 2
SQL
).pluck(:identifying_doi          ,
:registrants_internal_reference,
:identifying_isrc              ,
:identifying_isbn13            )


Client.is_unbound.works.where(<<~SQL
  case when identifying_doi                is null then 0 else 1 end +
  case when registrants_internal_reference is null then 0 else 1 end +
  case when identifying_isrc               is null then 0 else 1 end +
  case when identifying_isbn13             is null then 0 else 1 end >= 2
SQL
).pluck(:id)
