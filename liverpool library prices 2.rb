Client.is_liverpool.prices.where(:currency_code => 'GBP', :price_type => '01').map(&:book).uniq.each do |book|
  if book.current_gbp_inctax_consumer_price.nil?
    book.current_gbp_inctax_consumer_price = (book.current_gbp_exctax_consumer_price * (book.is_ebook? ? 1.2 : 1.0) ).round(2)
  end
  book.current_gbp_exctax_consumer_price = nil




ActiveRecord::Base.transaction do
  Client.is_liverpool.books.includes(:prices).each do |book|

    if book.current_gbp_inctax_consumer_price.nil? && book.current_gbp_exctax_consumer_price
      book.current_gbp_inctax_consumer_price = (book.current_gbp_exctax_consumer_price * (book.is_ebook? ? 1.2 : 1.0) ).round(2)
    end

    if book.epub_type_code == '002'
      if book.current_gbp_inctax_library_price.nil? && book.current_gbp_inctax_consumer_price &&
        book.current_gbp_inctax_library_price = (book.current_gbp_inctax_consumer_price * 2.25).round(2)
      end
      if book.current_usd_exctax_library_price.nil? && book.current_usd_exctax_consumer_price &&
        book.current_usd_exctax_library_price = (book.current_usd_exctax_consumer_price * 2.25).round(2)
      end
    end
  end
end



Client.is_liverpool.prices.where(:currency_code => 'GBP', :price_type => '01').destroy_all

