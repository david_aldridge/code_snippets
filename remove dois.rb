isbns = %w[
9780750317047
9780750315784
9780750313230
9780750317108
9780750315753
9780750313445
9780750320726
9780750314220
9780750314374
9780750316415
9780750317269
9780750320269
9780750320481
9780750314909
9780750317504
9780750316682
9780750312813
9780750315029
9780750314251
9781643270784
9781643271507
9781643272344
9781643272924
9781681742892
9781643271682
9781643271743
9781643273266
9781643271804
9781643273389
9781643271927
9781643273327
9781643273440
9781643271989
9781643273501
9781643273624
9781643273686
9781643273556
9781643272153
9781643273730
9781643273198
9781643273075
9781643273136
9781643273013
9781681745336
]

ActiveRecord::Base.transaction do
  Work.no_touching do
    Book.where(isbn: isbns).includes(work: :books).each do |book|
      book.work.update(identifying_doi: nil)
      book.work.books.each {|b| b.update(identifying_doi: nil) }
    end
  end
end

ActiveRecord::Base.transaction do
  Work.no_touching do
    Book.where.not(publishing_status: Book::PUB_STATUS_ACTIVE).each { |book| book.update(identifying_doi: nil) }
  end
end





isbns = %w[
9780750312486
9780750312875
9780750315722
9780750315876
9780750316354
9780750316712
9780750316774
9780750316989
9780750317269
9780750317412
9780750318303
9780750321990
9781643270784
9781643271132
9781643271385
9781643271620
9781681745176
9781681747125
9781681749259
9781681749662
]

ActiveRecord::Base.transaction do
  Work.no_touching do
    Book.where(isbn: isbns).includes(work: :books).each do |book|
      book.work.books.each do |b|
        next if b.isbn_invalid?
        b.update(identifying_doi: "10.1088/#{b.isbn_formatted}")
      end
    end
  end
end



Book.where(isbn: isbns).includes(work: :books).map(&:work).flat_map(&:books).map do |book|
  book.versions.where("created_at < ?", Date.today - 3.hours).where(whodunnit: nil).where("object_changes like '%identifying_doi:\n- 10%'").last&.object_changes#.reify.identifying_doi
end
