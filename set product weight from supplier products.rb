client = Client.is_snowbooks

weightless_products = client.books.not_ebook.reject{|b| b.unit_weight_gr_value}

weightless_products.each do |product|
  next unless new_weight = product.supplier_products.detect(&:weight_gr)
  product.update_attributes(unit_weight_gr: new_weight)
end ; 0

