require "/Users/david/Documents/Bibliocloud/Clients/Rebellion/temp/numbers.rb"

client = Client.is_rebellion

ActiveRecord::Base.transaction do
  Work.no_touching do
    Rebellion.numbers.each do |isbn, unit_weight_kg, carton_qty, pallet_qty, dim1, dim2, dim3|
      next unless unit_weight_kg || carton_qty || pallet_qty || dim1 || dim2 || dim3

      book = client.books.find_by(isbn: isbn)
      next unless book
      book.unit_weight_gr = unit_weight_kg * 1_000 if unit_weight_kg
      book.carton_qty     = carton_qty if carton_qty
      book.pallet_qty     = pallet_qty if pallet_qty && pallet_qty > 0
      height, width, thickness = [dim1 , dim2, dim3].compact.sort.reverse
      book.product_height_mm    = height    if height
      book.product_width_mm     = width     if width
      book.product_thickness_mm = thickness if thickness
      book.save!
    end
  end
end
