require 'benchmark'
require 'set'
COUNTRY_ARY = ["AD", "AE", "AF", "AG", "AI", "AL", "AM", "AN", "AO", "AQ", "AR", "AS", "AT", "AU", "AW", "AX", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BL", "BM", "BN", "BO", "BQ", "BR", "BS", "BT", "BV", "BW", "BY", "BZ", "CA", "CC", "CD", "CF", "CG", "CH", "CI", "CK", "CL", "CM", "CN", "CO", "CR", "CS", "CU", "CV", "CW", "CX", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "EH", "ER", "ES", "ET", "FI", "FJ", "FK", "FM", "FO", "FR", "GA", "GB", "GD", "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN", "GP", "GQ", "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HM", "HN", "HR", "HT", "HU", "ID", "IE", "IL", "IM", "IN", "IO", "IQ", "IR", "IS", "IT", "JE", "JM", "JO", "JP", "KE", "KG", "KH", "KI", "KM", "KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK", "LR", "LS", "LT", "LU", "LV", "LY", "MA", "MC", "MD", "ME", "MF", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MP", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NC", "NE", "NF", "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK", "PL", "PM", "PN", "PR", "PS", "PT", "PW", "PY", "QA", "RE", "RO", "RS", "RU", "RW", "SA", "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SL", "SM", "SN", "SO", "SR", "SS", "ST", "SV", "SX", "SY", "SZ", "TC", "TD", "TF", "TG", "TH", "TJ", "TK", "TL", "TM", "TN", "TO", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "UM", "US", "UY", "UZ", "VA", "VC", "VE", "VG", "VI", "VN", "VU", "WF", "WS", "YE", "YT", "YU", "ZA", "ZM", "ZW"]

COUNTRY_HSH = COUNTRY_ARY.each_with_object({}) {|x,h| h[x=nil]}

COUNTRY_SET = Set.new(COUNTRY_ARY)

COUNTRY_SST = SortedSet.new(COUNTRY_ARY)

class FastUniqueArray
  def initialize(ary)
    @store = ary.each_with_object({}) {|x,h| h[x=nil]}
    freeze
  end

  def include?(value)
    @store.key? value
  end
end

COUNTRY_OBJ = FastUniqueArray.new(COUNTRY_ARY)

def method0(code)
  true
end

def method1(code)
  COUNTRY_ARY.include?(code)
end

def method2(code)
  COUNTRY_ARY.bsearch {|x| x == code}
end

def method3(code)
  COUNTRY_HSH.include?(code)
end

def method4(code)
  COUNTRY_SET.include?(code)
end

def method5(code)
  COUNTRY_SST.include?(code)
end

def method6(code)
  COUNTRY_OBJ.include?(code)
end

n = 1000000
Benchmark.bm(7) do |x|
  x.report("method 0")   { n.times {method0("ZW")} }
  puts "End"
  x.report("method1")   { n.times {method1("ZW")}}
  x.report("method2")   { n.times {method2("ZW")}}
  x.report("method3")   { n.times {method3("ZW")}}
  x.report("method4")   { n.times {method4("ZW")}}
  x.report("method5")   { n.times {method5("ZW")}}
  x.report("method6")   { n.times {method6("ZW")}}
  puts "Start"
  x.report("method1")   { n.times {method1("KZ")}}
  x.report("method2")   { n.times {method2("KZ")}}
  x.report("method3")   { n.times {method3("KZ")}}
  x.report("method4")   { n.times {method4("KZ")}}
  x.report("method5")   { n.times {method5("KZ")}}
  x.report("method6")   { n.times {method6("KZ")}}
  puts "Middle"
  x.report("method1")   { n.times {method1("AD")}}
  x.report("method2")   { n.times {method2("AD")}}
  x.report("method3")   { n.times {method3("AD")}}
  x.report("method4")   { n.times {method4("AD")}}
  x.report("method5")   { n.times {method5("AD")}}
  x.report("method6")   { n.times {method6("AD")}}
  puts "Nowhere"
  x.report("method1")   { n.times {method1("__")}}
  x.report("method2")   { n.times {method2("__")}}
  x.report("method3")   { n.times {method3("__")}}
  x.report("method4")   { n.times {method4("__")}}
  x.report("method5")   { n.times {method5("__")}}
  x.report("method6")   { n.times {method6("__")}}
end ; ""
