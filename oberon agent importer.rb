class OberonAgentImporter


  def initialize(key,client)
    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key(key)
    @agent_sheet   = @sheet.worksheet_by_title("Agents")
    @agent_list    = @agent_sheet.list
    @client        = client
  end

  def load_agents
    @agent_list.each do |agent|

      if agent["Company"].presence
        contact = @client.contacts.create!(corporate_name:   agent["Company"],
                                           contributor_type: "Company"      ,
                                           contact_types:    ContactType.where(name: "Literary Agency"))

        contact.emails.create!(email:         agent["Email"],
                             default:       true,
                             email_type:    "Work") if agent["Email"].presence

      elsif agent["Last Name"].presence and not agent["Company"].presence
        contact = @client.contacts.create!(names_before_key:     agent["First Name"].presence ,
                                           keynames:             agent["Last Name"].presence  ,
                                           person_name:          [agent["First Name"].presence, agent["Last Name"].presence].compact.join(" "),
                                           person_name_inverted: [agent["Last Name"].presence , agent["First Name"].presence].compact.join(", "),
                                           contributor_type:     "Person"      ,
                                           contact_types:        [ContactType.where(name: "Literary agency")])

        contact.emails.create!(email:         agent["Email"],
                               default:       true,
                               email_type:    "Work") if agent["Email"].presence

      elsif agent["Last Name"].presence and agent["Company"].presence
        contact = @client.contacts.create!(corporate_name:   agent["Company"],
                                           contributor_type: "Company"      ,
                                           contact_types:    ContactType.where(name: "Literary Agency"))

        subcontact = @client.contacts.create!(names_before_key:  agent["First Name"].presence,
                                           keynames:             agent["Last Name"].presence,
                                           person_name:          [agent["First Name"].presence, agent["Last Name"].presence].compact.join(" "),
                                           person_name_inverted: [agent["Last Name"].presence, agent["First Name"].presence].compact.join(", "),
                                           contributor_type:     "Person"      ,
                                           contact_types:        ContactType.where(name: "Literary agency"))
        subcontact.emails.create!(email:         agent["Email"],
                                   default:       true,
                                   email_type:    "Work") if agent["Email"].presence


        contact.employees << subcontact
      end

      contact.addresses.create!(address_one:   agent["Address 1"].presence,
                                address_two:   agent["Address 2"].presence,
                                address_three: agent["Address 3"].presence,
                                address_four:  agent["Country"  ].presence,
                                postcode:      agent["Post Code"].presence,
                                set_as_default_address: true)

      contact.phones.create!(phone_number:  agent["Phone"],
                             phone_type:    "Landline") if agent["Phone"].presence

    end
  end

end


__END__

i = OberonAgentImporter.new("1vSvkS30eYFYa_TY6CPSHQV4x03_-qpqSTBM8ySUMhLc",  Client.find_by(:webname => "Oberon Books"))

i.load_agents

    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key("1vSvkS30eYFYa_TY6CPSHQV4x03_-qpqSTBM8ySUMhLc")
    @agent_sheet   = @sheet.worksheet_by_title("Agents")
    @agent_list    = @agent_sheet.list
    @client        = Client.find_by(:webname => "Oberon Books")


    agent = @agent_list.first

      contact = @client.contacts.create!(corporate_name:   agent["Company"],
                                         contributor_type: "Company"      ,
                                         contact_types:    ContactType.where(name: "Literary Agency"))

      contact.addresses.create!(address_one:   agent["Address 1"].presence,
                          address_two:   agent["Address 2"].presence,
                          address_three: agent["Address 3"].presence,
                          address_four:  agent["Country"  ].presence,
                          postcode:      agent["Post Code"].presence,
                          set_as_default_address: true)

      contact.emails.create!(email:         agent["Email"],
                             default:       true,
                             email_type:    "Work") if agent["Email"].presence

      contact.phones.create!(phone_number:  agent["Phone"],
                             default:       true,
                             phone_type:    "Landline") if agent["Phone"].presence





Contact.where(:id => 48015..48144).each do |contact|
  contact.contact_types = ContactType.where(name: "Literary agency")
end



