client = Client.find(404)

#file = '/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/NielsenRecordSupply.xml'
file = '/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/Pharma ONIX.xml'

document = ONIX::XML::Document.new(node: Nokogiri::XML(open(file)).remove_namespaces!) ; 0

number_of_products = document.products.size

products_by_form   = document.products.map(&:product_form).map{|x| PRODUCT_FORMS[x]}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}
{"Electronic book text"=>163, "Paperback / softback"=>371, "Diskette"=>6, "Hardback"=>97, "CD-ROM"=>42, "Mixed media product"=>16, "Loose-leaf"=>16, "Book"=>5}

epub_types = OnixCodeList.find_by(:list_number => 10  ).to_h
ebook_formats   = document.products.select{|p| p.product_form == 'DG'}.map{|p| epub_types[p.epub_type_code]}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}


products_by_form   = document.products.select{|p| p.product_form=='WW'}.map{|p| p.contained_items.map(&:product_form)}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}

document.products.map(&:pub_date).compact.min
document.products.map(&:pub_date).compact.max

document.products.map(&:supply_details).flatten.map(&:prices).flatten.map(&:currency_code).uniq

"USD", "GBP", "AUD", "INR"


ISBN Prefixes and count of products

pp document.products.map(&:product_identifiers).flatten.select(&:isbn13?).map(&:value).map{|isbn| Lisbn.new(isbn).isbn_with_dash.split("-")[0..2].join("-")}.each_with_object(Hash.new(0)){|i,h| h[i] += 1}.sort{|a,b| a[1]<=>b[1] }.reverse
[["978-0-85369", 546], ["978-0-85711", 167], ["978-1-56363", 2], ["978-0-388", 1]]


pp document.products.select{|product| product.product_identifiers.select(&:isbn13?).first.value.starts_with? '9780388'}.map{|p| p.titles.map(&:distinctive)}



contributor_roles   = document.products.map(&:contributors).flatten.map(&:role).map{|x| CONTRIBUTOR_ROLES[x]}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}
"By (author)"=>631
"Volume editor"=>40
"Edited by"=>314
"Revised by"=>13
"Created by"=>9
"Series edited by"=>2
"Foreword by"=>1
"Contributions by"=>3


series = document.products.map(&:series).flatten.map(&:titles).flatten.map(&:distinctive).each_with_object(Hash.new(0)){|p,h| h[p]+= 1}
"Fast Track Pharmacy Series"=>6
"Fast Track Pharmacy"=>1
"FastTrack"=>4
"FASTtrack"=>9
"In Focus"=>4
"Nurse Prescribers' Formulary"=>2
"Pharmacy Business Administration"=>4
"Pocket Companion Series"=>1
"Remington Education"=>4
"Tomorrow's Pharmacist Series"=>2
"ULLA Pharmacy Series"=>2
"ULLA Pharmacy"=>1
"ULLA Series: Science Textbooks for Postgraduates"=>3

schemes = OnixCodeList.find_by(:list_number => 26).to_h
document.products.map(&:additional_subjects).flatten.select{|s| s.scheme_identifier == '03'}.map(&:code)
document.products.map(&:additional_subjects).flatten.select{|s| s.scheme_identifier == '04'}.map(&:heading_text)
subject_schemes   = document.products.map(&:additional_subjects).flatten.map(&:scheme_identifier).map{|x| schemes[x]}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}
"BIC E4L"=>556
"BIC geographical qualifier"=>109
"BIC subject category"=>690
"BIC time period qualifier"=>42
"BIC UKSLC"=>556
"BISAC Subject Heading"=>424
"Dewey"=>567
"LC classification"=>27
"LC subject heading"=>495
"Proprietary subject scheme"=>628
"Thema geographical qualifier"=>108
"Thema subject category"=>1274
"Thema time period qualifier"=>42

proprietary_subjects   = document.products.map(&:additional_subjects).flatten.select{|s| s.scheme_identifier == '24'}.map{|s| [s.proprietary_scheme_name, s.code, s.heading_text]}.each_with_object(Hash.new(0)){|p,h| h[p]+= 1}

{["Nielsen BookScan Product Class", "S6.6", "Medical Nursing & Ancillary Services"]=>172, ["Nielsen BookScan Product Class", "S6.2", "Clinical Medicine: Professional"]=>254, ["Nielsen BookScan Product Class", "S6.1T", "Medicine: Textbooks & Study Guides"]=>70, ["Nielsen BookScan Product Class", "T9.4", "Fitness & Diet"]=>1, ["Nielsen BookScan Product Class", "S6.3", "Surgery: Professional"]=>1, ["Nielsen BookScan Product Class", "T2.0", "Encyclopedias & General Reference"]=>5, ["Nielsen BookScan Product Class", "S6.0", "Medicine: General"]=>29, ["Nielsen BookScan Product Class", "Z99.9", "Unclassifiable: no BIC"]=>4, ["Nielsen BookScan Product Class", "S7.9T", "Science & Mathematics: Textbooks & Study Guides"]=>1, ["Nielsen BookScan Product Class", "T10.2", "Alternative Therapies & Health"]=>10, ["Nielsen BookScan Product Class", "S9.4", "Industrial Chemistry & Manufacturing"]=>16, ["Nielsen BookScan Product Class", "S7.7", "Veterinary Science"]=>14, ["Nielsen BookScan Product Class", "S6.5", "Dentistry & Dental Nursing"]=>10, ["Nielsen BookScan Product Class", "S8.4", "Agriculture & Farming"]=>8, ["Nielsen BookScan Product Class", "T17.1", "Careers & Success"]=>1, ["Nielsen BookScan Product Class", "S4.6", "Industrial Studies: General"]=>12, ["Nielsen BookScan Product Class", "S9.1", "Mechanical Engineering"]=>1, ["Nielsen BookScan Product Class", "S3.3", "Social Issues, Services & Welfare"]=>3, ["Nielsen BookScan Product Class", "S4.0T", "Business, Accounting & Vocational: Textbooks & Study Guides"]=>1, ["Nielsen BookScan Product Class", "S4.3", "Sales & Marketing"]=>2, ["Nielsen BookScan Product Class", "S5.0", "Law: General & Reference"]=>3, ["Nielsen BookScan Product Class", "S9.9T", "Engineering & Technology: Textbooks & Study Guides"]=>2, ["Nielsen BookScan Product Class", "S6.4", "Psychiatry & Clinical Psychology: Professional"]=>1, ["Nielsen BookScan Product Class", "S5.4", "National Law: Professional"]=>2, ["Nielsen BookScan Product Class", "S4.2", "Management & Business: General"]=>1, ["Nielsen BookScan Product Class", "T9.5", "Coping with Problems & Illness"]=>1, ["Nielsen BookScan Product Class", "S3.2", "Sociology & Anthropology: Professional"]=>1, ["Nielsen BookScan Product Class", "S3.7", "Library & Information Science"]=>1, ["Nielsen BookScan Product Class", "T1.1", "Fine Arts / Art History"]=>1}



CSV.open("/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/NielsenRecordSupply_products_2.csv", "wb") do |csv|
  csv << ["Distinctive Title", "ISBN", "Product Form", "ePub Type Code", "Publication Date", "Edition Number", "Publishing Status", "Series", "Series number"]
  #document.products.map{|p| ONIX::ProductReader.new(product: p)}.each do |p|
  document.products.each do |p|
    csv << [p.titles.first.text,
            Lisbn.new(p.product_identifiers.first.isbn13).isbn_with_dash,
            p.product_form,
            p.epub_type_code,
            p.pub_date.try(:iso8601),
            p.edition,
            p.publishing_status,
            p.series.map(&:title).to_sentence,
            p.series.map(&:number_within_series).to_sentence]
  end
end ; 0




CSV.open("/Users/david/Documents/Bibliocloud/Clients/Pharmaceutical Press/Nielsen Record Supply/NielsenRecordSupply_contributors.csv", "wb") do |csv|
  csv << ["Person name", "Person name inverted", "Titles Before Names", "Names before Key", "Key names", "Corporate Name"]
  document.products.map(&:contributors).flatten.each do |c|
    csv << [c.person_name,
            c.person_name_inverted,
            c.titles_before_names,
            c.names_before_key,
            c.key_names,
            c.corporate_name]
  end
end ; 0



Hash[*document.products.map{|p| [p.product_identifiers.flatten.select(&:isbn13?).map(&:value).first, p.additional_subjects.select(&:thema?).map(&:code).map     ]}.flatten(1)]

Work::THEMA_SUBJECT_CODES.map(&:last)

Work::THEMA_GEOG_CODES
Work::THEMA_LANG_CODES
Work::THEMA_TIME_CODES
Work::THEMA_EDU_CODES
Work::THEMA_AGE_CODES
Work::THEMA_INTEREST_CODES
Work::THEMA_STYLE_CODES
Work::THEMA_SUBJECT_AREAS

file="https://s3-eu-west-1.amazonaws.com/bibliocloudimages/Pharma_ONIX.xml"

doc = Nokogiri::XML(open(file)).remove_namespaces! ; 0

doc.xpath('/ONIXMessage/Product').map{|product_node| ONIX::ProductReader.new(node: product_node)}.
  map{|product_reader| [product_reader.isbn13, product_reader.thema_code_attributes]}.
  each do |product|
  work = client.books.find_by(:isbn => product[0]).work
  work.update_attributes(product[1])
end



file="https://s3-eu-west-1.amazonaws.com/bibliocloudimages/Pharma_ONIX.xml"

doc = Nokogiri::XML(open(file)).remove_namespaces! ; 0

Hash[
  *doc.xpath('/ONIXMessage/Product').map{|product_node| ONIX::ProductReader.new(node: product_node)}.
    map{|product_reader| [product_reader.isbn13, product_reader.main_bisac_code]}.
    map do |product|
      if product[1].blank?
        nil
      else
        work = client.books.find_by(:isbn => product[0]).work
        [work, product[1]]
      end
  end.compact.flatten].each_pair do |work, main_bisac_code|
  work.update_attributes(:main_bisac_code => main_bisac_code)
end ; 0


Client.