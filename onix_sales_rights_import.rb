9780948230240



doc       = Nokogiri::XML(File.open("/Users/david/Documents/Bibliocloud/Clients/Oberon/onix_oberon copy.xml")) ; 0
client    = Client.find_by_webname("Oberon Books")

user_id   = client.users.first.id

doc.remove_namespaces! ; 0

ActiveRecord::Base.transaction do
  doc.xpath('//Product').each do |product|
    isbn = product.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").text
    next if isbn.nil?
    book = client.books.where(:isbn => isbn).first
    next if book.nil?
    contract = book.work.contract
    sales_rights = []
    product.xpath("SalesRights").each do |sales_right_node|
      sales_rights_type  =  sales_right_node.at_xpath("SalesRightsType").text
      countries_included =  sales_right_node.at_xpath("RightsCountry").try(:text).try(:split," ")
      regions_included   =  sales_right_node.at_xpath("RightsTerritory").try(:text).try(:split," ")
      if regions_included and regions_included.include?("WORLD")
        regions_included = ["WORLD"]
        countries_included = nil
      end

      sales_rights       << {:sales_rights_type  => sales_rights_type ,
                             :countries_included => countries_included,
                             :regions_included   => regions_included  ,
                             :client_id          => client.id         ,
                             :user_id            => user_id           }
    end
    if sales_rights.size == 0
      next
    #elsif sales_rights.size == 1 and sales_rights[0][:sales_right_code] == "01" and sales_rights[0][:regions_included] == ["WORLD"]
    #  next
    elsif sales_rights.size == 1 and sales_rights[0][:regions_included] == ["ROW"]
      sales_rights[0][:regions_included] == ["WORLD"]
      sales_rights[0][:countries_included] == nil
    else
      contract.sales_rights.destroy_all
      sales_rights.each do |sales_right_attribs|
        contract.sales_rights.create!(sales_right_attribs)
      end
    end
  end
  #ActiveRecord::Rollback
end

client.sales_rights.delete_all
client.sales_rights.group(:sales_rights_type, :countries_included, :regions_included).count

{["01", ["AU", "CA", "GB", "IE", "NZ", "US", "ZA"], ["ROW"]]=>16, ["03", ["ZA"], nil]=>1, ["01", ["GB"], ["ROW"]]=>1, ["02", ["AU", "NZ"], nil]=>199, ["01", ["AU", "CA", "GB", "IE", "NZ", "US", "ZA"], nil]=>8, ["01", ["AU", "CA", "GB", "IE", "NZ", "US"], ["ROW"]]=>1, ["03", ["CA", "US"], nil]=>1, ["02", nil, ["WORLD"]]=>1, ["01", ["GB"], nil]=>1, ["01", nil, ["ROW"]]=>1, ["01", ["CA", "GB", "IE", "US", "ZA"], ["ROW"]]=>1, ["02", ["AU", "GB", "NZ"], nil]=>2, ["01", ["AU", "CA", "GB", "IE", "NZ", "ZA"], nil]=>1, ["02", ["AU", "GB", "NZ", "US"], nil]=>1, ["02", ["GB"], nil]=>1, ["02", ["US"], nil]=>1, ["02", ["AU", "NZ", "US"], nil]=>1, ["01", nil, ["WORLD"]]=>1673, ["01", ["AU", "GB", "IE", "NZ", "ZA"], ["ROW"]]=>3, ["03", ["US"], nil]=>2, ["01", ["AU", "CA", "GB", "IE", "NZ", "ZA"], ["ROW"]]=>3, ["02", ["AU", "GB"], nil]=>1, ["01", ["AU", "NZ"], nil]=>17} 




<SalesRights>
  <SalesRightsType>01</SalesRightsType>
  <RightsTerritory>WORLD</RightsTerritory>
</SalesRights>


require 'nokogiri'
require 'open-uri'
require "aws/s3"
require 'cgi'
require 'net/http'

class OnixImport

  def string_to_date(str)
    return nil if str.nil?

    case str.size
    when 8 then Date.strptime(str,"%Y%m%d")
    when 6 then Date.strptime(str,"%Y%m")
    end
  end

  def import(client_id, user_id, upload)
    filename  = upload.xml_file_name
    import_from_url(client_id, user_id, "https://#{ENV['AWS_URL']}/#{ENV['AWS_BUCKET']}/#{filename}")
  end

  def import_from_url(client_id, user_id, url, create_publisher = false, delay_creation = true)
    f         = open(url)
    import_from_xml(client_id, user_id, f, create_publisher, delay_creation)
  end

  def import_from_xml(client_id, user_id, xml, create_publisher = false, delay_creation = true)
    doc       = Nokogiri::XML(xml)
    doc.remove_namespaces!
    doc.xpath('//Product|//product').each do |node|
      if create_publisher
        onix_publisher_name = node.xpath("./Publisher/PublisherName|./publisher/b081").text
        unless onix_publisher_name.blank?
          system_client  = Client.where("upper(trim(client_name)) = upper(trim(?))",onix_publisher_name).first
          if system_client
            client_id = system_client.id
          else
            client    = Client.create(:client_name => onix_publisher_name, :webname => onix_publisher_name).id
            client_id = client.id
          end
        end
      end
      onix_isbn = node.xpath("./ProductIdentifier[ProductIDType=15]/IDValue|./productidentifier[b221=15]/b244").text
      if ISBN.valid?(onix_isbn) and !Book.where(:isbn => onix_isbn, :client_id => client_id).exists?
        create_new_book_record(client_id, user_id, node)
      end

    end # of each product
  end #of xmlupdate
  #handle_asynchronously :xmlupdate

  def create_new_book_record(client_id, user_id, node)
    work   = get_work(                         node, client_id)
    Book.skip_callback(:create, :after, :set_availability_to_forthcoming)
    book   = create_book(                         node, client_id, user_id,work.id)
    if book
      Book.set_callback(:create, :after, :set_availability_to_forthcoming)
      series = create_series(                     node, client_id, book)
      create_measurement(                         node, client_id, book)
      create_imprint(                             node, client_id)
      create_language(                            node, client_id, book)
      create_publisher(                           node, client_id, book)
      create_relatedproduct(                      node, client_id, book)
      create_audience(                            node, client_id, work, book)
      create_audience_if_audience_code_provided(  node, client_id, work, book)
      create_extent(                              node, client_id, book)
      create_rights(                              node, client_id, book)
      create_not_for_sale(                        node, client_id, book)
      create_extent_if_number_of_pages_provided(  node, client_id, book)
      create_supporting_resource(                 node, client_id, work, book)
      create_identifier(                          node, client_id, book)
      create_subjects(                            node, client_id, work, book)
      create_text(                                node, client_id, work, book)
      create_prize(                               node, client_id, work, book)
      create_supply(                              node, client_id, book)
      create_contact(                             node, client_id, work, book)
      book.update_column(:no_validation, false)
    end
  end


  def get_publishername_id(client_id, node)
    onix_name     = node.xpath("./Publisher/PublisherName|./publisher/b081").text
    publishername = Publishername.where(:client_id => client_id).where("upper(trim(publisher_name)) = upper(trim(?))", onix_name).first ||
                    Publishername.create!(:client_id => client_id,:publisher_name=> onix_name)
    publishername.id
  end

  def get_imprint_id(client_id, node)
    onix_name     = node.xpath("./Imprint/ImprintName|./imprint/b079").text
    imprint       = Imprint.where(:client_id => client_id).where("upper(trim(value)) = upper(trim(?))", onix_name).first ||
                    Imprint.create!(:client_id => client_id,:value=> onix_name)
    imprint.id
  end

  def create_book(node, client_id, user_id, work_id)
    puts "*" * 30, "create book","*" * 30

    unless Book.where(:client_id  => client_id,
                      :isbn       => node.xpath("./ProductIdentifier[ProductIDType=15]/IDValue|./productidentifier[b221=15]/b244").text).exists? or
           node.xpath("./b012|./ProductForm").text == "DG"

      product_form_detail = []
      node.xpath("./ProductFormDetail").each do |pfd|
        product_form_detail << pfd.text.presence
      end

      book = Book.new(
        :isbn                                       => node.xpath("./ProductIdentifier[ProductIDType=15]/IDValue|./productidentifier[b221=15]/b244").text.presence,
        :notification                               => node.xpath("./a002|./NotificationType").text.presence,
        :product_form_detail                        => product_form_detail.empty? ? nil : product_form_detail,
        :product_form                               => node.xpath("./b012|./ProductForm").text.presence,
        :format_description                         => node.xpath("./b014|./ProductFormDescription").text.presence,
        :epub_type_code                             => node.xpath("./b211|./EpubType"              ).text.presence,
        :epub_type_version                          => node.xpath("./b212|./EpubTypeVersion"       ).text.presence,
        :epub_type_description                      => node.xpath("./b213|./EpubTypeDescription"   ).text.presence,
        :epub_format_code                           => node.xpath("./b214|./EpubFormat"            ).text.presence,
        :epub_format_version                        => node.xpath("./b215|./EpubFormatVersion"     ).text.presence,
        :epub_format_description                    => node.xpath("./b216|./EpubFormatDescription" ).text.presence,
        :epub_source_code                           => node.xpath("./b278|./EpubSource"            ).text.presence,
        :epub_source_version                        => node.xpath("./b279|./EpubSourceVersion"     ).text.presence,
        :epub_source_description                    => node.xpath("./b280|./EpubSourceDescription" ).text.presence,
        :epub_type_note                             => node.xpath("./b277|./EpubTypeNote"          ).text.presence,
        :subtitle                                   => node.xpath("./title/b029|./Title/Subtitle"   ).text.presence,
        :prefix                                     => node.xpath("./title/b030|./Title/TitlePrefix").text.presence,
        :title                                      => node.xpath("./title/b031|./Title/TitleWithoutPrefix").try(:text).presence || node.xpath("./title/b203|./Title/TitleText").try(:text).presence,
        :edition                                    => node.xpath("./b057|./EditionNumber"         ).text.presence,
        :edition_statement                          => node.xpath("./b058|./EditionStatement"      ).text.presence,
        :edition_version_number                     => node.xpath("./b217|./EditionVersionNumber").text.presence,
        #:imprint                                    => Imprint.where(:value => node.xpath("./Imprint/ImprintName").text, :client_id => client_id).first_or_create,
        :publisher_role                             => node.xpath("./publisher/b291|./Publisher/PublishingRole").text,
        #:publishername_id                           => Publishername.where(:publisher_name => node.xpath("./Publisher/PublisherName").text, :client_id => client_id).first_or_create.id,
        #:publication_city                           => City.where(:value => node.xpath("./b209|./CityOfPublication").text).first_or_create.value,
        #:publication_country                        => node.xpath("./b083|./CountryOfPublication"     ).text.presence,
        :publishing_status                          => node.xpath("./b394|./PublishingStatus"         ).text.presence,
        :illustrations_note                         => node.xpath("./b062|./IllustrationsNote"        ).text.presence,
        :pub_date                                   => string_to_date(node.xpath("./b003|./PublicationDate").text.presence),
        :part_number                                => node.xpath("./series/b019|./Series/NumberWithinSeries").text.presence,
        :annual_year                                => node.xpath("./series/b020|./Series/YearOfAnnual"      ).text.presence,
        :client_id                                  => client_id,
        :user_id                                    => user_id,
        :work_id                                    => work_id,
        :pages_roman                                => node.xpath("./b254|./PagesRoman"           ).text.presence,
        :pages_arabic                               => node.xpath("./b255|./PagesArabic"          ).text.presence,
        :copyright_year                             => node.xpath("./b087|./CopyrightYear"        ).text.presence,
        :language                                   => node.xpath("./b059|./LanguageOfText"       ).text.presence,
        :number_of_illustrations                    => node.xpath("./b125|./NumberOfIllustrations").text.presence,
        :internal_reference                         => node.xpath("./a001|./RecordReference"      ).text.presence,
        :unpriced_item_type_code                    => node.xpath("./j192|./UnpricedItemType"     ).text.presence,
        :ancillary_contents_attributes              => ancillary_content_attributes(node,client_id)
        )
      book.no_validation = true
      Book.skip_callback(:save, :after, :update_validations)
      book.save!
      return book
    else
      return nil
    end
  end

  def ancillary_content_attributes(node, client_id)
    contents = []
    node.xpath("Illustrations").each do |i|
      content = Hash.new
      content[:ancillary_content_type_code] = i.at_xpath("IllustrationType").text
      content[:client_id                  ] = client_id
      content[:number                     ] = i.at_xpath("Number").text                      if i.at_xpath("Number")
      content[:description                ] = i.at_xpath("IllustrationTypeDescription").text if i.at_xpath("IllustrationTypeDescription")
      contents << content
    end
    puts contents
    contents
  end

  def onix_work_id_by_type(node,type)
    id = node.xpath("./workidentifier[b201=#{type}]/b244|./WorkIdentifier[WorkIDType=#{type}]/IDValue").text.presence
  end

  def onix_product_title_by_type(node,type)
    node.xpath("./title/b202[. = #{type}]/../b203|./Title/TitleType[. = #{type}]/../TitleText").text.presence
  end

  def onix_main_subject_code_by_type(node,type)
    node.xpath("./mainsubject/b191[. = #{type}]/../b069|./MainSubject/MainSubjectSchemeIdentifier[. = #{type}]/../SubjectCode").text.presence
  end

  def get_work(node, client_id)
    work_istc   = onix_work_id_by_type(node,"11")
    work_istc   = nil unless (work_istc =~ /[(0-9)|(A-E)]{3}[ -][(0-9)]{4}[ -][(0-9)|(A-E)]{8}[ -][(0-9)|(A-E)]{1}/)
    work_isbn13 = onix_work_id_by_type(node,"15")
    #work_isbn13 = node.xpath("./a001|./RecordReference").text.presence # fix for ePubDirect
    work_isbn10 = onix_work_id_by_type(node,"02")
    work_doi    = onix_work_id_by_type(node,"06")
    work_isrc   = onix_work_id_by_type(node,"18")
    work_prop   = onix_work_id_by_type(node,"01")

    work = Work.where(:client_id => client_id, :istc_reference                 => work_istc                 ).first if work_istc
    work = Work.where(:client_id => client_id, :identifying_isbn13             => work_isbn13               ).first if work_isbn13 and work.nil?
    work = Work.where(:client_id => client_id, :identifying_doi                => work_doi                  ).first if work_doi    and work.nil?
    work = Work.where(:client_id => client_id, :identifying_isrc               => work_isrc                 ).first if work_isrc   and work.nil?
    work = Work.where(:client_id => client_id, :registrants_internal_reference => work_prop                 ).first if work_prop   and work.nil?
    work = Work.where(:client_id => client_id, :identifying_isbn13             => ISBN.thirteen(work_isbn10)).first if work_isbn10 and work.nil?
    if work.nil? and ( work_isbn13 or work_isbn10 )
      search_isbn = work_isbn13 || ISBN.thirteen(work_isbn10)
      work        = Book.where(:client_id => client_id, :isbn => search_isbn).first.try(:work)
    end if
    if work.nil?
      title = onix_product_title_by_type(node,"01")
      title = onix_product_title_by_type(node,"00") if title.blank?
      unless title.nil?
        work = Work.where(:client_id => client_id).where("upper(trim(title)) = upper(trim(?))",title).first
        work = Work.new(:client_id => client_id, :title => title) if work.nil?
      end
    end
    onix_main_bic_code   = node.xpath("./b065|./BICMainSubject").text.presence
    onix_main_bic_code   = onix_main_subject_code_by_type(node,"12") if onix_main_bic_code.blank?
    onix_main_bisac_code = node.xpath("./b064|./BASICMainSubject").text.presence
    onix_main_bisac_code = onix_main_subject_code_by_type(node,"10") if onix_main_bisac_code.blank?


    if work.new_record?
      contract = Contract.create!(:client_id => client_id, :contract_name => title, :signed_date => pub_date(node))
    end


    work.assign_attributes( :istc_reference                 => work.istc_reference                 || work_istc ,
                            :identifying_isbn13             => work.identifying_isbn13             || work_isbn13 || ( work_isbn10 ? ISBN.thirteen(work_isbn10) : nil ),
                            :identifying_doi                => work.identifying_doi                || work_doi ,
                            :identifying_isrc               => work.identifying_isrc               || work_isrc,
                            :registrants_internal_reference => work.registrants_internal_reference || work_prop,
                            :main_bic_code                  => work.main_bic_code                  || onix_main_bic_code,
                            :main_bisac_code                => work.main_bisac_code                || onix_main_bisac_code,
                            :contract_id                    => work.contract_id                    || contract.id                                          ,
                            :imprint_id                     => work.imprint_id                     || get_imprint_id(client_id,node)                       ,
                            :publishername_id               => work.publishername_id               || get_publishername_id(client_id,node)                 ,
                            :publication_city               => node.xpath("./b209|./CityOfPublication").text                                                      ,
                            :publication_country            => node.xpath("./b083|./CountryOfPublication").text                                                   ,
                            #:publishing_status              => node.xpath("./PublishingStatus").text.blank? ? '00' : node.xpath("./PublishingStatus").text ,
                            #:publisher_role                 => node.xpath("./Publisher/PublishingRole").text.presence                                 ,
                            :audience_description           => work.audience_description           || node.xpath("./b207|./AudienceDescription").text.presence)

    work.save! if work.changed?

    work
  end

  def pub_date(node)
    string_to_date(node.xpath("./b003|./PublicationDate").text.presence)
  end

  def create_series(node, client_id, book)
    puts "create series"
    node.xpath("./series|./Series").each do |series|
      title                = series.xpath("./title/b203|./Title/TitleText"         ).try(:text).presence || series.xpath("./b018|./TitleOfSeries").try(:text).presence
      title_prefix         = series.xpath("./title/b030|./Title/TitlePrefix"       ).try(:text).presence
      title_without_prefix = series.xpath("./title/b031|./Title/TitleWithoutPrefix").try(:text).presence
      subtitle             = series.xpath("./title/b029|./Title/Subtitle"          ).try(:text).presence
      idtype               = series.xpath("./seriesidentifier/b273|./SeriesIdentifier/SeriesIDType").try(:text).presence
      idvalue              = series.xpath("./seriesidentifier/b244|./SeriesIdentifier/IDValue").try(:text).presence
      unless title_prefix and title_without_prefix
        match                = /^(The |A |An )([^ ].*$)/i.match(title)
        if match
          title_prefix         = match[0].try(:strip)
          title_without_prefix = match[1].try(:strip)
        else
          title_without_prefix = title
        end
      end

      series_name = Seriesname.where(:collection_type      => "Series",
                                     :title_without_prefix => title_without_prefix,
                                     :client_id            => client_id).
                               first_or_create!(
                                     :title_prefix         => title_prefix,
                                     :subtitle             => subtitle    ,
                                     :idtype               => idtype      ,
                                     :idvalue              => idvalue     )



      wsn = WorkSeriesname.where(:work_id       => book.work_id,
                                 :seriesname_id => series_name.id).first_or_create!
      wsn.year_of_annual       = series.xpath("./b020|./YearOfAnnual").text.presence
      wsn.number_within_series = series.xpath("./b019|./NumberWithinSeries").text.presence
    end
  end

  def create_measurement(node, client_id, book)
    puts "create measurement"
    node.xpath("./measure|./Measure").each do |measure|
      measurement = Measurement.new(
        :book_id                    => book.id,
        :measurement_type_id        => MeasurementType.find_by_code(measure.xpath("./c093|./MeasureTypeCode").text).id,
        :measurement                => measure.xpath("./c094|./Measurement").text,
        :measurement_unit_id        => MeasurementUnit.find_by_code(measure.xpath("./c095|./MeasureUnitCode").text).id,
        :client_id                  => client_id
      )
      measurement.save!
    end #end of each measure loop
  end

  def create_imprint(node, client_id)
    get_imprint_id(client_id,node)
  end

  def create_publisher(node, client_id, book)
    get_publishername_id(client_id,node)
  end

  def create_relatedproduct(node, client_id, book)
    puts "create rel pro"
    node.xpath("./relatedproduct|./RelatedProduct").each do |relatedproduct|
      relatedproduct = Relatedproduct.new(
        :book_id                                            => book.id,
        :share                                              => "f",
        :relation_code                                      => relatedproduct.xpath("./h208|./RelationCode").text,
        :id_value                                           => relatedproduct.xpath("./productidentifier/b244[b221=15]|./ProductIdentifier/IDValue[ProductIDType=15]").text,
        :format                                             => relatedproduct.xpath("./b012|./ProductForm").text,
        :client_id                                          => client_id
        )
      relatedproduct.save!
    end
  end

  def create_audience(node, client_id, work, book)
    node.xpath("./audience|./Audience").each do |audience|
      aud = Audience.where(
        :work_id                                => work.id,
        :audience_code_type                     => audience.xpath("./b204|./AudienceCodeType").text,
        :audience_code_value                    => audience.xpath("./b206|./AudienceCodeValue").text,
        :client_id                              => client_id
        ).first_or_create
      Bookaudience.where(:book_id => book.id, :audience_id => aud.id).first_or_create
    end
  end

  def create_language(node, client_id, book)
    node.xpath("./productidentifier/language|./ProductIdentifier/Language").each do |language|
      options = {:code    => language.xpath("b252|LanguageCode").text,
                 :role    => language.xpath("b253|LanguageRole").text}
      options[:country] = language.xpath("b251|CountryCode" ).text if language.xpath("b251|CountryCode" ).text.presence

      book.ensure_language=options
    end
  end


  def create_audience_if_audience_code_provided(node, client_id, work, book)
    puts "create aud cod"
    node.xpath("./b073|./AudienceCode").each do |audience|
      work_audience = Audience.where(:work_id             => work.id,
                                     :audience_code_type  => "01",
                                     :audience_code_value => audience.text,
                                     :client_id           => client_id).first_or_create
      Bookaudience.where(:client_id   => client_id,
                         :book_id     => book.id  ,
                         :audience_id => work_audience.id).first_or_create

    end
  end

  def create_extent(node, client_id, book)
    puts "create ext"
    node.xpath("./extent|./Extent").each do |extent|
      extent_type  = ExtentType.find_by_code(extent.xpath("./b218|./ExtentType").text.presence)
      extent_unit  = ExtentUnit.find_by_code(extent.xpath("./b220|./ExtentUnit").text.presence)
      extent_value = extent.xpath("./b219|./ExtentValue").text.presence
      if extent_type and extent_unit and extent_value
        Extent.create(
           :book_id                              => book.id,
           :extent_type_id                       => extent_type.id,
           :extent_value                         => extent.xpath("./b219|./ExtentValue").text,
           :extent_unit_id                       => extent_unit.id,
           :client_id                            => client_id
           )
      end
    end
  end

  def create_rights(node, client_id, book)
    puts "create rights"
    node.xpath("./salesrights|./SalesRights").each do |right|

      territory_list     = Rightlist.clean_list(right.xpath("./b388|./RightsTerritory").text)
      country_list       = Rightlist.clean_list(right.xpath("./b090|./RightsCountry"  ).text)

      rightlistterritory = Rightlist.where(          :client_id => client_id     ,
                                                     :value     => territory_list,
                                                     :area      => "Region"      ).
                                     first_or_create(:code      => territory_list)

      rightlistcountry   = Rightlist.where(          :client_id => client_id     ,
                                                     :value     => territory_list,
                                                     :area      => "Country"     ).
                                     first_or_create(:code      => territory_list)

      rightlistterritory.save! if rightlistterritory.changed?
      rightlistcountry.save!   if rightlistcountry.changed?

      new_right = Right.new(
        :book_id                        => book.id,
        :sales_rights_type              => right.xpath("./b089|./SalesRightsType").text.presence,
        :countries_included             => rightlistcountry.try(:id),
        :regions_included               => rightlistterritory.try(:id),
        :client_id                      => client_id
        )
      new_right.save!
    end
  end

  def create_not_for_sale(node, client_id, book)
    puts "create not for sale"
    node.xpath("./notforsale|./NotForSale").each do |right|
      rightlistterritory = Rightlist.where(:value     => right.xpath("./b388|./RightsTerritory").text,
                                           :client_id => client_id,
                                           :area      => "Region").
                                     first_or_create(
                                           :code => "Created on ONIX import: #{right.xpath("./b388|./RightsTerritory").text}")

      rightlistcountry   = Rightlist.where(:value     => right.xpath("./b090|./RightsCountry").text,
                                           :client_id => client_id,
                                           :area      => "Country").
                                     first_or_create(
                                           :code => "Created on ONIX import: #{right.xpath("./b090|./RightsCountry").text}")

      rightlistcountry.right_countries.build(:country_code_id => CountryCode.find_by_name(rightlistcountry.value).try(:id))
      rightlistcountry.save!

      rightlistterritory.rightregions.build(:region_code_id => RegionCode.find_by_name(rightlistterritory.value).try(:id))
      rightlistterritory.save!

      new_right = Right.new(
        :book_id                        => book.id,
        :sales_rights_type              => right.xpath("./b089|./SalesRightsType").text.presence,
        :countries_excluded             => rightlistcountry.try(:id),
        :regions_excluded               => rightlistterritory.try(:id),
        :client_id                      => client_id
        )
      new_right.save!
    end
  end

  def create_extent_if_number_of_pages_provided(node, client_id, book)
    puts "create extent"
    unless node.xpath("./b061|./NumberOfPages").text.blank?
      extent = Extent.new(
        :book_id                              => book.id,
        :extent_type_id                       => 2,
        :extent_value                         => node.xpath("./b061|./NumberOfPages").text.presence,
        :extent_unit_id                       => 1,
        :client_id                            => client_id
        )
      extent.save!
    end
  end

  def create_supporting_resource(node, client_id, work, book)
    puts "create sup res"
    node.xpath("./mediafile|./MediaFile").each do |supportingres|
      if supportingres.xpath("./f116|./MediaFileLinkTypeCode").text == "01" #if it's a URL
        puts "url is #{supportingres.xpath("./f117|./MediaFileLink").text}"
        u      = supportingres.xpath("./f117|./MediaFileLink").text.gsub(" ", "%20")
        u = u.gsub(/\/native\//,"/thumbnail/")
        url = URI.parse(u)
        req = Net::HTTP.new(url.host, url.port)
        res = req.request_head(url.path)
        if res.code == "200"
          resource = Supportingresource.new(
          :work_id                => work.id,
          :resource_content_type  => supportingres.xpath("./f114|./MediaFileTypeCode").text.presence,
          :resource_form          => supportingres.xpath("./f115|./MediaFileFormatCode").text.presence,
          :resource_link          => supportingres.xpath("./f117|./MediaFileLink").text.presence,
          :client_id              => client_id,
          :image                  => open(url)
          )
          if resource.save!
            Booksupportingresource.where(:book_id => book.id, :supportingresource_id => resource.id).first_or_create
          else
            break
          end
        end # of whether it's a 200
      else
        puts "not a url"
        break
      end
    end
  end

  def create_prize(node, client_id, work, book)
    puts "create prize"
    node.xpath("./Prize").each do |prize_node|
      prize_name    = prize_node.xpath("./g126|./PrizeName"   ).text.presence
      prize_year    = prize_node.xpath("./g127|./PrizeYear"   ).text.presence
      prize_country = prize_node.xpath("./g128|./PrizeCountry").text.presence
      prize_code    = prize_node.xpath("./g129|./PrizeCode"   ).text.presence
      prize_jury    = prize_node.xpath("./g343|./PrizeJury"   ).text.presence

      prize = Prize.where(:work_id    => work.id,
                          :client_id  => client_id,
                          :prize_name => prize_name,
                          :prize_year => prize_year).
                  first_or_create(:prize_country => prize_country,
                                  :prize_code    => prize_code   ,
                                  :prize_jury    => prize_jury   )
      prize.book_prizes.where(:book_id   => book.id)

    end
  end

  def create_identifier(node, client_id, book)
    puts "create id"
    node.xpath("./productidentifier|./ProductIdentifier").each do |identifier|
      productcode = Productcode.new(
        :book_id   => book.id,
        :idtype    => identifier.xpath("./b221|./ProductIDType").text,
        :idvalue   => identifier.xpath("./b244|./IDValue").text,
        :client_id => client_id
        )
      productcode.save!
    end
  end

  def create_subjects(node, client_id, work, book)
    puts "create subj"
    node.xpath("./subject|./Subject").each do |subject|
      if subject.xpath("./b067|./SubjectSchemeIdentifier").text == "10" #bisac
        subj = Bisacsubject.where(:work_id              => work.id,
                                  :code                 => subject.xpath("./b069|./SubjectCode").text,
                                  :value                => subject.xpath("./b070|./SubjectHeadingText").text,
                                  :client_id            => client_id
                                  ).first_or_create
        Bookbisacsubject.where(:book_id => book.id, :bisacsubject_id => subj.id).first_or_create
      elsif  subject.xpath("./b067|./SubjectSchemeIdentifier").text == "12" #bic
        subj = Bicsubject.where(
        :work_id              => work.id,
        :code                 => subject.xpath("./b069|./SubjectCode").text,
        #:value                => subject.xpath("./SubjectHeadingText").text,
        :client_id            => client_id
        ).first_or_create
        Bookbicsubject.where(:book_id => book.id, :bicsubject_id => subj.id).first_or_create
      else
        subj = Subject.where(
        :work_id                    => work.id,
        :subject_scheme_identifier  => subject.xpath("./b067|./SubjectSchemeIdentifier").text,
        :subject_code               => subject.xpath("./b069|./SubjectCode").text,
        :subject_description        => subject.xpath("./b070|./SubjectHeadingText").text,
        :client_id                  => client_id
        ).first_or_create
        Booksubject.where(:book_id => book.id, :subject_id => subj.id).first_or_create
        # puts "subj saved : #{subj.id}"
      end
    end #end of main subject loop
  end

  def create_text(node, client_id, work, book)
    puts "create text"
    node.xpath("./othertext|./OtherText").each do |text|
      othertext = Marketingtext.where(:work_id        => work.id,
                                      :legacy_code    => text.xpath("./d102|./TextTypeCode").text,
                                      :marketing_text => text.xpath("./d104|./Text").text,
                                      :client_id      => client_id,
                                      :text_type      => '01').
                                first_or_create(:pull_quote=> text.xpath("./d104|./Text").text)

      Bookmarketingtext.where(:book_id => book.id, :marketingtext_id => othertext.id).first_or_create
    end #end of each other text loop
  end

  def create_supply(node, client_id, book)
    puts "create supply"
    node.xpath("./supplydetail|./SupplyDetail").each do |supplydetail|
      supplier_name    = supplydetail.xpath("./j137|./SupplierName").text.presence

      if true #["Oberon Press", "Oberon Books", 'Marston Book Services Ltd'].include? supplier_name

        supplier = Contact.find_or_create_supplier_by_name(client_id, supplier_name)

        sd = Supplydetail.create!(:book_id => book.id,
                :supply_to_countries          => supplydetail.xpath("./j138|./SupplyToCountry"    ).text.to_clean_array,
                :supply_to_region             => supplydetail.xpath("./j139|./SupplyToRegion"  ).text.presence,
                :supply_to_countries_excluded => supplydetail.xpath("./j140|./CountriesExcluded"  ).text.to_clean_array,
                :supply_to_territories        => supplydetail.xpath("./j397|./SupplyToTerritory").text.to_clean_array,
                :supplier_role                => supplydetail.xpath("./j292|./SupplierRole"       ).text.presence || "00",
                :contact_id                   => supplier.id,
                :product_availability         => supplydetail.xpath("./j396|./ProductAvailability").text.presence,
                :availability_code            => supplydetail.xpath("./j141|./AvailabilityCode"   ).text.presence,
                :unpriced_item_type           => supplydetail.xpath("./j192|./UnpricedItemType"   ).text.presence,
                :client_id                    => client_id)
      end

      supplydetail.xpath("./price|./Price").each do |price|
        if true #price.xpath("./CurrencyCode"     ).text == "GBP"
          currency_id = Currency.where(:client_id    => client_id,
                                       :currencycode => price.xpath("./j152|./CurrencyCode").text.presence).
                                 first_or_create
          price = Price.where(
            :book_id                          => book.id,
            :price_type                       => price.xpath("./j148|./PriceTypeCode"             ).text.presence,
            :price_amount                     => price.xpath("./j151|./PriceAmount"               ).text.presence,
            :currency_code                    => price.xpath("./j152|./CurrencyCode"              ).text.presence,
            :tax_rate_code                    => price.xpath("./j153|./TaxRateCode1"              ).text.presence,
            :tax_rate_percent                 => price.xpath("./j154|./TaxRatePercent1"           ).text.presence,
            :taxable_amount                   => price.xpath("./j155|./TaxableAmount1"            ).text.presence,
            :tax_value                        => price.xpath("./j156|./TaxAmount1"                ).text.presence,
            :tax_rate_code_2                  => price.xpath("./j157|./TaxRateCode2"              ).text.presence,
            :tax_rate_percent_2               => price.xpath("./j158|./TaxRatePercent2"           ).text.presence,
            :taxable_amount_2                 => price.xpath("./j159|./TaxableAmount2"            ).text.presence,
            :tax_value_2                      => price.xpath("./j160|./TaxAmount2"                ).text.presence,
            :price_effective_from             => price.xpath("./j161|./PriceEffectiveFrom"        ).text.presence.try(:strftime,"%Y%m%d"),
            :price_effective_to               => price.xpath("./j162|./PriceEffectiveUntil"       ).text.presence.try(:strftime,"%Y%m%d"),
            :discount_code_type               => price.xpath("./discountcoded/j363|./DiscountCoded/DiscountCodeType").text.presence,
            :discount_code                    => price.xpath("./discountcoded/j364|./DiscountCoded/DiscountCode"    ).text.presence,
            :client_id                        => client_id).first_or_create
        end
      end
    end #end of each supply loop
  end

  def create_contact(node, client_id, work, book)
    puts "create contact"
    node.xpath("./contributor|./Contributor").each do |contrib|
      contact_id = 0
      if contrib.xpath("./b047|./CorporateName").text.presence
        contact = Contact.where(:corporate_name => contrib.xpath("./b047|./CorporateName").text.strip,
                                :client_id      => client_id).
                first_or_create!( :person_name_inverted       => contrib.xpath("./b047|./CorporateName"   ).text.strip,
                                  :biographical_note          => contrib.xpath("./b044|./BiographicalNote").text.strip.presence)
        contact_id = contact.id
      else
        contact = Contact.where(:client_id                  => client_id,
                                :person_name_inverted       => contrib.xpath("./b037|./PersonNameInverted"    ).text.strip).
               first_or_create!(:keynames                   => ( contrib.xpath("./b040|./KeyNames"            ).text.presence ||
                                                                   contrib.xpath("./b037|./PersonNameInverted"  ).text.split(/[,]+/)[0].try(:strip)),
                                :person_name                => contrib.xpath("./b036|./PersonName"            ).text.strip.presence,
                                :names_before_key           => ( contrib.xpath("./b039|./NamesBeforeKey"      ).text.presence ||
                                                                   contrib.xpath("./b037|./PersonNameInverted").text.split(/[,]+/)[1].try(:strip)),
                                :titles_before_names        => contrib.xpath("./b038|./TitlesBeforeNames"     ).text.strip.presence,
                                :biographical_note          => contrib.xpath("./b044|./BiographicalNote"      ).text.strip.presence)
        contrib.xpath(".//persondate|.//PersonDate").each do |persondate|
          date_type        = persondate.xpath("/b305|/PersonDateRole").text == "007" ? "birth" : "death"
          date_format      = persondate.xpath("/b305|/PersonDateRole").text == "008" ? "date"  : "year"
          target_attribute = date_format + "_of_" + date_type
          contact.update_attribute(target_attribute.to_sym, persondate.xpath("/b306|/Date"))
        end

        contact_id = contact.id
      end

      ContactContactType.where(:contact_id => contact_id, :contact_type_id => 1).first_or_create

      unless contrib.xpath("./professionalaffiliation/b046|./ProfessionalAffiliation/Affiliation"        ).text.blank?
        Contactfeature.where(:contact_id      => contact_id,
                             :feature_type    => "Professional association",
                             :feature_content => contrib.xpath("./professionalaffiliation/b046|./ProfessionalAffiliation/Affiliation").text.strip.presence,
                             :feature_title   => contrib.xpath("./professionalaffiliation/b045|./ProfessionalAffiliation/ProfessionalPosition").text.strip.presence
                             ).first_or_create
      end

      onix_sequence_number  = contrib.xpath("./b034|./SequenceNumber").text.to_i

      sequence_number_taken = Workcontact.where(:work_id           => work.id  ,
                                                :client_id         => client_id,
                                                :sequence_number   => onix_sequence_number).
                                          where.not(:contact_id        => contact_id).exists?

      if sequence_number_taken
        onix_sequence_number =  [Workcontact.where(:work_id => work.id, :client_id => client_id).maximum(:sequence_number), 999].max + 1
      end

      language_codes = []
      country_codes  = []
      region_codes   = []


      contrib.xpath("./b252|./LanguageCode").each{|language_code| language_codes << language_code.text} if contrib.xpath("./b252|./LanguageCode").first
      contrib.xpath("./b251|./CountryCode" ).each{|country_code | country_codes  << country_code.text } if contrib.xpath("./b251|./CountryCode" ).first
      contrib.xpath("./b398|./RegionCode"  ).each{|region_code  | region_codes   << region_code.text  } if contrib.xpath("./b398|./RegionCode"  ).first

      workcontact = Workcontact.where(:work_id           => work.id,
                                      :client_id         => client_id,
                                      :sequence_number   => onix_sequence_number).
                                first_or_create!(
                                      :language_code_array => language_codes,
                                      :country_code_array  => country_codes ,
                                      :region_code_array   => region_codes  ,
                                      :contact_id          => contact_id,
                                      :work_contact_role   => contrib.xpath("./b035|./ContributorRole").text)


      bookcontact = Bookcontact.where(:workcontact_id   => workcontact.id,
                                      :book_id          => book.id       ,
                                      :sequence_number  => contrib.xpath("./b034|./SequenceNumber" ).text,
                                      :contributor_role => contrib.xpath("./b035|./ContributorRole").text).first_or_create!

      # puts "bookcontactid saved: #{bookcontact.id}"
      contrib.xpath("./website|./Website"   ).each do |website|
        WebLink.create(:client_id     => client_id,
                       :website_role  => website.xpath("./b367|./WebsiteRole"        ).text,
                       :website_link  => website.xpath("./b295|./WebsiteLink"        ).text,
                       :note          => website.xpath("./b294|./WebsiteDescription" ).text,
                       :linkable_id   => contact_id,
                       :linkable_type => "Contact")
      end # end of each website
    end # end of each contrib
  end #end of each contact method
end # end of class

