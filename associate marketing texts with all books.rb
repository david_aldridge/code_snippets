Rails.logger.level=0
ActiveRecord::Base.transaction do
  Book.includes(:marketingtexts, work: :marketingtexts).each do |b|
    b.marketingtexts = b.work.marketingtexts
  end
  # raise ActiveRecord::Rollback
end

