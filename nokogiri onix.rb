Find products without an ISBN13

doc = Nokogiri::XML(File.open("/Users/david/Documents/Bibliocloud/Clients/Amber/BROBKSONIX20170619143949_original_1fd81dede6528176f0a36f4b46a0955a.xml")) ; 0
doc = Nokogiri::XML(open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/purdue_2.xml")) ; 0

Find products without an ISBN13

doc.
  xpath("//Product").
  select do |node|
    node.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").nil?
  end

ISBNs and copyright years

hash = doc.xpath("//Product").each_with_object({}) do |node, hsh|
  hsh[node.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").text] = node.at_xpath("CopyrightYear").text
end.compact

client = Client.is_purdue
hash.each do |isbn, copyright_year|
  work = client.books.find_by(isbn: isbn).work
  if work.copyright_year.nil? || work.copyright_year.to_i < copyright_year.to_i
    work.copyright_year = copyright_year
    work.save
  end
end

doc.xpath("")
