@doc = Nokogiri::XML(open(@file_name), nil, 'ISO-8859-1') do |config|
  config.default_xml.noblanks
end

csv_string = CSV.generate do |csv|
	@doc.xpath("/xmlns:ONIXMessage/xmlns:Product").each_with_index do |product_node, i|
		product_num         = i + 1
		isbn                = isbn(product_node)
		product_form_detail = product_node.at_xpath('xmlns:DescriptiveDetail/xmlns:ProductFormDetail').try(:text)
	    product_node.xpath("xmlns:ProductSupply/xmlns:SupplyDetail/xmlns:Price").each_with_index do |price_node, ix|
	    	csv << [
	    	  product_num,
	    	  isbn,
	    	  product_form_detail,
	    	  ix,
	    	  price_node.at_xpath("xmlns:PriceType").try(:text),
	    	  price_node.at_xpath("xmlns:PriceQualifier").try(:text),
	    	  price_node.at_xpath("xmlns:DiscountCoded/xmlns:DiscountCode").try(:text),
	          price_node.at_xpath("xmlns:DiscountCoded/xmlns:DiscountCodeType").try(:text),
	          price_node.at_xpath("xmlns:DiscountCoded/xmlns:DiscountCodeTypeName").try(:text),
	          price_node.at_xpath("xmlns:CurrencyCode").try(:text),
	          price_node.at_xpath("xmlns:PriceAmount").try(:text),
	          price_node.at_xpath("xmlns:Territory/xmlns:CountriesIncluded").try(:text)]
	    end
	end
end

puts csv



