client = Client.is_liverpool

PaperTrail::Version.where(:client_id => client.id).
                     where("created_at > ?", Date.today - 6.days).
                     where(item_type: "Work").
                     where(event: 'destroy').
                     map{|v| [v.id, v.created_at, v.reify.id, v.reify.title]}

work_deletion = PaperTrail::Version.find(2580030)

deletions = PaperTrail::Version.where(:client_id => client.id).
                                where("created_at between ? and ?",
                                       work_deletion.created_at - 10.seconds,
                                       work_deletion.created_at).
                                where(event: 'destroy')

pp deletions.order(:created_at).map{|v| [v.id, v.created_at, v.item_id, v.item_type]}

e.g.

[[2580011, Tue, 27 Sep 2016 13:45:47 UTC +00:00, 43655, "Audience"],
 [2580013, Tue, 27 Sep 2016 13:45:47 UTC +00:00, 5739, "RoyaltySpecifier"],
 [2580014, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186630, "Price"],
 [2580015, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186631, "Price"],
 [2580016, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 86404, "Book"],
 [2580017, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 5740, "RoyaltySpecifier"],
 [2580018, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186635, "Price"],
 [2580019, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186633, "Price"],
 [2580020, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186927, "Price"],
 [2580021, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 86403, "Book"],
 [2580022, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 5738, "RoyaltySpecifier"],
 [2580023, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186634, "Price"],
 [2580024, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 186632, "Price"],
 [2580025, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 86405, "Book"],
 [2580026, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 178208, "Marketingtext"],
 [2580027, Tue, 27 Sep 2016 13:45:48 UTC +00:00, 178207, "Marketingtext"],
 [2580028, Tue, 27 Sep 2016 13:45:49 UTC +00:00, 178206, "Marketingtext"],
 [2580029, Tue, 27 Sep 2016 13:45:49 UTC +00:00, 77432, "Workcontact"],
 [2580030, Tue, 27 Sep 2016 13:45:49 UTC +00:00, 55286, "Work"]]




  module CustomClient
    def client
      Client.is_liverpool
    end
  end

  Book.send(:include, CustomClient)





ActiveRecord::Base.transaction do
  deletions.order(id: :desc).each do |version|
    obj = version.reify
    obj.save!
  end
end



