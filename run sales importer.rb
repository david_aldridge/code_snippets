OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

import = Google::Import.new(
  client:                  Client.is_bds,
  spreadsheet_key:         "1o1zKFbojbk25Q6J3dxq34JeZByuLQ7XLKatBWFkzT2I",
  worksheet_name:          "Sales",
  import_class:            Google::SalesRow,
  response_column_heading: "System Messages",
  no_errors_response:      "OK",
  rollback_on_error:       true
)

import.call




import = Google::Import.new(
  client:                  Client.is_bds,
  spreadsheet_key:         "1FnOKpEHLSVaJIQgw_iqi_JqXgdIERzw7Q6whFzKgb_s",
  worksheet_name:          "Rates",
  import_class:            Google::RoyaltySpecifierRow,
  response_column_heading: "System Messages",
  no_errors_response:      "OK",
  rollback_on_error:       true
)

import.call

Client.is_bds.sales.includes(:book, :channel).map{|sale| [sale.book, sale.channel]}.uniq.size
Client.is_bds.book_channels.includes(:book, :channel).map{|bc| [bc.book, bc.channel]}.size


def setup_book_channels
  ActiveRecord::Base.transaction do
    Client.is_vertebrate.sales.map{|sale| [sale.book, sale.channel]}.uniq.each do |book, channel|
      next if channel.in? book.channels
      masterchannel = channel.masterchannel
      book.masterchannels << masterchannel unless masterchannel.in? book.masterchannels
      book_masterchannel = book.book_masterchannels.find_by(masterchannel_id: masterchannel.id)
      book_masterchannel.channels << channel
    end
  end
end

ActiveRecord::Base.transaction do
  Client.is_vertebrate.sales.map{|sale| [sale.book, sale.channel]}.uniq.each do |book, channel|
    next if channel.in? book.channels

    masterchannel = channel.masterchannel
    book.masterchannels << masterchannel unless masterchannel.in? book.masterchannels
    book_masterchannel = book.book_masterchannels.find_by(masterchannel_id: masterchannel.id)
    book_masterchannel.channels << channel
  end
end

Client.is_vertebrate.sales.map{|sale| [sale.book, sale.channel]}.uniq.count do |book, channel|
  !channel.in? book.channels
end



Client.is_vertebrate.sales.
  where.not(
    BookChannel.
      joins(:book_masterchannel).
      where("book_channels.channel_id = sales.channel_id and book_masterchannels.book_id = sales.book_id").
      exists
  )


Sale.where.not(
  BookChannel.
    joins(:book_masterchannel).
    where("book_channels.channel_id = sales.channel_id and book_masterchannels.book_id = sales.book_id").
    exists
  ).count


Sale.includes(book: [:masterchannels, channels: :masterchannel], channel: :masterchannel).where.not(
  BookChannel.
    joins(:book_masterchannel).
    where("book_channels.channel_id = sales.channel_id and book_masterchannels.book_id = sales.book_id").
    exists
  ).map{|sale| [sale.book, sale.channel]}.uniq.each do |book, channel|
    book.masterchannels << channel.masterchannel unless channel.masterchannel.in? book.masterchannels
    book_masterchannel = book.book_masterchannels.detect{|bmc| bmc.masterchannel_id = masterchannel.id}
    book_masterchannel.channels << channel
  end
end
