client = Client.is_canelo


books = client.books.includes(:prices) ; 0


target_prices =
  [
    :current_usd_exctax_consumer_price,
    :current_eur_inctax_consumer_price,
    :current_cad_exctax_consumer_price,
    :current_aud_exctax_consumer_price,
    :current_nzd_exctax_consumer_price,
    :current_inr_exctax_consumer_price,
    :current_jpy_inctax_consumer_price
  ]
price_map =
  {
    0.99 => [ 1.99 , 1.99 , 1.99 ,  1.99 ,  3.99 , 149 , 199 ],
    1.49 => [ 2.99 , 2.99 , 2.99 ,  3.99 ,  4.99 , 149 , 299 ],
    1.99 => [ 2.99 , 2.99 , 3.99 ,  3.99 ,  4.99 , 149 , 299 ],
    2.99 => [ 3.99 , 3.99 , 4.99 ,  5.99 ,  6.99 , 149 , 399 ],
    3.99 => [ 5.99 , 4.99 , 6.99 ,  7.99 ,  8.99 , 199 , 499 ],
    4.99 => [ 6.99 , 6.99 , 7.99 ,  8.99 , 10.99 , 249 , 599 ],
    5.99 => [ 8.99 , 7.99 , 9.99 ,  9.99 , 10.99 , 299 , 699 ],
    6.99 => [ 9.99 , 8.99 , 9.99 , 11.99 , 12.99 , 349 , 799 ],
    9.99 => [ 9.99 , 9.99 , 9.99 , 11.99 , 12.99 , 499 , 999 ],
  }
price_map =
  {
    0.99 => [ 0.99 , 1.99 , 1.99 , 1.99 , 3.99 , 149 , 199],
    1.49 => [ 1.99 , 2.99 , 1.99 , 3.99 , 4.99 , 149 , 299],
    1.99 => [ 2.99 , 2.99 , 2.99 , 3.99 , 4.99 , 149 , 299],
    2.99 => [ 2.99 , 3.99 , 3.99 , 5.99 , 6.99 , 149 , 399],
    3.99 => [ 3.99 , 4.99 , 4.99 , 7.99 , 8.99 , 199 , 499],
    4.99 => [ 4.99 , 6.99 , 5.99 , 8.99 , 10.99 , 249 , 599],
    5.99 => [ 5.99 , 7.99 , 6.99 , 9.99 , 10.99 , 299 , 699],
    6.99 => [ 6.99 , 8.99 , 7.99 , 11.99 , 12.99 , 349 , 799],
    9.99 => [ 9.99 , 9.99 , 9.99 , 11.99 , 12.99 , 499 , 999]
  }

price_map.each do |gbp_price, other_prices|
  books.select{|b| b.current_gbp_inctax_consumer_price == gbp_price}.each do |book|
    target_prices.each_with_index do |method, idx|
      book.send("#{method}=".to_sym, other_prices[idx])
    end
  end
end ; 0



target_prices =
  [
    :current_jpy_inctax_consumer_price
  ]

price_map =
  {
    0.99 => [ 199 ],
    1.49 => [ 299 ],
    1.99 => [ 299 ],
    2.99 => [ 399 ],
    3.99 => [ 499 ],
    4.49 => [ 499 ],
    4.99 => [ 599 ],
    5.49 => [ 599 ],
    5.99 => [ 699 ],
    6.99 => [ 799 ],
    9.99 => [ 999 ],
  }

Client.is_canelo.books.select{|b| b.current_gbp_inctax_library_price = 10}.each {|b| b.current_gbp_inctax_library_price=12.99 }
