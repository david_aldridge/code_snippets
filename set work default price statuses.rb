
Rails.logger.level=1

Client.find(404).books.includes(:work => :books).where("pub_date <= ?", Date.today).map(&:work).uniq.each do |work|
  work.update_attributes(:default_price_status => Price::PRICE_STATUS_FIRM)
end


Client.find(404).works.where(default_price_status: Price::PRICE_STATUS_UNSPECIFIED).each do |work|
  work.update_attributes(:default_price_status => Price::PRICE_STATUS_PROVISIONAL)
end ; 0
