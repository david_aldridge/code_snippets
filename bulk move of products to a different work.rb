changes = [
  %w(9780746309094 9780746311516),
  %w(9780746308592 9780746312810),
  %w(9780746309452 9780746310335),
  %w(9780746307090 9780746307045),
  %w(9780746307939 9780746307922),
  %w(9780746308813 9780746310632),
  %w(9780853835189 9780582010017),
  %w(9780746310427 9780746310472),
  %w(9780746312179 9780746308547),
  %w(9780582012417 9780746309582),
  %w(9780746307694 9780582012875 9780746307595),
  %w(9780746309421 9780746311233),
  %w(9780746312247 9780746307052),
  %w(9780746307038 9780746307083),
  %w(9780746310175 9780746309759),
  %w(9780746307601 9780746307519),
  %w(9780746307489 9780746307618),
  %w(9780746311387 9780746311332),
  %w(9780746310373 9780746309537),
  %w(9780746308332 9780582011755),
  %w(9780746308288 9780746308882),
  %w(9780746307823 9780746311059),
  %w(9780746307496 9780746307632),
  %w(9780746308615 9780746311424),
  %w(9780746310915 9780746308530),
  %w(9780746310571 9780746309506),
  %w(9780746310878 9780746309605),
  %w(9780746308066 9780746308059),
  %w(9780746309247 9780746310908),
  %w(9780746310960 9780746309711),
  %w(9780746308332 9780582011755),
  %w(9780746307267 9780746307212),
  %w(9780746308301 9780746308929 9780746308301),
  %w(9780746307311 9780746307366),
  %w(9780746311288 9780746308820)
]

client = Client.is_liverpool

changes.each do |change|
  target_work = client.books.find_by(isbn: change.last)&.work
  raise "Cannot find #{change.last}" unless target_work
  change[0..-2].each do |from_isbn|
    from_book = client.books.find_by(isbn: from_isbn)
    raise "Cannot find #{from_isbn}" unless from_book
    from_book.work                = target_work
    from_book.workcontacts        = target_work.workcontacts
    from_book.marketingtexts      = target_work.marketingtexts
    from_book.supportingresources = target_work.supportingresources
    from_book.audiences           = target_work.audiences
    from_book.save
  end
end




work_from.books.each do |book|
  book.work                = work_to
  book.workcontacts        = work_to.workcontacts
  book.marketingtexts      = work_to.marketingtexts
  book.supportingresources = work_to.supportingresources
  book.audiences           = work_to.audiences
  book.save
end

