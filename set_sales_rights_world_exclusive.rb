ActiveRecord::Base.transaction do 
  client = Client.where(:webname => "snowbooks").first
  user_id = client.users.first.id
  client.contracts.each do |contract|
    SalesRight.create(
      :contract_id      => contract.id,
    :sales_rights_type  => "01",
    :regions_included   => ["WORLD"],
    :client_id          => contract.client_id,
    :user_id            => user_id)
  end
end

