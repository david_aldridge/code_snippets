class GoogleSheetProductImport

  def initialize(spreadsheet_key, client)
    @connection    = GoogleDriveConn.new
    @sheet         = @connection.session.spreadsheet_by_key(spreadsheet_key)
    @metadata      = @sheet.worksheet_by_title("Metadata").list
    @client        = Client.find_by(:webname => webname)
  end

  def import
    @metadata.each do |metadata|
      if book = @client.books.find_by(:isbn => metadata["isbn"])
        metadata["book_id"] = book.id
        if current_absolute_pages = book.absolute_pages
          unless current_absolute_pages = metadata["isbn"]
    end
    @author_sheet.save
    @products.each do |product|
      [ product["Creator One"].presence,
        product["Creator Two"].presence,
        product["Creator Three"].presence].compact.each do |name|
          Contact.where(:client_id   => @client.id, :person_name => name).first_or_create!
      end
    end
  end

  def import_publishernames
    @client.publishernames = self.publishernames
  end

  def import_imprints
    @client.imprints       = self.imprints
  end

  def import_seriesnames
    current_series       = @client.seriesnames.pluck(:title_without_prefix).map{|x| x.upcase}
    @client.seriesnames  += (self.seriesnames + self.sets).reject{|s| current_series.include?(s.title_without_prefix.upcase)}
  end

  def import_contracts
    current_contracts     = @client.contracts.pluck(:contract_name).map{|x| x.upcase.strip}
    @client.contracts     += self.contracts.reject{|s| current_contracts.include?(s.contract_name.upcase.strip)}
  end

  def import_works
    self.works.each do |work|
      @client.works << work unless Work.find_by(:contract_id => work.contract_id)
    end
  end

  def import_books
    self.books.each do |book|
      unless book.valid?
        puts book.isbn
        puts book.errors.messages
      end
      @client.books  << book
    end
  end

  def seriesnames
    get_uniq_pair("Series Title","Series Desc").
      select{|x| x[0].presence}.
      map{|s| Seriesname.new(:title_without_prefix => s[0],
                               :short_description    => s[1].length >  350 ? nil : s[1],
                               :long_description     => s[1].length <= 350 ? nil : s[1],
                               :collection_type      => "Series")}
  end

  def publishernames
    get_uniq("Publisher").map{|p| Publishername.new(:publisher_name => p)}
  end

  def imprints
    get_uniq("Imprint").map{|i| Imprint.new(:value => i)}
  end

  def sets
    get_uniq_trip("Title","Subtitle", "Vol No").
      select{|x| x[2].presence}.
      map{|x| [x[0], x[1]]}.
      uniq.
      map{|s| Seriesname.new(:title_without_prefix => s[0],
                             :subtitle             => s[1],
                             :collection_type      => "Sets")}
  end

  def contracts
    get_uniq_pair("Title", "Vol No").map{ |s| Contract.new(:contract_name => [s[0].presence, s[1].presence].compact.join(": ") ) }
  end

  def works
    @products.map do |product|
      Work.new(:client      => @client,
               :contract    => @client.contracts.find_by(:contract_name => [product["Title"].strip.presence, product["Vol No"].strip.presence].compact.join(": ")),
              :title        => [product["Title"].strip.presence, product["Vol No"].strip.presence].compact.join(": "),
              :subtitle     => product["Subtitle"].strip.presence                                           ,
              :publishername => @client.publishernames.find_by(:publisher_name => product["Publisher"].strip),
              :imprint      => @client.imprints.find_by(:value => product["Imprint"])                 ,
              :workcontacts => [Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator One"].strip).first,
                                                :work_contact_role => product["Creator One Type"].strip.presence || "A01",
                                                :sequence_number   => 1),
                                Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator Two"].strip).first,
                                                :work_contact_role => product["Creator Two Type"].strip.presence || "A01",
                                                :sequence_number   => 2),
                                Workcontact.new(:client            => @client,
                                                :contact           => @client.contacts.fuzzy_find_person(product["Creator Three"].strip).first,
                                                :work_contact_role => product["Creator Three Type"].strip.presence || "A01",
                                                :sequence_number   => 3)].reject{|a| a.contact.nil?},
              :work_seriesnames => [WorkSeriesname.new(:seriesname => @client.seriesnames.where(:title_without_prefix => product["Series Title"].strip).first),
                                    WorkSeriesname.new(:seriesname => @client.seriesnames.where(:title_without_prefix => product["Title"].strip).first,
                                                       :number_within_series => product["Vol No"])].reject{|x| x.seriesname_id.nil?},
              :marketingtexts   => [Marketingtext.new(:client      => @client,
                                                      :legacy_code => "01",
                                                      :text_type   => "03",
                                                      :main_type   => "02",
                                                      :marketing_text => product["Short Desc"].strip.presence,
                                                      :pull_quote     => product["Short Desc"].strip.presence),
                                    Marketingtext.new(:client      => @client,
                                                      :legacy_code => "02",
                                                      :text_type   => "02",
                                                      :main_type   => "01",
                                                      :marketing_text => product["Long Desc"].strip.presence,
                                                      :pull_quote     => product["Long Desc"].strip.presence),
                                    Marketingtext.new(:client      => @client,
                                                      :legacy_code => "04",
                                                      :text_type   => "04",
                                                      :main_type   => "05",
                                                      :marketing_text => product["Contents"].strip.presence,
                                                      :pull_quote     => product["Contents"].strip.presence)].reject{|x| x.marketing_text.nil?},
              :language_of_text => (product["Language"].presence || "eng").split(",")[0].downcase,
              :main_bisac_code  => product["BISAC one"].strip.presence,
              :main_bic_code    => product["BIC one"  ].strip.presence)
    end
  end

  def books
    @products.map do |product|
      work = @client.works.find_by(:title => [product["Title"].strip.presence, product["Vol No"].strip.presence].compact.join(": "))
      Book.new(:client          => @client,
               :work            => work       ,
               :title           => [product["Title"].strip.presence, product["Vol No"].strip.presence].compact.join(": "),
               :subtitle        => product["Subtitle"].strip.presence                                           ,
               :publishing_status => product["Avail"].strip.try(:split, ":").try(:[],0),
               :workcontacts    => work.workcontacts,
               :marketingtexts  => work.marketingtexts,
               :language        => (product["Language"].strip.presence || "eng").split(",")[0].downcase,
               :languages       => [Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],0).try(:downcase),
                                                 :language_role => "07"),
                                    Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],1).try(:downcase),
                                                 :language_role => "06"),
                                    Language.new(:client        => @client,
                                                 :language_code => product["Language"].presence.try(:split,",").try(:[],2).try(:downcase),
                                                 :language_role => "06")
                                    ].reject{|x| x.language_code.nil?},
               :prices          => [Price.new(:currency_code => "USD",
                                              :price_amount  => product["USD price"].strip,
                                              :price_type    => "01",
                                              :onix_price_type_qualifier_id => "05",
                                              :status_code => "00"),
                                    Price.new(:currency_code => "GBP",
                                              :price_amount  => product["GBP Price"].strip,
                                              :price_type    => "03",
                                              :onix_price_type_qualifier_id => "05",
                                              :status_code => "00")].reject{|x| x.price_amount.nil?},
               :isbn            => product["ISBN"].strip.presence || "placeholder",
               :pub_date        => product["On Sale Date"].strip,
               :extent_page     => Extent.new(:extent_value => product["Pages"].strip),
               :product_form    => product["Format"].strip.presence#,
               #:product_form_detail => [product["Format Details"].strip.presence].compact.presence
               )
    end
  end


  def get_uniq(key)
    @products.map{|product| product[key].strip}.uniq
  end

  def get_uniq_pair(key1, key2)
    @products.map{|product| [product[key1].strip, product[key2].strip]}.uniq
  end

  def get_uniq_trip(key1, key2, key3)
    @products.map{|product| [product[key1], product[key2], product[key3]]}.uniq
  end

end


__END__
