Subjectcode.group(:code, :value).having("count(*) > 1").count.each_key do |code, value|
  first = Subjectcode.where(code: code, value: value).order(:id).first
  last  = Subjectcode.where(code: code, value: value).order(:id).last
  first.works = first.works + last.works
  last.works = []
  last.destroy
end
