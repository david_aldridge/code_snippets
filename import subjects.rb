OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = ::Google::Import.new(client:            nil,
                            spreadsheet_key:   '1TGMqYuVZi_k13LkUfAvrh7ocpzIcYlWfygX-Cp3cu8Y',
                            worksheet_name:    'Sheet1',
                            import_class:      nil,
                            rollback_on_error: true)

client = Client.is_bds
codes = client.subjectcodes

all_names = import.send(:list).each_with_object([]) do |row, array|
  array << [
    row["Subject 1"],
    row["Subject 2"],
    row["Subject 3"],
    row["Subject 4"],
    row["Subject 5"],
    row["Subject 6"]
  ].map(&:squish).map(&:presence).compact
end.flatten.uniq

all_names - codes.pluck(:value)

Work.no_touching do
  ActiveRecord::Base.transaction do
    import.send(:list).each do |row|
      work = Work.find(row['System Work ID'])
      names = [
        row["Subject 1"],
        row["Subject 2"],
        row["Subject 3"],
        row["Subject 4"],
        row["Subject 5"],
        row["Subject 6"]
      ].map(&:squish).map(&:presence).compact
      puts names
      work.subjectcodes = names.map{|n| puts "looking for #{n}"; x = codes.find_by!(value: n) }
      "Setting main code"
      work.subjectcode  = work.work_subjectcodes.joins(:subjectcode).find_by!(subjectcodes: {value: names.first})
      work.save if work.changed?
    end
    # raise ActiveRecord::Rollback
  end
end


client = Client.is_liverpool

works_to_update = client.imprints.find(778).works
works_to_update = client.works.where(id: 85385)

subject_code = client.subjectcodes.find(1290)
Work.no_touching do
  ActiveRecord::Base.transaction do
    works_to_update.each do |work|
      work.subjectcodes = [subject_code]
      "Setting main code"
      work.subjectcode  = work.work_subjectcodes.first
      work.save if work.changed?
    end
    # raise ActiveRecord::Rollback
  end
end
