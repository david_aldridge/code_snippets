class Link
  def initialize(name, arg=nil)
    @name = name
    if block_given?
      @children = []
      yield self
    else
      @link = arg
    end
  end

  def add_child child
    @children << child
  end

  attr_accessor :children
  attr_reader :link, :name
end



l = Link.new "name", "link"


Link.new "parent" do |parent_link|
  parent_link.add_child(Link.new "a", "b")
  parent_link.add_child(Link.new("c") do |link2|
    link2.add_child Link.new("a", "b")
    link2.add_child Link.new("a", "b")
    link2.add_child Link.new("a", "b")
  end)
end



