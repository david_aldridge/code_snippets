class PublishersWebLink

  def initialize(work:)
    self.work = work
  end

  def call
    url_builder
  end

  private
    attr_accessor :work

    def url_builder
      case work.client.identifier
        when 'bds'
        then  prefix_and_suffix_url(prefix: "https://shop.bdspublishing.com/checkout/Store/bds/Detail/WorkGroup/3-190-".freeze,
                                    suffix: work.registrants_internal_reference)
      end
    end

    def prefix_and_suffix_url(prefix:, suffix:)
      "#{prefix}#{suffix}" if suffix && prefix
    end

end

PublishersWebLink.new(work: Client.is_bds.works.first).call



Work.joins(:client).includes(:client).map{|work| PublishersWebLink.new(work: work).call}

