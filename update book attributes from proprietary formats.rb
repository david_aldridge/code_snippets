Client.
  is_notre_dame.
  books.
  joins(:proprietary_format_description).
  where.not(proprietary_format_descriptions: {page_trim_height_in: nil, page_trim_width_in: nil}).
  where(books: {page_trim_height_in: nil, page_trim_width_in: nil}).
  count

ActiveRecord::Base.transaction do
  Work.no_touching do
    Client.
    is_notre_dame.
    books.
    joins(:proprietary_format_description).
    references(:proprietary_format_description).
    where.not(proprietary_format_descriptions: {page_trim_height_in: nil}).
    where.not(proprietary_format_descriptions: {page_trim_width_in: nil}).
    where(books: {page_trim_height_in: nil, page_trim_width_in: nil}).
    each do |book|
      book.update(
        page_trim_height_in: book.proprietary_format_description.page_trim_height_in,
        page_trim_width_in:  book.proprietary_format_description.page_trim_width_in
      )
    end
  end
  # raise ActiveRecord::Rollback
end



update books b
set    page_trim_height_in = proprietary_format_descriptions.page_trim_height_in,
       page_trim_width_in = proprietary_format_descriptions.page_trim_width_in
from   proprietary_format_descriptions
where  proprietary_format_descriptions.id = books.proprietary_format_description_id
and books.client_id = (select id from clients where identifier = 'notre-dame')