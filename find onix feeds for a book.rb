book       = Book.find_by(isbn: "9781780323244")
identifier = "nbni"

Last transmission
==================

book.
  digital_assets.
  joins(:recipient).
  where(contacts: {identifier: identifier}).
  last.
  contact_digital_asset_transfers.
  last.
  transfer_loggable.
  logs.
  last

book.
  digital_assets.
  joins(:recipient).
  where(contacts: {identifier: identifier}).
  last.
  contact_digital_asset_transfers.
  first.
  transfer_loggable.
  logs.
  last

Last ONIX fragment
==================

book.
  digital_asset_content_items.
  joins(digital_assets: :recipient).
  where(contacts: {identifier: identifier}).
  last.
  asset_text

All recipients
==================

book.
  digital_assets.
  includes(:recipient).
  map(&:recipient).
  uniq.
  map(&:to_s)

Last digital_asset
==================

book.
  digital_asset_content_items.
  joins(digital_assets: :recipient).
  where(contacts: {identifier: identifier}).
  last.
  digital_assets.
  last

Last Export Batch
==================

book.
  export_batch_items.
  joins(
    export_batch: {
      contact_digital_asset_transfer_template: :contact
      }
    ).
  where(contacts: {identifier: identifier}).
  last



For a set of books
==================

books = Client.is_liverpool.seriesnames.find(12337).books

identifier = 'nielsen'

No transmission
-----------------

books.map do |book|
  book.
    digital_assets.
    includes(:recipient).
    map(&:recipient).
    uniq.
    map(&:to_s)
end



For a contact
===============

Client.is_pharma.
  contacts.is_ebooks_dot_com.
  transfer_includes.
  group(:transfer_includeable_type).
  count

For a series
============

book = Book.find_by(isbn: "9780750313704")

book = Seriesname.find(PHYSICS_WORLD_DISCOVERY_SERIESNAME_ID).
  books.
  joins(digital_assets: :recipient).
  where(contacts: {identifier: "ingram"}).
  flat_map(&:digital_assets).
  sort_by(&:created_at).
  last


book = Seriesname.find(PHYSICS_WORLD_DISCOVERY_SERIESNAME_ID).
  books.
  joins(digital_assets: :recipient).
  where(contacts: {identifier: "ingram"}).
  first



series_books = Seriesname.find(PHYSICS_WORLD_DISCOVERY_SERIESNAME_ID).books






pp book.versions.sort_by(&:id).map{|v| [v.id, v.created_at, v.object_changes]}

id: 1322815,
  item_type: "Book",
  item_id: 78227,
  event: "create",
  whodunnit: "5597",
  object: nil,
  created_at: Wed, 10 Feb 2016 15:37:01 UTC +00:00,
  object_changes:
   "--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nclient_id:\n- \n- 356\ntitle:\n- \n- Translation of Deutsch sein und schwarz dazu\nisbn:\n- \n- placeholder\nproprietary_edition_description_id:\n- \n- 143\nwork_id:\n- \n- 51415\nunpriced_item_type_code:\n- \n- '02'\nuse_edition_suppliers:\n- \n- true\nfull_title:\n- \n- Translation of Deutsch sein und schwarz dazu\ncreated_at:\n- \n- &1 2016-02-10 15:37:01.735849389 Z\nupdated_at:\n- \n- *1\nvalid_onix:\n- \n- false\nid:\n- \n- 78227\n",
  client_id: 356,
  book_id: nil>,
