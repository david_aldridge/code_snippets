class MyClass

  default_scope :hide_ignored

  def self.hide_ignored
    where(ignore: false)
  end

  def self.including_ignored
    unscope(where: :ignore)
  end
end
