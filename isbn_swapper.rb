isbn1 = "9781612495323"
isbn2 = "9781612495316"

Book.joins(:client).where(isbn: isbn1).count
Book.joins(:client).where(isbn: isbn2).count

isbn1 = "9781612495323"
isbn2 = "9781612495316"

book1 = Book.joins(:client).find_by(isbn: isbn1)
book2 = Book.joins(:client).find_by(isbn: isbn2)

book1 = Book.find(94639)
book2 = Book.find(82987)


book2.isbn, book1.isbn = book1.isbn, book2.isbn

"Book 1: From #{book1.isbn_was} to #{book1.isbn}"
"Book 2: From #{book2.isbn_was} to #{book2.isbn}"

book1.save(validate: false)
book2.save(validate: false)


book1.valid?
book2.valid?


client = Client.is_unbound

isbns = {
  "9781783521173" => "9781783521654",
  "9781783521357" => "9781783521715",
  "9781908717917" => "9781783521678"
  }
isbns.size
client.books.where(isbn: isbns.keys).group(:isbn).count
isbns.keys.map{|isbn| Lisbn.new(isbn).valid?}
client.books.where(isbn: isbns.keys).map(&:valid?)
client.books.where(isbn: isbns.values).group(:isbn).count
isbns.values.map{|isbn| Lisbn.new(isbn).valid?}
client.books.where(isbn: isbns.values).map(&:valid?)

ActiveRecord::Base.transaction do
  isbns.each do |isbn_from, isbn_to|
    book = client.books.find_by(isbn: isbn_from)
    book.isbn = isbn_to
    book.save(validate: false)
  end
end

client.books.where(isbn: isbns.keys).map(&:valid?)
client.books.where(isbn: isbns.values).map(&:valid?)
