select
  coalesce(b.client, w.client) client,
  coalesce(b.work_id, w.work_id) work_id,
  coalesce(b.seriesname_id, w.seriesname_id) s_id,
  count(*) over (partition by coalesce(b.work_id, w.work_id)) w_rows,
  count(*) over (partition by coalesce(b.work_id, w.work_id), coalesce(b.seriesname_id, w.seriesname_id)) ws_rows,
  w.year_of_annual,
  w.number_within_series,
  b.year_of_annual,
  b.number_within_series
from (
  select
  c.identifier client,
  wsn.work_id,
  wsn.seriesname_id,
  wsn.year_of_annual,
  wsn.number_within_series
from
  clients c join
  works   w on w.client_id = c.id join
  work_seriesnames wsn on w.id = wsn.work_id
where
  c.status = 1
) w full outer join
(
  select distinct
    c.identifier client,
    b.work_id,
    bsn.seriesname_id,
    bsn.year_of_annual year_of_annual,
    bsn.number_within_series number_within_series
  from
    clients c join
    books   b on b.client_id = c.id join
    book_seriesnames bsn on b.id = bsn.book_id
  where
    c.status = 1
  -- group by
  --   b.work_id,
  --   bsn.seriesname_id
) b on b.work_id = w.work_id and b.seriesname_id = w.seriesname_id
order by 5 desc, 4 desc, 1, 2,3;