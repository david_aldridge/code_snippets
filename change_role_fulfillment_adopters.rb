[{"id"=>2,
  "client_id"=>3,
  "task_role_id"=>2,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>13},
 {"id"=>4,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>16},
 {"id"=>6,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>18},
 {"id"=>8,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>13},
 {"id"=>9,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>17},
 {"id"=>11,
  "client_id"=>3,
  "task_role_id"=>11,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>12},
 {"id"=>12,
  "client_id"=>3,
  "task_role_id"=>7,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>12},
 {"id"=>13,
  "client_id"=>3,
  "task_role_id"=>8,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>12},
 {"id"=>14,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>12},
 {"id"=>16,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>19},
 {"id"=>19,
  "client_id"=>3,
  "task_role_id"=>7,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>27},
 {"id"=>20,
  "client_id"=>3,
  "task_role_id"=>17,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>26},
 {"id"=>21,
  "client_id"=>3,
  "task_role_id"=>18,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>30},
 {"id"=>24,
  "client_id"=>3,
  "task_role_id"=>21,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>21},
 {"id"=>28,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>20},
 {"id"=>31,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>49},
 {"id"=>33,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>66},
 {"id"=>34,
  "client_id"=>3,
  "task_role_id"=>1,
  "role_adoptability_type"=>"User",
  "role_adoptability_id"=>70}].map do |at|
    RoleAdopter.new(at)
  end.each(&:save!)




ActiveRecord::Base.transaction do
  RoleFulfillment.
    where(
      role_adopter: RoleAdopter.where(task_role_id: [1,2])
    ).
    includes(:role_adopter).map do |f|
      current_adopter =  f.role_adopter
      new_adopter =
        RoleAdopter.find_by!(
          role_adoptability_id: current_adopter.role_adoptability_id,
          task_role_id:         25
        )
      f.update!(role_adopter: new_adopter)
    end
end

RoleAdopter.where(task_role_id: [1,2]).map(&:role_fulfillments)
