
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

reload!
import = Google::SimpleImporter.new(client:           Client.is_lund,
                                         spreadsheet_key: '1ZzHoe1Nln3D0MuoQPDjWN0xG3kkla7DzcqZP10Vb28g',
                                         worksheet_name:   'Sheet1',
                                         import_class: MarketRepresentationSheetRowImportService)
import.call

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

reload!
import = Google::Import.new(client:           Client.is_periscope,
                                   spreadsheet_key: '1y8xHGmk4k9dON4jeBKW4bYtyz9ilqNl4Zvu3esDFo4M',
                                   worksheet_name:   'Sheet1',
                                   import_class: RoyaltySheetRowImportService)
import.call

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:           Client.is_liverpool,
                                   spreadsheet_key: '1RySyNzHpfGIPDydMsFVTVUsrCbQMwUMmsvpL76YcxXk',
                                   worksheet_name:   'Sales',
                                   import_class: SaleSheetRowImportService)
import.call

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:           Client.is_riba,
                                   spreadsheet_key: '12-GloRFvgFOd5MnzaQt6owVP0CgNgOaayal_gDHauzg',
                                   worksheet_name:   'Sales',
                                   import_class: SaleSheetRowImportService)
import.call

Client.is_riba.sales.destroy_all
Client.is_riba.book_channels.size
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:         Client.is_riba,
                           spreadsheet_key: '12-GloRFvgFOd5MnzaQt6owVP0CgNgOaayal_gDHauzg',
                           worksheet_name:  'Sales',
                           import_class:    SaleSheetRowImportService,
                            rollback_on_error: false)
import.call
Client.is_riba.sales.size
Client.is_riba.book_channels.size

Client.is_riba.royalty_specifiers.destroy_all
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:            Client.is_riba,
                            spreadsheet_key:   '19i57vCytW5qJxz12AfBZgkq5nieZmajkpyi2qwgAQF4',
                            worksheet_name:    'Rates',
                            import_class:      RoyaltySheetRowImportService,
                            rollback_on_error: false)
import.call
Client.is_riba.royalty_specifiers.size

client = Client.is_pharma
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1qn4WVKqRpy05eRf_icLNwDz2sJdOW7Ns3i7yMCz0GcQ',
                            worksheet_name:    'Sheet 1',
                            import_class:      MarketingTextSheetRowImportService,
                            rollback_on_error: false)
import.call

client = Client.is_sydney
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1Z1opzmF9PDGeDYEUtjH_Y3kwpV6HZ23nnZaLr70R5O4',
                            worksheet_name:    'Sheet 1',
                            import_class:      DuplicateProductSheetRowImportService,
                            rollback_on_error: false)
import.call

client = Client.is_liverpool
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1EqTat0tenfP80zOh7j-kiYhk8o5zSnXDPlONnopz3LA',
                            worksheet_name:    'Sales',
                            import_class:      SaleSheetRowImportService,
                            rollback_on_error: true)
import.call


client = Client.is_hbku
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
reload!
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1lt8R6VdyB8wTTmUlM_RNU4EkOmVkuuxb1dFrUdpMaaE',
                            worksheet_name:    'Sales',
                            import_class:      SaleSheetRowImportService,
                            rollback_on_error: false)
import.call


Vertebrate
==========

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

reload!
import = Google::VertebrateSalesImporter.new
import.step_1
import.step_2
import.step_3

test = import.sales_list[0]
test_obj = Google::Vertebrate::SalesImporter::SalesRow.new(test)
test_obj.valid?
test_obj.errors
test_obj.validate!

import.sales

Snowbooks
=========


client = Client.is_snowbooks
#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
#reload!
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1NXAUyXqBL9j3-16stXldS8niJSPF0wKatBNcoekTfCY',
                            worksheet_name:    'Sales',
                            import_class:      SaleSheetRowImportService,
                            rollback_on_error: true)
import.call




LUP
===


client = Client.is_liverpool
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1EqTat0tenfP80zOh7j-kiYhk8o5zSnXDPlONnopz3LA',
                            worksheet_name:    'Sales',
                            import_class:      SaleSheetRowImportService,
                            rollback_on_error: true)
import.call



Bulk price importer
===================

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_pharma
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1oyH6B_UQ9ZTwkohemwlXL9UecXYombhQiSWM6Ubqtgg',
                            worksheet_name:    '2018',
                            import_class:      PriceSheetRowImportService,
                            rollback_on_error: true)

PriceSheetRowImportService.new(client: nil, row: import.send(:list).first).send(:price_columns)

import.call





1oyH6B_UQ9ZTwkohemwlXL9UecXYombhQiSWM6Ubqtgg




Statement contract importer
===========================

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_liverpool
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '168Y4WWJmXdwIutZX5KN58zrqGv6fHAmsqvnoRuqe7ls',
                            worksheet_name:    'Sheet1',
                            import_class:      StatementContractPaymentRowImportService,
                            rollback_on_error: true)


import.call





1oyH6B_UQ9ZTwkohemwlXL9UecXYombhQiSWM6Ubqtgg


Contact importer
===========================

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_pharma
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1ojhfJIn9qsyVyAZJsGZvhOt6oCDwys6ZlsqaJlNbDOM',
                            worksheet_name:    'Sheet1',
                            import_class:      ContactSheetRowImportService,
                            rollback_on_error: true)

ActiveRecord::Base.transaction do
  import.call
  #raise ActiveRecord::Rollback
end



Vertebrate new contacts importer
===========================

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_vertebrate
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1ZinNa_zJu1HDt4dYUVYjeu1ZcyAoq5vh2TZ_k-CV1fI',
                            worksheet_name:    'New',
                            import_class:      ContactSheetRowImportService,
                            rollback_on_error: true)

ActiveRecord::Base.transaction do
  import.call
  raise ActiveRecord::Rollback
end



Zedbooks existing contacts importer
===========================

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_zed
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '10rwv48g0BMl_hwi61Ak88VfPjoMeXRkqNof_VBo6lXc',
                            worksheet_name:    '2 Current records',
                            import_class:      ContactSheetRowImportService,
                            rollback_on_error: true)

ActiveRecord::Base.transaction do
  import.call
  #raise ActiveRecord::Rollback
end

NEW
===

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_zed
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '10rwv48g0BMl_hwi61Ak88VfPjoMeXRkqNof_VBo6lXc',
                            worksheet_name:    '3 New records',
                            import_class:      ContactSheetRowImportService,
                            rollback_on_error: true)

ActiveRecord::Base.transaction do
  import.call
  #raise ActiveRecord::Rollback
end


NEW 2
=====

reload!
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
client = Client.is_zed
import = Google::Import.new(client:            client,
                            spreadsheet_key:   '1rl4ogQSREa9qNzJTALjDrEffyPrbuk09MIS3NEcWkWY',
                            worksheet_name:    'Focus Contact records',
                            import_class:      ContactSheetRowImportService,
                            rollback_on_error: true)

ActiveRecord::Base.transaction do
  import.call
  #raise ActiveRecord::Rollback
end
