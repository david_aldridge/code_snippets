Create Reference file
=====================


Copy Ref file and edit
===============

From:
<!DOCTYPE ONIXMessage SYSTEM "http://www.editeur.org/onix/2.1/reference/onix-international.dtd">
To:
<!DOCTYPE ONIXMessage SYSTEM "/Users/david/Development/onix_validator/2.1/03/reference/onix-international.dtd">

Create Short from Copy
======================

xsltproc -o \
"/Users/david/Downloads/ingram unicode test/institute_of_physics_201905201_short.xml" \
/Users/david/Documents/Bibliocloud/Onix/switch-onix-tagnames-1.1.xsl.xml \
"/Users/david/Downloads/ingram unicode test/institute_of_physics_201905201.xml"

xsltproc -o crimson_20180424134454_short.xml \
/Users/david/Documents/Bibliocloud/Onix/switch-onix-tagnames-1.1.xsl.xml \
crimson_20180424134454_reference.xml

Create Reference from Short
======================

Change From:
<!DOCTYPE ONIXMessage SYSTEM "http://www.editeur.org/onix/2.1/short/onix-international.dtd">
To:
<!DOCTYPE ONIXMessage SYSTEM "/Users/david/Development/onix_validator/2.1/03/short/onix-international.dtd">


xsltproc -o \
"McGill_Queens_ref.xml" \
/Users/david/Documents/Bibliocloud/Onix/switch-onix-tagnames-1.1.xsl.xml \
"ebook_onix_from_coresource_short.xml"

Change From:
<!DOCTYPE ONIXMessage SYSTEM "{$dtd-url}">
To:
<!DOCTYPE ONIXMessage SYSTEM "http://www.editeur.org/onix/2.1/reference/onix-international.dtd">



Change short
============

From:
<!DOCTYPE ONIXmessage SYSTEM "{$dtd-url}">
To:
<!DOCTYPE ONIXmessage SYSTEM "http://www.editeur.org/onix/2.1/short/onix-international.dtd">








/Users/david/rails/bibliocloud/lib/onix/v2.1/ONIX_BookProduct_Release2.1_reference.xsd
/Users/david/rails/bibliocloud/lib/onix/v2.1/ONIX_BookProduct_Release2.1_short.xsd



xmllint --noout  \
 "/Users/david/rails/bibliocloud/lib/onix/v2.1/ONIX_BookProduct_Release2.1_reference.xsd" \
 "/Users/david/Documents/Bibliocloud/Clients/Quiller/To Bowker/Quiller_20180307145317_reference.xml"


xmllint --noout --schema \
 "/Users/david/rails/bibliocloud/lib/onix/v2.1/ONIX_BookProduct_Release2.1_short.xsd" \
 "/Users/david/Documents/Bibliocloud/Clients/Quiller/To Bowker/Quiller_20180307145317_short.xml"


xmllint --noout  --schema ./ONIX_BookProduct_Release2.1_reference.xsd \
 "/Users/david/Documents/Bibliocloud/Clients/Quiller/To Bowker/Quiller_20180307145317_reference.xml"




xmllint --noout  \
 "/Users/david/Documents/Bibliocloud/Clients/Quiller/To Bowker/Quiller_20180307145317_reference copy.xml"
