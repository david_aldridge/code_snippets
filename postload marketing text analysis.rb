Client.find(401).marketingtexts.group(:main_type).count

Client.find(401).books.order(:pub_date).map do |b|
  [
    b.pub_date,
    b.isbn,
    b.short_description_text.try(:size),
    b.description_text.try(:size),
    b.long_description_text.try(:size),
    b.short_description_text && b.long_description_text ? b.short_description_text == b.long_description_text : nil
  ]
end



