Client.is_alisonjones.
  books.
  joins(:proprietary_format_description).
  each {|b| b.copy_format_values; b.save!}

Client.is_alisonjones.
  books.
  where("product_form_detail = ARRAY['E116']::text[]").
  update_all(product_form_detail: nil)

{["BB", ["B501"], "", nil]=>1,
 ["BC", ["B101"], nil, 166]=>3,
 ["BC", ["B103"], "", 155]=>2,
 ["BC", ["B103"], nil, 155]=>6,
 ["BC", ["B103"], nil, nil]=>8,
 ["BC", ["B105"], "", 156]=>1,
 ["BC", ["B105"], nil, 156]=>4,
 ["BC", ["B105"], nil, nil]=>3,
 ["BC", ["B106"], nil, 154]=>2,
 ["BC", ["B106"], nil, nil]=>1,
 ["BC", nil, nil, nil]=>1,
 ["ED", ["E101"], nil, 157]=>8,
 ["ED", ["E107"], "", 159]=>2,
 ["ED", ["E116"], nil, 158]=>12,
 [nil, ["E116"], nil, 158]=>1,
 [nil, nil, "", 257]=>2,
 [nil, nil, "", nil]=>1,
 [nil, nil, nil, 257]=>8,
 [nil, nil, nil, nil]=>5}

pp Client.is_alisonjones.books.
  group(:product_form, :product_form_detail, :epub_type_code, :proprietary_format_description_id).
  order(:product_form, :product_form_detail, :epub_type_code, :proprietary_format_description_id).
  count

{["BB", ["B501"], nil, nil]=>1,
 ["BC", ["B101"], nil, 1787]=>3,
 ["BC", ["B103"], nil, 1785]=>13,
 ["BC", ["B103"], nil, nil]=>3,
 ["BC", ["B105"], nil, 1785]=>2,
 ["BC", ["B105"], nil, 1803]=>5,
 ["BC", ["B105"], nil, 1804]=>1,
 ["BC", ["B106"], nil, 1782]=>2,
 ["BC", ["B106"], nil, 1785]=>1,
 ["BC", nil, nil, 1782]=>2,
 ["BC", nil, nil, 1791]=>1,
 ["DG", ["E116"], "029", 1780]=>31,
 ["DG", ["E116"], nil, 158]=>26,
 ["DG", nil, "022", 158]=>3,
 ["DG", nil, "029", 1780]=>3,
 ["ED", ["E107"], "", 159]=>2,
 ["ED", ["E116"], nil, 158]=>4,
 [nil, ["E116"], nil, 158]=>1,
 [nil, nil, nil, nil]=>1}

 159 = PDF
 158 = Mobi 022
 1780 ePub 029
