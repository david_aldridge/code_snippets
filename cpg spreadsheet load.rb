module CPG
  class Row
    attr_reader :row, :work

    def initialize(row)
      @row = row
    end

    def call
      return "Could not find #{isbn}" if work.nil?

      work.onix_audiences.destroy_all if audience_codes.any?
      audience_codes.each do |code|
        aud = work.onix_audiences.new(audience_code_value: code)
        byebug unless aud.save
      end
      byebug unless work.update_attributes(work_attributes)
    end

    def work_attributes
      {
        interest_age_from: interest_age_from,
        interest_age_to:   interest_age_to,
        main_bisac_code:   main_bisac_code,
        main_bic_code:     main_bic_code,
        thema_main_code:   thema_main_code,
        keywords:          keywords
      }.merge(bisac_attribs).merge(bic_attribs).merge(thema_attribs)
    end

    def work
      @_work ||= client.
      works.
      joins(:books).
      where(books: { isbn: isbn}
      ).distinct.first
    end

    delegate :client, to: :class

    def self.client
      @_client ||= Client.is_cpg
    end

    def isbn
      productidentifier1_isbn13_idvalue
    end

    def audience_codes
      [descriptivedetail_audience1_onixaudiencecodes_audiencecodevalue,
        descriptivedetail_audience2_onixaudiencecodes_audiencecodevalue].compact
    end

    def interest_age_from
      descriptivedetail_audiencerange1_audiencerangevalue1
    end

    def interest_age_to
      descriptivedetail_audiencerange1_audiencerangevalue2
    end

    def bisac_attribs
      ArrayOfHashesToHashOfArrays.new(
        array_of_hashes: additional_bisac_codes.map{|code| ONIX::BISACCodeToAttribute.new(subject_code: code).call}
      ).call
    end

    def main_bisac_code
      descriptivedetail_subject2_bisacsubjectheading_subjectcode
    end

    def additional_bisac_codes
      [
        descriptivedetail_subject3_bisacsubjectheading_subjectcode,
        descriptivedetail_subject4_bisacsubjectheading_subjectcode,
        descriptivedetail_subject5_bisacsubjectheading_subjectcode,
        descriptivedetail_subject6_bisacsubjectheading_subjectcode
      ].compact
    end

    def raw_bic_codes
      [
        descriptivedetail_subject1_bicsubjectcategory_subjectcode,
        descriptivedetail_subject16_bicsubjectcategory_subjectcode
      ].compact
    end

    def main_bic_code
      raw_bic_codes.detect{|c| !c.starts_with?("5") }
    end

    def bic_codes
      raw_bic_codes - [main_bic_code]
    end

    def bic_attribs
      ArrayOfHashesToHashOfArrays.new(
        array_of_hashes: bic_codes.map{|code| ONIX::BICCodeToAttribute.new(subject_code: code).call}
      ).call
    end

    def thema_main_code
      descriptivedetail_subject17_themasubjectcategory_subjectcode
    end

    def thema_codes
      [descriptivedetail_subject18_themasubjectcategory_subjectcode].compact
    end

    def thema_attribs
      ArrayOfHashesToHashOfArrays.new(
        array_of_hashes: thema_codes.map{|code| ONIX::ThemaCodeToAttribute.new(subject_code: code).call}
      ).call
    end

    def keywords
      [
        descriptivedetail_subject7_keywords_subjectcode,
        descriptivedetail_subject7_keywords_subjectheadingtext,
        descriptivedetail_subject8_keywords_subjectheadingtext,
        descriptivedetail_subject9_keywords_subjectheadingtext,
        descriptivedetail_subject10_keywords_subjectheadingtext,
        descriptivedetail_subject11_keywords_subjectheadingtext,
        descriptivedetail_subject12_keywords_subjectheadingtext,
        descriptivedetail_subject13_keywords_subjectheadingtext,
        descriptivedetail_subject14_keywords_subjectheadingtext,
        descriptivedetail_subject15_keywords_subjectheadingtext
      ].compact.uniq.join(";")
    end

    %w[
      ProductIdentifier1_ISBN13_IDValue
      DescriptiveDetail_Audience1_ONIXaudiencecodes_AudienceCodeValue
      DescriptiveDetail_Audience2_ONIXaudiencecodes_AudienceCodeValue
      DescriptiveDetail_AudienceRange1_AudienceRangeValue1
      DescriptiveDetail_AudienceRange1_AudienceRangeValue2
      Barcode1_Type
      Barcode1_PositionOnProduct
      DescriptiveDetail_Subject1_BICsubjectcategory_SubjectCode
      DescriptiveDetail_Subject16_BICsubjectcategory_SubjectCode
      DescriptiveDetail_Subject2_BISACSubjectHeading_SubjectCode
      DescriptiveDetail_Subject3_BISACSubjectHeading_SubjectCode
      DescriptiveDetail_Subject4_BISACSubjectHeading_SubjectCode
      DescriptiveDetail_Subject5_BISACSubjectHeading_SubjectCode
      DescriptiveDetail_Subject6_BISACSubjectHeading_SubjectCode
      DescriptiveDetail_Subject17_Themasubjectcategory_SubjectCode
      DescriptiveDetail_Subject18_Themasubjectcategory_SubjectCode
      DescriptiveDetail_Subject7_Keywords_SubjectCode
      DescriptiveDetail_Subject7_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject8_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject9_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject10_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject11_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject12_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject13_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject14_Keywords_SubjectHeadingText
      DescriptiveDetail_Subject15_Keywords_SubjectHeadingText
    ].each do |column_name|
      define_method column_name.parameterize.to_sym do
        row.fetch(column_name).presence
      end
    end
  end
end

__END__
reload!
filename = "/Users/david/Documents/Bibliocloud/Clients/CPG/From CPG/cpg.xlsx"

filename = "https://bibliocloudimages.s3-eu-west-1.amazonaws.com/clients/cpg.xlsx"

spreadsheet = Roo::Spreadsheet.open(filename, extension: :xlsx)
rows        = spreadsheet.sheet(0).parse(headers: true, clean: true)[1..1189] ; 0
objs = rows.map{|row| CPG::Row.new(row) } ; 0


ActiveRecord::Base.transaction do
  objs.each(&:call)
  # raise ActiveRecord::Rollback
end
