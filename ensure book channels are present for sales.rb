

Client.is_liverpool.sales.joins(:book, :channel).map{|sales| [sales.book, sales.channel]}.uniq.each do |book, channel|
  next if channel.in? book.channels
  masterchannel       = channel.masterchannel
  book.masterchannels << masterchannel unless masterchannel.in? book.masterchannels
  book_masterchannel  = book.book_masterchannels.find_by(:masterchannel_id => masterchannel.id)
  book_masterchannel.channels << channel
end ; 0
