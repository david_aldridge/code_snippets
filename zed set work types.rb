work_ids = [
  55375,
46840,
50651,
47020,
47203,
46594,
72595,
48069,
87636,
52485,
47194,
53337,
47075,
48045,
46992,
50659,
46840,
48050,
47113,
61383,
46488,
47191,
74227,
46695,
53090,
61346,
75924,
49481,
72697,
72656,
73005,
78405,
72159,
72532,
47137,
47047,
83163,
46986,
76072,
53160,
53165,
47034,
47522,
72409,
72878,
72696,
56528,
75925,
88634,
47058,
46808,
46543,
75907,
49464,
82803,
47064,
82389,
47168,
76062,
48071,
49076,
47162,
46425,
55374
]

peer_reviewed_work_type_id     = Client.is_zed.work_types.find_by(name: "Peer reviewed").id
non_peer_reviewed_work_type_id = Client.is_zed.work_types.find_by(name: "Not peer reviewed").id
compilation_work_type_id       = Client.is_zed.work_types.find_by(name: "Compilation").id

works = Client.is_zed.works

works.update_all(work_type_id: nil)

non_peer_reviewed_works = works.where(id: work_ids)
non_peer_reviewed_works.count
non_peer_reviewed_works.update_all(work_type_id: non_peer_reviewed_work_type_id)

compilation_works = Work.where(id: Client.is_zed.work_derivations.compilations.pluck(:derived_work_id).uniq )
compilation_works.update_all(work_type_id: compilation_work_type_id)


peer_reviewed_works = works.where(work_type_id: nil)
peer_reviewed_works.update_all(work_type_id: peer_reviewed_work_type_id)

Shop.find(15).books = Shop.find(15).books - non_peer_reviewed_works.includes(:books).map(&:books)


Client.is_zed.works.joins(:work_type).group("work_types.name").count