class DeweyDecimalClassificationValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    value.errors.each { |error| record.errors.add(attribute, error) }
  end
end
