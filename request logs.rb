select   request_logs.created_at::Date,
         max(ip_address) max_ip_addresses,
         users.id,
         users.email,
         count(*) requests,
         count(distinct ip_address) ip_addresses
from     request_logs
join     users on users.id = request_logs.user_id
where    user_id is not null and
         users.email not like '%bibliocloud.com'
group by request_logs.created_at::Date,
         users.id
order by 1,2,3;



select * from request_logs
where user_id = 5154;
