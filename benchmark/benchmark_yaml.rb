filepath1 = YAML.load_file(Rails.root.join("config", "thema.yml")).transform_keys(&:freeze)

data = YAML.load_file(Rails.root.join("config", "thema.yml")).transform_keys(&:freeze)

class Thema
  def self.new(code:,
        code_description: nil,
        code_parent: nil,
        issue_number: nil,
        code_notes: nil,
        modified: nil)
    instance_cache[:code] ||= super(
      code:             code,
      code_description: code_description,
      code_parent:      code_parent,
      issue_number:     issue_number,
      code_notes:       code_notes,
      modified:         modified
    )
  end

  def self.instance_cache
    @instance_cache ||= {}
  end

  def self.instance_cache=(cache)
    @instance_cache = cache
  end

  def initialize(code:,
        code_description: nil,
        code_parent: nil,
        issue_number: nil,
        code_notes: nil,
        modified: nil)
    @code        = code
    @description = code_description
    @parent      = code_parent
    @issue       = issue_number
    @notes       = code_notes
    @modified    = modified
    freeze
  end
end


class Thema
  def self.new(code)
    instance_cache[code]
  end

  def self.instance_cache
    @instance_cache ||= {}
  end

  def self.instance_cache=(cache)
    @instance_cache = cache
  end

  def initialize(code:,
        code_description: nil,
        code_parent: nil,
        issue_number: nil,
        code_notes: nil,
        modified: nil)
    @code        = code
    @description = code_description
    @parent      = code_parent
    @issue       = issue_number
    @notes       = code_notes
    @modified    = modified
    freeze
  end

  attr_reader :code,
              :code_description,
              :code_parent,
              :issue_number,
              :code_notes,
              :modified
end

objects = data.map{|x| x.last.reverse_merge(code: x.first) }.map{|x| Thema.new(x)}

File.open(Rails.root.join("config", "thema3.yml"), 'w') do |f|
  f.write YAML.dump(objects)
end

Thema.instance_cache = YAML.load_file(Rails.root.join("config", "thema3.yml")).each_with_object({}) {|t,h| h[t.code] = t}
Thema.new("A")
