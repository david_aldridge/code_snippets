amazon = Client.is_canelo.contacts.is_amazon
canelo = Client.is_canelo.contacts.find_by(corporate_name: "Canelo")
dc = DiscountCode.find_by(discount_code_type_name: 'KEE-GBP')
price = Book.find(94870).prices.gbp.first

Client.is_canelo.discount_codes.map do |dc|
  dc.applies_to?(canelo, amazon, price)
end
