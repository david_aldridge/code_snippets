class SetSupportingresourceNotnullableColumns < ActiveRecord::Migration[5.2]
  def change
    Supportingresource.
      where(resource_variant: nil).
      where(resourceable_type: "Work").each(&:unknown!)

    Supportingresource.
      where(resource_variant: nil).
      where(resourceable_type: "Contact").each(&:contributor_picture!)

    Supportingresource.
      where(approval: nil).each(&:approved!)

    # change_column :supportingresources, :resource_variant,  :integer, nullable: false
    # change_column :supportingresources, :approval,          :integer, nullable: false
    # change_column :supportingresources, :client_id,         :integer, nullable: false
    # change_column :supportingresources, :resourceable_type, :string,  nullable: false
    # change_column :supportingresources, :resourceable_id,   :integer, nullable: false
  end
end
