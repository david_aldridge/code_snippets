client = Client.is_mcgill_queens
client.contacts.group_by{|c| c.person_name || c.corporate_name}.select{|k,v| v.many?}.transform_values{|v| v.map(&:id).sort}.values.each do |contact_ids|
  keep_id = contact_ids.first
  drop_ids = contact_ids[1..-1]
  client.workcontacts.where(contact_id: drop_ids).update_all(contact_id: keep_id)
  client.contacts.where(id: drop_ids).destroy_all
end


client.contacts.update_all("workcontacts_count = (select count(*) from workcontacts wc where wc.contact_id = contacts.id)")


ids = [[120990, 123952], [121004, 121005], [121072, 122739], [121231, 121236], [121273, 121278, 123062, 123961], [121308, 122885, 122887, 123157], [121354, 121364], [121365, 124042], [121393, 121394, 121395, 121396], [121400, 122709], [121403, 122553], [121531, 121532], [121572, 121574, 122573, 122606, 123962], [121657, 121658], [121673, 121987], [121725, 121768], [121870, 121872, 121873, 121874, 121875, 121877, 121878, 121880, 121881, 121883, 121884, 121886, 121887, 121888], [121890, 121892, 121895, 121897, 121898, 121899, 121900, 121901], [121909, 121910], [121935, 121937], [121997, 122384], [122035, 122271], [122154, 122156, 122159, 122161], [122184, 122435], [122297, 122300], [122376, 123988], [122380, 122820], [122495, 123907], [122564, 122569], [122718, 122719], [122764, 122765], [122797, 122798], [122835, 122836, 122837], [122929, 122933], [123110, 123112], [123111, 123113], [123181, 123184], [123995, 123996], [124012, 124013], [124016, 124017, 124018]]

client.contacts.where(id: ids.flat_map{|x| x[1..-1]}).pluck(:workcontacts_count)
client.contacts.where(id: ids.flat_map{|x| x[1..-1]}).destroy_all


client.contacts.where(names_before_key: nil).pluck(:id, :person_name)

client.contacts.where(id: [121890, 121870]).each do |c|
  c.corporate_name = c.keynames
  c.keynames = nil
  c.save
end
