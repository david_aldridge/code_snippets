Client.where.not(slack_api_key: nil).map do |client|
  begin
    client.sign_in_to_slack
    slack = Slack::Web::Client.new
    slack.auth_test
    channels = slack.channels_list["channels"]
    if channels.map { |channel| channel["name"] }.include?(APPLICATION_NAME.downcase)
      puts "OK for #{client.identifier}"
    else
      puts "#{client.identifier}: #{channels.map { |channel| channel["name"] }}"
    end
  rescue Slack::Web::Api::Error => e
    puts "#{client.identifier}: #{e}"
  end
end
