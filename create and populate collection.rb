client = Client.is_leuven

collection = client.collections.create(
  name: "Title in uppercase",
  private: false
)

collection.books = client.books.where("title = upper(title)")
