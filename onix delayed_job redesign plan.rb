


def generate_advisory_lock_id(obj)
  hash = Zlib.crc32("#{obj.class.name}-#{obj.id}")
  2053462845 * hash
end


obj = Work.first ; 0

ActiveRecord::Base.connection.get_advisory_lock(generate_advisory_lock_id(obj))






create batch
add items to batch
store number of items

run batch
i) set processed to zero
ii) set auto_create_file flag
iii) set auto_send_file flag
iv) add delayed jobs, one per item

item batch
i) Gather content
ii) get advisory lock
iii) update number processed
iv) release advisory lock

batch
after update of number processed
if number processed == number of items && auto_create_file
  create file
