Client.is_leuven.prices.includes(:book).gbp.map(&:valid?).each_with_object(Hash.new(0)){|x,h| h[x] += 1}
Client.is_leuven.prices.includes(:book).gbp.map(&:price_type).each_with_object(Hash.new(0)){|x,h| h[x] += 1}

Client.is_leuven.prices.gbp.update_all(
  taxable_amount:   nil,
  tax_rate_code:    nil,
  tax_rate_percent: nil,
  tax_value:        nil
  )
