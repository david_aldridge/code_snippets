Vertebrate
==========

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

reload!
import = Google::VertebrateSalesImporter.new
import.sales_objects[0]
import.sales_array

import.write_system_messages!
import.setup_book_channels! ; 0
import.save_sales!

test = import.sales_list[0]
test_obj = Google::Vertebrate::SalesImporter::SalesRow.new(test)
test_obj.valid?
test_obj.errors
test_obj.validate!

import.sales


