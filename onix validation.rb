@doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/Kogan Page/KoganPage_uk_books_onix_15062.onx"), nil, 'ISO-8859-1') do |config|
  config.default_xml.noblanks
end ; "Done"

@doc.xpath("/xmlns:ONIXMessage/xmlns:Product[./xmlns:DescriptiveDetail/xmlns:ProductFormDetail='E101']").each_with_index do |e101, i|
  e101.xpath("xmlns:CollateralDetail/xmlns:TextContent[not(xmlns:Text)]").remove
end; "Done"




schema_path = "/Users/david/Documents/Bibliocloud/Onix/ONIX_BookProduct_RNG_schema+codes_Issue_28/ONIX_BookProduct_3.0_reference.rng"    # Or any valid path to a .RNG File
doc_path    = "/Users/david/rails/onix_ninja_files/KoganPage300720191602.xml" # Or any valid path to a .XML File

schema = Nokogiri::XML::RelaxNG(File.open(schema_path))

instance = Nokogiri::XML(File.open(doc_path))
errors = schema.validate(instance)

is_valid = errors.empty?
