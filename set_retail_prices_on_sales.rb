Client.is_vertebrate.sales.where(retail_price: nil).joins(:book).each do |sale|
  new_price = sale.book.default_price_amount
  next unless new_price
  sale.update_columns(:retail_price => new_price)
end
