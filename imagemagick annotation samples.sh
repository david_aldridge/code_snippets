convert -size 320x100 xc:lightblue  -font Candice -pointsize 72 \
          -fill blue  -draw "text 25,65 'Anthony'" text_draw.gif


convert -draw "text 25,65 'Another great chapter'" sample.jpg



convert sample.jpg  +polaroid label:'Another great chapter' +swap -pointsize 70 -gravity Center -append anno_label.jpg




width=`identify -format %w sample.jpg`; \
  convert -background '#0008' -fill white -gravity center -size ${width}x30 \
          caption:"Another great chapter" \
          sample.jpg +swap -gravity north -composite  anno_label.jpg
