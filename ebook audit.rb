require "terminal-table"

puts Terminal::Table.new(
  headings: %w[ISBN Date EpubType Count, ContentTypes],
  rows: Client.is_bds.books.ebook.order(:pub_date).includes(:production_files).map do |book|
          [
            book.isbn,
            book.pub_date&.strftime(STRFTIME_YYYYMMDD),
            book.epub_type_code.description,
            book.production_files.size,
            book.production_files.map(&:attachment_content_type).uniq.sort
          ]
        end
 )


require "terminal-table"

puts Terminal::Table.new(
  headings: %w[ISBN Date EpubType Count, ContentTypes],
  rows: Book.ebook.order(:pub_date).includes(:production_files).map do |book|

          [
            book.isbn,
            book.pub_date&.strftime(STRFTIME_YYYYMMDD),
            book.epub_type_code.description,
            book.production_files.size,
            book.production_files.map(&:attachment_content_type).uniq.sort
          ]
        end
 )
