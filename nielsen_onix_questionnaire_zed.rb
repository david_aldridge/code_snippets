client = Client.by_key("zedbooks")
ISBNs


client.books.
       select{|b| b.isbn_valid?}.
       map{|b| Lisbn.new(b.isbn).parts.try(:[],0,3).try(:join,Constant::HYPHEN)}.
       each_with_object(Hash.new(0)){|i,h| h[i] += 1}.sort.
       map{|k,v| puts "#{k.ljust(13)} #{v}"}

client.works.count
client.books.count

client.prices.
       group(:currency_code).
       order(:currency_code).
       count

client.books.
       group(:publishing_status).
       order(:publishing_status).
       count.
       map{|p, c| puts "#{p}: #{PUBLISHING_STATUSES[p]} #{c}"}

       Rails.logger.level = 1

ExportBatch.find(39).items.destroy_all
pub_statuses = client.books.select(:publishing_status).uniq.pluck(:publishing_status)
books = []
pub_statuses.each do |publishing_status|
  books += client.books.where("pub_date between date '1990-01-01'  and ?", Date.today + 9.months).
                        order(:pub_date => :desc).
                        select{|b| b.inferred_publishing_status == publishing_status && b.isbn_valid}[0..50]
end

isbn_prefixes = [
'978086232%',
'9780901787%',
'9780905762%',
'978178032%',
'978178360%',
'978184277%',
'978184813%',
'978185649%']

isbn_prefixes.each do |isbn_prefix|
  books += client.books.where("pub_date between date '1990-01-01'  and ?", Date.today + 9.months).order(:pub_date => :desc).where("isbn like ?", isbn_prefix).limit(50).select{|b| b.isbn_valid?}
end


books.each_with_object(Hash.new(0)){|b, h| h[b.publishing_status] += 1}
books.uniq! ; 0
ExportBatch.find(39).books += books; 0
Rails.logger.level = 1
ExportBatch.find(39).get_content_items ; 0



client.prices.group(:book_id, :currency_code, :price_type, :onix_price_type_qualifier_id).having("count(*) > 1").count


client.prices.where(:currency_code => 'GBP', :price_type => '01').joins(:book).where(:books => {:product_form => ['BB','BC']})

Book.where(:id =>
client.prices.where(:currency_code => 'GBP', :price_type => '01').pluck(:book_id) -
client.prices.where(:currency_code => 'GBP', :price_type => '02').pluck(:book_id) ).pluck(:id, :pub_date, :product_form)





       client.books.
              group(:publishing_status).
              order(:publishing_status).
              count.
              map{|p, c| puts "#{p}: #{PUBLISHING_STATUSES[p]} #{c}"}


client.books.
      group(:product_form).
      order(:product_form).
      count

client.books.
      group(:epub_type_code).
      order(:epub_type_code).
      count
