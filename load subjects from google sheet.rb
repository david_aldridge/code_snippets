
Client.first.seriesnames.create(
  title_without_prefix: 'IOP Expanding Physics',
  issn:                 '2053-2563',
  imprint:              Imprint.first,
  code:                 'EP',
  short_description:    'IOP Expanding Physics publishes high-quality texts from leading voices across the research landscape on key areas in physics and related subject areas')

Physics World Discovery
Client.first.seriesnames.first.works = Client.first.works

Subjectcode.destroy_all

Client.first.subjectcodes.create(code: '10' , value: 'General & Introductory Physics'          )
Client.first.subjectcodes.create(code: '20' , value: 'Classical Physics'                       )
Client.first.subjectcodes.create(code: '30' , value: 'Thermodynamics'                          )
Client.first.subjectcodes.create(code: '40' , value: 'Dynamics & Fluid Dynamics'               )
Client.first.subjectcodes.create(code: '50' , value: 'Mechanics & Statics'                     )
Client.first.subjectcodes.create(code: '60' , value: 'Electricity, Magnetism & Electrodynamics')
Client.first.subjectcodes.create(code: '70' , value: 'Applied Physics'                         )
Client.first.subjectcodes.create(code: '80' , value: 'Engineering & Industrial Physics'        )
Client.first.subjectcodes.create(code: '90' , value: 'Astronomy & Astrophysics'                )
Client.first.subjectcodes.create(code: '100', value: 'Gravitational Physics & Cosmology'       )
Client.first.subjectcodes.create(code: '110', value: 'Geophysics & Planetary Science'          )
Client.first.subjectcodes.create(code: '120', value: 'Environmental Physics & Clean Technology')
Client.first.subjectcodes.create(code: '130', value: 'Atomic & Molecular Physics'              )
Client.first.subjectcodes.create(code: '140', value: 'Chemical Physics'                        )
Client.first.subjectcodes.create(code: '150', value: 'Condensed Matter Physics'                )
Client.first.subjectcodes.create(code: '160', value: 'Materials Science'                       )
Client.first.subjectcodes.create(code: '170', value: 'Electronic Materials & Devices'          )
Client.first.subjectcodes.create(code: '180', value: 'Mathematical & Computational Physics'    )
Client.first.subjectcodes.create(code: '190', value: 'Lasers, Optics & Photonics'              )
Client.first.subjectcodes.create(code: '200', value: 'Imaging & Image Processing'              )
Client.first.subjectcodes.create(code: '210', value: 'Biophysics'                              )
Client.first.subjectcodes.create(code: '220', value: 'Medical Physics & Biomedical Engineering')
Client.first.subjectcodes.create(code: '230', value: 'Electrical & Electronic Engineering'     )
Client.first.subjectcodes.create(code: '240', value: 'Metrology, Instrumentation & Sensors'    )
Client.first.subjectcodes.create(code: '250', value: 'Nanoscience & Nanotechnology'            )
Client.first.subjectcodes.create(code: '260', value: 'Nuclear Physics'                         )
Client.first.subjectcodes.create(code: '270', value: 'Particle & High Energy Physics'          )
Client.first.subjectcodes.create(code: '280', value: 'Plasma Physics'                          )
Client.first.subjectcodes.create(code: '290', value: 'Quantum Physics'                         )
Client.first.subjectcodes.create(code: '300', value: 'Quantum Information & Computing'         )
Client.first.subjectcodes.create(code: '310', value: 'Nonlinear and Statistical Physics'       )
Client.first.subjectcodes.create(code: '900', value: 'Other'                                   )


Subject codes
=============

OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
import = Google::Import.new(client:            nil,
                            spreadsheet_key:   '1v3BDXK1OgPTFFiKwOi0oBainNd17YTna5_PZZkSONYE',
                            worksheet_name:    'Sheet1',
                            import_class:      nil,
                            rollback_on_error: true)


Based on Work Ref
=================

client = Client.is_iop

ActiveRecord::Base.transaction do
  import.send(:list).each do |row|
    work = Work.find_by(:registrants_internal_reference => row["Book Ref"])
    unless work
      puts "No work for #{row['Book Ref']}"
      next
    end
    work.subjectcodes = work.client.subjectcodes.where(:code => [ row["Subject 1"],row["Subject 2"],row["Subject 3"]].map(&:squish).map(&:presence).compact )
    work.subjectcode  = work.work_subjectcodes.joins(:subjectcode).find_by(:subjectcodes => {:code => row["Subject 1"]})
    work.save if work.changed?
  end
  # raise ActiveRecord::Rollback
end



client = Client.is_leuven

ActiveRecord::Base.transaction do
  import.send(:list).each do |row|
    work = client.books.find_by(isbn: row["ISBN"])&.work
    unless work
      puts "No work for #{row['ISBN']}"
      next
    end
    work.subjectcodes = work.client.subjectcodes.where(:code => [ row["Subject 1"],row["Subject 2"],row["Subject 3"],row["Subject 4"] ].map(&:squish).map(&:presence).compact )
    work.subjectcode  = work.work_subjectcodes.joins(:subjectcode).find_by(:subjectcodes => {:code => row["Subject 1"]})
    work.save if work.changed?
  end
  # raise ActiveRecord::Rollback
end


Based on ISBN
=============


client = Client.is_leuven

ActiveRecord::Base.transaction do
  import.send(:list).each do |row|
    work = client.books.find_by(isbn: row["ISBN"].delete("-"))&.work
    unless work
      puts "No work for #{row['ISBN']}"
      next
    end
    work.subjectcodes = work.client.subjectcodes.where(:code => [ row["Subject1"],row["Subject2"],row["Subject3"],row["Subject4"],row["Subject5"] ].map(&:squish).map(&:presence).compact )
    work.subjectcode  = work.work_subjectcodes.joins(:subjectcode).find_by(:subjectcodes => {:code => row["Subject1"]})
    work.save if work.changed?
  end
  # raise ActiveRecord::Rollback
end


Leuven codes
============

client.subjectcodes.create(code:"ANTH",  value: "Anthropology")
client.subjectcodes.create(code:"ARCHA",  value: "Archaeology")
client.subjectcodes.create(code:"ARCHI",  value: "Architecture")
client.subjectcodes.create(code:"ART",  value: "Art")
client.subjectcodes.create(code:"BIOM",  value: "Biomedical Science")
client.subjectcodes.create(code:"ECON",  value: "Economy")
client.subjectcodes.create(code:"EDU",  value: "Education")
client.subjectcodes.create(code:"GENSC",  value: "General Science")
client.subjectcodes.create(code:"HIST",  value: "History")
client.subjectcodes.create(code:"LAW",  value: "Law")
client.subjectcodes.create(code:"LING",  value: "Linguistics")
client.subjectcodes.create(code:"LIT",  value: "Literature")
client.subjectcodes.create(code:"PHIL",  value: "Philosophy")
client.subjectcodes.create(code:"PHOT",  value: "Photography")
client.subjectcodes.create(code:"POL",  value: "Political Science")
client.subjectcodes.create(code:"PSYCH",  value: "Psychology")
client.subjectcodes.create(code:"REL",  value: "Religion")
client.subjectcodes.create(code:"SET",  value: "Science, Engineering & Technology")
client.subjectcodes.create(code:"SOC",  value: "Social Science")
client.subjectcodes.create(code:"GEDI",  value: "Gender & Diversity")
client.subjectcodes.create(code:"CLAS",  value: "Classical Literature")
client.subjectcodes.create(code:"PSYCHA",  value: "Psychoanalysis")
client.subjectcodes.create(code:"POSTC",  value: "Post-Colonial Studies")
client.subjectcodes.create(code:"ARTP",  value: "Artistic Practice")
client.subjectcodes.create(code:"MUS",  value: "Music")
client.subjectcodes.create(code:"MEVIC",  value: "Media & Visual Culture")
client.subjectcodes.create(code:"NEOL",  value: "Neo-Latin Studies")
