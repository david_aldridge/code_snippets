
doc = Nokogiri::XML(open("/Users/david/Downloads/nbni1510070903_1.xml")) ; 0
doc.remove_namespaces! ; 0
clients   = {}
suppliers = {}
as_of_date_text = doc.at_xpath("ONIXMessage/Header/SentDate").text
raise "Could not find as-of date text" if as_of_date_text.nil?
as_of_date = Date.parse(as_of_date_text) - 1.days
doc.xpath("//SupplyDetail").each do |supply_detail|
  on_hand_qty = supply_detail.at_xpath("Stock/OnHand").try(:text).try(:to_i)
  next if on_hand_qty.nil?
  supplier_name  = supply_detail.at_xpath("SupplierName").try(:text)
  raise "Could not find supplier name" unless supplier_name
  publisher_name = supply_detail.at_xpath("../Publisher[PublishingRole='01']/PublisherName").try(:text)
  raise "Could not find publisher_name" if publisher_name.nil?
  client   = (clients[publisher_name] ||= Client.by_key(publisher_name.downcase.gsub(" ","")))
  raise "Could not find client for publisher #{publisher_name}" unless client
  supplier = (suppliers[[supplier_name,publisher_name]] ||= client.contacts.find_by(:corporate_name => @fromcompany_name))
  raise "Could not find supplier for publisher #{publisher_name} and client id #{client.id}" unless supplier
  product_availability = supply_detail.at_xpath("ProductAvailability").try(:text)
  isbn    = supply_detail.at_xpath("../ProductIdentifier[ProductIDType='15']/IDValue").try(:text)
  raise "Could not find ISBN" if isbn.nil?
  book    = client.books.find_by(:isbn => isbn)
  next if book.nil?
  raise "Could not find book for ISBN #{isbn}" if book.nil?
  last_on_hand_qty = book.stock_availabilities.
                          where("as_of_date < ?", as_of_date).
                          where(:stock_reportable_id => supplier.id, :stock_reportable_type => supplier.class.name).
                          order(:as_of_date => :desc).
                          limit(1)

  sa = StockAvailability.where(:stock_reportable_id   => supplier.id        ,
                                :stock_reportable_type => supplier.class.name,
                                :book_id               => book.id            ,
                                :as_of_date            => as_of_date         ).
                          first_or_initialize(:client_id             => client.id,
                                              :identifier            => isbn               ,
                                              :in_stock_qty          => on_hand_qty        ,
                                              :product_availability  =>  product_availability)
  unless sa.new_record?
    sa.assign_attributes(:client_id             => client.id,
                          :identifier            => isbn               ,
                          :in_stock_qty          => on_hand_qty        ,
                          :product_availability  =>  product_availability)
  end
  sa.save! if sa.changed?
end




doc = Nokogiri::XML(open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/zedbooks_nbni_test_file.xml")) ; 0
doc.remove_namespaces! ; 0

file_isbns = doc.xpath("//ProductIdentifier[ProductIDType='15']/IDValue").map{|i| i.text}
system_isbns = Client.by_key("zedbooks").books.pluck(:isbn)

(file_isbns - system_isbns).count

doc.xpath("//Product").count

products = doc.xpath("//Product").reject{|p| system_isbns.include?(p.at_xpath("ProductIdentifier[ProductIDType='15']/IDValue").text)}.map{|p| [p.at_xpath("ProductIdentifier[ProductIDType='15']/IDValue").text, p.at_xpath("Title/TitleText").text, p.at_xpath("PublicationDate").text, p.at_xpath("SupplyDetail/ProductAvailability").text, p.at_xpath("Stock/Onhand").try(:text)]}




file_pod_isbns = doc.xpath("//Product[SupplyDetail/ProductAvailability='23']/ProductIdentifier[ProductIDType='15']/IDValue").map{|i| i.text}

nbn = Client.by_key("zedbooks").contacts.find_by(:corporate_acronym => "NBN")
system_nbn_pod_isbns = nbn.print_on_demand_specs_as_supplier.map{|pods| pods.book.isbn}
system_all_pod_isbns = Client.by_key("zedbooks").print_on_demand_specs.map{|pods| pods.book.isbn}.uniq


diff = file_pod_isbns.diff(system_all_pod_isbns)
diff[:removed].count
diff[:added  ].count
diff[:common ].count

system_all_pod_isbns.include? "9781842772003"
file_pod_isbns.include? "9781842772003"
