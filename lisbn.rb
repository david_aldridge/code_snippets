require "nori"
require "multi_json"
require_relative "lib/lisbn/ranges"

Lisbn::Ranges.save_to_yaml!

Lisbn::Ranges.to_yaml


require 'nori'
require 'yaml'
require_relative "lib/lisbn/cache_method"
require_relative "lib/lisbn/lisbn"
require_relative "lib/lisbn/ranges"
require_relative "lib/lisbn/lisbn/isbn13_parts"
require_relative "lib/lisbn/lisbn/isbn10_parts"

ranges = Lisbn::Ranges.ranges
group = ranges.find{|range| range["Prefix"] == "978-613"}

group = ranges.find{|range| range["Prefix"] == "978-614"}


Lisbn::Ranges.to_hash
Lisbn::Ranges.save_to_yaml!
Lisbn::ISBN13Parts::RANGES

isbn13="9788894148312"
l = Lisbn::ISBN13Parts.new(isbn13)
RANGES = Lisbn::ISBN13Parts::RANGES
possible_prefixes        = l.send(:possible_prefixes)
matching_prefix          = l.send(:matching_prefix)
agency                   = l.send(:agency)
prefix_registrant_ranges = l.send(:prefix_registrant_ranges)
isbn_range                = l.send(:isbn_range)
isbn_range_number          = l.send(:isbn_range_number)
registrant_range_definition = l.send(:registrant_range_definition)
registrant_element_length = l.send(:registrant_element_length)
registrant_element        = l.send(:registrant_element)


l.ean_ucc_prefix
l.registration_group_element
l.registrant_element
l.publication_element
l.check_digit





Lisbn::ISBN13Parts::RANGES
