class GoogleSheetShopifyHistory

  def initialize(spreadsheet_key, shop)
    @connection       = GoogleDriveConn.new
    @googlesheet      = @connection.session.spreadsheet_by_key(spreadsheet_key)
    @customer_sheet   = @googlesheet.worksheet_by_title('customer')
    @order_sheet      = @googlesheet.worksheet_by_title('order')
    @order_item_sheet = @googlesheet.worksheet_by_title('order_item')
    @address_sheet    = @googlesheet.worksheet_by_title('address')
    @customers        = @customer_sheet.list
    @orders           = @order_sheet.list
    @order_items      = @order_item_sheet.list
    @addresses        = @address_sheet.list
    @shop             = shop
    @client           = @shop.client
  end

  def read
    @data.each do |row|

      isbn = row['isbn'].strip
      if book = @client.books.find_by(:isbn => isbn)
        row['bibliocloud_id'] = book.id
      else
        row['bibliocloud_notes'] = 'Cannot find ISBN'
        next
      end
      if row['price_gbp_new'].presence
        gbp_price = book.prices.find_by(:onix_price_type_qualifier_id => '05', :currency_code => 'GBP', :price_amount => row['price_gbp_old'].to_d.round(2))
        if gbp_price
          gbp_price.update_attributes(:price_amount     => row['price_gbp_new'].to_d.round(2),
                                      :price_type       => '02',
                                      :tax_rate_code    => 'Z',
                                      :taxable_amount   => row['price_gbp_new'].to_d.round(2),
                                      :tax_rate_percent => 0.to_d,
                                      :tax_value        => 0.to_d)
        else
          row['bibliocloud_notes'] = "No GBP price found"
          gbp_price = book.prices.find_by(:onix_price_type_qualifier_id => '05', :currency_code => 'GBP')
          row['bibliocloud_notes'] = "Old GBP price is #{gbp_price.price_amount.to_s}" if gbp_price
        end
      end


      if row['price_usd_new'].presence
        usd_price = book.prices.find_by(:onix_price_type_qualifier_id => '05', :currency_code => 'USD', :price_amount => row['price_usd_old'].to_d.round(2))
        if usd_price
          usd_price.update_attributes(:price_amount     => row['price_usd_new'].to_d.round(2),
                                      :price_type       => '01',
                                      :tax_rate_code    => nil,
                                      :taxable_amount   => row['price_usd_new'].to_d.round(2),
                                      :tax_rate_percent => nil,
                                      :tax_value        => nil)
        else
          row['bibliocloud_notes'] = "No USD price found"
          usd_price = book.prices.find_by(:onix_price_type_qualifier_id => '05', :currency_code => 'USD')
          row['bibliocloud_notes'] = "Old USD price is #{usd_price.price_amount.to_s}" if usd_price
        end
      end
    end
    @worksheet.save
  end

  def write_to_collection(collection)
    @data.each do |row|
      if book = @client.books.find_by(:isbn => row['isbn'])
        collection.books << book unless collection.books.include?(book)
      end
    end
  end

end


__END__

reload!
PaperTrail.enabled = false

Rails.logger.level=1
client          = Client.by_key('zedbooks')
spreadsheet_key = '1EDqww-fmRD_SiACBTV4UfPpxO-Bq3ioOMCi8-aE1Mnc'
#OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
sheet = GoogleSheetPriceChanger.new(spreadsheet_key, client)
sheet.read



client.prices.where(:onix_price_type_qualifier_id => '05', :currency_code => 'USD').group(:price_type).count
client.prices.where(:onix_price_type_qualifier_id => '05', :currency_code => 'USD').group(:tax_rate_code).count
client.prices.where(:onix_price_type_qualifier_id => '05', :currency_code => 'USD').group(:tax_rate_percent).count


collection = client.collections.find_by(:name => "Vidisha's price increases")
sheet.write_to_collection(collection)
