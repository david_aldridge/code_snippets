client = Client.is_crimson

product_forms = client.books.pluck(:product_form).compact.uniq - ["00"]
publishing_statuses = client.books.pluck(:publishing_status).compact.uniq


eb = client.export_batches.create(
  contact_digital_asset_transfer_template_id: client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first.id,
  name: "Nielsen test",
  incremental: false
  )

eb.books +=
    client.imprints.flat_map do |imprint|
      imprint.books.onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months)).
        order(pub_date: :desc).
        limit(3)
    end

eb.books += product_forms.flat_map do |product_form|
      client.books.
      where.not(id: eb.book_ids).
      where(product_form: product_form).
      onix_export_allowed.
      where(Book.arel_table[:pub_date].lt(Date.today + 3.months)).
      order(pub_date: :desc).
      limit(3)
    end

eb.books += publishing_statuses.flat_map do |pub_status|
      client.books.
      where.not(id: eb.book_ids).
      where(publishing_status: pub_status).
      onix_export_allowed.
      where(Book.arel_table[:pub_date].lt(Date.today + 3.months)).
      order(pub_date: :desc).
      limit(3)
    end

ExportBatchItem.where(id: [11038153, 11038147, 11038127]).destroy_all
