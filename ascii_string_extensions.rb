require 'benchmark'

str = "55746568454796479679x74790"

def method1(str)
  byebug
  str.each_byte.none?{|b| !(48..57).include? b}
end

def method2(str)
  str.each_byte.none?{|b| b > 57 || b < 48}
end

def method3(str)
  str.each_byte.none?{|b| b < 48 || b > 57}
end

def method4(str)
  str.bytes.none?{|b|b < 48 ||  b > 57}
end

def method4(str)
  str.to_i
end

str1 = "5574656845479647967974790x"
str2 = "x5574656845479647967974790"
str3 = "55746568454796479679747903" * 10 + "z"

"Zażółć gęślą jaźń".bytes

00 to 7F hex (0 to 127): first and only byte of a sequence.
80 to BF hex (128 to 191): continuing byte in a multi-byte sequence.
C2 to DF hex (194 to 223): first byte of a two-byte sequence.
E0 to EF hex (224 to 239): first byte of a three-byte sequence.
F0 to FF hex (240 to 255): first byte of a four-byte sequence.

"Zażółć gęślą jaźń".bytesize
"Zażółć gęślą jaźń".size
"Zażółć gęślą jaźń".bytes.count{|x| x & }


def char_class(byte)
  return "1" if byte < 128
  return "C" if byte < 192
  return "2" if byte < 223
  return "3" if byte < 239
  "4"
end

"Zażółć gęślą jaźń".bytes.map{|x| char_class(x)}

n = 1000000
Benchmark.bm(13) do |x|
  x.report("method 0")   { n.times {} }
  x.report("method 1 str1")   { n.times {method1(str1)}}
  x.report("method 2 str1")   { n.times {method2(str1)}}
  x.report("method 3 str1")   { n.times {method3(str1)}}
  x.report("method 4 str1")   { n.times {method4(str1)}}
  x.report("method 1 str2")   { n.times {method1(str2)}}
  x.report("method 2 str2")   { n.times {method2(str2)}}
  x.report("method 3 str2")   { n.times {method3(str2)}}
  x.report("method 4 str2")   { n.times {method4(str2)}}
  x.report("method 1 str3")   { n.times {method1(str3)}}
  x.report("method 2 str3")   { n.times {method2(str3)}}
  x.report("method 3 str3")   { n.times {method3(str3)}}
  x.report("method 4 str3")   { n.times {method4(str3)}}
end ; ""
