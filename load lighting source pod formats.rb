formats =
[
  "POD 1 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Perfect Bound on Creme",
  "POD 10 B&W 6 x 9 in or 229 x 152 mm Gray Cloth on Creme",
  "POD 100 Color 6 x 9 in or 229 - 152 mm Blue Cloth w/Jacket on White",
  "POD 101 Color 6 x 9 in or 229 - 152 mm Gray Cloth w/Jacket on White",
  "POD 102 Color 6.14 x 9.21 in or 234 x 156 mm Saddle Stitch  (4-47 page booklet) on White",
  "POD 103 Color 6.14 x 9.21 in or 234 x 156 mm Perfect Bound (24-480 page book) on White",
  "POD 104 Color 6.14 x 9.21 in or 234 x 156 mm Case Laminate on White",
  "POD 105 Color 6.14 x 9.21 in or 234 x 156 mm Blue Cloth on White",
  "POD 106 Color 6.14 x 9.21 in or 234 x 156 mm Gray Cloth on White",
  "POD 107 Color 6.14 x 9.21 in or 234 x 156 mm Blue Cloth w/Jacket on White",
  "POD 108 Color 6.14 x 9.21 in or 234 x 156 mm Gray Cloth w/Jacket on White",
  "POD 109 Color 7 x 10 in or 254 x 178 mm Case Laminate on White",
  "POD 11 B&W 6 x 9 in or 229 x 152 mm Gray Cloth w/Jacket on Creme",
  "POD 110 Color 8 x 10 in or 254 x 203 mm Case Laminate on White",
  "POD 111 Color 8.5 x 11 in or 280 x 216 mm Case Laminate on White",
  "POD 12 B&W 8.5 x 11 in or 280 x 216 mm Perfect Bound on White",
  "POD 13 B&W 8.5 x 11 in or 280 x 216 mm Case Laminate on White",
  "POD 14 B&W 6 x 9 in or 229 x 152 mm Case Laminate on White",
  "POD 15 B&W 5.5 x 8.38 in or 213 x 140 mm Perfect Bound Matt Lam on Creme",
  "POD 17 B&W 7 x 9.1875 in or 233 x 178 mm Perfect Bound on White",
  "POD 18 B&W 7.375 x 9 in or 229 x 187 mm Perfect Bound on White",
  "POD 2 B&W 6 x 9 in or 229 x 152 mm Perfect Bound on Creme",
  "POD 20 B&W 5 x 8 in or 203 x 127 mm Perfect Bound on White",
  "POD 21 B&W 5.06 x 7.81 in or 198 x 129 mm Perfect Bound on White",
  "POD 22 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Perfect Bound on White",
  "POD 23 B&W 6 x 9 in or 229 x 152 mm Perfect Bound on White",
  "POD 24 B&W 5.83 x 8.27 in or 210 x 148 mm (A5) Perfect Bound on Creme",
  "POD 25 B&W 5.83 x 8.27 in or 210 x 148 mm (A5) Perfect Bound on White",
  "POD 3 B&W 7.5 x 9.25 in or 235 x 191 mm Perfect Bound on White",
  "POD 30 B&W 5.118 x 7.087 in or 180 x 130mm Perfect Bound on Creme",
  "POD 31 B&W 5.118 x 7.087 in or 180 x 130mm Perfect Bound on White",
  "POD 32 B&W 5.315 x 8.465 in or 215 x 135mm Perfect Bound on Creme",
  "POD 33 B&W 5.315 x 8.465 in or 215 x 135mm Perfect Bound on White",
  "POD 34 B&W 5.250 x 8.000 in or 203 x 133mm Perfect Bound on Creme",
  "POD 35 B&W 5.250 x 8.000 in or 203 x 133mm Perfect Bound on White",
  "POD 36 B&W 5.512 x 8.859 in or 225 x 140mm Perfect Bound on Creme",
  "POD 37 B&W 5.512 x 8.859 in or 225 x 140mm Perfect Bound on White",
  "POD 38 B&W 6.024 x 9.252 in or 235 x 153mm Perfect Bound on White",
  "POD 39 B&W 8.000 x 10.000 in or 254 x 203mm Perfect Bound on White",
  "POD 4 B&W 5 x 8 in or 203 x 127 mm Perfect Bound on Creme",
  "POD 42 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Blue Cloth on Creme",
  "POD 43 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Blue Cloth w/Jacket on Creme",
  "POD 44 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Case Laminate on Creme",
  "POD 45 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Gray Cloth on Creme",
  "POD 46 B&W 5.5 x 8.5 in or 216 x 140 mm (Demy 8vo) Gray Cloth w/Jacket on Creme",
  "POD 49 B&W 6.14 x 9.21 in or 234 x 156 mm (Royal 8vo) Perfect Bound on White",
  "POD 50 B&W 7.44 x 9.69 in or 246 x 189 mm (Crown 4vo) Perfect Bound on White",
  "POD 51 B&W 8.25 X 11 in or 280 X 210 mm Tape Bound on White",
  "POD 52 B&W 6.14 x 9.21in or 234 x 156mm (Royal 8vo) Case Laminate on White",
  "POD 53 B&W 6.14 x 9.21in or 234 x 156mm (Royal 8vo) Blue Cloth on White",
  "POD 54 B&W 6.14 x 9.21in or 234 x 156mm (Royal 8vo) Blue Cloth w/Jacket on White",
  "POD 55 B&W 6.14 x 9.21in or 234 x 156mm (Royal 8vo) Gray Cloth on White",
  "POD 56 B&W 6.14 x 9.21in or 234 x 156mm (Royal 8vo) Gray Cloth w/Jacket on White",
  "POD 6 B&W 8.25 x 11 in or 280 x 210 mm Perfect Bound on White",
  "POD 64 B&W 8.268 x 11.693 in or 297 x 210 mm (A4) Perfect Bound on White",
  "POD 66 B&W 7 x 10 in or 254 x 178 mm Perfect Bound on White",
  "POD 67 B&W 6.69 x 9.61 in or 244 x 170 mm (Pinched Crown) Perfect Bound on White",
  "POD 68 B&W 7 x 10 in or 254 x 178 mm Case Laminate on White",
  "POD 7 B&W 6 x 9 in or 229 x 152 mm Blue Cloth on Creme",
  "POD 8 B&W 6 x 9 in or 229 x 152 mm Blue Cloth w/Jacket on Creme",
  "POD 80 Color 8.5 x 8.5 in or 216 x 216 mm Saddle Stitch  (4-47 page booklet) on White",
  "POD 81 Color 8.5 x 8.5 in or 216 x 216 mm Perfect Bound (24-480 page book) on White",
  "POD 82 Color 8.5 x 11 in or 280 x 216 mm Saddle Stitch   (4-47 page booklet) on White",
  "POD 83 Color 8.5 x 11 in or 280 x 216 mm Perfect Bound  (24-480 page book) on White",
  "POD 84 Color 5.5 x 8.5 in or 216 x 140 mm Saddle Stitch  (4-47 page booklet) on White",
  "POD 85 Color 5.5 x 8.5 in or 216 x 140 mm Perfect Bound (24-480 page book) on White",
  "POD 86 Color 7 x 10 in or 254 x 178 mm Saddle Stitch (4-47 page booklet) on White",
  "POD 87 Color 7 x 10 in or 254 x 178 mm Perfect Bound (24-480 page book) on White",
  "POD 88 Color 8 x 10 in or 254 x 203 mm Saddle Stitch (4-47 page booklet) on White",
  "POD 89 Color 8 x 10 in or 254 x 203 mm Perfect Bound (24-480 page book) on White",
  "POD 9 B&W 6 x 9 in or 229 x 152 mm Case Laminate on Creme",
  "POD 90 Color 5.5 x 8.5 in or 216 x 140 mm Case Laminate on White",
  "POD 91 Color 5.5 x 8.5 in or 216 x 140 mm Blue Cloth on White",
  "POD 92 Color 5.5 x 8.5 in or 216 x 140 mm Gray Cloth on White",
  "POD 93 Color 5.5 x 8.5 in or 216 x 140 mm Blue Cloth w/Jacket on White",
  "POD 94 Color 5.5 x 8.5 in or 216 x 140 mm Gray Cloth w/Jacket on White",
  "POD 95 Color 6 x 9 in or 229 - 152 mm Saddle Stitch  (4-47 page booklet) on White",
  "POD 96 Color 6 x 9 in or 229 - 152 mm Perfect Bound (24-480 page book) on White",
  "POD 97 Color 6 x 9 in or 229 - 152 mm Case Laminate on White",
  "POD 98 Color 6 x 9 in or 229 - 152 mm Blue Cloth on White",
  "POD 99 Color 6 x 9 in or 229 - 152 mm  Gray Cloth on White",
  "POD 189 B&W Generic Perfect  (SCAN ONLY)",
  "POD 190 B&W Generic Case  (SCAN ONLY)",
  "POD 191 B&W Generic Other  (SCAN ONLY)",
  "POD 192 Color Generic Perfect (SCAN ONLY)",
  "POD 193 Color Generic Case (SCAN ONLY)",
  "POD 194 Color Generic Other (SCAN ONLY)",
  "POD 195 B&W Generic ShortRun (Below 6x9) on White",
  "POD 196 B&W Generic ShortRun (Above 6x9) on White",
  "POD 197 B&W Generic Capped-Only  ShortRun (6x9 and smaller) on Creme",
  "POD 198 B&W Generic  ShortRun (6x9 and smaller) on Creme",
  "POD 199 B&W Generic ShortRun (Above 8.25x11) on White",
  "POD 300 MultiVolume POD Book",
  "POD 70 B&W Outsourced - Perfect Bound on Creme",
  "POD 71 B&W Outsourced - Case Bound on Creme",
  "POD 72 B&W Outsourced - Case Bound w/Jacket on Creme",
  "POD 73 Color Outsourced -  Perfect Bound on Creme",
  "POD 74 Color Outsourced -  Case Bound on Creme",
  "POD 75 Color Outsourced -  Case Bound w/Jacket on Creme",
]



client = Client.is_cpg

ActiveRecord::Base.transaction do
  formats.each do |format_name|
    product_form =
      if format_name.include? "Cloth"
        "BB"
      elsif format_name.include? "Case"
        "BB"
      elsif format_name.include? "Perfect"
        "BC"
      else
        "BA"
      end
    page_trim_width_in, page_trim_height_in, page_trim_height_mm, page_trim_width_mm =
      format_name.match(/ ([\d\.]+) [Xx] ([\d\.]+).* ([\d]+) [Xx-] ([\d]+)/)&.captures&.map(&:to_d)

    if product_form == "BB"
      product_width_in  = page_trim_width_in + 0.125  if page_trim_width_in
      product_height_in = page_trim_height_in + 0.25  if page_trim_height_in
      product_height_mm = page_trim_height_mm + 7  if page_trim_height_mm
      product_width_mm  = page_trim_width_mm + 4  if page_trim_width_mm
    else
      product_width_in  = page_trim_width_in
      product_height_in = page_trim_height_in
      product_height_mm = page_trim_height_mm
      product_width_mm  = page_trim_width_mm
    end

    if client.metric_units_only?
      client.proprietary_format_descriptions.create(
        name:                format_name,
        product_form:        product_form,
        product_height_mm:   product_height_mm,
        product_width_mm:    product_width_mm,
        page_trim_height_mm: page_trim_height_mm,
        page_trim_width_mm:  page_trim_width_mm
      )
    elsif client.us_units_only?
      client.proprietary_format_descriptions.create(
        name:                format_name,
        product_form:        product_form,
        product_height_in:   product_height_in,
        product_width_in:    product_width_in,
        page_trim_height_in: page_trim_height_in,
        page_trim_width_in:  page_trim_width_in
      )
    else
      client.proprietary_format_descriptions.create(
        name:                format_name,
        product_form:        product_form,
        product_height_mm:   product_height_mm,
        product_width_mm:    product_width_mm,
        page_trim_height_mm: page_trim_height_mm,
        page_trim_width_mm:  page_trim_width_mm,
        product_height_in:   product_height_in,
        product_width_in:    product_width_in,
        page_trim_height_in: page_trim_height_in,
        page_trim_width_in:  page_trim_width_in
      )
    end
  end
end

client.proprietary_format_descriptions.where("name like 'POD %'").destroy_all
