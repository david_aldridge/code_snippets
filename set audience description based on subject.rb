aud = {
"Cereals" => "Academic researchers in cereal science; International and national agencies supporting agricultural development; Cereal processors and companies supplying the agricultural sector (e.g. seed companies)",
"Fruit, vegetables & nuts" => "Academic researchers in horticultural science; Government agencies supporting horticulture; Fruit and vegetable processors",
"Cassava" => "Academic researchers in crop science; International and national agencies supporting agricultural development; Companies supplying the agricultural sector (e.g. seed companies)",
"Beef" => "Academic researchers in meat, dairy and animal science; Government agencies responsible for food safety/quality and livestock farming; Meat and dairy processors",
"Milk" => "Academic researchers in meat, dairy and animal science; Government agencies responsible for food safety/quality and livestock farming; Meat and dairy processors",
"Poultry" => "Academic researchers in poultry science; Government agencies responsible for food safety/quality and poultry farming; Poultry and egg processors",
"Eggs" => "Academic researchers in poultry science; Government agencies responsible for food safety/quality and poultry farming; Poultry and egg processors",
"Sheep" => "Academic researchers in animal (esp. small ruminant) science; Government agencies responsible for food safety/quality and livestock farming; Meat processors",
"Wheat" => "Academic researchers in agricultural science; International and national agencies supporting agricultural development; Companies supplying the agricultural sector (e.g. seed companies)"}



 Client.is_bds.works.each do |work|
   work.assign_attributes(:audience_description => aud[work.main_subject])
   work.save if work.changed?
  end



Client.is_bds.works.map(&:main_subject)

  ["Beef", "Fruit, vegetables & nuts", "Livestock", "Cereals", "Poultry", "Wheat", "Cassava", "Sheep", "Milk", "Physiology & breeding", "Maize", "Eggs", "Rice", "Management", "Tomatoes", "Apples", "Pests & diseases"]