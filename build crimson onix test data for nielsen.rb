client = Client.is_crimson

product_forms = client.books.pluck(:product_form).compact.uniq - ["00"]
publishing_statuses = client.books.pluck(:publishing_status).compact.uniq


eb = client.export_batches.create(
  contact_digital_asset_transfer_template_id: client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first.id,
  name: "Nielsen test 2",
  incremental: false
  )

eb.books +=
  client.imprints.flat_map do |imprint|
      books = imprint.books.
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))

      selected = books.order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "imprint #{imprint}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
product_forms.flat_map do |product_form|
      books = client.books.
        where(product_form: product_form).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))


      selected = books.
        where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "form #{product_form}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
publishing_statuses.flat_map do |pub_status|
      books = client.books.
        where(publishing_status: pub_status).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))

      selected = books.where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select(&:short_description).
        select{|b| b.marketingtext_descriptions || b.marketingtext_long_descriptions}.
        select(&:main_bic_code).
        first(3)
      puts "status #{pub_status} (#{OnixCode.lookup(:publishing_statuses, pub_status)}): #{selected.size} of #{books.size}"
      selected
    end ; 0

ExportBatchItem.where(id: [11038153, 11038147, 11038127]).destroy_all




client.works.select do |w|
  !w.marketingtexts.where(legacy_code: "02").any? && w.marketingtexts.where(legacy_code: "03").any?
end.size


client.works.includes(:marketingtexts).map do |w|
  [
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)


client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)

["04", false, true, true]=>166,

client.books.onix_export_allowed.includes(:marketingtexts).select do |w|
    w.marketingtexts.none?{|x| x.legacy_code == "01"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "02"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
end.map do |w|
  [
    w.marketingtexts.detect{|x| x.legacy_code == "02"}.pull_quote.size,
    w.marketingtexts.detect{|x| x.legacy_code == "03"}.pull_quote.size
  ]
end.sort
group_by(&:itself).transform_values(&:size)







client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.detect{|x| x.legacy_code == "01"}&.pull_quote&.size&.>(350),
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.detect{|x| x.legacy_code == "02"}&.pull_quote&.size&.<=(350),
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)







patrick@crimsonpublishing.co.uk

21d Charles Street
Bath
BA1 1HX


client = Client.is_crimson
client.books.onix_export_allowed.map do |b|
 [
   b.imprint.to_s,
   b.isbn_registrant_hyphenated
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}

client.books.onix_export_allowed.map do |b|
 [
   b.isbn_registrant_hyphenated,
   b.imprint.to_s
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}




 "978-1-85458"   =>  "Crimson Publishing", "Trotman", "Pathfinder Guides", "Vacation Work"
 "978-1-911067"  => "Trotman Education"
 "978-1-78059"   => "Crimson Publishing", "Pathfinder Guides", "Time Out Guides"
 "978-1-84455"   => "Trotman", "Trotman Education"
 "978-1-905410"  => "White Ladder Press"
 "978-1-910336"  => "White Ladder Press"
 "978-0-9544767" => "Lifestyle Press"
 "978-1-908281"  => "White Ladder Press"
 "978-1-907087"  => "Pocket Bibles"
 "978-1-906041"  => "Trotman Education"
 "978-1-909319"  => "Trotman Education"
 "978-0-9553308" => "Monstermedia"
 "978-0-7117"    => "Pathfinder Guides", "Crimson Short Walks"
 "978-988-99143" => "Daiichi Publishers Co. Ltd."
 "978-1-84670"   => "Time Out Guides"
 "978-1-905042"  => "Time Out Guides"
 "978-0-9545803" => "Malnoue Publications"
 "978-0-9568350" => "Trotman Education"




isbns = %w(9780856609909
9781841623849
9781854586063
9781905410491
9781909319868
9781909319929
9781910336106
9781910336434
9781780592602)




978‑1‑911067‑86‑3
978‑1‑911067‑87‑0