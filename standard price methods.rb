  def standard_price_methods
    standard_price_types.flat_map do |typ|
      standard_price_qualifiers.map do |qual|
        "current_#{typ}_#{qual}_price".to_sym
      end
    end
  end
