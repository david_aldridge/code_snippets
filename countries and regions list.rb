TW: Taiwan, Province of China
TZ: Tanzania, United Republic of
UA: Ukraine
UG: Uganda
UM: United States Minor Outlying Islands
US: United States
US-AK: Alaska
US-AL: Alabama
US-AR: Arkansas
US-AZ: Arizona
US-CA: California
US-CO: Colorado
US-CT: Connecticut
US-DC: District of Columbia
US-DE: Delaware
US-FL: Florida
US-GA: Georgia
US-HI: Hawaii
US-IA: Iowa


(OnixCode.where(list_number: 49).pluck(:value, :description) + OnixCode.where(list_number: 91).pluck(:value, :description)).sort

ONIXValue::Country.dropdown + ONIXValue::Region.select{|r| r.subnational? && !r.rights_only?}.map(&:to_dropdown)