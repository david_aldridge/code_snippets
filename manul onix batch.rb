client = Client.is_pharma

template  = client.
              contacts.
              find_by(identifier: "amazon-p2k").
              contact_digital_asset_transfer_templates.
              first

books     = client.
              books.
              is_print.
              onix_export_allowed.
              where("pub_date <= ? ", Date.today + 13.months)

books     = client.
              books.
              pdfs.
              onix_export_allowed.
              where("pub_date <= ? ", Date.today + 13.months) +
            client.
              books.
              epubs.
              onix_export_allowed.
              where("pub_date <= ? ", Date.today + 13.months)


books     = client.
              books.
              kindles.
              onix_export_allowed.
              where("pub_date <= ? ", Date.today + 13.months)

DigitalAssetPush.new(
  client,
  template,
  books: books,
  incremental: false,
  transmit:    false
).run
