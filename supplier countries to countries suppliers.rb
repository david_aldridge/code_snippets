suppliers = {
x: [1,2,3,4,5,6,7,8,9],
y: [2,4,7,8],
z: [2,3,7,8,9]
}


all = suppliers.values.flatten.uniq

all.each_with_object(Hash.new([])) do |country, hash|
  suppliers.each do |supplier, countries|
    hash[country] += [supplier] if country.in? countries
  end
end.to_a.group_by(&:last).transform_values{|v| v.map(&:first)}





all.each_with_object(Hash.new([])) do |country, hash|
  hash[country] = suppliers.select do |supplier, countries|
    country.in? countries
  end.keys
end.to_a.group_by(&:last).transform_values{|v| v.map(&:first)}




suppliers = {
  "Publisher" => ["GB", "FR", "MX", "US", "CA"],
  "Ingram"    => ["US", "CA"],
  "NBNI"      => ["GB", "FR", "MX"],
  "Agency A"  => ["MX", "US"],
  "Agency B"  => ["CA", "FR"]
  }

suppliers.values.flatten.uniq.each_with_object(Hash.new([])) do |country, hash|
  hash[country] = suppliers.select do |supplier, countries|
    country.in? countries
  end.keys
end.to_a.group_by(&:last).transform_values{|v| v.map(&:first)}.invert

pp _
