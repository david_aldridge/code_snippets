delete from versions where(item_id, item_type)in (
select item_id, item_type
from versions v
where event = 'destroy' and
created_at < current_date - interval '1 months'
and not exists (select null
from versions v2
where v2.item_id = v.item_id and v2.item_type = v.item_type
and v2.created_at > v.created_at and event != 'destroy'));

delete from versions where item_type in
('Royarchive', 'Roystatement', 'Altcontact',
'ProductTitle',
'Task',
'TaskUser',
'PrintCosting',
'PrintItem',
'Onixarchive',
'Righttype',
'Quickstart',
'EstimateTemplate',
'EstimatePeriod',
'Masterrule',
'Rule',
'Roystatement',
'RoyaltyLiability',
'RoyaltyQuantityEscalatorChannelRate',
'Profitarchive',
'Report',
'Level',
'ProductionFile',
'Altcontact',
'Post',
'Login',
'Comment',
'Web',
'Expectedsale',
'Tooltip',
'Period',
'Periodactive',
'Contractset',
'DigitalAsset',
'MeasurementType',
'Format',
'MeasurementType',
'MeasurementUnit',
'Measurement',
'Page',
'WedgeElement',
'Wedge',
'Extent',
'ExtentType',
'ExtentUnit')
