client = Client.by_webname("hbkupress")

client.books.group(:publication_city, :publication_country).count


client.books.update_all(:publication_city => nil, :publication_country => nil)
client.works.update_all(:publication_city => nil, :publication_country => nil)

pp client.books.group(:edition_statement).count

nil=>358,
 "Export/Airside"=>4,
 "World Book Day"=>3,
 "PLO Shelf"=>1,
 "Paperback (second edition)"=>10,
 "ePdf"=>1,
 "Slipcase Hardback"=>1,
 "EPUB"=>3,
 "Arabic"=>1,
 "Hardback - FOR CLIENT ONLY"=>1,
 "Epdf for Dutch deal"=>1,
 "VIP Slipcase"=>1,
 "VIP edition for Oxy"=>1,
 "ePUB"=>1,
 "EPDF"=>5,
 "Epdfs for Dutch Deal"=>5,
 "Epdf for Dutch Deal"=>6,
 "Special Edition"=>1,
 "ePDF"=>8,
 "VIP Hardback edition"=>1,
 "ePub"=>7,
 "QMA"=>1,
 "Public Library Online"=>17,
 "Tie-In"=>5}


EDITION_MAP= {
"abridged edition"                =>["ABR"  ,nil],
"2nd"                             =>[[]     ,2],
"5th Revised edition"             =>[["REV"],5],
"Revised and expanded ed"         =>[["REV","ENL"]],
"Limited edition"                 =>3,
"Special edition"                 =>[["SPE"],nil],
"New ed of 2 Revised ed (1936)"   =>[["REV","NEW"],2]
"3rd Revised edition"             =>[["REV"],3],
"illustrated edition"             =>[["ILL"],nil],
"special edition with print"      =>[["SPE"],nil],
"4th Revised edition"             =>[["REV"],4],
"28th edition"                    =>[[]     ,28],
"4th"                             =>[[]     ,4],
"2nd Revised edition"             =>[["REV"],2],
"Limited ed"                      =>[], Is this NUMbered?
"New edition"                     =>[["NEW"],nil],
"Revised edition"                 =>[["REV"],nil],
"3rd"                             =>[[],3]}

client.prices.where.not(:currency_code => ["GBP","USD"]).count
client.prices.group(:price_status).count

client = Client.by_key("lund")
client.books.select{|b| b.product_form.try(:[],0) == "P"}.count

client = Client.by_key("lund")
client.prices.where(:price_type => "21").update_all(:price_type => "01")

client = Client.by_key("lund")
client.prices.where(:price_type => "01", :currency_code => "GBP").update_all(:price_type => "02")
client.prices.where(:price_type => "02", :currency_code => "USD").update_all(:price_type => "01")








##########################################


client = Client.by_key("lund")

doc = Nokogiri::XML(open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/lundh_onix.xml")) ; 0
#doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/Lund Humphries/Nielsen ONIX/lundh_onix.xml")) ; 0
doc.remove_namespaces! ; 0
doc.xpath("//Series/Title/TitleText").map{|x| x.text}.each_with_object(Hash.new(0)){|s, h| h[s] += 1}



include ApplicationHelper
client = Client.by_key("lund")
client.seriesnames.destroy_all ; 0
ActiveRecord::Base.transaction do
  doc.xpath("//Series").each do |series_node|
    title_prefix, title_without_prefix = de_prefixer(series_node.at_xpath("Title/TitleText").text)
    series = Seriesname.where( title_prefix:         title_prefix,
                               title_without_prefix: title_without_prefix,
                               client_id:            client.id,
                               collection_type:      "Series".freeze).first_or_create!
    if isbn =  series_node.at_xpath("../ProductIdentifier[ProductIDType='15']/IDValue").text
      if work =  client.works.joins(:books).references(:books).where(:books => {:isbn => isbn}).take
        work_seriesname = WorkSeriesname.where(:work_id       => work.id,
                                               :seriesname_id => series.id).
                                         first_or_create!(:number_within_series => series_node.at_xpath("NumberWithinSeries").try(:text))
      end
    end
  end
end


client = Client.by_key("lund")

client.books.where(:isbn => ["978075464".."978075466"]).destroy_all
client.books.where(:isbn => "9781874084006").destroy_all


client.works.where.not(Book.where("books.work_id = works.id").exists)


client.books.select(:id, :isbn, :client_id, :imprint_id).select{|b| b.isbn_valid?}.each do |book|
  book.create_isbnlist!(:number     => book.isbn     ,
                        :used       => "used"        ,
                        :client_id  => book.client_id,
                        :imprint_id => book.imprint_id)
end

pp client.isbnlists.order(:number).map{|i| Lisbn.new(i.number).isbn_with_dash}.map{|i| [i.match(/^\d*-\d*-\d*/)[0], i]}.group_by{|x| x[0]}.map{|k,v| v.map{|x| x[1]}.max}

https://grp.isbn-international.org/
{"978-0-85331"=>472
"978-1-84822"=>164 Lund Humphries Publishers Ltd
"978-0-86824"=>2 Random House Australia
"978-1-86317"=>18 Powerhouse Publishing
"978-0-85351"=>1 Relate
"978-0-9515811"=>1 "Monkshatch Publications" "Veronica Gould"
"978-1-85745"=>1 "World Architecture" "Kenrick Place Media Ltd" "IBC Business Publishing" "Century Press (a division of Grosvenor Press International)"
"978-0-7546"=>9 Ashgate Publishing Limited
"978-90-72828"=>1 M HKA vzw (Belgium)
"978-1-874084"=>2 Severnside Printers Ltd
"978-0-88179"=>8 ?
"978-0-904831"=>3 Eaton House Publishers Ltd, Art Review
"978-0-85307"=>1 Johnson Publications Ltd
"978-90-238"=>1} SMD Educatieve Uitgevers







EDITION_MAP= {
"abridged edition"                =>[["ABR"]      ,nil],
"2nd"                             =>[nil             ,2  ],
"5th Revised edition"             =>[["REV"]      ,5  ],
"Revised and expanded ed"         =>[["REV","ENL"],nil],
"Limited edition"                 =>[["SPE"]      ,3  , "Limited special edition, in slipcase with removable, numbered, limited edition print by the artist"],
"Special edition"                 =>[["SPE"]      ,nil, "Limited special edition, in slipcase with removable, numbered, limited edition print by the artist"],
"Limited ed"                      =>[["SPE"]      ,nil, "Limited special edition, in slipcase with removable, numbered, limited edition print by the artist"],
"New ed of 2 Revised ed (1936)"   =>[["REV","NEW"],2  ],
"3rd Revised edition"             =>[["REV"]      ,3  ],
"illustrated edition"             =>[["ILL"]      ,nil],
"special edition with print"      =>[["SPE"]      ,nil, "Limited special edition, in slipcase with removable, numbered, limited edition print by the artist"],
"4th Revised edition"             =>[["REV"]      ,4  ],
"28th edition"                    =>[nil             ,28 ],
"4th"                             =>[nil          ,4  ],
"2nd Revised edition"             =>[["REV"]      ,2  ],
"New edition"                     =>[["NEW"]      ,nil],
"Revised edition"                 =>[["REV"]      ,nil],
"3rd"                             =>[nil          ,3  ],
"The millennium ed"               =>[nil          ,nil,"The Millenium Edition"]}

client = Client.by_key("lund")
Rails.logger.level = 1
ActiveRecord::Base.transaction do
  client.books.where(:edition_statement => EDITION_MAP.keys).each do |book|
    book.update_attributes(:edition            => EDITION_MAP[book.edition_statement][1],
                           :edition_statement  => EDITION_MAP[book.edition_statement][2],
                           :edition_type_codes => EDITION_MAP[book.edition_statement][0])
  end

end

client.books.group(:edition , :edition_statement ,:edition_type_codes).count





new = ['9781848221451',
'9781848221505',
'9781848220638',
'9781848220720',
'9780853318972',
'9780853318804']

client = Client.by_key("lund")

client.books.where.not(:edition_type_codes => nil).select{|b| b.edition_type_codes.include? 'NEW'}.each do |b|
  b.edition_type_codes = b.edition_type_codes - ["NEW"] + ["NED"]
  b.save!
end

client.books.where.not(:isbn => new).
             where.not(:edition_type_codes => nil).
             select{|b| b.edition_type_codes.include? 'NED'}.each do |b|
  b.edition_type_codes = b.edition_type_codes - ["NED"]
  b.save!
end ; 0

client.books.where(:isbn => new).each do |b|
  b.edition_type_codes = ((b.edition_type_codes || []) + ["NED"]).uniq.sort
  b.save! if b.changed?
end ; 0






client.works.where.not(Book.where("books.work_id = works.id").exists).each {|w| w.contract.destroy}




====================
load ISBNs
====================


9781848221925


block              = "978184822"
block_length       = block.size
remaining chars    = 13 - block_length
publication_digits = chars - 1
publication_max    = 10**publication_digits
isbns              = []
publication_max.times do |n|
  raw_isbn = "#{block}#{'%03d' % n}0"
  isbn     = raw_isbn[0..-2] + Lisbn.new(raw_isbn).isbn_13_checksum
  isbns << isbn if Lisbn.new(isbn).valid?
end


client          = Client.by_key("lund")
imprint         = client.imprints.first
isbnlist_isbns  = client.isbnlists.pluck(:number)

client.books.includes(:work => :imprint).select{|b| b.isbn_valid?}.reject{|b| isbnlist_isbns.include?(b.isbn)}.each do |book|
  new_isbnlist = Isbnlist.new(:number     => book.isbn     ,
                              :used       => "used"        ,
                              :client_id  => book.client_id,
                              :imprint    => book.imprint.value,
                              :book_id    => book.id)
  new_isbnlist.save!
end ; 0

def Isbnlist
block              = "978184822"
block_length       = block.size
remaining chars    = 13 - block_length
publication_digits = chars - 1
publication_max    = 10**publication_digits
isbns              = []
publication_max.times do |n|
  raw_isbn = "#{block}#{'%03d' % n}0"
  isbn     = raw_isbn[0..-2] + Lisbn.new(raw_isbn).isbn_13_checksum
  isbns << isbn if Lisbn.new(isbn).valid?
end ; 0

isbnlist_isbns  = client.isbnlists.pluck(:number)

(isbns - isbnlist_isbns).each do |isbn|
  isbnlist = Isbnlist.create!(:number     => isbn          ,
                              :used       => (isbn < '9781848221925') ? 'used' : 'not used'        ,
                              :client_id  => client.id,
                              :imprint    => imprint.value)

end




def import(client,imprint,block, first_available = 0)
  raise ArguemntError "client is a (#{client.class.name}): should be a Client"     unless client.is_a? Client
  raise ArguemntError "imprint is a (#{imprint.class.name}): should be an Imprint" unless imprint.is_a? Imprint
end

import(Imprint.first, Client.first,"r")






========================================================

Reload


[ '9780853319108',
  '9781848220300',
  '9781848220317',
  '9781848220324',
  '9781848220539',
  '9781848220515']

c = Client.find_by_webname("lundhumphries")
c.proprietary_edition_descriptions.pluck(:code)

PaperTrail.enabled = false
Rails.logger.level = 1

x                  = OnixImport.new

ActiveRecord::Base.transaction do
  x.import_from_url(c.id, c.users.first, "https://s3-eu-west-1.amazonaws.com/bibliocloudimages/lundh_anthony_caro.xml" ,false,false) ; 0
  #x.import_from_url(c.id, c.users.first, "/Users/david/Documents/Bibliocloud/Clients/Lund Humphries/Nielsen ONIX/lundh_onix.xml" ,false,false)
  #raise ActiveRecord::Rollback
end








========================================================




code_map = {
            "01"=>"02",
            "02"=>"01",
            "03"=>"03",
            "04"=>"05",
            "08"=>"08",
            "09"=>"10",
            "19"=>"11",
            "13"=>"20",
            "25"=>"13"
          }
Rails.logger.level = 3
client = Client.by_key("lund")
client.marketingtexts.where(:legacy_code => code_map.keys).each_with_index do |mt, i|
  puts i+1
  mt.main_type = code_map[mt.legacy_code]
  mt.save! if mt.changed?
end ; 0
