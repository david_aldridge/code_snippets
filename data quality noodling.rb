class Validation < Struct.new(:level, :identifier, :info)

  LEVELS = {
  0 => :pass,
  1 => :info,
  2 => :warning,
  3 => :error
  }

end


class ClientStatistics
  def initialize(client)
    self.client = client
  end

  def count_of_works
    @count_of_works ||= client.works.size
  end

  def count_of_works_with_main_bisac_code
    @count_of_works_with_main_bisac_code ||= client.works.where.not(main_bisac_code: nil).count
  end

  private

  attr_accessor :client
end


class Client< ActiveRecord::Base

  def statistics
    @statistics ||= ClientStatistics.new(self)
  end
end


class WorkValidation
  def initialize(work)
    self.work = work
  end

  def call
  [
  no_imprint,
  no_publisher,
  no_main_bisac
  ].compact
  end

  private

  attr_accessor :work

  def no_imprint
    return if work.imprint
    Validation.new(3, :"work/no_imprint")
  end

  def no_publisher
    return if work.publishername
    Validation.new(3, :"work/no_publisher")
  end

  def no_main_bisac
    return if work.main_bisac_code
    client_fraction_with_no_bisac = client_statistics.count_of_works_with_main_bisac_code.to_f / client_statistics.count_of_works
    Validation.new(3, :"work/subject/no_main_bisac", client_fraction_with_no_bisac)
  end

  def no_main_bisac?
    work.main_bisac_code.nil?
  end

  def no_main_thema
    return if work.thema_main_code
    Validation.new(3, :"work/subject/no_main_thema")
  end

  def client_statistics
    work.client.statistics
  end

end

class Work < ActiveRecord::Base
  def validations
    WorkValidation.new(self).call
  end
end

Client.first.works.includes(:imprint,:publishername).map(&:validations)
