Client.is_sas.books.joins(:proprietary_format_description).each_with_object(Hash.new(0)){|b,h| h[ [b.proprietary_format_description.try(:epub_type_code), b.epub_type_code] ] += 1 }

{["001", "001"]=>50, ["031", "029"]=>11, [nil, ""]=>89, ["029", "029"]=>58, ["031", "031"]=>46, ["002", "029"]=>19, ["001", "029"]=>5, ["002", "002"]=>43}


{["029", "029"]=>58, ["002", "002"]=>62, ["001", "001"]=>55, ["031", "031"]=>57, [nil, ""]=>89}


Rails.logger.level=1

Client.is_ucl.books.where.not(:proprietary_format_description_id => nil).each do |b|
  restore_to_pfd_id = b.proprietary_format_description_id
  b.proprietary_format_description_id = nil
  b.save

  b.proprietary_format_description_id = restore_to_pfd_id
  b.save
end




Client.is_sas.books.where.not(:proprietary_format_description_id => nil).each do |b|
  restore_to_pfd_id = b.proprietary_format_description_id
  b.proprietary_format_description_id = nil
  b.save

  b.proprietary_format_description_id = restore_to_pfd_id
  b.save
end
