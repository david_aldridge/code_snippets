  Supportingresource.find_each do |resource|
  next unless resource.image&.exists?
  resource.image.copy_to_local_file(:original, resource.image_file_name)
  geometry = Paperclip::Geometry.from_file(resource.image_file_name)
  resource.update_columns(
    image_width:     geometry.width.to_i,
    image_height:    geometry.height.to_i,
    image_file_size: resource.image.size
  )
  File.delete(resource.image_file_name)
end
