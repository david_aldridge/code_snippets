RequestLog.
  where("request_logs.created_at >= ? and request_logs.created_at < ?", Date.civil(2017,05,01), Date.civil(2017,06,01) ).
  joins(user: :client).
  where.not("email like '%bibliocloud%'").
  group(:user_id).
  count("distinct date_trunc('day', request_logs.created_at)")
