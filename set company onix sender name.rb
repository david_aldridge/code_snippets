Company.where(onix_sender_company_name: nil).joins(:client).includes(:client).each do |company|
  company.update(onix_sender_company_name: company.client.client_name)
end
