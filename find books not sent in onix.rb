sent_books = Client.is_lund.contacts.is_nbni.contact_digital_asset_transfer_templates.first.digital_assets.flat_map(&:books).uniq

all_books = Client.is_lund.books

(all_books - sent_books).map(&:pub_date).compact.sort

collection = Client.is_lund.collections.create(name: "Not sent to NBNI")
