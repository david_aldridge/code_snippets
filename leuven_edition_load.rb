connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1kvFzBRBwUjBNbYXER5enPlEaD7kej4CuDKk6LRHEOw0")
metadata_sheet = sheet.worksheets.first
metadata       = metadata_sheet.list
client         = Client.is_leuven

class LoadLeuvenRow
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    return unless book && edition
    book.proprietary_edition_description = edition
    book.save if book.changed?
  end

  private

  def isbn
    row["isbn"].squish
  end

  def book
    @book ||= client.books.find_by(isbn: isbn)
  end

  def edition_name
    row["Inhouseeditiontype"].squish.presence
  end

  def edition
    return unless edition_name
    @edition ||= client.proprietary_edition_descriptions.find_by(name: edition_name)
  end

  attr_reader :row, :client
end

metadata.each do |row|
  LoadLeuvenRow.new(row, client).call
end
