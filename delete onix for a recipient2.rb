client    = Client.is_restless_books
recipient = client.contacts.is_pgw


recipient.digital_assets.each do |da|
  da.contact_digital_asset_transfers.destroy_all
  da.destroy!
end



recipient.contact_digital_asset_transfer_templates.first.export_batches.each {|eb| eb.destroy! }


ContactDigitalAssetTransferInclude.
  joins(contact: :client).
  where(
    contacts: {
      identifier: recipient.identifier,
      },
    clients:  {
      identifier: client.identifier,
      }
  ).destroy_all



  ContactDigitalAssetTransferInclude.
  joins(contact: :client).
  where(
    transfer_includeable_type: "Supportingresource",
    contacts: {
      identifier: recipient.identifier,
      },
    clients:  {
      identifier: client.identifier,
      }
  ).destroy_all
