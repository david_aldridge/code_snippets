connection     = Google::DriveConn.new
sheet          = connection.session.spreadsheet_by_key("1foUsxLvLlyQhjYOv7wtp4sYSf1Fe0UAZlkpDeKOuH-c")
metadata_sheet = sheet.worksheets.first
metadata       = metadata_sheet.list
client         = Client.is_leuven

class LoadLeuvenRow
  def initialize(row, client)
    @row    = row
    @client = client
  end

  def call
    return unless book && publishing_status
    book.update_attributes(publishing_status: publishing_status)
  end

  private

  def isbn
    row["isbn"].squish
  end

  def title
    row["title"].squish
  end

  def publishing_status
    row["status"].squish
  end

  def book
    @book ||=
      find_book(isbn: isbn) ||
      find_book(title: title) ||
      find_book("upper(title) = ?", title.upcase)
  end

  def find_book(arg, *args)
    puts arg
    puts *args
    Client.is_leuven.books.find_by arg, *args
  end

  attr_reader :row, :client
end

ActiveRecord::Base.transaction do
  metadata.each do |row|
    LoadLeuvenRow.new(row, client).call
  end
end
