Client.is_leuven.works.includes(:language_of_text).map(&:language_of_text).compact.map(&:language_code).each_with_object(Hash.new(0)){|l,h| h[l] += 1}
{"dut"=>1568, "eng"=>593, "ger"=>5, "fre"=>29, "mul"=>62, "spa"=>4, "ita"=>4, "lat"=>2}
