@doc = Nokogiri::XML(open('/Users/david/rails/onix_ninja_files/KoganPage030620160911staging.xml'), nil) do |config|
  config.default_xml.noblanks
end



@doc = Nokogiri::XML(open('/Users/david/rails/onix_ninja_files/Metadata_Only_Kogan_Page_0003629916.xml-2016-05-31-22_25_16 (1).xml'), nil) do |config|
  config.default_xml.noblanks
end



@doc.xpath("//xmlns:Product").each do |product|
   ref  = product.at_xpath("xmlns:RecordReference").text
   pf  = product.at_xpath("xmlns:DescriptiveDetail/xmlns:ProductForm").text
   isbn = product.at_xpath("xmlns:ProductIdentifier[xmlns:ProductIDType='03']/xmlns:IDValue").try(:text)
   rp = product.xpath("xmlns:RelatedMaterial/xmlns:RelatedProduct").map{|rp| [rp.at_xpath("xmlns:ProductRelationCode").try(:text) || 'None', rp.at_xpath("xmlns:ProductIdentifier/xmlns:IDValue").try(:text) || 'None'].join(" -> ")}.sort
   puts "#{ref} #{pf} (#{isbn}): #{rp.join(', ')}"
end


@doc.xpath("//Product").each do |product|
   ref  = product.at_xpath("RecordReference").text
   pf  = product.at_xpath("DescriptiveDetail/ProductForm").try(:text)
   isbn = product.at_xpath("ProductIdentifier[ProductIDType='03']/IDValue").try(:text)
   rp = product.xpath("PublishingStatus").try(:text)
   puts "\"#{ref}\",\"#{rp}\""
end





doc = Nokogiri::XML(open('/Users/david/Documents/Bibliocloud/Clients/Purdue/purdue.xml'), nil) do |config|
  config.default_xml.noblanks
end



x = doc.xpath("//TitleText").select{|x| x.text.starts_with?("Veterinary Medical School Admission Requirements")}.map do |node|
  [
    node.xpath("../../ProductForm").text,
    node.xpath("../../RelatedProduct").size
  ]
end


pp x.sort
