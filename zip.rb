zipfilepath = "/Users/david/Downloads/test 2.zip"

Zip::File.open('foo.zip') do |zip_file|
  # Handle entries one by one
  zip_file.each do |entry|
    # Extract to file/directory/symlink
    puts "Extracting #{entry.name}"
    entry.extract(dest_file)

    # Read into memory
    content = entry.get_input_stream.read
  end

  # Find specific entry
  entry = zip_file.glob('*.csv').first
  puts entry.get_input_stream.read
end




Zip::File.open(zipfilepath) do |zip_file|
  zip_file.each do |entry|
    if entry.name.starts_with? "__MACOSX"
      zip_file.remove entry
    end
  end
  zip_file.close
end

Zip::File.open(zipfilepath) do |zip_file|
  zip_file.each do |entry|
    if entry.directory?
      puts "#{entry.name} is a folder!"
    elsif entry.symlink?
      puts "#{entry.name} is a symlink!"
    elsif entry.file?
      puts "#{entry.name} is a regular file!"
    else
      puts "#{entry.name} is something unknown, oops!"
    end
  end
  zip_file.close
end ; 0
