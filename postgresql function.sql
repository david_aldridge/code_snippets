CREATE or REPLACE FUNCTION product_to_s(book books) RETURNS text AS $$
BEGIN
    RETURN  book.title || ' ' ||
            book.product_form || ' ' ||
            case
              when book.isbn = 'placeholder'
              then '(no ISBN assigned)'
              else book.isbn
            end;
END;
$$ LANGUAGE plpgsql;
