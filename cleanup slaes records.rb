Sale.includes(:book => :book_masterchannels).where(:book_channel_id => nil).each do |s|
  next unless s.book
  s.update_columns(:book_channel_id => s.book.book_channels.find_by(:channel_id => s.channel_id).try(:id))
end

Sale.where(:book_channel_id => nil).group(:client_id).count


Sale.where.not(Book.where("books.id = sales.book_id").exists).group(:client_id).count
Sale.where(Book.where("books.id = sales.book_id").exists).count

Sale.where.not(Book.where("books.id = sales.book_id").exists).delete_all

Sale.where(:book_channel_id => nil).includes(:channel, :book).map{|s| [s.book, s.channel]}.uniq.each do |book,channel|
  CreateBookChannelService.new( book: book, channel: channel).call
end



Sale.where.not(:book_channel_id => nil).eager_load_for_royalty_specifiers.select(&:has_royalty_specifier?).size
