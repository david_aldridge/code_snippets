require 'benchmark'

def method1(string, n)
  string.gsub(/.{#{n}}/){ |sub| sub.chop }
end

def method2(string, n)
  (0..(string.bytesize / n)).each_with_object("") do |x, new_string|
    new_string << string.byteslice(x*n,n-1)
  end
end

string = '1234A1234B1234C'
n      = 5
method1(string, n)
method2(string, n)

string = 'ABAB'
n      = 2
method1(string, n)
method2(string, n)

string = 'ABA'
n      = 2
method1(string, n)
method2(string, n)

string = 'ABA'
n      = 1
method1(string, n)
method2(string, n)

string = 'ABA'
n      = 5
method1(string, n)
method2(string, n)



runs = 100000
Benchmark.bm(7) do |x|
  string = '1234A1234B1234C'
  n      = 5
  x.report("method 1")   { runs.times {method1(string, n)}}
  x.report("method 2")   { runs.times {method2(string, n)}}
end ; ""


runs = 100000
Benchmark.bm(7) do |x|
  string = '1234A1234B1234C'.force_encoding("ascii")
  n      = 5
  x.report("method 1")   { runs.times {method1(string, n)}}
  x.report("method 2")   { runs.times {method2(string, n)}}
end ; ""




runs = 100000
Benchmark.bm(7) do |x|
  string = '1234A1234B1234C'*50
  n      = 5
  x.report("method 1")   { runs.times {method1(string, n)}}
  x.report("method 2")   { runs.times {method2(string, n)}}
end ; ""
