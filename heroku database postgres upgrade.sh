heroku pg:info -a bibliocloud-test

heroku addons:create heroku-postgresql:standard-0 --follow DATABASE_URL --app bibliocloud-test

heroku pg:wait -a bibliocloud-test

heroku pg:info -a bibliocloud-test

heroku maintenance:on --app bibliocloud-test
heroku scale worker=0 web=0 --app bibliocloud-test

heroku pg:upgrade HEROKU_POSTGRESQL_GRAY --app bibliocloud-test

heroku pg:promote HEROKU_POSTGRESQL_GRAY --app bibliocloud-test

heroku maintenance:off --app bibliocloud-test
heroku scale web=1 --app bibliocloud-test
