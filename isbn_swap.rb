old_hb = Book.find(75149)
new_hb = Book.find(78306)
old_pb = Book.find(75150)
new_pb = Book.find(78305)

old_hb.isbn
new_hb.isbn
old_pb.isbn
new_pb.isbn

new_hb.isbn, old_hb.isbn = old_hb.isbn, "placeholder"
new_pb.isbn, old_pb.isbn = old_pb.isbn, "placeholder"

old_hb.isbn
new_hb.isbn
old_pb.isbn
new_pb.isbn

old_hb.save!
old_pb.save!
new_hb.save!
new_pb.save!

old_hb.destroy
old_pb.destroy

Book.find(78307).destroy
Book.find(78308).destroy
