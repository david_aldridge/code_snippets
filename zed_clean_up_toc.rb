


Client.is_zed.marketingtexts.
  where(:main_type => "05").
  each_with_object(Hash.new(0)){| mt, h | h[mt.pull_quote.split("\n").index { |x| x[0].to_i == 0 }.nil?] += 1}

Client::ZED.marketingtexts.
  where(:main_type => "05").
  each_with_object(Hash.new(0)){| mt, h | h[mt.pull_quote_can_be_converted_to_ul?] += 1}


  def pull_quote_can_be_converted_to_ul?
    pull_quote.split("\n").index { |x| x[0].to_i == 0 }.nil?
  end


Rails.logger.level=1
PaperTrail.enabled = false

Client.is_zed.marketingtexts.
  where(:main_type => "05").
  reject{| mt | mt.pull_quote_has_html?}.
  each{|mt| mt.update_attributes(:marketing_text => mt.pull_quote_split_and_cleaned_as_ul)} ; "don't bug me with to_s"

Client.is_zed.marketingtexts.includes(:work => :books).where(:main_type => "05").
  sort_by{|mt| mt.work.books.map{|b| b.pub_date}.compact.max || Date.today + 10.years}.reverse.
  each{|mt| puts "<a href='https://v4.bibliocloud.com/marketingtexts/#{mt.id}/edit'>#{mt.work.title}</a>"; puts "<br>"; puts mt.marketing_text ; puts "<br>";} ; "don't bug me with to_s"

ActiveRecord::Base.transaction do
  Client.is_zed.marketingtexts.where(:main_type => "05").each do |toc|
    toc.marketing_text = toc.marketing_text.gsub(/[‘’]/, "'").gsub(/[“”]/, '"')
    toc.save! if toc.changed?
  end
  #raise ActiveRecord::Rollback
end

ActiveRecord::Base.transaction do
  Client.is_zed.
         marketingtexts.
         where(:main_type => "05").
         where("marketing_text != pull_quote and marketing_text is not null").
         each{|toc| toc.update_attributes(:pull_quote => toc.marketing_text)}
end





Client.is_zed.marketingtexts.includes(:work => :books).where(:main_type => ["08", "09", "10", "11"]).
  sort_by{|mt| mt.work.books.map{|b| b.pub_date}.compact.max || Date.today + 10.years}.reverse.
  each do |mt|
    puts "<a href='https://v4.bibliocloud.com/marketingtexts/#{mt.id}/edit'>#{mt.work.title}</a>"
    if mt.pull_quote_is_html
      puts "<br>#{mt.pull_quote}<br>"
    else
      puts "<pre>#{mt.pull_quote}</pre>"
    end
  end ; "don't bug me with to_s"

TEXTS = {"pull_quote" => "External",
         "marketing_text" => "Internal",
         "alternate_pull_quote" => "Alternate external",
         "alternate_marketing_text" => "Alternate internal"}

Client.
  is_zed.
  works.
  where(Marketingtext.where(:main_type => %w(01 02 03 04 20)).where("marketingtexts.work_id = works.id").exists).
  includes(:books, :marketingtexts).
  sort_by{|work| work.books.map{|b| b.pub_date}.compact.max || Date.today + 10.years}.
  reverse.each do |work|
  puts "<h1>#{work.title}</h1>"
  work.marketingtexts.where(:main_type => %w(01 02 03 04 20)).sort_by{|mt| mt.main_type}.each do |mt|
    puts "<h2>#{mt.class::MAIN_TYPES[mt.main_type][:name]}</h2>"
    TEXTS.keys.each do |text_name|
      text = mt.send(text_name.to_sym)
      next if text.blank?
      puts "<a href='https://v4.bibliocloud.com/marketingtexts/#{mt.id}/edit'>#{TEXTS[text_name]}</a>"
      if !!(/<.+>/.match text)
        puts "<br>#{text}<br>"
      else
        puts "<pre>#{text}</pre>"
      end
    end
  end
  work.contacts.uniq.each do |contact|
    next if contact.biographical_note.blank?
    puts "<h2>#{contact.to_s}: individual biographical note</h2>"
    puts "<a href='https://v4.bibliocloud.com/contacts/#{contact.id}/edit'>edit</a>"
    if !!(/<.+>/.match contact.biographical_note)
      puts "<br>#{contact.biographical_note}<br>"
    else
      puts "<pre>#{contact.biographical_note}</pre>"
    end
  end
end ; "don't bug me with to_s"
