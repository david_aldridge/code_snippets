files = {
  "application/x-mobipocket-ebook" => 'Mobi',
  "application/epub+zip" => 'ePub',
  "application/pdf" => 'PDF',
  "image/vnd.adobe.photoshop" => 'Photoshop file',
  "image/png" => 'Png Image',
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => 'OpenOffice document',
  "image/tiff" => 'TIFF image',
  "image/jpeg" => 'JPEG image'
  }
