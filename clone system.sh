heroku plugins:install heroku-fork
heroku fork --region eu --from bibliocloud-iop --to bibliocloud-iop-test
heroku scale web=0 worker=0 -a bibliocloud-iop-test
heroku pg:info -a bibliocloud-iop
heroku pg:info -a bibliocloud-iop-test
# destroy follower
heroku addons:destroy HEROKU_POSTGRESQL_BRONZE_URL -a bibliocloud-iop-test
heroku pg:backups:restore  `heroku pg:backups:url --app bibliocloud-iop` DATABASE_URL --app bibliocloud-iop-test
heroku config:set DOMAIN_NAME=bibliocloud-iop-test.herokuapp.com AWS_ACCESS_KEY_ID=AKIA* AWS_SECRET_ACCESS_KEY=* AWS_BUCKET=bibliocloudimages-iop-test  AWS_SECURE_BUCKET=bibliocloudimages-iop-test -a bibliocloud-iop-test
heroku config:unset ZEN_TOKEN NEW_RELIC_ID NEW_RELIC_LICENSE_KEY GOOGLE_DRIVE_CLIENT EXAVAULT_API_APIKEY -a bibliocloud-iop-test
heroku scale web=1:Standard-2X -a bibliocloud-iop-test



# Clone database only
heroku pg:config -a bibliocloud-test
heroku addons:create heroku-postgresql:standard-0 -a bibliocloud-test
heroku pg:wait -a bibliocloud-test
heroku pg:backups:restore  `heroku pg:backups:url --app bibliocloud-app` HEROKU_POSTGRESQL_ROSE --app bibliocloud-test

heroku addons:destroy HEROKU_POSTGRESQL_BRONZE_URL -a bibliocloud-test


# Fast copy via follower

## get the name of the addon for the source database
heroku pg:info -a bibliocloud-app
## create a follower on the target system
heroku addons:create heroku-postgresql:standard-0 --follow postgresql-clean-67782 -a bibliocloud-test
## get the colour of the target database
heroku pg:info -a bibliocloud-test
## unfollow the source database
heroku pg:unfollow HEROKU_POSTGRESQL_GRAY_URL -a bibliocloud-test
## promote the target database
heroku pg:promote HEROKU_POSTGRESQL_GRAY_URL -a bibliocloud-test
## get colour of old database
heroku pg:info -a bibliocloud-test
## destroy old database
heroku addons:destroy HEROKU_POSTGRESQL_MAROON_URL -a bibliocloud-test

# Copy app database to test
heroku scale web=0 worker=0  -a bibliocloud-test
heroku maintenance:on -a bibliocloud-test
heroku addons:create heroku-postgresql:standard-0 -a bibliocloud-test
heroku pg:wait -a bibliocloud-test
heroku pg:info -a bibliocloud-test
heroku pg:backups:restore  `heroku pg:backups:url --app bibliocloud-app` HEROKU_POSTGRESQL_MAROON_URL --app bibliocloud-test
heroku pg:info -a bibliocloud-test
heroku pg:promote HEROKU_POSTGRESQL_MAROON_URL -a bibliocloud-test

heroku scale web=1:Standard-2x  -a bibliocloud-test
heroku maintenance:off -a bibliocloud-test

heroku pg:info -a bibliocloud-test
heroku addons:destroy HEROKU_POSTGRESQL_BRONZE_URL -a bibliocloud-test

#Fast
## Create new test database
heroku addons:create heroku-postgresql:standard-0 -a bibliocloud-test
heroku pg:wait -a bibliocloud-test

## Get colours of databases
heroku pg:info -a bibliocloud-test
heroku pg:info -a bibliocloud-app

## Perform fast copy
heroku pg:copy bibliocloud-app::HEROKU_POSTGRESQL_NAVY_URL HEROKU_POSTGRESQL_PUCE_URL --app bibliocloud-test



# Copy IOP database to test
heroku scale web=0 worker=0  -a bibliocloud-test
heroku maintenance:on -app-test

heroku addons:create heroku-postgresql:standard-0 -app-test
heroku pg:wait -a bibliocloud-iop-test
heroku pg:backups:restore  `heroku pg:backups:url --app bibliocloud-iop` HEROKU_POSTGRESQL_AMBER_URL --app bibliocloud-iop-test
heroku pg:info -a bibliocloud-iop-test
heroku pg:promote HEROKU_POSTGRESQL_AMBER_URL -a bibliocloud-iop-test

heroku scale web=1:Standard-2x  -a bibliocloud-iop-test
heroku maintenance:off -a bibliocloud-iop-test

heroku pg:info -a bibliocloud-iop-test
heroku addons:destroy HEROKU_POSTGRESQL_BRONZE_URL -a bibliocloud-iop-test
