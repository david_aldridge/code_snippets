Client.is_british_library.works.joins(:dewey_subjects).includes(:dewey_subjects, :contract, :quickrights, :client).uniq.each do |work|
  edition, code = work.dewey_subjects.
    map{|s| [s.subject_scheme_version.to_i, s.subject_code] }.
    sort_by{|x| x.last.size}.
    last
  edition = nil if edition.zero?
  # puts "Setting #{edition} and #{code}"
  work.update_attributes(
    dewey_decimal_edition: edition,
    dewey_decimal_code: code
  )
end
