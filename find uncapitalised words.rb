common_words = ["der", "th", "und", "from", "on", "a", "for", "to", "v", "a", "on", "the", "of", "and", "in"].each_with_object({}){|x,h| h[x] = nil}

Client.is_sas.works.pluck(:title).select do |title|
  words = title.split(" ")
  test = words.
    map{|x| x.gsub(/[^a-zA-Z]/,"")}.
    reject(&:blank?).
    reject{|x| common_words.include?(x.downcase)}
  puts test.join(" ")
  test.any?{|w| w != w.titlecase}
end




Client.is_sas.works.pluck(:title).flat_map do |title|
  title.split(" ").map{|x| x.gsub(/[^a-zA-Z]/,"")}.reject(&:blank?)
end.group_by(&:itself).transform_values(&:size).select{|x,y| y > 10}.sort_by(&:last).map(&:first)
