LOOKUP = {0.99 => 1.25,
1.00 => 1.25,
1.98 => 2.25,
1.99 => 2.25,
2.99 => 3.25,
3.99 => 4.50,
4.99 => 5.50,
5.99 => 6.50,
6.99 => 7.75,
7.00 => 7.75,
7.99 => 8.00,
8.99 => 9.00,
9.00 => 9.00,
9.99 => 10.00,
10.99 => 11.00,
11.99 => 12.00,
12.99 => 12.50,
14.99 => 14.50,
15.59 => 15.00,
15.99 => 15.00,
19.99 => 18.75,
20.00 => 18.75,
24.99 => 23.50
}

client = Client.is_quiller

client.books.is_ebook.
  joins(:prices).merge(Price.gbp).
  uniq.each do |ebook|
  gbp_price = ebook.current_gbp_inctax_consumer_price
  next unless gbp_price
  next if ebook.current_eur_exctax_consumer_price
  ebook.current_eur_exctax_consumer_price = gbp_price
  ebook.save
end
