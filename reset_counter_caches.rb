client = Client.is_bds
client.proprietary_format_descriptions.each{|p| p.class.reset_counters(p.id, :books)}
client.proprietary_edition_descriptions.each{|p| p.class.reset_counters(p.id, :books)}
