explain SELECT
  books.*
FROM "books"
INNER JOIN (
    SELECT
      "books"."id" AS pg_search_id,
      /*(
        ts_rank(
          (
            setweight(
              to_tsvector(
                'simple',
                unaccent(coalesce("books"."isbn" :: text, ''))
              ),
              'A'
            ) || setweight(
              to_tsvector(
                'simple',
                unaccent(coalesce("books"."full_title" :: text, ''))
              ),
              'B'
            ) || setweight(
              to_tsvector(
                'simple',
                unaccent(coalesce("books"."subtitle" :: text, ''))
              ),
              'C'
            )
          ),
          (
            to_tsquery(
              'simple',
              ''' ' || unaccent('phosphorus') || ' ''' || ':*'
            )
          ),
          0
        )
      ) + 0.5 * */ (
        similarity(
          unaccent('phosphorus'),
          (
            unaccent(
              coalesce("books"."full_title" :: text, '') || ' ' || coalesce("books"."subtitle" :: text, '')
            )
          )
        )
      ) AS rank
    FROM "books"
    WHERE
      /* (
        (
          setweight(
            to_tsvector(
              'simple',
              unaccent(coalesce("books"."isbn" :: text, ''))
            ),
            'A'
          ) || setweight(
            to_tsvector(
              'simple',
              unaccent(coalesce("books"."full_title" :: text, ''))
            ),
            'B'
          ) || setweight(
            to_tsvector(
              'simple',
              unaccent(coalesce("books"."subtitle" :: text, ''))
            ),
            'C'
          )
        ) @@ (
          to_tsquery(
            'simple',
            ''' ' || unaccent('phosphorus') || ' ''' || ':*'
          )
        )
      )
      OR */ (
        similarity(
          unaccent('phosphorus'),
          (

              coalesce("books"."full_title" :: text, '') || ' ' || coalesce("books"."subtitle" :: text, '')

          )
        ) >= 0.5
      )
  ) AS pg_search_6e317bcd6839e887739541 ON "books"."id" = pg_search_6e317bcd6839e887739541.pg_search_id
ORDER BY
  pg_search_6e317bcd6839e887739541.rank DESC,
  books.pub_date desc
LIMIT
  50 OFFSET 0;


CREATE INDEX index_books_on_title_search ON books USING GIST (
              ((COALESCE(full_title, ''::text) || ' '::text) || COALESCE((subtitle)::text, ''::text))
             gist_trgm_ops);

CREATE INDEX index_works_on_title_search ON works USING GIST (
              ((COALESCE(title, ''::text) || ' '::text) || COALESCE((subtitle)::text, ''::text))
             gist_trgm_ops);


CREATE INDEX index_contacts_on_title_search_person ON contacts USING GIST (person_name gist_trgm_ops);
CREATE INDEX index_contacts_on_title_search_corporate ON contacts USING GIST (corporate_name gist_trgm_ops);



explain select count(*) from books where 'phosphorus' % ((COALESCE(full_title, ''::text) || ' '::text) || COALESCE((subtitle)::text, ''::text))