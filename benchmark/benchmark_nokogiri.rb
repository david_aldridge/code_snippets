Preparation
===========


filepath = "/Users/david/Documents/Bibliocloud/Code/benchmark/nbni_all1806140600_1.xml"

require 'nokogiri'

doc = Nokogiri::XML(File.open(filepath)).remove_namespaces! ; 0

require 'memory_profiler'

MemoryProfiler.report do
  doc.xpath("//SupplyDetail").size
end.pretty_print

MemoryProfiler.report do
  doc.xpath("/ONIXMessage/Product/SupplyDetail").size
end.pretty_print

require 'benchmark'

n = 100
Benchmark.bm(12) do |x|
  x.report("method 0") { n.times {doc.xpath("//SupplyDetail").size} }
  x.report("method 1") { n.times {doc.xpath("/ONIXMessage/Product/SupplyDetail").size} }
end ; ""



require 'benchmark/ips'

Benchmark.ips do |x|
  doc.xpath("//SupplyDetail").size
end

require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("normal") { noop("foo") }
  x.report("frozen") { noop("foo".freeze)  }
end

Benchmark.ips do |x|
  x.report("normal") { 100.times {1000.times { |x| "x#{x}"}}}
  x.report("normal") { 100.times {1000.times { |x| "x#{x}".freeze}}}
  x.report("normal") { 1000.times {100.times { |x| "x#{x}"}}}
  x.report("normal") { 1000.times {100.times { |x| "x#{x}".freeze}}}
  x.report("normal") { 10000.times {10.times { |x| "x#{x}"}}}
  x.report("normal") { 10000.times {10.times { |x| "x#{x}".freeze}}}
end


require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("normal") { Book.find(3384).to_s }
end






require 'memory_profiler'

ary = ["a".freeze, "b".freeze, "c".freeze, "d".freeze, "e".freeze, "f".freeze, "g".freeze, "h".freeze, "i".freeze, "j".freeze]

MemoryProfiler.report do
  10000.times {ary.each { |x| y = x}}
end.pretty_print
