reload!
Rails.logger.level=1

work = Work.first
book = work.books.first

def method1(book)
  book.marketingtext_ids
end

def method2(book)
  MiniSql::Connection.new(ActiveRecord::Base.connection.raw_connection).
    query_single(
      "select marketingtext_id from bookmarketingtexts where book_id = ?",
      book.id
    )
end

method1(book)
method2(book)


require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("1") { method1(book) }
  x.report("2") { method2(book) }
end

require 'memory_profiler'
MemoryProfiler.report do
  method1(book)
end.pretty_print

MemoryProfiler.report do
  method2(book)
end.pretty_print
