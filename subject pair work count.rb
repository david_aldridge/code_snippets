Client.is_zed.works.includes(:subjectcodes).
  select{|w| (w.subjectcodes.map(&:to_s) & ["Africa", "Politics"]).size == 2}.map(&:title).sort


work_subjects =
  Client.is_zed.works.includes(:subjectcodes).each_with_object({}) do |w, h|
    h[w.id] = w.subjectcodes.map(&:to_s).sort
  end

work_subjects_ary = work_subjects.to_a

all_subjects = work_subjects.values.flatten.uniq

subject_pairs = all_subjects.combination(2).map(&:sort).uniq

subject_pair_count = Hash[*subject_pairs.each_with_object({}) do |pair, h|
  h[pair] = work_subjects_ary.count{|_id, subjects| (subjects & pair).size == 2}
end.reject{|k,v| v.zero?}.sort_by(&:last).flatten(1)]
