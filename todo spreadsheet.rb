names = Client.is_scribd.todos.not_adhoc.pluck(:name)

select
  t.object_id,
  t.object_type,
  t.name,
  d.due_date,
  percent_rank(d.due_date) over (partition by object_id, object_type order by d.due_date)
from
  todos t join todo_dates d on t.id = d.todo_id
where
  t.client_id = 479 and
  d.due_date is not null
;
