class Orientation
  def initialize(value)
    raise "Orientation must be :up or :down, got #{value}" unless %i[up down].include? value
    @value = value
  end

  def self.up
    self.new(:up)
  end

  def self.down
    self.new(:down)
  end

  def up?
    value == :up
  end

  def down?
    value == :down
  end

  def swap!
    @value = if up?
              :down
            else
              :up
            end
  end

  attr_reader :value
end

o = Orientation.up
o.swap!
o.swap!

o = Orientation.new(:up)
o.swap!
o.swap!

o = Orientation.new(:up)
o.swap!
o.swap!

o = Orientation.new(:x)

class Point
  def initialize(x,y)
    @x = x
    @y = y
  end

  def go_down(dy)
    @y += dy
    self
  end

  def go_right(dx)
    @x += dx
    self
  end

  def go_up(dy)
    @y -= dy
    self
  end

  def go_left(dx)
    @x -= dx
    self
  end

  def to_s
    "#{x},#{y}"
  end

  def distance_from(point:)
    x_distance = self.x - point.x
    y_distance = self.y - point.y
    sqrt(x*x + y*y)
  end

  attr_reader :x, :y
end

Point.new(0,0)
Point.new(3,5)
Point.new(5,3)

class Polygon
  def initialize(*points)
    @points = points
  end

  attr_reader :points, :color

  def color!(hex)
    @color = hex
    self
  end

  def to_s
    points.map(&:to_s).join(" ")
  end

  def points_string
    points.map(&:to_s).join(" ")
  end

  def style_string
    "fill:#{color}" if color
  end

  def to_svg
    {
      points: points_string,
      style:  style_string
    }
  end

  def rightmost
    points.map(&:x).max
  end

  def leftmost
    points.map(&:x).min
  end

  def width
    rightmost - leftmost
  end
end


a = Point.new(0,0)
b = Point.new(3,5)
c = Point.new(5,3)

Polygon.new(a,b,c)
Polygon.new(a,b,c).to_s
Polygon.new(a,b,c).rightmost
Polygon.new(a,b,c).leftmost
Polygon.new(a,b,c).width



class EquilateralTriangle < Polygon
  WIDTH_TO_HEIGHT = 1.1547
  def initialize(*points)
    raise "Must be three points, got #{points.size}" unless points.size == 3
    super
  end

  def self.from_top_left(origin:, height:, orientation:)
    if orientation.up?
      from_top_left_up(origin: origin, height: height)
    else
      from_top_left_down(origin: origin, height: height)
    end
  end

  private_class_method def self.from_top_left_up(origin:, height:)
    a = origin.dup.go_down(height)
    b = origin.dup.go_right(height * WIDTH_TO_HEIGHT/2.0)
    c = origin.dup.go_right(height * WIDTH_TO_HEIGHT).go_down(height)
    self.new(a,b,c)
  end

  private_class_method def self.from_top_left_down(origin:, height:)
    a = origin.dup
    b = origin.dup.go_right(height * WIDTH_TO_HEIGHT/2.0).go_down(height)
    c = origin.dup.go_right(height * WIDTH_TO_HEIGHT)
    self.new(a,b,c)
  end
end

a = Point.new(0,0)
b = Point.new(3,5)
c = Point.new(5,3)

EquilateralTriangle.new(a,b,c).width

EquilateralTriangle.from_top_left(origin: Point.new(0,0), height: 3, orientation: Orientation.up)
EquilateralTriangle.from_top_left(origin: Point.new(0,0), height: 3, orientation: Orientation.up).width


class RowOfEquilateralTriangles
  def initialize(top_left:, height:, width:, first_orientation:)
    @top_left          = top_left
    @height            = height
    @width             = width
    @first_orientation = first_orientation
    @triangles         = []
    @righthand_side    = top_left.x + width
  end

  def last_triangle_rightmost
    last_triangle&.rightmost || top_left.x
  end

  def polygons
    triangles
  end

  def last_triangle
    triangles.last
  end

  def build!
    rightmost     = top_left.x
    orientation   = first_orientation.dup
    next_top_left = top_left.dup
    until last_triangle_rightmost >= righthand_side do
      triangles << EquilateralTriangle.from_top_left(origin: next_top_left, height: height, orientation: orientation)
      orientation.swap!
      next_top_left.go_right(last_triangle.width / 2.0)
    end
    self
  end

  private

  attr_reader :top_left, :height, :width, :first_orientation, :righthand_side, :triangles
end

row = RowOfEquilateralTriangles.new(top_left: Point.new(0.0, 0.0), height: 3.0, width: 300.0, first_orientation: Orientation.up)
row.build!


class AreaOfEquilateralTriangles
  def initialize(top_left:, height:, width:, first_orientation:, number_of_rows:)
    @top_left          = top_left
    @height            = height
    @width             = width
    @first_orientation = first_orientation
    @number_of_rows    = number_of_rows
    @rows              = []
    @height_of_row     = height / number_of_rows
  end

  def polygons
    rows.flat_map(&:polygons)
  end

  def build!
    orientation   = first_orientation.dup
    next_top_left = top_left.dup
    number_of_rows.times.each do
      rows << RowOfEquilateralTriangles.new(
        top_left:          next_top_left,
        height:            height_of_row,
        width:             width,
        first_orientation: orientation).build!
      orientation.swap!
      next_top_left.go_down(height_of_row)
    end
    self
  end

  def random_colors!(colors)
    polygons.each do |polygon|
      polygon.color!(colors.sample)
    end
    self
  end

  attr_reader :top_left, :height, :width, :first_orientation, :rows, :number_of_rows

  private

  attr_reader :height_of_row
end


require "nokogiri"

class DrawSVG
  def initialize(polygons)
    @polygons = polygons
  end

  def content
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.svg(xmlns: "http://www.w3.org/2000/svg") do
        xml.desc "Triangles"
        # xml.polygon(points: "0,20 10,0 20,20")
        polygons.each do |polygon|
          xml.polygon polygon.to_svg
        end
      end
    end
    builder.to_xml
  end

  def call
    filename = 'output.svg'
    doc = Nokogiri::XML(content)
    File.write(filename, doc.to_xml)
  end

  attr_reader :polygons
end

PALETTE = %w[
#3D0079
#4F009C
#6800CD
#8407FF
#9325FF
#AD57FF
#C78FFF
#E4CFFF
#F6EBFF
#24211E
#2E2A27
#38332F
#4A4541
#C0BBB6
#D0C9BF
#E1DCD4
#EFEBE7
#F5F2EF
#FAF7F5
#FCFAF8
#790007
#9C000A
#CD000C
#FF0716
#FF2533
#FF5760
#FF8F96
#0F6A49
#13885D
#18B47B
#28DD9B
#47DCA6
#73E3BB
#ABE3CE
#064873
#075D93
#087BC3
#169BEF
#38A5EB
#77B6DD
#A5CFE8
#C29100
#DFA700
#FFC500
#FFCE36
#FFD34E
#FFDF77
#FFE8A4
]

PURPLES = %w[
#3D0079
#4F009C
#6800CD
#8407FF
#9325FF
#AD57FF
#C78FFF
]


DrawSVG.new(
  AreaOfEquilateralTriangles.new(
    top_left:          Point.new(0,0),
    height:            59.0 * 40,
    width:             88.0 * 40,
    first_orientation: Orientation.up,
    number_of_rows:    47
  ).build!.random_colors!(PURPLES).polygons
).call


open -a "Google Chrome" output.svg
Message Input

Message @Emma Barnes
