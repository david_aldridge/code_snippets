file = "/Users/david/Documents/Bibliocloud/Clients/McGill Queens/From McGill/McGill_Queens_ref.xml"
doc = Nokogiri::XML(open(file)) ; 0
doc.xpath("//Product/MediaFile[MediaFileTypeCode='04']").map do |media_file|
  "curl #{media_file.at_xpath("MediaFileLink").content} -L --output #{media_file.at_xpath("../ProductIdentifier[ProductIDType='15']/IDValue").content}.jpg"
end


product_json =
    JSON.parse(
      File.read(
        "/Users/david/Documents/Bibliocloud/Clients/Taylor & Francis/From T&F/all.json"
      )
    )


product_json.map do |json|
  "curl #{json.dig('image', 'cover')} #{json.dig('isbn')}"
end