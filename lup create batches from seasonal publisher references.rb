client = Client.is_liverpool
client.books.where.not(publishers_reference: nil).group(:publishers_reference).count.keys.each do |batch_name|
  collection = client.collections.create(
    name: "Season #{batch_name}"
  )
  collection.books = collection.client.books.where(publishers_reference: batch_name)
end
