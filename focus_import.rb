require 'open-uri'
require "aws/s3"
require 'cgi'
require 'net/http'



FOCUS_DATABASE = {adapter: "postgresql",
    encoding: "unicode",
    pool: 5,
    host: "ec2-54-246-104-52.eu-west-1.compute.amazonaws.com",
    database: "d3182mhncc9d1",
    username: "wylfnzmuxcvjlq",
    password: "7fDIjm_5uZWWcTr-inunnkQD_E",
    port: "5432"}

class FocusPrice < ActiveRecord::Base
  establish_connection FOCUS_DATABASE

  def self.table_name
   "ONIX_PR24_Price"
  end
end


class FocusBlurb < ActiveRecord::Base
  establish_connection FOCUS_DATABASE

  def self.table_name
   "Stock_Biblio"
  end
end

class FocusContrib < ActiveRecord::Base
  establish_connection FOCUS_DATABASE

  def self.table_name
   "STK_Contributors"
  end
end


class FocusImport


    PROD_STATUS=
    { "Author Copies Sent"  => {:signed => true },
      "Contract Offered"    => {:signed => false},
      "Contract Received"   => {:signed => true },
      "Contract Sent"       => {:signed => false},
      "Dormant"             => {:signed => true },
      "In Production"       => {:signed => true },
      "Is Published"        => {:signed => true },
      "Is Rejected"         => {:signed => false},
      "Manuscript Deliv'd"  => {:signed => true },
      "New Proposal"        => {:signed => false},
      "Null"                => {:signed => false},
      "Proposal Meeting"    => {:signed => false},
      "Under Development"   => {:signed => true }}

    STATUS=
    {"Cancelled"        => ["01","AB"],
      "In Stock"        => ["04","IP"],
      "NYP"             => ["02","NP"],
      "Old Edition"     => ["07","OR"],
      "Out of Print"    => ["07","OP"],
      "Out of Stock"    => ["06","RU"],
      "Out of print"    => ["07","OP"],
      "Print on Demand" => ["04","MD"],
      "Rights Reverted" => ["07","OP"],
      "eBook"           => ["04","IP"],
      "Unknown"         => ["09","CS"]}

  def import_stock_rec(webname, file)
    f             = open(file)
    @client       = Client.find_by_webname(webname)
    stock_recs    = CSV.read(f, :headers => :first_row, :encoding => 'windows-1251:utf-8')
    #loaded_codes  = @client.books.pluck(:internal_reference)
    loaded_codes  = []
    @data_to_load = stock_recs.reject{|r| r["BKTitle"].blank? ||r["CoverType"] == "14" || loaded_codes.include?(r["Code"])}
    #create_contributors(contributors_in_file)
    @imprint_id = @client.imprints.first.try(:id)
    @data_to_load.each do |product|
      work = get_work(product)
      pub_date_raw = product["Pub_Date"].presence
      pub_date = (Date.parse(pub_date_raw) rescue nil)
      if !pub_date_raw.nil? and pub_date.nil?
        puts "Could not parse date "+ pub_date_raw
      end
      if pub_date == Date.parse("01/01/1900")
        pub_date = (Date.parse(product["Pub_Date_Orig"].presence) rescue nil)
      end
      prefix, title = de_prefixer(product["BKTitle" ].strip.gsub("/n",""))


      pub_date = nil if pub_date and (pub_date == Date.parse("01/01/1900") or pub_date >= Date.parse("01/01/2099"))
      status = product["Status"].presence || "Unknown"

      book_attributes = {:work_id                       => work.id,
                         :isbn                          => ISBN.valid?(product["ISBN"]) ? product["ISBN" ] : "placeholder",
                         :internal_reference            => product["Code"],
                         :prefix                        => prefix,
                         :title                         => title ,
                         :subtitle                      => product["SubTitle"],
                         :use_edition_suppliers         => true,
                         :publishing_status             => STATUS[status][0],
                         :availability_code             => STATUS[status][1],
                         :client_id                     => @client.id,
                         :product_form                  => cover_type_to_product_form(product["CoverType"]),
                         :pub_date                      => pub_date,
                         :measurements_attributes       => measurements_attributes(product["Dimension"].presence, @client.id),
                         :extents_attributes            => extents_attributes(product["TotPages"].presence, @client.id)}
                         #:bookmarketingtexts_attributes => bookmarketingtexts_attributes(product["MarketPointUK"].presence,
                         #                                                                product["MarketPointUS"].presence,
                         #                                                                work.id),
                         #:prices_attributes             => prices_attributes(product["Sell_Price0"].presence.try(:to_d),
                         #                                                    product["Sell_Price1"].presence.try(:to_d))}
      Book.create!(book_attributes)
    end
  end


  def import_dealers(webname, file)
    f             = open(file)
    @client       = Client.find_by_webname(webname)
    dealers       = CSV.read(f, :headers => :first_row, :encoding => 'windows-1251:utf-8')
    @dealers.drop(3).each do |dealer|
      puts dealer["Title"], dealer["Title"].nil?
    end
  end





  def import_prices(webname)
    @client       = Client.find_by_webname(webname)

    FocusPrice.all.each do |p|
      b = @client.books.find_by_internal_reference(p.StockCode)
      unless b.nil?
        price = {:client_id                    => b.client_id,
                 :price_type                   => p.TypeCode,
                 :price_amount                 => p.PriceAmount.try(:to_d),
                 :tax_rate_code                => p.TaxCode_1,
                 :tax_rate_percent             => p.TaxRate_1.try(:to_d),
                 :currency_code                => p.CurrencyCode,
                 :taxable_amount               => p.TaxableAmt_1.try(:to_d),
                 :tax_value                    => p.TaxAmount_1.try(:to_d),
                 :onix_price_type_qualifier_id => p.Qualifier,
                 :type_description             => p.TypeDescr,
                 :unit_of_pricing              => p.PriceUnit,
                 :minimum_order_quantity       => p.MinOrderQty,
                 :price_effective_from         => p.PriceFrom,
                 :price_effective_to           => p.PriceUntil,
                 :status_code                  => p.PriceStatus,
                 :tax_rate_code_2              => p.TaxCode_2,
                 :tax_rate_percent_2           => p.TaxRate_2.try(:to_d),
                 :taxable_amount_2             => p.TaxableAmt_2.try(:to_d),
                 :tax_value_2                  => p.TaxAmount_2.try(:to_d)}

        b.prices.create!(price)
      end

    end
  end


  def import_blurbs(webname)
    @client       = Client.find_by_webname(webname)

    FocusBlurb.all.each do |p|
      b = @client.books.find_by_internal_reference(p.StockCode)
      unless b.nil?
        work              = b.work
        text_type         = p.TypeCode.presence || "35"
        marketing_text    = p.Blurb_FULL
        legacy_code       = text_type
        client_id         = @client.id
        work_id           = work.id
        exclude_from_onix = (text_type == "35")
        pull_quote        = marketing_text
        marketingtext = Marketingtext.where(:text_type         => text_type        ,
                                            :marketing_text    => marketing_text   ,
                                            :legacy_code       => legacy_code      ,
                                            :client_id         => client_id        ,
                                            :work_id           => work_id          ,
                                            :exclude_from_onix => exclude_from_onix,
                                            :pull_quote        => pull_quote       ).first_or_create
        Bookmarketingtext.where(:book_id => b.id, :marketingtext_id => marketingtext.id).create

      end

    end
  end

  def import_contribs(webname)
    @client       = Client.find_by_webname(webname)

    FocusContrib.all.each do |x|
      b = @client.books.find_by_internal_reference(x.StockCode)
      unless b.nil?
        work              = b.work
        role              = x.Role
        person_name       = x.Person.gsub("\n","").strip.presence || x.Corporate.gsub("\n","").strip.presence || "Unknown"

        c = Contact.where(:client_id   => @client.id,
                          :person_name => person_name).first_or_create!

        wc = Workcontact.where(:client_id      => @client.id,
                               :contact_id        => c.id,
                               :work_id           => work.id,
                               :work_contact_role => role).first_or_create!(
                               :sequence_number   => (Workcontact.where(:work_id => work.id).maximum(:sequence_number) || 0) + 1)

        bc = Bookcontact.where(:client_id      => @client.id,
                               :workcontact_id => wc.id,
                               :book_id        => b.id).first_or_create!

      end

    end
  end



  def import_stock_rec_prices(webname, file)
    f             = open(file)
    @client       = Client.find_by_webname(webname)
    stock_recs    = CSV.read(f, :headers => :first_row, :encoding => 'windows-1251:utf-8')
    @data_to_load = stock_recs.reject{|r| r["BKTitle"].blank? ||r["CoverType"] == "14"}
    @data_to_load.each do |r|
      b = @client.books.find_by_internal_reference(r["Code"])
      unless b.nil?
        gbp_inc_price = b.is_ebook? ? 1.03 * r["Sell_Price0"].to_d : r["Sell_Price0"].to_d
        gbp_exc_price = b.is_ebook? ? gbp_inc_price / 1.2.to_d : r["Sell_Price0"].to_d
        usd_inc_price = b.is_ebook? ? 1.03 * r["Sell_Price1"].to_d : r["Sell_Price1"].to_d
        gbp_inc_cons_gb = {:client_id                    => b.client_id,
                           :price_type                   => "02",
                           :price_amount                 => gbp_inc_price,
                           :currency_code                => "GBP",
                           :onix_price_type_qualifier_id => "05",
                           :status_code                  => "02",
                           :countries                    => ["GB"]}
        gbp_exc_cons_gb = {:client_id                    => b.client_id,
                           :price_type                   => "01",
                           :price_amount                 => gbp_exc_price,
                           :currency_code                => "GBP",
                           :onix_price_type_qualifier_id => "05",
                           :status_code                  => "02",
                           :countries                    => ["GB"]}
        gbp_inc_cons_wo = {:client_id                    => b.client_id,
                           :price_type                   => "02",
                           :price_amount                 => gbp_inc_price,
                           :currency_code                => "GBP",
                           :onix_price_type_qualifier_id => "05",
                           :status_code                  => "02",
                           :territories                  => ["WORLD"],
                           :countries_excluded           => ["GB"]}
        usd_inc_cons    = {:client_id                    => b.client_id,
                           :price_type                   => "02",
                           :price_amount                 => usd_inc_price,
                           :currency_code                => "GBP",
                           :onix_price_type_qualifier_id => "05",
                           :status_code                  => "02"}

        b.prices.create!(gbp_inc_cons_gb)
        b.prices.create!(gbp_exc_cons_gb)
        b.prices.create!(gbp_inc_cons_wo)
        b.prices.create!(usd_inc_cons   )
      end
    end
  end
  private





  def measurements_attributes(dimensions, client_id)
    return [] if dimensions.blank?
    height, width = dimensions.scan(/\d+/)
    [{measurement: height, measurement_type_id: 1, measurement_unit_id: 1, client_id: client_id},
     {measurement: width , measurement_type_id: 2, measurement_unit_id: 1, client_id: client_id}]
  end

  def extents_attributes(tot_pages, client_id)
    return [] if tot_pages.blank?
    pages_unit_id = ExtentUnit.find_by_value("Page count").id
    pages_type_id = ExtentType.find_by_value("Production page count").id
    [extent_value: tot_pages,  client_id: client_id, extent_type_id: pages_unit_id, extent_unit_id: pages_type_id]
  end



  def bookcontacts_attributes(work_id)
    return_ary = []
    Workcontact.where(:work_id => work_id).each do |wc, i|
      return_ary << {:workcontact_id   => wc.id}
    end
    return_ary
  end



  def bookmarketingtexts_attributes(description_1, description_2, work_id)
    return_ary = []
    [description_1, description_2].uniq.compact.each do |desc|
      return_ary < {  :marketingtext_id => Marketingtext.where(:marketing_text => desc,
                                                             :work_id        => work_id).first.id}
    end
    return_ary
  end


  def create_contributors(contributor_ary)
    loaded_contributors = @client.contacts.pluck(:person_name).compact.uniq
    (contributor_ary - loaded_contributors).each do |contributor|
      @client.contacts.create(:person_name => contributor, :contributor_type => "Person")
    end
  end

  def de_prefixer(str)
    return [nil,nil] if str.presence.nil?
    prefix,without_prefix  = /^(The |A |An |Le |La )([^ ].*$)/i.match(str).try(:captures)
    prefix.try(:strip!)
    without_prefix ||= str
    without_prefix.try(:strip!)
    [prefix,without_prefix]
  end

  def seriesname_id(title)
    return nil if title.presence.nil?
    title_prefix,title_without_prefix  = de_prefixer(title)
    seriesname = Seriesname.where(:collection_type      => "Series",
                                  :title_without_prefix => title_without_prefix,
                                  :client_id            => @client.id).
                                  first_or_create!(:title_prefix         => title_prefix)
    seriesname.id
  end

  def prices_attributes(gbp_price, usd_price)
    return_ary = []
    return_ary << {:currency_code                => "GBP",
                   :price_amount                 => gbp_price,
                   :onix_price_type_qualifier_id => "05",
                   :status_code                  => "00",
                   :price_type                   => "02"} if gbp_price
    return_ary << {:currency_code                => "USD",
                   :price_amount                 => usd_price,
                   :onix_price_type_qualifier_id => "05",
                   :status_code                  => "00",
                   :price_type                   => "02"} if usd_price
    return_ary
  end

  def workcontacts_attributes(authors)
    return [] if authors.presence.nil?
    role = authors.gsub!("Edited by","").nil? ? "A01" : "B01"
    author_names = authors.split(/( and )|(,)|( with )|( & )/).
                           map{|a| a.strip.presence}.
                           compact.
                           reject{|a| ["and" , "," , "with", "&"].include?(a)}
    return_ary = []
    author_names.each_with_index do |author,i|
      return_ary << {:contact_id        => Contact.
                                            where(:client_id        => @client.id,
                                                  :person_name      => author,
                                                  :contributor_type => "Person").
                                            first_or_create!.id,
                    :sequence_number   => i+1,
                    :work_contact_role => role}
    end
    return_ary
  end

  def clean_null(str)
    return nil if str.presence.nil?
    str == "Null" ? nil : str
  end

  def cover_type_to_product_form(str)
    case str
    when "0" then "BC"
    when "1" then "BB"
    when "41" then "DG"
    else nil
    end
  end

  #def measurements_attributes(str, client_id)
  #  return [] if str.nil?
  #  @height_mm_type_id ||= MeasurementType.height_mm.first.id
  #  @width_mm_type_id  ||= MeasurementType.width_mm.first.id
  #  @mm_unit_id        ||= MeasurementUnit.mm.first.id
#
  #  h , w = str.split(" x ").map{|x| x.to_i}
  #  height = {:measurement         => h.to_s,
  #            :measurement_type_id => @height_mm_type_id,
  #            :measurement_unit_id => @mm_unit_id,
  #            :client_id           => client_id}
  #  width  = {:measurement         => w.to_s,
  #            :measurement_type_id => @width_mm_type_id,
  #            :measurement_unit_id => @mm_unit_id,
  #            :client_id           => client_id}
  #  [height, width]
  #end

  def subjects_attributes(str)
    return [] if str.nil?
    return_ary = []
    str.split(";").each do |subject|
      Subjectcode.where(:client_id => @client.id,
                        :code      => subject).
                 first_or_create(:value     => subject)

      return_ary << {:subject_scheme_identifier => "23"   ,
                 :subject_code              => subject,
                 :subject_description       => subject,
                 :client_id                 => @client.id}
    end
    return_ary
  end

  def marketingtexts_attributes(description_1, description_2)
    return_ary = []
    [description_1, description_2].compact.each do |desc|
      return_ary < {:marketing_text => desc,
                    :legacy_code    => "01",
                    :client_id      => @client.id,
                    :text_type      => "03"}
    end
    return_ary
  end

  def get_work(product)
    main_bic_code = product["Subj_BIC_Code"].presence
    authors       = product["Author"       ].presence
    title         = product["BKTitle"      ].presence
    main_subject  = product["MainSubject"  ].presence
    description_1 = product["MarketPointUK"].presence
    description_2 = product["MarketPointUS"].presence
    note          = product["Memo"         ].presence
    prod_status   = product["Prod_Status"  ].presence
    series_title  = product["Series_Title" ].presence
    status        = product["Status"       ].presence
    subjects      = product["Subject"      ].presence
    subtitle      = product["SubTitle"     ].presence
    pub_date      = (Date.parse(product["Pub_Date"].presence) rescue nil)

    work = @client.works.find_by_title(title)
    if work.nil?
      contract_attributes = {:contract_name   => title,
                             :terminated_date => status == "Rights Reverted" ? Date.parse("2099-01-01") : nil,
                             :signed_date     => pub_date,
                             :client_id       => @client.id}
      if note
        contract_attributes.merge(:notes_attributes => {private: false, note_text: note, subject_line: "Imported from Focus"})
      end
      puts "Contract: ", contract_attributes
      contract = @client.contracts.create!(contract_attributes)

      work_attributes    = {:contract_id               => contract.id,
                            :title                     => title      ,
                            :seriesname_id             => seriesname_id(series_title),
                            :main_subject              => main_subject,
                            :main_bic_code             => main_bic_code,
                            :imprint_id                => @imprint_id,
                            :client_id                 => @client.id,
                            #:workcontacts_attributes   => workcontacts_attributes(authors),
                            :subjects_attributes       => subjects_attributes(subjects)#,
                            #:marketingtexts_attributes => marketingtexts_attributes(description_1, description_2)
                          }

      #puts "Work:", work_attributes

      work = Work.create!(work_attributes)
    end
    work
  end


end

__END__

c = Client.where(webname: "zedbooks").first
c.webname = "ex-zedbooks"
c.save

c = Client.create!(client_name: "Zed Books", webname: "zedbooks")




c = Client.find(352)

PaperTrail.enabled = false
 c.age_range_codes.destroy_all
 c.binding_types.destroy_all
 c.client_marketing_setups.destroy_all
 c.collections.destroy_all
 c.brief_setups.destroy_all
 c.payments.destroy_all
 Workcontact.where(:client_id => c.id).destroy_all
 Sale.where(:client_id => c.id).destroy_all
 RoyaltyLiability.where(:client_id => c.id).destroy_all
 c.contracts.destroy_all
 CatalogueTemplate.where(:client_id => c.id).destroy_all
 c.contacts.destroy_all
 c.foreignrights.destroy_all
 c.imprints.destroy_all
 c.measurements.destroy_all
 c.publishernames.destroy_all
 c.rightlists.destroy_all
 c.rightslist_bundles.destroy_all
 c.seriesnames.destroy_all
 c.subjectcodes.destroy_all
 c.finance_codes.destroy_all
 c.cost_groups.destroy_all




 c.users.delete_all
 c.destroy





x = CSV.read(open("http://s3-eu-west-1.amazonaws.com/bibliocloudimages/zed_books_focus.csv"), :headers => :first_row, :encoding => 'windows-1251:utf-8')

focus_code_ary = x.reject{|r| r["CoverType"] == "14"}.map{|r| r["Code"]}
focus_isbn_ary = x.reject{|r| !ISBN.valid?(r["ISBN"])}.map{|r| r["ISBN"]}
c              = Client.find_by_webname("zedbooks")
loaded_isbn_ary = c.books.pluck(:isbn).reject{|i| !ISBN.valid?(i)}
loaded_code_ary = c.books.pluck(:internal_reference)

focus_isbn_ary.group_by{|a| a}.select{|k,v| v.size > 1}.map{|k,v| k}

(focus_isbn_ary - loaded_isbn_ary).count

focus_code_ary.count
loaded_code_ary.count

focus_code_ary.uniq.count
loaded_code_ary.uniq.count

(focus_code_ary - loaded_code_ary).count
(loaded_code_ary - focus_code_ary).count

focus_main_subject_ary = x.reject{|r| r["CoverType"] == "14"}.map{|r| r["MainSubject"]}.uniq
focus_series_ary = x.reject{|r| r["CoverType"] == "14"}.map{|r| r["Series_Title"]}.uniq.compact
loaded_series_ary = c.seriesnames.pluck(:title_without_prefix)
(focus_series_ary - loaded_series_ary)
(loaded_series_ary - focus_series_ary)


x.map{|r| r["Author"]}.reject{|x| x.nil?}.map{|x| x.gsub("Edited by","").split(/( and )|(,)|( with )|( & )/)}.flatten.
       map{|a| a.strip}.
       reject{|a| ["and" , "," , "with", "&"].include?(a)}.uniq


  def contributor_role(str)
    str.gsub!("Edited by","").nil? ? "A01" : "B01"
  end

  def author_string_to_contributors(str)
    str.split(/( and )|(,)|( with )|( & )/).
       map{|a| a.strip}.
       reject{|a| ["and" , "," , "with", "&"].include?(a)}
  end





focus_include_code_ary = x.reject{|r| r["CoverType"] == "14" || ["Cancelled", "Old Edition", "Rights Reverted"].include?(r["Status"])}.map{|r| r["Code"]}
(focus_include_code_ary - loaded_code_ary).count


focus_new_code_ary = x.reject{|r| r["CoverType"] == "14" || ["Cancelled", "Old Edition", "Rights Reverted"].include?(r["Status"] || loaded_code_ary.include?(r["Code"]) )}.
                       map{|r| Date.parse(r["Pub_Date"])  rescue nil}.compact.
                       sort.
                       group_by{|x| x}.
                       map{|k,v| [k,v.count]}




########################################################

focus load


reload!
PaperTrail.enabled = false
importer           = FocusImport.new

webname  = "zedbooks"
filename = "/Users/david/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/Stock_Rec_corrected.csv"
dealers  = "/Users/david/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/Dealers.csv"
#filename = "http://s3-eu-west-1.amazonaws.com/bibliocloudimages/Stock_Rec_corrected.csv"

ActiveRecord::Base.transaction do
  #importer.import_stock_rec(webname, filename) ; 0
  #importer.import_prices(webname) ; 0
  #importer.import_stock_rec_prices(webname, filename) ; 0
  #importer.import_dealers(webname, dealers) ; 0
  #importer.import_blurbs(webname) ; 0
  importer.import_contribs(webname) ; 0
raise ActiveRecord::Rollback
end


PaperTrail.enabled = false

    f             = open("http://s3-eu-west-1.amazonaws.com/bibliocloudimages/Stock_Rec_corrected.csv")
    @client       = Client.find_by_webname("zedbooks")
    stock_recs    = CSV.read(f, :headers => :first_row, :encoding => 'windows-1251:utf-8')
    #loaded_codes  = @client.books.pluck(:internal_reference)
    loaded_codes  = []
    @data_to_load = stock_recs.reject{|r| r["ePub_Type"].blank? || r["BKTitle"].blank? ||r["CoverType"] == "14" || loaded_codes.include?(r["Code"])} ; 0
    @data_to_load.each do |product|
      b = @client.books.find_by_internal_reference(product["Code"])
      unless b.nil?
        b.update_attributes!(epub_type_code: product["ePub_Type"  ].presence,
                            epub_format_code:      product["ePub_Format"].presence)
      end
    end ;0







header = []
File.foreach("/Users/davidaldridge/Documents/Bibliocloud/Clients/Zed/metadata/Focus/focus_csv/Dealers.csv", :encoding =>'windows-1251:utf-8' ) do |csv_line|
  begin
   row = CSV.parse(csv_line).first
    if header.empty?
      header = row.map(&:to_sym)
      next
    end

  rescue
    puts csv_line
  end

end