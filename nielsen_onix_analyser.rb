
MINOR
Missing NoContributor
Just a few records involved, but although NoSeries and NoEdition are sent throughout, NoContributor isn’t included on records without any Contributor composites.  Can you include the NoContributor flag if appropriate?

QUERY
MediaFile composites not included
MediaFile composites (primarily for jacket image URL links) are not included, but are included in current Liverpool UP feed to Nielsen.  Can you send these where available?

QUERY
Low percentage of short descriptions
Just 115 short descriptions out of a total of 477 records.  Is this the expected level of short description coverage for Liverpool UP?  Please confirm.


NEXT STEPS
Please let me have a revised file when ready, with a note of the fixes applied so that we can move the re-test along quickly.
If any of the above items are difficult for you to resolve please let me know - depending on the issue we may be able to find a solution at our end.

Many thanks.

Julia




doc = Nokogiri::XML(open("/Users/david/rails/LUP201509240915_original_7d27510fff593d69f4549f3cca1cdb6b.xml")) ; 0
doc.remove_namespaces! ; 0


Missing ExpectedShipDate
ExpectedShipDate is required with AvailabilityCode NP/ProductAvailability 10 but has not been included at all.
Please refer to code list 65 for the ProductAvailability codes that require a future ExpectedShipDate -
in this particular test sample it must be sent for all ‘forthcoming’ records with ProductAvailability 10.

doc.xpath("//SupplyDetail[AvailabilityCode='NP']/ExpectedShipDate").map{|x| x.text}.uniq


SHOWSTOPPER
Missing PublicationDate on some records
Omitting PublicationDate is only OK if PublishingStatus is 01 (abandoned) or 03 (postponed indefinitely) - it’s expected for all other products.   But it’s missing on a significant number of records with PublishingStatus 02 or 04 - please can you include.

OK

SIGNIFICANT
Large number of records with availability status ‘contact supplier’ but none ‘available’
This test file contains a very high percentage of AvailabilityCode CS/ProductAvailability 99.  Is this appropriate?  There are no products with IP/20 status at all which is unusual.
The current live feed from Liverpool UP to Nielsen contains very few records with availability status ‘contact supplier’ but a lot of ‘available’ - more the ratio we’d expect.  Please confirm why such a high percentage of ‘contact supplier’?

 doc.xpath("//SupplyDetail").map{|x| [x.at_xpath("SupplierName").text, x.at_xpath("../PublishingStatus").text, x.at_xpath("AvailabilityCode").text]}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}.sort_by{|_k, v| v}


 SIGNIFICANT
 Inappropriate use of TaxRateCode2
 Some Price composites contain both TaxRateCode1 and TaxRateCode2 but both values are Z.  TaxRateCode2 should be used only for mixed tax rate items (i.e. one value would be Z and one S, indicating components in a pack taxed at different rates).  Can you omit TaxRateCode2 unless the item is a pack containing items taxed at different rates?  Example - 9780853238232:

 doc.xpath("//TaxRateCode2").map{|x| [x.text, x.at_xpath("../TaxRateCode1").text]}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}


 SIGNIFICANT
 Some GBP with PriceTypeCode 01 rather than 02
 Most GBP have the expected PriceTypeCode 02 (inclusive of tax) but just a few have 01 (eg 9781781381946).  Nielsen expects GBP to be sent with PriceTypeCode 02, indicating a GB retail price inclusive of either zero or standard rate tax.

 doc.xpath("//Price[CurrencyCode='GBP']").map{|x| x.at_xpath("PriceTypeCode").try(:text)}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}




 SIGNIFICANT
 Missing AudienceCode
 No AudienceCode included - just AudienceDescription.  Please can you include an ONIX Audience code rather than just a free text statement?  (AudienceCode is included in the current Liverpool UP feed to Nielsen.)

 doc.xpath("//Audience").map{|x| [x.at_xpath("AudienceCodeType").try(:text), x.at_xpath("AudienceCodeTypeName").try(:text), x.at_xpath("AudienceCodeValue").try(:text)]}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}
 doc.xpath("//AudienceDescription").map{|x| x.try(:text)}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}




 MINOR
 EditionNumber with decimal points
 EditionNumber 2.0 and similar - can you remove decimal points from EditionNumber?  (If not we could apply a rule to do it - please confirm?)

   doc.xpath("//EditionNumber").map{|x| x.try(:text)}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}




   MINOR
   Missing SubjectSchemeVersion
   SubjectSchemeVersion is omitted in all Subject composites with SubjectSchemeIdentifier 12 (BIC).  Can you include?
   Or if not, can we apply the default SubjectSchemeVersion 2.1 for any subsidiary BIC codes sent in Subject composites?
   Example - 9781781381311:

   doc.xpath("//Subject[SubjectSchemeIdentifier='12']").map{|x| [x.at_xpath("SubjectSchemeIdentifier").try(:text), x.at_xpath("SubjectSchemeVersion").try(:text), x.at_xpath("SubjectCode").try(:text)]}.each_with_object(Hash.new(0)){|x, c| c[x] += 1}



   MINOR
   Invalid BISAC subject codes
   Four invalid BISAC subject codes reported (each occurring on a number of records).  Can you correct?

   LCO017000
   LIT020000
   POE022000
   POL057000
