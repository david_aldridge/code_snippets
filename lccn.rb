class LCCN
  def initialize(lccn_string)
    self.lccn_string = lccn_string
  end

  def normalised
    valid? ? with_expanded_serial : nil
  end

  def valid?
    @valid ||= (clean =~ VALID_REGEX) ? true : false
  end

  def permalink
    "#{URL}#{normalized}" if valid?
  end

  def marcxml_link
    "#{permalink}/marcxml" if valid?
  end

  def mods_link
    "#{permalink}/mods" if valid?
  end

  def mads_link
    "#{permalink}/mads" if valid?
  end

  def dublin_core_link
    "#{permalink}/dc" if valid?
  end

  private

  attr_accessor :lccn_string

  URL = "https://lccn.loc.gov/".freeze

  def clean
    lccn_string.delete(" ").downcase
  end

  def with_expanded_serial
    return unless valid?
    clean =~ /^(?<prefix>.*?)(?<separator)\-(?<serial>.+)/
    format("%s%06d", prefix, serial.to_i)
  end

  # rubocop:disable Matrics/LineLength
  # Ref: https://www.wikidata.org/wiki/Property:P1144
  VALID_REGEX = /^((a(c|fl?|gr)?|b[irs]|c(a?d?|lc|[sxy])|do?|es?|f(i[ae]?)?|g[ms]?|h(a|e[wx]?)?|in?t|j[ax]?|kx?|l(lh|tf)?|m([ams]|ap|ed|i[cdef]|pa?|us)?|n(cn|ex?|[tu]c)|or|p([aop]|h[opq])|r[aceu]?|s(ax?|[cdfgnsu])?|t(b|mp)|u(m|nk)|w(ar)?|[xz])(\b|-)?|20)?\d\d(-\d{1,5}|(\b|-)?\d{6})$/
end
