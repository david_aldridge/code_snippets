@royalty_batch = RoyaltyBatch.find(29)
@royalty_batch.royalty_batch_books.includes(:book => [:contract]).group_by{|rbb| rbb.book.contract}


Sale.where(:book_id => @royalty_batch.royalty_batch_books.pluck(:book_id)).
     select("book_id, channel_id, sum(sale_quantity) sale_quantity, sum(sale_value) sale_value").
     group(:book_id, :channel_id).
     group_by{|s| s.book_id}

books    = @royalty_batch.books.group_by{|b| b.id}
channels = @royalty_batch.client.channels.group_by{|c| c.id}
sales    = Sale.group(:book_id, :channel_id).
                 where(:book_id => books.keys).
                 select("book_id, channel_id, sum(sale_quantity) sale_quantity, sum(sale_value) sale_value")

sales.group_by{|s| {:book_id => s.book_id, :channel_id => s.channel_id}}.group_by{|s| :book_id}

sales.each_with_object(Hash.new([])){|s,h| h[[s.book_id, s.channel_id]] = [s.sale_quantity, s.sale_value]}.
      each_with_object(Hash.new({})){|(k,v),h| h[k[0]] = h[k[0]].merge(k[1] => v)}


sales.map{|s| [:book => books[s.book_id],:channel => channels[s.channel_id], :sale_quantity => s.sale_quantity, :sale_value => s.sale_value]}.



sales.group_by{|s| [s.book_id, s.channel_id]}.
      group_by{|k,v| k[0]}
