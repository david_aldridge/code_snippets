create view todo_dates
as
select
  w.id as work_id,
  case 4
    when 3 then b.min_pub_date - interval '3 days'
    when 4 then b.min_print_pub_date
  end
from
  works w left join
  (
    select
      b.work_id,
      min(b.pub_date) as min_pub_date,
      max(b.pub_date) as max_pub_date,
      min(b.pub_date) filter (where product_form like 'B_') AS min_print_pub_date
    from
      books b
    group by
      b.work_id
  ) b on b.work_id = w.id
;


create view todo_dates
as
select
  t.id as todo_id,
  case t.relative_to
    when 0  then t.due_date
    when 1  then work_products.earliest_pub_date
    when 10 then product_book.pub_date
  end relative_date,
  case t.relative_to
    when 0  then t.due_date
    when 1  then work_products.earliest_pub_date
    when 10 then product_book.pub_date
  end -
    (months_before||' month')::interval  -
    (days_before||' day')::interval
  as due_date
from
  todos t
  left join lateral
    (
      select
        min(books.pub_date) as earliest_pub_date
      from
        books
      where
        books.work_id  = t.object_id and
        t.object_type = 'Work'
      group by
        books.work_id
    ) work_products on true
  left join lateral
    (
      select ms_delivery_date
      from   contracts
      where  id  = t.object_id
    ) contract on t.object_type = 'Contract'
  left join lateral
    (
      select
        pub_date
      from
        books
      where
        books.id      = t.object_id and
        t.object_type = 'Book'
    ) product_book on true;




emun :relative_to, %i[
  nothing
  work_earliest_pub_date
  work_earliest_ebook_pub_date
  work_earliest_print_pub_date
  work_latest_ebook_pub_date
  work_latest_print_pub_date
  work_latest_pub_date
  contract_ms_delivery_date
  contract_renewal_date
  contract_signed_date
  product_pub_date
]

RELATIVE_TO_VALIDITY = {
  work:     [0,1,2,3,4,5,6],
  contract: [0,7,8,9],
  product:  [0,10]
}

  work_earliest_pub_date:       proc {}
  work_earliest_ebook_pub_date: proc {}
  work_earliest_print_pub_date: proc {}
  work_latest_ebook_pub_date:   proc {}
  work_latest_print_pub_date:   proc {}
  work_latest_pub_date:         proc {}
  contract_ms_delivery_date:    proc {}
  contract_renewal_date:        proc {}
  contract_signed_date:         proc {}
  product_pub_date:
