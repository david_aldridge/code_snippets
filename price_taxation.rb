class PriceTaxation
  def initialize(product: , currency_code:, amount:, amount_includes_tax:)
    self.product             = product
    self.currency_code       = currency_code
    self.amount              = amount
    self.amount_includes_tax = amount_includes_tax
  end

  def call
    {
      rate:           rate          ,
      rate_code:      code_string   ,
      exc_tax_amount: exc_tax_amount,
      inc_tax_amount: inc_tax_amount,
      tax_amount:     tax_amount
    }
  end

  private

  attr_accessor :product            ,
                :currency_code      ,
                :amount             ,
                :amount_includes_tax

  def country_code
    # GB
    currency_code && currency_code[0..1].to_sym
  end

  def country_config
    # {rates: {standard: 20},codes:{print_book: :zero } }
    country_code && TAX_RATES[country_code]
  end

  def codes
    # {print_book: :zero }
    country_config && country_config[:codes]
  end

  def code
    # {:zero}
    return :standard unless codes
    codes[taxable_type] || :standard
  end

  def code_string
    code_strings[code]
  end

  def rate
    return 0 if code == :zero
    country_config[:rates][code]
  end

  def exc_tax_amount
    amount_includes_tax ?
      amount - tax_amount :
      amount
  end

  def inc_tax_amount
    amount_includes_tax ?
      amount :
      amount + tax_amount
  end

  def tax_amount
    amount_includes_tax ?
      (amount / (1 + 0.01 * rate)).round(2):
      (amount * 0.01 * rate      ).round(2)
  end

  def taxable_type
    return :ebook      if product.is_ebook?
    return :print_book if product.is_print?
    :standard
  end

  def code_strings
    {
      zero:     "Z",
      reduced:  "R",
      standard: "S"
    }
  end
end

__END__

PriceTaxation.new(product: Book.new(:product_form => 'DG'), currency_code: 'GBP', amount: 10, amount_includes_tax: true).call
