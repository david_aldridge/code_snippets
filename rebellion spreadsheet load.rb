module Rebellion
  class Row
    attr_reader :row, :work

    def initialize(row)
      @row = row
    end

    def call
      work =
        client.
          works.
          joins(:books).
          where(
            books: {
              isbn:
                [
                  isbn13epub,
                  isbn13mobi,
                  isbn13printuk,
                  isbn13printus
                ].reject(&:blank?)
            }
          ).distinct.first ||
        client.works.new

      work.title                          = title
      work.subtitle                       = subtitle
      if rebstockcode.present? && !client.works.where(registrants_internal_reference: rebstockcode).exists?
        work.registrants_internal_reference = rebstockcode
      end
      work.work_type                      = client.work_types.where(name: contenttype).first_or_create! if contenttype.present?
      work.main_bic_code                  = bicv2_classification                                       if bicv2_classification.present?
      work.main_bisac_code                = bisac_code                                                 if bisac_code.present?
      work.keywords                       = tagscommaseparated.split(",").map(&:squish)

      work.save!

      work.books.epubs.create!(isbn: isbn13epub)        unless isbn13epub.blank?    || work.books.where(isbn: isbn13epub).exists?
      work.books.mobis.create!(isbn: isbn13mobi)        unless isbn13mobi.blank?    || work.books.where(isbn: isbn13mobi).exists?
      work.books.paperback.create!(isbn: isbn13printuk) unless isbn13printuk.blank? || work.books.where(isbn: isbn13printuk).exists?
      work.books.paperback.create!(isbn: isbn13printus) unless isbn13printus.blank? || work.books.where(isbn: isbn13printus).exists?

      series_prefix, series_title = DePrefixer.new(series).call
      if series_title
        series =
          client.
          seriesnames.
          where("lower(title_without_prefix) = ?", series_title.downcase).
          first ||
          client.seriesnames.create!(
            title_prefix:         series_prefix,
            title_without_prefix: series_title
          )
        work.seriesnames = [series]

        work.work_seriesnames.first.update!(number_within_series: numberinseries) if numberinseries.present?
      end


      work.publishername = client.publishernames.where(publisher_name: publisher).first_or_create! if publisher.present?

      work.imprint = client.imprints.where(value: imprint).first_or_create! if imprint.present?

      work.contract.sales_rights.destroy_all

      work.contract.sales_rights_template_id =
        case territorialrights
        when "WORLD"
          then 1
        when "US,CA"
          then 2
        else
          3
        end

      if work.contract.sales_rights_template_id == 3
        work.contract.sales_rights = [
          SalesRight.create!(
            client_id:          client.id,
            contract_id:        work.contract.id,
            sales_rights_type:  "01",
            countries_included: territorialrights.split(",").map(&:squish).sort
          ),
          SalesRight.create!(
            client_id:          client.id,
            contract_id:        work.contract.id,
            sales_rights_type:  "03",
            regions_included:   ["WORLD"],
            countries_excluded: territorialrights.split(",").map(&:squish).sort
          )
        ]
      end

      if shortdescription.present?
        work.short_descriptions.destroy_all
        work.short_descriptions.create!(pull_quote: shortdescription)
        work.books.each{|b| b.marketingtext_short_descriptions = work.short_descriptions}
      end

      work.save!

      work.books.each do |book|
        book.prefix      = titleprefix
        book.title       = title
        book.subtitle    = subtitle

        if book.is_print?
          book.pub_date                          = printbookpubdate             if printbookpubdate.present?
          puts "printbooklistprice_usd", printbooklistprice_usd
          book.current_usd_exctax_consumer_price = printbooklistprice_usd       if printbooklistprice_usd.present?
          book.main_content_page_count           = printbookpagecount           if printbookpagecount.present?
        else
          puts "ebooklistprice_gbp_incvat", ebooklistprice_gbp_incvat
          book.current_gbp_inctax_consumer_price = ebooklistprice_gbp_incvat    if ebooklistprice_gbp_incvat.present?
          puts "ebooklistprice_usd", ebooklistprice_usd
          book.current_usd_exctax_consumer_price = ebooklistprice_usd           if ebooklistprice_usd.present?
          puts "audcadeur", audcadeur
          if audcadeur.present?
            book.current_aud_inctax_consumer_price = audcadeur
            book.current_cad_exctax_consumer_price = audcadeur
            book.current_eur_inctax_consumer_price = audcadeur
          end
        end
        book.save!

        book.book_seriesnames.destroy_all
        if work.seriesnames.exists?
          work_seriesname = work.work_seriesnames.first
          book.book_seriesnames.create!(
            seriesname_id:        work_seriesname.seriesname_id,
            number_within_series: work_seriesname.number_within_series
          )
        end
      end
    end

    delegate :client, to: :class

    def self.client
      @_client ||= Client.is_rebellion
    end

    def series_prefix_and_title
      DePrefixer.new(series).call
    end

    %w[
      Title TitleSortName TitlePrefix Subtitle Series NumberInSeries Creator1Name Creator1SortName Creator1Role Creator1Code Creator1BioGoogleKindle
      Creator1Bio Creator2Name Creator2SortName Creator2Role Creator2Code Creator2Bio Creator3Name Creator3SortName Creator3Role Creator3Code Creator4Name
      Creator4SortName Creator4Role ISBN13Epub ISBN13Mobi RebStockCode ISBN13PrintUK ISBN13PrintUS PrintBookPubDate PrintBookPubYYYYMMDD RebStore_PreOrder_Date
      EpubFilename MobiFilename CoverFilename EpubGardContDrive MobiGardContDrive FilesDrive Publisher Imprint eBookListPrice_USD eBookListPrice_GBP_incVAT
      eBookListPrice_GBP_lessVAT AudCadEur iTunesPrices PrintBookListPrice_USD PrintBookPageCount PrintBookForm Language TerritorialRights
      TerritorialRightsExcluded ContentType FictionSubject BICv2_Classification BISAC_Code BISAC_Category BISAC_MajorSubject BISAC_Subject
      DriveThru_PrimaryCategory DriveThru_SecondayCategory RebStoreCategory ShortDescription ShortDescCharCount ShortDescWordCount Review ReviewSource
      ReviewText TagsCommaSeparated TagsCharCount ServerPathName
    ].each do |column_name|
      define_method column_name.parameterize.to_sym do
        row.fetch(column_name).presence
      end
    end
  end
end

__END__
reload!
client      = Client.is_rebellion
filename = "https://bibliocloudimages.s3-eu-west-1.amazonaws.com/clients/eBook_Metadata.xlsx"

require "/Users/david/Documents/Bibliocloud/Code/rebellion spreadsheet load.rb"
filename    = "/Users/david/Documents/Bibliocloud/Clients/Rebellion/From Rebellion/eBook_Metadata.xlsx"
spreadsheet = Roo::Spreadsheet.open(filename, extension: :xlsx)
rows        = spreadsheet.sheet(0).parse(headers: true, clean: true)[1..471] ; 0
objs = rows.map{|row| Rebellion::Row.new(row)} ; 0

ActiveRecord::Base.transaction do
  rows.each{ |row| Rebellion::Row.new(row).call }
  # raise ActiveRecord::Rollback
end




works = [100131, 100502,
  100140, 100517,
  99852, 100376,
  99845, 100493,
  100361, 100588,
  100139, 100482,
  100195, 100629,
  100526, 100646,
  99847, 100524,
  99909, 100271,
  99883, 100054,
  100040, 100310,
  100196, 100443,
  100228, 100626,
  99965, 100649,
  100440, 100573,
  99798, 99894
  ]

ActiveRecord::Base.transaction do
  Client.is_rebellion.works.where(id: works).each(&:destroy!)
  # raise ActiveRecord::Rollback
end
