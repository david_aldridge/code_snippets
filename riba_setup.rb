

code_map = {"03"=>"03",
            "04"=>"05",
            "08"=>"08",
            "02"=>"01",
            "25"=>"13",
            "01"=>"02",
            "13"=>"20"}
Rails.logger.level = 3
client = Client.find_by(:webname => "riba")
client.marketingtexts.where(:legacy_code => code_map.keys).each_with_index do |mt, i|
  puts i+1
  mt.main_type = code_map[mt.legacy_code]
  mt.save! if mt.changed?
end ; 0
