s3 = AWS::S3.new(
  :access_key_id     =>"AKIAIGCG4JH5G22RHHXA",
:secret_access_key => "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")

bucket = s3.buckets["bibliocloudimages"]
obj = bucket.objects['oberon_addresses.csv'].write(open("/Users/davidaldridge/Documents/Bibliocloud/Clients/Oberon/addresses.csv"), :acl => :public_read)
client_id = 349
obj = bucket.objects['oberon_addresses.csv']
csv_file = CSV.parse(obj.read, :headers => true)
Client.transaction do
    csv_file.each do |csv|
        c = Contact.where(:client_id            => client_id,
                          :keynames             => csv["Last Name"],
                          :names_before_key     =>  csv["First Name"]).
                    first_or_create

        attribs = {}

        addresses_attributes = []
        phones_attributes    = []
        emails_attributes    = []
        notes_attributes     = []

        if csv["Address 1"] or csv["Address 2"] or csv["Address 3"] or csv["City"] or csv["Country"] or csv["Postal Code"]
            country_code = (csv["Country"].presence.nil? ? nil : OnixCode.countries.where(:description => csv["Country"]).first.try(:value))
            attribs[:addresses_attributes] = [{:address_one            => csv["Address 1"],
                                               :address_two            => csv["Address 2"],
                                               :address_three          => [csv["Address 3"].presence, csv["City"].presence, (country_code.nil? ? csv["Country"].presence : nil)].compact.join(", "),
                                               :address_four           => country_code,
                                               :postcode               => csv["Postal Code"]}]
        end

        phones_attributes << {:phone_number => csv["Phone Number"]                           } unless csv["Phone Number"].presence.nil?
        phones_attributes << {:phone_number => csv["Mobile"]       ,:phone_type   => "Mobile"} unless csv["Mobile"].presence.nil?
        emails_attributes << {:email        => csv["EMail Address"], :default => true        } unless csv["EMail Address"].presence.nil?
        notes_attributes  << {:note_text    => csv["Notes"]                                  } unless csv["Notes"].presence.nil?


        attribs[:phones_attributes] = phones_attributes unless phones_attributes.empty?
        attribs[:emails_attributes] = emails_attributes unless emails_attributes.empty?
        attribs[:notes_attributes ] = notes_attributes  unless notes_attributes.empty?
        puts "UPDATING", attribs

        c.update_attributes!(attribs)
    end
end
