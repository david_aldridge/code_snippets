Client.is_iop.
  works.
  where(main_bic_code: nil).
  map do |w|
    [
      w.title,
      w.owning_user.try(:email)||w.role_fulfillments.map{|rf| rf.role_adopter.to_s},
      "https://iop.bibliocloud.com/works/#{w.id}/edit",
      w.subjectcode.try(:value)
    ]
  end
