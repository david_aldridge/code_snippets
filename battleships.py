from array import *
class Board(object):
    def __init__(self, name, height, width):
        self.name        = name
        self.height      = height
        self.width       = width
        self.ships       = []
        self.shots_fired = []
    def squares(self):
        return self.height * self.width
    def shoot_at(self, x, y):
        if self.already_shot_at(x, y):
          return "Already shot there!"
        self.log_shot(x,y)
        for ship in self.ships:
            if ship.is_hit_by(x,y):
                if ship.sunk()
                    return "You sunk my {name}!"
                else
                    return "Hit!"
            else
                return "Miss"
    def add_ship(self, name, length, orientation, x_root, y_root):
        self.ships.append(Ship(name, length, orientation, x_root, y_root))
    def already_shot_at(self, x, y):
        return [x,y] in self.shots_fired
    def log_shot(self,x,y):
        self.shots_fired.append([x,y])

b1 = Board("My board", 10,15)
b1.name
b1.height
b1.width
b1.squares()
b1.add_ship("Battleship",4,1,2,1)
b1.shoot_at(1,1)


class Ship(object):
    def __init__(self, name, length, orientation, x_root, y_root):
        self.name        = name
        self.length      = length
        self.orientation = orientation
        self.x_root      = x_root
        self.y_root      = y_root
        self.hits        = []
    def squares(self):
        result = []
        for i in range(0,self.length):
            x = self.x_root if (self.orientation == 0) else self.x_root+i
            y = self.y_root if (self.orientation == 1) else self.y_root+i
            result.append([x,y])
        return result
    def is_hit_by(self,x,y):
        if [x,y] in self.squares()
            log_hit(self,x,y)
            return True
        else
            return False
        end
    def log_hit(self,x,y):
            self.hits << [x,y]
    def sunk(self)
            len(self.hits) == length





s = Ship("s1", 3, 0, 1, 1)
s.name
s.length
s.orientation
s.x_root
s.y_root
s.squares()
s.shoot_at(4,5)


b1 = Board("My board", 10,15)


b1.name
b1.height
b1.width
b1.squares()
b1.add_ship("Battleship",4,1,1,1)
b1.shoot_at(1,1)
