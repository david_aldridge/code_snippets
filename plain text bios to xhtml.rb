Client.is_canelo.contacts.
  where.not(biographical_note: [nil, ""]).
  where.not("biographical_note ilike '%<%'").
  select{|c| c.biographical_note.match?(/\n/) }.
  map do |c|
    [
      c.biographical_note,
      c.biographical_note.
        split("\n").
        map(&:presence).
        compact.map do |line|
          "<p>#{line.squish}</p>"
        end.
        join("\n")
    ]
  end

Client.is_canelo.contacts.
  where.not(biographical_note: [nil, ""]).
  where.not("biographical_note ilike '%<%'").
  select{|c| c.biographical_note.match?(/\n/) }.map(&:id)



  Client.is_canelo.contacts.
  where.not(biographical_note: [nil, ""]).
  where.not("biographical_note ilike '%<%'").
  select{|c| c.biographical_note.match?(/\n/) }.
  each do |c|
    c.update(
      biographical_note:
        c.biographical_note.
          split("\n").
          map(&:presence).
          compact.map do |line|
            "<p>#{line.squish}</p>"
          end.
          join("\n")
    )
  end
