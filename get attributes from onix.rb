file_path = '/Users/david/rails/onix_ninja_files/KoganPage_ebooks (3).xml'

doc = Nokogiri::XML(open(file_path)).document
doc.remove_namespaces!

query = "/ONIXMessage/Product[DescriptiveDetail/ProductForm='EA']"\
        "/ProductSupply/SupplyDetail/Price[Tax/TaxRateCode='S'][CurrencyCode='USD']"\
        "/../../../ProductIdentifier[ProductIDType='03']/IDValue"

doc.xpath(query).map(&:text).uniq






doc.xpath("/ONIXMessage/Product").size

puts "Products: #{doc.xpath("/ONIXMessage/Product").count}"
[ 'PublishingStatus',
  'Title/TitleType',
  'Title/TitleText',
  'Imprint/ImprintName',
  'PublicationDate',
  'Publisher/PublisherName',
  'Publisher/PublishingRole',
  'SupplyDetail/SupplierName',
  'SupplyDetail/SupplyToCountry',
  'SupplyDetail/ProductAvailability',
  'SupplyDetail/Price/PriceTypeCode',
  'SupplyDetail/Price/PriceAmount',
  'SupplyDetail/Price/CurrencyCode',
  'SupplyDetail/Price/CountryCode',
  'Series/TitleOfSeries',
  'Series/NumberWithinSeries',
  'Contributor/ContributorRole',
  'OtherText/TextTypeCode',
  'SalesRights/SalesRightsType',
  'SalesRights/RightsCountry',
  'ProductForm',
  'ProductFormDetail',
  'RelatedProduct/RelationCode',
  'BICMainSubject',
  'BASICMainSubject'].each do |element|
  puts element
  pp doc.xpath("/ONIXMessage/Product").each_with_object(Hash.new(0)){| p , h | h[p.at_xpath(element).try(:text)] += 1}.sort_by{|k,v| k || ''}
end


doc.
  xpath("/ONIXMessage/Product").
  select{|product_node| product_node.xpath("BICMainSubject").empty?}.
  map{|product_node| product_node.at_xpath("RecordReference").text}
