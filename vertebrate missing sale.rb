sale = Sale.find(138620)

smr = StatementMonthlyRoyalty.
  joins(:statement_book, :statement_channel).
  where(
    statement_channels: {
      channel_id: sale.channel_id
    }
  ).
  where(
    statement_books: {
      book_id: sale.book_id
    }
  ).
  where(invoice_month: sale.invoice_date.beginning_of_month)

smr.sale_quantity
smr.sale_value
smr.royalty_payable_amount
