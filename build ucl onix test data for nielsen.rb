client = Client.is_ucl

CreateStandardContacts.new(client: client, identifiers: ["nielsen"]).call

ids = [
13_124_705,
13_124_706,
13_124_707,
13_124_708,
13_124_709,
13_124_716,
13_124_717,
13_124_718,
13_124_719,
13_124_720,
13_124_721,
13_124_722,
13_124_723,
13_124_725,
13_124_731,
13_124_732,
13_124_733,
13_124_740,
13_124_741,
13_124_743,
13_124_744,
13_124_755,
13_124_756,
13_124_761,
13_124_762,
13_124_769,
13_124_770,
13_124_771,
13_124_778,
13_124_781
]




product_forms = client.books.onix_export_allowed.pluck(:product_form).compact.uniq - ["00"]
publishing_statuses = client.books.onix_export_allowed.pluck(:publishing_status).compact.uniq.sort - ["00"]


eb = client.export_batches.create(
  contact_digital_asset_transfer_template_id: client.contacts.is_nielsen.contact_digital_asset_transfer_templates.first.id,
  name: "Nielsen test",
  incremental: false
  )

eb.books +=
  client.imprints.flat_map do |imprint|
      books = imprint.books.
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))

      selected = books.order(pub_date: :desc).
        select{|b| b.short_description_text && b.description_text && b.main_bic_code}.
        first(6)
      puts "imprint #{imprint}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
product_forms.flat_map do |product_form|
      books = client.books.
        where(product_form: product_form).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))


      selected = books.
        where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select{|b| b.short_description_text && b.description_text && b.main_bic_code}.
        first(6)
      puts "form #{product_form}: #{selected.size} of #{books.size}"
      selected
    end ; 0

eb.books +=
publishing_statuses.flat_map do |pub_status|
      books = client.books.
        where(publishing_status: pub_status).
        onix_export_allowed.
        where(Book.arel_table[:pub_date].lt(Date.today + 3.months))

      selected = books.where.not(id: eb.book_ids).
        order(pub_date: :desc).
        select{|b| b.short_description_text && b.description_text && b.main_bic_code}.
        first(6)
      puts "status #{pub_status} (#{OnixCode.lookup(:publishing_statuses, pub_status)}): #{selected.size} of #{books.size}"
      selected
    end ; 0


eb.get_content_items


client.check_results.delete_all
CheckRunner.call(CheckClass.all.checks, eb.books.for_check_runner).persist









"9781571574213" "9781907279317" have no contributors


ExportBatchItem.where(id: [11038153, 11038147, 11038127]).destroy_all




client.works.select do |w|
  !w.marketingtexts.where(legacy_code: "02").any? && w.marketingtexts.where(legacy_code: "03").any?
end.size


client.works.includes(:marketingtexts).map do |w|
  [
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)


client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)

["04", false, true, true]=>166,

client.books.onix_export_allowed.includes(:marketingtexts).select do |w|
    w.marketingtexts.none?{|x| x.legacy_code == "01"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "02"} &&
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
end.map do |w|
  [
    w.marketingtexts.detect{|x| x.legacy_code == "02"}.pull_quote.size,
    w.marketingtexts.detect{|x| x.legacy_code == "03"}.pull_quote.size
  ]
end.sort
group_by(&:itself).transform_values(&:size)







client.books.onix_export_allowed.includes(:marketingtexts).map do |w|
  [
    w.publishing_status,
    w.marketingtexts.any?{|x| x.legacy_code == "01"},
    w.marketingtexts.detect{|x| x.legacy_code == "01"}&.pull_quote&.size&.>(350),
    w.marketingtexts.any?{|x| x.legacy_code == "02"},
    w.marketingtexts.detect{|x| x.legacy_code == "02"}&.pull_quote&.size&.<=(350),
    w.marketingtexts.any?{|x| x.legacy_code == "03"}
  ]
end.group_by(&:itself).transform_values(&:size)






client = Client.is_quiller
client.books.onix_export_allowed.map do |b|
 [
   b.imprint.to_s,
   b.isbn_registrant_hyphenated
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}

client.books.onix_export_allowed.map do |b|
 [
   b.isbn_registrant_hyphenated,
   b.imprint.to_s
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last).uniq}




 {"978-1-872119"=>["Kenilworth Press", "Half Halt Press Inc"],
 "978-1-872082"=>["Kenilworth Press"],
 "978-1-904057"=>["Swan Hill Press", "Quiller", "The Sportsman's Press"],
 "978-1-84037"=>["Swan Hill Press", "Quiller"],
 "978-1-85310"=>["Swan Hill Press", "Quiller"],
 "978-0-948253"=>["The Sportsman's Press"],
 "978-1-84689"=>["Quiller", "Swan Hill Press"],
 "978-0-901366"=>["Kenilworth Press", "Half Halt Press Inc"],
 "978-1-85487"=>["Excellent Press", "Swan Hill Press"],
 "978-1-899163"=>["Quiller"],
 "978-1-900318"=>["Excellent Press"],
 "978-0-907621"=>["Quiller"],
 "978-1-57157"=>["Swan Hill Press", "Safari Press (U.S.)", "Quiller"],
 "978-1-905693"=>["Kenilworth Press", "Quiller"],
 "978-1-910016"=>["Kenilworth Press"],
 "978-1-4930"=>["The Lyons Press", "Falcon Guides"],
 "978-1-57076"=>["Trafalgar Square", "Stackpole Books"],
 "978-0-8117"=>["Stackpole Books", "Swan Hill Press"],
 "978-0-949114"=>["Game Fields Press"],
 "978-0-9541531"=>["The Pony Club"],
 "978-0-900226"=>["The Pony Club"],
 "978-0-939481"=>["Half Halt Press Inc", "Kenilworth Press"],
 "978-0-9561071"=>["The Pony Club"],
 "978-0-9548863"=>["The Pony Club"],
 "978-0-9793460"=>["Headwater Books", "Stackpole Books"],
 "978-0-9553374"=>["The Pony Club"],
 "978-1-907279"=>["The Pony Club"],
 "978-1-58574"=>["The Lyons Press"],
 "978-1-934753"=>["Headwater Books"],
 "978-1-59921"=>["The Lyons Press"],
 "978-1-939226"=>["Stonefly Press"],
 "978-1-58667"=>["Derrydale Press"],
 "978-0-9537167"=>["The Pony Club"],
 "978-1-58080"=>["Burford Books (U.S.)"],
 "978-0-312"=>["St Martin's Press"],
 "978-1-4422"=>["Rowman & Littlefield Publishers"],
 "978-1-870948"=>["Quiller"],
 "978-0-905601"=>["Quiller"],
 "978-0-600"=>["Kenilworth Press"],
 "978-0-906393"=>["Swan Hill Press"]}
