require 'benchmark'
module Tester
  using ASCIIStringExtensions

  def method0(string)
    false
  end

  def self.method1(string)
    string.match? /\A[0-9]{9}\z/
  end

  def self.method2(string)
    string.bytesize == 9 && string.digits_only?
  end

  def self.method3(string)
    string.bytesize == 9
  end

  def self.method4(string)
    string.each_byte.none? { |byte| byte > 57 || byte < 48 }
  end

  def self.method5(string)
    string.each_byte.any? { |byte| byte > 57 || byte < 48 }
  end

  def self.method6(string)
    string.match? /\A[0-9]+\z/
  end
end

n = 1000000
Benchmark.bm(7) do |x|
  x.report("method 0")   { n.times {Tester.method0(string) } }
  string = "012345678"
  puts Tester.method1(string)
  puts Tester.method2(string)
  puts Tester.method3(string)
  puts Tester.method4(string)
  puts Tester.method5(string)
  puts Tester.method6(string)
  x.report("method 1 pass")   { n.times {Tester.method1(string)}}
  x.report("method 2 pass")   { n.times {Tester.method2(string)}}
  x.report("method 3 pass")   { n.times {Tester.method3(string)}}
  x.report("method 4 pass")   { n.times {Tester.method4(string)}}
  x.report("method 5 pass")   { n.times {Tester.method5(string)}}
  x.report("method 6 pass")   { n.times {Tester.method6(string)}}
  string = "01"
  puts Tester.method1(string)
  puts Tester.method2(string)
  x.report("method 1 fail size short")   { Tester.method1(string) }
  x.report("method 2 fail size short")   { Tester.method2(string) }
  string = "010000000000000000000000000000000000000"
  puts Tester.method1(string)
  puts Tester.method2(string)
  x.report("method 1 fail size long")   { Tester.method1(string) }
  x.report("method 1 fail size long")    { Tester.method2(string) }
  string = "x12345678"
  puts Tester.method1(string)
  puts Tester.method2(string)
  x.report("method 1 fail chars early")   { Tester.method1(string) }
  x.report("method 2 fail chars early")   { Tester.method2(string) }
  string = "12345678x"
  puts Tester.method1(string)
  puts Tester.method2(string)
  x.report("method 1 fail chars late")   { Tester.method1(string) }
  x.report("method 2 fail chars late")   { Tester.method2(string) }
end ; ""
