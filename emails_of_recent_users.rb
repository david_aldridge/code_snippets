recent_users = User.where(:id => RequestLog.where("created_at > ?", Date.today - 3.months).pluck(:user_id).uniq )
dormant_users = User.dormant
system_users = User.where("email like '%@bibliocloud.com'")

(recent_users - dormant_users - system_users).sort_by(&:client_id).map(&:email).join(' ; ')