








isbns = %w(9781787350472
9781787350779
9781787351127
9781787351134
9781787351189
9781787351196
9781787351721
9781787351738
9781787352827
)

Client.is_ucl.books.where(isbn: isbns).flat_map(&:print_on_demand_specs).map(&:active)

Client.is_ucl.books.where(isbn: isbns).flat_map(&:active_print_on_demand_specs).map(&:active)
