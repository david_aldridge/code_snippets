vol_isbns = sheet.list.map{|row| row["ISBN"]}
set_isbns = sheet.list.map{|row| row["Set_ISBN"]}
vol_isbns.count
set_isbns.count

vol_isbns.uniq.count
2865
set_isbns.uniq.count
2555

vol_clean_isbns = vol_isbns.select{|i| Lisbn.new(i).valid?}.map{|i| Lisbn.new(i).isbn}
vol_clean_isbns.uniq.count
2858
set_clean_isbns = set_isbns.select{|i| Lisbn.new(i).valid?}.map{|i| Lisbn.new(i).isbn}
set_clean_isbns.uniq.count
2548


client.books.count
2815

client.books.pluck(:isbn).uniq.count
=> 2549

source_isbns = (vol_isbns + set_isbns)

source_clean_isbns = (vol_clean_isbns + set_clean_isbns).uniq
source_clean_isbns.count
2981

(source_clean_isbns - client.books.pluck(:isbn)).map{|i| Lisbn.new(i).isbn_with_dash}
["978-1-59333-345-4", "978-1-59333-314-0", "159333223-8", "978-1-59333-491-8", "978-1-61143-942-7"]




vol_isbns = sheet.list.map{|row| row["ISBN"]}
