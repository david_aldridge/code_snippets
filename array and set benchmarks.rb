

require 'benchmark'

ARRAY_LIST      = WORLD_COUNTRIES_LIST
SET_LIST        = Set.new(ARRAY_LIST)
SORTED_SET_LIST = SortedSet.new(ARRAY_LIST)

X_ARRAY_LIST      = ARRAY_LIST.dup << " "
X_SET_LIST        = SET_LIST << " "
X_SORTED_SET_LIST = SORTED_SET_LIST << " "

n = 100000
Benchmark.bm(13) do |x|
  x.report("array 1")      { n.times {(ARRAY_LIST & ARRAY_LIST).any? } }
  x.report("array 2")      { n.times {ARRAY_LIST.any?{|country| country.in? ARRAY_LIST}}}
  x.report("set 1")        { n.times {(SET_LIST & SET_LIST).any? } }
  x.report("set 2")        { n.times {SET_LIST.any?{|country| country.in? SET_LIST}}}
  x.report("sorted set 1") { n.times {(SORTED_SET_LIST & SORTED_SET_LIST).any? } }
  x.report("sorted set 2") { n.times {SORTED_SET_LIST.any?{|country| country.in? SORTED_SET_LIST}}}
  x.report("array 1")      { n.times {(ARRAY_LIST & X_ARRAY_LIST).any? } }
  x.report("array 2")      { n.times {ARRAY_LIST.any?{|country| country.in? X_ARRAY_LIST}}}
  x.report("set 1")        { n.times {(SET_LIST & X_SET_LIST).any? } }
  x.report("set 2")        { n.times {SET_LIST.any?{|country| country.in? X_SET_LIST}}}
  x.report("sorted set 1") { n.times {(SORTED_SET_LIST & X_SORTED_SET_LIST).any? } }
  x.report("sorted set 2") { n.times {SORTED_SET_LIST.any?{|country| country.in? X_SORTED_SET_LIST}}}
end ; ""

