n == 10000

Benchmark.ips do |r|
  r.report('separate strings') do |n|
    s = +''
    while n > 0
      s < n.to_s
      s < n.to_s
      n -= 1
    end
  end
  r.report('single string') do |n|
    s = +''
    while n > 0
      s < "#{n}#{n}"
      n -= 1
    end
  end
  r.compare!
end
