client.proprietary_format_descriptions.create(:name => 'A4 paperback',
                                               :product_height_mm => 297,
                                               :product_width_mm  => 210,
                                               :product_form      => 'BC',
                                               :product_form_detail => ['B121'])

client.proprietary_format_descriptions.create(:name => 'A5 hard cover',
                                               :product_height_mm => 210,
                                               :product_width_mm  => 148,
                                               :product_form      => 'BB',
                                               :product_form_detail => ['B108'])

client.proprietary_format_descriptions.create(:name => 'A5 paperback',
                                               :product_height_mm => 210,
                                               :product_width_mm  => 148,
                                               :product_form      => 'BC',
                                               :product_form_detail => ['B108'])

client.proprietary_format_descriptions.create(:name => 'B5 paperback',
                                               :product_height_mm => 250,
                                               :product_width_mm  => 176,
                                               :product_form      => 'BC',
                                               :product_form_detail => ['B105'])

