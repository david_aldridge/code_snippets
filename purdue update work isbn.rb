Client.is_purdue.works.
  where(identifying_isbn13: nil).each do |w|
  books = w.books.
    not_cancelled.
    where.not(pub_date: nil).
    where.not(isbn: "placeholder").
    order(:pub_date)
  next if books.empty?
  book = books.detect(&:hardback?) || books.detect(&:paperback?)
  next unless book
  w.update_attributes(identifying_isbn13: book.isbn)
end
