require 'serrano'
Serrano.configuration do |config|
  config.base_url = "https://api.crossref.org"
  config.mailto = "david@consonance.app"
end

Serrano.works(ids: '10.11116/9789461662743')


