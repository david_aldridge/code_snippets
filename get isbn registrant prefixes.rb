Client.is_rethink.
  books.
  map do |b|
    [
      ISBNReader.new(isbn: b.isbn).isbn_registrant_hyphenated,
      b.imprint&.to_s
    ]
  end.
  group_by(&:first).
  transform_values(&:uniq).
  transform_values{|x| x.map(&:last).compact.sort}


isbns = {
"978-0-7117"     => "Jarrold Publishing",
"978-0-9544767"  => "Lifestyle Press",
"978-0-9545803"  => "Malnoue Pub",
"978-0-9553308"  => "Monstermedia",
"978-1-910336"   => "Publisher White Ladder Press",
"978-1-911067"   => "Publisher Trotman Education",
"978-988-99143"  => "Publisher Daiichi"
}





isbns.keys.map{|x| ISBNReader.new(isbn: x).isbn_registrant_hyphenated}

"978-0-7117"
"978-0-9544767"
"978-0-9545803"
"978-0-9553308"
"978-1-910336"
"978-1-911067"
"978-988-99143"


isbns = %w(
9780000101334
9780118800389
9780198154099
9780631159339
9780631174929
9784944113118
9784944113170
9784944113187
9784944113255
9784944113262
9784944113286
9784944113323
9784944113330
9784944113460
9784944113477
9784944113521
9784944113606
9788885033269
9789053908198
)

isbns.map{|x| ISBNReader.new(isbn: x).isbn_registrant_hyphenated}.group_by(&:itself).transform_values(&:size)



