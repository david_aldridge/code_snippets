client = Client.is_pharma

bic_codes = client.works.
  where(main_bisac_code: nil).
  pluck(:main_bic_code).
  uniq


code_map = client.works.
  where(main_bic_code: bic_codes).
  where.not(main_bisac_code: nil).
  group(:main_bic_code, :main_bisac_code).
  count

pp code_map


client.works.where(main_bisac_code: nil).
where(main_bic_code: "MMG").
each do |w|
  w.update_attributes(main_bisac_code: "MED071000")
end
