isbns = %w[
9780995577060
]

client = Client.is_what_on_earth

client.
  books.where(isbn: isbns).
  size

client.
  books.where(isbn: isbns).
  flat_map(&:digital_asset_content_items).
  flat_map(&:digital_asset_contents).
  each(&:destroy)

client.
  books.where(isbn: isbns).
  flat_map(&:export_batch_items).
  each(&:destroy)


client.
  books.where(isbn: isbns).
  flat_map(&:digital_asset_content_items).
  each(&:destroy)

client.
  books.where(isbn: isbns).
  each(&:destroy)

client.
  books.where(isbn: isbns).
  size
