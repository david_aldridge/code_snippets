

require 'benchmark'
ID = Client.is_zed.id

def method1(id)
    MiniSql::Connection.new(ActiveRecord::Base.connection.raw_connection).
      query(
        <<~SQL,
          select
            c.corporate_name,
            c.id
          from
            contacts c
          where
            c.client_id in (?) and
            c.corporate_name is not null
          order by
            lower(unaccent(c.corporate_name))
        SQL
        Array.wrap(id)
      ).map { |row| [row.corporate_name, row.id] }
end

def method2(id)
    MiniSql::Connection.new(ActiveRecord::Base.connection.raw_connection).
      query_single(
        <<~SQL,
          select array_to_json(array_agg(row_to_json(t.*))) #>> '{}'
          from
          (
            select
              c.corporate_name,
              c.id
            from
              contacts c
            where
              c.client_id in (354) and
              c.corporate_name is not null
            order by
              lower(unaccent(c.corporate_name))
          ) t
        SQL
        Array.wrap(CLIENT_ID)
      ).first
end ; 0

def method3(id)
    MiniSql::Connection.new(ActiveRecord::Base.connection.raw_connection).
      query_single(
        <<~SQL,
          select '['|| array_to_string(array_agg(row_to_json(t.*)), ',')||']'
          from
          (
            select
              c.corporate_name,
              c.id
            from
              contacts c
            where
              c.client_id in (354) and
              c.corporate_name is not null
            order by
              lower(unaccent(c.corporate_name))
          ) t
        SQL
        Array.wrap(CLIENT_ID)
      ).first
end


n = 100
Benchmark.bm(7) do |x|
  x.report("method 1")   { n.times {method1(ID)}}
  x.report("method 2")   { n.times {method2(ID)}}
  x.report("method 3")   { n.times {method3(ID)}}
end ; ""

require 'memory_profiler'
MemoryProfiler.report do
  method1(ID)
end.pretty_print

allocated memory by class
-----------------------------------
    253992  Array
    248783  String
    178120  <<Unknown>>
       560  MatchData
        72  MiniSql::Connection
        40  MiniSql::InlineParamEncoder
        40  PG::Result
        40  PG::TypeMapByColumn

allocated objects by class
-----------------------------------
      4463  Array
      4461  String
      4453  <<Unknown>>
         2  MatchData
         1  MiniSql::Connection
         1  MiniSql::InlineParamEncoder
         1  PG::Result
         1  PG::TypeMapByColumn


MemoryProfiler.report do
  method2(ID) ; 0
end.pretty_print

allocated memory by class
-----------------------------------
    255529  String
       240  Array
        72  MiniSql::Connection
        40  MiniSql::InlineParamEncoder
        40  PG::Result

allocated objects by class
-----------------------------------
         6  Array
         5  String
         1  MiniSql::Connection
         1  MiniSql::InlineParamEncoder
         1  PG::Result

MemoryProfiler.report do
  method3(ID) ; 0
end.pretty_print

allocated memory by class
-----------------------------------
    255325  String
       240  Array
        72  MiniSql::Connection
        40  MiniSql::InlineParamEncoder
        40  PG::Result

allocated objects by class
-----------------------------------
         6  Array
         5  String
         1  MiniSql::Connection
         1  MiniSql::InlineParamEncoder
         1  PG::Result
