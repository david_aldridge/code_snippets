
chapter = Client.is_bds.work_types.find_by(name: "Chapter")
volume = Client.is_bds.work_types.find_by(name: "Volume")
monograph = Client.is_bds.work_types.find_by(name: "Monograph")

Work.no_touching do
  Client.is_bds.proprietary_edition_descriptions.find_by(code: "Ch").works.uniq.each do |work|
    work.work_type = chapter
    work.save
  end
end ; 0

Work.no_touching do
  Client.is_bds.proprietary_edition_descriptions.find_by(code: "Vol").works.uniq.each do |work|
    work.work_type = volume
    work.save
  end
end ; 0

Work.no_touching do
  Client.is_bds.proprietary_edition_descriptions.find_by(code: "Mono").works.uniq.each do |work|
    work.work_type = monograph
    work.save
  end
end ; 0
