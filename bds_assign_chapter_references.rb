client = Client.is_bds
client.books.where(:publishers_reference => nil).count
ActiveRecord::Base.transaction do
  client.books.joins(:work => :work_derivations).where(:publishers_reference => nil, :epub_type_code => '002').each do |book|
    chapter_number = book.work.work_derivations.first.nominal_order
    next unless chapter_number
    derived_book      = book.work.work_derivations.first.derived_work.books.is_hardback.take
    next unless derived_book
    derived_book_isbn   = derived_book.isbn
    next unless Lisbn.new(derived_book_isbn).valid?
    publishers_reference = "#{derived_book_isbn}_#{chapter_number.to_s.rjust(3,'0')}"
    puts "#{publishers_reference}: \"#{book.title}\""
    book.update_columns(:publishers_reference => publishers_reference)
  end
end ; 0

client.books.pluck(:publishers_reference)
