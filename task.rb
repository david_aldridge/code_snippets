class Task < ActiveRecord::Base
  STATUS_DONE       = "STATUS_DONE".freeze
  STATUS_SUSPENDED  = "STATUS_SUSPENDED".freeze
  STATUS_ACTIVE     = "STATUS_ACTIVE".freeze
  STATUS_FAILED     = "STATUS_FAILED".freeze
  STATUS_UNDEFINED  = "STATUS_UNDEFINED".freeze

  STATUS_NAMES      = {
    STATUS_DONE      => "Completed".freeze,
    STATUS_SUSPENDED => "Pending".freeze  ,
    STATUS_ACTIVE    => "Active".freeze   ,
    STATUS_FAILED    => "Failed".freeze   ,
    STATUS_UNDEFINED => "Undefined".freeze
  }.freeze

  STATUS_LIST       = STATUS_NAMES.to_a.freeze

  REPORT_COLUMNS    =
    {
      0  => ["IDs"           , "Bibliocloud Schedule ID", "The internal ID for the schedule record"      , "id"                        ],
      1  => ["Extras"        , "Blank column"           , "A blank column"                               , ""                          ],
      2  => ["Extras"        , "Y"                      , "Print 'Y' in the column"                      , "print_a_word"     , "Y"    ],
      3  => ["Extras"        , "N"                      , "Print 'N' in the column"                      , "print_a_word"     , "N"    ],
      4  => ["IDs"           , "Name"                   , "The name of the task"                         , "name"                      ],
      5  => ["IDs"           , "Schedule"               , "The name of the schedule"                     , "schedule"                  ],
      6  => ["Responsibility", "External contact"       , "The first external person down to do the task", "contacts" , "first"],
      7  => ["Responsibility", "In house contact"       , "The first in house person down to do the task", "first_user"                ],
      8  => ["Timings"       , "Due date"               , "When the task is due"                         , "end"                       ],
      9  => ["Timings"       , "Duration"               , "Number of days the task is estimated to take" , "duration"                  ],
      10 => ["Timings"       , "Status"                 , "Current status"                               , "translated_status"         ]
    }.freeze

  strip_attributes

  attr_accessible :benchmark              ,
                  :client_id              ,
                  :code                   ,
                  :completed              ,
                  :created_at             ,
                  :date_before_publication,
                  :depends                ,
                  :description            ,
                  :duration               ,
                  :end                    ,
                  :endIsMilestone         ,
                  :index_number           ,
                  :level                  ,
                  :name                   ,
                  :owner                  ,
                  :position               ,
                  :production_task        ,
                  :progress               ,
                  :schedule_id            ,
                  :start                  ,
                  :startIsMilestone       ,
                  :status                 ,
                  :task_users_attributes  ,
                  :contact_ids            ,
                  :updated_at             ,
                  :user_id

  include PgSearch

  pg_search_scope :task_search ,
                  against:            [:name ,
                                       :owner ,
                                       :description],
                  using:              {tsearch: {dictionary: "english", prefix: true, any_word: true}},
                  associated_against: {users:      [:first_name,
                                                    :last_name ,
                                                    :email     ],
                                       schedule:   [:name      ],
                                       task_users: [:effort    ],
                                       task_roles: [:name      ] }

  multisearchable against: [:name, :owner, :description]

  def self.text_search(query)
    if query.present?
      task_search(query).select("tasks.*").with_pg_search_rank
    else
      all
    end
  end

  belongs_to :schedule  , inverse_of: :tasks
  belongs_to :user      , inverse_of: :tasks

  has_many   :task_users    , inverse_of: :task         , dependent: :destroy
  has_many   :task_contacts , inverse_of: :task         , dependent: :destroy
  has_many   :contacts      , through: :task_contacts
  has_many   :critical_books, class_name: "Book"
  has_many   :users         , through: :task_users
  has_many   :task_roles    , through: :task_users

  has_many   :same_schedule_tasks, through: :schedule, source: :tasks

  has_many   :precursor_tasks,
             -> (task) {where(index_number: task.dependent_on)},
             through: :schedule,
             source:  :tasks

  has_many   :dependent_tasks,
             -> (task) {where("','||depends||',' like ','||#{(task.index_number+1).to_s}||','")},
             through: :schedule,
             source:  :tasks

  has_one    :collection_task, inverse_of: :schedule_task

  has_one    :parent_task,
             -> (task) {where(level: task.level-1).
                        where("index_number < ?",task.index_number).
                        order(index_number: :desc).
                        limit(1)},
             through: :schedule,
             source:  :tasks

  has_many   :child_tasks,
             -> (task) {where("index_number > ? and level > ?", task.index_number, task.level).
                        where.not("exists (select 1
                                             from tasks t2
                                            where t2.schedule_id  = tasks.schedule_id  and
                                                  t2.index_number >  ?                 and
                                                  t2.index_number <= tasks.index_number and
                                                  t2.level        = ?)", task.index_number, task.level).
                         order("index_number asc")},
             through: :schedule,
             source:  :tasks

  def tasks_below
    schedule.tasks.select { |task| task.index_number > self.index_number }
  end

  def children
    tasks_below.sort_by(&:index_number).take_while { |task| task.level > self.level }
  end

  has_many   :immediate_child_tasks,
             -> (task) {where("index_number > ? and level > ? and level < ?", task.index_number, task.level, task.level + 2).
                        where.not("exists (select 1
                                             from tasks t2
                                            where t2.schedule_id  = tasks.schedule_id  and
                                                  t2.index_number >  ?                 and
                                                  t2.index_number <= tasks.index_number and
                                                  t2.level        = ?)", task.index_number, task.level).
                         order("index_number asc")},
             through: :schedule,
             source:  :tasks

  has_one    :next_task,
             -> (task) {where("index_number = ?",task.index_number+1)},
             through: :schedule,
             source:  :tasks

  validates :duration, presence: true
  validates :schedule, presence: true
  validates :level   , presence: true
  validates :name    , presence: true

  accepts_nested_attributes_for :task_users   , allow_destroy: true
  accepts_nested_attributes_for :task_contacts, allow_destroy: true

  after_update :notify_task_user

  def self.key_tasks
    where(production_task: true)
  end

  def notify_task_user
    self.task_users.each do |task_user|
      Notification.create!(client_id: self.client_id, user_id: task_user.user_id, message: "Task #{self.id} updated: #{self.to_s} Due: #{self.end} #{task_user.user.slack_name}", notifyable_type: self.class, notifyable_id: self.id)
    end
  end

  def first_user
    task_users.first.try(:user).try(:to_s)
  end

  def to_s
    name
  end

  def name_and_end_date
    "#{name}: #{self.end}"
  end

  def late
    not_completed? && ended?
  end

  def ended?
    self.end < Date.today
  end

  def completed
    status == STATUS_DONE
  end

  def not_completed?
    !completed
  end

  def translated_status
    STATUS_NAMES[status] || "Unknown"
  end

  def set_benchmark
    self.update_column(:benchmark, self.end)
  end

  def unset_benchmark
    self.update_column(:benchmark, nil)
  end

  def dependencies_incomplete?
    dependent_tasks.where.not(:status, STATUS_DONE).exists?
  end

  def dependencies_complete?
    !dependencies_incomplete?
  end

  def depends_positions
    return unless depends
    depends.split(",").map(&:to_i)
  end

  def dependent_on
    return unless depends_positions
    depends_positions.map{|i| i-1}
  end

  def depends_on_something?
    !!depends.presence
  end

  def complex?
    @complex ||= level > 1 || depends_on_something?
  end

  def simple?
    !complex?
  end

  def latest_start
    return self.start if is_milestone
    return new_week_day(latest_end,-(duration-1)) unless has_children?
    return child_tasks.minimum(:start)
  end

  def latest_end
    return self.end if is_milestone
    earliest_dependent_task_start = dependent_tasks.minimum(:start)
    parents_dependent_task_start  = (parent_task and parent_task.dependent_tasks.exists? ? parent_task.dependent_tasks.minimum(:start) : nil)
    return [latest_child_task_end,
            new_week_day(earliest_dependent_task_start,-1),
            new_week_day(parents_dependent_task_start ,-1)].reject(&:nil?).min || self.end
  end

  def is_milestone
    startIsMilestone or endIsMilestone
  end

  def new_week_day(date,inc)
    return date if inc == 0 || date.nil?
    days_moved = 0
    new_date  = date
    movement  = inc/inc.abs

    while days_moved < inc.abs
      begin
        new_date = new_date + movement
      end while [0,6].include? new_date.wday
      days_moved += 1
    end
    new_date
  end

  def new_duration
    (self.start..self.end).to_a.reject{|day| [0,6].include? day.wday}.size
  end

  def has_children?
    !!self.next_task and self.level < self.next_task.try(:level)
  end

  def self.childless
    where.not("exists (select 1 from tasks nt where nt.schedule_id = tasks.schedule_id and nt.index_number = tasks.index_number + 1 and nt.level > tasks.level)")
  end

  def end_month_string
    try(:end).try(:strftime,"%Y/%m   %B %Y")
  end

  def to_ics
    event = Icalendar::Event.new
    event.dtstart       = Icalendar::Values::Date.new(start)
    event.dtend         = Icalendar::Values::Date.new(self.end)
    event.summary       = "#{schedule.schedulable} #{schedule.to_s}: #{self.to_s}"
    event.description   = self.description
    event.ip_class      = "PUBLIC"
    event.created       = self.created_at
    event.last_modified = self.updated_at
    event
  end

  def self.not_on_templates
    where("exists (select null from schedules s where s.id = tasks.schedule_id and s.template is not true)")
  end

  def self.top_level
    where(level: 1)
  end

  def latest_child_task_end
    child_tasks.maximum(:end)
  end
  private :latest_child_task_end

  def earliest_dependent_task_start
    dependent_tasks.minimum(:start)
  end
  private :earliest_dependent_task_start

  def parents_dependent_task_start
    (parent_task and parent_task.dependent_tasks.exists? ? parent_task.dependent_tasks.minimum(:start) : nil)
  end
  private :parents_dependent_task_start
end
