class Google::SheetBasic

  def initialize(spreadsheet_key)
    @spreadsheet_key = spreadsheet_key
  end

  def connection
    @connection    ||= Google::DriveConn.new
  end

  def spreadsheet
    @spreadsheet   ||= connection.session.spreadsheet_by_key(@spreadsheet_key)
  end

  def worksheet
    @worksheet = spreadsheet.worksheets.first
  end

  def list
    @list          ||= worksheet.list
  end

end



__END__


  def contract
    Client.is_liverpool.contracts.join(:books).where(:books =>{:isbn => })
  end






heroku run console -a bibliocloud-monochrome

Rails.logger.level=1
PaperTrail.enabled = false
client          = Client.is_liverpool


OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
connection  = Google::DriveConn.new
spreadsheet = connection.session.spreadsheet_by_key('1ZoZXsVtJ18U6oGSVTrhqt02YgV-XRzrNG7fCeIKAA2M')
worksheet = spreadsheet.worksheets.first
list      = worksheet.list
list.each do |row|
  contract = client.contracts.joins(:books).where(:books =>{:isbn => row['HB ISBN'].strip.presence}).take ||
             client.contracts.joins(:books).where(:books =>{:isbn => row['PB ISBN'].strip.presence}).take ||
             client.contracts.joins(:books).where("upper(contracts.contract_name) = ?", row['BOOK TITLE'].strip.upcase.presence).take
  next unless contract
  row['bibliocloud_contract_id'] = contract.try(:id)
  row['bibliocloud_contact_1'] = contract.contacts[0]
  row['bibliocloud_contact_2'] = contract.contacts[1]
  row['bibliocloud_contact_3'] = contract.contacts[2]
  if contract.contacts.size == 1
    contact  = contract.contacts.take
  else
    contact  = contract.contacts.where(:keynames => row['surname']).take
  end
  row['bibliocloud_contact_id'] = contact.try(:id) if contact
end
worksheet.save
