doc = Nokogiri::XML(open("/Users/david/Documents/Bibliocloud/Clients/British Library/From Nielsen/Record Supply/british_library.xml"))
doc = Nokogiri::XML(open("https://s3-eu-west-1.amazonaws.com/bibliocloudimages/onix/british_library.xml"))



doc.xpath("ONIXMessage/Product").map do |product|
  [
    product.xpath("Title/TitleText").text,
    product.xpath("Imprint/ImprintName").text
  ]
end.group_by(&:first).transform_values{|x| x.map(&:last)}.select{|k,v| v.uniq.size > 1}

doc.xpath("ONIXMessage/Product").map do |product|
  product.xpath("Imprint/ImprintName").text
end.group_by(&:itself).transform_values(&:size)

{"The British Library Publishing Division"=>1533,
  "British Library"=>24,
  "British Library Publishing"=>36,
  "Blackwell Publishers"=>2,
  "Not Avail"=>2,
  "NAKANO Museum Books Ltd"=>1}

Client.is_british_library.works.group(:imprint).count.transform_keys(&:to_s)

{"British Library Publishing"=>1496, "Not Avail"=>2, "NAKANO Museum Books Ltd"=>1, "Blackwell Publishers"=>2}

Client.is_british_library_test.works.group(:imprint).count.transform_keys(&:to_s)





isbns = doc.xpath("ONIXMessage/Product").select do |product|
  product.xpath("Imprint/ImprintName").text == "The British Library Publishing Division"
end.map do |product|
  product.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").text
end

Client.is_british_library.books.where(isbn: isbns).map(&:imprint_name).map(&:to_s)
Client.is_british_library.books.where(isbn: isbns).map(&:work).map(&:imprint).last
Client.is_british_library.imprints.find(685)
Client.is_british_library.works.group(:imprint).count.transform_keys(&:to_s)

isbn_imprint = doc.xpath("ONIXMessage/Product").each_with_object({}) do |product, hash|
  hash[product.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").text] = product.xpath("Imprint/ImprintName").text
end

Hash[*isbn_imprint.flat_map(&:reverse)].transform_values(&:size)

isbn_imprint_sys = Client.is_british_library.books.includes(work: :imprint).each_with_object({}) do |book, hash|
  hash[book.isbn] = book.imprint.value
end

both = isbn_imprint.keys.each_with_object({}) do |isbn, hash|
  hash[isbn] = [isbn_imprint[isbn], isbn_imprint_sys[isbn]]
end

both.select do |key, value|
  value.first != value.second
end

Client.is_british_library.books.includes(:work, client: :imprints).each do |book|
  book.work.update(imprint_id: Client.is_british_library.imprints.where(value: isbn_imprint[book.isbn]).pluck(:id).first )
end


book = Client.is_british_library.books.includes(:work, client: :imprints).first







doc.xpath("ONIXMessage/Product").map do |product|
  product.at_xpath("RecordReference").text
end.uniq.count


file_isbns = doc.xpath("ONIXMessage/Product").map do |product|
  product.at_xpath("ProductIdentifier[ProductIDType=15]/IDValue").text
end.uniq

loaded_isbns = Client.is_british_library.books.pluck(:isbn)