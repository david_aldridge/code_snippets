client = Client.is_pharma

listed_isbns = client.isbnlists.pluck(:number)
listed_isbns.count
listed_isbns.max

used_isbns = client.isbnlists.where(:used => 'used').pluck(:number)
used_isbns.count
used_isbns.max

unused_isbns = client.isbnlists.where(:used => 'not used').pluck(:number)
unused_isbns.count
unused_isbns.max
unused_isbns.min

book_isbns = client.books.pluck(:isbn).select{|isbn| Lisbn.new(isbn).valid?}
book_isbns.count
book_isbns.max
book_isbns.min

incorrect_unused_isbns = unused_isbns & book_isbns
missing_from_list = book_isbns - listed_isbns

used_isbn_prefixes = used_isbns.map{|isbn| Lisbn.new(isbn).isbn_with_dash.split("-")[0..2].join("-")}.each_with_object(Hash.new(0)){|p,h| h[p] += 1}

unused_isbn_prefixes = unused_isbns.map{|isbn| Lisbn.new(isbn).isbn_with_dash.split("-")[0..2].join("-")}.each_with_object(Hash.new(0)){|p,h| h[p] += 1}

book_isbn_prefixes = book_isbns.map{|isbn| Lisbn.new(isbn).isbn_with_dash.split("-")[0..2].join("-")}.each_with_object(Hash.new(0)){|p,h| h[p] += 1}
