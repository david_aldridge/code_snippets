client = Client.is_cpg

ActiveRecord::Base.transaction do
  Work.no_touching do
    client.audiences.where(audience_code_type: "01").delete_all

    client.works.each do |w|
      w.onix_audiences.create(audience_code_type: "01", audience_code_value: "01")
    end
  end
end

01	General/trade	For a non-specialist adult audience. Consider also adding an ONIX Adult audience rating
02	Children/juvenile	For a juvenile audience, not specifically for any educational purpose. An audience range should also be included
03	Young adult	For a teenage audience, not specifically for any educational purpose. An audience range should also be included
04	Primary and secondary/elementary and high school	Kindergarten, pre-school, primary/elementary or secondary/high school education. An audience range should also be included
05	College/higher education	For tertiary education – universities and colleges of higher education
06	Professional and scholarly	For an expert adult audience, including professional development and academic research
07	ELT/ESL	Intended for use in teaching English as a second, non-native or additional language. Indication of the language level (eg CEFR) should be included where possible. An audience range should also be included if the product is (also) suitable for use in primary and secondary education
08	Adult education	For an adult audience in a formal or semi-formal learning setting, eg vocational training, apprenticeships, or academic or recreational learning for adults
09	Second language teaching	Intended for use in teaching second, non-native or additional languages, for example teaching German to Spanish speakers. Indication of the language level (eg CEFR) should be included where possible. An audience range should also be included if the product is (also) suitable for use in primary and secondary education. Prefer code 07 for products specific to teaching English
© EDItEUR