
ActiveRecord::Base.transaction do
  Client.is_leuven.works.joins(:marketingtexts).where(marketingtexts: {legacy_code: "18"}).uniq.each do |work|
    next if work.marketingtexts.where(main_type: %w(01 02)).any?
    back_cover_text = work.marketingtexts.where(legacy_code: "18").last
    description = back_cover_text.dup
    if description.pull_quote.size <= 350
      description.main_type   = "01"
      description.text_type   = "02"
      description.legacy_code = "02"
    else
      description.main_type   = "02"
      description.text_type   = "03"
      description.legacy_code = "01"
    end
    description.save!
    description.books = back_cover_text.books
  end
end


ActiveRecord::Base.transaction do
  Client.is_unbound.works.joins(:marketingtexts).where(marketingtexts: {main_type: "12"}).uniq.each do |work|
    next if work.marketingtexts.where(main_type: "01").any?
    keynote = work.marketingtexts.where(main_type: "12").last
    description = keynote.dup
    description.main_type   = "01"
    description.text_type   = "02"
    description.legacy_code = "02"
    description.save!
    description.books = work.books
  end
end

