a = "aefweGERHRTNERYNETB"
b = "adrhaehsrtrynetyertye"

require 'benchmark/ips'

Benchmark.ips do |x|
  x.report("1") { "A: #{a} B: #{b}" }
  x.report("2") { "A: " + a + " B: " + b}
  x.report("2") { "A: " << a << " B: " << b}
end

require 'memory_profiler'
MemoryProfiler.report do
  "A: #{a} B: #{b}"
end.pretty_print


require 'memory_profiler'
MemoryProfiler.report do
  "A: " + a + " B: " + b
end.pretty_print


require 'memory_profiler'
MemoryProfiler.report do
  "A: " << a << " B: " << b
end.pretty_print

MemoryProfiler.report do
  (("A: " << a) << " B: ") << b
end.pretty_print

MemoryProfiler.report do
  ("A: " << a) << (" B: " << b)
end.pretty_print
