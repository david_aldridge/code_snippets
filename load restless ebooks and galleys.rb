OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

list =
  Google::DriveConn.new.
  session.
  spreadsheet_by_key("11MAt9T-tVWIJBSKfAdiu1ccCJrLL0Yc6gDelX927gUI").
  worksheet_by_title("restless works").
  list


client = Client.is_restless_books

galley_format = client.proprietary_format_descriptions.find_by(name: "Galley")
galley_edition = client.proprietary_edition_descriptions.find_by(name: "Galley")
ActiveRecord::Base.transaction do
  list.each do |row|
    work = client.works.find_by(id: row["System Work ID"].strip)
    unless work
      puts "Cannot find work #{row["System Work ID"]}"
      next
    end

    isbn = row["eBook ISBN"].strip
    book = client.books.find_by(isbn: row["System Work ID"])
    if book
      puts "ISBN already exists: #{isbn}"
      next
    end

    new_book =
      work.books.create(
        isbn:                               isbn,
        pub_date:                           Date.parse(row["Publication date"]),
        proprietary_edition_description_id: 917,
        proprietary_format_description_id:  2668,
        pages_arabic:                       work.books.maximum(:pages_arabic)
        )

    unless new_book.valid?
      puts "Errors on #{isbn}: #{new_book.errors.messages}"
      next
    end

    price = MoneyStringToFloat.new(row["eBook price"]).call
    new_book.current_usd_exctax_consumer_price = price

    new_book.workcontacts        = work.workcontacts
    new_book.supportingresources = work.supportingresources





    isbn = row["Galley ISBN"].strip.presence
    unless isbn
      puts "No ISBN"
      next
    end

    galley = client.books.find_by(isbn: isbn)
    if galley
      puts "ISBN already exists: #{isbn}"
      next
    end

    new_book =
      work.books.create(
        isbn:                            isbn,
        proprietary_edition_description: galley_edition,
        proprietary_format_description:  galley_format,

        )

    unless new_book.valid?
      puts "Errors on #{isbn}: #{new_book.errors.messages}"
      next
    end
  end
  # raise ActiveRecord::Rollback
end



# client.books.where("created_at >= ?", Time.now - 1.hour).destroy_all


