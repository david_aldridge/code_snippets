client = Client.is_unbound

formats = client.proprietary_format_descriptions.where(sales_restriction_internal_use_only: true)

books = client.books.where(proprietary_format_description: formats)

puts books.size

ActiveRecord::Base.transaction do
  books.each do |book|
    book.copy_format_values
    book.save
  end
  # raise ActiveRecord::Rollback
end



client.books.
  joins(:proprietary_edition_description, :proprietary_format_description).
  includes(:proprietary_edition_description, :proprietary_format_description).
  select do |book|
    book.proprietary_edition_description.name.ends_with?("Special") &&
    !book.proprietary_format_description.name.ends_with?("S")
  end.
  each do |book|
    new_format = client.proprietary_format_descriptions.find_by(name: book.proprietary_format_description.name + "S")
    if new_format
      puts "From #{book.proprietary_format_description.name} to #{new_format.name}"
      book.update(proprietary_format_description_id: new_format.id)
    else
      puts false
    end
  end; 0











