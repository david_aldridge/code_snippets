Book.joins(:proprietary_format_description).includes(:proprietary_format_description).each do |b|
  b.assign_attributes(
    :epub_type_code          => b.epub_type_code          || b.proprietary_format_description.epub_type_code         ,
    :epub_type_version       => b.epub_type_version       || b.proprietary_format_description.epub_type_version      ,
    :epub_type_description   => b.epub_type_description   || b.proprietary_format_description.epub_type_description  ,
    :epub_format_code        => b.epub_format_code        || b.proprietary_format_description.epub_format_code       ,
    :epub_format_version     => b.epub_format_version     || b.proprietary_format_description.epub_format_version    ,
    :epub_format_description => b.epub_format_description || b.proprietary_format_description.epub_format_description,
    :epub_source_code        => b.epub_source_code        || b.proprietary_format_description.epub_source_code       ,
    :epub_source_version     => b.epub_source_version     || b.proprietary_format_description.epub_source_version    ,
    :epub_source_description => b.epub_source_description || b.proprietary_format_description.epub_source_description,
    :epub_type_note          => b.epub_type_note          || b.proprietary_format_description.epub_type_note         ,
    :product_form            => b.product_form            || b.proprietary_format_description.product_form           ,
    :product_form_detail     => b.product_form_detail     || b.proprietary_format_description.product_form_detail    )
  b.save if b.changed?
end ; 0
