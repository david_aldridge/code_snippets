

client                 = Client.is_zed
#recipient_name         = 'UBS Press (Singapore)'
recipient_name         = 'Wordery'
host                   = 'ftp03.wordery.net'
uid                    = 'bibliocloud'
pwd                    = 'Aikficigmun4'
subtype                = DigitalAssetSubtype.find(2)
onix_file_name_pattern = 'zedbooks[%Y%m%d%H%M%S].xml'

contact        = client.contacts.create!(corporate_name: recipient_name,
                                        onix_to_company: recipient_name)

contact.contact_types = [ContactType.find_by(:name => 'Data aggregator')]

ftp_template   = contact.ftp_templates.create!(client_id:              client.id,
                                               name:                   "#{recipient_name} ONIX",
                                               login_user:             uid,
                                               login_password:         pwd,
                                               connect_host:           host,
                                               onix_file_name_pattern: onix_file_name_pattern)

cdatt = client.contact_digital_asset_transfer_templates.create!(
            contact_id: contact.id,
            digital_asset_subtype_id: subtype.id,
            transferable_id: ftp_template.id,
            transferable_type: 'FtpTemplate',
            price_type_qualifier_codes: ['05'],
            send_ebook_files: false,
            send_cover_files: true,
            include_media_file_link: true,
            suppress_market_representations: false,
            cover_style: "jpg_rgb_1500h")

  push = DigitalAssetPush.new(  client,
                                cdatt,
                                :product_forms  => ['BB', 'BC'],
                                :pub_dates_from => Date.today - 3.months,
                                :pub_dates_to   => Date.today + 1.months)

  push.create_batch
  push.get_content_items
  push.build_content_file
  push.transmit_file
