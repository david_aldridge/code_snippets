client = Client.is_british_library

client.books.group(:product_form).count

client.prices.joins(:book).where(books: {product_form: "WW"}).gbp.pluck(
  :book_id,
  :tax_rate_code,
  :tax_rate_percent,
  :taxable_amount,
  :tax_value,
  :tax_rate_code_2,
  :tax_rate_percent_2,
  :taxable_amount_2,
  :tax_value_2
)

pp _

[119004, "Z", 0.0, 0.4999e2, 0.0, "S", 0.2e2, 0.1663e2, 0.333e1],
[119122, nil, nil, nil, nil, nil, nil, nil, nil],
[118976, "Z", 0.0, 0.0, 0.0, "S", 0.2e2, 0.6246e2, 0.1249e2]]



client.prices.joins(:book).where.not(books: {product_form: "WW"}).update_all(
 discount_code_type:     nil,
 discount_code:          nil,
 supplydetail_id:        nil,
 defaultprice:           false,
 unit_of_pricing:        "00",
 minimum_order_quantity: 1,
 tax_rate_code:          nil,
 tax_rate_percent:       nil,
 taxable_amount:         nil,
 tax_value:              nil,
 tax_rate_code_2:        nil,
 tax_rate_percent_2:     nil,
 taxable_amount_2:       nil,
 tax_value_2:            nil
)
