
1. Get file from AWS
2. Create data-only dump file for a specific table

/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_restore --data-only --table=work_derivations /Users/david/Downloads/2018-09-26-02-01-bibliocloud-iop-DATABASE_URL.dump > restore.dump

3. Optionally create a new table to restore to ...
heroku pg:psql -a bibliocloud-iop
create table work_derivations_restor as select * from work_derivations limit (0);

4. Optionally edit the restore.dump to restore to the different table

5. Restore the data

heroku pg:psql -a bibliocloud-iop < restore.dump

class Price
  attr_accessible :created_at
end

PaperTrail::Version.
  where(item_type: "Price").
  where("created_at >= ?", Date.today - 1.day).
  where("item_id > 296205 and item_id < 296276").
  map{|v| PaperTrail.serializer.load(v.object_changes)}.
  map{|c| c.transform_values(&:last)}.
  map{|c| c.delete("id"); c}.
  each{|c| Price.create(c)}

Book.update_all("prices_count = (select count(*) from prices x where x.book_id = books.id)")




heroku config:set MAINTENANCE_PAGE_URL=//s3.amazonaws.com/<your_bucket>/your_maintenance_page.html


/Applications/Postgres.app/Contents/Versions/9.6/bin/pg_dump $(heroku config:get DATABASE_URL -a bibliocloud-app) --data-only --table=prices > prices_only.dump

pg_dump $(/app/vendor/heroku-toolbelt/bin/heroku config:get DATABASE_URL -a bibliocloud-app) --data-only --table=prices > prices_only.dump